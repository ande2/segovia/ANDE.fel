#########################################################################
## Function  : msg
##
## Parameters: msg
##
## Returnings: none
##
## Comments  : Devuelve un mensaje de Error
#########################################################################
--DATABASE ventas

FUNCTION msg(msg)
   DEFINE msg  STRING
   CALL box_valdato(msg)
END FUNCTION

#########################################################################
## Function  : lib_cleanTinyScreen()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Limpia el background de la ventana desplegada
#########################################################################
FUNCTION lib_cleanTinyScreen()
define screenToBeCleaned   om.DomNode
define doc                 om.DomDocument
define l                   om.NodeList
define att                 STRING
constant nodeName = "Window"
constant nodeAtt  = "screen"

   let doc = ui.Interface.getDocument()
   let screenToBeCleaned = doc.getDocumentElement()
   let l = screenToBeCleaned.selectByTagName(nodeName)
   let screenToBeCleaned = l.item(1)
   let att = screenToBeCleaned.getAttribute("name")
   if att = nodeAtt then
      call doc.removeElement(screenToBeCleaned)
   end if
END FUNCTION

#########################################################################
## Function  : combo_din2
##
## Parameters: inputParameters
##
## Returnings: returnParameters
##
## Comments  : Funcion que genera un combo dinamico
#########################################################################
{FUNCTION combo_din2(vl_2_campo, vl_2_query)
define		vl_2_campo		  string
define		vl_2_query		  string
define		vl_2_lista		  char(500)
define		vl_2_lista2	  char(500)
define		vl_2_metodo	  string
define		vl_2_valor		  string
define		vl_2_valor2	  string
define		vl_2_combo      ui.ComboBox
define		vl_2_lista_nodb base.StringTokenizer

	let vl_2_campo = vl_2_campo.trim()
	let vl_2_query = vl_2_query.trim()

	#-- Obtiene el nodo del metodo Combo de la clase ui
	let vl_2_combo = ui.ComboBox.forName(vl_2_campo)
	if vl_2_combo is null then
		return
	end if

	#-- Limpia los registros anteriores del combo
	call vl_2_combo.clear()

	#-- Procesa segun el metodo convirtiendo a mayusculas para mejor tratamiento
	let vl_2_metodo = vl_2_query.toUpperCase()

	if vl_2_metodo matches "SELECT *"   then	
		#--Prepara el query
		prepare q_comboList2 from vl_2_query
		declare c_comboList2 cursor for q_comboList2
		#-- Obtiene la lista de valores de la tabla segun el query enviado
		foreach c_comboList2 into vl_2_lista, vl_2_lista2
			let vl_2_valor = vl_2_lista clipped
			let vl_2_valor2 = vl_2_lista2 clipped
			call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end foreach	
	else
	   #-- Obtiene la lista de valores que no son de base de datos
		let vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query, "|")
		while vl_2_lista_nodb.hasMoreTokens()
         let vl_2_valor = vl_2_lista_nodb.nextToken()
         call vl_2_combo.addItem(vl_2_valor, vl_2_valor)
		end while
	end if
END FUNCTION}


--variante, aqui si pueden mandar la descripcion del campo!!
FUNCTION combo_dinamic(vl_2_campo, vl_2_query)
define		vl_2_campo		  string
define		vl_2_query		  string
define		vl_2_lista		  char(500)
define		vl_2_lista2	  char(500)
define		vl_2_metodo	  string
define		vl_2_valor		  string
define		vl_2_valor2	  string
define		vl_2_combo      ui.ComboBox
define		vl_2_lista_nodb base.StringTokenizer

	let vl_2_campo = vl_2_campo.trim()
	let vl_2_query = vl_2_query.trim()

	#-- Obtiene el nodo del metodo Combo de la clase ui
	let vl_2_combo = ui.ComboBox.forName(vl_2_campo)
	if vl_2_combo is null then
		return
	end if

	#-- Limpia los registros anteriores del combo
	call vl_2_combo.clear()

	#-- Procesa segun el metodo convirtiendo a mayusculas para mejor tratamiento
	let vl_2_metodo = vl_2_query.toUpperCase()

	if vl_2_metodo matches "SELECT*"   then	
		#--Prepara el query
		prepare q_combodin from vl_2_query
		declare c_combodin cursor for q_combodin
		#-- Obtiene la lista de valores de la tabla segun el query enviado
		foreach c_combodin into vl_2_lista, vl_2_lista2
			let vl_2_valor = vl_2_lista clipped
			let vl_2_valor2 = vl_2_lista2 clipped
			call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end foreach	
	else
	   #-- Obtiene la lista de valores que no son de base de datos
		let vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query, "|")
		while vl_2_lista_nodb.hasMoreTokens()
         let vl_2_valor = vl_2_lista_nodb.nextToken()
         let vl_2_valor2 = vl_2_lista_nodb.nextToken()
         call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end while
	end if
END FUNCTION

#########################################################################
## Function  : writeXML()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Escribe un arbol AUI en XML
#########################################################################
FUNCTION writeXML()
   DEFINE doc        om.DomDocument
   DEFINE n, t, t2   om.DomNode
   DEFINE w          ui.Window
   DEFINE xxx        STRING
   DEFINE l          om.NodeList
   
   LET doc = ui.Interface.getDocument()
   LET n = ui.Interface.getRootNode()
   CALL n.writeXml("aui.xml")
   ERROR "File aui.xml written"
END FUNCTION

#########################################################################
## Function  : confirma()
##
## Parameters: msg         << Pregunta a confirmar
##
## Returnings: res         >> Respuesta
##
## Comments  : Confirmacion
#########################################################################
FUNCTION confirma(msg)
   DEFINE msg  STRING
   DEFINE res  SMALLINT
   LET res = FALSE
   MENU "Confirmacion..." ATTRIBUTES (STYLE="dialog", COMMENT=msg, IMAGE="question")
      COMMAND "Si"
         LET res = TRUE
         EXIT MENU
      COMMAND "No"
         LET res = FALSE
         EXIT MENU
   END MENU
   RETURN res
END FUNCTION

FUNCTION createStartMenuGroup(p,t)
   DEFINE p om.DomNode
   DEFINE t STRING
   DEFINE s om.DomNode

   LET s = p.createChild("StartMenuGroup")
   CALL s.setAttribute("text",t)
   RETURN s
END FUNCTION

FUNCTION createStartMenuCommand(p,t,c,i)
   DEFINE p om.DomNode
   DEFINE t,c,i STRING
   DEFINE s om.DomNode

   LET s = p.createChild("StartMenuCommand")
   CALL s.setAttribute("text",t)
   CALL s.setAttribute("exec",c)
   CALL s.setAttribute("image",i)
RETURN s
END FUNCTION

#########################################################################
## Function  : createGrid()
##
## Parameters: width    << Ancho del Grid
##             height   << Alto del Grid
##             wName    << Window name
##
## Returnings: none
##
## Comments  : Crea un Grid en la forma
#########################################################################
FUNCTION createGrid(width, height, wName)
   DEFINE w             ui.Window
   DEFINE n, g          om.DomNode
   DEFINE width, height SMALLINT
   DEFINE wName         STRING
   
   LET w = ui.Window.getCurrent()
   LET g = w.findNode("Form", wName)
   LET n = g.createChild("Grid")
      CALL n.setAttribute("width", width)
      CALL n.setAttribute("height", height)
END FUNCTION

#########################################################################
## Function  : Opcion en ToolBar()
##
## Parameters: name     << commando de la opcion 
##             text   <<   nombre que aparece en la opcion 
##             comment   << comentario de la opcion 
#              image     << imagen en el 
## Returnings: retorna la opcion 
##
## Comments  : construye cada opcion depediendo el orden que la coloques
#########################################################################
FUNCTION createToolBarItem(tb,n,t,c,i)
DEFINE tb om.DomNode
DEFINE n,t,c,i VARCHAR(100)
DEFINE tbi om.DomNode
DEFINE h INTEGER

LET tbi = tb.createChild("ToolBarItem")
CALL tbi.setAttribute("name",n CLIPPED)
CALL tbi.setAttribute("text",t CLIPPED)
CALL tbi.setAttribute("comment",c CLIPPED)
CALL tbi.setAttribute("image",i CLIPPED)

RETURN tbi

END FUNCTION

#########################################################################
## Function  : Opcion del separador del ToolBar()
##
## Parameters: tb     << commando de la opcion
##             
##          
#              
## Returnings: la separacion de las opciones
##
## Comments  : construye el separador del orden de las opciones del menu
#########################################################################

FUNCTION createToolBarSeparator(tb) 
DEFINE tb  om.DomNode,
       tbs om.DomNode 

	LET  tbs = tb.createChild("ToolBarSeparator")

	RETURN tbs
	
END FUNCTION 


#########################################################################
## Function  : Opciones del TopMenu()
##
## Parameters: tb     << commando de la opcion
##
##
## Returnings: la separacion de las opciones
##
## Comments  : construye las opcionees del topmenu
#########################################################################


FUNCTION creaopcion(tmg,opcion,texto,comen,imagen)
DEFINE tmg om.DomNode, 
       tmi om.DomNode, 
       opcion, texto, comen,imagen VARCHAR(100)		 


		 LET tmi = tmg.createChild("TopMenuCommand")
		 CALL tmi.setAttribute("name",opcion clipped)
		 CALL tmi.setAttribute("text",texto clipped)
		 CALL tmi.setAttribute("comment",comen clipped)
		 CALL tmi.setAttribute("image",imagen clipped)
		 
		 RETURN tmi
		 
END FUNCTION 

#########################################################################
## Function  : get_usuario()
##
## Parameters: get_usuario    << busca el id del usuario 
##
##
## Returnings: devuelve el id del usuario 
##
## Comments  : 
#########################################################################

FUNCTION get_usuario(idusu) 
DEFINE idusu char(20) ,
       gusu_id INTEGER 
 LET idusu = idusu clipped 

 SELECT adm_usu.usu_id
            INTO gusu_id 
            FROM adm_usu
            WHERE adm_usu.usu_nomunix =idusu 

 IF SQLCA.SQLCODE = 0 THEN
    RETURN gusu_id 
 ELSE 
    LET gusu_id = 0
  RETURN gusu_id 
	 
 END IF 
END FUNCTION 

#########################################################################
## Function  : get_usuname()
##
## Parameters: id_usu    << login del usuario
##
##
## Returnings: devuelve el Nombre del usuario
##
## Comments  : 
#########################################################################

FUNCTION get_usuname(idusu) 
DEFINE idusu VARCHAR(15),
       gusunom VARCHAR(30) 

 SELECT nvl(adm_usu.usu_nom,'**SIN NOMBRE**')
            INTO gusunom 
            FROM adm_usu
            WHERE adm_usu.usu_nomunix =idusu 

 IF SQLCA.SQLCODE <> 0 THEN
    LET gusunom = idusu
 END IF
 RETURN gusunom 
END FUNCTION 

#########################################################################
## Function  : get_llavcomp()
##
## Parameters: get_usuario    << devuelve un numero de una llave compuesta
##
##
## Returnings: devuelve el id del usuario
##
## Comments  :
#########################################################################

FUNCTION get_llavcomp(id1,id2)
	DEFINE id1,id2,id3 integer

   IF id1 IS NOT NULL THEN 
		LET id3 = id1*1000
   	LET id3 = id3+id2 
      RETURN id3
   ELSE
	   RETURN 0 
	END IF

END FUNCTION
#########################################################################
## Function  : lib_showhist()
##
## Parameters: get_usuario    << muestra un historial de estados
##
##
## Returnings: muestra un listado de todos los cambios realizados 
##             al registro que esta en ese momento
##
## Comments  :
#########################################################################


FUNCTION lib_showhist(mtipo,vlink)
   
   DEFINE mtipo,vlink integer, 
          num_row,j  smallint
	 {
   DEFINE ga_dhist  DYNAMIC ARRAY OF RECORD  -- arreglo historial
          corre      LIKE mvitcamest.usu_id,
          est_desc   LIKE mest.est_desc,
          vce_fechor LIKE mvitcamest.vce_fechor,
          usu_nom    LIKE adm_usu.usu_nom
   END RECORD

   DEFINE na_dhist  DYNAMIC ARRAY OF RECORD  -- arreglo historial
          corre      LIKE mvitcamest.usu_id,
          est_desc   LIKE mest.est_desc,
          vce_fechor LIKE mvitcamest.vce_fechor,
          usu_nom    LIKE adm_usu.usu_nom
   END RECORD

   DECLARE historial_general CURSOR FOR
      SELECT 0,mest.est_desc,mvitcamest.vce_fechor,adm_usu.usu_nom
      FROM mest, mvitcamest, adm_usu
      WHERE  mvitcamest.vce_linkdoc = vlink
      AND mvitcamest.tip_id = mtipo
      AND mest.est_id = mvitcamest.est_id
      AND adm_usu.usu_id = mvitcamest.usu_id
      ORDER BY 3

   LET num_row =  1
   FOREACH historial_general  INTO ga_dhist[num_row].*
      LET ga_dhist[num_row].corre = num_row
      LET num_row = num_row + 1
   END FOREACH

   OPEN WINDOW show_historial AT 5,10 WITH FORM "hist_esta"
   CALL fgl_settitle("Consulta historial")

   DISPLAY  ARRAY ga_dhist To sa_dhist[j].*
            BEFORE DISPLAY
                   CALL fgl_dialog_setkeylabel("ACCEPT","Salir")
                   CALL fgl_dialog_setkeylabel("INTERRUPT","")

   END DISPLAY
   CALL ga_dhist.clear()

   CLOSE WINDOW show_historial
}
	RETURN 


END FUNCTION
            
#########################################################################
## Function  : lib_showhist()
##
## Parameters: get_usuario    << muestra un historial de estados
##
##
## Returnings: muestra un listado de todos los cambios realizados
##             al registro que esta en ese momento
##
## Comments  :
#########################################################################
				
FUNCTION lib_bitacora(mtipo,vlink,mestado)
 DEFINE mtipo,vlink,mestado, musu_id integer  




     LET musu_id = get_usuario(FGL_GETENV("LOGNAME")) 
     
     INSERT INTO mvitcamest VALUES(mtipo,vlink,mestado,current,musu_id) 

     IF SQLCA.SQLCODE <0  THEN 
        return 1 
	  ELSE 
        return 0 
     END IF  

END FUNCTION

#########################################################################
## Function  : lib_helpFIELD(vappl,vtipo,vayu_nom)
##
## Parameters: aplicacion, tipo y nombre de campo
##
## Returnings: llamara a la funcion del HELP
##
## Comments  : en base al nombre de campo, buscara el HELP correspondiente (segun aplicacion y tipo)
#########################################################################
FUNCTION lib_helpfield(vappl, vtipo, vayu_nom)
   DEFINE 
	 vappl, vtipo, vlink integer,
	 vayu_nom VARCHAR(50),
    gr_mayuda RECORD
      link integer,
      apl_id integer,
		ayu_tip SMALLINT,
      ayu_nom varchar(20),
      ayu_des VARCHAR(100),
      ayu_con VARCHAR(2500),
		ayu_tooltip VARCHAR(255),
      ayu_ref VARCHAR(25)
   END RECORD

	#vtipo = "1|Campo|2|Menu|3|Aplicacion|4|Boton"

   SELECT  link into vlink
    FROM ande:mayuda
    WHERE  ande:mayuda.apl_id = vappl
    AND    ande:mayuda.ayu_tip= vtipo
    AND    ande:mayuda.ayu_nom= vayu_nom

   IF SQLCA.SQLCODE <> 0  THEN
		CALL box_valdato("No hay registro de ayuda para el campo \""||vayu_nom CLIPPED||"\"")
	ELSE
		CALL lib_showhelp(vlink, "")
	END IF
END function

#########################################################################
## Function  : lib_gethelp()
##
## Parameters: opci, vlink   << Si desea Contenido(1), tooltip(2) y el link
##
## Returnings: El contenido o Tooltip de la ayuda
##
## Comments  :
#########################################################################
FUNCTION lib_gethelp(opci,vlink)
   DEFINE opci,vlink integer,
    gr_mayuda RECORD
      link integer,
      apl_id integer,
		ayu_tip SMALLINT,
      ayu_nom varchar(20),
      ayu_des VARCHAR(100),
      ayu_con VARCHAR(2500),
		ayu_tooltip VARCHAR(255),
      ayu_ref VARCHAR(25),
		ayu_icono VARCHAR(50)
   END RECORD

   SELECT  ayu_con, ayu_tooltip, ayu_icono into gr_mayuda.ayu_con, gr_mayuda.ayu_tooltip, gr_mayuda.ayu_icono
    FROM ande:mayuda
    WHERE  ande:mayuda.link = vlink

   IF SQLCA.SQLCODE <> 0  THEN
		LET gr_mayuda.ayu_con = NULL
		LET gr_mayuda.ayu_tooltip = NULL
		LET gr_mayuda.ayu_icono = NULL
	END IF

	CASE opci 
		WHEN 1 RETURN gr_mayuda.ayu_con
		WHEN 2 RETURN gr_mayuda.ayu_tooltip||"    (id:"||vlink||")"
		WHEN 3 RETURN gr_mayuda.ayu_icono
		OTHERWISE RETURN NULL
	END CASE
END function

#########################################################################
## Function  : lib_showHELP()
##
## Parameters: vlink, vappl    << muestra un historial de estados
##							el parametro de vappl queda para un futuro, ahora NO funciona para nada
##
##
## Returnings: muestra un listado de todos los cambios realizados
##             al registro que esta en ese momento
##
## Comments  :
#########################################################################
FUNCTION lib_showhelp(vlink,vappl)
   DEFINE mtipo,vlink,link1, vlink2 integer,
          num_row,j,i,bandera, pasos, registros   smallint,
          minicio CHAR(1), rsql CHAR(1000),
          vappl STRING,
          w ui.Window,
          f ui.Form,
			 tipo VARCHAR(25),
    gr_mayuda RECORD
      link integer,
      apl_id integer,
		ayu_tip SMALLINT,
      ayu_nom varchar(20),
      ayu_des VARCHAR(100),
      ayu_con VARCHAR(2500),
      ayu_icono VARCHAR(100)
   END RECORD,
   ga_dayuda DYNAMIC ARRAY OF RECORD
      link_a INTEGER,
		ayu_icono VARCHAR(100),
      cam_desc_a VARCHAR(100)
   END RECORD,
   f_nomlog STRING

   LET f_nomlog =FGL_GETENV("PICSDIR")

	create TEMP TABLE tmp_pasos 
	(paso INTEGER, 
	 link INTEGER) WITH NO LOG

   LET w = ui.Window.getcurrent()
   LET f = w.getForm()

   CALL f.setelementHidden("anterior",1)
   CALL f.setelementHidden("proximo",1)
   OPEN WINDOW show_ayuda AT 5,10 WITH FORM "form_ayuda"
   CALL fgl_settitle("Ayuda en linea")

    DELETE FROM tmp_pasos
    LET pasos= 0
    LET minicio="A"
--inicio ciclo

    WHILE TRUE
      SELECT link, apl_id, ayu_tip, ayu_nom, ayu_des, ayu_con, ayu_icono into gr_mayuda.*
      FROM ande:mayuda
      WHERE  ande:mayuda.link = vlink

      IF SQLCA.SQLCODE <> 0  THEN
         CALL box_valdato("No hay ayuda disponible para este tema  ")
         IF pasos > 1 THEN
                LET link1 = NULL
                LET pasos = pasos - 1
                SELECT link INTO link1
                       FROM tmp_pasos WHERE paso = pasos
                IF SQLCA.SQLCODE = 0 THEN
                    LET vlink = link1
                    CONTINUE WHILE
                end IF

         ELSE
            EXIT WHILE
         END IF
		END IF


		LET gr_mayuda.ayu_icono = f_nomlog||gr_mayuda.ayu_icono CLIPPED

      LET rsql = "select a.link,  a.ayu_icono, a.ayu_des ",
              "from ande:mayudar r, ande:mayuda a ",
              "WHERE r.link1 =", vlink,
              " AND a.link = r.link2 ",
              " order by a.ayu_des , a.link " CLIPPED
      PREPARE ex_ayuda FROM rsql
      DECLARE ayuda_ptr  CURSOR FOR  ex_ayuda
      LET i =1

      FOREACH ayuda_ptr INTO ga_dayuda[i].*
			LET ga_dayuda[i].ayu_icono = f_nomlog||ga_dayuda[i].ayu_icono CLIPPED
        	LET i=i+1
      END FOREACH
      LET ga_dayuda[i].cam_desc_a = "Total temas relacionados: ",i-1 USING "##&"
      LET ga_dayuda[i].link_a = 0

      IF minicio= "A" THEN
         LET pasos= pasos+1
         INSERT INTO tmp_pasos VALUES(pasos,vlink)
         LET minicio= "B"
      END IF
      LET bandera= 0


		CASE gr_mayuda.ayu_tip
			WHEN 1
				LET tipo = 'Campo'
			WHEN 2
				LET tipo = 'Menu'
			WHEN 3
				LET tipo = 'Aplicacion/Modulo'

			WHEN 4
				LET tipo = 'Boton'
			OTHERWISE
				LET tipo = ''
		END CASE

    	DISPLAY by name  gr_mayuda.link, gr_mayuda.ayu_nom, gr_mayuda.ayu_des, gr_mayuda.ayu_con, gr_mayuda.ayu_icono, tipo

      DISPLAY  ARRAY ga_dayuda To sr[j].*

            BEFORE DISPLAY
                 CALL fgl_dialog_setkeylabel("ACCEPT","")
                 CALL fgl_dialog_setkeylabel("INTERRUPT","")

      			IF pasos > 1 THEN
						CALL DIALOG.setActionActive("anterior",TRUE)
						CALL DIALOG.setActionActive("proximo",TRUE)
      			ELSE
						CALL DIALOG.setActionActive("anterior",FALSE)
						CALL DIALOG.setActionActive("proximo",FALSE)
	 				END IF

            BEFORE ROW
                 LET num_row = ARR_CURR()
                 LET vlink =  ga_dayuda[num_row].link_a

           ON ACTION buscar
			  		CALL lib_findhelp() RETURNING link1
               LET vlink =  link1
               IF vlink = 0 THEN
                  LET bandera= 1
                  EXIT DISPLAY
               ELSE
                  LET pasos=pasos+1
                  LET registros = 0
                  LET link1     = null
                  SELECT paso,link  INTO registros,link1
                      FROM tmp_pasos WHERE paso=pasos
                  IF SQLCA.SQLCODE <> 0 THEN
                      INSERT INTO tmp_pasos VALUES(pasos,vlink)
                  ELSE
                     IF link1 <> vlink then
                      	delete from tmp_pasos where paso >=pasos
                        INSERT INTO tmp_pasos VALUES(pasos,vlink)
                     END IF
                  END IF
                  LET bandera = 2
                  EXIT DISPLAY
               END IF


            ON ACTION consultar
               IF vlink IS NULL THEN
                  LET bandera= 1
                  EXIT DISPLAY
               ELSE
                  LET pasos=pasos+1
                  LET registros = 0
                  LET link1     = null
                  SELECT paso,link  INTO registros,link1
                      FROM tmp_pasos WHERE paso=pasos
                  IF SQLCA.SQLCODE <> 0 THEN
                      INSERT INTO tmp_pasos VALUES(pasos,vlink)
                  ELSE
                     IF link1 <> vlink then
                      	delete from tmp_pasos where paso >=pasos
                        INSERT INTO tmp_pasos VALUES(pasos,vlink)
                     END IF
                  END IF
                  LET bandera = 2
                  EXIT DISPLAY
               END IF

            ON ACTION salir
                 LET int_flag=FALSE
                 LET bandera=1
                 EXIT DISPLAY

            ON ACTION anterior
                 LET link1 = NULL
                 LET pasos = pasos - 1
                 SELECT link INTO link1
                       FROM tmp_pasos WHERE paso = pasos
                 IF SQLCA.SQLCODE = 0 THEN
                    LET vlink = link1
                    let bandera = 2
                    EXIT DISPLAY
                 ELSE
                    LET pasos = pasos + 1
                 END IF


           ON ACTION proximo
                 LET link1 = NULL
                 LET pasos = pasos + 1
                 SELECT link INTO link1
                       FROM tmp_pasos WHERE tmp_pasos.paso = pasos
                  IF SQLCA.SQLCODE = 0 THEn
                       LET vlink = link1
                       LET bandera= 2
                       EXIT DISPLAY
                 ELSE
                       LET pasos = pasos -1
                 END IF

            AFTER DISPLAY
                  IF INT_FLAG THEN
                      LET int_flag=FALSE
                      LET bandera=1
                  END IF
   END DISPLAY
   IF bandera= 1 THEN
      EXIT WHILE
   ELSE
       IF vlink IS  NULL THEN
          EXIT WHILE
       ELSE
           IF bandera = 0 THEN
            LET pasos=pasos+1
              LET registros = 0
              LET link1     = null
              SELECT paso,link  INTO registros,link1
                   FROM tmp_pasos WHERE paso=pasos
              IF SQLCA.SQLCODE <> 0 THEN
                 INSERT INTO tmp_pasos VALUES(pasos,vlink)
              ELSE
                  IF link1 <> vlink then
                     delete from tmp_pasos where paso >=pasos
                     INSERT INTO tmp_pasos VALUES(pasos,vlink)
                  END IF

              END IF
           ELSE
              let bandera = 0
           END IF
           CALL ga_dayuda.clear()
           CONTINUE WHILE
      END IF
   END IF

   END WHILE

	DROP TABLE tmp_pasos
   CLOSE window show_ayuda

END FUNCTION


#########################################################################
## Function  : lib_findhelp()
##
## Parameters: 
##
##
## Returnings: link a tema encontrado
##
## Comments  :
#########################################################################
FUNCTION lib_findhelp()
   DEFINE retorna integer,
          j,i, aborta, registros   smallint,
          w ui.Window,
          f ui.Form,
	la_temas dynamic ARRAY OF RECORD 	# Detalle de temas relacionados
		link2 INTEGER,
      aplica   VARCHAR(100),
		tipo VARCHAR(50),
		tema VARCHAR(100)
   END RECORD,
	refe VARCHAR(100),
	buscar VARCHAR(105)


   OPEN WINDOW find_ayuda AT 5,10 WITH FORM "form_ayuda2"
   LET w = ui.Window.getcurrent()
   LET f = w.getForm()

   CALL fgl_settitle("Busca tema de Ayuda")

	CALL combo_dinamic("formonly.tipo", "1|Campo|2|Menu|3|Aplicacion|4|Boton")

	CALL combo_dinamic("formonly.aplica", "SELECT b.prog_id, b.prog_nom"||
						 " FROM ande:mprog a, ande:mprog b"|| 
						 " WHERE a.prog_tip = 'M' "||
						 " AND a.prog_padre = -1 "||
						 " AND b.prog_padre = a.prog_id"||
						 " ORDER BY b.prog_nom")

	LET retorna = 0  LET refe = "Ingrese tema a buscar..." 
	WHILE TRUE
			LET aborta = 0  LET INT_FLAG = FALSE  LET buscar = refe LET i = 1
			INPUT BY NAME refe WITHOUT DEFAULTS
				BEFORE INPUT
					CALL fgl_dialog_setkeylabel("ACCEPT","Buscar")
					CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
				AFTER input
					IF INT_FLAG THEN
						LET aborta = 1
						EXIT INPUT
					END IF
				AFTER FIELD refe
					IF refe = "Ingrese tema a buscar..." THEN
						NEXT FIELD refe
					END IF

				ON idle 1
					CALL GET_FLDBUF(refe) RETURNING refe
					IF refe IS NULL THEN
						LET refe = "ronald palencia"
					END IF
					IF buscar <> refe THEN
						LET buscar = "*"||refe||"*"
						DECLARE estos2 CURSOR FOR
						SELECT  a.link link2, a.apl_id, a.ayu_tip, a.ayu_des
     					from ande:mayuda a 
     					WHERE upper(a.ayu_des) MATCHES upper(buscar)
     					order by a.apl_id, a.ayu_des

						CALL la_temas.CLEAR()
   					LET i =1
   					FOREACH estos2 INTO la_temas[i].*
      					LET i=i+1
   					END FOREACH

						CALL f.setElementText("relacion",'Temas encontrados '||(i-1))

						LET aborta = 0  
						DISPLAY ARRAY la_temas TO sa_temas.* 
							BEFORE DISPLAY
									CALL fgl_dialog_setkeylabel("ACCEPT","Mostrar\nContenido")
									CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva\nBusqueda")
	  								CALL fgl_dialog_setkeylabel("insert", "")
	  								CALL fgl_dialog_setkeylabel("Append", "")
	  								CALL fgl_dialog_setkeylabel("Delete", "")

							BEFORE ROW
									EXIT DISPLAY
						END DISPLAY

						LET buscar = refe
					END IF

			END INPUT

			IF aborta THEN
				LET INT_FLAG = FALSE
				EXIT WHILE
			END IF

	IF i = 1 THEN
			CALL box_valdato("No encontro datos!")
			EXIT WHILE
	ELSE
		CALL la_temas.deleteElement(i)

		LET aborta = 0  
		DISPLAY ARRAY la_temas TO sa_temas.* 
			BEFORE DISPLAY
					CALL fgl_dialog_setkeylabel("ACCEPT","Mostrar\nContenido")
					CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva\nBusqueda")
	  				CALL fgl_dialog_setkeylabel("insert", "")
	  				CALL fgl_dialog_setkeylabel("Append", "")
	  				CALL fgl_dialog_setkeylabel("Delete", "")

			BEFORE ROW
				LET i = ARR_CURR()
				LET retorna = la_temas[i].link2

			AFTER DISPLAY
				IF INT_FLAG THEN
					LET aborta = 1
					EXIT DISPLAY
				END IF
		END DISPLAY

		IF aborta THEN
			LET refe = "Ingrese tema a buscar..."
			CALL la_temas.CLEAR()
			DISPLAY ARRAY la_temas TO sa_temas.*
				BEFORE DISPLAY
				EXIT DISPLAY
			END DISPLAY
			LET aborta = 0
		ELSE --NO presiono nueva busqueda
			LET aborta = 1
		END IF
	END IF

	IF aborta = 1 THEN
		EXIT WHILE
	END if

	END WHILE

   CLOSE window find_ayuda
	RETURN retorna
END FUNCTION

#########################################################################
## Parameters: vdbname = Nombre de la BD
##					vlink = al otro programa
##             vprog = Nombre del programa a correr
##             vbtn  = Nombre del boton a esconder en el programa que se corre
##
## Returnings: nothing
##
## Comments  : Corrre un programa que es llamado al presionar un boton en la pantalla principal
#########################################################################
FUNCTION run_prog(vdbname, vlink, vprog, vbtn)
	DEFINE vdbname VARCHAR(15),
			 vlink VARCHAR(15),
			 vprog varchar(50), 
			 vruta STRING, 
			 vpath varchar(250),
          vbtn varchar(50)

 SELECT prog_dirpro  into vpath 
	from ande:mprog 
	where prog_nom=vprog

 IF STATUS = NOTFOUND THEN
    CALL box_valdato("El programa : "||vprog||" No existe en la B.D.!")
    RETURN
 else
	if vpath is null then
    	CALL box_valdato("Error: El path del programa que desea correr esta en blanco!")
    	RETURN
	end if
 END IF

 LET vprog = "  ",vprog
 LET vruta = "cd "||vpath clipped||"; fglrun *.42r "|| vdbname||' '|| vbtn||' '|| vlink
 DISPLAY vruta
 RUN vruta  WITHOUT WAITING 
END FUNCTION

--version que corre con espera
FUNCTION corre_prog(vdbname, vlink, vprog, vbtn)
	DEFINE vdbname VARCHAR(15),
			 vlink VARCHAR(15),
			 vprog varchar(50), 
			 vruta STRING, 
			 vpath varchar(250),
          vbtn varchar(50)

 SELECT prog_dirpro  into vpath 
	from ande:mprog 
	where prog_nom=vprog

 IF STATUS = NOTFOUND THEN
    CALL box_valdato("El programa : "||vprog||" No existe en la B.D.!")
    RETURN
 else
	if vpath is null then
    	CALL box_valdato("Error: El path del programa que desea correr esta en blanco!")
    	RETURN
	end if
 END IF

 LET vprog = "  ",vprog
 LET vruta = "cd "||vpath clipped||"; fglrun *.42r "|| vdbname||' '|| vbtn||' '|| vlink
 DISPLAY vruta
 RUN vruta  
END FUNCTION

#########################################################################
## Parameters: d = DIALOG
##             tf1 = true/false para boton 1
##             tf2 = true/false para boton 2
##             tf3 = true/false para boton 3
##             tf4 = true/false para boton 4
##             tf5 = true/false para boton 5
##             tf6 = true/false para boton 6
##             tf7 = true/false para boton 7
##             tf8 = true/false para boton 8
##             tf9 = true/false para boton 9
##
## Returnings: nothing
##
## Comments  : Activa los botones del Lado Derecho de la pantalla principal
#########################################################################
FUNCTION active_button(d,tf1,tf2,tf3,tf4,tf5,tf6,tf7,tf8,tf9)
	DEFINE d ui.Dialog,
			 tf1,tf2,tf3,tf4,tf5,tf6,tf7,tf8,tf9 smallint

   CALL d.setActionActive("btn1",tf1)
   CALL d.setActionActive("btn2",tf2)
   CALL d.setActionActive("btn3",tf3)
   CALL d.setActionActive("btn4",tf4)
   CALL d.setActionActive("btn5",tf5)
   CALL d.setActionActive("btn6",tf6)
   CALL d.setActionActive("btn7",tf7)
   CALL d.setActionActive("btn8",tf8)
   CALL d.setActionActive("btn9",tf9)
END FUNCTION


#########################################################################
## Parameters: pprog = Nombre del programa del cual se desea obtener informacion
##
## Returnings: Nombre del programa
##
## Comments  : Retorna la descripcion del programa a ser desplegado en las pantallas
#########################################################################
FUNCTION get_nprog(pprog)
	DEFINE 
			 pprog varchar(50), 
			 vprog_des VARCHAR(200)

 SELECT prog_des  into vprog_des 
	from ande:mprog 
	where prog_nom=pprog
	AND est_id = 13

 IF STATUS = NOTFOUND THEN
    CALL box_valdato("El programa : "||pprog||" No existe en la B.D.!")
 	 LET vprog_des = "NO EXISTE PROGRAMA"
 END IF

 RETURN vprog_des
END FUNCTION
-- Verifica, si existe tabla de temporal o NO   erick alvarez 
FUNCTION existe_tabla(ctabla)
DEFINE   lexiste_tabla SMALLINT,
         ctabla VARCHAR(50,0),
         cquery VARCHAR(500,0),
         val_tempo VARCHAR(15)

   LET lexiste_tabla = FALSE

   LET cquery = "SELECT COUNT(*) FROM ", ctabla CLIPPED

   WHENEVER ERROR CONTINUE
   PREPARE qry_mestant FROM cquery
   WHENEVER ERROR STOP

   IF SQLCA.SQLCODE <> -206 THEN
      LET lexiste_tabla = TRUE
   ELSE
      -- Para que ignore el ERROR que trae del PREPARE
      WHENEVER ERROR CONTINUE
      EXECUTE qry_mestant
      WHENEVER ERROR STOP
   END IF

   RETURN lexiste_tabla
END FUNCTION
