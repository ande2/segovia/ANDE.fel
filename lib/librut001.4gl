{
Programa : librut001 
Programo : Mynor Ramirez 
Objetivo : Programa de subrutinas de libreria.
}

DATABASE segovia 

-- Subrutina para el menu de grabacion 

FUNCTION librut001_menugraba(msg,texto,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        texto, 
        msg,
        opc1,
        opc2,
        opc3,
        opc4 STRING

  MENU msg 
  ATTRIBUTE(STYLE="dialog",COMMENT=texto,IMAGE="question")
  BEFORE MENU
   IF opc1 IS NULL THEN
      HIDE OPTION opc1
   END IF
   IF opc2 IS NULL THEN
      HIDE OPTION opc2
   END IF
   IF opc3 IS NULL THEN
      HIDE OPTION opc3
   END IF
   IF opc4 IS NULL THEN
      HIDE OPTION opc4
   END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
  COMMAND opc3
   LET ope = 0
  COMMAND opc4 
   LET ope = 3 
  END MENU
  RETURN ope
END FUNCTION

-- Subrutina para el menu de confirmacion a una pregunta 

FUNCTION librut001_yesornot(title,msg,opc1,opc2,icon)
 DEFINE ope  SMALLINT,
        title,
        msg,
        opc1,
        opc2,
        icon STRING

  MENU title
  ATTRIBUTE(STYLE="dialog",COMMENT=msg,IMAGE=icon)
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 0
  END MENU
  RETURN ope
END FUNCTION

-- Subrutina para cambiar el atributo TEXT de algun objeto en un forma 

FUNCTION librut001_dpelement(ne,st)
 DEFINE st,ne STRING,
        w     ui.Window,
        f     ui.Form

 -- Obteniendo datos de la ventana
 LET w = ui.Window.getCurrent()

 -- Obteniendo datos de la forma
 LET f = w.getForm()

 -- Desplegando elemento
 CALL f.setElementText(ne,st)
END FUNCTION

-- Subrutina para un menu de opciones 

FUNCTION librut001_menuopcs(msg1,msg2,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        msg1,
        msg2,
        opc1,
        opc2,
        opc3,
        opc4 STRING

 -- Menu de opciones 
 MENU msg1 
  ATTRIBUTE(STYLE="dialog",COMMENT=msg2,IMAGE="question")
  BEFORE MENU
   IF LENGTH(opc1)=0 THEN
      HIDE OPTION opc1
   END IF
   IF LENGTH(opc2)=0 THEN
      HIDE OPTION opc2
   END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
 END MENU
 RETURN ope
END FUNCTION

-- Subrutina para los nombres de los meses del anio 

FUNCTION librut001_nombremeses(n_mes,idioma)
 DEFINE array_mes    ARRAY[12] OF CHAR(10),
        n_mes,idioma SMALLINT,
	nombre_mes   CHAR(10)

 -- Verificando idioma
 CASE (idioma)
  WHEN 0 -- Ingles
   LET array_mes[1] ="January"
   LET array_mes[2] ="February"
   LET array_mes[3] ="March"
   LET array_mes[4] ="April"
   LET array_mes[5] ="May"  
   LET array_mes[6] ="June"  
   LET array_mes[7] ="July" 
   LET array_mes[8] ="August"
   LET array_mes[9] ="September"
   LET array_mes[10]="October" 
   LET array_mes[11]="November"
   LET array_mes[12]="December" 
   LET nombre_mes   = array_mes[n_mes]
  WHEN 1 -- Espaniol
   LET array_mes[1] ="Enero"
   LET array_mes[2] ="Febrero"
   LET array_mes[3] ="Marzo"
   LET array_mes[4] ="Abril"
   LET array_mes[5] ="Mayo" 
   LET array_mes[6] ="Junio"
   LET array_mes[7] ="Julio"
   LET array_mes[8] ="Agosto"
   LET array_mes[9] ="Septiembre"
   LET array_mes[10]="Octubre"
   LET array_mes[11]="Noviembre"
   LET array_mes[12]="Diciembre"
   LET nombre_mes   = array_mes[n_mes]
 END CASE
 RETURN nombre_mes
END FUNCTION

-- Subrutina para obtener los dias del mes

FUNCTION librut001_diasmes(wmes,wanio)
 DEFINE array_dias       ARRAY[12] OF SMALLINT,
        wmes,wanio,wdias SMALLINT,
        residuo          INTEGER

 -- Verificando si es anio bisiesto
 LET residuo = (wanio mod 4)
 IF (residuo>0) THEN
    LET array_dias[2] = 28
 ELSE
    LET array_dias[2] = 29
 END IF

 -- Asigando dias del mes
 LET array_dias[1]  = 31
 LET array_dias[3]  = 31
 LET array_dias[4]  = 30
 LET array_dias[5]  = 31
 LET array_dias[6]  = 30
 LET array_dias[7]  = 31
 LET array_dias[8]  = 31
 LET array_dias[9]  = 30
 LET array_dias[10] = 31
 LET array_dias[11] = 30
 LET array_dias[12] = 31
 LET wdias = array_dias[wmes]
 RETURN wdias
END FUNCTION

-- Subrutina para formatear una fecha 

FUNCTION librut001_formatofecha(w_fecha,idioma,pais)
 DEFINE w_mname,w_dname CHAR(10),
        w_month,w_day   SMALLINT,
        idioma          SMALLINT,
        w_date          CHAR(78),
        pais            CHAR(30),
        abbr            CHAR(2),
        w_fecha         DATE

 -- Obteniendo datos de la fecha
 LET w_date  = NULL
 LET w_month = MONTH(w_fecha)
 LET w_mname = librut001_nombremeses(w_month,idioma)
 LET w_day   = WEEKDAY(w_fecha)  
 LET w_dname = librut001_nombredias(w_day,idioma)

 -- Verificando pais
 IF (LENGTH(pais)>0) THEN
    LET pais = pais CLIPPED,","
 END IF 

 -- Obteniendo fecha formateada  
 CASE (idioma)
  WHEN 0 -- Ingles
   -- Verificando numero de dia
   CASE (DAY(w_fecha))
    WHEN 1    LET abbr = "st"
    WHEN 2    LET abbr = "nd"
    WHEN 3    LET abbr = "rd"
    OTHERWISE LET abbr = "th"
   END CASE 

   -- Construyendo fecha
   LET w_date = pais CLIPPED," ",w_mname CLIPPED," ",
                DAY(w_fecha) USING "<<<<<",abbr,", ",
                YEAR(w_fecha) USING "<<<<","."

  WHEN 1 -- Espaniol
   LET w_date = pais CLIPPED," ",
                DAY(w_fecha) USING"<<<<<"," de ",
                w_mname CLIPPED," de ",
                YEAR(w_fecha) USING "<<<<","."
 END CASE
 RETURN w_date
END FUNCTION

-- Subrutina para desplegar valores

FUNCTION librut001_valores(wcodigo)
 DEFINE wcodigo CHAR(3)
 
 -- Evaluando valor
 CASE (wcodigo)
  WHEN "V" RETURN "VIGENTE" 
  WHEN "A" RETURN "ANULADO" 
 END CASE
END FUNCTION 

-- Subrutina para los nombre de los dias de la semana 

FUNCTION librut001_nombredias(w_day,idioma)
 DEFINE w_arrayw ARRAY[7] OF CHAR(10),
	w_dname  CHAR(9),
	w_day    SMALLINT,
	idioma   SMALLINT        

 -- Verificando dias de la semana
 IF (w_day=0) THEN
    LET w_day = 7
 END IF

 -- Verificando idioma   
 CASE (idioma)
  WHEN 0 -- Ingles
   LET w_arrayw[1] = "Monday" 
   LET w_arrayw[2] = "Tuesday"
   LET w_arrayw[3] = "Wednesday"
   LET w_arrayw[4] = "Thrusday"
   LET w_arrayw[5] = "Friday"
   LET w_arrayw[6] = "Saturday"
   LET w_arrayw[7] = "Sunday"
   LET w_dname     = w_arrayw[w_day]   
  WHEN 1 -- Espaniol
   LET w_arrayw[1] = "Lunes"  
   LET w_arrayw[2] = "Martes"
   LET w_arrayw[3] = "Miercoles"
   LET w_arrayw[4] = "Jueves"
   LET w_arrayw[5] = "Viernes"
   LET w_arrayw[6] = "Sabado"
   LET w_arrayw[7] = "Domingo"
   LET w_dname     = w_arrayw[w_day]   
 END CASE

 RETURN w_dname
END FUNCTION

-- Subrutina para desplegar datos del encabezado de un programa en pantala

FUNCTION librut001_header(wcodpro,wpais,wlang) 
 DEFINE wcodpro LIKE glb_programs.codpro,
        wdcrpro LIKE glb_programs.dcrpro,
        wlang   SMALLINT, 
        wfecha  STRING,
        wpais   STRING 

 -- Seleccionando nombre del programa
 SELECT NVL(a.dcrpro,"NO DEFINIDO")
  INTO  wdcrpro
  FROM  glb_programs a
  WHERE (a.codpro = wcodpro)

 -- Desplegando titulo del programa
 CALL fgl_settitle(wdcrpro)

 -- Deslegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,wlang,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)

 -- Despelgando usuario
 MESSAGE " Usuario: "||FGL_GETENV("LOGNAME") CLIPPED ATTRIBUTE(RED) 
END FUNCTION 

-- Subrutioa para obtener parametros del sistema 

FUNCTION librut001_parametros(wnumpar,wtippar)
 DEFINE wtippar LIKE glb_paramtrs.tippar,
        wnumpar LIKE glb_paramtrs.numpar, 
        wvalchr LIKE glb_paramtrs.valchr

 -- Seleccionando parametros
 INITIALIZE wvalchr TO NULL 
 SELECT a.valchr
  INTO  wvalchr
  FROM  glb_paramtrs a
  WHERE (a.numpar = wnumpar)
    AND (a.tippar = wtippar)
  IF (status!=NOTFOUND) THEN
     RETURN TRUE,wvalchr
  ELSE
     RETURN FALSE,wvalchr
  END IF
END FUNCTION 

-- Subrutina para configurar los tipos de letras de acuerdo la impresora

FUNCTION librut001_fontsprn(pipeline,impresora)
 DEFINE fnt     RECORD
        cmp,nrm   CHAR(12),
        tbl,fbl   CHAR(12),
        t88,t66   CHAR(12),
        p12,p10   CHAR(12),
        srp       CHAR(12),
        twd,fwd   CHAR(12),
        tda,fda   CHAR(12),
        ini       CHAR(12)
       END RECORD,
       impresora   STRING,
       pipeline    STRING 

 -- Definiendo tipos de fonts
 INITIALIZE fnt.* TO NULL

 -- Verificando si salida es pantalla, pdf o corre electronico
 IF (pipeline="screen") OR
    (pipeline="pdf") OR
    (pipeline MATCHES "*@*") THEN
    RETURN fnt.*
 END IF

 -- Verificando tipos de impresora
 CASE (impresora)
  WHEN "epson"
   -- Salida a impresora EPSON ESC/2
   LET fnt.cmp = ASCII 15 --condensado
   LET fnt.nrm = ASCII 18 --normal
   LET fnt.tbl = ASCII 27,ASCII 71 --Habilita negrita
   LET fnt.fbl = ASCII 27,ASCII 72 --Desabilita negrita
   LET fnt.twd = ASCII 27,ASCII 87,"1" --Habilita doble ancho
   LET fnt.fwd = ASCII 27,ASCII 87,"0" --deshabilita doble ancho
   LET fnt.t88 = ASCII 27,ASCII 48 --1/8 en altura
   LET fnt.t66 = ASCII 27,ASCII 50 --Regresa a 1/6  de altura(default)
   LET fnt.p12 = ASCII 27,ASCII 77 --pica condensada
   LET fnt.p10 = ASCII 27,ASCII 80 --desabilita pica
   LET fnt.tda = ASCII 27,ASCII 119,"1" --habilita doble alto
   LET fnt.fda = ASCII 27,ASCII 119,"0" --desabilita doble alto
   LET fnt.ini = ASCII 27,"@" --inicializa impresora y toma el default
   LET fnt.srp = ASCII 27,ASCII 56 --desabilita sensor de papel
  OTHERWISE
   -- Salida a impresora EPSON ESC/2
   LET fnt.cmp = ASCII 15
   LET fnt.nrm = ASCII 18
   LET fnt.tbl = ASCII 27,ASCII 71
   LET fnt.fbl = ASCII 27,ASCII 72
   LET fnt.twd = ASCII 27,ASCII 87,"1"
   LET fnt.fwd = ASCII 27,ASCII 87,"0"
   LET fnt.t88 = ASCII 27,ASCII 48
   LET fnt.t66 = ASCII 27,ASCII 50
   LET fnt.p12 = ASCII 27,ASCII 77
   LET fnt.p10 = ASCII 27,ASCII 80
   LET fnt.tda = ASCII 27,ASCII 119,"1"
   LET fnt.fda = ASCII 27,ASCII 119,"0"
   LET fnt.ini = ASCII 27,"@"
   LET fnt.srp = ASCII 27,ASCII 56
 END CASE

 RETURN fnt.*
END FUNCTION

-- Subrutina para enviar un reporte a un destino 

FUNCTION librut001_enviareporte(filename,pipeline,title)
 DEFINE wcommand,pipeline,filename,title,wdocto STRING

 -- Tipos de dispositivo (pipeline)
 -- local  = impresora conectada a puerto local donde se ejecuta el aplicativo
 -- screen = pantalla
 -- *@*    = buzon de correo

 -- Seleccionando dispositivo
 CASE
  WHEN (pipeline="screen")
   CALL librut001_visor(filename,title)
  WHEN (pipeline="local")
   LET wcommand = "../../cmd/prtty ",filename CLIPPED
 END CASE

 -- Ejecutando commando
 DISPLAY "cmd ", wcommand 
 RUN wcommand
END FUNCTION

-- Subrutina para enviar un arhivo a un visor de pantalla 

FUNCTION librut001_visor(fn,title)
 DEFINE visordata,fn,title STRING

 -- Asignando titulo
 LET title = " Visor de Reportes - ",title CLIPPED

 -- Llenando visor de datos
 LET visordata = librut001_readfile(fn)

 -- Abriendo la ventana del visor
 OPEN WINDOW visor WITH FORM "formvisor"
  ATTRIBUTE(TEXT=title)

  -- Desplegando datos en el visor
  DISPLAY BY NAME visordata
  MENU ""
   ON ACTION regresar
    EXIT MENU

	ON ACTION imprimir
		CALL librut001_enviareporte(fn,"local",title)
		EXIT MENU
  END MENU

 -- Cerrando ventana del visor
 CLOSE WINDOW visor
END FUNCTION

-- Subrutina para leer un archivo 

FUNCTION librut001_readfile(fn)
 DEFINE fn  STRING
 DEFINE txt STRING
 DEFINE ln  STRING
 DEFINE ch  base.Channel

 LET ch=base.Channel.create()
 CALL ch.openfile(fn,"r")
 CALL ch.setDelimiter("")
 WHILE ch.read(ln)
  IF txt IS NULL THEN
     IF ln IS NULL THEN
        LET txt = "\n"
     ELSE
        LET txt = ln
     END IF
  ELSE
     IF ln IS NULL THEN
        LET txt = txt || "\n"
     ELSE
        LET txt = txt || "\n" || ln
     END IF
  END IF
 END WHILE
 CALL ch.close()
 RETURN txt
END FUNCTION

-- Subrutina para calcular el area

FUNCTION librut001_area(xancho,ylargo)
 DEFINE xancho,ylargo,area  DEC(8,2)

 -- Calculando
 LET area = (xancho*ylargo)
 IF area IS NULL THEN LET area = 0 END IF 
 RETURN area
END FUNCTION

-- Subrutina para convertir los numeros a letras 

FUNCTION librut001_numtolet(valor)
DEFINE f_tentxt ARRAY[8] OF RECORD
         tentxt         CHAR(10)
       END RECORD,
       f_unittxt ARRAY[20] OF RECORD
         unittxt        CHAR(11)
       END RECORD,
       f_gruptxt ARRAY[3] OF RECORD
         gruptxt        CHAR(7)
       END RECORD,
       f_letras,letras  CHAR(100),
       i,j,esp SMALLINT,
       f_cox            CHAR(20),
       f_deci           DECIMAL(3,0),
       valor            DECIMAL(11,2),
       f_numero, f_stp, f_divisor, f_grup, f_unit, f_ten, f_kk  INTEGER,
       f_deci2          CHAR(1)
LET f_tentxt[1].tentxt = "VEINTI"
LET f_tentxt[2].tentxt = "TREINTA "
LET f_tentxt[3].tentxt = "CUARENTA "
LET f_tentxt[4].tentxt = "CINCUENTA "
LET f_tentxt[5].tentxt = "SESENTA "
LET f_tentxt[6].tentxt = "SETENTA "
LET f_tentxt[7].tentxt = "OCHENTA "
LET f_tentxt[8].tentxt = "NOVENTA "

LET f_unittxt[1].unittxt = "UN "
LET f_unittxt[2].unittxt = "DOS "
LET f_unittxt[3].unittxt = "TRES "
LET f_unittxt[4].unittxt = "CUATRO "
LET f_unittxt[5].unittxt = "CINCO "
LET f_unittxt[6].unittxt = "SEIS "
LET f_unittxt[7].unittxt = "SIETE "
LET f_unittxt[8].unittxt = "OCHO "
LET f_unittxt[9].unittxt = "NUEVE "
LET f_unittxt[10].unittxt = "DIEZ "
LET f_unittxt[11].unittxt = "ONCE "
LET f_unittxt[12].unittxt = "DOCE "
LET f_unittxt[13].unittxt = "TRECE "
LET f_unittxt[14].unittxt = "CATORCE "
LET f_unittxt[15].unittxt = "QUINCE "
LET f_unittxt[16].unittxt = "DIECISEIS "
LET f_unittxt[17].unittxt = "DIECISIETE "
LET f_unittxt[18].unittxt = "DIECIOCHO "
LET f_unittxt[19].unittxt = "DIECINUEVE "
LET f_unittxt[20].unittxt = "VEINTE "

LET f_gruptxt[1].gruptxt = "MILLON "
LET f_gruptxt[2].gruptxt = "MIL "
LET f_gruptxt[3].gruptxt = " "


LET f_numero = valor
LET f_letras = NULL
LET f_stp = 1
LET f_divisor = 1000000
LET f_grup = 0
LET f_unit = 0
LET f_ten  = 0

WHILE f_stp <= 3

        LET f_grup = (f_numero / f_divisor)
        LET f_grup = f_grup * f_divisor
        LET f_numero = f_numero - f_grup
        LET f_grup = f_grup / f_divisor

        IF f_grup > 0
           THEN LET f_kk = f_grup
                LET f_ten = 0
           IF f_grup > 99
              THEN LET f_unit = (f_grup / 100)
                   LET f_grup = f_grup - (f_unit * 100)
                   LET f_cox = f_unittxt[f_unit].unittxt CLIPPED,"CIENTOS "
                   IF f_unit = 1
                      THEN IF f_grup = 0
                              THEN LET f_cox = "CIEN "
                              ELSE LET f_cox = "CIENTO "
                           END IF
                      END IF
                   IF f_unit = 5
                      THEN LET f_letras = f_letras CLIPPED," ","QUINIENTOS "
                      ELSE IF f_unit = 7
                              THEN 
                           LET f_letras = f_letras CLIPPED," ","SETECIENTOS "
                              ELSE IF f_unit = 9
                                      THEN
                           LET f_letras = f_letras CLIPPED," ","NOVECIENTOS "
                                      ELSE 
                           LET f_letras = f_letras CLIPPED," ",f_cox
                                   END IF
                           END IF
                   END IF
           END IF
        IF f_grup > 30
           THEN LET f_ten = (f_grup / 10)
                LET f_grup = f_grup - (f_ten * 10)
                LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                IF ((f_grup > 0) AND (f_letras IS NOT NULL))
                   THEN LET f_letras = f_letras CLIPPED," ","Y "
                END IF
           ELSE IF f_grup > 20
                 THEN LET f_ten = (f_grup / 10)
                      LET f_grup = f_grup - (f_ten * 10)
                      LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                END IF
         END IF
        IF f_grup > 0
           THEN IF f_ten = 2
           THEN LET f_letras = f_letras CLIPPED,f_unittxt[f_grup].unittxt
           ELSE LET f_letras = f_letras CLIPPED," ",f_unittxt[f_grup].unittxt
                END IF
        END IF
        LET f_letras = f_letras CLIPPED," ",f_gruptxt[f_stp].gruptxt
        IF ((f_stp = 1) AND (f_grup > 1))
           THEN LET f_letras = f_letras CLIPPED,"ES "
        END IF
        END IF

        LET f_stp = f_stp + 1
        LET f_divisor = f_divisor /1000

END WHILE
    LET f_numero = valor
    LET f_deci = (valor - f_numero)*100
    IF valor >= 2
       THEN IF f_deci = 0
             THEN LET f_cox = " EXACTOS"
             ELSE LET f_cox = " CON ",f_deci,"/100"
            END IF
            IF f_deci > 0 AND f_deci < 9
              THEN LET f_deci2 = f_deci
                   LET f_cox = " CON 0",f_deci2,"/100" 
            END IF 

            LET f_letras = "*",f_letras CLIPPED," ",f_cox CLIPPED," *"
       ELSE IF valor < 1
             THEN IF f_deci > 1
                   THEN LET f_letras = "* ",f_deci," CENTAVOS *"
                   ELSE LET f_letras = "* ",f_deci," CENTAVO *"
                  END IF
            ELSE IF f_deci = 0
                  THEN LET f_cox = " EXACTO"
                  ELSE LET f_cox = " CON ",f_deci,"/100"
                  END IF

                  IF f_deci > 0 AND f_deci < 9
                     THEN LET f_deci2 = f_deci
                          LET f_cox = " CON 0",f_deci2,"/100" 
                  END IF 

                  LET f_letras="* ",f_letras CLIPPED," ",f_cox CLIPPED," *"

            END IF
    END IF

    LET esp  = 0
    LET j    = 1
    FOR i = 1 TO LENGTH(f_letras)
     IF (f_letras[i,i] = " ") THEN
        LET esp = (esp+1)
     ELSE
        LET esp = 0 
     END IF

     IF (esp<=1) THEN
        LET letras[j,j] = f_letras[i,i] 
        LET j = (j+1)
     END IF
    END FOR
    RETURN letras  
END FUNCTION

-- Subrutina para buscar acceso a opciones por medio del rol del usuario

FUNCTION seclib001_accesos(wuserid,wopcrid)
 DEFINE wuserid LIKE glb_userrole.userid,
        wopcrid LIKE glb_opcsrole.opcrid,
        cnt     SMALLINT 

 -- Seleccionando rol del usuario
{ SELECT COUNT(*) 
  INTO  cnt
  FROM  glb_userrole a,glb_opcsrole b
  WHERE a.roleid = b.roleid 
    AND a.userid = wuserid
    AND a.activo = 1 
    AND b.opcrid = wopcrid 
  IF (cnt>0) THEN
     RETURN TRUE 
  ELSE
     RETURN FALSE
  END IF 
}

 RETURN TRUE -- temporal 
END FUNCTION 



FUNCTION librut001_rep_pdf(lTitulo,lFile,fntSize,lPosition,lMargin)
   DEFINE
      lTitulo                    , -- Titulo a colocar en el reporte PDF
		lFile                      , -- Path del archivo TXT generado por el sistema
		lCmd                       , -- Variable en donde se concatena el comando para conversion de TXT a PDF
		lLocalFile                 , -- Variable que tiene el path de pdf local
		lRemoteFile    STRING      , -- Variable que tiene el path de pdf remoto
		lDelete        STRING      , -- Variable del comando para elimnar PDF en windows
      lLogoEmp       VARCHAR(100), -- Logo de la empresa a poner en PDF
		fntSize        INT         , -- Tamaño de fuente para el PDF
		lMargin        INT         , -- Variable que define si el margen es menor (mas amplio) con valor 2 o con valor 1 NORMAL
		lValMargin     VARCHAR(100), -- Variable q concatena para que los margenes cambien
      lPosition      CHAR(1)     , -- Variable que recibe si la pagina es HORIZONTAL O VERTICAL (LANDSCAPE - PORTRAIT)
      lPage          VARCHAR(100), -- Variable q concatena para que la pagina sea HORIZONTAL (LANDSCAPE)
      res                        , -- Respuesta de WINSHELLEXEC
		ii					STRING      , -- Respuesta 
		i              INT           -- Variable que barre el path del archivo txt para obtener solo el nombre

   -- El menu envia la base de datos que se traduce en empresa
	-- Se Selecciona el logo por empresa
   SELECT pathLogo
   INTO   lLogoEmp
   FROM   glb_empresas
   WHERE  codEmp = 1

	-- Se encuentra solo el nombre del archivo sin todo el path que se envia
   FOR i = lFile.getLength() to 1 STEP -1
      IF lFile.subString(i,i) = "/" THEN
         LET i = i+1
         EXIT FOR
      END IF
   END FOR

	CASE lPosition -- Posicion de la pagina
		WHEN "xx"
		WHEN "L" -- Landscape / Horizontal
			LET lPage=" --page-width=842 --page-height=595 "
		WHEN "P" -- Portrait / Vertical
			LET lPage=NULL
		OTHERWISE  -- Portrait / Vertical
			LET lPage=NULL 
	END CASE

	CASE lMargin -- Se elige el tama¤o de los margenes laterales
      WHEN 5
		WHEN 4
			LET lValMargin=" --left-margin=10 --right-margin=10 --top-margin=20 --bottom-margin=20 "
		WHEN 3
			LET lValMargin=" --left-margin=5 --right-margin=5 --top-margin=20 --bottom-margin=20 "
      WHEN 2
			LET lValMargin=" --left-margin=40 --right-margin=40 --top-margin=20 --bottom-margin=20 "
		WHEN 1
			LET lValMargin=NULL -- Margen Normal 48 x 48
	END CASE
   --LET lLocalFile=lFile.subString(1,lFile.getLength()-3)CLIPPED||"pdf"

	-- Se define el nombre del archivo PDF destino
   LET lLocalFile=lFile.subString(1,LENGTH(lFile)-4)CLIPPED||".pdf"

	-- Se define el path donde quedara el file PDF en windows
   LET lRemoteFile="C:\\temp\\", lFile.subString(i,LENGTH(lFile)-4),".pdf"

	-- Comando elimina todos los archivos pdf de windos para que no genere basura
	LET lDelete='cmd /C \"del c:\\temp\\*.pdf "'

	-- Corre comando para elimnar pdf en windows
	CALL ui.Interface.frontCall("standard","shellexec",[lDelete],[ii])

	-- Arma comando para convertir a pdf el reporte TXT generado por Genero
   LET lCmd="a2pdf --icon=", lLogoEmp CLIPPED," ",lPage," ",lValMargin,
       " --font-size="||fntSize||"  --icon-scale=0.15 --noline-numbers --title='",
       lTitulo CLIPPED, "' ", lFile CLIPPED ," -o ", lLocalFile

   RUN lCmd
   WHENEVER ERROR CONTINUE
   	CALL fgl_putfile(lLocalFile,lRemoteFile)
	WHENEVER ERROR STOP
   LET res = WINSHELLEXEC(lRemoteFile)
	IF res = 0 THEN
		DISPLAY "STATUS ",STATUS
		CALL box_valdato("No se pudo abrir el file "||lFile.subString(i,LENGTH(lFile)-4)CLIPPED||".pdf Revise")
	ELSE
		CALL box_valdato("El archivo "||lFile.subString(i,LENGTH(lFile)-4)CLIPPED||".pdf fue abierto exitosamente.")
	END IF
END FUNCTION

FUNCTION box_valdato(mensaje)
DEFINE
   mensaje VARCHAR(255),
   flag_return SMALLINT

   MENU "Advertencia"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="exclamation")
      COMMAND "Aceptar"
         LET flag_return = FALSE
         EXIT MENU
   END MENU
END FUNCTION

FUNCTION box_pregunta(mensaje)
DEFINE
   mensaje VARCHAR(255),
	respuesta SMALLINT

   MENU "Confirmacion del Sistema"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="question")
      COMMAND "Si"
         LET respuesta =1 
         EXIT MENU
      COMMAND "No"
         LET respuesta =2
         EXIT MENU
   END MENU
   RETURN respuesta
END FUNCTION
