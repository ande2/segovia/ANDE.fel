 
FUNCTION ParametrosReporte ( )
DEFINE
	accion INTEGER,
   salida STRING,
   preview INTEGER

   --CLOSE WINDOW SCREEN
   OPEN WINDOW forma WITH FORM "repgrw"
   LET preview =1
   LET salida = "SVG"
   INPUT BY NAME preview, salida ATTRIBUTES(UNBUFFERED,WITHOUT DEFAULTS)


	BEFORE INPUT
		CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
		CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
		LET accion = FALSE

      ON ACTION ACCEPT
         --CALL FGL_DIALOG_GETBUFFER ( ) RETURNING salida
         LET accion = GET_FLDBUF(preview)
         LET salida = GET_FLDBUF(salida)
			LET accion = TRUE
         LET int_flag=FALSE
         EXIT INPUT

			{
		ON ACTION INTERRUPT
			LET accion = FALSE
         LET int_flag=TRUE
         EXIT INPUT
			}

      AFTER FIELD salida
         CALL FGL_DIALOG_GETBUFFER ( ) RETURNING salida

      AFTER INPUT
         IF NOT INT_FLAG THEN
            IF salida IS NULL THEN
               ERROR "Debe ingresar la salida del reporte"
               NEXT FIELD salida
            END IF
            
            IF preview IS NULL THEN
               ERROR "Debe ingresar la opcion de salida del reporte"
               NEXT FIELD preview
            END IF
         END IF
         
      END INPUT
   CLOSE WINDOW forma


   RETURN salida, preview, accion
END FUNCTION