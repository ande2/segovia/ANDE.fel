CREATE TABLE fac_mnotcre
(
	linknc                serial   NOT NULL,
	serdoc                varchar(5)   NOT NULL,
	numdoc                integer   NOT NULL,
	codcli                integer   NOT NULL,
	fecdoc                date   NOT NULL,
	numnit                varchar(20)   NOT NULL,
	nomcli                varchar(100)   NOT NULL,
	dircli                varchar(150)   NOT NULL,
	telcli                varchar(9)  ,
	numpos                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	obsnotcre             varchar(200)  ,
	exento                smallint   NOT NULL,
	moneda                smallint   NOT NULL,
	tascam                decimal(14,6)   NOT NULL,
	subtotal              decimal(14,2)   NOT NULL,
	totdoc                decimal(14,2)   NOT NULL,
	totiva                decimal(14,2)   NOT NULL,
	poriva                decimal(14,2)   NOT NULL,
	estado                char(1)   NOT NULL,
	usrope                varchar(15)   NOT NULL,
	usrsis                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                datetime hour to second   NOT NULL,
	motanl                varchar(100)  ,
	usranl                varchar(15)  ,
	fecanl                date  ,
	horanl                datetime hour to second  ,
	maicli                varchar(50)  ,
	exportacion           smallint   NOT NULL,
	tdocdol               decimal(14,2)  ,
	lnktra                integer   NOT NULL,
	tipodoc               char(2)  
);



ALTER TABLE fac_mnotcre
	ADD CONSTRAINT  PRIMARY KEY (linknc) CONSTRAINT XPKencabezado_nota_de_credito ;






CREATE TABLE fac_dnotcre
(
	linknc                integer   NOT NULL,
	correl                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                varchar(20)   NOT NULL,
	unimed                smallint   NOT NULL,
	cantid                decimal(14,2)   NOT NULL,
	preuni                decimal(12,5)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	factor                decimal(14,6)   NOT NULL,
	deitem                lvarchar(5000)  ,
	predol                decimal(12,5)  ,
	prtdol                decimal(14,2)  
);



ALTER TABLE fac_dnotcre
	ADD CONSTRAINT  PRIMARY KEY (linknc,correl) CONSTRAINT XPKdetalle_de_la_nota_de_credito ;






