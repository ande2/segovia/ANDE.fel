
CREATE TABLE adm_usu
(
	usu_id                serial   NOT NULL,
	empr_id               smallint   NOT NULL,
	usu_nomunix           char(10)   NOT NULL,
	usu_nom               char(30)   NOT NULL,
	dpe_id                integer   NOT NULL,
	ptb_id                integer   NOT NULL,
	usu_ext               char(4)   NOT NULL,
	usu_jef               smallint   NOT NULL,
	usu_passw             char(10)  ,
	usu_nomwin            varchar(30)  ,
	gru_id                integer  ,
	est_id                integer  ,
	usu_tip               smallint  ,
	usu_jefdep            integer  ,
	usu_gruliq            smallint  
);



ALTER TABLE adm_usu
	ADD CONSTRAINT  PRIMARY KEY (usu_id) CONSTRAINT adm_usu_pk ;



CREATE TABLE bbit
(
	mov_id                serial   NOT NULL,
	cam_id                integer  ,
	mov_fecha             DATETIME YEAR TO SECOND  ,
	usu_id                varchar(32)  ,
	cam_exp               varchar(100)  ,
	cam_vanterior         varchar(100)  ,
	cam_vactual           varchar(100)  ,
	cam_llaveval          varchar(100)   NOT NULL
);



ALTER TABLE bbit
	ADD CONSTRAINT  PRIMARY KEY (mov_id) CONSTRAINT bbit_pk ;



CREATE INDEX bbit_camid_idx ON bbit
(
	cam_id                ASC
);



CREATE TABLE bbitp
(
	mov_id                serial   NOT NULL,
	proc_id               integer  ,
	mov_fecha             date   DEFAULT today,
	cam_exp               varchar(20)  ,
	cam_vanterior         integer   DEFAULT 13,
	cam_vactual           integer   DEFAULT 13,
	usu_id                varchar(32)  ,
	
		 CHECK (cam_vactual IN (13 ,14 ))
		 CONSTRAINT  informix.c265_876,
		 CHECK (cam_vanterior IN (13 ,14 ))
		 CONSTRAINT  informix.c265_875
);



ALTER TABLE bbitp
	ADD CONSTRAINT  PRIMARY KEY (mov_id) CONSTRAINT bbitp_pk ;



CREATE TABLE bcam
(
	cam_id                serial   NOT NULL,
	tab_id                integer   NOT NULL,
	cam_nom               varchar(50)   NOT NULL,
	cam_des               varchar(100)   NOT NULL,
	cam_llave             smallint   DEFAULT 0,
	cam_identifica        smallint   DEFAULT 0,
	cam_bitacora          smallint   DEFAULT 0,
	
		 CHECK (cam_llave IN (0 ,1 ))
		 CONSTRAINT  informix.c243_798,
		 CHECK (cam_bitacora IN (0 ,1 ))
		 CONSTRAINT  informix.c243_799
);



ALTER TABLE bcam
	ADD CONSTRAINT  PRIMARY KEY (cam_id) CONSTRAINT bcam_pk ;



CREATE TABLE bproc
(
	proc_id               integer   NOT NULL,
	proc_desc             varchar(30)   NOT NULL,
	est_id                integer   DEFAULT 13,
	
		 CHECK (est_id IN (13 ,14 ))
		 CONSTRAINT  informix.c244_872
);



ALTER TABLE bproc
	ADD CONSTRAINT  PRIMARY KEY (proc_id) CONSTRAINT bproc_pk ;



CREATE TABLE btab
(
	tab_id                integer   NOT NULL,
	tab_fid               integer  ,
	tab_nom               varchar(40)  ,
	tab_des               varchar(200)  ,
	proc_id               integer  ,
	tab_habilitado        smallint   DEFAULT 1
);



ALTER TABLE btab
	ADD CONSTRAINT  PRIMARY KEY (tab_id) CONSTRAINT btab_pk ;



CREATE TABLE dia_proenbod
(
	fecha                 date   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	exican                decimal(14,2)   NOT NULL
);



ALTER TABLE dia_proenbod
	ADD CONSTRAINT  PRIMARY KEY (fecha,codemp,codsuc,codbod,cditem) CONSTRAINT XPKdia_proenbod ;



CREATE TABLE fac_clientes
(
	codcli                serial   NOT NULL,
	nomcli                char(60)   NOT NULL,
	numnit                char(30)   NOT NULL,
	numtel                char(30)  ,
	numfax                char(15)  ,
	dircli                char(100)   NOT NULL,
	nomcon                char(60)  ,
	bemail                char(100)  ,
	hayfac                smallint   NOT NULL,
	haydat                smallint   NOT NULL,
	moncre                decimal(14,2)   NOT NULL,
	diacre                smallint   NOT NULL,
	salcre                decimal(14,2)   NOT NULL,
	status                smallint   NOT NULL,
	estado                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (estado IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacclientes1,
		 CHECK (status IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacclientes2,
		 CHECK (hayfac IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacclientes3,
		 CHECK (moncre >= 0.00)
		 CONSTRAINT  sistemas.ckfacclientes4,
		 CHECK (diacre >= 0)
		 CONSTRAINT  sistemas.ckfacclientes5
);



ALTER TABLE fac_clientes
	ADD CONSTRAINT  PRIMARY KEY (codcli) CONSTRAINT pkfacclientes ;



CREATE TABLE fac_dnotcre
(
	linknc                integer   NOT NULL,
	correl                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                varchar(20)   NOT NULL,
	unimed                smallint   NOT NULL,
	cantid                decimal(14,2)   NOT NULL,
	preuni                decimal(12,5)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	factor                decimal(14,6)   NOT NULL,
	deitem                lvarchar(5000)  ,
	predol                decimal(12,5)  ,
	prtdol                decimal(14,2)  
);



ALTER TABLE fac_dnotcre
	ADD CONSTRAINT  PRIMARY KEY (linknc,correl) CONSTRAINT XPKdetalle_de_la_nota_de_credito ;



CREATE TABLE fac_dtransac
(
	lnktra                integer   NOT NULL,
	correl                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                char(20)   NOT NULL,
	unimed                smallint   NOT NULL,
	canori                decimal(14,2)   NOT NULL,
	unimto                smallint   NOT NULL,
	cantid                decimal(14,2)   NOT NULL,
	preuni                decimal(12,5)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	factor                decimal(14,6)   NOT NULL,
	deitem                lvarchar(5000)  ,
	predol                decimal(12,5)  ,
	prtdol                decimal(14,2)  ,
	
		 CHECK (correl > 0)
		 CONSTRAINT  sistemas.ckfacdtransac1,
		 CHECK (totpro >= 0.)
		 CONSTRAINT  sistemas.ckfacdtransac4
);



CREATE TABLE fac_emitacre
(
	codemi                smallint   NOT NULL,
	nomemi                varchar(40)   NOT NULL,
	nomabr                char(12)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE fac_emitacre
	ADD CONSTRAINT  PRIMARY KEY (codemi) CONSTRAINT pkfacemitacre ;



CREATE TABLE fac_impresol
(
	lnkres                serial   NOT NULL,
	codemp                smallint   NOT NULL,
	tipdoc                smallint   NOT NULL,
	nserie                char(10)   NOT NULL,
	nresol                char(20)   NOT NULL,
	fecres                date   NOT NULL,
	numini                integer   NOT NULL,
	numfin                integer   NOT NULL,
	estado                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (estado IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacivaresol1,
		 CHECK (fecres >= DATE ('01/01/2009' ))
		 CONSTRAINT  sistemas.ckfacivaresol2,
		 CHECK (numini > 0)
		 CONSTRAINT  sistemas.ckfacivaresol3,
		 CHECK (numfin > 0)
		 CONSTRAINT  sistemas.ckfacivaresol4
);



ALTER TABLE fac_impresol
	ADD CONSTRAINT  PRIMARY KEY (lnkres) CONSTRAINT pkfacivaresol ;



CREATE UNIQUE INDEX sistemas.uqfacivaresol ON fac_impresol
(
	codemp                ASC,
	tipdoc                ASC,
	nserie                ASC,
	nresol                ASC
);



CREATE TABLE fac_mnotcre
(
	linknc                serial   NOT NULL,
	serdoc                varchar(5)   NOT NULL,
	numdoc                integer   NOT NULL,
	codcli                integer   NOT NULL,
	fecdoc                date   NOT NULL,
	numnit                varchar(20)   NOT NULL,
	nomcli                varchar(100)   NOT NULL,
	dircli                varchar(150)   NOT NULL,
	telcli                varchar(9)  ,
	numpos                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	obsnotcre             varchar(200)  ,
	exento                smallint   NOT NULL,
	moneda                smallint   NOT NULL,
	tascam                decimal(14,6)   NOT NULL,
	subtotal              decimal(14,2)   NOT NULL,
	totdoc                decimal(14,2)   NOT NULL,
	totiva                decimal(14,2)   NOT NULL,
	poriva                decimal(14,2)   NOT NULL,
	estado                char(1)   NOT NULL,
	usrope                varchar(15)   NOT NULL,
	usrsis                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                datetime hour to second   NOT NULL,
	motanl                varchar(100)  ,
	usranl                varchar(15)  ,
	fecanl                date  ,
	horanl                datetime hour to second  ,
	maicli                varchar(50)  ,
	exportacion           smallint   NOT NULL,
	tdocdol               decimal(14,2)  ,
	lnktra                integer   NOT NULL,
	tipodoc               char(2)  
);



ALTER TABLE fac_mnotcre
	ADD CONSTRAINT  PRIMARY KEY (linknc) CONSTRAINT XPKencabezado_nota_de_credito ;



CREATE TABLE fac_mtransac
(
	lnktra                serial   NOT NULL,
	numpos                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	lnktdc                integer   NOT NULL,
	tipdoc                smallint   NOT NULL,
	nserie                varchar(5)   NOT NULL,
	numdoc                integer   NOT NULL,
	fecemi                date   NOT NULL,
	codcli                integer   NOT NULL,
	numnit                varchar(20)   NOT NULL,
	nomcli                varchar(100)   NOT NULL,
	dircli                varchar(150)   NOT NULL,
	telcli                char(9)  ,
	credit                smallint   NOT NULL,
	hayord                smallint   NOT NULL,
	ordcmp                char(20)  ,
	obspre                varchar(200)  ,
	exenta                smallint   NOT NULL,
	moneda                smallint   NOT NULL,
	tascam                decimal(14,6)   NOT NULL,
	efecti                decimal(14,2)   NOT NULL,
	cheque                decimal(14,2)   NOT NULL,
	tarcre                decimal(14,2)   NOT NULL,
	ordcom                decimal(14,2)   NOT NULL,
	depmon                decimal(14,2)   NOT NULL,
	subtot                decimal(14,2)   NOT NULL,
	totdoc                decimal(14,2)   NOT NULL,
	totiva                decimal(14,2)   NOT NULL,
	poriva                decimal(5,2)   NOT NULL,
	totpag                decimal(14,2)   NOT NULL,
	saldis                decimal(14,2)   NOT NULL,
	estado                char(1)   NOT NULL,
	feccor                date  ,
	usrope                varchar(15)   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	motanl                varchar(100,1)  ,
	usranl                varchar(15)  ,
	fecanl                date  ,
	horanl                DATETIME HOUR TO SECOND  ,
	lnkinv                integer   NOT NULL,
	maicli                varchar(50,1)  ,
	tipfac                char(1)  ,
	tdodol                decimal(14,2)  ,
	incote                char(3)  ,
	nomcom                varchar(100)  ,
	dircom                varchar(100)  ,
	otrref                varchar(32)  ,
	paicom                varchar(15)  ,
	
		 CHECK (estado IN ('V' ,'A' ))
		 CONSTRAINT  sistemas.ckfacmtransac1,
		 CHECK (numdoc > 0)
		 CONSTRAINT  sistemas.ckfacmtransac2,
		 CHECK (fecemi > DATE ('01/01/2011' ))
		 CONSTRAINT  sistemas.ckfacmtransac3,
		 CHECK (efecti >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac4,
		 CHECK (cheque >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac5,
		 CHECK (tarcre >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac6,
		 CHECK (subtot >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac7,
		 CHECK (totdoc >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac8,
		 CHECK (poriva >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac10,
		 CHECK (totiva >= 0.)
		 CONSTRAINT  sistemas.ckfacmtransac11
);



ALTER TABLE fac_mtransac
	ADD CONSTRAINT  PRIMARY KEY (lnktra) CONSTRAINT pkfacmtransac ;



CREATE TABLE fac_puntovta
(
	numpos                smallint   NOT NULL,
	nompos                varchar(40)   NOT NULL,
	chkinv                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	chkexi                smallint   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (chkexi IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacpuntovta1,
		 CHECK (chkinv IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckfacpuntovta2
);



ALTER TABLE fac_puntovta
	ADD CONSTRAINT  PRIMARY KEY (numpos) CONSTRAINT pkfacpuntovta ;



CREATE TABLE fac_tarjetas
(
	lnktra                integer   NOT NULL,
	correl                smallint   NOT NULL,
	codemi                smallint   NOT NULL,
	ntacre                char(20)   NOT NULL,
	numvou                integer   NOT NULL,
	totval                decimal(8,2)   NOT NULL,
	
		 CHECK (correl > 0)
		 CONSTRAINT  sistemas.ckfactarjetas1,
		 CHECK (numvou > 0)
		 CONSTRAINT  sistemas.ckfactarjetas2,
		 CHECK (totval > 0.)
		 CONSTRAINT  sistemas.ckfactarjetas3
);



CREATE TABLE fac_tdocxpos
(
	lnktdc                serial   NOT NULL,
	numpos                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	tipdoc                smallint   NOT NULL,
	nserie                char(10)   NOT NULL,
	numcor                integer  ,
	estado                char(1)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (estado IN ('A' ,'B' ))
		 CONSTRAINT  sistemas.ckfactdocxpos1
);



ALTER TABLE fac_tdocxpos
	ADD CONSTRAINT  PRIMARY KEY (lnktdc) CONSTRAINT pkfactdocxpos ;



CREATE TABLE fac_tipodocs
(
	tipdoc                smallint   NOT NULL,
	nomdoc                varchar(40)   NOT NULL,
	nomabr                char(6)   NOT NULL,
	exeimp                smallint   NOT NULL,
	hayimp                smallint   NOT NULL,
	numfor                smallint   NOT NULL,
	userid                char(10)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (exeimp IN (0 ,1 ))
		 CONSTRAINT  sistemas.ckfactipodocs1,
		 CHECK (hayimp IN (0 ,1 ))
		 CONSTRAINT  sistemas.ckfactipodocs2
);



ALTER TABLE fac_tipodocs
	ADD CONSTRAINT  PRIMARY KEY (tipdoc) CONSTRAINT pkfactipodocs ;



CREATE TABLE fac_usuaxpos
(
	numpos                smallint   NOT NULL,
	userid                varchar(15)   NOT NULL,
	passwd                varchar(20)   NOT NULL,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE fac_vicortes
(
	lnkcor                serial   NOT NULL,
	numpos                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	feccor                date   NOT NULL,
	tipcor                smallint   NOT NULL,
	cajero                varchar(15)   NOT NULL,
	ndpque                integer  ,
	ndpdol                integer  ,
	ndpsof                integer  ,
	numlot                integer  ,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (feccor >= DATE ('01/02/2011' ))
		 CONSTRAINT  sistemas.ckfacvicortes1
);



CREATE TABLE fis_proenbod
(
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	exican                decimal(14,2)   NOT NULL
);



ALTER TABLE fis_proenbod
	ADD CONSTRAINT  PRIMARY KEY (codemp,codsuc,codbod,cditem) CONSTRAINT XPKfis_proenbod ;



CREATE TABLE glb_empresas
(
	codemp                smallint   NOT NULL,
	nomemp                varchar(50,1)   NOT NULL,
	nomabr                varchar(12,1)   NOT NULL,
	numnit                varchar(15,1)   NOT NULL,
	numtel                varchar(15,1)  ,
	numfax                varchar(15,1)  ,
	diremp                varchar(100,1)   NOT NULL,
	pathlogo              varchar(100)  ,
	userid                char(15)   NOT NULL,
	fecsis                date  ,
	horsis                DATETIME HOUR TO SECOND  
);



ALTER TABLE glb_empresas
	ADD CONSTRAINT  PRIMARY KEY (codemp) CONSTRAINT pkglbemrpesas ;



CREATE TABLE glb_mtpaises
(
	codpai                smallint   NOT NULL,
	nompai                varchar(40,1)   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE glb_mtpaises
	ADD CONSTRAINT  PRIMARY KEY (codpai) CONSTRAINT pkglbmtpaises ;



CREATE TABLE glb_opcsrole
(
	roleid                smallint   NOT NULL,
	opcrid                smallint   NOT NULL,
	fecini                DATETIME YEAR TO SECOND   NOT NULL,
	fecfin                DATETIME YEAR TO SECOND   NOT NULL,
	activo                char(1)   NOT NULL,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE glb_opcsrole
	ADD CONSTRAINT  PRIMARY KEY (roleid,opcrid) CONSTRAINT XPKglb_opcsrole ;



CREATE TABLE glb_paramtrs
(
	numpar                integer   NOT NULL,
	tippar                integer   NOT NULL,
	valchr                varchar(255,1)   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE glb_permxusr
(
	codpro                char(20)   NOT NULL,
	progid                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	activo                smallint   NOT NULL,
	passwd                char(20)  ,
	fecini                date  ,
	fecfin                date  ,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE glb_programs
(
	codpro                char(15)   NOT NULL,
	nompro                varchar(100,1)   NOT NULL,
	dcrpro                varchar(100,1)  ,
	userid                char(15)   NOT NULL,
	fecis                 date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE glb_programs
	ADD CONSTRAINT  PRIMARY KEY (codpro) CONSTRAINT pkglbprograms ;



CREATE TABLE glb_rolesusr
(
	idrole                smallint   NOT NULL,
	nmrole                varchar(50,1)   NOT NULL,
	tprole                smallint   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (tprole IN (1 ,2 ,3 ))
		 CONSTRAINT  sistemas.ckglbrolesusr1
);



ALTER TABLE glb_rolesusr
	ADD CONSTRAINT  PRIMARY KEY (idrole) CONSTRAINT pkglbrolesusr ;



CREATE TABLE glb_sucsxemp
(
	codsuc                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	nomsuc                varchar(50,1)   NOT NULL,
	nomabr                varchar(16,1)   NOT NULL,
	numnit                varchar(15,1)   NOT NULL,
	numtel                varchar(15,1)  ,
	numfax                varchar(15,1)  ,
	dirsuc                varchar(100,1)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date  ,
	horsis                DATETIME HOUR TO SECOND  
);



ALTER TABLE glb_sucsxemp
	ADD CONSTRAINT  PRIMARY KEY (codsuc) CONSTRAINT pkglbsucsxemp ;



CREATE TABLE glb_userrole
(
	userid                char(15)   NOT NULL,
	roleid                smallint   NOT NULL,
	fecini                DATETIME YEAR TO SECOND   NOT NULL,
	fecfin                DATETIME YEAR TO SECOND   NOT NULL,
	activo                char(1)   NOT NULL,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE glb_userrole
	ADD CONSTRAINT  PRIMARY KEY (userid,roleid) CONSTRAINT XPKglb_userrole ;



CREATE TABLE glb_usuarios
(
	userid                varchar(15,1)   NOT NULL,
	nomusr                varchar(50,1)   NOT NULL,
	email1                varchar(50,1)  ,
	usuaid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE glb_usuarios
	ADD CONSTRAINT  PRIMARY KEY (userid) CONSTRAINT pkglbusuarios ;



CREATE TABLE his_clientes
(
	lnkcli                serial   NOT NULL,
	codcli                integer   NOT NULL,
	nomcli                char(60)   NOT NULL,
	numnit                char(30)   NOT NULL,
	numtel                char(30)  ,
	numfax                char(15)  ,
	dircli                char(100)   NOT NULL,
	nomcon                char(60)  ,
	bemail                char(100)  ,
	hayfac                smallint   NOT NULL,
	moncre                decimal(14,2)   NOT NULL,
	diacre                smallint   NOT NULL,
	salcre                decimal(14,2)   NOT NULL,
	status                smallint   NOT NULL,
	estado                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	usract                char(15)   NOT NULL,
	fecact                date   NOT NULL,
	horact                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE his_clientes
	ADD CONSTRAINT  PRIMARY KEY (lnkcli) CONSTRAINT pkhisclientes ;



CREATE TABLE his_dtransac
(
	lnkhis                integer   NOT NULL,
	lnktra                integer   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	tipmov                smallint   NOT NULL,
	codori                integer   NOT NULL,
	coddes                integer   NOT NULL,
	fecemi                date  ,
	aniotr                smallint  ,
	mestra                smallint  ,
	cditem                integer   NOT NULL,
	codabr                varchar(20,1)   NOT NULL,
	lnkord                integer  ,
	nuitem                integer  ,
	codepq                smallint   NOT NULL,
	canepq                decimal(14,2)   NOT NULL,
	canuni                decimal(14,2)   NOT NULL,
	canmtc                decimal(14,2)  ,
	preuni                decimal(14,6)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	prepro                decimal(14,2)   NOT NULL,
	correl                smallint   NOT NULL,
	barcod                varchar(20)  ,
	tipope                smallint   NOT NULL,
	estado                char(1)   NOT NULL,
	opeval                decimal(14,2)   NOT NULL,
	opeuni                decimal(14,2)   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE his_mtransac
(
	lnkhis                serial   NOT NULL,
	lnktra                integer   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	tipmov                smallint   NOT NULL,
	codori                integer   NOT NULL,
	coddes                integer   NOT NULL,
	numrf1                varchar(20)  ,
	numrf2                varchar(20)  ,
	aniotr                smallint   NOT NULL,
	mestra                smallint   NOT NULL,
	fecemi                date   NOT NULL,
	tipope                smallint   NOT NULL,
	observ                varchar(255)  ,
	estado                char(1)   NOT NULL,
	motanl                varchar(100)  ,
	fecanl                date  ,
	horanl                DATETIME HOUR TO SECOND  ,
	usranl                varchar(15)  ,
	impres                char(1)   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	usract                varchar(15)   NOT NULL,
	fecact                date   NOT NULL,
	horact                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE his_mtransac
	ADD CONSTRAINT  PRIMARY KEY (lnkhis) CONSTRAINT pkhismtransac ;



CREATE TABLE his_preciosm
(
	lnkpre                serial   NOT NULL,
	cditem                integer   NOT NULL,
	preant                decimal(12,2)   NOT NULL,
	premin                decimal(12,2)   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE UNIQUE INDEX ix130_1 ON his_preciosm
(
	lnkpre                ASC
);



CREATE TABLE his_precioss
(
	lnkpre                serial   NOT NULL,
	cditem                integer   NOT NULL,
	preant                decimal(12,2)   NOT NULL,
	presug                decimal(12,2)   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE his_precioss
	ADD CONSTRAINT  PRIMARY KEY (lnkpre) CONSTRAINT pkhisprecioss1 ;



CREATE TABLE his_products
(
	lnkpro                serial   NOT NULL,
	cditem                integer   NOT NULL,
	codcat                smallint   NOT NULL,
	subcat                smallint   NOT NULL,
	codcol                smallint   NOT NULL,
	codprv                integer   NOT NULL,
	codmed                integer   NOT NULL,
	unimed                smallint   NOT NULL,
	dsitem                char(100)   NOT NULL,
	codabr                char(20)   NOT NULL,
	premin                decimal(12,2)   NOT NULL,
	estado                char(1)   NOT NULL,
	status                smallint  ,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	usract                char(15)   NOT NULL,
	fecact                date   NOT NULL,
	horact                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE his_products
	ADD CONSTRAINT  PRIMARY KEY (lnkpro) CONSTRAINT pkhisproducts ;



CREATE TABLE inv_categpro
(
	codcat                smallint   NOT NULL,
	nomcat                varchar(50,1)   NOT NULL,
	codabr                char(2)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_categpro
	ADD CONSTRAINT  PRIMARY KEY (codcat) CONSTRAINT pkinvcategpro ;



CREATE UNIQUE INDEX sistemas.uqinvcategpro ON inv_categpro
(
	codabr                ASC
);



CREATE TABLE inv_colorpro
(
	codcol                smallint   NOT NULL,
	nomcol                varchar(50,1)   NOT NULL,
	codabr                char(2)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_colorpro
	ADD CONSTRAINT  PRIMARY KEY (codcol) CONSTRAINT pkinvcolorpro ;



CREATE UNIQUE INDEX sistemas.uqinvcolorpro ON inv_colorpro
(
	codabr                ASC
);



CREATE TABLE inv_dasignac
(
	lnkasi                integer   NOT NULL,
	correl                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                char(20)   NOT NULL,
	unimed                smallint   NOT NULL,
	canori                decimal(14,2)   NOT NULL,
	unimto                smallint   NOT NULL,
	cantid                decimal(14,2)   NOT NULL,
	preuni                decimal(12,2)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	factor                decimal(14,6)   NOT NULL,
	
		 CHECK (correl > 0)
		 CONSTRAINT  sistemas.ckfacdasignac1,
		 CHECK (totpro >= 0.)
		 CONSTRAINT  sistemas.ckfacdasignac4
);



CREATE TABLE inv_dorden
(
	lnkord                integer   NOT NULL,
	nuitem                integer   NOT NULL,
	subcat                smallint   NOT NULL,
	codcol                smallint   NOT NULL,
	cantid                decimal(12,2)   NOT NULL,
	xlargo                decimal(5,2)   NOT NULL,
	yancho                decimal(5,2)   NOT NULL,
	descrp                char(100)  ,
	observ                char(500)  ,
	premed                decimal(16,6)   NOT NULL,
	precio                decimal(8,2)   NOT NULL,
	compre                char(100)  ,
	desman                smallint  ,
	sinord                smallint  ,
	ordext                smallint  ,
	medida                smallint   NOT NULL,
	tipord                smallint   NOT NULL,
	nofase                smallint   NOT NULL
);



CREATE TABLE inv_dtransac
(
	lnktra                integer   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	tipmov                smallint   NOT NULL,
	codori                integer   NOT NULL,
	coddes                integer   NOT NULL,
	fecemi                date  ,
	aniotr                smallint  ,
	mestra                smallint  ,
	cditem                integer   NOT NULL,
	codabr                varchar(20,1)   NOT NULL,
	lnkord                integer  ,
	nuitem                integer  ,
	codepq                smallint   NOT NULL,
	canepq                decimal(14,2)   NOT NULL,
	canuni                decimal(14,2)   NOT NULL,
	canmtc                decimal(14,2)  ,
	preuni                decimal(14,6)   NOT NULL,
	totpro                decimal(14,2)   NOT NULL,
	prepro                decimal(14,2)   NOT NULL,
	correl                smallint   NOT NULL,
	barcod                varchar(20)  ,
	tipope                smallint   NOT NULL,
	estado                char(1)   NOT NULL,
	opeval                decimal(14,2)   NOT NULL,
	opeuni                decimal(14,2)   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE inv_empaques
(
	codepq                smallint   NOT NULL,
	nomepq                varchar(30)   NOT NULL,
	cantid                decimal(12,6)   NOT NULL,
	unimed                smallint   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (cantid > 0.)
		 CONSTRAINT  sistemas.ckinvempaques1
);



ALTER TABLE inv_empaques
	ADD CONSTRAINT  PRIMARY KEY (codepq) CONSTRAINT pkiviempaques ;



CREATE TABLE inv_epqsxpro
(
	lnkepq                serial   NOT NULL,
	cditem                integer   NOT NULL,
	codepq                smallint   NOT NULL,
	nomepq                varchar(40,1)   NOT NULL,
	cantid                decimal(12,6)   NOT NULL,
	userid                varchar(15,1)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_epqsxpro
	ADD CONSTRAINT  PRIMARY KEY (lnkepq) CONSTRAINT pkinvepqsxpro ;



CREATE UNIQUE INDEX sistemas.uqinvepqsxpro ON inv_epqsxpro
(
	cditem                ASC,
	codepq                ASC
);



CREATE TABLE inv_masignac
(
	lnkasi                serial   NOT NULL,
	lnkord                integer   NOT NULL,
	estado                char(1)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_masignac
	ADD CONSTRAINT  PRIMARY KEY (lnkasi) CONSTRAINT pkinvordenasi ;



CREATE TABLE inv_mbodegas
(
	codbod                smallint   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	nombod                varchar(50,1)   NOT NULL,
	nomabr                varchar(12,1)  ,
	chkexi                smallint   NOT NULL,
	bodcon                smallint  ,
	hayfac                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (chkexi IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckinvmbodegas1
);



ALTER TABLE inv_mbodegas
	ADD CONSTRAINT  PRIMARY KEY (codbod) CONSTRAINT pkinvmbodegas ;



CREATE TABLE inv_medidpro
(
	codmed                smallint   NOT NULL,
	codabr                char(12)   NOT NULL,
	desmed                varchar(60,1)   NOT NULL,
	unimed                smallint   NOT NULL,
	epqfra                smallint   NOT NULL,
	xlargo                decimal(5,2)   NOT NULL,
	yancho                decimal(5,2)   NOT NULL,
	medtot                decimal(5,2)   NOT NULL,
	unimto                smallint  ,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_medidpro
	ADD CONSTRAINT  PRIMARY KEY (codmed) CONSTRAINT pkinvmedidpro ;



CREATE TABLE inv_morden
(
	lnkord                serial   NOT NULL,
	lnktra                integer   NOT NULL,
	numpos                smallint   NOT NULL,
	codcli                smallint   NOT NULL,
	fecord                date   NOT NULL,
	totord                decimal(14,2)   NOT NULL,
	totabo                decimal(14,2)   NOT NULL,
	salord                decimal(14,2)   NOT NULL,
	fecofe                date   NOT NULL,
	fecent                date   NOT NULL,
	doctos                char(100)  ,
	obspre                char(100)  ,
	estado                char(1)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_morden
	ADD CONSTRAINT  PRIMARY KEY (lnkord) CONSTRAINT pkinvmorden ;



CREATE TABLE inv_mtiposal
(
	tipsld                smallint   NOT NULL,
	destip                varchar(30)   NOT NULL,
	haymov                smallint  ,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_mtiposal
	ADD CONSTRAINT  PRIMARY KEY (tipsld) CONSTRAINT pkinvmtiposal ;



CREATE TABLE inv_mtransac
(
	lnktra                serial   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	tipmov                smallint   NOT NULL,
	haytra                smallint  ,
	codori                integer   NOT NULL,
	coddes                integer   NOT NULL,
	numrf1                varchar(20)  ,
	numrf2                varchar(20)  ,
	numord                char(15)  ,
	aniotr                smallint   NOT NULL,
	mestra                smallint   NOT NULL,
	fecemi                date   NOT NULL,
	tipope                smallint   NOT NULL,
	observ                varchar(255)  ,
	estado                char(1)   NOT NULL,
	motanl                varchar(100)  ,
	fecanl                date  ,
	horanl                DATETIME HOUR TO SECOND  ,
	usranl                char(10)  ,
	impres                char(1)   NOT NULL,
	userid                char(10)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_mtransac
	ADD CONSTRAINT  PRIMARY KEY (lnktra) CONSTRAINT pkinvmtransac ;



CREATE TABLE inv_ordentra
(
	lnkord                serial   NOT NULL,
	lnktra                integer   NOT NULL,
	numpos                smallint   NOT NULL,
	numord                char(15)   NOT NULL,
	codcli                smallint   NOT NULL,
	fecord                date   NOT NULL,
	subcat                smallint   NOT NULL,
	codcol                smallint   NOT NULL,
	cantid                decimal(12,2)   NOT NULL,
	xlargo                decimal(5,2)   NOT NULL,
	yancho                decimal(5,2)   NOT NULL,
	descrp                char(100)  ,
	observ                char(500)  ,
	premed                decimal(9,6)   NOT NULL,
	totord                decimal(14,2)   NOT NULL,
	totabo                decimal(14,2)   NOT NULL,
	salord                decimal(14,2)   NOT NULL,
	fecofe                date   NOT NULL,
	fecent                date   NOT NULL,
	doctos                char(100)  ,
	obspre                char(100)  ,
	precio                decimal(8,2)   NOT NULL,
	compre                char(100)  ,
	desman                smallint  ,
	sinord                smallint  ,
	ordext                smallint  ,
	medida                smallint   NOT NULL,
	tipord                smallint   NOT NULL,
	estado                char(1)   NOT NULL,
	nofase                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_ordentra
	ADD CONSTRAINT  PRIMARY KEY (lnkord) CONSTRAINT pkinvordentra ;



CREATE TABLE inv_permxbod
(
	codbod                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE inv_permxtmv
(
	tipmov                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	usuaid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE inv_products
(
	cditem                serial   NOT NULL,
	codcat                smallint   NOT NULL,
	subcat                smallint   NOT NULL,
	codcol                smallint   NOT NULL,
	codprv                integer   NOT NULL,
	codmed                integer   NOT NULL,
	unimed                smallint   NOT NULL,
	dsitem                varchar(100,1)   NOT NULL,
	codabr                varchar(20,1)   NOT NULL,
	premin                decimal(12,2)   NOT NULL,
	presug                decimal(12,2)   NOT NULL,
	pulcom                decimal(12,2)   NOT NULL,
	estado                smallint   NOT NULL,
	status                smallint   NOT NULL,
	fulcam                date  ,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (premin >= 0.)
		 CONSTRAINT  sistemas.ckinvproducts1,
		 CHECK (estado IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckinvproducts2,
		 CHECK (status IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckinvproducts3
);



ALTER TABLE inv_products
	ADD CONSTRAINT  PRIMARY KEY (cditem) CONSTRAINT pkinvproducts ;



CREATE TABLE inv_proenbod
(
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                varchar(20)   NOT NULL,
	eximin                decimal(14,2)   NOT NULL,
	eximax                decimal(14,2)   NOT NULL,
	fulent                date  ,
	fulsal                date  ,
	exican                decimal(14,2)   NOT NULL,
	exival                decimal(14,2)   NOT NULL,
	eximtc                decimal(14,2)  ,
	cospro                decimal(14,6)   NOT NULL,
	userid                varchar(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (eximin >= 0.)
		 CONSTRAINT  sistemas.ckinvproenbod1,
		 CHECK (eximax >= 0.)
		 CONSTRAINT  sistemas.ckinvproenbod2
);



ALTER TABLE inv_proenbod
	ADD CONSTRAINT  PRIMARY KEY (codemp,codsuc,codbod,cditem) CONSTRAINT primaria ;



CREATE TABLE inv_provedrs
(
	codprv                integer   NOT NULL,
	nomprv                varchar(100,1)   NOT NULL,
	codabr                char(2)   NOT NULL,
	numnit                varchar(30,1)   NOT NULL,
	numtel                varchar(30,1)  ,
	numfax                varchar(30,1)  ,
	dirprv                varchar(100,1)   NOT NULL,
	nomcon                varchar(50,1)  ,
	bemail                varchar(100,1)  ,
	tipprv                smallint   NOT NULL,
	codpai                smallint   NOT NULL,
	observ                varchar(255,1)  ,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_provedrs
	ADD CONSTRAINT  PRIMARY KEY (codprv) CONSTRAINT pkinvprovedrs ;



CREATE UNIQUE INDEX sistemas.uqinvprovedrs ON inv_provedrs
(
	codabr                ASC
);



CREATE TABLE inv_saldopro
(
	lnksal                serial   NOT NULL,
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	tipsld                smallint   NOT NULL,
	aniotr                smallint   NOT NULL,
	mestra                smallint   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                varchar(20,1)   NOT NULL,
	canuni                decimal(16,2)   NOT NULL,
	prepro                decimal(16,6)   NOT NULL,
	totpro                decimal(16,2)   NOT NULL,
	userid                char(10)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_saldopro
	ADD CONSTRAINT  PRIMARY KEY (lnksal) CONSTRAINT pkinvsaldopro ;



CREATE UNIQUE INDEX sistemas.uqinvsaldopro ON inv_saldopro
(
	codemp                ASC,
	codsuc                ASC,
	codbod                ASC,
	tipsld                ASC,
	aniotr                ASC,
	mestra                ASC,
	cditem                ASC
);



CREATE TABLE inv_subcateg
(
	codcat                smallint   NOT NULL,
	subcat                smallint   NOT NULL,
	nomsub                varchar(50,1)   NOT NULL,
	codabr                char(4)   NOT NULL,
	tipsub                smallint   NOT NULL,
	lonmed                smallint   NOT NULL,
	premin                decimal(9,6)   NOT NULL,
	presug                decimal(9,6)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_subcateg
	ADD CONSTRAINT  PRIMARY KEY (codcat,subcat) CONSTRAINT pkinvsubcateg ;



CREATE TABLE inv_tipomovs
(
	tipmov                smallint   NOT NULL,
	nommov                varchar(50,1)   NOT NULL,
	tipope                smallint   NOT NULL,
	nomabr                char(6)   NOT NULL,
	hayval                smallint   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



CREATE TABLE inv_tofisico
(
	codemp                smallint   NOT NULL,
	codsuc                smallint   NOT NULL,
	codbod                smallint   NOT NULL,
	aniotr                smallint   NOT NULL,
	mestra                smallint   NOT NULL,
	fecinv                date   NOT NULL,
	cditem                integer   NOT NULL,
	codabr                varchar(20,1)   NOT NULL,
	codepq                smallint   NOT NULL,
	canepq                decimal(16,2)   NOT NULL,
	canuni                decimal(16,2)   NOT NULL,
	totpro                decimal(16,2)   NOT NULL,
	canexi                decimal(16,2)   NOT NULL,
	finmes                smallint   NOT NULL,
	userid                char(10)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL,
	
		 CHECK (codepq >= 0)
		 CONSTRAINT  sistemas.ckinvtofisico1,
		 CHECK (canepq > 0.)
		 CONSTRAINT  sistemas.ckinvtofisico2,
		 CHECK (canuni > 0.)
		 CONSTRAINT  sistemas.ckinvtofisico3,
		 CHECK (totpro >= 0.)
		 CONSTRAINT  sistemas.ckinvtofisico4,
		 CHECK (canexi >= 0.)
		 CONSTRAINT  sistemas.ckinvtofisico5,
		 CHECK (finmes IN (1 ,0 ))
		 CONSTRAINT  sistemas.ckinvtofisico6,
		 CHECK (fecinv >= DATE ('01/01/2010' ))
		 CONSTRAINT  sistemas.ckinvtofisico7
);



ALTER TABLE inv_tofisico
	ADD CONSTRAINT  PRIMARY KEY (codemp,codsuc,codbod,aniotr,mestra,fecinv,cditem) CONSTRAINT pkinvtofisico ;



CREATE TABLE inv_unimedid
(
	unimed                smallint   NOT NULL,
	nommed                varchar(50,1)   NOT NULL,
	nomabr                char(4)   NOT NULL,
	factor                decimal(9,6)   NOT NULL,
	userid                char(15)   NOT NULL,
	fecsis                date   NOT NULL,
	horsis                DATETIME HOUR TO SECOND   NOT NULL
);



ALTER TABLE inv_unimedid
	ADD CONSTRAINT  PRIMARY KEY (unimed) CONSTRAINT pkinvunimedid ;



CREATE TABLE med
(
	codigo                smallint  ,
	codabr                char(20)  
);



CREATE TABLE mest
(
	est_id                serial   NOT NULL,
	est_desc              varchar(20)   NOT NULL
);



ALTER TABLE mest
	ADD CONSTRAINT  PRIMARY KEY (est_id) CONSTRAINT mest_pk ;



CREATE TABLE mprog
(
	prog_id               serial   NOT NULL,
	prog_nom              varchar(50)   NOT NULL,
	prog_des              varchar(150)   NOT NULL,
	prog_dirpro           varchar(150)   NOT NULL,
	prog_fechor           DATETIME YEAR TO SECOND   NOT NULL,
	prog_tip              char(1)   NOT NULL,
	prog_idx              integer  ,
	prog_verhe            smallint  ,
	prog_padre            integer  ,
	est_id                integer  
);



CREATE TABLE pro
(
	codbod                smallint  ,
	nombod                char(30)  ,
	lnktra                integer  ,
	correl                smallint  ,
	codabr                char(20)  ,
	dsitem                char(50)  ,
	cantid                decimal(12,2)  
);



CREATE VIEW vis_saldosxmovxmes ( codemp,codsuc,codbod,aniotr,mestra,cditem,tipope,saluni,salval )  AS 
	SELECT x0.codemp,x0.codsuc,x0.codbod,x0.aniotr,x0.mestra,x0.cditem,x0.tipope,NVL(sum(x0.canuni), 0),NVL(sum(x0.totpro), 0)
		FROM inv_dtransac x0
		WHERE (x0.estado =  'V')
		GROUP BY x0.codemp, x0.codsuc, x0.codbod, x0.aniotr, x0.mestra, x0.cditem, x0.tipope;



CREATE VIEW vis_tipdocxpos ( lnktdc,numpos,nomdoc )  AS 
	SELECT x0.lnktdc,x0.numpos,Expr_2112
		FROM ;



CREATE VIEW productos ( cditem,codcat,subcat,codcol,codprv,codmed,unimed,dsitem,codabr,premin,presug,pulcom,estado,status,fulcam,userid,fecsis,horsis,tipo )  AS 
	SELECT x0.cditem,x0.codcat,x0.subcat,x0.codcol,x0.codprv,x0.codmed,x0.unimed,x0.dsitem,x0.codabr,x0.premin,x0.presug,x0.pulcom,x0.estado,x0.status,x0.fulcam,x0.userid,x0.fecsis,x0.horsis,0
		FROM inv_products x0,inv_morden x1,inv_dorden x2,inv_subcateg x3,inv_categpro x4,inv_products x5,inv_medidpro x6,inv_unimedid x7,inv_colorpro x8;



CREATE VIEW vis_ordentraint ( numpos,nombrepos,ordentrabajo,numlona,fechaorden,cliente,cantidad,clase,color,medidas,nombremedida,usuario,serie,documento,ordencompra,fechaofrecida,descripcion,hora )  AS 
	SELECT x0.numpos,x5.nompos,x0.lnkord,x6.nuitem,x0.fecord,x4.nomcli,x6.cantid,x2.nomsub,x3.nomcol,((x6.xlargo || ' X ') || x6.yancho),CASE,Expr_2198,Expr_2200,Expr_2202,Expr_2204,Expr_2206,Expr_2208,Expr_2210
		FROM ;



CREATE VIEW vis_repventa ( fecemi,docvta,ptovta,codemp,totpag,subtot,totiva,usuario,tipdoc,serial,tipo )  AS 
	SELECT x2.fecemi,Expr_2217,Expr_2219,Expr_2221,Expr_2223,Expr_2225,Expr_2227,Expr_2229,Expr_2231,Expr_2233,Expr_2235
		FROM ;



ALTER TABLE bbitp
	ADD CONSTRAINT  FOREIGN KEY (proc_id) REFERENCES bproc (proc_id)
 CONSTRAINT  r265_878 ;



ALTER TABLE bproc
	ADD CONSTRAINT  FOREIGN KEY (est_id) REFERENCES mest (est_id)
 CONSTRAINT  r244_873 ;



ALTER TABLE fac_dnotcre
	ADD CONSTRAINT  FOREIGN KEY (linknc) REFERENCES fac_mnotcre (linknc)
 CONSTRAINT  R_37 ;



ALTER TABLE fac_dtransac
	ADD CONSTRAINT  FOREIGN KEY (lnktra) REFERENCES fac_mtransac (lnktra)
		ON DELETE CASCADE
 CONSTRAINT  fkfacmtransac1 ;



ALTER TABLE fac_mnotcre
	ADD CONSTRAINT  FOREIGN KEY (codcli) REFERENCES fac_clientes (codcli)
 CONSTRAINT  R_33 ;




ALTER TABLE fac_mnotcre
	ADD CONSTRAINT  FOREIGN KEY (lnktra) REFERENCES fac_mtransac (lnktra)
 CONSTRAINT  R_36 ;



ALTER TABLE fac_tarjetas
	ADD CONSTRAINT  FOREIGN KEY (lnktra) REFERENCES fac_mtransac (lnktra)
		ON DELETE CASCADE
 CONSTRAINT  fkfacmtransac2 ;



ALTER TABLE fac_usuaxpos
	ADD CONSTRAINT  FOREIGN KEY (numpos) REFERENCES fac_puntovta (numpos)
		ON DELETE CASCADE
 CONSTRAINT  fkfacpuntovta1 ;



ALTER TABLE glb_sucsxemp
	ADD CONSTRAINT  FOREIGN KEY (codemp) REFERENCES glb_empresas (codemp)
 CONSTRAINT  fkglbempresas ;



ALTER TABLE his_dtransac
	ADD CONSTRAINT  FOREIGN KEY (lnkhis) REFERENCES his_mtransac (lnkhis)
		ON DELETE CASCADE
 CONSTRAINT  fkhismtransac1 ;



ALTER TABLE his_preciosm
	ADD CONSTRAINT  FOREIGN KEY (cditem) REFERENCES inv_products (cditem)
		ON DELETE CASCADE
 CONSTRAINT  fkinvproducts2 ;



ALTER TABLE his_precioss
	ADD CONSTRAINT  FOREIGN KEY (cditem) REFERENCES inv_products (cditem)
		ON DELETE CASCADE
 CONSTRAINT  fkinvproducts4 ;



ALTER TABLE inv_dasignac
	ADD CONSTRAINT  FOREIGN KEY (lnkasi) REFERENCES inv_masignac (lnkasi)
		ON DELETE CASCADE
 CONSTRAINT  fkinvasignac1 ;



ALTER TABLE inv_dorden
	ADD CONSTRAINT  FOREIGN KEY (lnkord) REFERENCES inv_morden (lnkord)
 CONSTRAINT  fkinvdorden ;



ALTER TABLE inv_dtransac
	ADD CONSTRAINT  FOREIGN KEY (lnktra) REFERENCES inv_mtransac (lnktra)
 CONSTRAINT  fkinvmtransac1 ;



ALTER TABLE inv_epqsxpro
	ADD CONSTRAINT  FOREIGN KEY (cditem) REFERENCES inv_products (cditem)
		ON DELETE CASCADE
 CONSTRAINT  fkinvproducts3 ;



ALTER TABLE inv_masignac
	ADD CONSTRAINT  FOREIGN KEY (lnkord) REFERENCES inv_ordentra (lnkord)
 CONSTRAINT  R_29 ;



ALTER TABLE inv_mbodegas
	ADD CONSTRAINT  FOREIGN KEY (codemp) REFERENCES glb_empresas (codemp)
 CONSTRAINT  fkglbempresas2 ;




ALTER TABLE inv_mbodegas
	ADD CONSTRAINT  FOREIGN KEY (codsuc) REFERENCES glb_sucsxemp (codsuc)
 CONSTRAINT  fkglbsucsxemp1 ;



ALTER TABLE inv_ordentra
	ADD CONSTRAINT  FOREIGN KEY (lnktra) REFERENCES fac_mtransac (lnktra)
		ON DELETE CASCADE
 CONSTRAINT  fkfacmtransac3 ;



ALTER TABLE inv_products
	ADD CONSTRAINT  FOREIGN KEY (codcat) REFERENCES inv_categpro (codcat)
 CONSTRAINT  fkinvcategpro1 ;




ALTER TABLE inv_products
	ADD CONSTRAINT  FOREIGN KEY (codcol) REFERENCES inv_colorpro (codcol)
 CONSTRAINT  fkinvcolorpro1 ;




ALTER TABLE inv_products
	ADD CONSTRAINT  FOREIGN KEY (codprv) REFERENCES inv_provedrs (codprv)
 CONSTRAINT  fkinvprovedrs1 ;




ALTER TABLE inv_products
	ADD CONSTRAINT  FOREIGN KEY (unimed) REFERENCES inv_unimedid (unimed)
 CONSTRAINT  fkinvunimedid1 ;



ALTER TABLE inv_provedrs
	ADD CONSTRAINT  FOREIGN KEY (codpai) REFERENCES glb_mtpaises (codpai)
 CONSTRAINT  fkglbmtpaises1 ;



CREATE TRIGGER tins_bproc INSERT ON bproc
  REFERENCING NEW AS post
  FOR EACH ROW
(
        execute procedure "informix".ins_bbitp(post.proc_id ,
    post.proc_desc ,NULL ,post.est_id ))
;







CREATE TRIGGER tupd_bproc UPDATE ON bproc
  REFERENCING OLD AS prev NEW AS post
  FOR EACH ROW
(
        execute procedure "informix".ins_bbitp(post.proc_id ,
    post.proc_desc ,prev.est_id ,post.est_id ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_bproc DELETE ON bproc
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on bproc
    -- ERwin Builtin Trigger
    -- bproc  bbitp on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000b3db", PARENT_OWNER="", PARENT_TABLE="bproc"
    -- CHILD_OWNER="", CHILD_TABLE="bbitp"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="r265_878", FK_COLUMNS="proc_id"
    WHEN (EXISTS (
      SELECT * FROM bbitp
      WHERE
        --  %JoinFKPK(bbitp,deleted," = "," AND") 
        bbitp.proc_id = deleted.proc_id
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete bproc because bbitp exists.'
       )) 


;


CREATE TRIGGER trgupdateclientes UPDATE ON fac_clientes
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
(
        insert into "sistemas".his_clientes (lnkcli,codcli,nomcli,
    numnit,numtel,numfax,dircli,nomcon,bemail,hayfac,moncre,diacre,salcre,
    status,estado,userid,fecsis,horsis,usract,fecact,horact)  values 
    (0 ,pre.codcli ,pre.nomcli ,pre.numnit ,pre.numtel ,pre.numfax ,pre.dircli 
    ,pre.nomcon ,pre.bemail ,pre.hayfac ,pre.moncre ,pre.diacre ,pre.salcre 
    ,pre.status ,pre.estado ,pre.userid ,pre.fecsis ,pre.horsis ,USER 
    ,CURRENT year to fraction(3) ,CURRENT hour to second ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_fac_clientes DELETE ON fac_clientes
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on fac_clientes
    -- ERwin Builtin Trigger
    -- fac_clientes  fac_mnotcre on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000d06f", PARENT_OWNER="", PARENT_TABLE="fac_clientes"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_33", FK_COLUMNS="codcli"
    WHEN (EXISTS (
      SELECT * FROM fac_mnotcre
      WHERE
        --  %JoinFKPK(fac_mnotcre,deleted," = "," AND") 
        fac_mnotcre.codcli = deleted.codcli
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete fac_clientes because fac_mnotcre exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tI_fac_dnotcre INSERT ON fac_dnotcre
  REFERENCING NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- INSERT trigger on fac_dnotcre
    -- ERwin Builtin Trigger
    -- fac_mnotcre  fac_dnotcre on child insert restrict 
    -- ERWIN_RELATION:CHECKSUM="0000e252", PARENT_OWNER="", PARENT_TABLE="fac_mnotcre"
    -- CHILD_OWNER="", CHILD_TABLE="fac_dnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_37", FK_COLUMNS="linknc"
    WHEN (
      -- %NotnullFK(inserted," IS NOT NULL AND") 
      
      NOT EXISTS (
        SELECT * FROM fac_mnotcre
        WHERE
          -- %JoinFKPK(inserted,fac_mnotcre," = "," AND") 
          inserted.linknc = fac_mnotcre.linknc
      )
    )
      (EXECUTE PROCEDURE erwin_raise_except(
        -746,
        'Cannot insert fac_dnotcre because fac_mnotcre does not exist.'
      )) 


;

CREATE TRIGGER tU_fac_dnotcre UPDATE ON fac_dnotcre
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on fac_dnotcre
    -- ERwin Builtin Trigger
    -- fac_mnotcre  fac_dnotcre on child update restrict 
    -- ERWIN_RELATION:CHECKSUM="0000e5c3", PARENT_OWNER="", PARENT_TABLE="fac_mnotcre"
    -- CHILD_OWNER="", CHILD_TABLE="fac_dnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_37", FK_COLUMNS="linknc"
    WHEN (
      -- %NotnullFK(inserted," IS NOT NULL AND") 
      
      NOT EXISTS (
        SELECT * FROM fac_mnotcre
        WHERE
          -- %JoinFKPK(inserted,fac_mnotcre," = "," AND") 
          inserted.linknc = fac_mnotcre.linknc
      )
    )
      (EXECUTE PROCEDURE erwin_raise_except(
        -746,
        'Cannot update fac_dnotcre because fac_mnotcre does not exist.'
      )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tI_fac_mnotcre INSERT ON fac_mnotcre
  REFERENCING NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- INSERT trigger on fac_mnotcre
    -- ERwin Builtin Trigger
    -- fac_clientes  fac_mnotcre on child insert set null 
    -- ERWIN_RELATION:CHECKSUM="0002084d", PARENT_OWNER="", PARENT_TABLE="fac_clientes"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_33", FK_COLUMNS="codcli"
    (UPDATE fac_mnotcre
      SET
        -- %SetFK(fac_mnotcre,NULL) 
        fac_mnotcre.codcli = NULL
      WHERE
        -- %JoinPKPK(inserted,fac_mnotcre," = "," AND") 
        fac_mnotcre.linknc = inserted.linknc AND
        NOT EXISTS (
          SELECT * FROM fac_clientes
          WHERE
            -- %JoinFKPK(inserted,fac_clientes," = "," AND") 
            inserted.codcli = fac_clientes.codcli
        )
    ),

    -- ERwin Builtin Trigger
    -- fac_mtransac  fac_mnotcre on child insert set null 
    -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_36", FK_COLUMNS="lnktra"
    (UPDATE fac_mnotcre
      SET
        -- %SetFK(fac_mnotcre,NULL) 
        fac_mnotcre.lnktra = NULL
      WHERE
        -- %JoinPKPK(inserted,fac_mnotcre," = "," AND") 
        fac_mnotcre.linknc = inserted.linknc AND
        NOT EXISTS (
          SELECT * FROM fac_mtransac
          WHERE
            -- %JoinFKPK(inserted,fac_mtransac," = "," AND") 
            inserted.lnktra = fac_mtransac.lnktra
        )
    ) 


;

CREATE TRIGGER tD_fac_mnotcre DELETE ON fac_mnotcre
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on fac_mnotcre
    -- ERwin Builtin Trigger
    -- fac_mnotcre  fac_dnotcre on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000cc68", PARENT_OWNER="", PARENT_TABLE="fac_mnotcre"
    -- CHILD_OWNER="", CHILD_TABLE="fac_dnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_37", FK_COLUMNS="linknc"
    WHEN (EXISTS (
      SELECT * FROM fac_dnotcre
      WHERE
        --  %JoinFKPK(fac_dnotcre,deleted," = "," AND") 
        fac_dnotcre.linknc = deleted.linknc
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete fac_mnotcre because fac_dnotcre exists.'
       )) 


;

CREATE TRIGGER tU_fac_mnotcre UPDATE ON fac_mnotcre
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on fac_mnotcre
  -- ERwin Builtin Trigger
  -- fac_mnotcre  fac_dnotcre on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0003626a", PARENT_OWNER="", PARENT_TABLE="fac_mnotcre"
    -- CHILD_OWNER="", CHILD_TABLE="fac_dnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_37", FK_COLUMNS="linknc"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.linknc = deleted.linknc
    ) AND
    EXISTS (
      SELECT * FROM fac_dnotcre
      WHERE
        --  %JoinFKPK(fac_dnotcre,deleted," = "," AND") 
        fac_dnotcre.linknc = deleted.linknc
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update fac_mnotcre because fac_dnotcre exists.'
       )),

  -- ERwin Builtin Trigger
  -- fac_clientes  fac_mnotcre on child update set null 
  -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_clientes"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_33", FK_COLUMNS="codcli"
  WHEN(
    -- %JoinPKPK(deleted,inserted," <> "," OR") 
    deleted.linknc <> inserted.linknc
  )
      (UPDATE fac_mnotcre
        SET
          -- %SetFK(fac_mnotcre,NULL) 
          fac_mnotcre.codcli = NULL
        WHERE
          -- %JoinPKPK(fac_mnotcre,inserted," = "," AND") 
          fac_mnotcre.linknc = inserted.linknc AND 
          NOT EXISTS (
            SELECT * FROM fac_clientes
            WHERE
              -- %JoinFKPK(inserted,fac_clientes," = "," AND") 
              inserted.codcli = fac_clientes.codcli
          )
      ),

  -- ERwin Builtin Trigger
  -- fac_mtransac  fac_mnotcre on child update set null 
  -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_36", FK_COLUMNS="lnktra"
  WHEN(
    -- %JoinPKPK(deleted,inserted," <> "," OR") 
    deleted.linknc <> inserted.linknc
  )
      (UPDATE fac_mnotcre
        SET
          -- %SetFK(fac_mnotcre,NULL) 
          fac_mnotcre.lnktra = NULL
        WHERE
          -- %JoinPKPK(fac_mnotcre,inserted," = "," AND") 
          fac_mnotcre.linknc = inserted.linknc AND 
          NOT EXISTS (
            SELECT * FROM fac_mtransac
            WHERE
              -- %JoinFKPK(inserted,fac_mtransac," = "," AND") 
              inserted.lnktra = fac_mtransac.lnktra
          )
      ) 


;


CREATE TRIGGER trgupdateinventario UPDATE OF 
                                   lnkinv ON fac_mtransac
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
when (((pos.hayord = 0 ) AND (pos.estado = 'V' ) ) )
            (
            execute procedure "sistemas".sptinsertafacturas(pos.lnktra 
    ,pos.numpos ,pos.codemp ,pos.numdoc ,pos.fecemi ,pos.codcli ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_fac_mtransac DELETE ON fac_mtransac
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on fac_mtransac
    -- ERwin Builtin Trigger
    -- fac_mtransac  fac_tarjetas on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00031766", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="fac_tarjetas"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkfacmtransac2", FK_COLUMNS="lnktra"
    (DELETE FROM fac_tarjetas
      WHERE
        --  %JoinFKPK(fac_tarjetas,deleted," = "," AND") 
        fac_tarjetas.lnktra = deleted.lnktra
    ),

    -- ERwin Builtin Trigger
    -- fac_mtransac  fac_dtransac on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="fac_dtransac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkfacmtransac1", FK_COLUMNS="lnktra"
    (DELETE FROM fac_dtransac
      WHERE
        --  %JoinFKPK(fac_dtransac,deleted," = "," AND") 
        fac_dtransac.lnktra = deleted.lnktra
    ),

    -- ERwin Builtin Trigger
    -- fac_mtransac  inv_ordentra on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="inv_ordentra"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkfacmtransac3", FK_COLUMNS="lnktra"
    (DELETE FROM inv_ordentra
      WHERE
        --  %JoinFKPK(inv_ordentra,deleted," = "," AND") 
        inv_ordentra.lnktra = deleted.lnktra
    ),

    -- ERwin Builtin Trigger
    -- fac_mtransac  fac_mnotcre on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="fac_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="fac_mnotcre"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_36", FK_COLUMNS="lnktra"
    WHEN (EXISTS (
      SELECT * FROM fac_mnotcre
      WHERE
        --  %JoinFKPK(fac_mnotcre,deleted," = "," AND") 
        fac_mnotcre.lnktra = deleted.lnktra
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete fac_mtransac because fac_mnotcre exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_fac_puntovta DELETE ON fac_puntovta
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on fac_puntovta
    -- ERwin Builtin Trigger
    -- fac_puntovta  fac_usuaxpos on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00009e36", PARENT_OWNER="", PARENT_TABLE="fac_puntovta"
    -- CHILD_OWNER="", CHILD_TABLE="fac_usuaxpos"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkfacpuntovta1", FK_COLUMNS="numpos"
    (DELETE FROM fac_usuaxpos
      WHERE
        --  %JoinFKPK(fac_usuaxpos,deleted," = "," AND") 
        fac_usuaxpos.numpos = deleted.numpos
    ) 


;

CREATE TRIGGER tU_fac_puntovta UPDATE ON fac_puntovta
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on fac_puntovta
  -- ERwin Builtin Trigger
  -- fac_puntovta  fac_usuaxpos on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000feb3", PARENT_OWNER="", PARENT_TABLE="fac_puntovta"
    -- CHILD_OWNER="", CHILD_TABLE="fac_usuaxpos"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkfacpuntovta1", FK_COLUMNS="numpos"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.numpos = deleted.numpos
    ) AND
    EXISTS (
      SELECT * FROM fac_usuaxpos
      WHERE
        --  %JoinFKPK(fac_usuaxpos,deleted," = "," AND") 
        fac_usuaxpos.numpos = deleted.numpos
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update fac_puntovta because fac_usuaxpos exists.'
       )) 


;


CREATE TRIGGER trgcortecaja INSERT ON fac_vicortes
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptcortecaja(pos.numpos 
    ,pos.codemp ,pos.feccor ,pos.cajero ))
;







CREATE TRIGGER trganulcortecaja DELETE ON fac_vicortes
  REFERENCING OLD AS pre
  FOR EACH ROW
(
        execute procedure "sistemas".sptanulcortecaja(pre.numpos 
    ,pre.codemp ,pre.feccor ,pre.cajero ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_glb_empresas DELETE ON glb_empresas
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on glb_empresas
    -- ERwin Builtin Trigger
    -- glb_empresas  glb_sucsxemp on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0001ccb4", PARENT_OWNER="", PARENT_TABLE="glb_empresas"
    -- CHILD_OWNER="", CHILD_TABLE="glb_sucsxemp"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbempresas", FK_COLUMNS="codemp"
    WHEN (EXISTS (
      SELECT * FROM glb_sucsxemp
      WHERE
        --  %JoinFKPK(glb_sucsxemp,deleted," = "," AND") 
        glb_sucsxemp.codemp = deleted.codemp
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete glb_empresas because glb_sucsxemp exists.'
       )),

    -- ERwin Builtin Trigger
    -- glb_empresas  inv_mbodegas on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="glb_empresas"
    -- CHILD_OWNER="", CHILD_TABLE="inv_mbodegas"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbempresas2", FK_COLUMNS="codemp"
    WHEN (EXISTS (
      SELECT * FROM inv_mbodegas
      WHERE
        --  %JoinFKPK(inv_mbodegas,deleted," = "," AND") 
        inv_mbodegas.codemp = deleted.codemp
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete glb_empresas because inv_mbodegas exists.'
       )) 


;

CREATE TRIGGER tU_glb_empresas UPDATE ON glb_empresas
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on glb_empresas
  -- ERwin Builtin Trigger
  -- glb_empresas  glb_sucsxemp on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="00021596", PARENT_OWNER="", PARENT_TABLE="glb_empresas"
    -- CHILD_OWNER="", CHILD_TABLE="glb_sucsxemp"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbempresas", FK_COLUMNS="codemp"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codemp = deleted.codemp
    ) AND
    EXISTS (
      SELECT * FROM glb_sucsxemp
      WHERE
        --  %JoinFKPK(glb_sucsxemp,deleted," = "," AND") 
        glb_sucsxemp.codemp = deleted.codemp
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update glb_empresas because glb_sucsxemp exists.'
       )),

  -- ERwin Builtin Trigger
  -- glb_empresas  inv_mbodegas on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="glb_empresas"
    -- CHILD_OWNER="", CHILD_TABLE="inv_mbodegas"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbempresas2", FK_COLUMNS="codemp"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codemp = deleted.codemp
    ) AND
    EXISTS (
      SELECT * FROM inv_mbodegas
      WHERE
        --  %JoinFKPK(inv_mbodegas,deleted," = "," AND") 
        inv_mbodegas.codemp = deleted.codemp
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update glb_empresas because inv_mbodegas exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_glb_mtpaises DELETE ON glb_mtpaises
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on glb_mtpaises
    -- ERwin Builtin Trigger
    -- glb_mtpaises  inv_provedrs on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000d200", PARENT_OWNER="", PARENT_TABLE="glb_mtpaises"
    -- CHILD_OWNER="", CHILD_TABLE="inv_provedrs"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbmtpaises1", FK_COLUMNS="codpai"
    WHEN (EXISTS (
      SELECT * FROM inv_provedrs
      WHERE
        --  %JoinFKPK(inv_provedrs,deleted," = "," AND") 
        inv_provedrs.codpai = deleted.codpai
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete glb_mtpaises because inv_provedrs exists.'
       )) 


;

CREATE TRIGGER tU_glb_mtpaises UPDATE ON glb_mtpaises
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on glb_mtpaises
  -- ERwin Builtin Trigger
  -- glb_mtpaises  inv_provedrs on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f1d8", PARENT_OWNER="", PARENT_TABLE="glb_mtpaises"
    -- CHILD_OWNER="", CHILD_TABLE="inv_provedrs"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbmtpaises1", FK_COLUMNS="codpai"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codpai = deleted.codpai
    ) AND
    EXISTS (
      SELECT * FROM inv_provedrs
      WHERE
        --  %JoinFKPK(inv_provedrs,deleted," = "," AND") 
        inv_provedrs.codpai = deleted.codpai
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update glb_mtpaises because inv_provedrs exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_glb_sucsxemp DELETE ON glb_sucsxemp
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on glb_sucsxemp
    -- ERwin Builtin Trigger
    -- glb_sucsxemp  inv_mbodegas on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000d7d0", PARENT_OWNER="", PARENT_TABLE="glb_sucsxemp"
    -- CHILD_OWNER="", CHILD_TABLE="inv_mbodegas"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbsucsxemp1", FK_COLUMNS="codsuc"
    WHEN (EXISTS (
      SELECT * FROM inv_mbodegas
      WHERE
        --  %JoinFKPK(inv_mbodegas,deleted," = "," AND") 
        inv_mbodegas.codsuc = deleted.codsuc
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete glb_sucsxemp because inv_mbodegas exists.'
       )) 


;

CREATE TRIGGER tU_glb_sucsxemp UPDATE ON glb_sucsxemp
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on glb_sucsxemp
  -- ERwin Builtin Trigger
  -- glb_sucsxemp  inv_mbodegas on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="000106a8", PARENT_OWNER="", PARENT_TABLE="glb_sucsxemp"
    -- CHILD_OWNER="", CHILD_TABLE="inv_mbodegas"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkglbsucsxemp1", FK_COLUMNS="codsuc"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codsuc = deleted.codsuc
    ) AND
    EXISTS (
      SELECT * FROM inv_mbodegas
      WHERE
        --  %JoinFKPK(inv_mbodegas,deleted," = "," AND") 
        inv_mbodegas.codsuc = deleted.codsuc
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update glb_sucsxemp because inv_mbodegas exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_his_mtransac DELETE ON his_mtransac
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on his_mtransac
    -- ERwin Builtin Trigger
    -- his_mtransac  his_dtransac on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00009f75", PARENT_OWNER="", PARENT_TABLE="his_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="his_dtransac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkhismtransac1", FK_COLUMNS="lnkhis"
    (DELETE FROM his_dtransac
      WHERE
        --  %JoinFKPK(his_dtransac,deleted," = "," AND") 
        his_dtransac.lnkhis = deleted.lnkhis
    ) 


;

CREATE TRIGGER tU_his_mtransac UPDATE ON his_mtransac
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on his_mtransac
  -- ERwin Builtin Trigger
  -- his_mtransac  his_dtransac on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f947", PARENT_OWNER="", PARENT_TABLE="his_mtransac"
    -- CHILD_OWNER="", CHILD_TABLE="his_dtransac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkhismtransac1", FK_COLUMNS="lnkhis"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.lnkhis = deleted.lnkhis
    ) AND
    EXISTS (
      SELECT * FROM his_dtransac
      WHERE
        --  %JoinFKPK(his_dtransac,deleted," = "," AND") 
        his_dtransac.lnkhis = deleted.lnkhis
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update his_mtransac because his_dtransac exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_categpro DELETE ON inv_categpro
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_categpro
    -- ERwin Builtin Trigger
    -- inv_categpro  inv_products on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000dabd", PARENT_OWNER="", PARENT_TABLE="inv_categpro"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvcategpro1", FK_COLUMNS="codcat"
    WHEN (EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codcat = deleted.codcat
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_categpro because inv_products exists.'
       )) 


;

CREATE TRIGGER tU_inv_categpro UPDATE ON inv_categpro
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_categpro
  -- ERwin Builtin Trigger
  -- inv_categpro  inv_products on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f9f7", PARENT_OWNER="", PARENT_TABLE="inv_categpro"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvcategpro1", FK_COLUMNS="codcat"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codcat = deleted.codcat
    ) AND
    EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codcat = deleted.codcat
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_categpro because inv_products exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_colorpro DELETE ON inv_colorpro
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_colorpro
    -- ERwin Builtin Trigger
    -- inv_colorpro  inv_products on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000c26a", PARENT_OWNER="", PARENT_TABLE="inv_colorpro"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvcolorpro1", FK_COLUMNS="codcol"
    WHEN (EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codcol = deleted.codcol
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_colorpro because inv_products exists.'
       )) 


;

CREATE TRIGGER tU_inv_colorpro UPDATE ON inv_colorpro
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_colorpro
  -- ERwin Builtin Trigger
  -- inv_colorpro  inv_products on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f19c", PARENT_OWNER="", PARENT_TABLE="inv_colorpro"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvcolorpro1", FK_COLUMNS="codcol"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codcol = deleted.codcol
    ) AND
    EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codcol = deleted.codcol
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_colorpro because inv_products exists.'
       )) 


;


CREATE TRIGGER trginsertarproducto INSERT ON inv_dtransac
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ))
;







CREATE TRIGGER trgupdestadoproducto UPDATE OF 
                                    estado ON inv_dtransac
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ))
;







CREATE TRIGGER trgupdcanuniproducto UPDATE OF 
                                    canuni ON inv_dtransac
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ))
;







CREATE TRIGGER trgcupdpreuniproducto UPDATE OF 
                                     preuni ON inv_dtransac
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ))
;







CREATE TRIGGER trgeliminarproducto DELETE ON inv_dtransac
  REFERENCING OLD AS pre
  FOR EACH ROW
(
        execute procedure "sistemas".sptsaldosxproducto(pre.codemp 
    ,pre.codsuc ,pre.codbod ,pre.tipmov ,pre.aniotr ,pre.mestra ,pre.cditem 
    ,pre.tipope ,pre.codabr ,pre.preuni ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_masignac DELETE ON inv_masignac
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_masignac
    -- ERwin Builtin Trigger
    -- inv_masignac  inv_dasignac on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="0000a1f1", PARENT_OWNER="", PARENT_TABLE="inv_masignac"
    -- CHILD_OWNER="", CHILD_TABLE="inv_dasignac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvasignac1", FK_COLUMNS="lnkasi"
    (DELETE FROM inv_dasignac
      WHERE
        --  %JoinFKPK(inv_dasignac,deleted," = "," AND") 
        inv_dasignac.lnkasi = deleted.lnkasi
    ) 


;

CREATE TRIGGER tU_inv_masignac UPDATE ON inv_masignac
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_masignac
  -- ERwin Builtin Trigger
  -- inv_masignac  inv_dasignac on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000fb84", PARENT_OWNER="", PARENT_TABLE="inv_masignac"
    -- CHILD_OWNER="", CHILD_TABLE="inv_dasignac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvasignac1", FK_COLUMNS="lnkasi"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.lnkasi = deleted.lnkasi
    ) AND
    EXISTS (
      SELECT * FROM inv_dasignac
      WHERE
        --  %JoinFKPK(inv_dasignac,deleted," = "," AND") 
        inv_dasignac.lnkasi = deleted.lnkasi
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_masignac because inv_dasignac exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_morden DELETE ON inv_morden
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_morden
    -- ERwin Builtin Trigger
    -- inv_morden  inv_dorden on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000ccc7", PARENT_OWNER="", PARENT_TABLE="inv_morden"
    -- CHILD_OWNER="", CHILD_TABLE="inv_dorden"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvdorden", FK_COLUMNS="lnkord"
    WHEN (EXISTS (
      SELECT * FROM inv_dorden
      WHERE
        --  %JoinFKPK(inv_dorden,deleted," = "," AND") 
        inv_dorden.lnkord = deleted.lnkord
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_morden because inv_dorden exists.'
       )) 


;

CREATE TRIGGER tU_inv_morden UPDATE ON inv_morden
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_morden
  -- ERwin Builtin Trigger
  -- inv_morden  inv_dorden on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f92a", PARENT_OWNER="", PARENT_TABLE="inv_morden"
    -- CHILD_OWNER="", CHILD_TABLE="inv_dorden"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvdorden", FK_COLUMNS="lnkord"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.lnkord = deleted.lnkord
    ) AND
    EXISTS (
      SELECT * FROM inv_dorden
      WHERE
        --  %JoinFKPK(inv_dorden,deleted," = "," AND") 
        inv_dorden.lnkord = deleted.lnkord
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_morden because inv_dorden exists.'
       )) 


;


CREATE TRIGGER trgupdestadomovto UPDATE OF 
                                 estado ON inv_mtransac
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.estado 
    = pos.estado  where (lnktra = pos.lnktra ) )
;







CREATE TRIGGER trgeliminarmovto DELETE ON inv_mtransac
  REFERENCING OLD AS pre
  FOR EACH ROW
(
        delete from "sistemas".inv_dtransac  where (lnktra = 
    pre.lnktra ) )
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_ordentra DELETE ON inv_ordentra
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_ordentra
    -- ERwin Builtin Trigger
    -- inv_ordentra  inv_masignac on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000cc78", PARENT_OWNER="", PARENT_TABLE="inv_ordentra"
    -- CHILD_OWNER="", CHILD_TABLE="inv_masignac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_29", FK_COLUMNS="lnkord"
    WHEN (EXISTS (
      SELECT * FROM inv_masignac
      WHERE
        --  %JoinFKPK(inv_masignac,deleted," = "," AND") 
        inv_masignac.lnkord = deleted.lnkord
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_ordentra because inv_masignac exists.'
       )) 


;

CREATE TRIGGER tU_inv_ordentra UPDATE ON inv_ordentra
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_ordentra
  -- ERwin Builtin Trigger
  -- inv_ordentra  inv_masignac on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f66d", PARENT_OWNER="", PARENT_TABLE="inv_ordentra"
    -- CHILD_OWNER="", CHILD_TABLE="inv_masignac"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="R_29", FK_COLUMNS="lnkord"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.lnkord = deleted.lnkord
    ) AND
    EXISTS (
      SELECT * FROM inv_masignac
      WHERE
        --  %JoinFKPK(inv_masignac,deleted," = "," AND") 
        inv_masignac.lnkord = deleted.lnkord
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_ordentra because inv_masignac exists.'
       )) 


;


CREATE TRIGGER trginsertarempaque INSERT ON inv_products
  REFERENCING NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ))
;







CREATE TRIGGER trgupdatepreciominimo UPDATE OF 
                                     premin ON inv_products
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
(
        insert into "sistemas".his_preciosm (lnkpre,cditem,preant,
    premin,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.premin ,
    pos.premin ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ))
;







CREATE TRIGGER trgupdateunimed UPDATE OF 
                               unimed ON inv_products
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
(
        execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ))
;







CREATE TRIGGER trgupdateproductos UPDATE OF 
                                  fulcam ON inv_products
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
(
        insert into "sistemas".his_products (lnkpro,cditem,codcat,
    subcat,codcol,codprv,codmed,unimed,dsitem,codabr,premin,estado,status,
    userid,fecsis,horsis,usract,fecact,horact)  values (0 ,pre.cditem 
    ,pre.codcat ,pre.subcat ,pre.codcol ,pre.codprv ,pre.codmed ,pre.unimed 
    ,pre.dsitem ,pre.codabr ,pre.premin ,pre.estado ,pre.status ,pre.userid 
    ,pre.fecsis ,pre.horsis ,USER ,CURRENT year to fraction(3) ,CURRENT 
    hour to second ))
;







CREATE TRIGGER trgupdatepreciosugerido UPDATE OF 
                                       presug ON inv_products
  REFERENCING OLD AS pre NEW AS pos
  FOR EACH ROW
(
        insert into "sistemas".his_precioss (lnkpre,cditem,preant,
    presug,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.presug ,
    pos.presug ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ))
;









create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_products DELETE ON inv_products
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_products
    -- ERwin Builtin Trigger
    -- inv_products  inv_epqsxpro on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00021804", PARENT_OWNER="", PARENT_TABLE="inv_products"
    -- CHILD_OWNER="", CHILD_TABLE="inv_epqsxpro"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvproducts3", FK_COLUMNS="cditem"
    (DELETE FROM inv_epqsxpro
      WHERE
        --  %JoinFKPK(inv_epqsxpro,deleted," = "," AND") 
        inv_epqsxpro.cditem = deleted.cditem
    ),

    -- ERwin Builtin Trigger
    -- inv_products  his_preciosm on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="inv_products"
    -- CHILD_OWNER="", CHILD_TABLE="his_preciosm"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvproducts2", FK_COLUMNS="cditem"
    (DELETE FROM his_preciosm
      WHERE
        --  %JoinFKPK(his_preciosm,deleted," = "," AND") 
        his_preciosm.cditem = deleted.cditem
    ),

    -- ERwin Builtin Trigger
    -- inv_products  his_precioss on parent delete cascade 
    -- ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="inv_products"
    -- CHILD_OWNER="", CHILD_TABLE="his_precioss"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvproducts4", FK_COLUMNS="cditem"
    (DELETE FROM his_precioss
      WHERE
        --  %JoinFKPK(his_precioss,deleted," = "," AND") 
        his_precioss.cditem = deleted.cditem
    ) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_provedrs DELETE ON inv_provedrs
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_provedrs
    -- ERwin Builtin Trigger
    -- inv_provedrs  inv_products on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000d966", PARENT_OWNER="", PARENT_TABLE="inv_provedrs"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvprovedrs1", FK_COLUMNS="codprv"
    WHEN (EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codprv = deleted.codprv
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_provedrs because inv_products exists.'
       )) 


;

CREATE TRIGGER tU_inv_provedrs UPDATE ON inv_provedrs
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_provedrs
  -- ERwin Builtin Trigger
  -- inv_provedrs  inv_products on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="000106e8", PARENT_OWNER="", PARENT_TABLE="inv_provedrs"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvprovedrs1", FK_COLUMNS="codprv"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.codprv = deleted.codprv
    ) AND
    EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.codprv = deleted.codprv
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_provedrs because inv_products exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_inv_unimedid DELETE ON inv_unimedid
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on inv_unimedid
    -- ERwin Builtin Trigger
    -- inv_unimedid  inv_products on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000d0a0", PARENT_OWNER="", PARENT_TABLE="inv_unimedid"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvunimedid1", FK_COLUMNS="unimed"
    WHEN (EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.unimed = deleted.unimed
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete inv_unimedid because inv_products exists.'
       )) 


;

CREATE TRIGGER tU_inv_unimedid UPDATE ON inv_unimedid
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on inv_unimedid
  -- ERwin Builtin Trigger
  -- inv_unimedid  inv_products on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000f718", PARENT_OWNER="", PARENT_TABLE="inv_unimedid"
    -- CHILD_OWNER="", CHILD_TABLE="inv_products"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="fkinvunimedid1", FK_COLUMNS="unimed"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.unimed = deleted.unimed
    ) AND
    EXISTS (
      SELECT * FROM inv_products
      WHERE
        --  %JoinFKPK(inv_products,deleted," = "," AND") 
        inv_products.unimed = deleted.unimed
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update inv_unimedid because inv_products exists.'
       )) 


;




create procedure erwin_raise_except(err int,msg varchar(255))
raise exception err,0,msg;
end procedure;

CREATE TRIGGER tD_mest DELETE ON mest
  REFERENCING OLD AS DELETED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- DELETE trigger on mest
    -- ERwin Builtin Trigger
    -- mest  bproc on parent delete restrict 
  -- ERWIN_RELATION:CHECKSUM="0000af85", PARENT_OWNER="", PARENT_TABLE="mest"
    -- CHILD_OWNER="", CHILD_TABLE="bproc"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="r244_873", FK_COLUMNS="est_id"
    WHEN (EXISTS (
      SELECT * FROM bproc
      WHERE
        --  %JoinFKPK(bproc,deleted," = "," AND") 
        bproc.est_id = deleted.est_id
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot delete mest because bproc exists.'
       )) 


;

CREATE TRIGGER tU_mest UPDATE ON mest
  REFERENCING OLD AS DELETED NEW AS INSERTED
  FOR EACH ROW
-- ERwin Builtin Trigger
-- UPDATE trigger on mest
  -- ERwin Builtin Trigger
  -- mest  bproc on parent update restrict 
  -- ERWIN_RELATION:CHECKSUM="0000e058", PARENT_OWNER="", PARENT_TABLE="mest"
    -- CHILD_OWNER="", CHILD_TABLE="bproc"
    -- P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    -- FK_CONSTRAINT="r244_873", FK_COLUMNS="est_id"
  WHEN(
    NOT (
        -- %JoinPKPK(inserted,deleted," = "," AND") 
        inserted.est_id = deleted.est_id
    ) AND
    EXISTS (
      SELECT * FROM bproc
      WHERE
        --  %JoinFKPK(bproc,deleted," = "," AND") 
        bproc.est_id = deleted.est_id
    ))
       (EXECUTE PROCEDURE erwin_raise_except(
         -746,
         'Cannot update mest because bproc exists.'
       )) 


;

