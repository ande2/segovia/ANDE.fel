{
invmae005.4gl 
Mynor Ramirez
Mantenimiento de unidades de medida
}

-- Definicion de variables globales 

GLOBALS "invglo005.4gl"
DEFINE existe     SMALLINT,
       medidabase VARCHAR(10)

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilo y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae005.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae005_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae005_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae005a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invmae005",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Obteniendo parametro de unidad de medida base
  CALL librut003_parametros(10,1)
  RETURNING existe,medidabase
  IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " Parametro de unidad de medida base no existe registrado.\n Definir parametro antes de ingresar unidades de medida.",
    "stop")
    CLOSE WINDOW wing001a
    RETURN
  END IF

  -- Desplegando unidad de medida base 
  CALL librut001_dpelement("medidabase",medidabase) 

  -- Menu de opciones
  MENU " Unidades de Medida"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de unidades de medida."
    CALL invqbe005_unimedid(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva tupo de medida."
    LET savedata = invmae005_unimedid(1) 
   COMMAND "Modificar"
    " Modificacion de una unidad de medida existente."
    CALL invqbe005_unimedid(2) 
   COMMAND "Borrar"
    " Eliminacion de una unidad de medida existente."
    CALL invqbe005_unimedid(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae005_unimedid(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 


 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae005_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nommed,
                w_mae_pro.nomabr,
                w_mae_pro.factor 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nommed  
    --Verificando nombre del unidad de medida 
    IF (LENGTH(w_mae_pro.nommed)=0) THEN
       ERROR "Error: nombre de la unidad de medida invalido, VERIFICA"
       LET w_mae_pro.nommed = NULL
       NEXT FIELD nommed  
    END IF

    -- Verificando que no exista otra unidad de medida con el mismo nombre
    SELECT UNIQUE (a.unimed)
     FROM  inv_unimedid a
     WHERE (a.unimed != w_mae_pro.unimed) 
       AND (a.nommed  = w_mae_pro.nommed) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra unidad de medida con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nommed
     END IF 

   AFTER FIELD nomabr  
    --Verificando abreviatura
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: abreviatura invalida, VERIFICA"
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

    -- Verificando que no exista otra unidad de medida con la misma abreviatura
    SELECT UNIQUE (a.unimed)
     FROM  inv_unimedid a
     WHERE (a.unimed != w_mae_pro.unimed) 
       AND (a.nomabr  = w_mae_pro.nomabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra unidad de medida con la misma abreviatura, VERIFICA ...",
        "information")
        NEXT FIELD nomabr
     END IF 

   AFTER FIELD factor
    --Verificando factor
    IF w_mae_pro.factor IS NULL THEN 
       LET w_mae_pro.factor = 1 
       DISPLAY BY NAME w_mae_pro.factor 
    END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nommed IS NULL THEN 
       NEXT FIELD nommed
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.factor IS NULL THEN 
       LET w_mae_pro.factor = 1 
       DISPLAY BY NAME w_mae_pro.factor 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae005_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando unidad de medida
    CALL invmae005_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae005_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un unidad de medida

FUNCTION invmae005_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando unidad de medida ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.unimed),0)
    INTO  w_mae_pro.unimed
    FROM  inv_unimedid a
    IF (w_mae_pro.unimed IS NULL) THEN
       LET w_mae_pro.unimed = 1
    ELSE 
       LET w_mae_pro.unimed = (w_mae_pro.unimed+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_unimedid   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.unimed 

   --Asignando el mensaje 
   LET msg = "Unidad de Medid (",w_mae_pro.unimed USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_unimedid
   SET    inv_unimedid.*        = w_mae_pro.*
   WHERE  inv_unimedid.unimed = w_mae_pro.unimed 

   --Asignando el mensaje 
   LET msg = "Unidad de Medida (",w_mae_pro.unimed USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM inv_unimedid 
   WHERE (inv_unimedid.unimed = w_mae_pro.unimed)

   --Asignando el mensaje 
   LET msg = "Unidad de Medida (",w_mae_pro.unimed USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae005_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae005_inival(i)
 DEFINE i SMALLINT

 -- Verificando unidad de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.unimed = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.nommed,w_mae_pro.factor 
 DISPLAY BY NAME w_mae_pro.unimed,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
