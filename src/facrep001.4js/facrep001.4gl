{ 
Fecha    : Enero 2011
Programo : facrep001.4gl 
Objetivo : Reporte de corte de ventas
}

DATABASE segovia 

-- Definicion de variables globales 

DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_pos RECORD LIKE fac_puntovta.*,
       w_datos   RECORD
        numpos   LIKE fac_vicortes.numpos,
        codemp   LIKE fac_vicortes.codemp,
        cajero   LIKE fac_vicortes.cajero, 
        feccor   DATE
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       onlast    SMALLINT, 
       existe    SMALLINT,
       repgen    SMALLINT,
       filename  STRING,
       pipeline  STRING,
       fcajero   STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facrep001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep001_corteventas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep001_corteventas()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT
        

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "facrep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep001",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrep001.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_cbxempresas()
  CALL librut003_cbxpuntosventa()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.feccor,
                 w_datos.numpos,
                 w_datos.codemp,
                 w_datos.cajero
                 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcajero = GET_FLDBUF(w_datos.cajero)

     -- Verificando datos 
     IF w_datos.numpos IS NULL OR 
        w_datos.codemp IS NULL OR 
        w_datos.feccor IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD numpos
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcajero = GET_FLDBUF(w_datos.cajero)

     -- Verificando datos 
     IF w_datos.numpos IS NULL OR 
        w_datos.codemp IS NULL OR 
        w_datos.feccor IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD numpos
     END IF 
     EXIT INPUT 

    AFTER FIELD feccor 
     -- Verificando fecha de corte
     IF w_datos.feccor IS NULL THEN
        ERROR "Error: debe de ingresarse la fecha de corte a listar."
        NEXT FIELD feccor
     END IF
  
     
    ON CHANGE numpos
     -- Obteniendo datos del punto de venta
     INITIALIZE w_mae_pos.* TO NULL
     CALL librut003_bpuntovta(w_datos.numpos)
     RETURNING w_mae_pos.*,existe

     -- Limpiando combos de empresa y cajero
     LET w_datos.codemp = NULL
     LET w_datos.cajero = NULL
     CLEAR codemp,cajero 

    ON CHANGE codemp 
     -- Obteniendo datos de la empresa
     INITIALIZE w_mae_emp.* TO NULL
     CALL librut003_bempresa(w_datos.codemp)
     RETURNING w_mae_emp.*,existe

     -- Limpiando combo de cajero
     LET w_datos.cajero = NULL
     CLEAR cajero 

    AFTER FIELD numpos
     -- Verificando bodega 
     IF w_datos.numpos IS NULL THEN
        ERROR "Error: debe de seleccionarse el punto de venta listar."
        NEXT FIELD numpos
     END IF

    AFTER FIELD codemp 
     -- Verificando bodega 
     IF w_datos.codemp IS NULL THEN
        ERROR "Error: debe de seleccionarse la empresa listar."
        NEXT FIELD codemp
     END IF

    BEFORE FIELD cajero
     -- Llenando combobox de cajeros del punto de venta y la empresa
     LET res = librut003_cbxcajeroscorte(w_datos.numpos,w_datos.codemp,w_datos.feccor,0)

    
    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.codemp IS NULL OR 
        w_datos.feccor IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD numpos
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep001_gencorte()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para generar el reporte de corte de ventas 

FUNCTION facrep001_gencorte()
 DEFINE w_mae_fac RECORD LIKE fac_mtransac.*,
        credito   DEC(14,2),
        contado   DEC(14,2),
        s_query   STRING, 
        wcondcaj  STRING   
 DEFINE myHandler om.SaxDocumentHandler 

 -- Verificando si el reporte es por cajero y no general
 LET wcondcaj = NULL
 LET repgen   = TRUE
 IF (LENGTH(w_datos.cajero)>0) THEN 
    LET repgen   = FALSE 
    LET wcondcaj = " AND x.userid = '",w_datos.cajero CLIPPED,"' "
 END IF
 
 -- Preparando condiciones del reporte 
 LET s_query = "SELECT x.* ",
                "FROM  fac_mtransac x ",
                "WHERE x.numpos = ",w_datos.numpos, 
                 " AND x.codemp = ",w_datos.codemp,
                 --" AND x.feccor = '",w_datos.feccor,"' ",
                 " AND x.fecemi = '",w_datos.feccor,"' ",
                 wcondcaj CLIPPED, 
                " ORDER BY x.tipdoc,x.nserie,x.numdoc" 

 --DISPLAY "query ", s_query
 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_corte FROM s_query
 DECLARE c_corte CURSOR FOR s_corte
 LET existe  = FALSE
 LET onlast  = FALSE 

 LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
 FOREACH c_corte INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Seleccionando fonts para impresora epson
     CALL librut001_fontsprn(pipeline,"epson")
     RETURNING fnt.*
   
     -- Creando tabla temporal para totales por tipo de documento 
     CREATE TEMP TABLE tmp_totdocto 
     (
      codemp  SMALLINT,
      feccor  DATE,    
      tipdoc  SMALLINT,
      cajero  CHAR(15), 
      efecti  DEC(14,2),
      cheque  DEC(14,2),
      tarcre  DEC(14,2),
      ordcom  DEC(14,2),
      depmon  DEC(14,2),
      totdoc  DEC(14,2),
      credit  DEC(14,2),
      contad  DEC(14,2) 
     ) WITH NO LOG 

     -- Iniciando la impresion
     START REPORT facrep001_impcorvta --TO filename
     LET existe = TRUE
  END IF

  -- Verificando si factura esta anulada para poner valores a cero
  IF (w_mae_fac.estado="A") THEN
     LET w_mae_fac.efecti   = 0 
     LET w_mae_fac.cheque   = 0 
     LET w_mae_fac.tarcre   = 0 
     LET w_mae_fac.ordcom   = 0 
     LET w_mae_fac.depmon   = 0 
     LET w_mae_fac.subtot   = 0 
     LET w_mae_fac.totdoc   = 0 
     LET credito            = 0 
     LET contado            = 0 
  END IF 

  -- Totalizando credito y contado
  IF w_mae_fac.credit THEN
     LET credito = w_mae_fac.totdoc 
     LET contado = 0
  ELSE
     LET contado = w_mae_fac.totdoc 
     LET credito = 0
  END IF 

  -- Llenado el reporte
  OUTPUT TO REPORT facrep001_impcorvta(w_mae_fac.*,credito,contado) 
 END FOREACH
 CLOSE c_corte
 FREE  c_corte

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep001_impcorvta   

    -- Enviando reporte al destino seleccionado
    --CALL librut001_enviareporte(filename,pipeline,"Corte de Ventas")
    LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")

    -- Dropeando tabla temporal
    DROP TABLE tmp_totdocto

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep001_impcorvta(imp1,credito,contado)
 DEFINE imp1          RECORD LIKE fac_mtransac.*,
        w_tip_doc     RECORD LIKE fac_tipodocs.*,
        w             RECORD 
         tipdoc       SMALLINT,
         nomdoc       CHAR(18),
         efecti       DEC(14,2),
         cheque       DEC(14,2),
         tarcre       DEC(14,2),
         ordcom       DEC(14,2),
         depmon       DEC(14,2),
         totdoc       DEC(14,2),
         credit       DEC(14,2),
         contad       DEC(14,2)
        END RECORD, 
        z             RECORD 
         cajero       CHAR(15), 
         nombre       CHAR(30),
         efecti       DEC(14,2),
         cheque       DEC(14,2),
         tarcre       DEC(14,2),
         ordcom       DEC(14,2),
         depmon       DEC(14,2)
        END RECORD, 
        u             RECORD 
         cajero       CHAR(30),
         totcob       DEC(14,2)
        END RECORD,
        wnomemi       LIKE fac_emitacre.nomemi,
        wnumvou       LIKE fac_tarjetas.numvou,
        credito       DEC(14,2),
        contado       DEC(14,2),
        tefecti       DEC(14,2),
        tcheque       DEC(14,2),
        ttarcre       DEC(14,2), 
        tordcom       DEC(14,2), 
        tdepmon       DEC(14,2), 
        totscj        DEC(14,2), 
        minimo        INTEGER,
        haydoc        SMALLINT, 
        totcaj        SMALLINT,
        tottar        SMALLINT,
        maximo        INTEGER, 
        lineat        CHAR(100), 
        linea         CHAR(256), 
        exis,i        SMALLINT, 
        msg           CHAR(14),
        nestado       CHAR(3)

  OUTPUT
   PAGE   LENGTH 88
   LEFT  MARGIN 0
   RIGHT MARGIN 2
   
         --PAGE   LENGTH 88
         --TOP    MARGIN 3 
         --BOTTOM MARGIN 5

  FORMAT 
   PAGE HEADER
    LET linea = "--------------------------------------------------",
                "--------------------------------------------------",
                "--------------------------------------"
                
                

    -- Configurando impresora 
   -- PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.cmp CLIPPED,fnt.p12 CLIPPED

    -- Imprimiendo encabezado 
    PRINT COLUMN   1,"Facturacion",
          COLUMN  58,"*** C O R T E  D E  V E N T A S ***", 
          COLUMN 120,PAGENO USING "Pagina: <<"
    PRINT COLUMN   1,"Facrep001",
          COLUMN  62,"FECHA DE VENTA (",w_datos.feccor USING "dd/mmm/yyyy",")",
          COLUMN 120,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    -- Verificando si reporte es por cajero
    IF NOT repgen THEN 
       PRINT COLUMN   1,"Cajero (",
                        --fnt.tbl CLIPPED,
                        fcajero CLIPPED, 
                        --fnt.fbl CLIPPED,
                        ")",
             COLUMN 120,"Hora  : ",TIME 
    ELSE
       PRINT COLUMN   1, --fnt.tbl CLIPPED,
                        "GENERAL",
                        --fnt.fbl CLIPPED,
             COLUMN 120,"Hora  : ",TIME 
    END IF

    PRINT COLUMN   1,"Punto de Venta (",
                     --fnt.tbl CLIPPED,
                     w_mae_pos.nompos CLIPPED,
                     --fnt.fbl CLIPPED,
                     ")" 

   
     PRINT linea CLIPPED 
    IF NOT onlast THEN
     PRINT "Serie      Numero          EFECTIVO         CHEQUE        TARJETA       ORDEN DE      DEPOSITO      ",
           "T O T A L   C R E D I T O  C O N T A D O" 
     PRINT "           Documento                                      CREDITO       COMPRA        MONETARIO     ",
           "V E N T A    "
     PRINT "Vouchers Tarjetas de Credito                                   ",
           "                                  "
    ELSE
     PRINT "Serie      Numero          EFECTIVO         CHEQUE        TARJETA       ORDEN DE      DEPOSITO      ",
           "T O T A L   C R E D I T O  C O N T A D O"
     PRINT "           Documento                                      CREDITO       COMPRA        MONETARIO     ",
           "V E N T A    "
     PRINT ""
    END IF 
    PRINT linea CLIPPED 
    PRINT --fnt.twd CLIPPED,
          "Empresa: ",w_mae_emp.nomemp CLIPPED --,
          --fnt.fwd CLIPPED
    SKIP 1 LINES 

   BEFORE GROUP OF imp1.tipdoc
    -- Obteniendo datos del tipo de documento 
    INITIALIZE w_tip_doc.* TO NULL
    CALL librut003_btipodoc(imp1.tipdoc)
    RETURNING w_tip_doc.*,exis
    PRINT --fnt.tbl CLIPPED,
          "Documento: (",w_tip_doc.nomdoc CLIPPED,")" --,
          --fnt.fbl CLIPPED
    SKIP 1 LINES 

   ON EVERY ROW
    -- Verificando si documento esta anulado
    LET nestado = NULL
    IF (imp1.estado="A") THEN
       LET nestado = "(A)"
    END IF 

    -- Imprimiendo datos 
    PRINT imp1.nserie                                              , --4 SPACES,
          imp1.numdoc                                              ,2 SPACES,
          imp1.efecti                         USING "##,###,##&.&&",2 SPACES,
          imp1.cheque                         USING "##,###,##&.&&",2 SPACES,
          imp1.tarcre                         USING "##,###,##&.&&",2 SPACES,
          imp1.ordcom                         USING "##,###,##&.&&",2 SPACES,
          imp1.depmon                         USING "##,###,##&.&&",2 SPACES,
          imp1.totdoc                         USING "##,###,##&.&&",2 SPACES,
          credito                             USING "##,###,##&.&&",2 SPACES,
          contado                             USING "##,###,##&.&&",2 SPACES,
          nestado             

    -- Imprimiendo vouchers de tarjetas de credito
    IF (imp1.tarcre>0) THEN
       DECLARE ctarj CURSOR FOR
       SELECT b.nomemi,a.numvou
        FROM  fac_tarjetas a,fac_emitacre b 
        WHERE (a.lnktra = imp1.lnktra)
          AND (b.codemi = a.codemi)  
        ORDER BY 1,2

        LET tottar = 0
        FOREACH ctarj INTO wnomemi,wnumvou
         PRINT wnomemi CLIPPED," ",wnumvou USING "<<<<<<<<<<"," ";   
         LET tottar = (tottar+1)    
        END FOREACH
        CLOSE ctarj
        FREE  ctarj

        IF (tottar>0) THEN
           PRINT "" 
        END IF 
     END IF 

     -- Insertando totales en tabla temporal
     INSERT INTO tmp_totdocto
     VALUES (imp1.codemp,imp1.feccor,imp1.tipdoc,imp1.userid,
             imp1.efecti,imp1.cheque,imp1.tarcre,imp1.ordcom,
             imp1.depmon,imp1.totdoc,credito,contado)

   AFTER GROUP OF imp1.tipdoc
    -- Totalizando por tipo de documento 
    --PRINT fnt.tbl CLIPPED
    PRINT COLUMN   1,"TOTAL ...",
          COLUMN  23,GROUP SUM(imp1.efecti)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(imp1.cheque)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(imp1.tarcre)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(imp1.ordcom)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(imp1.depmon)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(imp1.totdoc)   USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(credito)       USING "##,###,##&.&&",2 SPACES,
                     GROUP SUM(contado)       USING "##,###,##&.&&"
    --PRINT fnt.fbl CLIPPED

   ON LAST ROW
    -- Totalizando
    --PRINT fnt.tbl CLIPPED
    PRINT COLUMN   1,"TOTAL CORTE -->",
          COLUMN  23,SUM(imp1.efecti)        USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.cheque)        USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.tarcre)        USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.ordcom)        USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.depmon)        USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.totdoc)        USING "##,###,##&.&&",2 SPACES,
                     SUM(credito)            USING "##,###,##&.&&",2 SPACES,
                     SUM(contado)            USING "##,###,##&.&&"
    --PRINT fnt.fbl CLIPPED

    -- Imprimiendo resumen cuando reporte es general
    IF repgen THEN
     LET onlast = TRUE
     SKIP TO TOP OF PAGE
     PRINT --fnt.twd CLIPPED,
     "R E S U  M E N" --, fnt.fwd CLIPPED 
     PRINT linea CLIPPED 

     -- Seleccionando tipos de documento del corte de tabla temporal
     DECLARE cres CURSOR FOR
     SELECT x.tipdoc,
            y.nomdoc,
            SUM(x.efecti),
            SUM(x.cheque),
            SUM(x.tarcre),
            SUM(x.ordcom),
            SUM(x.depmon),
            SUM(x.totdoc),
            SUM(x.credit),
            SUM(x.contad)
      FROM  tmp_totdocto x,fac_tipodocs y
      WHERE (x.tipdoc = y.tipdoc)
      GROUP BY 1,2
      ORDER BY 1 
      FOREACH cres INTO w.* 
       -- Seleccionando documentos maximo y minimo 
       {CALL librut003_maxmindoc(w_mae_fac.codemp,
                                w.tipdoc,
                                w_mae_fac.gruvta,
                                w_mae_fac.feccor,
                                0)
       RETURNING minimo,maximo,haydoc}
       LET haydoc = TRUE 

       IF haydoc THEN
        -- Totalizando por  tipo de documento 
        PRINT COLUMN   1,w.nomdoc                   ,
              COLUMN  23,w.efecti  USING "##,###,##&.&&",2 SPACES,
                         w.cheque  USING "##,###,##&.&&",2 SPACES,
                         w.tarcre  USING "##,###,##&.&&",2 SPACES,
                         w.ordcom  USING "##,###,##&.&&",2 SPACES,
                         w.depmon  USING "##,###,##&.&&",2 SPACES,
                         w.totdoc  USING "##,###,##&.&&",2 SPACES,
                         w.credit  USING "##,###,##&.&&",2 SPACES,
                         w.contad  USING "##,###,##&.&&",2 SPACES,
                         minimo    USING "<<<<<<<<<<", 
                         maximo    USING "<<<<<<<<<<"
       END IF 
      END FOREACH
      CLOSE cres
      FREE  cres 

     -- Totalizando resumen
     PRINT linea CLIPPED 
     PRINT COLUMN   1,"TOTAL CORTE -->",
           COLUMN  23,SUM(imp1.efecti)        USING "##,###,##&.&&",2 SPACES,
                      SUM(imp1.cheque)        USING "##,###,##&.&&",2 SPACES,
                      SUM(imp1.tarcre)        USING "##,###,##&.&&",2 SPACES,
                      SUM(imp1.ordcom)        USING "##,###,##&.&&",2 SPACES,
                      SUM(imp1.depmon)        USING "##,###,##&.&&",2 SPACES,
                      SUM(imp1.totdoc)        USING "##,###,##&.&&",2 SPACES,
                      SUM(credito)            USING "##,###,##&.&&",2 SPACES,
                      SUM(contado)            USING "##,###,##&.&&" 

    {
     -- Imprimiendo resumen por cajero  
     SKIP 2 LINES
     PRINT COLUMN 54,"*** RESUMEN POR CAJERO ***"
     LET lineat = "__________________________________________________",
                  "______________________________________________"

     PRINT COLUMN 22,lineat 
     PRINT COLUMN 22,"NOMBRE DEL CAJERO                ",
                     "TOTAL        TOTAL     TOTAL    TOTAL      ",
                     "T O T A L  "
     PRINT COLUMN 22,"                                 ",
                     "EFECTIVO     CHEQUE    TARJETA  ORDEN/COM  ",
                     "           "
     PRINT COLUMN 22,lineat 

     DECLARE crcaj CURSOR FOR
     SELECT x.cajero, 
            y.nomusr,
            SUM(x.efecti),
            SUM(x.cheque),
            SUM(x.tarcre),
            SUM(x.ordcom),
            SUM(x.depmon ,
            SUM(x.propin) 
      FROM  tmp_totdocto x,glb_maeusers y,tax_tipodocs d
      WHERE (x.cajero = y.susrid) 
        AND (x.tipdoc = d.tipdoc)
        AND (d.arqueo = "S") 
      GROUP BY 1,2
      ORDER BY 2

      LET tefecti = 0
      LET tcheque = 0
      LET ttarcre = 0 
      LET tordcom = 0 
      LET tdepmon = 0 
      LET tpropin = 0 
      FOREACH crcaj INTO z.*  
       -- Imprimiendo cajeros 
       PRINT COLUMN  22,z.nombre                    ,2 SPACES,
                        z.efecti  USING "##,##&.&&" ,2 SPACES,
                        z.cheque  USING "##,##&.&&" ,2 SPACES,
                        z.tarcre  USING "##,##&.&&" ,2 SPACES,
                        z.ordcom  USING "##,##&.&&" ,2 SPACES,
                        z.depmon  USING "##,##&.&&" ,2 SPACES,
                        (z.efecti+z.cheque+z.tarcre+z.ordcom)
                                  USING "##,##&.&&" ,2 SPACES,
                        ((z.efecti+z.cheque+z.tarcre+z.ordcom)-z.propin) 
                                  USING "##,##&.&&"  

       -- Totalizando
       LET tefecti = (tefecti+z.efecti)
       LET tcheque = (tcheque+z.cheque)
       LET ttarcre = (ttarcre+z.tarcre) 
       LET tordcom = (tordcom+z.ordcom) 
       LET tpropin = (tpropin+z.propin)
      END FOREACH
      CLOSE crcaj   
      FREE  crcaj 

     PRINT COLUMN 22,lineat
     PRINT COLUMN 22,"TOTAL CORTE DE CAJA -->",
           COLUMN 54,tefecti  USING "##,##&.&&",2 SPACES,
                     tcheque  USING "##,##&.&&",2 SPACES,
                     ttarcre  USING "##,##&.&&",2 SPACES,
                     tordcom  USING "##,##&.&&",2 SPACES,
                     (tefecti+tcheque+ttarcre+tordcom)
                              USING "##,##&.&&",2 SPACES,
                     ((tefecti+tcheque+ttarcre+tordcom)-tpropin) 
                              USING "##,##&.&&"
     PRINT COLUMN 22,lineat 

     -- Imprimiendo cajeros sin corte
     DECLARE cajsct CURSOR FOR
     SELECT b.nomusr,
            SUM(a.subtot+a.roomsv+a.propin)
      FROM  tax_facturas a,glb_maeusers b
      WHERE (a.codemp  = w_mae_fac.codemp) 
        AND (a.feccor IS NULL)
        AND (a.gruvta  = w_mae_fac.gruvta) 
        AND (a.gusrid  = b.susrid)
        AND (a.fecemi <= w_mae_fac.feccor) 
      GROUP BY 1
      ORDER BY 1

      LET totscj = 0
      LET totcaj = 0 
      FOREACH cajsct INTO u.*
       IF (totcaj=0) THEN
          SKIP 2 LINES
          PRINT COLUMN 52,"*** CAJEROS SIN CORTE ***"
          PRINT COLUMN 22,lineat
          PRINT COLUMN 22,"NOMBRE DEL CAJERO             TOTAL SIN CORTE"
          PRINT COLUMN 22,lineat
       END IF 

       -- Imprimiendo cajeros
       PRINT COLUMN  22,u.cajero                   ,6 SPACES,
                        u.totcob  USING "##,##&.&&" 

       -- Totalizando cobro
       LET totscj = (totscj+u.totcob)
       LET totcaj = (totcaj+1) 
      END FOREACH
      CLOSE cajsct
      FREE  cajsct

     IF (totcaj>0) THEN 
        PRINT COLUMN 22,lineat
        PRINT COLUMN 22,"TOTAL SIN CORTE -->",
              COLUMN 54,totscj USING "##,###,##&.&&" 
        PRINT COLUMN 22,lineat
     END IF } 

     -- Imprimiendo entrega del corte 
      SKIP TO TOP OF PAGE
     --PRINT fnt.twd CLIPPED
     PRINT "SE HACE CONSTAR QUE DESPUES DEL CORTE PRACTICADO, FUE ENTREGADO EL"
     PRINT "EFECTIVO  Y  DEMAS DOCUMENTOS  Y VALORES  QUE SIRVIERON DE BASE AL"
     PRINT "MISMO, DE LO CUAL SE FIRMA DE ENTERA CONFORMIDAD LA PRESENTE."
     SKIP 3 LINES
     PRINT COLUMN  5,"F",
           COLUMN 40,"F"
     PRINT COLUMN  5,"__________________________",
           COLUMN 40,"__________________________"
     PRINT COLUMN  5,"        RECEPTOR ",
           COLUMN 40,"   CONTADOR Y/O GERENTE"
     PRINT COLUMN  5,"        (ENTREGO)",
           COLUMN 40,"        (RECIBIO)"       
     SKIP 3 LINES 
     PRINT COLUMN  5,"F",
           COLUMN  5,"__________________________"
     PRINT COLUMN  5,"        CONTRALOR"
     PRINT COLUMN  5,"        (REVISO)" 
     SKIP 2 LINES
     PRINT "<< DESGLOCE DE MONEDA >>"
     PRINT "                           *** QUETZALES ***     *** DOLARES *** "
     SKIP 1 LINES 
     PRINT "BILLETES DE Q 100.00       ________________  US$ _______________" 
     SKIP 1 LINES
     PRINT "BILLETES DE Q  50.00       ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "BILLETES DE Q  20.00       ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "BILLETES DE Q  10.00       ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "BILLETES DE Q   5.00       ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE Q   1.00       ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE     0.50 Ctvs  ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE     0.25 Ctvs  ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE     0.10 Ctvs  ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE     0.05 Ctvs  ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "MONEDAS  DE     0.01 Ctvo  ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "TOTAL EFECTIVO       -->   ________________  US$ _______________" 
     SKIP 1 LINES 
     PRINT "TOTAL (Q+$)          -->   ________________"
     SKIP 1 LINES
     PRINT "TOTAL DOCUMENTOS     -->   ________________"
     SKIP 1 LINES
     PRINT "FALTANTE O SOBRANTE  -->   ________________  US$ _______________"
     SKIP 1 LINES
     --PRINT fnt.fwd CLIPPED
   END IF 

  PAGE TRAILER
   IF NOT onlast THEN
      SKIP 1 LINES
      PRINT "Nomenclatura: (A) Documento Anulado"
   ELSE 
      SKIP 2 LINES 
   END IF 
END REPORT
