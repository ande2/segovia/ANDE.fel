
DROP VIEW "sistemas".vis_ordentraint
;
create view "sistemas".vis_ordentraint (numpos,nombrepos,ordentrabajo,numlona,fechaorden,cliente,cantidad,clase,color,medidas,nombremedida,usuario,serie,documento,ordencompra,fechaofrecida,descripcion,hora) as
  select x0.numpos ,x5.nompos ,x0.lnkord ,x6.nuitem ,x0.fecord
    ,x4.nomcli ,x6.cantid ,x2.nomsub ,x3.nomcol ,((x6.xlargo
    || ' X ' ) || x6.yancho ) ,CASE WHEN (x6.medida = 1 )  THEN
    'PIES'  WHEN (x6.medida = 0 )  THEN 'METROS'  END ,x4.usrope
    ,x4.nserie ,x4.numdoc ,x4.ordcmp ,x0.fecofe ,x6.observ ,x0.horsis
    from "sistemas".inv_morden x0 ,"sistemas".fac_clientes x1 ,
    "sistemas".inv_subcateg x2 ,"sistemas".inv_colorpro x3 ,"sistemas"
    .fac_mtransac x4 ,"sistemas".fac_puntovta x5 ,"sistemas".inv_dorden
    x6 where (((((((((x1.codcli = x0.codcli ) AND (x2.subcat
    = x6.subcat ) ) AND (x3.codcol = x6.codcol ) ) AND (x4.lnktra
    = x0.lnktra ) ) AND (x5.numpos = x0.numpos ) ) AND (x6.tipord
    = 1 ) ) AND (x6.nofase IN (1 ,2 )) ) AND (x0.estado != 'A'
     ) ) AND (x6.lnkord = x0.lnkord ) ) ;


GRANT SELECT ON "sistemas".vis_ordentraint TO PUBLIC
