{ 
Fecha    : Febrero 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Impresion de facturacion 
}

-- Definicion de variables globales 
GLOBALS "invglb003.4gl" 

-- Subrutina para consultar/anular/imprimir facturacion 

FUNCTION facqbx001_facturacion(operacion)
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        titmenu             CHAR(15),
        qryres,msg,titqry   CHAR(80),
        wherestado          STRING,
        opc                 INTEGER ,
        regreso             SMALLINT

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET titmenu     = " Consultar"
         LET wherestado  = NULL
  WHEN 2 LET titmenu     = " Anular" 
		IF FGL_GETENV("LOGNAME") = "ximena" THEN
         LET wherestado  = " AND a.estado in ('V') "
		ELSE
         LET wherestado  = " AND a.estado in ('V') AND a.feccor IS NULL"
		END IF
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("labeld",0)
 CALL f.setElementHidden("compre",1)
  -- Menu de opciones
      -- Buscando datos 
      LET loop = TRUE 
      WHILE loop 
       -- Inicializando las variables 
       INITIALIZE qrytext,qrypart TO NULL
       LET int_flag = 0 
       CLEAR FORM

       -- Construyendo busqueda 
       CONSTRUCT BY NAME qrytext 
                      ON a.userid,
                         a.fecemi,
                         a.horemi,
                         a.estado, 
                         a.lnkasi,
                         a.lnkord

        ON ACTION cancel
         -- Salida
         LET loop = FALSE
         EXIT CONSTRUCT
       END CONSTRUCT
       IF NOT loop THEN
          EXIT WHILE
       END IF 

       -- Preparando la busqueda 
       ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

       -- Creando la busqueda 
       LET qrypart = " SELECT a.lnkasi ",
                     " FROM  inv_masignac a ",
                     " WHERE ",qrytext CLIPPED

       -- Declarando el cursor 
		 DISPLAY "' ",qrypart
       PREPARE estqbe001 FROM qrypart
       DECLARE c_asigna SCROLL CURSOR WITH HOLD FOR estqbe001  
       OPEN c_asigna 
       FETCH FIRST c_asigna INTO w_mae_tra.lnkasi
       IF (status = NOTFOUND) THEN
          INITIALIZE w_mae_tra.* TO NULL
          ERROR ""
          CALL fgl_winmessage(
          "Atencion",
          "No existen documentos con el criterio seleccionado.",
          --"No existen documentos con el criterio seleccionado.",
          "stop") 
       ELSE
          ERROR "" 

          -- Desplegando datos 
          CALL facqbx001_datos()

          -- Fetchando documentos 
          MENU titmenu 
           BEFORE MENU 
            -- Verificando tipo de operacion 
				CALL DIALOG.setActionActive("ver_especial",FALSE)
            --CASE (operacion)
             --WHEN 1 HIDE OPTION "Anular"
            --END CASE 

           COMMAND "Siguiente"
            "Visualiza el siguiente documento en la lista."
            FETCH NEXT c_asigna INTO w_mae_tra.lnkasi 
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos siguientes en lista.", 
                "information")
                FETCH LAST c_asigna INTO w_mae_tra.lnkasi
            END IF 

            -- Desplegando datos 
            CALL facqbx001_datos()

           COMMAND "Anterior"
            "Visualiza el documento anterior en la lista."
            FETCH PREVIOUS c_asigna INTO w_mae_tra.lnkasi
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos anteriores en lista.", 
                "information")
                FETCH FIRST c_asigna INTO w_mae_tra.lnkasi
             END IF

             -- Desplegando datos 
             CALL facqbx001_datos()


				ON ACTION ver_especial
					CALL inving003_ver_especial()

           COMMAND "Primero" 
            "Visualiza el primer documento en la lista."
            FETCH FIRST c_asigna INTO w_mae_tra.lnkasi
             -- Desplegando datos 
             CALL facqbx001_datos()

           COMMAND "Ultimo" 
            "Visualiza el ultimo documento en la lista."
            FETCH LAST c_asigna INTO w_mae_tra.lnkasi
             -- Desplegando datos
             CALL facqbx001_datos()

           COMMAND "Anular"
            "Permite anular el documento actual en pantalla."
            -- Verificando si documento ya fue anulado
            IF (w_mae_tra.estado="A") THEN
               CALL fgl_winmessage(
               "Atencion",
               "Documento ya fue anulado.",
               "stop")
               CONTINUE MENU 
       		END IF

       		-- Anulando movimiento
       		IF facqbx003_anular() THEN
          		CONTINUE MENU
       		END IF

            -- Desplegando datos
            CALL facqbx001_datos()

           COMMAND "Productos"
            "Permite visualizar el detalle completo de productos del documento."
            -- Verificando si no hay orden

           COMMAND "Consultar" 
            "Regresa a la pantalla de seleccion."
            EXIT MENU

           ON ACTION cancel
            LET loop = FALSE
            EXIT MENU

           COMMAND KEY(F4,CONTROL-E)
            EXIT MENU
          END MENU
        END IF     
       CLOSE c_asigna
      END WHILE
 -- Escondiendo campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("labeld",1)

 -- Habilitando tabla de productos
 CALL f.setElementHidden("tabla1",0)
 CALL f.setElementHidden("group4",1)
 CALL f.setElementHidden("group5",0)
 CALL f.setElementHidden("labelc",0)
 CALL f.setElementHidden("compre",1)

 -- Inicializando datos 
 CALL inving003_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del documento 

FUNCTION facqbx001_datos()
 DEFINE existe SMALLINT,
despliega RECORD
           fecord DATE,
           subcat INTEGER,
           codcol INTEGER,
           xlargo DECIMAL(10,2),
           yancho DECIMAL(10,2),
           medida INTEGER,
           nomcli VARCHAR(100)
         END RECORD

 -- Desplegando datos 
 CLEAR FORM
      CALL librut003_cbxsubcateg()
      CALL librut003_cbxcolores()


 -- Obteniendo datos del documento 
SELECT *  INTO w_mae_tra.* FROM inv_masignac

SELECT fecord, subcat , codcol ,
       xlargo, yancho, medida
  INTO despliega.fecord , despliega.subcat , despliega.codcol ,
                 despliega.xlargo , despliega.yancho , despliega.medida
  FROM inv_ordentra
 WHERE lnkord = w_mae_tra.lnkord

SELECT fac_mtransac.nomcli
  INTO despliega.nomcli
  FROM fac_mtransac , inv_ordentra
 WHERE fac_mtransac.lnktra = inv_ordentra.lnktra
        AND inv_ordentra.lnkord = w_mae_tra.lnkord

DISPLAY despliega.*

 -- Desplegando datos del documento 
 CLEAR FORM 
	DISPLAY "E ",w_mae_tra.estado
 DISPLAY BY NAME w_mae_tra.userid , 
					  w_mae_tra.fecsis ,
					  w_mae_tra.horsis ,
					  w_mae_tra.estado ,
					  w_mae_tra.lnkasi ,
					  w_mae_tra.lnkord ,
					  despliega.*
CALL facqbx001_detalle()
END FUNCTION 

-- Subrutina para seleccionar datos del detalle del documento 

FUNCTION facqbx001_detalle()
 DEFINE existe  SMALLINT

 -- Inicializando vector de productos
 CALL inving003_inivec()

 -- Seleccionando detalle del documento 
 DECLARE cdet CURSOR FOR
 SELECT x.cditem,
        x.codabr,
        y.dsitem,
        y.unimed,
        z.nommed, 
        x.canori,
        x.unimto,
        x.cantid,
        x.preuni,
        x.totpro,
        x.factor,
        x.correl
  FROM  inv_dasignac x, inv_products y, inv_unimedid z
  WHERE (x.lnkasi = w_mae_tra.lnkasi)
    AND (y.cditem = x.cditem)
    AND (z.unimed = y.unimed) 
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*

   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_products TO s_products.* ATTRIBUTE(BLUE)
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 

  -- Desplegando totales
  CALL inving003_totdet() 
END FUNCTION 

-- Subrutina para desplegar os datos de la orden de trabajo


FUNCTION facqbx003_anular()
 DEFINE loop,regreso SMALLINT,
        msg          STRING 

 -- Ingresando motivo de la anulacion
 LET loop = TRUE
 WHILE loop
  LET regreso = FALSE

  -- Verificando anulacion
  IF NOT librut001_yesornot("Confirmacion",
                            "Desea Anular el Documento ?",
                            "Si",
                            "No",
                            "question") THEN

     CLEAR motanl 
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Anulando documento 
  LET loop = FALSE 
  ERROR " Anulando Documento ..." ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  BEGIN WORK

   -- Marcando maestro del documento como anulado 
   SET LOCK MODE TO WAIT 
   DECLARE c_anul CURSOR FOR
   SELECT a.*
    FROM  inv_masignac a
    WHERE (a.lnkasi = w_mae_tra.lnkasi) 
    FOR UPDATE 
    FOREACH c_anul 
     UPDATE inv_masignac 
     SET    inv_masignac.estado = "A"
     WHERE CURRENT OF c_anul
    END FOREACH
    CLOSE c_anul
    FREE  c_anul

   -- Finalizando Transaccion
   COMMIT WORK

   ERROR ""
 END WHILE 

 RETURN regreso 
END FUNCTION 
