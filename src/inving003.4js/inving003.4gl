{ 
Fecha    : Enero 2011           
Programo : Mynor Ramirez
Objetivo : Programa de facturacion.
---------------------------------------------------------------------------------------------------------------
Cambios
---------------------------------------------------------------------------------------------------------------
Programo : Carlos Enrique Morales

1.  Se habilito el campo de orden de compra para cualquier ingreso
2.  Se agrego el ingreso del numero de telefono del cliente 
3.  Se modifico la forma de ontener los datos del maestro de clientes y que se refresquen los datos.  18/03/11 
4.  Se habilito el campo de credit para cualquier ingreso
5.  Se deshabilito la verificacion de si producto maneja precio minimo, solo se muestra mensaje de advertencia 
6.  Se agrego que cuando no exista precio sugerido este sea ingresado, actualizando de una vez el maestro de 
    productos.
7.  Se elimino la verificacion de si la orden es nula en el ingreso de datos de la orden de trabajo.  23/03/11 
8.  Se elimino la validacion del campo de descripcion de la orden de trabajao para que pueda ir en blanco
9.  Se elimino la validacion del campo de documentos de la orden de trabajao para que pueda ir en blanco
10.  Se valido para ingresar el precio por pie cuadrado 
11. Se elimino el caclulo del precio por factor de pie cuadrado, y se valido para que se ignresar el precio 
    directamente
}

-- Definicion de variables globales

GLOBALS "invglb003.4gl"
CONSTANT programa = "inving003" 

DEFINE username    LIKE glb_permxusr.userid,
       tasimp      DEC(9,6), 
       tascom      DEC(9,6), 
       tasvta      DEC(9,6), 
       vuelto      DEC(10,2), 
       portar      DEC(9,6),
       facord      DEC(9,6),
       haytar      SMALLINT, 
       wpais       VARCHAR(255), 
       regreso     SMALLINT,
       ordenes     SMALLINT, 
       existe      SMALLINT,
       msg         STRING ,
       ord     DYNAMIC ARRAY OF RECORD
         orden    INTEGER,
         material SMALLINT,
         color    SMALLINT,
         tamano   VARCHAR(30),
         nomcli   VARCHAR(30)
      END RECORD


-- Subrutina principal

MAIN

 DEFINE
      dbname      CHAR(20),
      n_param     SMALLINT
      
 -- Atrapando interrupts
 DEFER INTERRUPT

  LET n_param = num_args()

   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = ARG_VAL(1)
      DATABASE dbname
   END IF
 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar12")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Logeando usuario 
 --CALL inving003_login(0)
 --RETURNING regreso,username 
 LET flag_tmp=FALSE

 -- Verificanod regreso
 --IF NOT regreso THEN 
    -- Menu de opciones
    CALL inving003_menu()
 --END IF 
END MAIN

-- Subutina para el menu de facturacion 

FUNCTION inving003_menu()
 DEFINE regreso    SMALLINT, 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing003a AT 5,2  
  WITH FORM "inving003a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  --CALL librut001_header("inving003",wpais,1)

  -- Desplegando campo de motivo de anulacion
  CALL f.setFieldHidden("motanl",1)
  CALL f.setElementHidden("labeld",1)
  CALL f.setElementHidden("group4",1)
  CALL f.setElementHidden("labelg",1)
  CALL f.setFieldHidden("obspre",1)

  -- Cargando unidades de medida
  CALL librut003_cbxunidadesmedida("unimto") 

  -- Inicializando datos 
--  CALL inving003_inival(1)

  -- Menu de opciones
  MENU " Opciones" 
   BEFORE MENU
      LET w_mae_tra.userid=FGL_GETENV("LOGNAME")
      LET w_mae_tra.fecsis=today
      LET w_mae_tra.horsis=CURRENT
      LET w_mae_tra.estado="V"
      DISPLAY BY NAME w_mae_tra.userid, w_mae_tra.fecsis, w_mae_tra.horsis,w_mae_tra.estado
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(username,4) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(username,1) THEN
       HIDE OPTION "Ingresar"
    END IF
  -- Anular     
    IF NOT seclib001_accesos(username,2) THEN
       HIDE OPTION "Anular"
   END IF
   COMMAND "Consultar" 
    " Consulta de documentos de facturacion existentes."
    CALL facqbx001_facturacion(1)
   COMMAND "Ingresar" 
    " Emision de facturacion."
    CALL inving003_facturacion(1) 
	 CONTINUE MENU
   COMMAND "Anular"
    " Anulacion de documentos de facturacion existentes."
    CALL facqbx001_facturacion(2)
   COMMAND "Salir"
    " Abandona el menu de facturacion." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing003a
END FUNCTION

-- Subrutina para el ingreso de los datos del encabezado de la facturacion 

FUNCTION inving003_facturacion(operacion)
 DEFINE userauth          LIKE glb_permxusr.userid,
        xnomcli           LIKE fac_mtransac.nomcli, 
        xnumnit           LIKE fac_mtransac.dircli,
        xdircli           LIKE fac_mtransac.dircli,
        xtelcli           LIKE fac_mtransac.telcli,
        xdocto            CHAR(20), 
        retroceso         SMALLINT,
        operacion,acceso  SMALLINT,
        loop,existe,i     SMALLINT

 -- Inicio del loop
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF 

 -- Verificando si hay orden de trabajo
 IF ordenes THEN 
    -- Deshabilitando tabla de productos
    CALL f.setElementHidden("tabla1",1)
    CALL f.setElementHidden("group4",0)
    CALL f.setElementHidden("group5",1)
    CALL f.setElementHidden("labelc",1)
    CALL f.setElementHidden("label27",1)
    CALL f.setFieldHidden("compre",1)
 END IF 

 LET loop  = TRUE
 WHILE loop   

 	LET flag_especial = FALSE

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving003_inival(1) 
  END IF

  IF flag_tmp= FALSE THEN
     CALL inving003_tmp_db(1)
  END IF
  -- Ingresando datos
  INPUT BY NAME w_mae_tra.lnkord
                --w_mae_tra.numdoc
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)


	BEFORE INPUT
		LET w_mae_tra.userid=FGL_GETENV("LOGNAME")
		LET w_mae_tra.fecsis=today
		LET w_mae_tra.horsis=CURRENT
		LET w_mae_tra.estado="V"
		DISPLAY BY NAME w_mae_tra.userid, w_mae_tra.fecsis, w_mae_tra.horsis,w_mae_tra.estado
		CALL librut003_cbxsubcateg()
		CALL librut003_cbxcolores()

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(lnktdc) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        --CALL Dialog.SetFieldActive("lnktdc",TRUE) 
        CALL inving003_inival(1)
        --NEXT FIELD lnkord 
     	LET loop = FALSE 
      EXIT INPUT 
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

	AFTER FIELD lnkord
		IF w_mae_tra.lnkord IS NOT NULL THEN
			IF w_mae_tra.lnkord > 0 THEN
				SELECT *
			  	FROM inv_ordentra
			 	WHERE lnkord = w_mae_tra.lnkord
			   	AND nofase <> 2
				IF STATUS = 0 THEN
					ERROR "Error: Esa orden no es valida . . . "
					NEXT FIELD lnkord
				ELSE
					SELECT *
					  FROM inv_masignac
					 WHERE lnkord = w_mae_tra.lnkord
						AND estado = "V"
					IF STATUS = NOTFOUND THEN
						CALL inving003_llena()
					ELSE
						ERROR "Error: Ya hay asignacion a esa orden . . . "
						NEXT FIELD lnkord
					END IF
				END IF
			ELSE
				ERROR "Error: Orden no puede ser 0 . . . "
				NEXT FIELD lnkord
			END IF
		END IF

   ON ACTION listordenes
    -- Seleccionado lista de clientes
		CALL inving003_listord() RETURNING w_mae_tra.lnkord


   AFTER INPUT
     -- Ingresando detalle del movimiento 
     LET retroceso = inving003_detingreso()
	  LET loop = FALSE

  IF flag_tmp=TRUE THEN
     CALL inving003_tmp_db(2)
  END IF

	END INPUT

 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL inving003_inival(1) 
 END IF 

 -- Verificando si hay ordenes
 IF ordenes THEN
    -- Habilitando tabla de productos
    CALL f.setElementHidden("tabla1",0)
    CALL f.setElementHidden("group4",1)
    CALL f.setElementHidden("group5",0)
    CALL f.setElementHidden("labelc",0)
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos de la facturacion 

FUNCTION inving003_detingreso()
 DEFINE w_mae_art  RECORD LIKE inv_products.*,
        w_uni_vta  RECORD LIKE inv_unimedid.*,
        wpremin    DEC(12,2),
        wpresug    DEC(14,2),
        xtippre    SMALLINT, 
        loop,scr   SMALLINT,
        confirm    SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER,  
        repetido   SMALLINT,
        strcon     STRING,
        line_esp   SMALLINT,
		  flag_exis  SMALLINT 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT



   ON ACTION listproducto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos","Producto","Descirpcion del Producto",
                            "codabr","dsitem","inv_products","1=1",2,1,1)
    RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD cditem
    ELSE 
       -- Asignando descripcion
       DISPLAY v_products[arr].cditem TO s_products[scr].cditem 
       DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 
    END IF 

   ON CHANGE canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL inving003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL inving003_totdet()

   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Totalizando unidades
    CALL inving003_totdet()

   BEFORE ROW
    LET totlin = ARR_COUNT()

   BEFORE FIELD cditem
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cditem
    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cditem 
    END IF

   BEFORE FIELD canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Verificando producto
    IF v_products[arr].cditem IS NULL OR
       (v_products[arr].cditem <=0) THEN 
       ERROR "Error: codigo del producto invalido, VERIFICA ..."
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_products[arr].cditem)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       ERROR "Error: producto no registrado, VERIFICA ..."
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Asignando descripcion
    LET v_products[arr].codpro = w_mae_art.cditem
	 IF flag_especial = FALSE THEN
	    LET v_products[arr].dsitem = w_mae_art.dsitem 
	 END IF
    LET v_products[arr].unimed = w_mae_art.unimed 
    DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 

    -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed) 
    RETURNING w_uni_med.*,existe

    -- ASignando unidad de medida
    LET v_products[arr].nomuni = w_uni_med.nommed 
    DISPLAY v_products[arr].nomuni TO s_products[scr].nomuni 

    -- Obteniendo datos de la medida de producto
    INITIALIZE w_med_pro.* TO NULL
    CALL librut003_bmedidapro(w_mae_art.codmed) 
    RETURNING w_med_pro.*,existe

    -- Verificando productos duplicados 
    LET repetido = FALSE
    FOR i = 1 TO totlin
     IF v_products[i].cditem IS NULL THEN
        CONTINUE FOR
     END IF

     -- Verificando producto
     IF w_mae_art.dsitem NOT MATCHES "*MEDIDA*"   THEN
	     IF (i!=ARR_CURR()) THEN 
   	   IF (v_products[i].cditem = v_products[arr].cditem) THEN
      	   LET repetido = TRUE
	         EXIT FOR
   	   END IF 
	     END IF
	  END IF
    END FOR

    -- Si hay repetido
    IF repetido THEN
       LET msg = "Producto ya registrado en el detalle. Linea (",i USING "<<<",") \n VERIFICA ..." 
       CALL fgl_winmessage(" Atencion",msg,"stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem
    END IF

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si producto tiene precio sugerido  
    IF (w_mae_art.presug=0) AND w_mae_art.dsitem NOT MATCHES "*MEDIDA*" THEN
       -- Actualizando precio sugerido si no existe 
       LET w_mae_art.presug = inving003_actualiza_pre(w_mae_art.cditem) 
		 IF w_mae_art.presug <=0 then
			 LET arr = ARR_CURR()
			 INITIALIZE v_products[arr].* TO NULL
			 NEXT FIELD cditem
		 END IF
    END IF 

    -- Determinando tipo de precio
    LET xtippre = 1 -- Normal
    
    -- Verificando si mdida del producto es fraccionable
    IF w_med_pro.epqfra AND
       w_med_pro.medtot>0 THEN
       LET xtippre = 2  -- Rollos
    END IF 

    IF NOT w_med_pro.epqfra AND 
       w_med_pro.medtot>0 THEN
       LET xtippre = 3 -- Precios especiales
    END IF 

    -- Desplegando precio
    -- Verificando tipo de precio 
    CASE (xtippre)
     WHEN 1 -- Normal
      LET wpremin = w_mae_art.premin
      LET wpresug = w_mae_art.presug

      -- Precio sugerido de la lista de precios
      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug 
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni 
      END IF 

      -- Asignando unidad de medida
      LET v_products[arr].unimto = w_mae_art.unimed 
      DISPLAY v_products[arr].unimto TO s_products[scr].unimto 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",0) 
      CALL Dialog.SetFieldActive("canuni",0) 

     WHEN 2 -- Precio de rollo
      LET wpremin = w_mae_art.premin 
      LET wpresug = w_mae_art.presug

      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni 
      END IF 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",1) 
      CALL Dialog.SetFieldActive("canuni",0) 
     WHEN 3 -- Precios especiales 
      LET wpremin = w_mae_art.premin
      LET wpresug = w_mae_art.presug

      -- Precio sugerido de la lista de precios
      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug 
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni 
      END IF 

      -- Asignando unidad de medida
      LET v_products[arr].unimto = w_mae_art.unimed 
      DISPLAY v_products[arr].unimto TO s_products[scr].unimto 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",0) 
      CALL Dialog.SetFieldActive("canuni",0) 
    END CASE 

    -- Calculando cantidad total en unidades totales
    CALL inving003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL inving003_totdet()

   AFTER FIELD canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD canori 
    END IF

    -- Verificando cantidad
    IF v_products[arr].canori IS NULL OR
       (v_products[arr].canori <=0) THEN 
       ERROR "Error: cantidad invalida, VERIFICA ..."
       NEXT FIELD canori 
    END IF 

    -- Verificanod si tipo de precio es normal o especial
    IF (xtippre=1) OR 
       (xtippre=3) THEN
       LET v_products[arr].canuni = v_products[arr].canori
       LET v_products[arr].factor = 0 
       DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving003_cantidadtotal(arr,scr)
	

    -- Totalizando unidades
    CALL inving003_totdet()
{
   before field preuni
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      IF v_products[arr].canuni >0 then
         SELECT *
            FROM inv_proenbod
            WHERE codabr =  v_products[arr].cditem
            AND   codemp =  w_mae_pos.codemp
            AND   codsuc =  w_mae_pos.codsuc
            AND   codbod =  w_mae_pos.codbod
            AND   exican <= v_products[arr].canuni
         IF STATUS != NOTFOUND THEN
             LET opc = librut001_menugraba("Confirmacion",
                                           "Que desea hacer?",
                                           "Otras Bodegas",
                                           "Modificar",
                                           "Cancelar",
                                           "")
				CASE opc
					WHEN 1
						CALL inving003_existencias(v_products[arr].cditem,v_products[arr].canuni) RETURNING flag_exis
						IF flag_exis = FALSE	THEN
							INITIALIZE v_products[arr].* TO NULL
							CLEAR s_products[scr].* 
							NEXT FIELD cditem
						ELSE
							NEXT FIELD NEXT
						END IF
					WHEN 2
						LET v_products[arr].canuni =null
						NEXT FIELD cditem
					WHEN 3
						CALL v_products.deleteElement(arr)
						NEXT FIELD cditem
				END CASE
         END IF
      END IF
}
   AFTER FIELD unimto
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD unimto 
    END IF

    -- Verificando cantidad
    IF v_products[arr].unimto IS NULL THEN
       ERROR "Error: unidad de medida invalida, VERIFICA ..."
       NEXT FIELD unimto 
    END IF 

    -- Verificando medidas 
    IF (v_products[arr].unimed!=v_products[arr].unimto) THEN
       -- Obteniendo datos de la unidad de medida 
       INITIALIZE w_uni_vta.* TO NULL
       CALL librut003_bumedida(v_products[arr].unimto) 
       RETURNING w_uni_vta.*,existe
       
      -- Verificanod conversion 
      IF w_uni_vta.factor IS NULL OR
         w_uni_vta.factor=0 THEN 
         ERROR "Error: unidad de medida sin factor de conversion, VERIFICA ..."
         NEXT FIELD unimto 
      END IF 

      -- Convirtiendo cantidad de acuerdo al factor 
      LET v_products[arr].canuni = (v_products[arr].canori*w_uni_vta.factor) 
      LET v_products[arr].factor = w_uni_vta.factor 
      DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    ELSE 
      -- Asignando cantidad origen a cantidad total 
      LET v_products[arr].canuni = v_products[arr].canori
      LET v_products[arr].factor = 0
      DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL inving003_totdet()

   AFTER FIELD preuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD preuni 
    END IF

    -- Verificando precio
    IF v_products[arr].preuni IS NULL OR
       (v_products[arr].preuni <=0) THEN 
       ERROR "Error: precio invalido, VERIFICA ..."
       NEXT FIELD preuni 
    END IF 

    -- Verificando que precio no sea menor que el minimo
    IF (v_products[arr].preuni<wpremin) THEN
       -- Actualizando precio minimo 
       CALL fgl_winmessage("Atencion",
       " Precio ingresado menor al precio minimo. ("||wpremin||")",
       "stop")
   END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL inving003_totdet()

   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL inving003_totdet()

   AFTER DELETE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Limpiando producto borrado
    INITIALIZE v_products[arr].* TO NULL
    CLEAR s_products[scr].* 
	 IF arr = line_esp THEN
		LET flag_especial = FALSE
		CALL DIALOG.SetActionActive("agregar_especial",TRUE)
	   LET line_esp = NULL
		INITIALIZE w_mae_ord.* TO NULL
	 END IF

    -- Totalizando unidades
    CALL inving003_totdet()

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL inving003_totdet()
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando lineas incompletas 
  LET linea = inving003_incompletos()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") de producto incompleta. \n VERIFICA ..."
     CALL fgl_winmessage(
     " Atencion",
     msg, 
     "stop")
     CONTINUE WHILE
  END IF 

  -- Verificando que se ingrese al menos un producto
  IF (totval<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     " Debe facturarse al menos un producto. \n VERIFICA ...",
     "stop")
     CONTINUE WHILE
  END IF

  -- Verificando disponibilidad si documento es de credito
  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    -- Ingresando confirmacion de cajero
       -- Grabando inventario 
       CALL inving003_grabar()

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subutina para el ingreso de los datos de la orden de trabajo

FUNCTION inving003_detorden()
 DEFINE wpremin    DECIMAL(12,2), 
        wpresug    LIKE inv_subcateg.presug,
        confirm    SMALLINT, 
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER,  
        strcon     STRING,
        medida     VARCHAR(10,0),
        wnomcla                 ,
		  wnomcol    VARCHAR(25,0),
        descrip    VARCHAR(60,0),
		  wcodabr    LIKE inv_products.codabr,
        busqueda   VARCHAR(20,0)

 -- Iniciando el loop
 LET retroceso = FALSE
 LET w_mae_ord.sinord = 0
 LET w_mae_ord.ordext = 0
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT BY NAME w_mae_ord.fecord,
                w_mae_ord.cantid,
                w_mae_ord.subcat,
                w_mae_ord.codcol,
                w_mae_ord.desman,
                w_mae_ord.medida,
                w_mae_ord.xlargo,
                w_mae_ord.yancho, 
                w_mae_ord.descrp,
                w_mae_ord.observ,
					 w_mae_ord.sinord,
					 w_mae_ord.ordext,
                w_mae_ord.fecofe,
                w_mae_ord.fecent,
                w_mae_ord.totord,  
                w_mae_ord.totabo,
                w_mae_ord.doctos  WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION precios
    -- Cambiando precios 
    IF INFIELD(totabo) OR flag_especial THEN 
       CALL inving003_precio(wpremin,wpresug,w_mae_ord.premed)
    END IF 

	ON ACTION accept 
		LET retroceso = FALSE
      LET loop      = FALSE
		EXIT INPUT

   BEFORE INPUT
    -- Asignando datos
	 IF flag_especial = FALSE THEN
	    CALL DIALOG.setActionActive("precios",FALSE)
	    CALL DIALOG.setActionActive("accept",FALSE)
	 ELSE
	    CALL DIALOG.setActionActive("accept",TRUE)
	 END IF
	 --CALL DIALOG.setActionActive("precios",FALSE)
    CALL f.setElementHidden("label27",1)
    CALL f.setFieldHidden("compre",1)
    LET w_mae_ord.lnkord = 0
    --LET w_mae_ord.fecord = w_mae_tra.fecemi
    DISPLAY BY NAME w_mae_ord.numord,w_mae_ord.fecord,w_mae_ord.cantid,w_mae_ord.totord,
                    w_mae_ord.totabo,w_mae_ord.salord,w_mae_ord.lnkord,--w_mae_ord.premed,
                    w_mae_ord.nofase 

    -- Cargando combobox 
    CALL librut003_cbxsubcateg()
    CALL librut003_cbxfasesot() 

   ON CHANGE cantid
    -- Calculando saldo de la orden
    CALL inving003_totalord(w_mae_ord.premed)

   AFTER FIELD cantid
    -- Verificando cantidad
    IF w_mae_ord.cantid IS NULL OR
       w_mae_ord.cantid <=0 THEN
       ERROR "Error: cantidad invalida, VERIFICA ..." 
       LET w_mae_ord.cantid = NULL
       CLEAR cantid 
       NEXT FIELD cantid
    END IF 

   ON CHANGE subcat 
    -- Comobobox de colores x clase 
    IF w_mae_ord.subcat IS NOT NULL THEN
       -- Flag para calculo del precio
       LET flag_prec = FALSE
       --CALL librut003_cbxcoloresxsubcat(w_mae_ord.subcat) 
       CALL librut003_cbxcolores() 
    END IF 

   AFTER FIELD subcat
    -- Verificando clase
    IF w_mae_ord.subcat IS NULL THEN
       ERROR "Error: clase de producto invalida, VERIFICA ..." 
       NEXT FIELD subcat
    END IF 

    -- Buscando datos de la subcategoria
    SELECT NVL(a.tipsub,1),NVL(a.premin,0),NVL(a.presug,0)
     INTO  w_mae_ord.tipord,wpremin,wpresug 
     FROM  inv_subcateg a
     WHERE a.subcat = w_mae_ord.subcat

	  IF w_mae_ord.subcat = 5 THEN
			LET w_mae_ord.ordext = 1
	  END IF


	 BEFORE FIELD ordext
     IF w_mae_ord.subcat = 5 THEN
			NEXT FIELD NEXT
     END IF

    -- Verificando precios minimos y sugeridos
    IF wpremin=0 OR wpresug=0 THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Clase de producto sin precios definidos.\n Definir precios antes de poder facturar.",
       "warning")
       NEXT FIELD subcat 
    END IF  

    -- Desplegando precios
    LET pre_med          = wpresug 
    LET w_mae_ord.premed = wpresug 

    -- Calculando saldo de la orden
    CALL inving003_totalord(w_mae_ord.premed)

   BEFORE FIELD codcol
    -- Comobobox de colores x clase 
    IF w_mae_ord.subcat IS NOT NULL THEN
       --CALL librut003_cbxcoloresxsubcat(w_mae_ord.subcat) 
       CALL librut003_cbxcolores() 
    END IF 

   AFTER FIELD codcol 
    -- Verificando color
    IF w_mae_ord.codcol IS NULL THEN
       NEXT FIELD subcat 
    END IF 

   ON CHANGE medida 
    -- Calculando saldo de la orden
	 LET flag_prec = FALSE
    CALL inving003_totalord(w_mae_ord.premed)

   AFTER FIELD medida
    -- Verificanod medida
    IF w_mae_ord.medida IS NULL THEN
       ERROR "Error: tipo de medida invalido, VERIFICA ..."
       NEXT FIELD medida 
    END IF 

   ON CHANGE xlargo
    -- Calculando saldo de la orden
    -- Flag para calculo del precio 
    LET flag_prec = FALSE
    CALL inving003_totalord(w_mae_ord.premed)

   AFTER FIELD xlargo
    -- Verificando LARGO
    IF w_mae_ord.xlargo IS NULL OR
       w_mae_ord.xlargo <=0 THEN 
       ERROR "Error: largo de la medida invalido, VERIFICA ..." 
       LET w_mae_ord.xlargo = NULL
       CLEAR xlargo 
       NEXT FIELD xlargo
    END IF 

 --   -- Calculando saldo de la orden
 --   CALL inving003_totalord(w_mae_ord.premed)

   ON CHANGE yancho 
    -- Calculando saldo de la orden
    -- Flag para calculo del precio
    LET flag_prec = FALSE
    CALL inving003_totalord(w_mae_ord.premed)

   AFTER FIELD yancho 
    -- Verificando ANCHO 
    IF w_mae_ord.yancho IS NULL OR
       w_mae_ord.yancho <=0 THEN 
       ERROR "Error: ancho de la medida invalido, VERIFICA ..." 
       LET w_mae_ord.yancho = NULL
       CLEAR yancho 
       NEXT FIELD yancho
    END IF 

--    -- Calculando saldo de la orden
 --   CALL inving003_totalord(w_mae_ord.premed)

   AFTER FIELD fecofe 
    -- Verificando fecha sugerida
    IF w_mae_ord.fecofe IS NULL OR
       w_mae_ord.fecofe <TODAY THEN
       ERROR "Error: fecha sugerida invalida, VERIFICA ..." 
       NEXT FIELD fecofe
    END IF 

   AFTER FIELD fecent 
    -- Verificando fecha de entrega 
    IF w_mae_ord.fecent IS NULL OR
       w_mae_ord.fecent <TODAY THEN
       ERROR "Error: fecha de entrega invalida, VERIFICA ..." 
       NEXT FIELD fecent
    END IF 

   ON CHANGE totabo 
    -- Calculando saldo de la orden
    CALL inving003_totalord(w_mae_ord.premed)

   BEFORE FIELD totabo
    -- Activando boton de precios
    CALL DIALOG.setActionActive("precios",TRUE)


   AFTER INPUT 
		IF w_mae_ord.fecord IS NULL OR w_mae_ord.cantid IS NULL OR w_mae_ord.subcat IS NULL OR w_mae_ord.codcol IS NULL OR
			w_mae_ord.medida IS NULL OR w_mae_ord.xlargo IS NULL OR w_mae_ord.yancho IS NULL OR w_mae_ord.fecofe IS NULL OR
			w_mae_ord.fecent IS NULL OR w_mae_ord.totord IS NULL OR w_mae_ord.totabo IS NULL THEN
			ERROR "Error: Debe de llenar todos los datos necesarios."
			NEXT FIELD cantid
      END IF
    -- Verificando fechas de entrega 
    IF w_mae_ord.fecofe>w_mae_ord.fecent THEN
       ERROR "Error: fecha sugerida no puede ser mayor a fecha de entrega.";
       NEXT FIELD fecofe
    END IF 

    -- Calculando saldo de la orden
    CALL inving003_totalord(w_mae_ord.premed)
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Ingresando desgloce de pago
  -- Verificando si documento es de contado
  IF flag_especial = FALSE THEN
	  LET haytar = FALSE  
	  -- Menu de opciones
	  LET opc = librut001_menugraba("Confirmacion",
	                                "Que desea hacer?",
	                                "Guardar",
	                                "Modificar",
	                                "Cancelar",
	                                "")

	  -- Verificando opcion 
	  CASE (opc)
	   WHEN 0 -- Cancelando
	    LET loop      = FALSE
	    LET retroceso = FALSE
	   WHEN 1 -- Grabando
	    -- Ingresando confirmacion de cajero
      	 -- Grabando inventario 
   	    CALL inving003_grabar()
	   WHEN 2 -- Modificando
	    LET loop = TRUE 
	  END CASE
	ELSE
      IF NOT w_mae_ord.medida THEN
         LET medida = "METROS"
      ELSE
         LET medida = "PIES"
      END IF
      -- Obteniendo clase de producto
      LET wnomcla = NULL
      SELECT a.nomsub
       INTO  wnomcla
       FROM  inv_subcateg a
       WHERE a.subcat = w_mae_ord.subcat
		LET busqueda="*",wnomcla CLIPPED,"*MEDIDA"
		
		SELECT a.codabr
		  INTO wcodabr
		  FROM inv_products a
		  WHERE a.dsitem MATCHES busqueda
      -- Obteniendo color de producto
      LET wnomcol = NULL
      SELECT a.nomcol
       INTO  wnomcol
       FROM  inv_colorpro a
       WHERE a.codcol = w_mae_ord.codcol

		IF wnomcla <> "SARAN" THEN
			LET descrip = "LONA ",wnomcla CLIPPED," COLOR ",wnomcol CLIPPED," DE ",w_mae_ord.xlargo USING "<<<.##"," X ",w_mae_ord.yancho USING "<<<.##"," ",medida
			EXIT WHILE
		ELSE
			LET descrip = wnomcla CLIPPED," ",wnomcol CLIPPED," DE ",w_mae_ord.xlargo USING "<<<.##"," X ",w_mae_ord.yancho USING "<<<.##"," ",medida
			EXIT WHILE
		END IF
	END IF
 END WHILE
	IF flag_especial = TRUE THEN
		IF retroceso = TRUE  THEN
		   CLOSE WINDOW especial
	 		RETURN "","","","","","",""
		ELSE
		   CLOSE WINDOW especial
	 		RETURN wcodabr,"UNIDAD",descrip,w_mae_ord.cantid,w_mae_ord.precio,w_mae_ord.totord,8
		END IF
	ELSE
 		RETURN retroceso
	END IF
END FUNCTION

-- Subrutina para cambiar el precio unitario 

FUNCTION inving003_precio(premin,presug,precio)
 DEFINE premin   LIKE inv_subcateg.premin,
        presug   LIKE inv_subcateg.presug,
        precio   LIKE inv_subcateg.presug,
        regreso  SMALLINT 

 -- Ingresando precios
 INPUT BY NAME w_mae_ord.precio,
               w_mae_ord.compre WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED) 

  ON ACTION cancel
   -- Regreso
   LET regreso = TRUE
   EXIT INPUT 

  ON CHANGE precio 
   -- Calculando saldo de la orden
   CALL inving003_totalord(w_mae_ord.premed)

  AFTER FIELD precio
   -- Verificando precio
   IF w_mae_ord.precio IS NULL OR
      w_mae_ord.precio <=0 THEN
      ERROR "Error: precio invalido , VERIFICA ..."
      LET w_mae_ord.precio = 0 
      DISPLAY BY NAME w_mae_ord.precio 
      NEXT FIELD precio
   END IF 

   IF (w_mae_ord.premed<pre_med) AND
      (w_mae_ord.xlargo IS NOT NULL AND w_mae_ord.yancho IS NOT NULL)
      AND (w_mae_ord.xlargo >0 AND w_mae_ord.yancho >0) THEN
      CALL fgl_winmessage(
      " Atencion",
      " Precio ingresado menor al precio minimo por pie cuadrado. "||pre_med CLIPPED || ", \n VERIFICA ...",
      "warning")
   END IF

   -- Verificando si precios es menor que precio minimo
   IF (w_mae_ord.premed<pre_med) THEN
      -- Habilitando campo de comentario si el precio es menor al precio sugerido 
      CALL f.setElementHidden("label27",0)
      CALL f.setFieldHidden("compre",0)
   ELSE
      LET w_mae_ord.compre = NULL 
   END IF  
 END INPUT 

 -- Deshabilitando campo de comentario si el precio es menor al precio sugerido 
 CALL f.setElementHidden("label27",1)
 CALL f.setFieldHidden("compre",1)
END FUNCTION 

-- Subrutina para calcular el total y saldo de la orden de trabajo

FUNCTION inving003_totalord(wprecio)
 DEFINE wprecio LIKE inv_ordentra.premed, 
        faccon  DEC(9,2),
        tot_uni DEC(9,2)

 -- Verificando alto y ancho
 IF w_mae_ord.xlargo IS NULL THEN LET w_mae_ord.xlargo = 0 END IF 
 IF w_mae_ord.yancho IS NULL THEN LET w_mae_ord.yancho = 0 END IF 

 -- Calculando saldo de la orden
 IF NOT w_mae_ord.medida THEN
    LET faccon = facord
 ELSE
    LET faccon = 1
 END IF 

 IF flag_prec = FALSE THEN
   LET wprecio = pre_med
   LET w_mae_ord.precio = (((w_mae_ord.xlargo*faccon)*(w_mae_ord.yancho*faccon))*wprecio)
   LET flag_prec=TRUE
 ELSE
   LET wprecio = w_mae_ord.precio/((w_mae_ord.xlargo*faccon)*(w_mae_ord.yancho*faccon))
   IF wprecio > 0 THEN
      LET w_mae_ord.premed = wprecio
   END IF
   LET w_mae_ord.precio = (((w_mae_ord.xlargo*faccon)*(w_mae_ord.yancho*faccon))*w_mae_ord.premed)
 END IF

 -- Totalizando orden 
 LET w_mae_ord.totord = w_mae_ord.cantid*w_mae_ord.precio

 IF (w_mae_ord.totord>=w_mae_ord.totabo) THEN 
    LET w_mae_ord.salord = (w_mae_ord.totord-w_mae_ord.totabo)
 ELSE
    LET w_mae_ord.salord = 0 
 END IF 
 
 --LET w_mae_tra.totpag = w_mae_ord.totabo 
 IF flag_especial = FALSE THEN
	 --DISPLAY BY NAME w_mae_tra.totpag
 ELSE
	 LET w_mae_ord.salord = 0 
 END IF
 IF flag_especial THEN
	 let w_mae_ord.totabo = w_mae_ord.totord
 END IF
 DISPLAY BY NAME w_mae_ord.salord,w_mae_ord.totord,w_mae_ord.precio
END FUNCTION 

-- Subrutina para calcular las undiades totales (unidades ingresdas * cantidad empaque)

FUNCTION inving003_cantidadtotal(arr,scr) 
 DEFINE arr,scr  SMALLINT

 -- Calculando precio total
 LET v_products[arr].totpro = (v_products[arr].canuni*v_products[arr].preuni)

 -- Desplegando datos
 DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
 DISPLAY v_products[arr].totpro TO s_products[scr].totpro 
END FUNCTION 

-- Subrutina para totalizar unidades contadas

FUNCTION inving003_totdet()
 DEFINE i SMALLINT

 -- Totalizando
 LET totuni = 0
 LET totval = 0
 FOR i = 1 TO totlin
  IF v_products[i].codpro IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Totalizando
  LET totuni = (totuni+v_products[i].canuni)
  LET totval = (totval+v_products[i].totpro)
 END FOR

 -- Desplegando total de unidades del inventario 
 --LET w_mae_tra.totpag = totval
 --DISPLAY BY NAME totuni,totval,totlin,w_mae_tra.totpag 
END FUNCTION 

-- Subrutina para verificar si hay productos incompletos

FUNCTION inving003_incompletos()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO 100 
  IF v_products[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_products[i].canuni IS NULL OR
     v_products[i].preuni IS NULL OR
     v_products[i].totpro IS NULL THEN
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para ingresar el desgloce de pago

FUNCTION inving003_desglocepago()
 DEFINE recibi  DEC(14,2),
        loop    SMALLINT, 
        regreso SMALLINT, 
        lastkey SMALLINT,
        msg     STRING 

 OPTIONS INPUT WRAP 

 -- Ingresando desgloce 
 LET loop   = TRUE
 LET haytar = FALSE 

 OPTIONS INPUT NO WRAP 

 RETURN regreso 
END FUNCTION 

-- Subrutina para incrementar el porcentaje de tarjeta de credito 


-- Subrutina para ingresar la clave de confirmacion antes de grabar

FUNCTION inving003_confirmacion()
 DEFINE passwd    LIKE fac_usuaxpos.passwd,
        username  LIKE fac_usuaxpos.userid,
        confirm   SMALLINT

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wsecurity AT 9,27
  WITH FORM "inving003c"

  -- Ingresando clave de confirmacion 
  LET confirm = TRUE  
  INPUT BY NAME passwd
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel
    LET confirm = FALSE 
    EXIT INPUT 

   AFTER FIELD passwd
    -- Verificanod password
    IF LENGTH(passwd)<=0 THEN
       ERROR "Error: password invalido ..."
       NEXT FIELD passwd
    END IF 

    -- Verificando cajero por medio de su password corto
    LET username = NULL 
    SELECT UNIQUE a.userid
     INTO  username 
     FROM  fac_usuaxpos a
     --WHERE a.numpos = w_mae_tra.numpos
       --AND a.passwd = passwd
     IF (status=NOTFOUND) THEN
        ERROR "Error: password incorrecto ..."
        NEXT FIELD passwd 
     END IF
  END INPUT 
 CLOSE WINDOW wsecurity 

 RETURN confirm,username 
END FUNCTION 

-- Subrutina para grabar la facturacion 

FUNCTION inving003_grabar()
 DEFINE i,correl SMALLINT,
        pct      DEC(9,6) 

 -- Grabando transaccion
 ERROR " Registrando Facturacion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grababdo encabezado de la facturacion 
   -- Asignando datos
 --  IF w_mae_tra.tascam IS NULL THEN LET w_mae_tra.tascam=0 END IF
   LET w_mae_tra.lnkasi = 0 
   LET w_mae_tra.fecsis = TODAY
   LET w_mae_tra.horsis = CURRENT
   LET w_mae_tra.horsis = CURRENT
   LET w_mae_tra.estado = "V"
	LET w_mae_tra.fecsis = TODAY
   LET w_mae_tra.userid = FGL_GETENV("LOGNAME")

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO inv_masignac
   VALUES (w_mae_tra.*)
   LET w_mae_tra.lnkasi = SQLCA.SQLERRD[2] 

   -- 2. Grabando detalle de la facturacion 
   -- Si no es orden de trabajo
    LET correl = 0
    FOR i = 1 TO totlin 
     IF v_products[i].cditem IS NULL OR 
        v_products[i].canuni IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 

     -- Grabando
     SET LOCK MODE TO WAIT
     INSERT INTO inv_dasignac 
     VALUES (w_mae_tra.lnkasi    , -- link del encabezado
             correl              , -- correlativo de ingreso
             v_products[i].codpro, -- codigo del producto 
             v_products[i].cditem, -- codigo del producto abreviado
             v_products[1].unimed, -- unidad de medida del producto 
             v_products[i].canori, -- cantidad original
             v_products[i].unimto, -- unidad de medida de la medida de venta
             v_products[i].canuni, -- cantidad total 
             v_products[i].preuni, -- precio unitario 
             v_products[i].totpro, -- valor total 
             v_products[i].factor) -- factor

     
    END FOR 

   -- Desplegando mensaje
   CALL fgl_winmessage(" Atencion","Transaccion registrada.","information")

 -- Finalizando la transaccion
 COMMIT WORK

 -- Imprimiendo movimiento
 ERROR "Imprimiendo ... por favor espere ...."

 -- Verificando si pago es con tarjeta
 IF haytar THEN
    LET pct = portar
 ELSE
    LET pct = 0 
 END IF 

 --CALL facrpt001_facturacion(pct)
 ERROR "" 
END FUNCTION 

-- Subrutina para buscar si un numero de documento existe


-- Subrutina para grabar un documento como anulado


-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving003_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*,w_mae_ord.* TO NULL 
   CLEAR FORM 
 END CASE 
{
 -- Inicializando datos
 LET w_mae_tra.numpos = xnumpos
 LET w_mae_tra.estado = "V" 
 LET w_mae_tra.credit = 0
 LET w_mae_tra.hayord = ordenes 
 LET w_mae_tra.fecemi = TODAY 
 LET w_mae_tra.poriva = tasimp 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET w_mae_tra.efecti = 0
 LET w_mae_tra.cheque = 0
 LET w_mae_tra.tarcre = 0
 LET w_mae_tra.ordcom = 0 
 LET w_mae_tra.depmon = 0 
 LET w_mae_tra.subtot = 0
 LET w_mae_tra.totiva = 0
 LET w_mae_tra.totdoc = 0
 LET w_mae_tra.totpag = 0 
 LET w_mae_tra.saldis = 0 
 LET w_mae_ord.numord[1,2] = w_mae_tra.numpos USING "&&" 
 LET w_mae_ord.totord = 0
 LET w_mae_ord.totabo = 0
 LET w_mae_ord.salord = 0
 LET w_mae_ord.medida = 1  
 LET w_mae_ord.tipord = 1  
 LET w_mae_ord.estado = "V" 
 LET w_mae_ord.nofase = 1
 LET w_mae_ord.fecofe = TODAY+3
 LET w_mae_ord.fecent = TODAY+3
 LET w_mae_ord.premed = 0 
 LET totlin           = 0 
 LET totuni           = 0
 LET totval           = 0
 LET vuelto           = 0 
 LET w_mae_ord.desman = 0

 -- Obteniendo datos del punto de venta
 INITIALIZE w_mae_pos.* TO NULL
 CALL librut003_bpuntovta(w_mae_tra.numpos)
 RETURNING w_mae_pos.*,existe 
 CALL f.setElementText("labela","PUNTO DE VENTA [ "||w_mae_pos.nompos CLIPPED||" ]")

 -- Llenando combo de tipos de documento x punot de venta
 CALL librut003_cbxtiposdocxpos(w_mae_tra.numpos)

 -- Inicializando vectores de datos
 CALL inving003_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecemi,w_mae_tra.horsis
 DISPLAY BY NAME totuni,totval,totlin,vuelto 
 DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.cheque,w_mae_tra.tarcre,w_mae_tra.ordcom,w_mae_tra.totiva,w_mae_tra.totdoc
 DISPLAY BY NAME w_mae_tra.estado,w_mae_tra.hayord,w_mae_tra.depmon,w_mae_tra.totpag
}
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION inving003_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_products[i].*
 END FOR 
END FUNCTION 

-- Subrutina para el ingreso y actualizacion del precio sugerido 

FUNCTION inving003_actualiza_pre(id)
 DEFINE precio   DECIMAL(12,2 ),
	id       INTEGER, 
        regreso  SMALLINT,
        query    VARCHAR(255,0)

 -- Creadno estatuto  
 LET query = " UPDATE inv_products SET "," inv_products.presug =? "," WHERE inv_products.cditem =?"
 PREPARE prpUPD_pre FROM query

 CALL fgl_winmessage(
 " Atencion",
 " Producto no tiene precio definido, definirlo para poder facturar.",
 "stop")

 -- Ingredando precio 
 OPEN WINDOW wing001d AT 5,2 WITH FORM "inving003d"

  LET regreso = FALSE 
  INPUT BY NAME precio WITHOUT DEFAULTS
   ATTRIBUTE(UNBUFFERED) 

   ON ACTION cancel
    LET regreso = TRUE
	 EXIT INPUT 

	ON KEY (CONTROL-E)
	 LET regreso =TRUE
	 EXIT INPUT

   AFTER FIELD precio
    -- Verificando precio 
    IF precio IS NULL OR
       precio <=0 THEN
       ERROR "Error: precio del producto invalido, VERIFICA ..." 
       NEXT FIELD precio
    END IF
  END INPUT 

  -- Actualizando 
  IF NOT regreso THEN 
   BEGIN WORK
    EXECUTE prpUPD_pre USING precio,id 
    IF STATUS=0 THEN
       COMMIT WORK
    ELSE
       ROLLBACK WORK
    END IF
   END IF 
  CLOSE WINDOW wing001d

 RETURN precio
END FUNCTION

FUNCTION inving003_ver_especial()

	SELECT *
	  INTO w_mae_ord.*
	  FROM inv_ordentra
	  WHERE lnkord = w_mae_tra.lnkord

	OPEN WINDOW veres
	WITH FORM "inving003e"

	MENU


		BEFORE MENU
    -- Cargando combobox 
 			CALL librut003_cbxcolores() 
			CALL librut003_cbxsubcateg()
			CALL librut003_cbxfasesot() 
			DISPLAY BY NAME w_mae_ord.lnkord,
								 w_mae_ord.numord,
								 w_mae_ord.nofase,
								 w_mae_ord.fecord,
								 w_mae_ord.cantid,
								 w_mae_ord.subcat,
								 w_mae_ord.codcol,
								 w_mae_ord.desman,
								 w_mae_ord.medida,
								 w_mae_ord.xlargo,
								 w_mae_ord.yancho,
								 w_mae_ord.descrp,
								 w_mae_ord.observ,
								 w_mae_ord.fecofe,
								 w_mae_ord.fecent,
								 w_mae_ord.totord,
								 w_mae_ord.totabo,
								 w_mae_ord.salord,
								 w_mae_ord.doctos, 
								 w_mae_ord.precio
		ON ACTION cancel
			EXIT MENU

	END MENU

	INITIALIZE w_mae_ord.* TO NULL
	CLOSE WINDOW veres

END FUNCTION
FUNCTION inving003_existencias(prod,cant)
DEFINE flag_act smallint,
		 arr , s_line SMALLINT,
		 prod VARCHAR(30),
		 cant decimal(4,2),
		 i    SMALLINT,
		 count SMALLINT,
		 total DECIMAL (4,2),
		 length SMALLINT,
		 lastkey INTEGER

	LET length=0
   OPTIONS INPUT WRAP 
	CALL inving003_llena_exis(prod)
	OPEN WINDOW existencias WITH FORM "inving003f.per"
	LET flag_act = FALSE
	LET count = 0
	LET total=0

	IF v_existencia.getLength() = 0 THEN
		ERROR "No hay existencias en ninguna bodega, verifique"
		CLOSE WINDOW existencias
		RETURN FALSE
	END IF

	LET length=v_existencia.getLength()

	INPUT ARRAY v_existencia from existencias.* ATTRIBUTES(WITHOUT DEFAULTS)
	
		BEFORE INPUT
			LET int_flag = FALSE
			CALL DIALOG.setActionHidden("append",TRUE)
			CALL DIALOG.setActionHidden("close",TRUE)
			CALL DIALOG.setActionHidden("insert",TRUE)
			DISPLAY BY NAME total
			DISPLAY cant TO necesarios 
 

		ON ACTION accept
			LET flag_act=TRUE 
			GOTO :fuera
				
		ON ACTION cancel
			LET flag_act=FALSE
			GOTO :fuera
				

		AFTER FIELD salcan
			LET arr=ARR_CURR()
			LET s_line=SCR_LINE()
			IF v_existencia[arr].salcan IS NULL THEN
				LET v_existencia[arr].salcan = 0
				CALL inving003_totexi() RETURNING total
				DISPLAY BY NAME total
			ELSE
				IF v_existencia[arr].salcan > v_existencia[arr].exiact THEN
					CALL box_valdato("No hay suficientes en esa bodega, verifique")
					LET v_existencia[arr].salcan = 0
					CALL inving003_totexi() RETURNING total
					DISPLAY BY NAME total
					NEXT FIELD salcan
				ELSE
					CALL inving003_totexi() RETURNING total
					IF total > cant THEN 
						CALL box_valdato("Usted eligio mas productos de los necesarios, verifique")
						LET v_existencia[arr].salcan = 0
						CALL inving003_totexi() RETURNING total
						DISPLAY BY NAME total
						NEXT FIELD salcan
					ELSE
					   LET lastkey = FGL_LASTKEY()
						IF (arr = length) AND ((lastkey = FGL_KEYVAL("down")) OR (lastkey = FGL_KEYVAL("tab")) OR (lastkey = FGL_KEYVAL("return"))) THEN
							ERROR "Ya no hay mas bodegas . . . "
							CALL inving003_totexi() RETURNING total
							DISPLAY BY NAME total
							NEXT FIELD salcan
						ELSE
							CALL inving003_totexi() RETURNING total
							DISPLAY BY NAME total
						END IF
					END IF
				END IF
			END IF
{
		BEFORE FIELD salcan
			LET arr=ARR_CURR()
			LET s_line=SCR_LINE()
			IF v_existencia[arr].bodega IS NULL THEN
				ERROR "Ya no hay mas bodegas . . . "
				CALL v_existencia.deleteElement(arr)
			ELSE
				CALL inving003_totexi() RETURNING total
				DISPLAY BY NAME total
			END IF
}			
		AFTER INPUT
			LABEL fuera:
			IF total > cant AND flag_act = TRUE THEN
				CALL box_valdato ("Usted eligio mas producto del que vendera")
				NEXT FIELD salca
			END IF
			IF flag_act THEN
				SELECT COUNT(*)
					INTO count
					FROM existencia
					WHERE codabr=prod
				IF count <= 0 THEN
					FOR i = 1 TO v_existencia.getLength()
						INSERT INTO existencia VALUES (v_existencia[i].bodega, v_existencia[i].nombod,v_existencia[i].exiact,v_existencia[i].salcan,prod)
					END FOR
				ELSE
					FOR i = 1 TO v_existencia.getLength()
						UPDATE existencia SET salcan = v_existencia[i].salcan
							WHERE bodega=v_existencia[i].bodega
							AND   nombod=v_existencia[i].nombod 
							AND   codabr=prod
					END FOR
				END IF
				LET flag_act = TRUE
			END IF
			
	END INPUT
	
	CLOSE WINDOW existencias
   OPTIONS INPUT NO WRAP 

RETURN flag_act
	
END FUNCTION

FUNCTION inving003_llena_exis(nomabr)
DEFINE nomabr VARCHAR(30),
       i      SMALLINT   ,
		 sql    VARCHAR(512),
		 count  SMALLINT

	LET count=0
	SELECT COUNT(*)
	INTO count
	FROM existencia
	WHERE codabr=nomabr
	IF count = 0 THEN
		LET sql = " SELECT a.codbod , a.nombod, b.exican,0 ",
					 " FROM inv_mbodegas a , inv_proenbod b   ",
		  			 " WHERE a.codemp = b.codemp ",
		 			 " AND   a.codsuc = b.codsuc ",
			 		 " AND   a.codbod = b.codbod ",
					 " AND   b.exican > 0        ",
			 		 " AND   b.codabr = '", nomabr CLIPPED,"'"

		PREPARE sql FROM sql

		DECLARE existencia CURSOR FOR sql
		OPEN existencia
		CALL v_existencia.clear()
		LET i = 1
		FOREACH existencia INTO v_existencia[i].*
			LET i = i +1
		END FOREACH
		FOR i = 1 TO v_existencia.getLength()
			IF v_existencia[i].bodega IS NULL THEN
				CALL v_existencia.deleteElement(i)
			END IF
		END FOR
	ELSE
		DECLARE existencia2 CURSOR FOR
			SELECT bodega, nombod, exiact, cansal
				FROM existencia
				WHERE codabr=nomabr

		LET i = 1

		FOREACH existencia INTO v_existencia[i].*
			LET i = i +1
		END FOREACH

		FOR i = 1 TO v_existencia.getLength()
			IF v_existencia[i].bodega IS NULL THEN
				CALL v_existencia.deleteElement(i)
			END IF
		END FOR
	END IF

	CLOSE existencia

END FUNCTION

FUNCTION inving003_tmp_db(opc)
DEFINE opc SMALLINT

IF opc = 1 THEN
	LET flag_tmp = TRUE
	CREATE TEMP TABLE existencia (
    bodega  INTEGER,
	 nombod  VARCHAR(30),
	 exiact  DECIMAL(4,2),
	 salcan  DECIMAL(4,2),
	 codabr  VARCHAR(20)
	)
ELSE
	DROP TABLE existencia
	LET flag_tmp = FALSE
END IF
		

END FUNCTION
FUNCTION inving003_totexi()
  DEFINE total  DECIMAL(4,2),
			i      SMALLINT
	LET total=0
	FOR i = 1 to v_existencia.getLength()
		IF v_existencia[i].salcan IS NULL THEN
			LET v_existencia[i].salcan=0
		END IF
		LET total=total+v_existencia[i].salcan
	END FOR

RETURN total
END FUNCTION

FUNCTION inving003_listord()
DEFINE regresar   INTEGER,
       i     SMALLINT,
       arr   SMALLINT,
       numero INTEGER,
       query1 STRING      ,
       query  STRING     ,
       w_bus RECORD
         busque   VARCHAR(200),
			cambus   SMALLINT
		 END RECORD

	LET query1 = " SELECT a.lnkord , a.subcat , a.codcol , CASE WHEN a.medida = 1 THEN ",
                " (a.xlargo ||' X '|| a.yancho||' PIES ') WHEN a.medida = 0 THEN    ",
                " (a.xlargo ||' X '|| a.yancho||' METROS ') END , b.nomcli ",
					 " FROM inv_ordentra a , fac_mtransac b ",
					 " WHERE a.nofase = 2 AND a.lnktra = b.lnktra "
	OPEN WINDOW busqueda WITH FORM "inving003e.per"
	DIALOG ATTRIBUTES (UNBUFFERED)

		INPUT BY NAME w_bus.* ATTRIBUTES(WITHOUT DEFAULTS)

			BEFORE INPUT
				LET w_bus.cambus = 0

			ON CHANGE cambus
				IF w_bus.busque IS NOT NULL AND LENGTH(w_bus.busque) > 0 THEN
					IF w_bus.cambus = 0 THEN
						IF w_bus.cambus NOT MATCHES ">*" OR w_bus.cambus NOT MATCHES "<*" THEN
							LET numero = w_bus.busque
						END IF
						IF numero IS NULL THEN LET numero=0 END IF
						LET query = query1 ," AND lnkord = ",numero
					ELSE
						LET query = query1 ," AND nomcli MATCHES '",w_bus.busque CLIPPED,"'"
					END IF
					DISPLAY "ON CHANGE ",query
					CALL inving003_query(query)
					CALL ui.Interface.refresh()
				END IF

			AFTER FIELD busque
				IF w_bus.busque IS NOT NULL AND LENGTH(w_bus.busque) > 0 THEN
					IF w_bus.cambus = 0 THEN
						IF w_bus.cambus NOT MATCHES ">*" OR w_bus.cambus NOT MATCHES "<*" THEN
							LET numero = w_bus.busque
						END IF
						IF numero IS NULL THEN LET numero=0 END IF
						LET query = query1 ," AND lnkord = ",numero
						--LET query = query1 ," AND lnkord = ",w_bus.busque CLIPPED
					ELSE
						LET query = query1 ," AND nomcli MATCHES '",w_bus.busque CLIPPED,"'"
					END IF
						DISPLAY ".. ",query
					CALL inving003_query(query)
					CALL ui.Interface.refresh()
				END IF
	
			ON IDLE 1
				IF w_bus.busque IS NOT NULL AND LENGTH(w_bus.busque) > 0 THEN
					IF w_bus.cambus = 0 THEN
						IF w_bus.cambus NOT MATCHES ">*" OR w_bus.cambus NOT MATCHES "<*" THEN
							LET numero = w_bus.busque
						END IF
						IF numero IS NULL THEN LET numero=0 END IF
						LET query = query1 ," AND lnkord = ",numero
						--LET query = query1 ," AND lnkord = ",w_bus.busque CLIPPED
					ELSE
						LET query = query1 ," AND nomcli MATCHES '",w_bus.busque CLIPPED,"'"
					END IF
					CALL inving003_query(query)
					CALL ui.Interface.refresh()
				ELSE
					CALL ord.clear()
				END IF

		END INPUT

		DISPLAY ARRAY ord     TO tabl1.*

			BEFORE ROW 
				LET i=arr_curr()

		END DISPLAY

		ON ACTION ACCEPT
			EXIT DIALOG

	END DIALOG
	CLOSE WINDOW busqueda

RETURN FALSE
END FUNCTION

FUNCTION inving003_query(query)
DEFINE query STRING,
		 i     SMALLINT,
		 o     SMALLINT

	CALL ord.clear()
	PREPARE busq FROM query
	WHENEVER ERROR CONTINUE
	DECLARE bus CURSOR FOR busq
	LET i = 1

	FOREACH bus INTO ord[i].*

		LET i = i + 1

	END FOREACH
	LET o = 1 
{
	FOR i = 1 to o
		IF ord[i].orden IS NULL THEN
			CALL ord.deleteElement(i)
		END IF
	END FOR
}

	WHENEVER ERROR STOP
END FUNCTION

FUNCTION inving003_llena()
DEFINE despliega RECORD
		  fecord DATE,
		  subcat INTEGER,
		  codcol INTEGER,
		  xlargo DECIMAL(10,2),
		  yancho DECIMAL(10,2),
		  medida INTEGER,
		  nomcli VARCHAR(100)
		END RECORD
		

SELECT fecord, subcat , codcol ,
       xlargo, yancho, medida
  INTO despliega.fecord , despliega.subcat , despliega.codcol ,
		 despliega.xlargo , despliega.yancho , despliega.medida
  FROM inv_ordentra
 WHERE lnkord = w_mae_tra.lnkord

SELECT fac_mtransac.nomcli
  INTO despliega.nomcli
  FROM fac_mtransac , inv_ordentra
 WHERE fac_mtransac.lnktra = inv_ordentra.lnktra
	AND inv_ordentra.lnkord = w_mae_tra.lnkord

DISPLAY BY NAME despliega.*

END FUNCTION
