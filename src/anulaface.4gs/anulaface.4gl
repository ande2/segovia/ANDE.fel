DATABASE segovia

DEFINE gr_rec record LIKE fac_mtransac.*
DEFINE i, j SMALLINT
DEFINE numini, numfin INTEGER 
DEFINE lfecanul DATE 

MAIN

   --Parametros
   --numpos 1=zona9 2=zona 17 3=zona 11 5=Cenma 7=zona 9-pos2      
   LET gr_rec.numpos = 5
   LET gr_rec.nserie = 'C'  
   LET numini = 8093
   LET numfin = 8500

   --colocar fecha de anulacion
   LET lfecanul      = "30/06/2020"
   
   LET gr_rec.fecemi = lfecanul
   LET gr_rec.fecsis = lfecanul
   LET gr_rec.horsis = '10:31:42'

   LET gr_rec.usranl = 'sistemas'
   LET gr_rec.fecanl = lfecanul 
   LET gr_rec.horanl = '10:31:43' 

   LET j = 1 --registros a procesar
   
   --Fijos
   LET gr_rec.lnktra = 0
   LET gr_rec.codemp = 1
   LET gr_rec.lnktdc = 22
   LET gr_rec.tipdoc = 1
   LET gr_rec.motanl = "GRABADA COMO ANULADA PORQUE SE VENCIERON LAS FACTURAS"
   LET gr_rec.codcli = 1
   LET gr_rec.numnit = 'ANULADA'
   LET gr_rec.nomcli = 'ANULADA'
   LET gr_rec.dircli = 'ANULADA'
   LET gr_rec.telcli = NULL  
   LET gr_rec.credit = 0
   LET gr_rec.hayord = 0
   LET gr_rec.ordcmp = NULL  
   LET gr_rec.obspre = NULL 
   LET gr_rec.exenta = 0
   LET gr_rec.moneda = 1
   LET gr_rec.tascam = 8.145262
   LET gr_rec.efecti = 0.0
   LET gr_rec.cheque = 0.0
   LET gr_rec.tarcre = 0.0
   LET gr_rec.ordcom = 0.0
   LET gr_rec.depmon = 0.0
   LET gr_rec.subtot = 0.00
   LET gr_rec.totdoc = 0.00
   LET gr_rec.totiva = 0.00
   LET gr_rec.poriva = 0.0
   LET gr_rec.totpag = 0.0
   LET gr_rec.saldis = 0.0

   --estado A=Anulado V=Vigente
   LET gr_rec.estado = 'A'

   LET gr_rec.feccor = NULL 
   LET gr_rec.usrope = 'sistemas'
   LET gr_rec.userid = 'sistemas'
   LET gr_rec.lnkinv = 1

   --BEGIN WORK
   FOR i = numini TO numfin
      LET gr_rec.numdoc = i
      DISPLAY gr_rec.*

      INSERT INTO fac_mtransac(lnktra, numpos, codemp, lnktdc, tipdoc, nserie, numdoc, 
               fecemi, codcli, numnit, nomcli, dircli, telcli, credit, 
               hayord, ordcmp, obspre, exenta, moneda, tascam, efecti, cheque, tarcre, 
               ordcom, depmon, subtot, totdoc, totiva, poriva, totpag, saldis, estado, 
               feccor, usrope, userid, fecsis, horsis, motanl, usranl, fecanl, horanl, lnkinv) 
      VALUES(gr_rec.lnktra, gr_rec.numpos, gr_rec.codemp, gr_rec.lnktdc, gr_rec.tipdoc, gr_rec.nserie, gr_rec.numdoc, 
               gr_rec.fecemi, gr_rec.codcli, gr_rec.numnit, gr_rec.nomcli, gr_rec.dircli, gr_rec.telcli, gr_rec.credit, 
               gr_rec.hayord, gr_rec.ordcmp, gr_rec.obspre, gr_rec.exenta, gr_rec.moneda, gr_rec.tascam, gr_rec.efecti, gr_rec.cheque, gr_rec.tarcre, 
               gr_rec.ordcom, gr_rec.depmon, gr_rec.subtot, gr_rec.totdoc, gr_rec.totiva, gr_rec.poriva, gr_rec.totpag, gr_rec.saldis, gr_rec.estado, 
               gr_rec.feccor, gr_rec.usrope, gr_rec.userid, gr_rec.fecsis, gr_rec.horsis, gr_rec.motanl, gr_rec.usranl, gr_rec.fecanl, gr_rec.horanl, gr_rec.lnkinv)

      LET j = j + 1
   END FOR
   --ROLLBACK WORK
   DISPLAY "Total de registros procesados: ", j
END MAIN
