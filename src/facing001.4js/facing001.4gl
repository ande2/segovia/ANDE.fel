{ 
Fecha    : Enero 2011           
Programo : Mynor Ramirez
Objetivo : Programa de facturacion.
---------------------------------------------------------------------------------------------------------------
Cambios
---------------------------------------------------------------------------------------------------------------
Programo : Carlos Enrique Morales

1.  Se habilito el campo de orden de compra para cualquier ingreso
2.  Se agrego el ingreso del numero de telefono del cliente 
3.  Se modifico la forma de ontener los datos del maestro de clientes y que se refresquen los datos.  18/03/11 
4.  Se habilito el campo de credit para cualquier ingreso
5.  Se deshabilito la verificacion de si producto maneja precio minimo, solo se muestra mensaje de advertencia 
6.  Se agrego que cuando no exista precio sugerido este sea ingresado, actualizando de una vez el maestro de 
    productos.
7.  Se elimino la verificacion de si la orden es nula en el ingreso de datos de la orden de trabajo.  23/03/11 
8.  Se elimino la validacion del campo de descripcion de la orden de trabajao para que pueda ir en blanco
9.  Se elimino la validacion del campo de documentos de la orden de trabajao para que pueda ir en blanco
10.  Se valido para ingresar el precio por pie cuadrado 
11. Se elimino el caclulo del precio por factor de pie cuadrado, y se valido para que se ignresar el precio 
    directamente

Yo Carlos Enrique
Rectifico:
	Punto 11: No se elimino el calculo del precio, se agrego que si cambian el precio, cambie el precio de precio por pie.
}

-- Definicion de variables globales

GLOBALS "facglb001.4gl"
CONSTANT programa = "facing001" 

DEFINE tasimp      DEC(9,6), 
       tascom      DEC(9,6), 
       tasvta      DEC(9,6), 
       vuelto      DEC(10,2), 
       portar      DEC(9,6),
       facord      DEC(9,6),
       haytar      SMALLINT, 
       wpais       VARCHAR(255), 
       regreso     SMALLINT,
       ordenes     SMALLINT, 
       existe      SMALLINT,
       msg         STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

-- Cargando estilos y acciones default
CALL ui.Interface.loadActionDefaults("actiondefaults")
CALL ui.Interface.loadStyles("styles")
CALL ui.Interface.loadToolbar("toolbar9")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facing001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Logeando usuario 
 CALL facing001_login(0)
 RETURNING regreso,username 
 LET flag_tmp=FALSE

 -- Verificanod regreso
 IF NOT regreso THEN 
    -- Menu de opciones
    CALL facing001_menu()
 END IF 
END MAIN

-- Subutina para el menu de facturacion 

FUNCTION facing001_menu()
 DEFINE regreso    SMALLINT, 
        titulo     STRING,
       vfac_id INTEGER,
      cmd STRING 

 -- Abriendo la ventana del mantenimiento
  OPEN WINDOW wing001a AT 5,2  
  WITH FORM "facing001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("facing001",wpais,1)

  -- Desplegando campo de motivo de anulacion
  CALL f.setFieldHidden("motanl",1)
  CALL f.setElementHidden("labeld",1)
  CALL f.setElementHidden("group4",1)
  CALL f.setElementHidden("labelg",1)
  CALL f.setFieldHidden("obspre",1)
  CALL f.setElementHidden("group7",1)

  -- Cargando unidades de medida
  CALL librut003_cbxunidadesmedida("unimto") 

  -- Inicializando datos 
  CALL facing001_inival(1)

  CALL facing001_setenv()

  -- Menu de opciones
  MENU " Opciones" 
  {
   ON ACTION Fac_electronica

      --IF jjjjjj
      LET vfac_id = NULL;
      
      SELECT a.fac_id
      INTO vfac_id
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      
      LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
      RUN cmd
      }
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
	 SHOW OPTION ALL
    IF NOT seclib001_accesos(username,4) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(username,1) THEN
       HIDE OPTION "Ingresar"
    END IF
    -- Anular     
    IF NOT seclib001_accesos(username,2) THEN
       HIDE OPTION "Anular"
    END IF
   -- Eliminar
    IF NOT seclib001_accesos(username,3) THEN
       HIDE OPTION "delete"
	ELSE
		SHOW OPTION "delete"
    END IF
	 
   COMMAND "Consultar" 
    " Consulta de documentos de facturacion existentes."
    CALL facqbx001_facturacion(1)
    
   COMMAND "Ingresar" 
    " Emision de facturacion."
    LET ordenes = FALSE 
    CALL facing001_facturacion(1) 
    
   COMMAND "Ordenes" 
    " Emision de facturacion con ordenes de trabajo."
    LET ordenes = TRUE
	 --LET w_mae_tra.hayord = TRUE
    CALL facing001_facturacion(1) 
   COMMAND "Anular"
    " Anulacion de documentos de facturacion existentes."
    CALL facqbx001_facturacion(2)
	 
	COMMAND "delete"
    " Eliminación de documentos de facturacion existentes."
    IF username = "ximena" THEN
        CALL facqbx001_facturacion(3)
    ELSE
         CALL fgl_winmessage(
           "Atencion",
           "Usuario no Autorizado.",
           "stop")                 
    END IF
	 
   COMMAND "Salir"
    " Abandona el menu de facturacion." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

FUNCTION facing001_setenv()
   LET ande_desa = fgl_getenv("ANDE_DESA") --1=Si 2=No
END FUNCTION 

-- Subrutina para ingresar el cajero y el punto de venta
FUNCTION facing001_login(opc)
 DEFINE x         RECORD 
         userid   CHAR(15),
         passwd   CHAR(10),
         numpos   SMALLINT 
        END RECORD,
        wfecha    VARCHAR(80), 
        xnompos   VARCHAR(40), 
        msg       STRING, 
        regreso   SMALLINT,
        loop      SMALLINT,
        seleccion SMALLINT,
        conteo    SMALLINT,
        opc       SMALLINT,
        w2        ui.Window,
        f2        ui.Form,
        sql_stmt STRING 

 -- Abriendo la ventana del login
 OPEN WINDOW wing001b AT 5,2  
  WITH FORM "facing001b" ATTRIBUTE(BORDER)

  -- Desplegando fecha 
  LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
  CALL librut001_dpelement("labelz",wfecha)

  -- Desactivando salida automatica del input 
  OPTIONS INPUT WRAP 

  -- Escondiendo punto de venta
  LET w2 = ui.Window.getCurrent()
  LET f2 = w2.getForm()
  --CALL f2.setFieldHidden("formonly.numpos",1)
  
  --CALL f2.setElementHidden("labelx",1)
  CALL f2.setElementHidden("labelb",1)
  CALL f2.setElementHidden("labelc",1)

  -- Verificando si cajero es ingresado o tomado del sistema 
  INITIALIZE x.* TO NULL 
  IF (opc=1) THEN 
     -- Obteniendo cajero logeado
     LET x.userid = FGL_GETENV("LOGNAME")
     DISPLAY BY NAME x.userid 
  END IF 

  -- Ingresando datos
  LET loop = TRUE
  WHILE loop
   INPUT BY NAME x.userid,
                 x.passwd, x.numpos WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)
   
    ON ACTION cancelar
     -- Salida 
     LET regreso = TRUE
     LET loop    = FALSE 
     EXIT INPUT 

    ON KEY(F4,CONTROL-E) 
     -- Salida 
     LET regreso = TRUE 
     LET loop    = FALSE 
     EXIT INPUT 

    ON ACTION aceptar  
     -- Verificando datos
     IF x.userid IS NULL THEN
        ERROR "Error: usuario invalido, VERIFICA ..."
        NEXT FIELD userid
     END IF 
     IF x.passwd IS NULL THEN
        ERROR "Error: password invalido, VERIFICA ..."
        NEXT FIELD passwd
     END IF 

     LET regreso = FALSE
     LET loop    = TRUE 
     EXIT INPUT 

    BEFORE INPUT
     -- Verificando si cajero es ingresado o tomado del sistema 
     IF (opc=1) THEN
        CALL Dialog.SetFieldActive("userid",0) 
     END IF 

    AFTER FIELD userid 
     -- Verificando usuario 
     IF (LENGTH(x.userid)=0) THEN 
        ERROR "Error: usuario invalido, VERIFICA ..."
        NEXT FIELD userid
     ELSE 
        LET sql_stmt = "SELECT u.numpos, p.nompos FROM fac_usuaxpos u,fac_puntovta p WHERE u.numpos = p.numpos AND u.userid = '", x.userid CLIPPED, "'" 
        --CALL combo_din2("numpos",sql_stmt)
        CALL librut002_combobox("numpos",sql_stmt)
     END IF 

    AFTER FIELD passwd
     -- Verificando password
     IF (LENGTH(x.passwd)=0) THEN 
        ERROR "Error: password invalido, VERIFICA ..."
        NEXT FIELD passwd
     END IF 
    AFTER FIELD numpos
      IF (LENGTH(x.numpos)=0) THEN
         ERROR "Seleccione Punto de Venta"
         NEXT FIELD numpos
      END IF
     EXIT INPUT  
   END INPUT

   -- Verificando puntos de venta por cajero
   IF loop THEN 
    -- Buscando puntos de venta por cajero 
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_usuaxpos a
     WHERE a.userid = x.userid
     IF (conteo=0) THEN
        LET msg = "Cajero (",x.userid CLIPPED,") no tiene puntos de venta asignados. \n VERIFICA ..."

        CALL fgl_winmessage(
        " Atencion",
        msg, 
        "stop")
        CONTINUE WHILE 
     ELSE
        LET xnumpos = NULL
        SELECT a.numpos 
         INTO  xnumpos   
         FROM  fac_usuaxpos a,fac_puntovta y
         WHERE a.numpos = y.numpos 
           AND a.userid = x.userid
           AND a.passwd = x.passwd
         IF (status=NOTFOUND) THEN
            ERROR "Atencion: password incorrecto, VERIFICA ..."
            CONTINUE WHILE  
         END IF 
         LET xnumpos = x.numpos
         LET loop = FALSE 
     END IF 
   END IF 
  END WHILE

  -- Activando salida automatica del input 
  OPTIONS INPUT NO WRAP 
  
 CLOSE WINDOW wing001b

 RETURN regreso,x.userid 
END FUNCTION 

-- Subrutina para el ingreso de los datos del encabezado de la facturacion 

FUNCTION facing001_facturacion(operacion)
 DEFINE userauth          LIKE glb_permxusr.userid,
        xnomcli           LIKE fac_mtransac.nomcli, 
        xnumnit           LIKE fac_mtransac.dircli,
        xdircli           LIKE fac_mtransac.dircli,
        xtelcli           LIKE fac_mtransac.telcli,
        xdocto            CHAR(20), 
        retroceso         SMALLINT,
        operacion,acceso  SMALLINT,
        loop,existe,i     SMALLINT

 -- Obteniendo tasa de impuesto
 CALL librut003_parametros(8,1)
 RETURNING existe,tasimp
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de impuesto.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de dolares compra
 CALL librut003_parametros(9,1)
 RETURNING existe,tascom
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de compra de dolares.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de venta de dolares 
 CALL librut003_parametros(9,2)
 RETURNING existe,tasvta
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de compra de dolares.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de incremento por uso de tc
 CALL librut003_parametros(11,1)
 RETURNING existe,portar
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrado el % de incremento por uso de TARJETA DE CREDITO.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo factor de conversion de medida de ordenes de trabajo
 CALL librut003_parametros(12,1)
 RETURNING existe,facord 
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrado el factor de conversion para ordenes de trabajo..\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Inicio del loop
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF 

 -- Verificando si hay orden de trabajo
 IF ordenes THEN 
    -- Deshabilitando tabla de productos
    CALL f.setElementHidden("tabla1",1)
    CALL f.setElementHidden("group6",1)
    CALL f.setElementHidden("group4",0)
    CALL f.setElementHidden("group5",1)
    CALL f.setElementHidden("labelc",1)
    CALL f.setElementHidden("label27",1)
    CALL f.setFieldHidden("compre",1)
 END IF 

 LET loop  = TRUE
 WHILE loop   

 	LET flag_especial = FALSE
	LET sin_orden = 0

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL facing001_inival(1) 
     CALL f.setFieldHidden("preuni",0)
     CALL f.setFieldHidden("totpro",0)
     CALL f.setFieldHidden("totval",0)
     CALL f.setFieldHidden("predol",1)
     CALL f.setFieldHidden("prtdol",1)
     CALL f.setFieldHidden("totvaldol",1)
     CALL f.setElementHidden("group7",1)
  END IF

  IF flag_tmp= FALSE THEN
     CALL facing001_tmp_db(1)
  END IF
  -- Ingresando datos
  INPUT BY NAME w_mae_tra.lnktdc,
                w_mae_tra.numdoc,
		          w_mae_tra.fecemi,
                w_mae_tra.codcli,
                w_mae_tra.tipfac,
                w_mae_tra.tipoid,
                w_mae_tra.numnit,
                w_mae_tra.nomcli,
                w_mae_tra.dircli,
                w_mae_tra.telcli,
                w_mae_tra.maicli,
                w_mae_tra.credit,
                w_mae_tra.tascam,
                w_mae_tra.ordcmp,
                w_mae_tra.incote,
                w_mae_tra.nomcom,
                w_mae_tra.dircom,
                w_mae_tra.otrref WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Escondiendo botom de modificar el numero de documento y boton de anular numero de documento  
    CALL DIALOG.setActionActive("modnumdoc",FALSE)
    CALL DIALOG.setActionActive("anunumdoc",FALSE)

    -- Deshabilitando campo de numero de documento 
    CALL DIALOG.setFieldActive("numdoc",FALSE)

    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(lnktdc) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        CALL Dialog.SetFieldActive("lnktdc",TRUE) 
        CALL facing001_inival(1)
        NEXT FIELD lnktdc 
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

    --GYanes 27032020 manejo de tipo de facturacion LOCAL o EXPORTACION
   ON CHANGE tipfac
      IF w_mae_tra.tipfac = "L" THEN
         LET w_mae_tra.tascam = NULL
         CALL f.setFieldHidden("preuni",0)
         CALL f.setFieldHidden("totpro",0)
         CALL f.setFieldHidden("totval",0)
         CALL f.setFieldHidden("predol",1)
         CALL f.setFieldHidden("prtdol",1)
         CALL f.setFieldHidden("totvaldol",1)
         CALL f.setElementHidden("group7",1)
      ELSE
         LET w_mae_tra.tascam = tascom
         CALL f.setFieldHidden("preuni",1)
         CALL f.setFieldHidden("totpro",1)
         CALL f.setFieldHidden("totval",1)
         CALL f.setFieldHidden("predol",0)
         CALL f.setFieldHidden("prtdol",0)
         CALL f.setFieldHidden("totvaldol",0)
         CALL f.setElementHidden("group7",0)
      END IF
      DISPLAY BY NAME w_mae_tra.tascam

   
   ON CHANGE lnktdc 
    -- Obteniendo datos del tipo de documento x punto de venta 
    INITIALIZE w_mae_tdc.* TO NULL
    LET es_dte=NULL 
    CALL librut003_btdocxpos(w_mae_tra.lnktdc) 
    RETURNING w_mae_tdc.*,existe 
    

    -- Asignando datos de la empresa, tipo de documento, y serie 
    LET w_mae_tra.codemp = w_mae_tdc.codemp 
    LET w_mae_tra.tipdoc = w_mae_tdc.tipdoc 
    LET w_mae_tra.nserie = w_mae_tdc.nserie 
	 IF w_mae_tra.tipdoc = 1 THEN
      LET es_dte="S"
	 	CALL fgl_winmessage("Atention","Usted ha elegido una Factura"," information")
      
	 ELSE
		IF w_mae_tra.tipdoc = 2 THEN
         LET es_dte="N"
			CALL fgl_winmessage("Atention","Usted ha elegido un Ticket"," information")
      ELSE 
         --ERROR "Tipo de documento ", w_mae_tra.tipdoc 
         IF w_mae_tra.tipdoc = 4 THEN 
            LET es_dte="S"
            CALL fgl_winmessage("Atention","Usted ha elegido una Factura EXENTA"," information")
         END IF    
		END IF
	 END IF
    DISPLAY BY NAME w_mae_tra.nserie 
    -- Obteniendo datos de la empresa 
    INITIALIZE w_mae_emp.* TO NULL 
    CALL librut003_bempresa(w_mae_tra.codemp)
    RETURNING w_mae_emp.*,existe 

   ON ACTION modnumdoc 
    -- Verificando acceso para modificar el numero de documento
    -- Ingresando confirmacion de cajero
    CALL facing001_confirmacion() 
    RETURNING acceso,userauth 
    IF acceso THEN 
       -- Habilitando campo de numero de documento 
       CALL DIALOG.setFieldActive("numdoc",TRUE)
       CALL DIALOG.setFieldActive("fecemi",TRUE)
       NEXT FIELD numdoc 
    END IF 

   ON ACTION anunumdoc
    -- Obteniendo numero de documento 
    LET xdocto = GET_FLDBUF(w_mae_tra.numdoc)
    LET w_mae_tra.numdoc = xdocto
    IF LENGTH(w_mae_tra.numdoc)=0 THEN
       ERROR "Error: debe ingresarse el numero de documento, VERIFICA ..."
       NEXT FIELD numdoc 
    END IF 

    -- Verificando si el documento existe
    IF facing001_bdocumento() THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Numero de documento ya existe registrado, \n No puede ANULARSE con esta opcion.",
       "stop")
       NEXT FIELD numdoc
    END IF 

    -- Anulando documentos no grabados 
    -- Verificando anulacion
    IF librut001_yesornot("Confirmacion",
                          "Esta Seguro de Anular el Documento ?",
                          "Si",
                          "No",
                          "question") THEN

       -- Grabando documento como anulado 
       CALL facing001_anular(userauth)

       -- Inicializando datos
       CALL Dialog.setActionActive("anunumdoc",FALSE)
       CALL Dialog.SetFieldActive("lnktdc",TRUE) 
       CALL facing001_inival(1)
       NEXT FIELD lnktdc 
    END IF

   ON ACTION listcliente 
    -- Seleccionado lista de clientes
    CALL librut002_formlistcli("Consulta de Clientes","Cliente","Nombre del Cliente","Numero de Nit",
                               "codcli","nomcli","numnit","fac_clientes","1=1",2,1,1)
    RETURNING w_mae_tra.codcli,w_mae_tra.nomcli,w_mae_tra.numnit,regreso
    IF regreso THEN
       LET w_mae_tra.codcli = NULL 
       LET w_mae_tra.nomcli = NULL 
       LET w_mae_tra.numnit = NULL 
       LET w_mae_tra.dircli = NULL 
       CLEAR codcli,nomcli,numnit,dircli 
       NEXT FIELD codcli
    ELSE 
       DISPLAY BY NAME w_mae_tra.codcli,w_mae_tra.nomcli,w_mae_tra.numnit 
       NEXT FIELD codcli
    END IF 

   BEFORE INPUT
    -- Deshabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)
    -- Deshabilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",FALSE)
    -- Deshabilitando campo de numero de documento 
    CALL DIALOG.setFieldActive("numdoc",FALSE)
    CALL DIALOG.setFieldActive("fecemi",FALSE)
    IF w_mae_tra.tipfac = "L" THEN
         LET w_mae_tra.tascam = NULL
         CALL f.setFieldHidden("preuni",0)
         CALL f.setFieldHidden("totpro",0)
         CALL f.setFieldHidden("totval",0)
         CALL f.setFieldHidden("predol",1)
         CALL f.setFieldHidden("prtdol",1)
         CALL f.setFieldHidden("totvaldol",1)
         CALL f.setElementHidden("group7",1)
      ELSE
         LET w_mae_tra.tascam = tascom
         CALL f.setFieldHidden("preuni",1)
         CALL f.setFieldHidden("totpro",1)
         CALL f.setFieldHidden("totval",1)
         CALL f.setFieldHidden("predol",0)
         CALL f.setFieldHidden("prtdol",0)
         CALL f.setFieldHidden("totvaldol",0)
         CALL f.setElementHidden("group7",0)
      END IF
      DISPLAY BY NAME w_mae_tra.tascam

   BEFORE FIELD lnktdc 
    -- Verificando si es regreso se mueve al campo de cliente omitiendo el campo de tipo de documento
    IF retroceso THEN
       CALL Dialog.SetFieldActive("lnktdc",FALSE) 
       LET retroceso = FALSE 
    END IF 

   AFTER FIELD lnktdc
    -- Verificando tipo de documento
    IF w_mae_tra.lnktdc IS NULL THEN
       ERROR "Error: tipo de documento invalido, VERIFICA ..."
       NEXT FIELD lnktdc 
    END IF 
    CALL DIALOG.setFieldActive("codcli",TRUE)
 
    -- Obteniendo datos del tipo de documento 
    INITIALIZE w_tip_doc.* TO NULL
    CALL librut003_btipodoc(w_mae_tra.tipdoc)
    RETURNING w_tip_doc.*,regreso
    -- Verificando si el documento existe 
    SELECT UNIQUE a.codemp
     FROM  fac_mtransac a
     WHERE (a.codemp = w_mae_tra.codemp)
       AND (a.tipdoc = w_mae_tra.tipdoc)
       AND (a.nserie = w_mae_tra.nserie)
    IF (status=NOTFOUND) THEN 
       -- Verificando si existe resolucion 
       INITIALIZE w_mae_res.* TO NULL 
       CALL librut003_bresolucion(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie)
       RETURNING w_mae_res.*,existe
       IF existe THEN 
          -- Obteniendo correlativo inicial de la resolucion 
          LET w_mae_tra.numdoc = w_mae_res.numini 
       ELSE
          -- Obteniendo correaltivo inicial del tipo de documento x punto de venta
          LET w_mae_tra.numdoc = w_mae_tdc.numcor
       END IF 
    ELSE
       -- Buscando correlativo del tipo de documento 
       LET w_mae_tra.numdoc = librut003_correltipdoc(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie,0,0)

       -- Verificando si existe resolucion 
       INITIALIZE w_mae_res.* TO NULL 
       CALL librut003_bresolucion(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie)
       RETURNING w_mae_res.*,existe
       IF existe THEN
          -- Verificando si el correlativo esta fuera de la resolucion 
          IF w_mae_tra.numdoc >= w_mae_res.numini AND
             w_mae_tra.numdoc <= w_mae_res.numfin THEN
             -- Correlativo correcto
          {ELSE
             CALL fgl_winmessage(
             " Atencion",
             " Numero de documento fuera de resolucion. \n VERIFICA correlativos de la resolucion.", 
             "stop")}
          END IF 
       END IF 
    END IF 

    -- Desplegando numero de documento
	 IF w_tip_doc.tipdoc = 2 AND w_mae_tra.numpos=1 AND w_mae_tra.nserie = "G" THEN
       CALL Dialog.SetFieldActive("numnit",0)
       CALL Dialog.SetFieldActive("nomcli",0)
       CALL Dialog.SetFieldActive("dircli",0)
       CALL Dialog.SetFieldActive("telcli",0)
		 LET w_mae_tra.codcli = 54
	 ELSE

	 	 LET w_mae_tra.codcli=1
		 --CALL DIALOG.setFieldActive("numnit",0)
		 --CALL DIALOG.setFieldActive("nomcli",0)
		 --CALL DIALOG.setFieldActive("dircli",0)
		 --CALL DIALOG.setFieldActive("telcli",0)
	 END IF

   BEFORE FIELD numdoc 
    -- Habilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",TRUE)
    -- DesHabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)

   AFTER FIELD numdoc
    -- Verificando campo
    IF w_mae_tra.numdoc IS NULL OR
       (w_mae_tra.numdoc <=0) THEN 
       ERROR "Error: numero de documento invalido, VERIFICA ..." 
       LET w_mae_tra.numdoc = NULL
       CLEAR numdoc
       NEXT FIELD numdoc  
    END IF 

    -- Verificanod si numero de documento existe
    IF facing001_bdocumento() THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Numero de documento ya existe registrado. \n VERIFICA ...",
       "stop")
       NEXT FIELD numdoc 
    END IF 

    -- Deshabilitando campo de numero de documento 
    CALL DIALOG.setFieldActive("numdoc",FALSE)

    -- Deshabilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",TRUE)

    -- Habilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",FALSE)

--   BEFORE FIELD codcli
    -- Habilitando botom de modificar el numero de documento
--	 LET w_mae_tra.codcli=1
--	 NEXT FIELD NEXT

	AFTER FIELD fecemi
		CALL DIALOG.setFieldActive("fecemi",FALSE)
   --ON CHANGE codcli 
   BEFORE FIELD codcli 
    -- Verificando cliente
    IF w_mae_tra.codcli IS NULL THEN
       ERROR "Error: debe ingresarse el cliente a facturar, VERIFICA ..." 
       LET w_mae_tra.codcli = NULL 
       LET w_mae_tra.nomcli = NULL 
       LET w_mae_tra.numnit = NULL
       LET w_mae_tra.dircli = NULL 
       LET w_mae_tra.telcli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli
    END IF 

    -- Buscando datos del cliente
	IF w_mae_tra.numnit IS NULL AND w_mae_tra.nomcli IS NULL THEN
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.codcli)
    RETURNING w_mae_cli.*,existe 
    IF NOT existe THEN
       INITIALIZE w_mae_tra.codcli TO NULL 
       CALL fgl_winmessage(" Atencion","Cliente no existe registrado. \nVERIFICA ...","stop")
       LET w_mae_tra.codcli = NULL 
       LET w_mae_tra.nomcli = NULL 
       LET w_mae_tra.numnit = NULL
       LET w_mae_tra.dircli = NULL 
       LET w_mae_tra.telcli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli 
    END IF 

    -- Obteniendo datos del maestro de clientes 
    IF w_mae_tra.numnit <> "C/F" OR w_mae_tra.numnit <> "CF" OR w_mae_tra.nomcli <> "CONSUMIDOR FINAL" 
       OR w_mae_tra.nomcli <> "CLIENTES VARIOS" OR
       w_mae_tra.numnit IS NULL OR w_mae_tra.nomcli IS NULL THEN
       LET w_mae_tra.nomcli = w_mae_cli.nomcli
       LET w_mae_tra.numnit = w_mae_cli.numnit 
       LET w_mae_tra.dircli = w_mae_cli.dircli
       LET w_mae_tra.telcli = w_mae_cli.numtel
    END IF
    DISPLAY BY NAME w_mae_tra.nomcli,w_mae_tra.numnit,w_mae_tra.dircli,w_mae_tra.telcli 
	END IF
    -- Verificando si cliente esta autorizado para facturar
    IF NOT w_mae_cli.hayfac THEN
       CALL fgl_winmessage(" Atencion"," Cliente no autorizado para facturar. \n VERIFICA ...","stop")
       LET w_mae_tra.codcli = NULL 
       LET w_mae_tra.nomcli = NULL 
       LET w_mae_tra.numnit = NULL
       LET w_mae_tra.dircli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli 
    END IF 

    -- Verificando si cliente no tiene ingreso de datos
    IF NOT w_mae_cli.haydat THEN
       IF LENGTH(w_mae_tra.numnit)=0 THEN LET w_mae_tra.numnit = "CF" END IF 
       IF LENGTH(w_mae_tra.nomcli)=0 THEN LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" END IF 
       IF LENGTH(w_mae_tra.dircli)=0 THEN LET w_mae_tra.dircli = "CIUDAD" END IF 
       CALL Dialog.SetFieldActive("numnit",0) 
       CALL Dialog.SetFieldActive("nomcli",0) 
       CALL Dialog.SetFieldActive("dircli",0) 
       CALL Dialog.SetFieldActive("telcli",0) 
    ELSE 
       DISPLAY BY NAME w_mae_tra.numnit,w_mae_tra.nomcli,w_mae_tra.dircli,w_mae_tra.telcli
       CALL Dialog.SetFieldActive("numnit",1) 
       CALL Dialog.SetFieldActive("nomcli",1) 
       CALL Dialog.SetFieldActive("dircli",1) 
       CALL Dialog.SetFieldActive("telcli",1) 
    END IF 
	 NEXT FIELD NEXT

    -- Deshabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)

{    AFTER FIELD codcli
     IF w_mae_tra.codcli IS NULL THEN
       LET w_mae_tra.codcli = NULL
       LET w_mae_tra.nomcli = NULL
       LET w_mae_tra.numnit = NULL
       LET w_mae_tra.dircli = NULL
       LET w_mae_tra.telcli = NULL
       CALL fgl_winmessage(" Atencion","Debe ingresarse el cliente. \nVERIFICA ...","stop")
       CLEAR codcli,nomcli,numnit,dircli,telcli
       NEXT FIELD codcli
     END IF
}
	BEFORE FIELD numnit
    CALL DIALOG.setFieldActive("codcli",FALSE)
    CALL DIALOG.setActionActive("modnumdoc",TRUE)
   -- LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 

   AFTER FIELD numnit
    -- Verificando nit
    IF LENGTH(w_mae_tra.numnit)=0 THEN 
       LET w_mae_tra.numnit = "CF" 
       DISPLAY BY NAME w_mae_tra.numnit 
    ELSE
      IF w_mae_tra.numnit = "C/F" OR w_mae_tra.numnit = "C.F." OR w_mae_tra.numnit = "C F" THEN 
         LET w_mae_tra.numnit = "CF" 
         DISPLAY BY NAME w_mae_tra.numnit 
      END IF
		IF w_mae_tra.codcli = 0 OR w_mae_tra.codcli = 1 THEN
       -- Verificando si cliente ya existe registrado por el numero de NIT
       INITIALIZE xnomcli,xdircli TO NULL
       IF w_mae_tra.numnit!="CF" THEN 
		  INITIALIZE xnumnit,xdircli,xtelcli TO NULL
        SELECT a.nomcli,a.dircli,a.telcli 
         INTO  xnomcli,xdircli,xtelcli
         FROM  fac_mtransac a
         WHERE a.lnktra = (SELECT MAX(x.lnktra)
                            FROM  fac_mtransac x
                            WHERE x.numnit = w_mae_tra.numnit)
         IF (status!=NOTFOUND) THEN
           -- Asignando datos
            LET w_mae_tra.nomcli = xnomcli  
            LET w_mae_tra.dircli = xdircli
            LET w_mae_tra.telcli = xtelcli
            DISPLAY BY NAME w_mae_tra.nomcli,w_mae_tra.dircli,w_mae_tra.telcli 
         END IF 
       END IF 
		 END IF
    END IF
    IF w_mae_tra.numnit <> "CF" THEN
      IF w_mae_tra.tipfac = "L" THEN  
         IF w_mae_tra.tipoid = 0 THEN 
            IF NOT validarNIT(w_mae_tra.numnit) THEN
               CALL fgl_winmessage(
               " Atencion",
               " El NIT es inv�lido. \n INGRESE de nuevo ...",
               "stop")
               NEXT FIELD numnit
            END IF
         END IF
      END IF    
  END IF  
  --Asignado codigo de cliente según NIT
  SELECT codcli INTO w_mae_tra.codcli FROM fac_clientes WHERE numnit = w_mae_tra.numnit
  IF sqlca.sqlcode = 0 THEN DISPLAY BY NAME w_mae_tra.codcli END IF 
  
    -- Deshabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)

   AFTER FIELD nomcli 
    -- Verificando nombre
    IF LENGTH(w_mae_tra.nomcli)=0 THEN 
       LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 
       DISPLAY BY NAME w_mae_tra.nomcli 
    ELSE
       IF w_mae_tra.nomcli!="CONSUMIDOR FINAL" AND w_mae_tra.nomcli!="CLIENTES VARIOS" 
          AND w_mae_tra.numnit != "CF" THEN
		  INITIALIZE xnumnit,xdircli,xtelcli TO NULL
        SELECT --a.numnit,
               a.dircli,a.telcli
         INTO  --xnumnit,
               xdircli,xtelcli
         FROM  fac_mtransac a
         WHERE a.lnktra = (SELECT MAX(x.lnktra)
                            FROM  fac_mtransac x
                            WHERE x.nomcli = w_mae_tra.nomcli)
         IF (status!=NOTFOUND) THEN
           -- Asignando datos
            --LET w_mae_tra.numnit = xnumnit
            LET w_mae_tra.dircli = xdircli
            LET w_mae_tra.telcli = xtelcli
            DISPLAY BY NAME --w_mae_tra.numnit,
               w_mae_tra.dircli,w_mae_tra.telcli
         END IF
       END IF
    END IF 

   AFTER FIELD dircli
    -- Verificando direccion
    IF LENGTH(w_mae_tra.dircli)=0 THEN 
       LET w_mae_tra.dircli = "CIUDAD" 
       DISPLAY BY NAME w_mae_tra.dircli 
    END IF 

   AFTER FIELD credit
    -- Verificando forma de pago
    IF w_mae_tra.credit IS NULL THEN
       ERROR "Error: forma de pago invalida, VERIFICA ..."
       NEXT FIELD credit
    END IF 

   AFTER FIELD ordcmp 
    -- Verificanod numero de orden
    IF (w_mae_tra.ordcmp<=0) THEN
       ERROR "Error: orden de compra invalida, VERIFICA ..."
       LET w_mae_tra.ordcmp = NULL
       CLEAR ordcmp
       NEXT FIELD ordcmp
    END IF

   AFTER INPUT
    -- Verificando nulos
    IF w_mae_tra.lnktdc IS NULL THEN
       NEXT FIELD lnktdc
    END IF
    IF w_mae_tra.codcli IS NULL THEN
       NEXT FIELD codcli
    END IF
    IF w_mae_tra.numnit IS NULL THEN
       NEXT FIELD numnit
    END IF
    IF w_mae_tra.nomcli IS NULL THEN
       NEXT FIELD nomcli
    END IF
    IF w_mae_tra.dircli IS NULL THEN
       NEXT FIELD dircli
    END IF
    IF w_mae_tra.credit IS NULL THEN
       NEXT FIELD credit
    END IF 

    -- Verificando datos de credito si la forma de pago es credito
    IF w_mae_tra.credit THEN
       IF (w_mae_cli.moncre=0) THEN 
          CALL fgl_winmessage(
          " Atencion",
          " Cliente no tiene limite de credito autorizado. \n No puede facturarse de credito, VERIFICA ...",
          "stop")
          NEXT FIELD credit
       END IF 

       -- Calculando disponible
       LET w_mae_tra.saldis = (w_mae_cli.moncre-w_mae_cli.salcre)
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Verificanod si no hay orden de trabajo
  IF w_mae_tra.hayord THEN 
     -- Ingresando datos de la orden
     LET retroceso = facing001_detorden() 
  ELSE
     -- Ingresando detalle del movimiento 
     LET retroceso = facing001_detingreso()
  END IF 
  IF flag_tmp=TRUE THEN
     CALL facing001_tmp_db(2)
  END IF
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL facing001_inival(1) 
 END IF 

 -- Verificando si hay ordenes
 IF ordenes THEN
    -- Habilitando tabla de productos
    CALL f.setElementHidden("tabla1",0)
    CALL f.setElementHidden("group6",0)
    CALL f.setElementHidden("group4",1)
    CALL f.setElementHidden("group5",0)
    CALL f.setElementHidden("labelc",0)
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos de la facturacion 

FUNCTION facing001_detingreso()
 DEFINE w_mae_art  RECORD LIKE inv_products.*,
        w_uni_vta  RECORD LIKE inv_unimedid.*,
        wpremin    DEC(12,2),
        wpresug    DEC(14,2),
        xtippre    SMALLINT, 
        loop,scr   SMALLINT,
        confirm    SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER,  
        repetido   SMALLINT,
        strcon     STRING,
        line_esp   SMALLINT,
		  flag_exis  SMALLINT,
        flag_despecial SMALLINT,
        des_especial STRING,
        vpremindol DECIMAL(12,2),
        vpresugdol DECIMAL(14,2)
        
 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

 LET vpremindol = NULL;
 LET vpresugdol = NULL;
 
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT



	ON ACTION agregar_especial
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
		OPEN WINDOW especial
		WITH FORM "facing001e"
		LET flag_especial=TRUE
      CALL facing001_detorden() RETURNING v_products[arr].cditem, v_products[arr].unimed, v_products[arr].dsitem,
														v_products[arr].canori, v_products[arr].preuni, v_products[arr].totpro,
														v_products[arr].unimto, v_products[arr].nuitemf
		CLOSE WINDOW especial
		IF v_products[arr].cditem IS NULL OR v_products[arr].cditem ="NADA" THEN
			LET flag_especial=FALSE
			INITIALIZE w_mae_ord.* TO NULL
		   INITIALIZE v_products[arr].* TO NULL
			NEXT FIELD cditem
		ELSE
			LET line_esp = arr
			DISPLAY v_products[arr].* TO s_products[scr].*
   		CALL DIALOG.setActionActive("agregar_especial",FALSE)
			NEXT FIELD NEXT  
		END IF

   --Opcion para agregar descripcion especial del producto
   ON ACTION desc_especial
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      IF v_products[arr].cditem IS NOT NULL THEN
         OPEN WINDOW d_especial
         WITH FORM "facing001g"
            LET flag_despecial = FALSE;
            LET des_especial = NULL;
            CALL facing001_descespecial(v_products[arr].deitemh) RETURNING flag_despecial, des_especial
         CLOSE WINDOW d_especial
         IF flag_despecial = FALSE THEN --El usuario no cancel� el ingreso de la descripci�n
            LET v_products[arr].deitemh = des_especial
            DISPLAY v_products[arr].deitemh TO s_products[scr].deitemh 
            DISPLAY v_products[arr].deitemh TO deitem
         ELSE
            LET flag_despecial = FALSE
         END IF
      END IF
      
   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "1;Atencion: calculadora no disponible."
    END IF

   ON ACTION listproducto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos","Producto","Descirpcion del Producto",
                            "codabr","dsitem","inv_products","1=1",2,1,1)
    RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD cditem
    ELSE 
       -- Asignando descripcion
       DISPLAY v_products[arr].cditem TO s_products[scr].cditem 
       DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 
    END IF 

   ON CHANGE canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing001_totdet()

   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)
		IF flag_especial = FALSE THEN
	   	CALL DIALOG.setActionActive("agregar_especial",TRUE)
		END IF

    -- Totalizando unidades
    CALL facing001_totdet()

   BEFORE ROW
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      LET totlin = ARR_COUNT()
      DISPLAY v_products[arr].deitemh TO deitem
      LET vpremindol = NULL;
      LET vpresugdol = NULL;
    
   BEFORE FIELD cditem
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cditem
    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cditem 
    END IF

   BEFORE FIELD canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Verificando producto
    IF v_products[arr].cditem IS NULL OR
       (v_products[arr].cditem <=0) THEN 
       ERROR "Error: codigo del producto invalido, VERIFICA ..."
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_products[arr].cditem)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       ERROR "Error: producto no registrado, VERIFICA ..."
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Asignando descripcion
    LET v_products[arr].codpro = w_mae_art.cditem
	 IF flag_especial = FALSE THEN
	    LET v_products[arr].dsitem = w_mae_art.dsitem 
	 END IF
    LET v_products[arr].unimed = w_mae_art.unimed 
    DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 

    -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed) 
    RETURNING w_uni_med.*,existe

    -- ASignando unidad de medida
    LET v_products[arr].nomuni = w_uni_med.nommed 
    DISPLAY v_products[arr].nomuni TO s_products[scr].nomuni 

    -- Obteniendo datos de la medida de producto
    INITIALIZE w_med_pro.* TO NULL
    CALL librut003_bmedidapro(w_mae_art.codmed) 
    RETURNING w_med_pro.*,existe

    -- Verificando productos duplicados 
    LET repetido = FALSE
    FOR i = 1 TO totlin
     IF v_products[i].cditem IS NULL THEN
        CONTINUE FOR
     END IF

     -- Verificando producto
     IF w_mae_art.dsitem NOT MATCHES "*MEDIDA*"   THEN
	     IF (i!=ARR_CURR()) THEN 
   	   IF (v_products[i].cditem = v_products[arr].cditem) THEN
      	   LET repetido = TRUE
	         EXIT FOR
   	   END IF 
	     END IF
	  END IF
    END FOR

    -- Si hay repetido
    IF repetido THEN
       LET msg = "Producto ya registrado en el detalle. Linea (",i USING "<<<",") \n VERIFICA ..." 
       CALL fgl_winmessage(" Atencion",msg,"stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem
    END IF

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR s_products[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si producto tiene precio sugerido  
    IF (w_mae_art.presug=0) AND w_mae_art.dsitem NOT MATCHES "*MEDIDA*" THEN
       -- Actualizando precio sugerido si no existe 
       LET w_mae_art.presug = facing001_actualiza_pre(w_mae_art.cditem) 
		 IF w_mae_art.presug <=0 then
			 LET arr = ARR_CURR()
			 INITIALIZE v_products[arr].* TO NULL
			 NEXT FIELD cditem
		 END IF
    END IF 

    -- Determinando tipo de precio
    LET xtippre = 1 -- Normal
    
    -- Verificando si mdida del producto es fraccionable
    IF w_med_pro.epqfra AND
       w_med_pro.medtot>0 THEN
       LET xtippre = 2  -- Rollos
    END IF 

    IF NOT w_med_pro.epqfra AND 
       w_med_pro.medtot>0 THEN
       LET xtippre = 3 -- Precios especiales
    END IF 

    --GYanes 28032020 si es tipo de facturacion exportacion se cambia el precio a dolares sin IVA
    DISPLAY "iva: ",w_mae_tra.poriva
    LET vpremindol = NULL;
    LET vpresugdol = NULL;
    IF w_mae_tra.tipfac = "E" THEN
      LET w_mae_art.premin = w_mae_art.premin / (1+(w_mae_tra.poriva/100)) 
      LET w_mae_art.presug = w_mae_art.presug / (1+(w_mae_tra.poriva/100)) 
      LET vpremindol = w_mae_art.premin / w_mae_tra.tascam
      LET vpresugdol = w_mae_art.presug / w_mae_tra.tascam
    END IF

    -- Desplegando precio
    -- Verificando tipo de precio 
    CASE (xtippre)
     WHEN 1 -- Normal
      LET wpremin = w_mae_art.premin
      LET wpresug = w_mae_art.presug

      -- Precio sugerido de la lista de precios
      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni
      END IF 
      IF v_products[arr].predol IS NULL THEN 
         LET v_products[arr].predol = vpresugdol
         DISPLAY v_products[arr].predol TO s_products[scr].predol
      END IF

      -- Asignando unidad de medida
      LET v_products[arr].unimto = w_mae_art.unimed 
      DISPLAY v_products[arr].unimto TO s_products[scr].unimto 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",0) 
      CALL Dialog.SetFieldActive("canuni",0) 

     WHEN 2 -- Precio de rollo
      LET wpremin = w_mae_art.premin 
      LET wpresug = w_mae_art.presug

      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni 
      END IF
      IF v_products[arr].predol IS NULL THEN 
         LET v_products[arr].predol = vpresugdol
         DISPLAY v_products[arr].predol TO s_products[scr].predol 
      END IF 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",1) 
      CALL Dialog.SetFieldActive("canuni",0) 
     WHEN 3 -- Precios especiales 
      LET wpremin = w_mae_art.premin
      LET wpresug = w_mae_art.presug

      -- Precio sugerido de la lista de precios
      IF v_products[arr].preuni IS NULL THEN 
         LET v_products[arr].preuni = wpresug 
         DISPLAY v_products[arr].preuni TO s_products[scr].preuni 
      END IF
      IF v_products[arr].predol IS NULL THEN 
         LET v_products[arr].predol = vpresugdol
         DISPLAY v_products[arr].predol TO s_products[scr].predol 
      END IF 

      -- Asignando unidad de medida
      LET v_products[arr].unimto = w_mae_art.unimed 
      DISPLAY v_products[arr].unimto TO s_products[scr].unimto 

      -- Deshabiiltando campos 
      CALL Dialog.SetFieldActive("unimto",0) 
      CALL Dialog.SetFieldActive("canuni",0) 
    END CASE 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing001_totdet()

   AFTER FIELD canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD canori 
    END IF

    -- Verificando cantidad
    IF v_products[arr].canori IS NULL OR
       (v_products[arr].canori <=0) THEN 
       ERROR "Error: cantidad invalida, VERIFICA ..."
       NEXT FIELD canori 
    END IF 

    -- Verificanod si tipo de precio es normal o especial
    IF (xtippre=1) OR 
       (xtippre=3) THEN
       LET v_products[arr].canuni = v_products[arr].canori
       LET v_products[arr].factor = 0 
       DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)
	

    -- Totalizando unidades
    CALL facing001_totdet()
{
   before field preuni
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      IF v_products[arr].canuni >0 then
         SELECT *
            FROM inv_proenbod
            WHERE codabr =  v_products[arr].cditem
            AND   codemp =  w_mae_pos.codemp
            AND   codsuc =  w_mae_pos.codsuc
            AND   codbod =  w_mae_pos.codbod
            AND   exican <= v_products[arr].canuni
         IF STATUS != NOTFOUND THEN
             LET opc = librut001_menugraba("Confirmacion",
                                           "Que desea hacer?",
                                           "Otras Bodegas",
                                           "Modificar",
                                           "Cancelar",
                                           "")
				CASE opc
					WHEN 1
						CALL facing001_existencias(v_products[arr].cditem,v_products[arr].canuni) RETURNING flag_exis
						IF flag_exis = FALSE	THEN
							INITIALIZE v_products[arr].* TO NULL
							CLEAR s_products[scr].* 
							NEXT FIELD cditem
						ELSE
							NEXT FIELD NEXT
						END IF
					WHEN 2
						LET v_products[arr].canuni =null
						NEXT FIELD cditem
					WHEN 3
						CALL v_products.deleteElement(arr)
						NEXT FIELD cditem
				END CASE
         END IF
      END IF
}
   AFTER FIELD unimto
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD unimto 
    END IF

    -- Verificando cantidad
    IF v_products[arr].unimto IS NULL THEN
       ERROR "Error: unidad de medida invalida, VERIFICA ..."
       NEXT FIELD unimto 
    END IF 

    -- Verificando medidas 
    IF (v_products[arr].unimed!=v_products[arr].unimto) THEN
       -- Obteniendo datos de la unidad de medida 
       INITIALIZE w_uni_vta.* TO NULL
       CALL librut003_bumedida(v_products[arr].unimto) 
       RETURNING w_uni_vta.*,existe
       
      -- Verificanod conversion 
      IF w_uni_vta.factor IS NULL OR
         w_uni_vta.factor=0 THEN 
         ERROR "Error: unidad de medida sin factor de conversion, VERIFICA ..."
         NEXT FIELD unimto 
      END IF 

      -- Convirtiendo cantidad de acuerdo al factor 
      LET v_products[arr].canuni = (v_products[arr].canori*w_uni_vta.factor) 
      LET v_products[arr].factor = w_uni_vta.factor 
      DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    ELSE 
      -- Asignando cantidad origen a cantidad total 
      LET v_products[arr].canuni = v_products[arr].canori
      LET v_products[arr].factor = 0
      DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing001_totdet()

   AFTER FIELD preuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD preuni 
    END IF

    -- Verificando precio
    IF v_products[arr].preuni IS NULL OR
       (v_products[arr].preuni <=0) THEN 
       ERROR "Error: precio invalido, VERIFICA ..."
       NEXT FIELD preuni 
    END IF 

    -- Verificando que precio no sea menor que el minimo
    IF (v_products[arr].preuni<wpremin) THEN
       -- Actualizando precio minimo 
       CALL fgl_winmessage("Atencion"," Precio ingresado menor al precio minimo. ("||wpremin||")","stop")
			IF w_mae_tra.obspre IS NULL THEN
	 			CALL f.setElementHidden("labelg",0)
 				CALL f.setFieldHidden("obspre",0)
				IF NOT inving001_input_compre() THEN
 					CALL f.setElementHidden("labelg",1)
 					CALL f.setFieldHidden("obspre",1)
    				CALL facing001_cantidadtotal(arr,scr)
					NEXT FIELD NEXT
				ELSE
					LET v_products[arr].preuni=w_mae_art.presug
 					CALL f.setElementHidden("labelg",1)
 					CALL f.setFieldHidden("obspre",1)
					CALL fgl_winmessage("Atention","Si quiere bajar el precio minimo debe colocar la razon","information")
					NEXT FIELD preuni
				END IF
			ELSE
				CALL fgl_winmessage("Atention","Usted ya ha ingresado el motivo por el precio menor","information")
			END IF 
   END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing001_totdet()


  AFTER FIELD predol
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD predol 
    END IF

    -- Verificando precio
    IF v_products[arr].predol IS NULL OR
       (v_products[arr].predol <=0) THEN 
       ERROR "Error: precio invalido, VERIFICA ..."
       NEXT FIELD predol 
    END IF 

    -- Verificando que precio no sea menor que el minimo
    IF (v_products[arr].predol<vpremindol) THEN
       -- Actualizando precio minimo 
       CALL fgl_winmessage("Atencion"," Precio ingresado menor al precio minimo. ("||vpremindol||")","stop")
			IF w_mae_tra.obspre IS NULL THEN
	 			CALL f.setElementHidden("labelg",0)
 				CALL f.setFieldHidden("obspre",0)
				IF NOT inving001_input_compre() THEN
 					CALL f.setElementHidden("labelg",1)
 					CALL f.setFieldHidden("obspre",1)
    				CALL facing001_cantidadtotal(arr,scr)
					NEXT FIELD NEXT
				ELSE
					LET v_products[arr].predol=vpresugdol
 					CALL f.setElementHidden("labelg",1)
 					CALL f.setFieldHidden("obspre",1)
					CALL fgl_winmessage("Atention","Si quiere bajar el precio minimo debe colocar la razon","information")
					NEXT FIELD predol
				END IF
			ELSE
				CALL fgl_winmessage("Atention","Usted ya ha ingresado el motivo por el precio menor","information")
			END IF 
   END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing001_totdet()


   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing001_totdet()

   AFTER DELETE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Limpiando producto borrado
    INITIALIZE v_products[arr].* TO NULL
    CLEAR s_products[scr].* 
	 IF arr = line_esp THEN
		LET flag_especial = FALSE
		CALL DIALOG.SetActionActive("agregar_especial",TRUE)
	   LET line_esp = NULL
		INITIALIZE w_mae_ord.* TO NULL
	 END IF

    -- Totalizando unidades
    CALL facing001_totdet()

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing001_totdet()
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando lineas incompletas 
  LET linea = facing001_incompletos()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") de producto incompleta. \n VERIFICA ..."
     CALL fgl_winmessage(
     " Atencion",
     msg, 
     "stop")
     CONTINUE WHILE
  END IF 

  -- Verificando que se ingrese al menos un producto
  IF (totval<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     " Debe facturarse al menos un producto. \n VERIFICA ...",
     "stop")
     CONTINUE WHILE
  END IF

  -- Verificando disponibilidad si documento es de credito
  IF w_mae_tra.credit THEN
     IF (totval>w_mae_tra.saldis) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Cliente sin saldo disponible para cubrir el total a pagar. \n VERIFICA disponibiidad ...",
        "stop")
       CONTINUE WHILE
     END IF 
  END IF

  -- Ingresando desgloce de pago
  -- Verificando si documento es de contado
  LET haytar = FALSE  
  IF NOT w_mae_tra.credit THEN 
     IF facing001_desglocepago() THEN
        CONTINUE WHILE
     END IF 
  ELSE -- Credito
     -- Totalizando desgloce de pago
     CALL facing001_totpago()
  END IF 

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    -- Ingresando confirmacion de cajero
    CALL facing001_confirmacion() 
    RETURNING confirm,w_mae_tra.usrope
    IF NOT confirm THEN 
       LET loop = TRUE 
    ELSE
       LET loop      = FALSE
       LET retroceso = FALSE

       -- Grabando inventario 
       CALL facing001_grabar()
    END IF 

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subutina para el ingreso de los datos de la orden de trabajo

FUNCTION facing001_detorden()
 DEFINE wpremin    DECIMAL(12,2), 
        wpresug    LIKE inv_subcateg.presug,
        posicion   INT     , 
        confirm    SMALLINT, 
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER,  
        strcon     STRING,
        medida     VARCHAR(10,0),
        wnomcla                 ,
		  wnomcol    VARCHAR(25,0),
        descrip    VARCHAR(60,0),
        wcditem    LIKE inv_products.cditem,
		  wcodabr    LIKE inv_products.codabr,
        busqueda   VARCHAR(20,0)

 -- Iniciando el loop
 LET retroceso = FALSE
 LET w_det_ord.sinord = 0
 LET w_det_ord.ordext = 0
 LET posicion         = 0
 LET loop      = TRUE
 OPTIONS INPUT WRAP

 WHILE loop
  -- Ingresando productos
  INPUT BY NAME w_mae_ord.fecord,
                w_det_ord.cantid,
                w_det_ord.subcat,
                w_det_ord.codcol,
                w_det_ord.desman,
                w_det_ord.medida,
                w_det_ord.xlargo,
                w_det_ord.yancho, 
                w_det_ord.descrp,
                w_det_ord.observ,
					 --w_det_ord.sinord,
					 w_det_ord.ordext WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT


	ON ACTION lista
		LET posicion = facing001_disp(2)
		IF posicion IS NOT NULL AND posicion > 0 THEN
			LET w_det_ord.nuitem = v_ordenes[posicion].nuitem1
			DISPLAY BY NAME w_det_ord.nuitem
		ELSE
			CALL facing001_def_dord()
         NEXT FIELD cantid
		END IF

	ON ACTION agregar
		IF w_mae_ord.fecord IS NOT NULL AND w_det_ord.cantid IS NOT NULL AND
			w_det_ord.subcat IS NOT NULL AND w_det_ord.codcol IS NOT NULL AND
			w_det_ord.desman IS NOT NULL AND w_det_ord.medida IS NOT NULL AND
			w_det_ord.xlargo IS NOT NULL AND w_det_ord.yancho IS NOT NULL AND
			--w_det_ord.descrp IS NOT NULL AND w_det_ord.observ IS NOT NULL AND
			w_det_ord.ordext IS NOT NULL AND
			w_det_ord.precio IS NOT NULL THEN
			IF posicion IS NOT NULL AND posicion >0 THEN
				IF NOT facing001_ins_upd_del(2,posicion) THEN
					NEXT FIELD cantid
				END IF
			ELSE
				LET w_det_ord.sinord=0
				IF NOT facing001_ins_upd_del(1,posicion) THEN
					NEXT FIELD cantid
				ELSE
				   IF flag_especial = TRUE THEN
						SELECT a.codabr,a.dsitem
						  INTO wcodabr,descrip
						  FROM inv_products a , inv_subcateg
						 WHERE a.subcat = inv_subcateg.subcat
                     AND a.dsitem MATCHES '*'||inv_subcateg.nomsub||' '||'*MEDIDA'
							AND inv_subcateg.subcat= w_det_ord.subcat
							AND a.subcat = w_det_ord.subcat
						RETURN wcodabr,"UNIDAD",descrip,w_det_ord.cantid,tot   ,w_mae_ord.totord,8, w_det_ord.nuitem
					END IF
				END IF
			END IF
			CALL facing001_def_dord()
			NEXT FIELD cantid
		ELSE
			CALL box_valdato("Debe llenar todos los datos")
			NEXT FIELD fecord
		END IF
   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION precios
    -- Cambiando precios 
    IF w_det_ord.precio IS NOT NULL AND w_det_ord.precio > 0 THEN 
       CALL facing001_precio(wpremin,wpresug,w_det_ord.premed)
    END IF 

	ON ACTION accept 
		IF v_ordenes.getLength() > 0 THEN
			LET retroceso = FALSE
      	--LET loop      = FALSE
	   	-- Totalizando desgloce de pago
			IF facing001_abono_orden() THEN
	   		--CALL facing001_totpago()
				IF flag_especial <> TRUE THEN
					LET sin_orden=box_pregunta("Solicitar lona a confeccion??")
					CASE sin_orden
						WHEN  1
						LET sin_orden = 0
						WHEN  2
						LET sin_orden = 1
					END CASE
				END IF
				EXIT INPUT
			ELSE
				CALL facing001_def_dord()
				NEXT FIELD cantid
			END IF
		ELSE
			CALL box_valdato("Debe ingresar una lona como minimo.")
			NEXT FIELD cantid
		END IF
		

   BEFORE INPUT
	 -- Combos del arreglo
		-- Combo de las subcategorias
		CALL librut002_combobox("subcat1","SELECT a.subcat,a.nomsub FROM inv_subcateg a "||
										" WHERE a.lonmed = 1 ORDER BY 2 ")
		-- Combo de las colores
		CALL librut002_combobox("codcol1","SELECT a.codcol,a.nomcol FROM inv_colorpro a "||
										" ORDER BY 2")
		-- Combo de las fases
		CALL librut002_combobox("nofase1","SELECT a.tippar,a.valchr FROM glb_paramtrs a "||
										" WHERE a.numpar = 100 ORDER BY 1 ")
    -- Asignando datos
	 IF flag_especial = FALSE THEN
	--    CALL DIALOG.setActionActive("precios",FALSE)
	    --CALL DIALOG.setActionActive("accept",FALSE)
	 ELSE
	    CALL DIALOG.setActionActive("accept",TRUE)
	 END IF
    CALL f.setElementHidden("label27",1)
    CALL f.setFieldHidden("compre",1)
    -- Asignando punto de venta como parte de la orden 
    LET w_det_ord.nuitem=1
    LET w_mae_ord.lnkord = 0
    LET w_mae_ord.fecord = w_mae_tra.fecemi
    DISPLAY BY NAME w_det_ord.nuitem,w_mae_ord.fecord,w_det_ord.cantid,w_mae_ord.totord,
                    w_mae_ord.totabo,w_mae_ord.salord,w_mae_ord.lnkord,--w_mae_ord.premed,
                    w_det_ord.nofase 

    -- Cargando combobox 
    CALL librut003_cbxsubcateg()
    CALL librut003_cbxfasesot() 

   ON CHANGE cantid
    -- Calculando saldo de la orden
    CALL facing001_totalord(w_det_ord.premed)

   AFTER FIELD cantid
    -- Verificando cantidad
    IF w_det_ord.cantid IS NULL OR
       w_det_ord.cantid <=0 THEN
       ERROR "Error: cantidad invalida, VERIFICA ..." 
       LET w_det_ord.cantid = NULL
       CLEAR cantid 
       NEXT FIELD cantid
    END IF 

	AFTER FIELD medida
		IF w_det_ord.medida IS NULL THEN
			CALL box_valdato("Debe ingresar medida")
			NEXT FIELD medida
		ELSE
			IF w_det_ord.medida = 1 THEN
				CALL box_valdato("Usted eligio 'PIES'")
			ELSE
				CALL box_valdato("Usted eligio 'METROS'")
			END IF
		END IF

   ON CHANGE subcat 
    -- Comobobox de colores x clase 
    IF w_det_ord.subcat IS NOT NULL THEN
       -- Flag para calculo del precio
       LET flag_prec = FALSE
       --CALL librut003_cbxcoloresxsubcat(w_mae_ord.subcat) 
       CALL librut003_cbxcolores() 
    END IF 

   AFTER FIELD subcat
    -- Verificando clase
    IF w_det_ord.subcat IS NULL THEN
       ERROR "Error: clase de producto invalida, VERIFICA ..." 
       NEXT FIELD subcat
    END IF 

    -- Buscando datos de la subcategoria
    SELECT NVL(a.tipsub,1),NVL(a.premin,0),NVL(a.presug,0)
     INTO  w_det_ord.tipord,wpremin,wpresug 
     FROM  inv_subcateg a
     WHERE a.subcat = w_det_ord.subcat
	  LET w_det_ord.premed = wpresug
	  IF w_det_ord.tipord = 2 THEN
			LET w_det_ord.ordext = 1
	  END IF


    -- Verificando precios minimos y sugeridos
    IF wpremin=0 OR wpresug=0 THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Clase de producto sin precios definidos.\n Definir precios antes de poder facturar.",
       "warning")
       NEXT FIELD subcat 
    END IF  

    -- Desplegando precios
    LET pre_med          = wpresug 
    LET w_det_ord.premed = wpresug 

    -- Calculando saldo de la orden
    CALL facing001_totalord(w_det_ord.premed)

	BEFORE FIELD ordext
     IF w_det_ord.tipord = 2 THEN
			NEXT FIELD NEXT
     END IF

   BEFORE FIELD codcol
    -- Comobobox de colores x clase 
    IF w_det_ord.subcat IS NOT NULL THEN
       --CALL librut003_cbxcoloresxsubcat(w_mae_ord.subcat) 
       CALL librut003_cbxcolores() 
    END IF 

   AFTER FIELD codcol 
    -- Verificando color
    IF w_det_ord.codcol IS NULL THEN
       NEXT FIELD codcol 
    END IF 

   ON CHANGE medida 
    -- Calculando saldo de la orden
	 LET flag_prec = FALSE
    CALL facing001_totalord(w_det_ord.premed)

{
   AFTER FIELD medida
    -- Verificanod medida
    IF w_det_ord.medida IS NULL THEN
       ERROR "Error: tipo de medida invalido, VERIFICA ..."
       NEXT FIELD medida 
    END IF 
}
   ON CHANGE xlargo
    -- Calculando saldo de la orden
    -- Flag para calculo del precio 
    LET flag_prec = FALSE
    CALL facing001_totalord(w_det_ord.premed)

   AFTER FIELD xlargo
    -- Verificando LARGO
    IF w_det_ord.xlargo IS NULL OR
       w_det_ord.xlargo <=0 THEN 
       ERROR "Error: largo de la medida invalido, VERIFICA ..." 
       LET w_det_ord.xlargo = NULL
       CLEAR xlargo 
       NEXT FIELD xlargo
    END IF 

 --   -- Calculando saldo de la orden
   CALL facing001_totalord(w_det_ord.premed)

   ON CHANGE yancho 
    -- Calculando saldo de la orden
    -- Flag para calculo del precio
    LET flag_prec = FALSE
    CALL facing001_totalord(w_det_ord.premed)

   AFTER FIELD yancho 
    -- Verificando ANCHO 
    IF w_det_ord.yancho IS NULL OR
       w_det_ord.yancho <=0 THEN 
       ERROR "Error: ancho de la medida invalido, VERIFICA ..." 
       LET w_det_ord.yancho = NULL
       CLEAR yancho 
       NEXT FIELD yancho
    END IF 

--    -- Calculando saldo de la orden
    CALL facing001_totalord(w_det_ord.premed)

   {AFTER FIELD fecofe 
    -- Verificando fecha sugerida
    IF w_mae_ord.fecofe IS NULL OR
       w_mae_ord.fecofe <TODAY THEN
       ERROR "Error: fecha sugerida invalida, VERIFICA ..." 
       NEXT FIELD fecofe
    END IF 

   AFTER FIELD fecent 
    -- Verificando fecha de entrega 
    IF w_mae_ord.fecent IS NULL OR
       w_mae_ord.fecent <TODAY THEN
       ERROR "Error: fecha de entrega invalida, VERIFICA ..." 
       NEXT FIELD fecent
    END IF 

   ON CHANGE totabo 
    -- Calculando saldo de la orden
    CALL facing001_totalord(w_det_ord.premed)

   BEFORE FIELD totabo
    -- Activando boton de precios
    CALL DIALOG.setActionActive("precios",TRUE)

   AFTER FIELD totabo 
    -- Verificanod si es credito
    IF NOT w_mae_tra.credit THEN 
     -- Verificando abono de la orden
     IF w_mae_ord.totabo IS NULL OR
        w_mae_ord.totabo <0 OR 
        w_mae_ord.totabo >w_mae_ord.totord THEN
        ERROR "Error: abono de la orden invalido, VERIFICA ..." 
        LET w_mae_ord.totabo = NULL
        CLEAR totabo 
        NEXT FIELD totabo
     END IF 
    ELSE
     -- Si es credito el abono debe ser cero
     LET w_mae_ord.totabo = 0
     DISPLAY BY NAME w_mae_ord.totabo 
    END IF 

    -- Calculando saldo de la orden
    CALL facing001_totalord(w_det_ord.premed)

    -- Activando boton de precios
	 IF flag_especial = FALSE THEN
	    CALL DIALOG.setActionActive("precios",FALSE)
	 END IF
}
   AFTER INPUT 
		IF w_mae_ord.fecord IS NULL OR w_det_ord.cantid IS NULL OR w_det_ord.subcat IS NULL OR w_det_ord.codcol IS NULL OR
			w_det_ord.medida IS NULL OR w_det_ord.xlargo IS NULL OR w_det_ord.yancho IS NULL OR w_mae_ord.fecofe IS NULL OR
			w_mae_ord.fecent IS NULL OR w_mae_ord.totord IS NULL OR w_mae_ord.totabo IS NULL THEN
			ERROR "Error: Debe de llenar todos los datos necesarios."
			NEXT FIELD cantid
      END IF
    -- Verificando fechas de entrega 
    IF w_mae_ord.fecofe>w_mae_ord.fecent THEN
       ERROR "Error: fecha sugerida no puede ser mayor a fecha de entrega.";
       NEXT FIELD fecofe
    END IF 

    -- Calculando saldo de la orden
   -- CALL facing001_totalord(w_det_ord.premed)
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Ingresando desgloce de pago
  -- Verificando si documento es de contado
  IF flag_especial = FALSE THEN
	  LET haytar = FALSE  
	  IF NOT w_mae_tra.credit THEN
	     LET totval = w_mae_ord.totabo 

	     LET totval = facin001_suma()--w_mae_ord.totord
	     IF facing001_desglocepago() THEN
  	      CONTINUE WHILE
		  END IF 
	  ELSE -- Credito
	     LET totval = facin001_suma()--w_mae_ord.totord

	     -- Totalizando desgloce de pago
	     CALL facing001_totpago()
	  END IF

	  -- Menu de opciones
	  LET opc = librut001_menugraba("Confirmacion",
	                                "Que desea hacer?",
	                                "Guardar",
	                                "Modificar",
	                                "Cancelar",
	                                "")

	  -- Verificando opcion 
	  CASE (opc)
	   WHEN 0 -- Cancelando
	    LET loop      = FALSE
	    LET retroceso = FALSE
	   WHEN 1 -- Grabando
	    -- Ingresando confirmacion de cajero
	    CALL facing001_confirmacion() 
	    RETURNING confirm,w_mae_tra.usrope
	    IF NOT confirm THEN 
	       LET loop = TRUE 
	    ELSE
	       LET loop      = FALSE
	       LET retroceso = FALSE

      	 -- Grabando inventario 
   	    CALL facing001_grabar()
	    END IF 
	   WHEN 2 -- Modificando
	    LET loop = TRUE 
	  END CASE
	ELSE
      IF NOT w_det_ord.medida THEN
         LET medida = "METROS"
      ELSE
         LET medida = "PIES"
      END IF
      -- Obteniendo clase de producto
      LET wnomcla = NULL
      SELECT a.nomsub
       INTO  wnomcla
       FROM  inv_subcateg a
       WHERE a.subcat = w_det_ord.subcat
		LET busqueda="*",wnomcla CLIPPED,"*MEDIDA"
		
		SELECT a.codabr, a.cditem
		  INTO wcodabr, wcditem
		  FROM inv_products a
		  WHERE a.dsitem MATCHES busqueda
      -- Obteniendo color de producto
      LET wnomcol = NULL
      SELECT a.nomcol
       INTO  wnomcol
       FROM  inv_colorpro a
       WHERE a.codcol = w_det_ord.codcol

		IF wnomcla <> "SARAN" THEN
			LET descrip = "LONA ",wnomcla CLIPPED," ",wnomcol CLIPPED," DE ",w_det_ord.xlargo USING "<<<.##"," X ",w_det_ord.yancho USING "<<<.##"," ",medida
			EXIT WHILE
		ELSE
			LET descrip = wnomcla CLIPPED," ",wnomcol CLIPPED," DE ",w_det_ord.xlargo USING "<<<.##"," X ",w_det_ord.yancho USING "<<<.##"," ",medida
			EXIT WHILE
		END IF
	END IF
 END WHILE
 OPTIONS INPUT NO WRAP

	IF flag_especial = TRUE THEN
		IF retroceso = TRUE  THEN
	 		RETURN "","","","","","",""
		ELSE
	 		RETURN wcodabr,"UNIDAD",descrip,w_det_ord.cantid,w_det_ord.precio,w_mae_ord.totord,8,w_det_ord.nuitem
		END IF
	ELSE
 		RETURN retroceso
	END IF
END FUNCTION

-- Subrutina para cambiar el precio unitario 

FUNCTION facing001_precio(premin,presug,precio)
 DEFINE premin   LIKE inv_subcateg.premin,
        presug   LIKE inv_subcateg.presug,
        precio   LIKE inv_subcateg.presug,
        regreso  SMALLINT 
OPTIONS INPUT NO WRAP
 -- Ingresando precios
 INPUT BY NAME w_det_ord.precio,
               w_det_ord.compre WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED) 

  ON ACTION cancel
   -- Regreso
   LET regreso = TRUE
   EXIT INPUT 

  ON CHANGE precio 
   -- Calculando saldo de la orden
   CALL facing001_totalord(w_det_ord.premed)

  AFTER FIELD precio
   -- Verificando precio
   IF w_det_ord.precio IS NULL OR
      w_det_ord.precio <=0 THEN
      ERROR "Error: precio invalido , VERIFICA ..."
      LET w_det_ord.precio = 0 
      DISPLAY BY NAME w_det_ord.precio 
      NEXT FIELD precio
   END IF 

   IF (w_det_ord.premed<pre_med) AND
      (w_det_ord.xlargo IS NOT NULL AND w_det_ord.yancho IS NOT NULL)
      AND (w_det_ord.xlargo >0 AND w_det_ord.yancho >0) THEN
      CALL fgl_winmessage(
      " Atencion",
      " Precio ingresado menor al precio minimo por pie cuadrado. "||pre_med CLIPPED || ", \n VERIFICA ...",
      "warning")
   END IF
   -- Verificando si precios es menor que precio minimo
   IF (w_det_ord.premed<pre_med) THEN
      -- Habilitando campo de comentario si el precio es menor al precio sugerido 
      CALL f.setElementHidden("label27",0)
      CALL f.setFieldHidden("compre",0)
   ELSE
      LET w_det_ord.compre = NULL 
   END IF  
 END INPUT 
 OPTIONS INPUT WRAP
 -- Deshabilitando campo de comentario si el precio es menor al precio sugerido 
 CALL f.setElementHidden("label27",1)
 CALL f.setFieldHidden("compre",1)
END FUNCTION 

-- Subrutina para calcular el total y saldo de la orden de trabajo

FUNCTION facing001_totalord(wprecio)
 DEFINE wprecio LIKE inv_dorden.premed, 
        faccon  DEC(9,2),
        tot_uni DEC(9,2)
 -- Verificando alto y ancho
 IF w_det_ord.xlargo IS NULL THEN LET w_det_ord.xlargo = 0 END IF 
 IF w_det_ord.yancho IS NULL THEN LET w_det_ord.yancho = 0 END IF 

 -- Calculando saldo de la orden
 IF NOT w_det_ord.medida THEN
    LET faccon = facord
 ELSE
    LET faccon = 1
 END IF 

 IF flag_prec = FALSE THEN
   LET wprecio = pre_med
   LET w_det_ord.precio = (((w_det_ord.xlargo*faccon)*(w_det_ord.yancho*faccon))*wprecio)
   LET flag_prec=TRUE
 ELSE
   LET wprecio = w_det_ord.precio/((w_det_ord.xlargo*faccon)*(w_det_ord.yancho*faccon))
   IF wprecio > 0 THEN
      LET w_det_ord.premed = wprecio
   END IF
   LET w_det_ord.precio = (((w_det_ord.xlargo*faccon)*(w_det_ord.yancho*faccon))*w_det_ord.premed)
 END IF

 -- Totalizando orden 
 LET tot = w_det_ord.cantid*w_det_ord.precio

-- IF (w_mae_ord.totord>=w_mae_ord.totabo) THEN 
--    LET w_mae_ord.salord = (w_mae_ord.totord-w_mae_ord.totabo)
-- ELSE
 --   LET w_mae_ord.salord = 0 
-- END IF 
 
 --LET w_mae_tra.totpag = w_mae_ord.totabo 
-- IF flag_especial = FALSE THEN
--	 DISPLAY BY NAME w_mae_tra.totpag
 --ELSE
--	 LET w_mae_ord.salord = 0 
 --END IF
 --IF flag_especial THEN
	-- LET w_mae_ord.totabo = w_mae_ord.totord
 --END IF
-- LET w_mae_ord.salord=0
 --LET w_mae_ord.totord=0
 DISPLAY BY NAME w_mae_ord.salord,w_mae_ord.totord,w_det_ord.precio,tot
END FUNCTION 

-- Subrutina para calcular las undiades totales (unidades ingresdas * cantidad empaque)

FUNCTION facing001_cantidadtotal(arr,scr) 
 DEFINE arr,scr  SMALLINT

 -- Calculando precio total
 LET v_products[arr].totpro = (v_products[arr].canuni*v_products[arr].preuni)
 LET v_products[arr].prtdol = (v_products[arr].canuni*v_products[arr].predol)

 -- Desplegando datos
 DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
 DISPLAY v_products[arr].totpro TO s_products[scr].totpro
DISPLAY v_products[arr].prtdol TO s_products[scr].prtdol 
END FUNCTION 

-- Subrutina para totalizar unidades contadas

FUNCTION facing001_totdet()
 DEFINE i SMALLINT

 -- Totalizando
 LET totuni = 0
 LET totval = 0
 LET totvaldol = 0
 FOR i = 1 TO totlin
  IF v_products[i].codpro IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Totalizando
  LET totuni = (totuni+v_products[i].canuni)
  LET totval = (totval+v_products[i].totpro)
  LET totvaldol = (totvaldol+v_products[i].prtdol)
  
 END FOR

 -- Desplegando total de unidades del inventario
 IF w_mae_tra.tipfac = "E" THEN 
   LET w_mae_tra.totpag = totvaldol
 ELSE
   LET w_mae_tra.totpag = totval
 END IF
 DISPLAY BY NAME totuni,totval,totlin,w_mae_tra.totpag,totvaldol
END FUNCTION 

-- Subrutina para verificar si hay productos incompletos
FUNCTION facing001_incompletos()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO 100 
  IF v_products[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_products[i].canuni IS NULL OR
     v_products[i].preuni IS NULL OR
     v_products[i].totpro IS NULL THEN
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para ingresar el desgloce de pago
FUNCTION facing001_desglocepago()
 DEFINE recibi  DEC(14,2),
        loop    SMALLINT, 
        regreso SMALLINT, 
        lastkey SMALLINT,
        msg     STRING,
        recibido DEC(14,2)   

 OPTIONS INPUT WRAP 

 -- Ingresando desgloce 
 LET loop   = TRUE
 LET haytar = FALSE 
 WHILE loop 
 
LABEL inpag:
INPUT BY NAME w_mae_tra.efecti,
                w_mae_tra.cheque,
                w_mae_tra.tarcre,
                w_mae_tra.ordcom, 
                w_mae_tra.depmon 
		
   ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS)

  ON ACTION accept
   -- Avanza
   LET regreso = FALSE 
   LET loop    = FALSE
   EXIT INPUT 

  ON ACTION cancel
   -- Salida
   LET regreso = TRUE  
   LET loop    = FALSE
   EXIT INPUT

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF

  ON ACTION tarjetas
   -- Incrementando porcentaje x uso de tartejas
   IF haytar THEN 
      LET haytar = FALSE
   ELSE
      LET haytar = TRUE 
   END IF 
   CALL facing001_incrementotar() 

  BEFORE INPUT 
   -- Desabiitando boton de incremento x tarjeta
   CALL DIALOG.setActionActive("tarjetas",FALSE)

  AFTER FIELD efecti 
   -- Verificando  efectivo 
   IF w_mae_tra.efecti IS NULL OR
      (w_mae_tra.efecti <=0) THEN
      LET w_mae_tra.efecti = 0
      DISPLAY BY NAME w_mae_tra.efecti
   END IF

   -- Calculando vuelto
   CLEAR vuelto
   LET vuelto = 0
   LET recibi = 0
   IF (w_mae_tra.efecti>=w_mae_tra.totpag) THEN
       LET w_mae_tra.cheque = 0
       LET w_mae_tra.tarcre = 0
       LET recibi           = w_mae_tra.efecti
       LET vuelto           = (w_mae_tra.efecti-w_mae_tra.totpag)
       LET w_mae_tra.efecti = w_mae_tra.totpag 
       DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.cheque,w_mae_tra.tarcre

       -- Totalizando desgloce 
       CALL facing001_totpago()

       -- Verificando vuelto
       IF (vuelto>0) THEN
           DISPLAY BY NAME vuelto 
       END IF
   END IF

   -- Totalizando desgloce 
   CALL facing001_totpago()

  AFTER FIELD cheque
   -- Verificando cheque
   IF w_mae_tra.cheque IS NULL OR
      (w_mae_tra.cheque <=0) THEN
      LET w_mae_tra.cheque = 0
      DISPLAY BY NAME w_mae_tra.cheque
   END IF

   -- Totalizando desgloce 
   CALL facing001_totpago()

  BEFORE FIELD tarcre
   -- Hesabiitando boton de incremento x tarjeta
   CALL DIALOG.setActionActive("tarjetas",TRUE)

  AFTER FIELD tarcre
   -- Verificando tarjeta 
   IF w_mae_tra.tarcre IS NULL OR
      (w_mae_tra.tarcre <=0) THEN
      LET w_mae_tra.tarcre = 0
      DISPLAY BY NAME w_mae_tra.tarcre
   END IF

   -- Totalizando desgloce 
   CALL facing001_totpago()

   -- Desabiitando boton de incremento x tarjeta
   CALL DIALOG.setActionActive("tarjetas",FALSE)

  AFTER FIELD ordcom
   -- Verificando orden de compra 
   IF w_mae_tra.ordcom IS NULL OR
      (w_mae_tra.ordcom <=0) THEN
      LET w_mae_tra.ordcom = 0
      DISPLAY BY NAME w_mae_tra.ordcom
   END IF

   -- Totalizando desgloce 
   CALL facing001_totpago()

  AFTER FIELD depmon
   -- Verificando deposito
   IF w_mae_tra.depmon IS NULL OR
      (w_mae_tra.depmon <=0) THEN
      LET w_mae_tra.depmon = 0
      DISPLAY BY NAME w_mae_tra.depmon
   END IF

   -- Totalizando desgloce 
   CALL facing001_totpago()

  AFTER INPUT    
   -- Verificando datos
   IF w_mae_tra.efecti IS NULL THEN
      NEXT FIELD efecti
   END IF
   IF w_mae_tra.cheque IS NULL THEN
      NEXT FIELD cheque
   END IF
   IF w_mae_tra.tarcre IS NULL THEN
      NEXT FIELD tarcre
   END IF
   IF w_mae_tra.ordcom IS NULL THEN
      NEXT FIELD ordcom
   END IF
   IF w_mae_tra.depmon IS NULL THEN
      NEXT FIELD depmon
   END IF
  END INPUT
  --Debe ingresar un valor
  IF w_mae_tra.efecti IS NULL OR w_mae_tra.efecti = 0.0 THEN
     IF w_mae_tra.cheque IS NULL OR w_mae_tra.cheque = 0.0 THEN
        IF w_mae_tra.tarcre IS NULL OR w_mae_tra.tarcre = 0.0 THEN
           IF w_mae_tra.ordcom IS NULL OR w_mae_tra.ordcom = 0.0 THEN
              IF w_mae_tra.depmon IS NULL OR w_mae_tra.depmon = 0.0 THEN 
                 CALL fgl_winmessage(" Atencion","Debe ingresar desglose de pago","stop") 
                 GOTO inpag 
              END IF 
           END IF 
        END IF 
     END IF 
  END IF    
  --Validando total vs recibido
  LET recibido = 0.0
  IF w_mae_tra.efecti IS NOT NULL THEN LET recibido = recibido + w_mae_tra.efecti END IF
  IF w_mae_tra.cheque IS NOT NULL THEN LET recibido = recibido + w_mae_tra.cheque END IF
  IF w_mae_tra.tarcre IS NOT NULL THEN LET recibido = recibido + w_mae_tra.tarcre END IF
  IF w_mae_tra.ordcom IS NOT NULL THEN LET recibido = recibido + w_mae_tra.ordcom END IF
  IF w_mae_tra.depmon IS NOT NULL THEN LET recibido = recibido + w_mae_tra.depmon END IF
  IF w_mae_tra.totpag <> recibido THEN 
     CALL fgl_winmessage(" Atencion","Total del documento no es igual al total recibido, por favor revise","stop") 
     GOTO inpag 
  END IF 
  CALL facing001_totpago()
  IF regreso THEN
     EXIT WHILE
  END IF

  -- Verificanod totales del detalle del producto versus total desgloce
   IF w_mae_tra.tipfac = "E" THEN
      IF (w_mae_tra.totpag!=w_mae_tra.tdodol) THEN
         LET msg = " Total Recibido diferente al Total a Pagar. \n VERIFICA que los totales sean iguales."

         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         LET loop = TRUE 
         CONTINUE WHILE 
      END IF
   ELSE 
      IF (w_mae_tra.totpag!=w_mae_tra.totdoc) THEN
         LET msg = " Total Recibido diferente al Total a Pagar. \n VERIFICA que los totales sean iguales."

         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         LET loop = TRUE 
         CONTINUE WHILE 
      END IF
   END IF
 END WHILE

 OPTIONS INPUT NO WRAP 

 RETURN regreso 
END FUNCTION

-- Subrutina para incrementar el porcentaje de tarjeta de credito 

FUNCTION facing001_incrementotar()
 -- Incrementando al precio 
 IF haytar THEN 
    LET w_mae_tra.totpag = (w_mae_tra.totpag*(1+(portar/100))) 
 ELSE 
    IF NOT w_mae_tra.hayord THEN 
       LET w_mae_tra.totpag = totval
    ELSE
       LET w_mae_tra.totpag = w_mae_ord.totabo 
    END IF 
 END IF 
 DISPLAY BY NAME w_mae_tra.totpag 
END FUNCTION

-- Subrutina para totalizar el desgloce de pago 

FUNCTION facing001_totpago()
 -- Verificando si documento es de contado
 IF NOT w_mae_tra.credit THEN 
    -- Totalizando desgloce 
    LET w_mae_tra.subtot = (w_mae_tra.efecti+w_mae_tra.cheque+w_mae_tra.tarcre+w_mae_tra.ordcom+w_mae_tra.depmon)
 ELSE
    -- Totalizando desgloce 
    LET w_mae_tra.efecti = 0
    LET w_mae_tra.cheque = 0
    LET w_mae_tra.tarcre = 0
    LET w_mae_tra.ordcom = 0
    LET w_mae_tra.depmon = 0
    LET w_mae_tra.subtot = totval
 END IF 

 -- Verificando si documento esta exento de impuesto
 IF w_mae_tra.tipfac = "E" THEN
   LET w_mae_tra.totiva = 0
 ELSE
   IF NOT w_tip_doc.exeimp THEN --Calcula IVA
      LET w_mae_tra.totiva = (w_mae_tra.subtot-(w_mae_tra.subtot/(1+(w_mae_tra.poriva/100)))) 
   ELSE
      LET w_mae_tra.totiva = 0
   END IF
END IF

 IF w_mae_ord.totabo > 0 THEN 
   --LET w_mae_tra.totdoc = totval --(w_mae_tra.subtot)
   LET w_mae_tra.totdoc = w_mae_tra.subtot
 ELSE   
   LET w_mae_tra.totdoc = totval
 END IF   
 LET w_mae_tra.tdodol = totvaldol
   
 LET w_mae_tra.subtot = (w_mae_tra.totdoc-w_mae_tra.totiva) 
 
 IF w_mae_tra.hayord THEN 
   LET w_mae_tra.tdodol = w_mae_ord.totord 
   LET totvaldol = w_mae_ord.totord 
   --LET w_mae_tra.subtot = w_mae_ord.totord 
   --LET w_mae_tra.totdoc = w_mae_ord.totord 
END IF
 DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.cheque,w_mae_tra.tarcre,w_mae_tra.ordcom,
                 w_mae_tra.depmon,w_mae_tra.totiva,w_mae_tra.totdoc
END FUNCTION

-- Subrutina para ingresar la clave de confirmacion antes de grabar
FUNCTION facing001_confirmacion()
 DEFINE passwd    LIKE fac_usuaxpos.passwd,
        username  LIKE fac_usuaxpos.userid,
        confirm   SMALLINT

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wsecurity AT 9,27
  WITH FORM "facing001c"

  -- Ingresando clave de confirmacion 
  LET confirm = TRUE  
  INPUT BY NAME passwd
   --ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=TRUE)
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE)

   ON ACTION cancel
    LET confirm = FALSE 
    EXIT INPUT 

   AFTER FIELD passwd
    -- Verificanod password
    IF LENGTH(passwd)<=0 THEN
       ERROR "Error: password invalido ..."
       NEXT FIELD passwd
    END IF 

    -- Verificando cajero por medio de su password corto
    LET username = NULL 
    SELECT UNIQUE a.userid
     INTO  username 
     FROM  fac_usuaxpos a
     WHERE a.numpos = w_mae_tra.numpos
       AND a.passwd = passwd
     IF (status=NOTFOUND) THEN
        ERROR "Error: password incorrecto ..."
        NEXT FIELD passwd 
     END IF
  END INPUT 
 CLOSE WINDOW wsecurity 

 RETURN confirm,username 
END FUNCTION 

-- Subrutina para grabar la facturacion 
FUNCTION facing001_grabar()
 DEFINE i,correl SMALLINT,
        pct      DEC(9,6) ,
		  flag     SMALLINT,
        vfac_id INTEGER,
        cmd STRING
 DEFINE reg_e RECORD
   estatus LIKE facturafel_e.estatus,
   serie_e LIKE facturafel_e.serie_e,
   numdoc_e LIKE facturafel_e.numdoc_e,
   autorizacion LIKE facturafel_e.autorizacion
 END RECORD


	LET flag=FALSE

 -- Grabando transaccion
 ERROR " Registrando Facturacion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grababdo encabezado de la facturacion 
   -- Asignando datos
   IF w_mae_tra.tascam IS NULL THEN LET w_mae_tra.tascam=0 END IF
   IF w_mae_tra.cheque IS NULL THEN LET w_mae_tra.cheque=0 END IF
   IF w_mae_tra.efecti IS NULL THEN LET w_mae_tra.efecti=0 END IF
   IF w_mae_tra.tarcre IS NULL THEN LET w_mae_tra.tarcre=0 END IF
   IF w_mae_tra.ordcom IS NULL THEN LET w_mae_tra.ordcom=0 END IF
   IF w_mae_tra.depmon IS NULL THEN LET w_mae_tra.depmon=0 END IF
   IF w_mae_tra.subtot IS NULL THEN LET w_mae_tra.subtot=0 END IF
   IF w_mae_tra.totdoc IS NULL THEN LET w_mae_tra.totdoc=0 END IF
   IF w_mae_tra.totiva IS NULL THEN LET w_mae_tra.totiva=0 END IF
   IF w_mae_tra.poriva IS NULL THEN LET w_mae_tra.poriva=0 END IF
   IF w_mae_tra.totpag IS NULL THEN LET w_mae_tra.totpag=0 END IF
   IF w_mae_tra.saldis IS NULL THEN LET w_mae_tra.saldis=0 END IF
   LET w_mae_tra.lnktra = 0 
   LET w_mae_tra.exenta = w_tip_doc.exeimp

   IF w_mae_tra.tipfac = "E" THEN
      LET w_mae_tra.moneda = 2
   ELSE
      LET w_mae_tra.moneda = 1
   END IF
   
   LET w_mae_tra.feccor = NULL
   LET w_mae_tra.usranl = NULL
   LET w_mae_tra.fecanl = NULL
   LET w_mae_tra.horanl = NULL
   LET w_mae_tra.lnkinv = 0 

   -- Verificando tipo de moneda para obtener la tasa de cambio
   IF (w_mae_tra.moneda=2) THEN
      -- Moneda en Dolares
      LET w_mae_tra.tascam = tascom                  
   ELSE
      -- Moneda en Quetzales
      LET w_mae_tra.tascam = tasvta 
   END IF

	DISPLAY "HAY ",w_mae_tra.hayord
   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO fac_mtransac
   VALUES (w_mae_tra.*)
   LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 
	IF STATUS <> 0 THEN
		LET flag = TRUE
	END IF

   -- 2. Grabando detalle de la facturacion 
   -- Si no es orden de trabajo
	DISPLAY "flag ",flag
	IF flag = FALSE THEN
   	IF NOT w_mae_tra.hayord THEN 
    		LET correl = 0
    		FOR i = 1 TO totlin 
     		IF v_products[i].cditem IS NULL OR 
        		v_products[i].canuni IS NULL THEN 
        		CONTINUE FOR 
     		END IF 
     		LET correl = (correl+1) 
		
			LET w_mae_tra.hayord=flag_especial
     		-- Grabando
     		SET LOCK MODE TO WAIT
     		INSERT INTO fac_dtransac 
     		VALUES (w_mae_tra.lnktra    , -- link del encabezado
             		correl              , -- correlativo de ingreso
             		w_mae_pos.codemp    , -- empresa del pos
             		w_mae_pos.codsuc    , -- sucursal del pos
             		w_mae_pos.codbod    , -- bodega del pos 
             		v_products[i].codpro, -- codigo del producto 
             		v_products[i].cditem, -- codigo del producto abreviado
             		v_products[i].unimed, -- unidad de medida del producto 
             		v_products[i].canori, -- cantidad original
             		v_products[i].unimto, -- unidad de medida de la medida de venta
             		v_products[i].canuni, -- cantidad total 
             		v_products[i].preuni, -- precio unitario 
             		v_products[i].totpro, -- valor total 
             		v_products[i].factor,
                  v_products[i].deitemh,
                  v_products[i].predol,
                  v_products[i].prtdol ) --Descripcion especial del producto
					--	1,
					--	v_products[i].nuitemf ) -- factor
		
     		
    		END FOR 
			UPDATE fac_mtransac set lnkinv=1 WHERE lnktra=w_mae_tra.lnktra

         ###############################################################
         #Registro en tablas de FEL
         IF es_dte = "S" THEN 
            CALL registra_tablas_FEL(1) RETURNING flag  --Inserta factura electronica
            IF flag = TRUE THEN
               ROLLBACK WORK 
            END IF
         END IF    
         ###############################################################
   	ELSE
    		-- Orden de trabajo 
    		LET w_mae_ord.lnkord = 0
    		LET w_mae_ord.lnktra = w_mae_tra.lnktra 
    		LET w_mae_ord.numpos = w_mae_tra.numpos 
    		LET w_mae_ord.codcli = w_mae_tra.codcli
    		LET w_mae_ord.userid = w_mae_tra.userid
    		LET w_mae_ord.fecsis = w_mae_tra.fecsis
    		LET w_mae_ord.horsis = w_mae_tra.horsis  
		
	 		IF w_det_ord.sinord THEN
		 		LET w_det_ord.nofase=5
	 		END IF
    		-- Insertando orden de trabajo
			DISPLAY "inserta 1!"
    		SET LOCK MODE TO WAIT 
    		INSERT INTO inv_morden  
    		VALUES (w_mae_ord.*) 
    		LET w_mae_ord.lnkord = SQLCA.SQLERRD[2] 
			IF STATUS = 0 THEN
	 			FOR i = 1 TO v_ordenes.getLength()
					LET v_ordenes[i].lnkord1=w_mae_ord.lnkord
					LET v_ordenes[i].sinord1=sin_orden
         		IF sin_orden THEN
            		LET v_ordenes[i].nofase1 = 5 
         		END IF
					IF v_ordenes[i].lnkord1 IS NOT NULL AND
						v_ordenes[i].subcat1 IS NOT NULL AND
						v_ordenes[i].codcol1 IS NOT NULL AND
						v_ordenes[i].nuitem1 IS NOT NULL THEN
						LET v_ordenes[i].nuitem1=i 
						INSERT INTO inv_dorden VALUES (v_ordenes[i].*)
						IF STATUS <> 0 THEN
							EXIT FOR
						END IF
					END IF
				END FOR
			ELSE
				LET flag = TRUE
			END IF

         #######################################################################
         #Registro en tablas de FEL
         IF es_dte = "S" THEN 
            CALL registra_tablas_FEL(1) RETURNING flag  --Inserta factura electronica
            IF flag = TRUE THEN
               ROLLBACK WORK 
            END IF
         END IF    
         #######################################################################
    		DISPLAY BY NAME w_mae_ord.lnkord ATTRIBUTES(RED)
   	END IF 
	
		IF flag_especial = TRUE AND flag = FALSE THEN
         LET w_mae_ord.lnkord = 0
         LET w_mae_ord.lnktra = w_mae_tra.lnktra
         LET w_mae_ord.numpos = w_mae_tra.numpos
         LET w_mae_ord.codcli = w_mae_tra.codcli
         LET w_mae_ord.userid = w_mae_tra.userid
         LET w_mae_ord.fecsis = w_mae_tra.fecsis
         LET w_mae_ord.horsis = w_mae_tra.horsis

			IF box_pregunta("Solicitar a Confeccion?") = 1 THEN
				LET w_det_ord.sinord=0
			ELSE
				LET w_det_ord.sinord=1 
			END IF

         -- Insertando orden de trabajo
			DISPLAY "inserta 2!"
         SET LOCK MODE TO WAIT
         INSERT INTO inv_morden  
         VALUES (w_mae_ord.*)
         LET w_mae_ord.lnkord = SQLCA.SQLERRD[2]
         IF STATUS = 0 THEN
            FOR i = 1 TO v_ordenes.getLength()
               LET v_ordenes[i].lnkord1=w_mae_ord.lnkord
					LET v_ordenes[i].sinord1=w_det_ord.sinord
               IF v_ordenes[i].lnkord1 IS NOT NULL AND
                  v_ordenes[i].subcat1 IS NOT NULL AND
                  v_ordenes[i].codcol1 IS NOT NULL AND
                  v_ordenes[i].nuitem1 IS NOT NULL THEN
						LET v_ordenes[i].nuitem1=i
                  INSERT INTO inv_dorden VALUES (v_ordenes[i].*)
                  IF STATUS <> 0 THEN
                     EXIT FOR
                  END IF
               END IF
            END FOR
         ELSE
            LET flag = TRUE
         END IF
         
		END IF
      
	END IF


 IF flag = FALSE THEN

   -- Finalizando la transaccion
 	COMMIT WORK

   -- Desplegando mensaje
   CALL fgl_winmessage(" Atencion","Transaccion registrada.","information")
 	

   --Envia factura a firma
   LET vfac_id = NULL;
      
   SELECT a.fac_id
   INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra

   IF es_dte = "S" THEN
   
      LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
      
      IF ande_desa = 1 THEN 
         GOTO _imprimir 
      ELSE    
         DISPLAY "cmd: ",cmd
         RUN cmd
      END IF
      
   END IF 

   SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
      INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      
   DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion

   IF reg_e.estatus = "P" AND es_dte = "S" THEN
      -- Desplegando mensaje
      CALL fgl_winmessage(" Atencion","El documento no pudo ser firmado electronicamente.","stop")
   ELSE
      -- Desplegando mensaje
      IF es_dte = "S" THEN 
         CALL fgl_winmessage(" Atencion","No. de autorizaci�n electronica:"||reg_e.autorizacion,"information")
      END IF 

      LABEL _imprimir:
      -- Imprimiendo movimiento
      ERROR "Imprimiendo ... por favor espere ...."
	
      -- Verificando si pago es con tarjeta
      IF haytar THEN
         LET pct = portar
      ELSE
         LET pct = 0 
      END IF 
	
      CALL facrpt001_facturacion(pct)
      ERROR ""
   END IF 
   
 	 
 ELSE
	CALL fgl_winmessage(" Atencion","Error en la transaccion, no se ha grabado","stop")
 END IF
END FUNCTION 

-- Subrutina para buscar si un numero de documento existe
FUNCTION facing001_bdocumento()
 -- Verificando si el documento existe
 SELECT UNIQUE a.numdoc
  FROM  fac_mtransac a
  WHERE a.codemp = w_mae_tra.codemp
    AND a.tipdoc = w_mae_tra.tipdoc
    AND a.nserie = w_mae_tra.nserie 
    AND a.numdoc = w_mae_tra.numdoc 
 IF (status=NOTFOUND) THEN
    RETURN FALSE 
 ELSE
    RETURN TRUE 
 END IF 
END FUNCTION 

-- Subrutina para grabar un documento como anulado
FUNCTION facing001_anular(userauth)
 DEFINE userauth VARCHAR(15)

 -- Granando documento como anulado 
 ERROR " Anulando Documento ..." ATTRIBUTE(CYAN)

 -- Iniciando Transaccion
 BEGIN WORK

  -- Asignando datos
  LET w_mae_tra.lnktra = 0 
  LET w_mae_tra.codcli = 0
  LET w_mae_tra.numnit = "NINGUNO"
  LET w_mae_tra.nomcli = "NINGUNO"
  LET w_mae_tra.dircli = "NINGUNO" 
  LET w_mae_tra.exenta = 0 
  LET w_mae_tra.moneda = 0
  LET w_mae_tra.tascam = 0 
  LET w_mae_tra.estado = "A" 
  LET w_mae_tra.feccor = NULL 
  LET w_mae_tra.userid = userauth        
  LET w_mae_tra.usrope = userauth        
  LET w_mae_tra.motanl = "GRABADO COMO ANULADA POR PROBLEMAS EN LA FORMA"
  LET w_mae_tra.usranl = userauth 
  LET w_mae_tra.fecanl = CURRENT 
  LET w_mae_tra.horanl = CURRENT HOUR TO SECOND
  LET w_mae_tra.lnkinv = 0 

  -- Grabando 
  SET LOCK MODE TO WAIT
  INSERT INTO fac_mtransac
  VALUES (w_mae_tra.*) 

 -- Finalizando Transaccion
 COMMIT WORK

 CALL fgl_winmessage(
 " Atencion",
 " Numero de documento registrado como anulado.",
 "stop")

 ERROR ""
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing001_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*,w_mae_ord.*,w_det_ord.* TO NULL 
	CALL v_ordenes.clear()
   CLEAR FORM 
 END CASE 
 -- Inicializando datos
 LET w_mae_tra.numpos = xnumpos
 LET w_mae_tra.estado = "V" 
 LET w_mae_tra.credit = 0
 LET w_mae_tra.tipfac = "L"
 LET w_mae_tra.tipoid = 0
 LET w_mae_tra.hayord = ordenes 
 LET w_mae_tra.fecemi = TODAY 
 LET w_mae_tra.poriva = tasimp 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET w_mae_tra.efecti = 0
 LET w_mae_tra.cheque = 0
 LET w_mae_tra.tarcre = 0
 LET w_mae_tra.ordcom = 0 
 LET w_mae_tra.depmon = 0 
 LET w_mae_tra.subtot = 0
 LET w_mae_tra.totiva = 0
 LET w_mae_tra.totdoc = 0
 LET w_mae_tra.totpag = 0 
 LET w_mae_tra.saldis = 0 
 LET w_det_ord.nuitem = 1
 LET w_mae_ord.totord = 0
 LET w_mae_ord.totabo = 0
 LET w_mae_ord.salord = 0
 LET w_det_ord.medida = 1  
 LET w_det_ord.tipord = 1  
 LET w_mae_ord.estado = "V" 
 LET w_det_ord.nofase = 1
 LET w_mae_ord.fecofe = TODAY+3
 LET w_mae_ord.fecent = TODAY+3
 LET w_det_ord.premed = 0 
 LET w_det_ord.medida = 1 
 LET w_det_ord.sinord = 0 
 LET w_det_ord.ordext = 1 
 LET totlin           = 0 
 LET totuni           = 0
 LET totval           = 0
 LET vuelto           = 0 
 LET w_det_ord.desman = 0

 -- Obteniendo datos del punto de venta
 INITIALIZE w_mae_pos.* TO NULL
 CALL librut003_bpuntovta(w_mae_tra.numpos)
   RETURNING w_mae_pos.*,existe 
 CALL f.setElementText("labela","PUNTO DE VENTA [ "||w_mae_pos.nompos CLIPPED||" ]")

 -- Llenando combo de tipos de documento x punot de venta
 CALL librut003_cbxtiposdocxpos(w_mae_tra.numpos)

 -- Inicializando vectores de datos
 CALL facing001_inivec() 

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_pos.codsuc)
 RETURNING w_mae_suc.*,existe
 
 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecemi,w_mae_tra.horsis
 DISPLAY BY NAME totuni,totval,totlin,vuelto
 DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.cheque,w_mae_tra.tarcre,w_mae_tra.ordcom,w_mae_tra.totiva,w_mae_tra.totdoc
 DISPLAY BY NAME w_mae_tra.estado,w_mae_tra.hayord,w_mae_tra.depmon,w_mae_tra.totpag
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 
FUNCTION facing001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_products[i].*
 END FOR 
END FUNCTION

-- Subrutina para el ingreso y actualizacion del precio sugerido 
FUNCTION facing001_actualiza_pre(id)
 DEFINE precio   DECIMAL(12,2 ),
	id       INTEGER, 
        regreso  SMALLINT,
        query    VARCHAR(255,0)

 -- Creadno estatuto  
 LET query = " UPDATE inv_products SET "," inv_products.presug =? "," WHERE inv_products.cditem =?"
 PREPARE prpUPD_pre FROM query

 CALL fgl_winmessage(
 " Atencion",
 " Producto no tiene precio definido, definirlo para poder facturar.",
 "stop")

 -- Ingredando precio 
 OPEN WINDOW wing001d AT 5,2 WITH FORM "facing001d"

  LET regreso = FALSE 
  INPUT BY NAME precio WITHOUT DEFAULTS
   ATTRIBUTE(UNBUFFERED) 

   ON ACTION cancel
    LET regreso = TRUE
	 EXIT INPUT 

	ON KEY (CONTROL-E)
	 LET regreso =TRUE
	 EXIT INPUT

   AFTER FIELD precio
    -- Verificando precio 
    IF precio IS NULL OR
       precio <=0 THEN
       ERROR "Error: precio del producto invalido, VERIFICA ..." 
       NEXT FIELD precio
    END IF
  END INPUT 

  -- Actualizando 
  IF NOT regreso THEN 
   BEGIN WORK
    EXECUTE prpUPD_pre USING precio,id 
    IF STATUS=0 THEN
       COMMIT WORK
    ELSE
       ROLLBACK WORK
    END IF
   END IF 
  CLOSE WINDOW wing001d

 RETURN precio
END FUNCTION

FUNCTION facing001_ver_especial()
DEFINE vfac_id INTEGER,
cmd string
	SELECT *
	  INTO w_mae_ord.*
	  FROM inv_ordentra
	  WHERE lnktra = w_mae_tra.lnktra

	OPEN WINDOW veres
	WITH FORM "facing001e"
   
	MENU

    ON ACTION Fac_electronica2
      --IF es_dte = "N" THEN 

      --IF jjjjjj
      LET vfac_id = NULL;
      
      SELECT a.fac_id
      INTO vfac_id
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      
      LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
      
      RUN cmd


		BEFORE MENU
    -- Cargando combobox 
 			CALL librut003_cbxcolores() 
			CALL librut003_cbxsubcateg()
			CALL librut003_cbxfasesot() 
			DISPLAY BY NAME w_mae_ord.lnkord,
								 w_det_ord.nuitem,
								 w_det_ord.nofase,
								 w_mae_ord.fecord,
								 w_det_ord.cantid,
								 w_det_ord.subcat,
								 w_det_ord.codcol,
								 w_det_ord.desman,
								 w_det_ord.medida,
								 w_det_ord.xlargo,
								 w_det_ord.yancho,
								 w_det_ord.descrp,
								 w_det_ord.observ,
								 w_mae_ord.fecofe,
								 w_mae_ord.fecent,
								 w_mae_ord.totord,
								 w_mae_ord.totabo,
								 w_mae_ord.salord,
								 w_mae_ord.doctos, 
								 w_det_ord.precio
		ON ACTION cancel
			EXIT MENU

	END MENU

	INITIALIZE w_mae_ord.* TO NULL
	CLOSE WINDOW veres

END FUNCTION

FUNCTION facing001_existencias(prod,cant)
DEFINE flag_act smallint,
		 arr , s_line SMALLINT,
		 prod VARCHAR(30),
		 cant decimal(4,2),
		 i    SMALLINT,
		 count SMALLINT,
		 total DECIMAL (4,2),
		 length SMALLINT,
		 lastkey INTEGER

	LET length=0
   OPTIONS INPUT WRAP 
	CALL facing001_llena_exis(prod)
	OPEN WINDOW existencias WITH FORM "facing001f.per"
	LET flag_act = FALSE
	LET count = 0
	LET total=0

	IF v_existencia.getLength() = 0 THEN
		ERROR "No hay existencias en ninguna bodega, verifique"
		CLOSE WINDOW existencias
		RETURN FALSE
	END IF

	LET length=v_existencia.getLength()

	INPUT ARRAY v_existencia from existencias.* ATTRIBUTES(WITHOUT DEFAULTS)
	
		BEFORE INPUT
			LET int_flag = FALSE
			CALL DIALOG.setActionHidden("append",TRUE)
			CALL DIALOG.setActionHidden("close",TRUE)
			CALL DIALOG.setActionHidden("insert",TRUE)
			DISPLAY BY NAME total
			DISPLAY cant TO necesarios 
 

		ON ACTION accept
			LET flag_act=TRUE 
			GOTO :fuera
				
		ON ACTION cancel
			LET flag_act=FALSE
			GOTO :fuera
				

		AFTER FIELD salcan
			LET arr=ARR_CURR()
			LET s_line=SCR_LINE()
			IF v_existencia[arr].salcan IS NULL THEN
				LET v_existencia[arr].salcan = 0
				CALL facing001_totexi() RETURNING total
				DISPLAY BY NAME total
			ELSE
				IF v_existencia[arr].salcan > v_existencia[arr].exiact THEN
					CALL box_valdato("No hay suficientes en esa bodega, verifique")
					LET v_existencia[arr].salcan = 0
					CALL facing001_totexi() RETURNING total
					DISPLAY BY NAME total
					NEXT FIELD salcan
				ELSE
					CALL facing001_totexi() RETURNING total
					IF total > cant THEN 
						CALL box_valdato("Usted eligio mas productos de los necesarios, verifique")
						LET v_existencia[arr].salcan = 0
						CALL facing001_totexi() RETURNING total
						DISPLAY BY NAME total
						NEXT FIELD salcan
					ELSE
					   LET lastkey = FGL_LASTKEY()
						IF (arr = length) AND ((lastkey = FGL_KEYVAL("down")) OR (lastkey = FGL_KEYVAL("tab")) OR (lastkey = FGL_KEYVAL("return"))) THEN
							ERROR "Ya no hay mas bodegas . . . "
							CALL facing001_totexi() RETURNING total
							DISPLAY BY NAME total
							NEXT FIELD salcan
						ELSE
							CALL facing001_totexi() RETURNING total
							DISPLAY BY NAME total
						END IF
					END IF
				END IF
			END IF
{
		BEFORE FIELD salcan
			LET arr=ARR_CURR()
			LET s_line=SCR_LINE()
			IF v_existencia[arr].bodega IS NULL THEN
				ERROR "Ya no hay mas bodegas . . . "
				CALL v_existencia.deleteElement(arr)
			ELSE
				CALL facing001_totexi() RETURNING total
				DISPLAY BY NAME total
			END IF
}			
		AFTER INPUT
			LABEL fuera:
			IF total > cant AND flag_act = TRUE THEN
				CALL box_valdato ("Usted eligio mas producto del que vendera")
				NEXT FIELD salca
			END IF
			IF flag_act THEN
				SELECT COUNT(*)
					INTO count
					FROM existencia
					WHERE codabr=prod
				IF count <= 0 THEN
					FOR i = 1 TO v_existencia.getLength()
						INSERT INTO existencia VALUES (v_existencia[i].bodega, v_existencia[i].nombod,v_existencia[i].exiact,v_existencia[i].salcan,prod)
					END FOR
				ELSE
					FOR i = 1 TO v_existencia.getLength()
						UPDATE existencia SET salcan = v_existencia[i].salcan
							WHERE bodega=v_existencia[i].bodega
							AND   nombod=v_existencia[i].nombod 
							AND   codabr=prod
					END FOR
				END IF
				LET flag_act = TRUE
			END IF
			
	END INPUT
	
	CLOSE WINDOW existencias
   OPTIONS INPUT NO WRAP 

RETURN flag_act
	
END FUNCTION

FUNCTION facing001_llena_exis(nomabr)
DEFINE 
	nomabr 	VARCHAR(30),
	i      	SMALLINT   ,
	tSQL    	VARCHAR(512),
	COUNT  	SMALLINT

	LET count=0
	SELECT COUNT(*)
	INTO count
	FROM existencia
	WHERE codabr=nomabr
	
	IF count = 0 THEN
		LET tsql = " SELECT a.codbod , a.nombod, b.exican,0 ",
					 " FROM inv_mbodegas a , inv_proenbod b   ",
		  			 " WHERE a.codemp = b.codemp ",
		 			 " AND   a.codsuc = b.codsuc ",
			 		 " AND   a.codbod = b.codbod ",
					 " AND   b.exican > 0        ",
			 		 " AND   b.codabr = '", nomabr CLIPPED,"'"

		PREPARE sql1 FROM tsql

		DECLARE existencia CURSOR FOR sql1
		OPEN existencia
		CALL v_existencia.clear()
		LET i = 1
		FOREACH existencia INTO v_existencia[i].*
			LET i = i +1
		END FOREACH
		FOR i = 1 TO v_existencia.getLength()
			IF v_existencia[i].bodega IS NULL THEN
				CALL v_existencia.deleteElement(i)
			END IF
		END FOR
	ELSE
		DECLARE existencia2 CURSOR FOR
			SELECT bodega, nombod, exiact, cansal
				FROM existencia
				WHERE codabr=nomabr

		LET i = 1

		FOREACH existencia INTO v_existencia[i].*
			LET i = i +1
		END FOREACH

		FOR i = 1 TO v_existencia.getLength()
			IF v_existencia[i].bodega IS NULL THEN
				CALL v_existencia.deleteElement(i)
			END IF
		END FOR
	END IF

	CLOSE existencia

END FUNCTION
 
 

FUNCTION facing001_tmp_db(opc)
DEFINE opc SMALLINT

IF opc = 1 THEN
	LET flag_tmp = TRUE
	CREATE TEMP TABLE existencia (
    bodega  INTEGER,
	 nombod  VARCHAR(30),
	 exiact  DECIMAL(4,2),
	 salcan  DECIMAL(4,2),
	 codabr  VARCHAR(20)
	)
ELSE
	DROP TABLE existencia
	LET flag_tmp = FALSE
END IF
END FUNCTION

FUNCTION facing001_totexi()
  DEFINE total  DECIMAL(4,2),
			i      SMALLINT
	LET total=0
	FOR i = 1 to v_existencia.getLength()
		IF v_existencia[i].salcan IS NULL THEN
			LET v_existencia[i].salcan=0
		END IF
		LET total=total+v_existencia[i].salcan
	END FOR
RETURN total
END FUNCTION

FUNCTION inving001_input_compre()
DEFINE flag_act SMALLINT

	INPUT BY NAME w_mae_tra.obspre WITHOUT DEFAULTS
		
		AFTER FIELD obspre
			IF w_mae_tra.obspre IS NULL THEN
				CALL fgl_winmessage("Atention","Debe ingresar una observacion valida","information")
				NEXT FIELD obspre
			END IF

		ON KEY (ESCAPE)
			LET w_mae_tra.obspre = NULL
			LET flag_act = TRUE
			EXIT INPUT

		ON ACTION cancel
			LET w_mae_tra.obspre = NULL
			LET flag_act = TRUE
			EXIT INPUT

		ON ACTION accept
			LET flag_act = FALSE
			EXIT INPUT


		AFTER INPUT
			IF flag_act = FALSE THEN
				IF w_mae_tra.obspre IS NULL THEN
					CALL fgl_winmessage("Atention","Debe ingresar una observacion valida","information")
					NEXT FIELD obspre
				END IF
			END IF

	END INPUT

RETURN flag_act
END FUNCTION

FUNCTION facing001_abono_orden()
DEFINE flag SMALLINT
INPUT BY NAME   w_mae_ord.fecofe,
                w_mae_ord.fecent,
                --w_mae_ord.totord,
                w_mae_ord.totabo,
                w_mae_ord.doctos  WITHOUT DEFAULTS

	ON ACTION accept
		LET flag = TRUE
		GOTO :fuera

	ON ACTION cancel
		LET flag = FALSE
		GOTO :fuera

	ON KEY(ESCAPE)
		LET flag = FALSE
		GOTO :fuera

   AFTER FIELD fecofe
    -- Verificando fecha sugerida
    IF w_mae_ord.fecofe IS NULL OR
       w_mae_ord.fecofe <TODAY THEN
       ERROR "Error: fecha sugerida invalida, VERIFICA ..."
       NEXT FIELD fecofe
    END IF

   AFTER FIELD fecent
    -- Verificando fecha de entrega
    IF w_mae_ord.fecent IS NULL OR
       w_mae_ord.fecent <TODAY THEN
       ERROR "Error: fecha de entrega invalida, VERIFICA ..."
       NEXT FIELD fecent
    END IF

   ON CHANGE totabo
    -- Calculando saldo de la orden
--    CALL facing001_totalord(w_det_ord.premed)
		LET w_mae_ord.salord = (w_mae_ord.totord-w_mae_ord.totabo)
		LET w_mae_tra.totpag = w_mae_ord.totabo
		DISPLAY BY NAME w_mae_tra.totpag,w_mae_ord.salord
		DISPLAY w_mae_ord.salord, " - ",w_mae_ord.totabo
   --BEFORE FIELD totabo
    -- Activando boton de precios
    --CALL DIALOG.setActionActive("precios",TRUE)

   AFTER FIELD totabo
    -- Verificanod si es credito
    IF NOT w_mae_tra.credit THEN
     -- Verificando abono de la orden
     IF w_mae_ord.totabo IS NULL OR
        w_mae_ord.totabo <0 OR
        w_mae_ord.totabo >w_mae_ord.totord THEN
        ERROR "Error: abono de la orden invalido, VERIFICA ..."
        LET w_mae_ord.totabo = NULL
        CLEAR totabo
        NEXT FIELD totabo
     END IF
    --ELSE
     -- Si es credito el abono debe ser cero
     --LET w_mae_ord.totabo = 0
     --DISPLAY BY NAME w_mae_ord.totabo
    END IF

	AFTER INPUT
		LABEL fuera:
		IF flag = TRUE THEN
			IF w_mae_ord.totord < w_mae_ord.totabo THEN
				CALL box_valdato("Abono no puede ser mayor a total a pagar")
				NEXT FIELD totabo
			END IF
		END IF

    -- Calculando saldo de la orden
    --CALL facing001_totalord(w_det_ord.premed)

    -- Activando boton de precios
    --IF flag_especial = FALSE THEN
       --CALL DIALOG.setActionActive("precios",FALSE)
    --END IF
RETURN flag
END INPUT

END FUNCTION

FUNCTION facin001_suma()
DEFINE i             SMALLINT,
		 total   DECIMAL (12,2)
	LET total = 0.00
	FOR i = 1 TO v_ordenes.getLength()
		IF v_ordenes[i].nuitem1 IS NOT NULL AND
			v_ordenes[i].precio1 IS NOT NULL AND
			v_ordenes[i].cantid1 IS NOT NULL THEN
			LET total = total + (v_ordenes[i].precio1*v_ordenes[i].cantid1)
		END IF
	END FOR
		 
--LET w_mae_ord.totabo = 0
LET w_mae_ord.salord = total - w_mae_ord.totabo
DISPLAY BY NAME w_mae_ord.salord, w_mae_ord.salord
RETURN total
END FUNCTION

FUNCTION facing001_ins_upd_del(opc,pos)
DEFINE opc         ,
		 i           ,
		 pos SMALLINT
		 
	CASE opc
		WHEN 1 -- A�ade a la lista
			IF NOT facing001_repetidos() THEN
				LET i = v_ordenes.getLength() + 1
				LET v_ordenes[i].* = w_det_ord.*
				LET w_mae_ord.totord = facin001_suma()
				LET pos = facing001_disp(1) 
			ELSE
				CALL box_valdato("Ya existe una orden con esas caracteristicas")
				RETURN FALSE
			END IF

		WHEN 2 -- Modifica seleccionado
			IF NOT facing001_repetidos() THEN
				INITIALIZE v_ordenes[pos].* TO NULL
				LET v_ordenes[pos].* = w_det_ord.*
				LET w_mae_ord.totord = facin001_suma()
			ELSE
				CALL box_valdato("Ya existe una orden con esas caracteristicas")
				RETURN FALSE
			END IF
		WHEN 3 -- Elimina Seleccionado
			CALL v_ordenes.deleteElement(pos)
			LET w_mae_ord.totord = facin001_suma()
	END CASE

DISPLAY BY NAME w_mae_ord.totord
RETURN TRUE
END FUNCTION

FUNCTION facing001_disp(opc)
DEFINE opc SMALLINT,
		 pos INT

	DISPLAY ARRAY v_ordenes TO  s_ordenes.*
 
		BEFORE ROW
			LET pos = 0
			IF  opc = 1 THEN
				EXIT DISPLAY
			END IF
			CALL facqbx001_desp(ARR_CURR())

		BEFORE DISPLAY
			IF opc= 2 THEN
			   CALL DIALOG.setActionActive("eliminar",1)
			   CALL DIALOG.setActionActive("modificar",1)
			   CALL DIALOG.setActionActive("nuevo",1)
			   CALL DIALOG.setActionActive("eliminar",1)
			ELSE
			   CALL DIALOG.setActionActive("eliminar",0)
			   CALL DIALOG.setActionActive("modificar",0)
			   CALL DIALOG.setActionActive("nuevo",0)
			   CALL DIALOG.setActionActive("eliminar",0)
			END IF

		ON KEY (ESCAPE)
			RETURN 0

		ON ACTION accept
			RETURN 0

		ON ACTION eliminar
			LET pos = ARR_CURR()
			IF facing001_ins_upd_del(3,pos) THEN
			END IF

		ON ACTION modificar
			LET pos = ARR_CURR()
			LET w_det_ord.* = v_ordenes[pos].*
			RETURN pos

		ON ACTION nuevo
			CALL facing001_def_dord()
			EXIT DISPLAY
			
			
	END DISPLAY
RETURN 0	
END FUNCTION

FUNCTION facing001_repetidos()
DEFINE i             ,
		 cont  SMALLINT

	LET cont = 0
	FOR i = 1 TO v_ordenes.getLength()
		IF w_det_ord.subcat  = v_ordenes[i].subcat1 AND
			w_det_ord.codcol  = v_ordenes[i].codcol1 AND
			w_det_ord.medida  = v_ordenes[i].medida1 AND
			w_det_ord.xlargo  = v_ordenes[i].xlargo1 AND
			w_det_ord.yancho  = v_ordenes[i].yancho1 AND
			w_det_ord.nuitem <> v_ordenes[i].nuitem1 THEN
			RETURN TRUE
		END IF
	END FOR

RETURN FALSE
END FUNCTION

 FUNCTION facing001_def_dord()

         INITIALIZE w_det_ord.cantid,w_det_ord.subcat,w_det_ord.codcol,w_det_ord.desman,
                    w_det_ord.medida,w_det_ord.xlargo,w_det_ord.yancho,w_det_ord.descrp,
                    w_det_ord.observ,w_det_ord.sinord,w_det_ord.ordext,w_det_ord.precio,
						  tot  TO NULL
         CLEAR cantid,subcat,codcol,desman,medida,xlargo,yancho,descrp,observ,sinord,ordext,precio,tot
 			LET w_det_ord.sinord = 0 LET w_det_ord.ordext = 0
 			LET w_det_ord.medida = 1 LET w_det_ord.desman = 0
			LET w_det_ord.nuitem = v_ordenes.getLength() + 1
			DISPLAY BY NAME w_det_ord.nuitem
END FUNCTION

-- Subrutina para cambiar ingresar descripcion especial 

FUNCTION facing001_descespecial(reg_desp)
 DEFINE reg_desp RECORD
   deitem STRING
 END RECORD
 DEFINE cancelo_ingreso SMALLINT

 LET cancelo_ingreso = FALSE
OPTIONS INPUT NO WRAP
 -- Ingresando precios
 INPUT BY NAME reg_desp.deitem WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED) 

  ON ACTION cancel
   -- Regreso
   LET cancelo_ingreso = TRUE
   EXIT INPUT 

 END INPUT 
 OPTIONS INPUT WRAP
 RETURN cancelo_ingreso, reg_desp.deitem
END FUNCTION 

FUNCTION registra_tablas_FEL(tipooperacion)
DEFINE tipooperacion SMALLINT; --1=Emision de factura, 2=Anulaci�n de factura 
DEFINE flag SMALLINT
DEFINE fel_e RECORD LIKE facturafel_e.*
DEFINE fel_ed RECORD LIKE facturafel_ed.*
DEFINE emp_fel RECORD LIKE empresas1.*
DEFINE vtime DATETIME HOUR TO FRACTION(3)
DEFINE i SMALLINT
DEFINE vcodcat SMALLINT;
DEFINE v_subcat, v_color STRING 
DEFINE lprecio_t LIKE facturafel_ed.base

INITIALIZE fel_e.*, fel_ed.*, emp_fel TO NULL
LET vtime = CURRENT HOUR TO FRACTION;
LET flag = FALSE;

IF tipooperacion = 2 THEN
   SELECT a.fac_id
   --INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "FC"
   AND a.estado_doc = "ANULADO"
   IF sqlca.sqlcode = 0 THEN
      GOTO _lreturn
   END IF  
END IF 

SELECT empresas1.*
INTO emp_fel.*
FROM empresas1
WHERE empresas1.bodega = 1


LET fel_e.serie   = w_mae_tra.nserie;
LET fel_e.num_doc = w_mae_tra.numdoc;
LET fel_e.tipod   = "FC"
LET fel_e.es_exenta = 0

--Establecimiento
LET fel_e.sat_establecimiento = NULL 
SELECT sat_establecimiento INTO fel_e.sat_establecimiento 
  FROM fac_puntovta e
   WHERE e.numpos = w_mae_tra.numpos
IF fel_e.sat_establecimiento IS NULL OR fel_e.sat_establecimiento = 0 THEN
   LET fel_e.sat_establecimiento = 1
END IF 

IF tipooperacion = 1 THEN --ingreso
   LET fel_e.fecha   = w_mae_tra.fecemi;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(w_mae_tra.fecemi) USING "&&&&",MONTH(w_mae_tra.fecemi) USING "&&",DAY(w_mae_tra.fecemi) USING "&&",vtime);
   LET fel_e.fecha_anul = NULL; --SE LLENA SOLO SI ES ANULACION
   LET fel_e.estado_doc = "ORIGINAL"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"
   IF w_mae_tra.tipfac = "E" THEN --Exportacion
      IF w_mae_tra.hayord THEN LET w_mae_tra.tdodol = w_mae_ord.totord END IF  
      LET fel_e.total_b          = w_mae_tra.tdodol; --Ej.  117.00
      LET fel_e.total_d          = w_mae_tra.tdodol; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es ero
      LET fel_e.ing_netosg       = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.total_i          = 0;
      LET fel_e.total_iva1       = 0;
      LET fel_e.base1            = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.tasa1            = 0; --Ej.   12.00
      LET fel_e.monto1           = 0; --Ej.   12.00
      LET fel_e.total_neto       = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_tra.tdodol)
      LET fel_e.es_exportacion = 1;
      LET fel_e.incoterm = w_mae_tra.incote;
      LET fel_e.nomconcomprador = w_mae_tra.nomcom;
      LET fel_e.dirconcomprador = w_mae_tra.dircom;
      LET fel_e.otrreferencia = w_mae_tra.otrref;
   ELSE
      LET fel_e.total_b          = w_mae_tra.totdoc; --Ej.  117.00
      LET fel_e.total_d          = w_mae_tra.totdoc; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es cero
      LET fel_e.ing_netosg       = w_mae_tra.subtot; --Ej.  100.00
      LET fel_e.total_i          = w_mae_tra.totiva; --Ej.   12.00 
      LET fel_e.total_iva1       = w_mae_tra.totiva; --Ej.   12.00
      LET fel_e.base1            = w_mae_tra.subtot; --Ej.  100.00 ******
      LET fel_e.tasa1            = w_mae_tra.poriva; --Ej.   12.00
      LET fel_e.monto1           = w_mae_tra.totiva; --Ej.   12.00 ******
      LET fel_e.total_neto       = w_mae_tra.totdoc; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_tra.totdoc)
      LET fel_e.es_exportacion = 0;
      --Si el impuesto viene en 0 o nulo
      IF fel_e.monto1 <= 0 OR fel_e.monto1 IS NULL THEN
         IF w_mae_tra.tipdoc = 4 THEN --factura exenta
            LET fel_e.es_exenta = 1
         ELSE  
            LET fel_e.monto1 = fel_e.base1 / 1.12 * 0.12
         END IF    
      END IF 
   END IF
   LET fel_e.monto_d          = 0                 --Ej.    5.00, poner cero, porque no manejan descuentos
   LET fel_e.tipo1            = "IVA";
   LET fel_e.id_factura       = 0 --Apunta a un apuntador recursivo cuando es anulacion o NC, si es factura va nulo--De donde se obtiene
   
   IF w_mae_tra.totpag = 0 THEN
      LET fel_e.tipo_pago = "CR";
   ELSE
      LET fel_e.tipo_pago = "CO";
   END IF
ELSE
   LET fel_e.fecha   = TODAY;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(fel_e.fecha) USING "&&&&",MONTH(fel_e.fecha) USING "&&",DAY(fel_e.fecha) USING "&&",vtime);
   LET fel_e.fecha_anul = fel_e.fecha_em;
   LET fel_e.estado_doc = "ANULADO"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"

   SELECT facturafel_e.fac_id
   INTO fel_e.id_factura
   FROM facturafel_e
   WHERE facturafel_e.num_int  = w_mae_tra.lnktra
   AND facturafel_e.estado_doc = "ORIGINAL"
   AND facturafel_e.tipod = "FC"

   LET fel_e.tipo_pago        = "CO";
   LET fel_e.nota1 = w_mae_tra.motanl;
END IF

LET fel_e.ordcmp        = w_mae_tra.ordcmp --Orden de compra
LET fel_e.from          = emp_fel.e_mail; --  DEJAR QUEMADO EL CORREO QUE SE COLOCARA AQUI   
LET fel_e.to            = NULL --w_mae_tra.maicli;
LET fel_e.cc            = NULL;
LET fel_e.formats       = "PDF"; 
LET fel_e.tipo_doc      = "FACT"; --DEJAR QUEMADO

LET fel_e.ant_serie = NULL;  
LET fel_e.ant_numdoc = NULL; 
LET fel_e.ant_fecemi = NULL;
LET fel_e.ant_resoluc = NULL;
IF w_mae_tra.moneda = 1 THEN 
   LET fel_e.c_moneda   = "GTQ"; 
   LET fel_e.tipo_cambio      = 1;
ELSE
   LET fel_e.c_moneda   = "USD";
   LET fel_e.tipo_cambio      = w_mae_tra.tascam;
END IF

LET fel_e.num_int          = w_mae_tra.lnktra;
LET fel_e.nit_e            = emp_fel.nit; --tomarlo de la tabla empresas1.nit donde la bodega 1; 
LET fel_e.nombre_c         = emp_fel.nom_sucursal;-- tomarlo de la tabla empresas1.nom_sucursal
LET fel_e.idioma_e         = "ES";
LET fel_e.nombre_e         = emp_fel.razon_social; --tomarlo de la tabla empresas1.razon_social
LET fel_e.codigo_e         = emp_fel.cod_sucursal; --Tomarlo de la tabla empresas1.cod_sucursal
LET fel_e.dispositivo_e    = NULL;
SELECT dirpos INTO emp_fel.direccion1 FROM fac_puntovta WHERE numpos = w_mae_tra.numpos
CASE w_mae_tra.numpos
   WHEN 1 LET fel_e.direccion_e = '         ', emp_fel.direccion1 --zona 9
   WHEN 7 LET fel_e.direccion_e = '         ', emp_fel.direccion1 --zona 9               2a Avenida 3-91 Zona 9
   WHEN 2 LET fel_e.direccion_e = '     ', emp_fel.direccion1 --zona 17              Km. 7.5 Ruta al Atlantico, Z.17
   WHEN 3 LET fel_e.direccion_e = '         ', emp_fel.direccion1 --zona 11              21 Calle 5-07 Zona 11
   WHEN 5 LET fel_e.direccion_e = '', emp_fel.direccion1 -- cenma               51 C Final Villalobos 1 Galp�n 4 Local 20 Z 12
END CASE 
--LET fel_e.direccion_e      = emp_fel.direccion1; --tomarlo de la tabla empresas1.direccion1
LET fel_e.departamento_e   = emp_fel.depto; --tomarlo de la tabla empresas1.depto
LET fel_e.municipio_e      = emp_fel.municipio; --tomarlo de la tabla empresas1.municipio
LET fel_e.pais_e           = "GT" --dejarlo quemado
LET fel_e.codpos_e         = emp_fel.codigo_postal --tomarlo de la tabla empresas1.codigo_postal
LET fel_e.nit_r            = w_mae_tra.numnit;
LET fel_e.es_cui           = w_mae_tra.tipoid;
LET fel_e.nombre_r         = w_mae_tra.nomcli;
LET fel_e.idioma_r         = "ES";
LET fel_e.direccion_r      = w_mae_tra.dircli;
LET fel_e.departamento_r   = NULL;
LET fel_e.municipio_r      = NULL;
LET fel_e.pais_r           = "GT";
LET fel_e.codpos_r         = NULL;
LET fel_e.serie_e          = NULL; --este campo lo llena la interfaz fel
LET fel_e.numdoc_e         = NULL; --este campo lo llena la interfaz fel
LET fel_e.autorizacion     = NULL; --este campo lo llena la interfaz fel
LET fel_e.estatus          = "P"; --P=pendiente, la interface lo vuelve C=certificado si todo bien
LET fel_e.fac_id           = 0; --Llave primaria 
LET fel_e.p_descuento      = 0; --PONER CERO no manejan descuento detallado en la factura
LET fel_e.mto_descuento    = 0; -- PONER CERO no manejan descuento detallado en la factura
LET fel_e.fel_msg          = NULL; --Lo llena la interfaz de fel
LET fel_e.info_reg         = "PAGO_TRIMESTRAL";
LET fel_e.num_oc           = w_mae_tra.ordcmp; --Número de Orden de Compra
LET fel_e.un_pago_de       = NULL;
LET fel_e.npagos           = NULL;
LET fel_e.mto_pago         = NULL;
LET fel_e.dia_pago         = NULL;
LET fel_e.fecha_pago       = NULL;
LET fel_e.nom_firma        = NULL;
LET fel_e.dpi_firma        = NULL;
LET fel_e.vencod           = w_mae_tra.usrope --AMPLIAR CAMPO EN TBLA A 15
LET fel_e.num_interno      = w_mae_tra.nserie CLIPPED||"-"||w_mae_tra.numdoc; 

SET LOCK MODE TO WAIT

   WHENEVER ERROR CONTINUE
   INSERT INTO facturafel_e(
   serie,num_doc,tipod,fecha,fecha_em,fecha_anul,
   from,to,cc,formats,tipo_doc,estado_doc,ant_serie,ant_numdoc,
   ant_fecemi,ant_resoluc,c_moneda,tipo_cambio,num_int,nit_e,nombre_c,
   idioma_e,nombre_e,codigo_e,dispositivo_e,direccion_e,departamento_e,municipio_e,
   pais_e,codpos_e,nit_r,es_cui,nombre_r,idioma_r,direccion_r,departamento_r,
   municipio_r,pais_r,codpos_r,total_b,total_d,monto_d,total_i,ing_netosg,total_iva1,
   tipo1,base1,tasa1,monto1,total_neto,serie_e,numdoc_e,autorizacion,estatus,
   fac_id,p_descuento,mto_descuento,fel_msg,id_factura,total_en_letras,tipo_pago,
   num_oc,un_pago_de,npagos,mto_pago,dia_pago,fecha_pago,nom_firma,dpi_firma,
   vencod,num_interno,info_reg,es_exportacion,incoterm,nomconcomprador,dirconcomprador,otrreferencia, 
   es_exenta, sat_establecimiento)
   VALUES(
   fel_e.serie, fel_e.num_doc, fel_e.tipod, fel_e.fecha, fel_e.fecha_em, fel_e.fecha_anul, 
   fel_e.from, fel_e.to, 
   fel_e.cc, fel_e.formats, fel_e.tipo_doc, fel_e.estado_doc, fel_e.ant_serie,
   fel_e.ant_numdoc, fel_e.ant_fecemi, fel_e.ant_resoluc, fel_e.c_moneda,
   fel_e.tipo_cambio, fel_e.num_int, fel_e.nit_e, fel_e.nombre_c,
   fel_e.idioma_e, fel_e.nombre_e, fel_e.codigo_e, fel_e.dispositivo_e, fel_e.direccion_e,
   fel_e.departamento_e, fel_e.municipio_e, fel_e.pais_e, fel_e.codpos_e, fel_e.nit_r,
   fel_e.es_cui, fel_e.nombre_r, fel_e.idioma_r, fel_e.direccion_r, fel_e.departamento_r,
   fel_e.municipio_r, fel_e.pais_r, fel_e.codpos_r, fel_e.total_b, fel_e.total_d,
   fel_e.monto_d, fel_e.total_i, fel_e.ing_netosg, fel_e.total_iva1, fel_e.tipo1,
   fel_e.base1, fel_e.tasa1, fel_e.monto1, fel_e.total_neto, fel_e.serie_e, fel_e.numdoc_e,
   fel_e.autorizacion, fel_e.estatus, fel_e.fac_id, fel_e.p_descuento, fel_e.mto_descuento,
   fel_e.fel_msg, fel_e.id_factura, fel_e.total_en_letras, fel_e.tipo_pago, fel_e.num_oc,
   fel_e.un_pago_de, fel_e.npagos, fel_e.mto_pago, fel_e.dia_pago, fel_e.fecha_pago,
   fel_e.nom_firma, fel_e.dpi_firma, fel_e.vencod, fel_e.num_interno, fel_e.info_reg,
   fel_e.es_exportacion, fel_e.incoterm, fel_e.nomconcomprador, fel_e.dirconcomprador, fel_e.otrreferencia,
   fel_e.es_exenta, fel_e.sat_establecimiento)

   WHENEVER ERROR STOP

   IF sqlca.sqlcode < 0 THEN
      ERROR "Existió un error al insertar el encabezado de facura FEL"
      LET flag = TRUE
      RETURN flag
   ELSE 
      LET fel_e.fac_id = SQLCA.sqlerrd[2] --Registrando valor serial asignado en la insercion del registro
      LET gfac_id = fel_e.fac_id
      DISPLAY "============================Pase por graba_FEL ", gfac_id 
   END IF 

   IF NOT w_mae_tra.hayord THEN
      FOR i = 1 TO v_products.getLength() 
         IF v_products[i].cditem IS NULL OR 
            v_products[i].canuni IS NULL THEN 
               CONTINUE FOR 
         END IF
         INITIALIZE fel_ed.* TO NULL;

         LET fel_ed.fac_id = fel_e.fac_id;
         LET fel_ed.serie  = fel_e.serie;
         LET fel_ed.num_doc = fel_e.num_doc;
         LET fel_ed.tipod  = fel_e.tipod;
         LET fel_ed.fecha  = fel_e.fecha;
         LET fel_ed.descrip_p = v_products[i].dsitem CLIPPED;
         LET fel_ed.codigo_p  =  v_products[i].codpro;
         LET fel_ed.unidad_m  = v_products[i].nomuni;
         --LET fel_ed.cantidad  = v_products[i].canuni; --Ej.    10
         LET fel_ed.cantidad  = v_products[i].canori
         IF w_mae_tra.tipfac = "E" THEN
            LET fel_ed.precio_u  = v_products[i].predol;    --Ej.  6513.00
            LET fel_ed.precio_t  = v_products[i].prtdol;    --Ej. 65130.00
            LET fel_ed.precio_p  = v_products[i].prtdol;    --Ej.    42350.00
            LET fel_ed.precio_m  = v_products[i].prtdol;    --Ej.   42350.00
            LET fel_ed.total_iva = 0;
            LET fel_ed.total_imp = 0;
            LET fel_ed.ing_netos = v_products[i].prtdol; --Ej.    42350.00
            LET fel_ed.base      = v_products[i].prtdol - fel_ed.total_iva; --Ej.   37812.50
            LET fel_ed.tasa      = 0;
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50

         ELSE
            IF w_mae_tra.tipdoc = 4 THEN --factura exenta
               LET fel_ed.total_iva = 0
               LET fel_ed.total_imp = 0
               LET fel_ed.base      = v_products[i].totpro  
               LET fel_ed.monto     = fel_ed.total_iva;  
            ELSE
               LET fel_ed.total_iva = v_products[i].totpro - (v_products[i].totpro/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
               LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
               LET fel_ed.base      = v_products[i].totpro  - fel_ed.total_iva; --Ej.   37812.50
               LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            END IF 
            LET fel_ed.precio_u  = v_products[i].preuni;    --Ej.  6513.00
            LET fel_ed.precio_t  = v_products[i].totpro ;    --Ej. 65130.00
            LET fel_ed.precio_p  = v_products[i].totpro;    --Ej.    42350.00
            LET fel_ed.precio_m  = v_products[i].totpro;    --Ej.   42350.00
            
            
            LET fel_ed.ing_netos = v_products[i].totpro; --Ej.    42350.00
            
            LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            
         
         END IF
         
         LET fel_ed.descto_t  = 0;  --No se manejan descuentos Ej.    22780.00
         LET fel_ed.descto_m  = 0; --NO se manejan descuentos  Ej.    22780.00
         
         
         LET fel_ed.tipo      = "IVA";
         
         LET vcodcat = NULL;
         SELECT inv_products.codcat
         INTO vcodcat
         FROM inv_products
         WHERE inv_products.cditem = v_products[i].codpro
         
         IF vcodcat = 6 THEN --OTROS  (SERVICIOS)
            LET fel_ed.categoria  =   "SERVICIO" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         ELSE
            LET fel_ed.categoria  =   "BIEN" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         END IF
         
         IF v_products[i].deitemh IS NULL THEN
            LET fel_ed.txt_especial = "N";
         ELSE
            LET fel_ed.txt_especial = "S";
            LET fel_ed.txt_largo = v_products[i].deitemh;
         END IF
         
         SET LOCK MODE TO WAIT

         WHENEVER ERROR CONTINUE
         INSERT INTO facturafel_ed(
         fac_id,serie,num_doc,tipod,fecha,descrip_p,codigo_p,unidad_m,cantidad,precio_u,
         precio_t,descto_t,descto_m,precio_p,precio_m,total_imp,ing_netos,total_iva,
         tipo,base,tasa,monto,categoria,txt_especial,txt_largo)
         VALUES(      
         fel_ed.fac_id,fel_ed.serie,fel_ed.num_doc,fel_ed.tipod,fel_ed.fecha,fel_ed.descrip_p,
         fel_ed.codigo_p,fel_ed.unidad_m,fel_ed.cantidad,fel_ed.precio_u,fel_ed.precio_t,
         fel_ed.descto_t,fel_ed.descto_m,fel_ed.precio_p,fel_ed.precio_m,fel_ed.total_imp,
         fel_ed.ing_netos,fel_ed.total_iva,fel_ed.tipo,fel_ed.base,fel_ed.tasa,fel_ed.monto,
         fel_ed.categoria,fel_ed.txt_especial,fel_ed.txt_largo)

         WHENEVER ERROR STOP
         IF sqlca.sqlcode < 0 THEN
            ERROR "Existi� un problema al insertar el detalle de la factura FEL"
            LET flag = TRUE
            EXIT FOR
         END IF
      END FOR
   ELSE --hay orden 
      FOR i = 1 TO v_ordenes.getLength()
         #IF v_products[i].cditem IS NULL OR v_products[i].canuni IS NULL THEN 
         IF v_ordenes[i].nuitem1 IS NULL THEN 
               CONTINUE FOR 
         END IF
         INITIALIZE fel_ed.* TO NULL;

         LET fel_ed.fac_id = fel_e.fac_id;
         LET fel_ed.serie  = fel_e.serie;
         LET fel_ed.num_doc = fel_e.num_doc;
         LET fel_ed.tipod  = fel_e.tipod;
         LET fel_ed.fecha  = fel_e.fecha;
         
         #LET fel_ed.descrip_p = v_products[i].dsitem CLIPPED;
         SELECT a.nomsub INTO v_subcat FROM  inv_subcateg a WHERE a.subcat = v_ordenes[i].subcat1
         SELECT a.nomcol INTO v_color FROM  inv_colorpro a WHERE a.codcol = v_ordenes[i].codcol1

         #LET fel_ed.unidad_m  = v_products[i].nomuni;
         SELECT nommed INTO fel_ed.unidad_m FROM inv_unimedid WHERE unimed = v_ordenes[i].medida1 ;
         

         LET v_ordenes[i].descrp1 = v_subcat CLIPPED, " ", v_color CLIPPED, " DE ", v_ordenes[i].xlargo1, " POR ", v_ordenes[i].yancho1
         LET fel_ed.linea1 = v_subcat CLIPPED, " ", v_color CLIPPED
         LET fel_ed.linea2 = v_ordenes[i].xlargo1, " X ", v_ordenes[i].yancho1 CLIPPED, ' ', fel_ed.unidad_m

         #LET fel_ed.descrip_p = v_ordenes[i]. .descrp1 CLIPPED; 
         
         #LET fel_ed.codigo_p  =  v_products[i].codpro;
         LET fel_ed.codigo_p  =  "ESP01";
         
         
         #LET fel_ed.cantidad  = v_products[i].canori
         LET fel_ed.cantidad  = v_ordenes[i].cantid1
         
         IF w_mae_tra.tipfac = "E" THEN
            #LET fel_ed.precio_u  = v_products[i].predol;    --Ej.  6513.00
            LET fel_ed.precio_u  = v_ordenes[i].precio1;    --Ej.  6513.00
            
            #LET fel_ed.precio_t  = v_products[i].prtdol;    --Ej. 65130.00
            LET fel_ed.precio_t  = v_ordenes[i].precio1 * v_ordenes[i].cantid1 ;
            
            #LET fel_ed.precio_p  = v_products[i].prtdol;    --Ej.    42350.00
            LET fel_ed.precio_p  = v_ordenes[i].precio1;    --Ej.    42350.00
            
            #LET fel_ed.precio_m  = v_products[i].prtdol;    --Ej.   42350.00
            LET fel_ed.precio_m  = v_ordenes[i].precio1;    --Ej.   42350.00
            
            LET fel_ed.total_iva = 0;
            LET fel_ed.total_imp = 0;

            #LET fel_ed.ing_netos = v_products[i].prtdol; --Ej.    42350.00
            LET fel_ed.ing_netos = v_ordenes[i].precio1; --Ej.    42350.00
            
            #LET fel_ed.base      = v_products[i].prtdol - fel_ed.total_iva; --Ej.   37812.50

            LET fel_ed.base      = v_ordenes[i].precio1 * v_ordenes[i].cantid1 
            --LET fel_ed.base      = v_ordenes[i].precio1 - fel_ed.total_iva; --Ej.   37812.50
               
            
            LET fel_ed.tasa      = 0;
            
            #LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50

         ELSE
            #LET fel_ed.precio_u  = v_products[i].preuni;    --Ej.  6513.00
            LET fel_ed.precio_u  = v_ordenes[i].precio1;    --Ej.  6513.00
            
            #LET fel_ed.precio_t  = v_products[i].totpro;    --Ej. 65130.00
            LET lprecio_t = v_ordenes[i].precio1 * v_ordenes[i].cantid1
            LET fel_ed.precio_t  = lprecio_t;    --Ej. 65130.00
            
            #LET fel_ed.precio_p  = v_products[i].totpro;    --Ej.    42350.00
            LET fel_ed.precio_p  = lprecio_t;    --Ej.    42350.00
            
            #LET fel_ed.precio_m  = v_products[i].totpro;    --Ej.   42350.00
            LET fel_ed.precio_m  = lprecio_t;    --Ej.   42350.00
            
            #LET fel_ed.total_iva = v_products[i].totpro - (v_products[i].totpro/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
            LET fel_ed.total_iva = lprecio_t - (lprecio_t/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
            
            #LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
            LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
            
            #LET fel_ed.ing_netos = v_products[i].totpro; --Ej.    42350.00
            LET fel_ed.ing_netos = lprecio_t; --Ej.    42350.00
            
            #****LET fel_ed.base      = v_products[i].totpro - fel_ed.total_iva; --Ej.   37812.50
            LET fel_ed.base      = lprecio_t - fel_ed.total_iva; --Ej.   37812.50
            
            #LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            
            #LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
         
         END IF
         
         LET fel_ed.descto_t  = 0;  --No se manejan descuentos Ej.    22780.00
         LET fel_ed.descto_m  = 0; --NO se manejan descuentos  Ej.    22780.00
         
         
         LET fel_ed.tipo      = "IVA";
         
         {LET vcodcat = NULL;
         SELECT inv_products.codcat
         INTO vcodcat
         FROM inv_products
         WHERE inv_products.cditem = v_products[i].codpro
         
         IF vcodcat = 6 THEN --OTROS  (SERVICIOS)
            LET fel_ed.categoria  =   "SERVICIO" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         ELSE}
            LET fel_ed.categoria  =   "BIEN" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         --END IF

         LET fel_ed.txt_especial = "S";
         LET fel_ed.txt_largo = v_ordenes[i].descrp1
         {IF v_products[i].deitemh IS NULL THEN
            LET fel_ed.txt_especial = "N";
         ELSE
            LET fel_ed.txt_especial = "S";
            LET fel_ed.txt_largo = v_products[i].deitemh;
         END IF}
         
         SET LOCK MODE TO WAIT

         WHENEVER ERROR CONTINUE
         INSERT INTO facturafel_ed(
         fac_id,serie,num_doc,tipod,fecha,descrip_p,codigo_p,unidad_m,cantidad,precio_u,
         precio_t,descto_t,descto_m,precio_p,precio_m,total_imp,ing_netos,total_iva,
         tipo,base,tasa,monto,categoria,txt_especial,txt_largo, linea1, linea2)
         VALUES(      
         fel_ed.fac_id,fel_ed.serie,fel_ed.num_doc,fel_ed.tipod,fel_ed.fecha,fel_ed.descrip_p,
         fel_ed.codigo_p,fel_ed.unidad_m,fel_ed.cantidad,fel_ed.precio_u,fel_ed.precio_t,
         fel_ed.descto_t,fel_ed.descto_m,fel_ed.precio_p,fel_ed.precio_m,fel_ed.total_imp,
         fel_ed.ing_netos,fel_ed.total_iva,fel_ed.tipo,fel_ed.base,fel_ed.tasa,fel_ed.monto,
         fel_ed.categoria,fel_ed.txt_especial,fel_ed.txt_largo, fel_ed.linea1, fel_ed.linea2)

         WHENEVER ERROR STOP
         IF sqlca.sqlcode < 0 THEN
            ERROR "Existi� un problema al insertar el detalle de la factura FEL"
            LET flag = TRUE
            EXIT FOR
         END IF
      END FOR 
   END IF 

LABEL _lreturn:
RETURN flag 
END FUNCTION