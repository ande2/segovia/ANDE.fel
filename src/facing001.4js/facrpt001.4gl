{
Fecha    : Febrero 2011   
Programo : Mynor Ramirez 
Objetivo : Impresion de facturas.
}

-- Definicion de variables globales  

GLOBALS "facglb001.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECorD,
       existe       SMALLINT, 
       filename     CHAR(90),
       pipeline     CHAR(90),
       nfile   VARCHAR(20),
       npc     VARCHAR (50),
       nprin   VARCHAR(50),
       npath   VARCHAR(100),
       i            INT,
       runcmd  STRING 

-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt001_facturacion(pct)
 DEFINE pct DEC(9,6),
	opc SMALLINT 
   DEFINE diractual STRING 
   DEFINE result BOOLEAN

   INITIALIZE w_mae_fel.* TO NULL
    SELECT *
    INTO w_mae_fel.*
    FROM facturafel_e
    WHERE facturafel_e.num_int = w_mae_tra.lnktra
    AND facturafel_e.estado_doc = "ORIGINAL"
    ;
    DISPLAY "estado ", w_mae_tra.estado

    IF ande_desa = 1 THEN 
       LET opc = box_pregunta("Imprimir?")
       GOTO _case_opc 
    END IF      
    
    IF w_mae_fel.estatus = "P" OR w_mae_fel.estatus IS NULL THEN
       IF w_mae_tra.estado = 'V' THEN
          IF w_mae_tra.tipdoc = 2 THEN --1=Fac 2=Ticket
             LET opc = box_pregunta("Imprimir?") 
          ELSE    
            LET opc = 2
          END IF   
       ELSE
          -- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
          LET opc = box_pregunta("Imprimir?") 
       END IF
    ELSE
       -- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
       LET opc = box_pregunta("Imprimir?")   
    END IF

  LABEL _case_opc:  
  CASE (opc)
   WHEN 1 -- SI
    -- Definiendo archivo de impresion
    LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt001.txt"

    DISPLAY "FILENAME: ",filename

    -- Asignando valores
    --LET pipeline = "screen"
    LET pipeline = "local"   

    -- Seleccionando fonts para impresora epson
    CALL librut001_fontsprn(pipeline,"epson") 
    RETURNING fnt.* 

    LET diractual = FGL_GETENV("PWD")
    
    -- Iniciando reporte
    IF isGDC() THEN 
      START REPORT facrpt001_printfacfel TO filename
    ELSE 
       START REPORT facrpt001_printfacfel TO "tmp.txt"
    END IF   
     OUTPUT TO REPORT facrpt001_printfacfel(1,pct)
    FINISH REPORT facrpt001_printfacfel 
    IF NOT isGDC() THEN
       LET runcmd = "rm tmp.txt"
       RUN runcmd 
    END IF 
    WHENEVER ERROR STOP  
      
    --DISPLAY "num pos ", w_mae_tra.numpos
    -- Imprimiendo el reporte
    CALL librut001_enviareporte(filename, pipeline, "")
      
   WHEN 2 -- NO 
     CALL fgl_winmessage(" Atencion","No Se Imprimira . . .","information")
	
  END CASE

END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 
REPORT facrpt001_printfacfel(numero,pct) 
 DEFINE wcanletras  STRING,
        pct         DEC(9,6),
        wnommes     CHAR(10), 
        exis,i,j    SMALLINT, 
        totdet      SMALLINT, 
        numero,nl   SMALLINT,
        tarjeta     SMALLINT,
        wnomcol     CHAR(30), 
        wnomcla     CHAR(30), 
        lh,lv,si    CHAR(1),
        sd,ii,id    CHAR(1),
        x           CHAR(1),
        medida      VARCHAR(30),
        aniodos     CHAR(10),
        anio        CHAR(2),
		  atras       VARCHAR(20),
        vdesespecial VARCHAR(1000),
        arrdesespecial DYNAMIC ARRAY OF STRING -- CHAR(55) ; 
   DEFINE linea1, linea2 STRING 

        OUTPUT LEFT   MARGIN 1
         PAGE   LENGTH 51
         TOP    MARGIN 0
         BOTTOM MARGIN 3

   

 FORMAT 
   BEFORE GROUP OF numero 
      -- Imprimiendo encabezado
      IF w_mae_tra.tipfac = "L" THEN --local
         LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 
      ELSE --exportacion
         LET wcanletras = facing001_numtoletdol(w_mae_tra.tdodol) 
      END IF 
      
      LET wnommes    = librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)
      LET aniodos    = w_mae_tra.fecemi
      LET anio       = aniodos[9,10]
      LET atras      = ASCII 27,ASCII 25,ASCII 66 --"B"

      -- Inicializando impresora
      --IF w_mae_tra.numpos <> 1 THEN
         --SKIP 1 LINE 
         --PRINT fnt.ini CLIPPED,atras CLIPPED,fnt.t66 CLIPPED,fnt.nrm CLIPPED,fnt.cmp CLIPPED,fnt.srp CLIPPED
      --ELSE 
         --SKIP 2 LINES    
      --END IF    
	  
	 -- SKIP 1 LINE 
      PRINT fnt.ini CLIPPED, fnt.t66 CLIPPED, fnt.nrm CLIPPED, fnt.cmp CLIPPED --, fnt.srp CLIPPED
      IF w_mae_tra.numpos = 3 THEN --zona 11
         SKIP 1 LINES
      ELSE    
 --        SKIP 1 LINES
      END IF   
       
      -- Verificando formato 
      CASE (w_tip_doc.numfor)
        WHEN 1 OR 3 -- Facturas
           --nuevo sistema de impresion lpd/lpr
           --SKIP 4 LINES 
           --PRINT COLUMN  53,w_mae_fel.serie_e
           PRINT COLUMN  78,w_mae_fel.serie_e
           PRINT COLUMN  78,w_mae_fel.numdoc_e
           PRINT COLUMN 78, DAY(w_mae_tra.fecemi) USING "&&","-",wnommes CLIPPED,"-", YEAR(w_mae_tra.fecemi) USING "&&&&" 
           SKIP 1 LINE --numero de acceso        
           PRINT COLUMN 78, w_mae_fel.c_moneda CLIPPED
           PRINT COLUMN 78, w_mae_tra.lnktra USING "<<<<<<<<<"

           {IF w_mae_tra.numpos = 7 THEN --z9pos2
               PRINT COLUMN 22, w_mae_suc.dirsuc[1,37], COLUMN 78, w_mae_fel.num_interno CLIPPED 
           ELSE}  
               PRINT COLUMN 78, w_mae_fel.num_interno CLIPPED
           --END IF

           {IF w_mae_tra.numpos = 7 THEN
              IF LENGTH(w_mae_suc.dirsuc) > 37 THEN
                 IF w_mae_suc.dirsuc[38]=' ' THEN 
                    PRINT COLUMN 11, w_mae_suc.dirsuc[39,74]
                 ELSE    
                    PRINT COLUMN 11, w_mae_suc.dirsuc[38,75] 
                 END IF
                 --SKIP 1 LINE  
              ELSE
                 PRINT COLUMN 22, "Guatemala, Guatemala." 
                 SKIP 1 LINE    
              END IF  
           ELSE} 
           IF w_mae_tra.numpos = 1 OR 2 THEN --Serie B1
               SKIP 2 LINES
           ELSE
               SKIP 1 LINE 
           END IF  
           PRINT COLUMN  15,w_mae_tra.nomcli CLIPPED
           --SKIP 1 LINES 
           PRINT COLUMN  75,w_mae_tra.numnit CLIPPED
           --SKIP 1 LINE
           IF LENGTH(w_mae_tra.dircli) > 50 THEN
              PRINT COLUMN 23, w_mae_tra.dircli CLIPPED WORDWRAP RIGHT MARGIN 65
           ELSE
              PRINT COLUMN 23, w_mae_tra.dircli CLIPPED
              SKIP 1 LINE 
           END IF
           PRINT  COLUMN 23, w_mae_tra.telcli CLIPPED,
                  COLUMN 82, w_mae_tra.ordcmp CLIPPED
                 
           SKIP 3 LINES  

          WHEN 2 -- Tickets
             SKIP 1 LINES
             PRINT COLUMN  90,w_mae_tra.lnktra   USING "<<<<<<<<<"
             SKIP 1 LINES 
             PRINT COLUMN   6,w_mae_tra.nomcli[1,60],
                   COLUMN  70,"FECHA: ",w_mae_tra.fecemi CLIPPED
             PRINT COLUMN   6,w_mae_tra.dircli CLIPPED, " (T.", w_mae_tra.telcli CLIPPED, ")"
             IF NOT w_mae_tra.hayord THEN
                PRINT COLUMN   6, "Cantidad",
                      COLUMN  20, "Descripcion",
                      COLUMN  58, "Precio U.",
                      COLUMN  70, "Precio Total"
             ELSE
                SKIP 1 LINES
            END IF

      END CASE 

  ON EVERY ROW
   -- Imprimiendo detalle 
   -- Verificando formato 
   
   CASE (w_tip_doc.numfor)
     WHEN 1 -- Facturas
       IF NOT w_mae_tra.hayord THEN 
          LET totdet = 15 
          FOR i = 1 TO 7
             IF v_products[i].cditem IS NULL THEN
                CONTINUE FOR
             END IF 
             IF v_products[i].canuni <=1 THEN
                IF v_products[i].nomuni MATCHES "*S" THEN
                   LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
                END IF
             END IF
             LET totdet = (totdet-1) 
            {
             IF v_products[i].nomuni != "UNIDAD" THEN
                --Factura yardas
                PRINT COLUMN  5, fnt.cmp CLIPPED, " ",
                                 v_products[i].canuni USING "##,##&.&&" ,
                      COLUMN 24, v_products[i].nomuni CLIPPED, " ",
                                 v_products[i].dsitem[1,40] CLIPPED,
                      --COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                      COLUMN 86, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
             ELSE
                --Factura por tapacarga
                PRINT COLUMN  5, fnt.cmp CLIPPED,  " ",  
                                 v_products[i].canuni USING "##,##&.&&" ,
                      COLUMN 24, v_products[i].dsitem[1,40] CLIPPED ,
                      --COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                      COLUMN 86, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
             END IF
             }
             
             LET vdesespecial = NULL;
                          
             IF v_products[i].nomuni != "UNIDAD" THEN
                --Factura yardas
                PRINT COLUMN  5, fnt.cmp CLIPPED, " ",
                                 v_products[i].canuni USING "##,##&.&&" ;
                      
                IF v_products[i].deitemh IS NOT NULL THEN
                   LET vdesespecial = v_products[i].deitemh;
                        
                   --Para descripci�n larga del item (Cuando se ingresa manualmente)
                   --CALL lineasprt(v_products[i].deitemh,40) RETURNING arrdesespecial
                   LET arrdesespecial = convierte(vdesespecial,55)
                   PRINT COLUMN 24, arrdesespecial[1] CLIPPED    
                   --PRINT COLUMN 24, v_products[i].nomuni CLIPPED, " ",
                   --                 vdesespecial[1,40] CLIPPED; 
                   
                ELSE
                   PRINT COLUMN 24, v_products[i].nomuni CLIPPED, " ",
                                    v_products[i].dsitem[1,40] CLIPPED;
                   LET linea1 = v_products[i].nomuni CLIPPED, " ", v_products[i].dsitem[1,40] CLIPPED;                 
                   
                END IF
                --COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                IF w_mae_tra.tipfac = "L" THEN --local
                   IF w_mae_tra.numpos = 2 THEN  
                      PRINT COLUMN 80, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                   ELSE 
                      PRINT COLUMN 86, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                   END IF    
                ELSE --exportacion    
                   IF w_mae_tra.numpos = 2 THEN  
                      PRINT COLUMN 80, (v_products[i].prtdol*(1+(pct/100))) USING "###,##&.&&"
                   ELSE
                      PRINT COLUMN 86, (v_products[i].prtdol*(1+(pct/100))) USING "###,##&.&&" 
                   END IF  
                   
                END IF 
                --PRINT COLUMN 86, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
             ELSE
                --Factura por tapacarga
                PRINT COLUMN  5, fnt.cmp CLIPPED,  " ",  
                                 v_products[i].canuni USING "##,##&.&&" ;
                IF v_products[i].deitemh IS NOT NULL THEN
                   LET vdesespecial = v_products[i].deitemh;
                   --PRINT COLUMN 24, vdesespecial[1,55] CLIPPED ;
                     
                   --llena el arreglo vdesespecial con las lineas que va a imprimir
                   LET arrdesespecial = convierte(vdesespecial,55)
                   PRINT COLUMN 24, arrdesespecial[1] CLIPPED ;
                ELSE
                   PRINT COLUMN 24, v_products[i].dsitem[1,55] CLIPPED ;
                   LET linea1 = v_products[i].dsitem[1,55] CLIPPED ;
                END IF
                      --COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                IF w_mae_tra.tipfac = "L" THEN --local  
                   IF w_mae_tra.numpos = 2 THEN      
                      PRINT COLUMN 80, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                   ELSE    
                      PRINT COLUMN 86, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
                   END IF    
                ELSE --exportacion
                   IF w_mae_tra.numpos = 2 THEN      
                      PRINT COLUMN 80, (v_products[i].prtdol*(1+(pct/100))) USING "###,##&.&&"
                   ELSE 
                      PRINT COLUMN 86, (v_products[i].prtdol*(1+(pct/100))) USING "###,##&.&&"
                   END IF    
                END IF   
             END IF
             IF v_products[i].nomuni MATCHES "*S" THEN
                LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
             END IF
             LET totdet = (totdet-1)
             IF v_products[i].nomuni != "UNIDAD" THEN
               IF LENGTH(vdesespecial[41,95]) > 0 THEN
                  --PRINT COLUMN 24, vdesespecial[41,95] CLIPPED
                  PRINT COLUMN 24, arrdesespecial[2] CLIPPED ;
                  LET totdet = (totdet-1)
               END IF
               IF LENGTH(vdesespecial[96,150]) > 0 THEN
                  --PRINT COLUMN 24, vdesespecial[96,150] CLIPPED
                  PRINT COLUMN 24, arrdesespecial[3] CLIPPED ;
                  LET totdet = (totdet-1)
               END IF
               IF LENGTH(vdesespecial[151,205]) > 0 THEN
                  --PRINT COLUMN 24, vdesespecial[151,205] CLIPPED
                  PRINT COLUMN 24, arrdesespecial[4] CLIPPED ;
                  LET totdet = (totdet-1)
               END IF
             ELSE
               IF LENGTH(vdesespecial[56,110]) > 0 THEN
                  --PRINT COLUMN 24, vdesespecial[56,110] CLIPPED
                  PRINT COLUMN 24, arrdesespecial[2] CLIPPED 
                  LET totdet = (totdet-1)
               END IF
               IF LENGTH(vdesespecial[111,165]) > 0 THEN
                  PRINT COLUMN 24, arrdesespecial[3] CLIPPED 
                  --PRINT COLUMN 24, vdesespecial[111,165] CLIPPED
                  LET totdet = (totdet-1)
               END IF
               IF LENGTH(vdesespecial[166,220]) > 0 THEN
                  --PRINT COLUMN 24, vdesespecial[166,220] CLIPPED
                  PRINT COLUMN 24, arrdesespecial[4] CLIPPED 
                  LET totdet = (totdet-1)
               END IF
             END IF
             
             IF w_mae_tra.tipfac = "L" THEN --local
               PRINT COLUMN  24, fnt.cmp CLIPPED,
                               "PRECIO ",
                               (v_products[i].preuni*(1+(pct/100))) USING "Q<<,<<&.&&"," C/",v_products[i].nomuni
                               --fnt.nrm CLIPPED
               LET linea2 = "PRECIO ", (v_products[i].preuni*(1+(pct/100))) USING "Q<<,<<&.&&"," C/",v_products[i].nomuni
              ELSE                  
                  PRINT COLUMN 24, fnt.cmp CLIPPED,
                               "PRECIO ",
                               (v_products[i].predol*(1+(pct/100))) USING "$<<,<<&.&&"," C/",v_products[i].nomuni
                  LET linea2 = "PRECIO ", (v_products[i].predol*(1+(pct/100))) USING "$<<,<<&.&&"," C/",v_products[i].nomuni
              END IF
              SELECT fac_id INTO gfac_id FROM facturafel_e WHERE serie = w_mae_tra.nserie AND num_doc = w_mae_tra.numdoc AND tipo_doc = 'FACT' AND estado_doc = 'ORIGINAL'
              SELECT cditem INTO gcditem FROM fac_dtransac WHERE lnktra = w_mae_tra.lnktra AND codabr = v_products[i].cditem
              UPDATE facturafel_ed 
               SET facturafel_ed.linea1 = linea1,
                   facturafel_ed.linea2 = linea2
               WHERE facturafel_ed.fac_id = gfac_id
                 AND facturafel_ed.codigo_p = gcditem
            DISPLAY "================== actualice con fac_id ", gfac_id
            DISPLAY "linea1 ", linea1
            DISPLAY "linea2 ", linea2
            DISPLAY "codigo_p ", gcditem
         END FOR
         LET totdet = (totdet-1)
         PRINT COLUMN 20,"-------------- ULTIMA LINEA --------------" 

         SKIP 1 LINE 
         IF w_mae_tra.tipfac = "E" THEN --local
            PRINT COLUMN 24, "Tipo de Cambio ", w_mae_tra.tascam
         ELSE 
            SKIP 1 LINE 
         END IF    
         -- Saltando lineas extra
         
         IF (totdet>0) THEN
            IF w_mae_tra.numpos = 7 THEN --z9pos2
               LET totdet = totdet - 2 -- - 3
            ELSE 
               LET totdet = totdet - 3
            END IF 
            SKIP totdet LINES 
                DISPLAY "linea 209ccc"
         END IF 
      ELSE --Cuando hay orden
         LET nl = 14
         FOR i = 1 to 6 
            IF v_ordenes[i].nuitem1 IS NULL OR
               v_ordenes[i].subcat1 IS NULL THEN
               CONTINUE FOR
            END IF
            IF NOT v_ordenes[i].medida1 THEN
               LET medida = "MTS"
            ELSE
               LET medida = "PIES"
            END IF
            -- Obteniendo clase de producto
            LET wnomcla = NULL
            SELECT a.nomsub
            INTO  wnomcla 
            FROM  inv_subcateg a
            WHERE a.subcat = v_ordenes[i].subcat1 
      
            -- Obteniendo color de producto
            LET wnomcol = NULL
            SELECT a.nomcol
            INTO  wnomcol 
            FROM  inv_colorpro a
            WHERE a.codcol = v_ordenes[i].codcol1
      
            IF NOT v_ordenes[i].desman1 THEN
               -- Diferenciando saran de las lonas en otro material
               IF wnomcla <> "SARAN" CLIPPED THEN
                  PRINT COLUMN  10, --fnt.nrm CLIPPED,
                     fnt.cmp CLIPPED, " ",
                     v_ordenes[i].cantid1 USING "##&.&&",
                        COLUMN 24, --fnt.cmp CLIPPED, 
                        "LONAS ",wnomcla CLIPPED ," ",wnomcol CLIPPED --, fnt.nrm CLIPPED
                  PRINT COLUMN 24, v_ordenes[i].xlargo1 USING "<<&.&&", " X ", v_ordenes[i].yancho1 USING "<<&.&&",
                                   " ", medida, " ", "PRECIO U. ", v_ordenes[i].precio1 USING "Q<<,<<&.&&",
                        COLUMN 78, --fnt.nrm CLIPPED, 
                        (v_ordenes[i].precio1 * v_ordenes[i].cantid1) USING "##,##&.&&"  --CS , fnt.cmp CLIPPED 
               ELSE
                  PRINT COLUMN  10, --fnt.nrm CLIPPED, 
                    v_ordenes[i].cantid1 USING "##&.&&",
                        COLUMN 24, --fnt.cmp CLIPPED,
                          wnomcla CLIPPED," ",wnomcol CLIPPED --fnt.nrm CLIPPED
                  PRINT COLUMN 24, --fnt.cmp CLIPPED,
                     v_ordenes[i].xlargo1  USING "<<&.&&", " X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                 " ",medida," ","PRECIO U. ",v_ordenes[i].precio1 USING "Q<<,<<&.&&", --fnt.nrm CLIPPED,
                        COLUMN 78,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,###,##&.&&"
               END IF
               LET nl = nl - 2
            ELSE
               PRINT COLUMN  10, --fnt.nrm CLIPPED, 
                 v_ordenes[i].cantid1 USING "#&.&&",
                     COLUMN 24, "DESCRIPCION: "     
               IF LENGTH(v_ordenes[i].descrp1[01,20]) AND v_ordenes[i].descrp1 IS NOT NULL THEN 
                  LET nl = nl-1
                  PRINT COLUMN 24, --fnt.cmp CLIPPED,
                     v_ordenes[i].descrp1 WORDWRAP RIGHT MARGIN 60 --,
                                  --fnt.nrm CLIPPED
               END IF
            END IF
         END FOR
         PRINT COLUMN 24, --fnt.cmp CLIPPED, 
            "ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<,<<<" --, fnt.nrm CLIPPED 
         PRINT COLUMN 24,"-------------- ULTIMA LINEA --------------" 
         --LET nl = 3 
         
         IF (nl>0) THEN
            IF w_mae_tra.numpos = 7 THEN --z9pos2
               LET totdet = totdet - 1 -- - 3
            ELSE    
               LET nl = nl - 3
            END IF    
            SKIP nl LINES 
         END IF 
      END IF 

    WHEN 2 -- Tickets 
     IF NOT w_mae_tra.hayord THEN
        LET totdet =  8
        FOR i = 1 TO 8  
           IF v_products[i].cditem IS NULL THEN
              CONTINUE FOR
           END IF
           IF v_products[i].canuni <=1 THEN
              IF v_products[i].nomuni MATCHES "*S" THEN
                LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
              END IF
          END IF
          LET totdet = (totdet-1)

          IF v_products[i].nomuni != "UNIDAD" THEN
             LET v_products[i].dsitem=v_products[i].nomuni CLIPPED," ",v_products[i].dsitem CLIPPED
          END IF
          PRINT COLUMN   2,v_products[i].canuni       USING "##,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED, v_products[i].dsitem[1,40],
                           fnt.nrm CLIPPED,
                COLUMN  72,(v_products[i].preuni*(1+(pct/100))) USING "###,##&.&&",
                COLUMN  90,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
          LET totdet = (totdet-1)
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 15,"------------------ ULTIMA LINEA -------------------"
      -- Saltando lineas extra
      IF (totdet>0) THEN
         SKIP totdet LINES
DISPLAY "linea 369"         
      END IF
	ELSE
		  LET totdet = 8
		  FOR i = 1 TO 8
			 IF v_ordenes[i].cantid1 IS NULL AND
			  	 v_ordenes[i].subcat1 IS NULL AND
				 v_ordenes[i].codcol1 IS NULL THEN
				 LET totdet=totdet-1
				 CONTINUE FOR
			 END IF 
			 IF NOT v_ordenes[i].medida1 THEN
   		 	 LET medida = "METROS"
			 ELSE
   		 	 LET medida = "PIES"
			 END IF
			 -- Obteniendo clase de producto
			 LET wnomcla = NULL
			 SELECT a.nomsub
 			 INTO  wnomcla
 			 FROM  inv_subcateg a
 			 WHERE a.subcat = v_ordenes[i].subcat1
			
			 -- Obteniendo color de producto
			 LET wnomcol = NULL
			 SELECT a.nomcol
 			 INTO  wnomcol
 			 FROM  inv_colorpro a
 			 WHERE a.codcol = v_ordenes[i].codcol1

          IF NOT w_det_ord.desman THEN
             IF wnomcla <> "SARAN" CLIPPED THEN
                PRINT COLUMN  10, v_ordenes[i].cantid1         USING "##,###&.&&",
                      COLUMN  24, fnt.cmp CLIPPED,"LONAS ",wnomcla CLIPPED ," ",wnomcol CLIPPED," ",
                                v_ordenes[i].xlargo1  USING "<<&.&&", " X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                " ",medida,fnt.nrm CLIPPED,
                      COLUMN 72,v_ordenes[i].precio1 USING                       "###,##&.&&",
                      COLUMN 84,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
					 LET totdet=totdet-1
             ELSE
                PRINT COLUMN  10,v_ordenes[i].cantid1         USING "##.###&.&&",
                      COLUMN  24,fnt.cmp CLIPPED,wnomcla CLIPPED," ",wnomcol CLIPPED," ",
                                v_ordenes[i].xlargo1  USING "<<&.&&"," X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                " ",medida,fnt.nrm CLIPPED,
					 	    COLUMN 72,v_ordenes[i].precio1 USING "###,##&.&&",
                      COLUMN 84,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
					 LET totdet=totdet-1
            END IF
         ELSE
            PRINT COLUMN  10,v_ordenes[i].cantid1         USING "##,###&.&&",
                  COLUMN  24, --fnt.cmp CLIPPED,
                    "DESCRIPCION: ", v_ordenes[i].descrp1[1,60], --fnt.nrm CLIPPED,
						COLUMN  72,v_ordenes[i].precio1                      USING "###,##&.&&",
                  COLUMN 90,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
				LET totdet=totdet-1
            END IF
		END FOR
      --LET nl = 3
      PRINT COLUMN 16, --fnt.cmp CLIPPED, 
         "ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<,<<<" --,fnt.nrm CLIPPED
      IF (totdet>0) THEN
         IF w_mae_tra.numpos = 7 THEN --z9pos2
            LET totdet = totdet + 1
         ELSE 
            LET totdet = totdet - 2  
         END IF    
         SKIP totdet LINES
      END IF
      SKIP 1 LINES
     END IF

   END CASE 

  ON LAST ROW
   -- Verificando formato 
   CASE (w_tip_doc.numfor)
      WHEN 1 -- Facturas
        -- Imprimiendo final
        IF w_mae_tra.numpos = 7 THEN --z9pos2
           IF NOT w_mae_tra.hayord THEN
              --cs SKIP 3 LINES
              SKIP 1 LINES
           --cs ELSE 
              --cs SKIP 2 LINES 
           END IF 
        ELSE --resto del mundo
           IF NOT w_mae_tra.hayord THEN
              --cs SKIP 3 LINES
              SKIP 1 LINES
           ELSE 
              --cs SKIP 4 LINES
              SKIP 2 LINES
           END IF    
        END IF 

        PRINT COLUMN 30, "AGENTE DE RETENCION DEL IVA"
        SKIP 1 LINE
        
        IF w_mae_tra.tipfac = "L" THEN --local
            IF w_mae_tra.numpos = 2 THEN
               PRINT COLUMN  75,w_mae_tra.totdoc  USING "###,###,##&.&&"
            ELSE
               PRINT COLUMN  81,w_mae_tra.totdoc  USING "###,###,##&.&&"
            END IF 
            
        ELSE 
            IF w_mae_tra.numpos = 2 THEN
               PRINT COLUMN  75,w_mae_tra.tdodol  USING "$###,###,##&.&&"
            ELSE
               PRINT COLUMN  81,w_mae_tra.tdodol  USING "$###,###,##&.&&"
            END IF 
            
        END IF 
        SKIP 1 LINES 
        PRINT COLUMN  18,wcanletras CLIPPED
        IF w_mae_tra.numpos = 7 THEN --z9pos2
           --IF w_mae_tra.hayord THEN
              --SKIP 1 LINES
           --ELSE 
              SKIP 2 LINES
           --END IF    
        ELSE 
            IF w_mae_tra.hayord THEN
               SKIP 2 LINES
            ELSE 
               SKIP 2 LINES
            END IF
            --SKIP 2 LINES
        END IF 
        PRINT COLUMN 53, w_mae_fel.autorizacion CLIPPED
        
        SKIP 1 LINES
        PRINT COLUMN 60, w_mae_fel.fecha_em CLIPPED
        SKIP 2 LINES 
        PRINT COLUMN 20, w_mae_tra.usrope CLIPPED, 10 SPACES, TIME  

    WHEN 2 -- Ticket 
      -- SKIP 6 LINES 
	   PRINT COLUMN  65,"ABONO: ",w_mae_ord.totabo            USING "###,##&.&&"
	   PRINT COLUMN  65,"SALDO: ",w_mae_ord.salord            USING "###,##&.&&"
      PRINT COLUMN  65,"TOTAL: ",w_mae_tra.totdoc            USING "###,##&.&&"
      
   END CASE 
END REPORT 


FUNCTION facing001_numtoletdol(valor)
DEFINE f_tentxt ARRAY[8] OF RECORD
         tentxt         CHAR(10)
       END RECORD,
       f_unittxt ARRAY[20] OF RECORD
         unittxt        CHAR(11)
       END RECORD,
       f_gruptxt ARRAY[3] OF RECORD
         gruptxt        CHAR(7)
       END RECORD,
       f_letras,letras  CHAR(100),
       i,j,esp SMALLINT,
       f_cox            CHAR(20),
       f_deci           DECIMAL(3,0),
       valor            DECIMAL(11,2),
       f_numero, f_stp, f_divisor, f_grup, f_unit, f_ten, f_kk  INTEGER,
       f_deci2          CHAR(1)
LET f_tentxt[1].tentxt = "VEINTI"
LET f_tentxt[2].tentxt = "TREINTA "
LET f_tentxt[3].tentxt = "CUARENTA "
LET f_tentxt[4].tentxt = "CINCUENTA "
LET f_tentxt[5].tentxt = "SESENTA "
LET f_tentxt[6].tentxt = "SETENTA "
LET f_tentxt[7].tentxt = "OCHENTA "
LET f_tentxt[8].tentxt = "NOVENTA "

LET f_unittxt[1].unittxt = "UN "
LET f_unittxt[2].unittxt = "DOS "
LET f_unittxt[3].unittxt = "TRES "
LET f_unittxt[4].unittxt = "CUATRO "
LET f_unittxt[5].unittxt = "CINCO "
LET f_unittxt[6].unittxt = "SEIS "
LET f_unittxt[7].unittxt = "SIETE "
LET f_unittxt[8].unittxt = "OCHO "
LET f_unittxt[9].unittxt = "NUEVE "
LET f_unittxt[10].unittxt = "DIEZ "
LET f_unittxt[11].unittxt = "ONCE "
LET f_unittxt[12].unittxt = "DOCE "
LET f_unittxt[13].unittxt = "TRECE "
LET f_unittxt[14].unittxt = "CATORCE "
LET f_unittxt[15].unittxt = "QUINCE "
LET f_unittxt[16].unittxt = "DIECISEIS "
LET f_unittxt[17].unittxt = "DIECISIETE "
LET f_unittxt[18].unittxt = "DIECIOCHO "
LET f_unittxt[19].unittxt = "DIECINUEVE "
LET f_unittxt[20].unittxt = "VEINTE "

LET f_gruptxt[1].gruptxt = "MILLON "
LET f_gruptxt[2].gruptxt = "MIL "
LET f_gruptxt[3].gruptxt = " "


LET f_numero = valor
LET f_letras = NULL
LET f_stp = 1
LET f_divisor = 1000000
LET f_grup = 0
LET f_unit = 0
LET f_ten  = 0

WHILE f_stp <= 3

        LET f_grup = (f_numero / f_divisor)
        LET f_grup = f_grup * f_divisor
        LET f_numero = f_numero - f_grup
        LET f_grup = f_grup / f_divisor

        IF f_grup > 0
           THEN LET f_kk = f_grup
                LET f_ten = 0
           IF f_grup > 99
              THEN LET f_unit = (f_grup / 100)
                   LET f_grup = f_grup - (f_unit * 100)
                   LET f_cox = f_unittxt[f_unit].unittxt CLIPPED,"CIENTOS "
                   IF f_unit = 1
                      THEN IF f_grup = 0
                              THEN LET f_cox = "CIEN "
                              ELSE LET f_cox = "CIENTO "
                           END IF
                      END IF
                   IF f_unit = 5
                      THEN LET f_letras = f_letras CLIPPED," ","QUINIENTOS "
                      ELSE IF f_unit = 7
                              THEN 
                           LET f_letras = f_letras CLIPPED," ","SETECIENTOS "
                              ELSE IF f_unit = 9
                                      THEN
                           LET f_letras = f_letras CLIPPED," ","NOVECIENTOS "
                                      ELSE 
                           LET f_letras = f_letras CLIPPED," ",f_cox
                                   END IF
                           END IF
                   END IF
           END IF
        IF f_grup > 30
           THEN LET f_ten = (f_grup / 10)
                LET f_grup = f_grup - (f_ten * 10)
                LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                IF ((f_grup > 0) AND (f_letras IS NOT NULL))
                   THEN LET f_letras = f_letras CLIPPED," ","Y "
                END IF
           ELSE IF f_grup > 20
                 THEN LET f_ten = (f_grup / 10)
                      LET f_grup = f_grup - (f_ten * 10)
                      LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                END IF
         END IF
        IF f_grup > 0
           THEN IF f_ten = 2
           THEN LET f_letras = f_letras CLIPPED,f_unittxt[f_grup].unittxt
           ELSE LET f_letras = f_letras CLIPPED," ",f_unittxt[f_grup].unittxt
                END IF
        END IF
        LET f_letras = f_letras CLIPPED," ",f_gruptxt[f_stp].gruptxt
        IF ((f_stp = 1) AND (f_grup > 1))
           THEN LET f_letras = f_letras CLIPPED,"ES "
        END IF
        END IF

        LET f_stp = f_stp + 1
        LET f_divisor = f_divisor /1000

   END WHILE
    LET f_numero = valor
    LET f_deci = (valor - f_numero)*100
    IF valor >= 2 THEN 
       IF f_deci = 0 THEN 
          LET f_cox = " DOLARES EXACTOS"
       ELSE 
          LET f_cox = " DOLARES CON ",f_deci,"/100"
       END IF
       IF f_deci > 0 AND f_deci < 9 THEN 
          LET f_deci2 = f_deci
          LET f_cox = " DOLARES CON 0",f_deci2,"/100" 
       END IF 

       LET f_letras = "*",f_letras CLIPPED," ",f_cox CLIPPED," *"
    ELSE 
       IF valor < 1 THEN 
          IF f_deci > 1 THEN 
             LET f_letras = "* ",f_deci," CENTAVOS *"
          ELSE 
             LET f_letras = "* ",f_deci," CENTAVO *"
          END IF
       ELSE 
          IF f_deci = 0 THEN 
             LET f_cox = " EXACTO"
          ELSE 
             LET f_cox = " CON ",f_deci,"/100"
          END IF

          IF f_deci > 0 AND f_deci < 9 THEN 
             LET f_deci2 = f_deci
             LET f_cox = " CON 0",f_deci2,"/100" 
          END IF 

          LET f_letras="* ",f_letras CLIPPED," ",f_cox CLIPPED," *"

       END IF
    END IF

    LET esp  = 0
    LET j    = 1
    FOR i = 1 TO LENGTH(f_letras)
     IF (f_letras[i,i] = " ") THEN
        LET esp = (esp+1)
     ELSE
        LET esp = 0 
     END IF

     IF (esp<=1) THEN
        LET letras[j,j] = f_letras[i,i] 
        LET j = (j+1)
     END IF
    END FOR
    RETURN letras  
END FUNCTION
{
# Recibe como parametro un string de caracteres largo y devuelve un arreglo
# con tantas lineas como se necesite para contener el string recibido
# Recibe como parametro el lago de linea del arreglo que retorna
FUNCTION convierte(lvar,lg)
DEFINE lvar             STRING  	--string a dividir
DEFINE lg	            SMALLINT  --largo de linea
DEFINE larr 	         DYNAMIC ARRAY OF STRING  --array resultado
DEFINE ini,fin,i,j,lv 	SMALLINT

LET lv  = lvar.getlength()

--display "Largo del string ", lv

--Si es menor o igual al largo de linea
IF lv <= lg THEN  
   LET larr[1] = lvar CLIPPED  
   GOTO _salir
END IF  

--Mas de una linea
LET ini = 1
LET fin = lg
LET lv  = lvar.getlength()
LET i   = 1

WHILE TRUE 

  IF lvar.getCharAt(fin) = ' ' THEN  
     LET larr[i] = lvar.subString(ini,fin) --justifical(lvar.subString(ini,fin),lg)
  ELSE  
     FOR j = fin TO ini STEP - 1
       LET fin = fin - 1
       IF lvar.getCharAt(fin) = ' ' THEN
          
          LET larr[i] = lvar.subString(ini,fin) --justifical(lvar.subString(ini,fin),lg)
          EXIT FOR  
       END IF  
       
     END FOR   
  END IF  

  LET i = i + 1
  LET ini = fin + 1
  LET fin = ini + lg
  IF ini > lv THEN  
     EXIT WHILE 
  END IF   

END WHILE 

LABEL _salir:

WHILE TRUE 
  IF LENGTH(larr[larr.getLength( )]) = 0 THEN  
     CALL larr.deleteElement(larr.getLength( )) 
  ELSE  
     EXIT WHILE 
  END IF  
END WHILE 

RETURN larr

END FUNCTION  

# Recibe un string de caracteres y devuelve un segundo string justificado
# Recibe como parámetro el string a justificar y el largo de linea 
# Retorna el string justificado
FUNCTION justifical(str,ll)
DEFINE str      STRING 
DEFINE ll       SMALLINT 
DEFINE ts,i, ini, j, salir     SMALLINT 

  IF LENGTH(str) = ll THEN 
     GOTO _regresa
  END IF 

   LET ts = ll - LENGTH(str)
   LET j = 0
   WHILE ts > 0 
      LET ini = 1
      LET salir = 0
      LET j = j + 1
      WHILE TRUE  
         --buscar espacio
         FOR i = ini TO LENGTH(str)
            IF str.getCharAt(i) = " " THEN 
               LET str = str.subString(1,i), " ", str.subString(i+1,str.getLength())
               LET ts = ts - 1
               IF ts = 0 THEN LET salir = 1 END IF 
               LET ini = i + j + 1
               EXIT FOR 
            ELSE 
               IF i = LENGTH(str) THEN LET salir=1 END IF 
            END IF 
         END FOR 
         IF salir = 1 THEN EXIT WHILE END IF
   END WHILE 
 END WHILE   

  LABEL _regresa:

  RETURN str
END FUNCTION }
