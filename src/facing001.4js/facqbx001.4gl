{ 
Fecha    : Febrero 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Impresion de facturacion 
}

-- Definicion de variables globales 
GLOBALS "facglb001.4gl" 

-- Subrutina para consultar/anular/imprimir facturacion 

FUNCTION facqbx001_facturacion(operacion)
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        titmenu             CHAR(15),
        qryres,msg,titqry   CHAR(80),
        wherestado          STRING,
        opc                 INTEGER

      DEFINE vfac_id INTEGER,
     cmd STRING 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET titmenu     = " Consultar"
         LET wherestado  = NULL
  WHEN 2 LET titmenu     = " Anular" 
		IF FGL_GETENV("LOGNAME") = "ximena" OR
		   FGL_GETENV("LOGNAME") = "gusa" OR
		   FGL_GETENV("LOGNAME") = "ventasz11" OR
		   FGL_GETENV("LOGNAME") = "mruano" OR  
		   FGL_GETENV("LOGNAME") = "sistemas" THEN
         LET wherestado  = " AND a.estado in ('V') "
		ELSE
         LET wherestado  = " AND a.estado in ('V') AND a.feccor IS NULL"
		END IF
	WHEN 3 LET titmenu = " Eliminar"
		IF FGL_GETENV("LOGNAME") = "ximena" OR
		   FGL_GETENV("LOGNAME") = "gusa" OR
		   FGL_GETENV("LOGNAME") = "sistemas" THEN
         LET wherestado  = " AND a.estado in ('V') "
		END IF
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("labeld",0)
 CALL f.setElementHidden("compre",1)
  -- Menu de opciones
  LET opc = librut001_menugraba("B U S Q U E D A . . . ",
                                "Que desea Buscar?",
                                "Facturas",
                                "Ordenes",
                                "Cancelar",
                                "")

  -- Verificando opcion
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
   WHEN 1 -- Facturas
      -- Buscando datos 
      LET loop = TRUE 
      WHILE loop 
       -- Inicializando las variables 
       INITIALIZE qrytext,qrypart TO NULL
       LET int_flag = 0 
       CLEAR FORM

       -- Construyendo busqueda 
       CONSTRUCT BY NAME qrytext 
                      ON a.userid,
                         a.fecemi,
                         a.horsis,
                         a.estado, 
                         a.feccor, 
                         a.usrope,
                         a.lnktdc, 
                         a.nserie,  
                         a.numdoc,
                         f.serie_e,
                         f.numdoc_e,
                         f.autorizacion,
                         a.codcli,
                         a.tipoid, 
                         a.numnit,  
                         a.nomcli, 
                         a.dircli,
                         a.credit,
                         a.hayord, 
                         a.ordcmp, 
                         a.efecti,
                         a.cheque,
                         a.tarcre,
                         a.totdoc,
                         a.totiva 

        ON ACTION cancel
         -- Salida
         LET loop = FALSE
         EXIT CONSTRUCT
       END CONSTRUCT
       IF NOT loop THEN
          EXIT WHILE
       END IF 

       -- Preparando la busqueda 
       ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

       -- Creando la busqueda 
       LET qrypart = "SELECT a.lnktra ",
                      "FROM  fac_mtransac a, OUTER facturafel_e f ",
                      "WHERE a.lnktra = f.num_int AND ", qrytext CLIPPED, 
                       {" AND a.numpos = ",xnumpos,} 
                       wherestado CLIPPED, 
                       " ORDER BY 1 DESC" 
       DISPLAY "SELECT PRINCIPAL ==============> ", qrypart
       -- Declarando el cursor 
       PREPARE estqbe001 FROM qrypart
       DECLARE c_factur SCROLL CURSOR WITH HOLD FOR estqbe001  
       OPEN c_factur 
       FETCH FIRST c_factur INTO w_mae_tra.lnktra
       IF (status = NOTFOUND) THEN
          INITIALIZE w_mae_tra.* TO NULL
          ERROR ""
          CALL fgl_winmessage(
          "Atencion",
          "No existen documentos con el criterio seleccionado.",
          "stop") 
       ELSE
          ERROR "" 

          -- Desplegando datos 
          CALL facqbx001_datos()

          -- Fetchando documentos 
          MENU titmenu 
          
           BEFORE MENU 
            -- Verificando tipo de operacion 
				CALL DIALOG.setActionActive("ver_especial",FALSE)
            
            CASE (operacion)
             WHEN 1 HIDE OPTION "Anular"
            END CASE 
            
   ON ACTION FirmarFEL

      IF w_mae_fel.autorizacion IS NOT NULL THEN
         CALL box_valdato("La fatura ya fue firmada eletronicamente")
      ELSE
      
         SELECT a.fac_id
         INTO vfac_id
         FROM facturafel_e a
         WHERE a.num_int = w_mae_tra.lnktra
         AND tipo_doc = 'FACT' AND estado_doc = 'ORIGINAL'
         ERROR "Else -> ",  vfac_id
         LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
         DISPLAY "cmd: ",cmd
         RUN cmd
      END IF

   ON ACTION PDF
      IF w_mae_fel.autorizacion IS NULL THEN
         CALL box_valdato("La fatura no ha sido firmada eletronicamente")
      ELSE
         SELECT a.fac_id
         INTO vfac_id
         FROM facturafel_e a
         WHERE a.num_int = w_mae_tra.lnktra
         AND tipo_doc = 'FACT' AND estado_doc = 'ORIGINAL'
         --ERROR "Else -> ",  vfac_id
         LET cmd = "fglrun facpdf01.42r ",vfac_id USING "<<<<<<<"  --," segovia 0 0"
         DISPLAY "cmd: ",cmd
         RUN cmd
      END IF
      
 
   COMMAND "Siguiente"
            "Visualiza el siguiente documento en la lista."
            FETCH NEXT c_factur INTO w_mae_tra.lnktra 
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos siguientes en lista.", 
                "information")
                FETCH LAST c_factur INTO w_mae_tra.lnktra
            END IF 

            -- Desplegando datos 
            CALL facqbx001_datos()
				IF NOT w_mae_tra.hayord THEN
				   SELECT * FROM inv_ordentra
					 WHERE lnktra = w_mae_tra.lnktra
					IF STATUS = NOTFOUND THEN
						CALL DIALOG.setActionActive("ver_especial",FALSE)
					ELSE
						CALL DIALOG.setActionActive("ver_especial",TRUE) 
					END IF
				END IF

           COMMAND "Anterior"
            "Visualiza el documento anterior en la lista."
            FETCH PREVIOUS c_factur INTO w_mae_tra.lnktra
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos anteriores en lista.", 
                "information")
                FETCH FIRST c_factur INTO w_mae_tra.lnktra
             END IF

             -- Desplegando datos 
             CALL facqbx001_datos()
				IF NOT w_mae_tra.hayord THEN
				   SELECT * FROM inv_ordentra
					 WHERE lnktra = w_mae_tra.lnktra
					IF STATUS = NOTFOUND THEN
						CALL DIALOG.setActionActive("ver_especial",FALSE)
					ELSE
						CALL DIALOG.setActionActive("ver_especial",TRUE) 
					END IF
				END IF


				ON ACTION ver_especial
					CALL facing001_ver_especial()

           COMMAND "Primero" 
            "Visualiza el primer documento en la lista."
            FETCH FIRST c_factur INTO w_mae_tra.lnktra
             -- Desplegando datos 
             CALL facqbx001_datos()
				IF NOT w_mae_tra.hayord THEN
				   SELECT * FROM inv_ordentra
					 WHERE lnktra = w_mae_tra.lnktra
					IF STATUS = NOTFOUND THEN
						CALL DIALOG.setActionActive("ver_especial",FALSE)
					ELSE
						CALL DIALOG.setActionActive("ver_especial",TRUE) 
					END IF
				END IF

           COMMAND "Ultimo" 
            "Visualiza el ultimo documento en la lista."
            FETCH LAST c_factur INTO w_mae_tra.lnktra
             -- Desplegando datos
             CALL facqbx001_datos()
				IF NOT w_mae_tra.hayord THEN
				   SELECT * FROM inv_ordentra
					 WHERE lnktra = w_mae_tra.lnktra
					IF STATUS = NOTFOUND THEN
						CALL DIALOG.setActionActive("ver_especial",FALSE)
					ELSE
						CALL DIALOG.setActionActive("ver_especial",TRUE) 
					END IF
				END IF

            --
            COMMAND "delete"
               -- Eliminando documento 
                IF username = "ximena" THEN
                   IF facqbx001_delete() THEN
                      CONTINUE MENU 
                   END IF
                ELSE
                 CALL fgl_winmessage(
                   "Atencion",
                   "Usuario no Autorizado.",
                   "stop")                 
                END IF

               -- Desplegando datos
               CALL facqbx001_datos()
              
           COMMAND "Anular"
            "Permite anular el documento actual en pantalla."
            -- Verificando si documento ya fue anulado
            IF (w_mae_tra.estado="A") THEN
               CALL fgl_winmessage(
               "Atencion",
               "Documento ya fue anulado.",
               "stop")
               CONTINUE MENU 
            END IF 

            -- Anulando documento 
            IF facqbx001_anular() THEN
               CONTINUE MENU 
            END IF 

            -- Desplegando datos
            CALL facqbx001_datos()

           COMMAND "Productos"
            "Permite visualizar el detalle completo de productos del documento."
            -- Verificando si no hay orden
            IF NOT w_mae_tra.hayord THEN 
               CALL facing001_verproductos()
            ELSE
               ERROR "Atencion: productos no disponibles ..."
            END IF 

           COMMAND "Consultar" 
            "Regresa a la pantalla de seleccion."
            EXIT MENU

           COMMAND "Imprimir"
					SELECT * INTO w_tip_doc.* FROM fac_tipodocs WHERE tipdoc = w_mae_tra.tipdoc
					CALL facrpt001_facturacion(0)
					INITIALIZE w_tip_doc.* TO NULL

           ON ACTION cancel
            LET loop = FALSE
            EXIT MENU

           COMMAND KEY(F4,CONTROL-E)
            EXIT MENU
          END MENU
        END IF     
       CLOSE c_factur
      END WHILE
   WHEN 2 -- Ordenes
      -- Habilitando campos de orden de trabajo
       CALL f.setElementHidden("tabla1",1)
       CALL f.setElementHidden("group6",1)
       CALL f.setElementHidden("group4",0)
       CALL f.setElementHidden("group5",1)
       CALL f.setElementHidden("labelc",1)
      -- Buscando datos 
      LET loop = TRUE 
      WHILE loop 
       -- Inicializando las variables 
       INITIALIZE qrytext,qrypart TO NULL
       CALL librut003_cbxsubcateg()
       CALL librut003_cbxfasesot()
	    CALL librut003_cbxcolores()
       LET int_flag = 0 
       CLEAR FORM

       -- Construyendo busqueda 
       CONSTRUCT BY NAME qrytext 
                      ON a.userid,
                         a.fecemi,
                         a.horsis,
                         a.estado, 
                         a.feccor, 
                         a.usrope,
                         a.lnktdc, 
                         a.nserie,  
                         a.numdoc,
                         a.codcli, 
                         a.numnit,  
                         a.nomcli, 
                         a.dircli,
                         a.credit,
                         a.hayord, 
                         a.ordcmp, 
                         a.efecti,
                         a.cheque,
                         a.tarcre,
                         a.totdoc,
                         a.totiva,
                         b.lnkord,
								 b.fecord

        ON ACTION cancel
         -- Salida
         LET loop = FALSE
         EXIT CONSTRUCT
       END CONSTRUCT
       IF NOT loop THEN
          EXIT WHILE
       END IF 

       -- Preparando la busqueda 
       ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

       -- Creando la busqueda 
       LET qrypart = "SELECT a.lnktra ",
                      " FROM  fac_mtransac a , inv_morden b ",
                      " WHERE a.numpos = ",xnumpos,
                      " AND   a.lnktra = b.lnktra ", 
                       " AND ", qrytext CLIPPED,
                       wherestado CLIPPED, 
                       " ORDER BY 1 DESC" 
       
       -- Declarando el cursor 
       PREPARE estqbe002 FROM qrypart
       DECLARE c_factur2 SCROLL CURSOR WITH HOLD FOR estqbe002  
       OPEN c_factur2
       FETCH FIRST c_factur2 INTO w_mae_tra.lnktra
       IF (status = NOTFOUND) THEN
          INITIALIZE w_mae_tra.* TO NULL
          ERROR ""
          CALL fgl_winmessage(
          "Atencion",
          "No existen documentos con el criterio seleccionado.",
          "stop") 
       ELSE
          ERROR "" 

          -- Desplegando datos 
          CALL facqbx001_datos()

          -- Fetchando documentos 
          MENU titmenu 
           BEFORE MENU 
            -- Verificando tipo de operacion 
            CASE (operacion)
             WHEN 1 HIDE OPTION "Anular"
            END CASE 

           COMMAND "Siguiente"
            "Visualiza el siguiente documento en la lista."
            FETCH NEXT c_factur2 INTO w_mae_tra.lnktra 
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos siguientes en lista.", 
                "information")
                FETCH LAST c_factur2 INTO w_mae_tra.lnktra
            END IF 

            -- Desplegando datos 
            CALL facqbx001_datos()

           COMMAND "Anterior"
            "Visualiza el documento anterior en la lista."
            FETCH PREVIOUS c_factur2 INTO w_mae_tra.lnktra
             IF (status = NOTFOUND) THEN
                CALL fgl_winmessage(
                "Atencion",
                "No existen mas documentos anteriores en lista.", 
                "information")
                FETCH FIRST c_factur2 INTO w_mae_tra.lnktra
             END IF

             -- Desplegando datos 
             CALL facqbx001_datos()

           COMMAND "Primero" 
            "Visualiza el primer documento en la lista."
            FETCH FIRST c_factur2 INTO w_mae_tra.lnktra
             -- Desplegando datos 
             CALL facqbx001_datos()

           COMMAND "Ultimo" 
            "Visualiza el ultimo documento en la lista."
            FETCH LAST c_factur2 INTO w_mae_tra.lnktra
             -- Desplegando datos
             CALL facqbx001_datos()

           COMMAND "Anular"
            "Permite anular el documento actual en pantalla."
            -- Verificando si documento ya fue anulado
            IF (w_mae_tra.estado="A") THEN
               CALL fgl_winmessage(
               "Atencion",
               "Documento ya fue anulado.",
               "stop")
               CONTINUE MENU 
            END IF 

            -- Anulando documento 
            IF facqbx001_anular() THEN
               CONTINUE MENU 
            END IF 

            -- Desplegando datos
            CALL facqbx001_datos()

           COMMAND "Lista"
            "Permite visualizar el detalle completo de las lonas a la medida"
            -- Verificando si no hay orden
            IF w_mae_tra.hayord THEN 
               IF facing001_disp(0) THEN END IF
            ELSE
               ERROR "Atencion: productos no disponibles ..."
            END IF 

           COMMAND "Consultar" 
            "Regresa a la pantalla de seleccion."
            EXIT MENU

           COMMAND "Imprimir"
					SELECT * INTO w_tip_doc.* FROM fac_tipodocs WHERE tipdoc = w_mae_tra.tipdoc
					CALL facrpt001_facturacion(0)
					INITIALIZE w_tip_doc.* TO NULL

           ON ACTION cancel
            LET loop = FALSE
            EXIT MENU

           COMMAND KEY(F4,CONTROL-E)
            EXIT MENU
          END MENU
        END IF     
       CLOSE c_factur2
      END WHILE
  END CASE
 -- Escondiendo campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("labeld",1)

 -- Habilitando tabla de productos
 CALL f.setElementHidden("tabla1",0)
 CALL f.setElementHidden("group6",0)
 CALL f.setElementHidden("group4",1)
 CALL f.setElementHidden("group5",0)
 CALL f.setElementHidden("labelc",0)
 CALL f.setElementHidden("compre",1)

 -- Inicializando datos 
 CALL facing001_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del documento 

FUNCTION facqbx001_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos del documento 
 CALL librut003_bfacturacion(w_mae_tra.lnktra)
 RETURNING w_mae_tra.*,existe

 -- Desplegando datos del documento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecemi,
                 w_mae_tra.horsis,w_mae_tra.lnktdc, 
                 w_mae_tra.nserie,w_mae_tra.numdoc,
                 w_mae_tra.codcli,w_mae_tra.nomcli,
                 w_mae_tra.tipoid, 
                 w_mae_tra.numnit,w_mae_tra.dircli,
                 w_mae_tra.efecti,w_mae_tra.cheque,
                 w_mae_tra.tarcre,w_mae_tra.totdoc,
                 w_mae_tra.totiva,w_mae_tra.motanl,
                 w_mae_tra.credit,w_mae_tra.feccor,
                 w_mae_tra.hayord,w_mae_tra.ordcom,
                 w_mae_tra.depmon,w_mae_tra.totpag,
                 w_mae_tra.usrope,w_mae_tra.estado,
                 w_mae_tra.ordcmp,w_mae_tra.telcli,

                 w_mae_tra.tipfac,w_mae_tra.tascam,
                 w_mae_tra.maicli,w_mae_tra.incote,
                 w_mae_tra.nomcom, w_mae_tra.dircom,
                 w_mae_tra.otrref
                 
			IF w_mae_tra.obspre IS NOT NULL THEN
				CALL f.setElementHidden("labelg",0)
				CALL f.setFieldHidden("obspre",0)
				DISPLAY BY NAME w_mae_tra.obspre
			ELSE
				CALL f.setElementHidden("labelg",1)
				CALL f.setFieldHidden("obspre",1)
			END IF

--Datos de facturafel_e         
INITIALIZE w_mae_fel.* TO NULL 
SELECT MIN(facturafel_e.fac_id)
INTO w_mae_fel.fac_id
FROM facturafel_e
WHERE facturafel_e.num_int = w_mae_tra.lnktra

SELECT facturafel_e.*
INTO w_mae_fel.*
FROM facturafel_e
WHERE facturafel_e.fac_id = w_mae_fel.fac_id

--Estado de Anulación en SAT
SELECT a.estatus
INTO w_mae_fel.estatus
FROM facturafel_e a
WHERE a.num_int = w_mae_tra.lnktra
AND a.estado_doc = 'ANULADO'

DISPLAY BY NAME w_mae_fel.serie_e, w_mae_fel.numdoc_e, w_mae_fel.autorizacion, w_mae_fel.estatus

IF w_mae_tra.tipfac = "L" THEN -- ex portacion
   CALL f.setFieldHidden("preuni",0)
   CALL f.setFieldHidden("totpro",0)
   CALL f.setFieldHidden("totval",0)
   CALL f.setFieldHidden("predol",1)
   CALL f.setFieldHidden("prtdol",1)
   CALL f.setFieldHidden("totvaldol",1)
   CALL f.setElementHidden("group7",1)
ELSE
   CALL f.setFieldHidden("preuni",1)
   CALL f.setFieldHidden("totpro",1)
   CALL f.setFieldHidden("totval",1)
   CALL f.setFieldHidden("predol",0)
   CALL f.setFieldHidden("prtdol",0)
   CALL f.setFieldHidden("totvaldol",0)
   CALL f.setElementHidden("group7",0)
END IF
 -- Verificando si no hay ordenes 
 IF NOT w_mae_tra.hayord THEN
    -- Habilitando tabla de productos
    CALL f.setElementHidden("tabla1",0)
    CALL f.setElementHidden("group6",0)
    CALL f.setElementHidden("group4",1)
    CALL f.setElementHidden("group5",0)
    CALL f.setElementHidden("labelc",0)

    -- Seleccionando detalle del documento
    CALL facqbx001_detalle()
 ELSE
    -- Deshabilitando tabla de productos
    CALL f.setElementHidden("tabla1",1)
    CALL f.setElementHidden("group6",1)
    CALL f.setElementHidden("group4",0)
    CALL f.setElementHidden("group5",1)
    CALL f.setElementHidden("labelc",1)

    -- Seleccionando datos de la orden de trabajo
    CALL facqbx001_orden()
 END IF 
END FUNCTION 

-- Subrutina para seleccionar datos del detalle del documento 

FUNCTION facqbx001_detalle()
 DEFINE existe  SMALLINT

 -- Inicializando vector de productos
 CALL facing001_inivec()

 -- Seleccionando detalle del documento 
 DECLARE cdet CURSOR FOR
 SELECT x.cditem,
        x.codabr,
        y.dsitem,
        y.unimed,
        z.nommed, 
        x.canori,
        x.unimto,
        x.cantid,
        x.preuni,
        x.totpro,
        x.predol,
        x.prtdol,
        x.factor,
        x.correl,
        x.deitem
  FROM  fac_dtransac x, inv_products y, inv_unimedid z
  WHERE (x.lnktra = w_mae_tra.lnktra)
    AND (y.cditem = x.cditem)
    AND (z.unimed = y.unimed) 
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*

   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_products TO s_products.* ATTRIBUTE(BLUE)
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 
  DISPLAY v_products[1].deitemh TO deitem

  -- Desplegando totales
  CALL facing001_totdet() 
END FUNCTION 

-- Subrutina para desplegar os datos de la orden de trabajo

FUNCTION facqbx001_orden()
 -- Cargando combobox 
 CALL librut003_cbxcolores() 
 CALL librut003_cbxsubcateg()
 CALL librut003_cbxfasesot()  

 -- Seleccionando datos de la orden
 INITIALIZE w_mae_ord.* TO NULL
 SELECT a.*
  INTO  w_mae_ord.*
  FROM  inv_morden a
  WHERE (a.lnktra = w_mae_tra.lnktra)
  IF (status!=NOTFOUND) THEN
	  CALL facqbx001_det_ord()
     DISPLAY BY NAME w_mae_ord.lnkord,w_mae_ord.fecord,w_mae_ord.totord,
                     w_mae_ord.totabo,w_mae_ord.salord,w_mae_ord.fecofe,
                     w_mae_ord.fecent,w_mae_ord.doctos
  END IF 
END FUNCTION 

-- Subrutina para ver los productos completos del documento

FUNCTION facing001_verproductos()
DEFINE cnt, scr SMALLINT

 -- Desplegando productos
 DISPLAY ARRAY v_products TO s_products.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE)

  ON ACTION cancel 
   -- Salida
   EXIT DISPLAY 

  BEFORE ROW 
   LET cnt = arr_curr()
   LET scr = scr_line()
   DISPLAY v_products[cnt].deitemh TO deitem
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY 
END FUNCTION 

-- Subrutina para anular el documento

FUNCTION facqbx001_anular()
 DEFINE loop,regreso SMALLINT,
        msg          STRING,
        flag SMALLINT,
        flag_up0, flag_up1, flag_up2   SMALLINT,
        vfac_id INTEGER,
        vautorizacion STRING, 
        cmd, vsel STRING
 DEFINE reg_e RECORD
   estatus LIKE facturafel_e.estatus,
   serie_e LIKE facturafel_e.serie_e,
   numdoc_e LIKE facturafel_e.numdoc_e,
   autorizacion LIKE facturafel_e.autorizacion
 END RECORD 

 -- Ingresando motivo de la anulacion
 LET loop = TRUE
 WHILE loop
  LET regreso = FALSE
  INPUT BY NAME w_mae_tra.motanl  
   ATTRIBUTES (UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    LET loop    = FALSE
    LET w_mae_tra.motanl = NULL
    CLEAR motanl 
    EXIT INPUT 
   AFTER FIELD motanl
    -- Verificando motivo
    IF (LENGTH(w_mae_tra.motanl)<=0) THEN
       ERROR "Error: debe ingresarse el motivo de la anulacion, VERIFICA ..."
       NEXT FIELD motanl
    END IF 
  END INPUT 
  IF regreso THEN     
     EXIT WHILE
  END IF 

  -- Verificando anulacion
  IF NOT librut001_yesornot("Confirmacion",
                            "Desea Anular el Documento ?",
                            "Si",
                            "No",
                            "question") THEN

     LET w_mae_tra.motanl = NULL
     CLEAR motanl 
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Anulando documento 
  LET loop = FALSE 
  ERROR " Anulando Documento ..." ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  ###############################################################
     #Registro en tablas de FEL
      DISPLAY "Entrando a registrar tables FEL"
      CALL registra_tablas_FEL(2) RETURNING flag  --Anula factura electronica
      DISPLAY "Retorno flag-> ", flag 
      IF flag = TRUE THEN
         --ROLLBACK WORK
         EXIT WHILE
      END IF
     ###############################################################
  LET flag_up0 = NULL  LET flag_up1 = NULL  LET flag_up2 = NULL 
  DISPLAY "NULL up0 ", flag_up0, " up1 ", flag_up1, " up2 ", flag_up2 
  
  BEGIN WORK

   -- Marcando maestro del documento como anulado 
   SET LOCK MODE TO WAIT 
   DECLARE c_anul CURSOR FOR
   SELECT a.*
    FROM  fac_mtransac a
    WHERE (a.lnktra = w_mae_tra.lnktra) 
    FOR UPDATE 
    FOREACH c_anul 
     UPDATE fac_mtransac
     SET    fac_mtransac.estado = "A",
            fac_mtransac.motanl = w_mae_tra.motanl, 
            fac_mtransac.usranl = USER,
            fac_mtransac.fecanl = CURRENT,
            fac_mtransac.horanl = CURRENT
     WHERE CURRENT OF c_anul
     ###############################################################
     #Registro en tablas de FEL
{      CALL registra_tablas_FEL(2) RETURNING flag  --Anula factura electronica
      IF flag = TRUE THEN
         ROLLBACK WORK
         EXIT WHILE
      END IF}
     ###############################################################
        
    END FOREACH
    
   DISPLAY "Despues del foreach sqlcode ", sqlca.sqlcode  
   
	IF SQLCA.SQLCODE = 0 THEN
      LET flag_up0 = TRUE 
      DISPLAY "Flag up0 ", flag_up0
		IF w_mae_tra.hayord = 1 THEN
			UPDATE inv_morden SET inv_morden.estado="A"
		 	WHERE inv_morden.lnktra = w_mae_tra.lnktra
			IF SQLCA.SQLCODE = 0 THEN
   			--COMMIT WORK
            LET flag_up1 = TRUE 
            DISPLAY "Then - Despues del update inv_morden ", sqlca.sqlcode, " - ", flag_up1
				--CALL box_valdato("El documento se anulo exitosamente.")
			ELSE
            LET flag_up1 = FALSE
            DISPLAY "Else - Despues del update inv_morden ", sqlca.sqlcode, " - ", flag_up1 
				--ROLLBACK WORK
				--CALL box_valdato("No se pudo anular el documento.")
			END IF
      -- Finalizando Transaccion
		ELSE
   		--COMMIT WORK
         LET flag_up1 = TRUE 
			--CALL box_valdato("El documento se anulo exitosamente.")
         --Envia factura a firma
      END IF 
   ELSE
		--ROLLBACK WORK
		--CALL box_valdato("No se pudo anular el documento.")
      LET flag_up0 = FALSE
      DISPLAY "Else desoues del foreach flag_up0 ", flag_up0 
	END IF  

############### F E L ########################## 
   --Ticket
   IF w_mae_tra.tipdoc = 2 THEN
      LET flag_up2 = TRUE
      GOTO _saltafel 
   END IF   
   LET vfac_id = NULL;

     
   
   SELECT a.fac_id
   INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "FC"
   AND a.estado_doc = "ANULADO"

   --Cuando documento original no está certificado
   LET vautorizacion = NULL 
   SELECT autorizacion
   INTO vautorizacion
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "FC"
   AND a.estado_doc = "ORIGINAL" 
   IF vautorizacion IS NULL THEN
      LET flag_up2 = TRUE
      GOTO _saltafel
   END IF
   
   DISPLAY "Despues del select fac_id ", vfac_id
   LET vsel = 'SELECT a.fac_id
   INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = ', w_mae_tra.lnktra,
   'AND a.tipod = "FC"
   AND a.estado_doc = "ANULADO"'
   DISPLAY "select de vfacid ", vsel-- vfac_id
   LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
   DISPLAY "cmd: ",cmd
   --IF 
   RUN cmd

   SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
   INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "FC"
   AND a.estado_doc = "ANULADO"
   DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
   IF reg_e.estatus = "P" THEN
   -- Desplegando mensaje
      --CALL fgl_winmessage(" Atencion","El documento no pudo ser firmado electronicamente.","stop")
      LET flag_up2 = FALSE
      DISPLAY "Then - estatus=P link ", w_mae_tra.lnktra 
   ELSE
      --CALL box_valdato("El documento FEL fue anulado exitosamente")
      LET flag_up2 = TRUE 
      DISPLAY "Else - estatus=P serie, numero y autorizacion ", reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion 
   END IF
    --
   LABEL _saltafel:    
   IF flag_up0 = TRUE AND flag_up1 = TRUE AND flag_up2 THEN
      DISPLAY "Then - Commit work"
      COMMIT WORK 
      CALL box_valdato("El documento se anulo exitosamente.")
   ELSE 
      DISPLAY "Else - Rollback"
      ROLLBACK WORK 
      CALL box_valdato("No se pudo anular el documento.")
   END IF 

   CLOSE c_anul
   FREE  c_anul

   ERROR ""
 END WHILE 

 RETURN regreso 
END FUNCTION 

FUNCTION facqbx001_det_ord()
DEFINE i SMALLINT

	DECLARE detalle CURSOR FOR
		SELECT *
		  FROM inv_dorden
		 WHERE lnkord = w_mae_ord.lnkord
	LET i = 1
	FOREACH detalle INTO v_ordenes[i].*
		DISPLAY v_ordenes[i].subcat1," _ ",v_ordenes[i].codcol1
		LET i = i + 1
	END FOREACH

	FOR i = 1 TO v_ordenes.getLength()
		IF v_ordenes[i].lnkord1 IS NULL OR v_ordenes[i].lnkord1 <= 0 THEN
			CALL v_ordenes.deleteElement(i)
		END IF	
	END FOR

          CALL librut002_combobox("subcat1","SELECT a.subcat,a.nomsub FROM inv_subcateg a "||
                                  " WHERE a.lonmed = 1 ORDER BY 2 ")
          -- Combo de las colores
          CALL librut002_combobox("codcol1","SELECT a.codcol,a.nomcol FROM inv_colorpro a "||
                                  " ORDER BY 2")
          -- Combo de las fases
          CALL librut002_combobox("nofase1","SELECT a.tippar,a.valchr FROM glb_paramtrs a "||
                                  " WHERE a.numpar = 100 ORDER BY 1 ")

	IF facing001_disp(1) THEN
	END IF
	LET w_det_ord.* = v_ordenes[1].*
END FUNCTION
FUNCTION facqbx001_desp(pos)
DEFINE pos INTEGER

   LET w_det_ord.* = v_ordenes[pos].*
	LET tot = w_det_ord.precio*w_det_ord.cantid
   DISPLAY BY NAME w_det_ord.nuitem,w_det_ord.subcat,w_det_ord.codcol,
                   w_det_ord.cantid,w_det_ord.xlargo,w_det_ord.yancho,
                   w_det_ord.descrp,w_det_ord.observ,w_det_ord.precio,
                   w_det_ord.compre,w_det_ord.desman,w_det_ord.sinord,
                   w_det_ord.ordext,w_det_ord.medida,w_det_ord.nofase,
						 tot

END FUNCTION

FUNCTION facqbx001_delete()
DEFINE lnktrainv  LIKE inv_mtransac.lnktra
DEFINE lnktraord  LIKE inv_morden.lnkord
DEFINE err_msg    STRING 

   -- Verificando eliminacion
   IF NOT librut001_yesornot("Confirmacion",
                            "Desea Eliminar el Documento ?",
                            "Si",
                            "No",
                            "question") THEN
      RETURN TRUE 
   END IF 

   -- Eliminando documento 
   ERROR " Eliminando Documento ..." ATTRIBUTE(CYAN)

   -- Iniciando Transaccion
   BEGIN WORK

   SET LOCK MODE TO WAIT 
   DECLARE c_delete CURSOR FOR
   SELECT a.*
   FROM  fac_mtransac a
   WHERE (a.lnktra = w_mae_tra.lnktra) 
   FOR UPDATE 
   FOREACH c_delete
      --Buscar Transaccion de Inventario
      LET lnktrainv = 0
      SELECT nvl(lnktra,0)
      INTO   lnktrainv
		FROM   inv_mtransac
		WHERE  numrf1 = cast(w_mae_tra.lnktra as varchar(20))
      --Si hay transacciones de inventario
      IF lnktrainv > 0 THEN 
         --Eliminar Detalle de Transacciones Inventario
         WHENEVER ERROR CONTINUE
         --
         DELETE FROM inv_dtransac
         WHERE lnktra = lnktrainv
         --
         WHENEVER ERROR STOP
         IF SQLCA.SQLCODE < 0 THEN
            LET err_msg = err_get(SQLCA.SQLCODE)
            ROLLBACK WORK
            ERROR err_msg
            CALL errorlog(err_msg CLIPPED)
            CALL box_valdato("No se pudo eliminar detalle de Inventario.")
            RETURN TRUE 
         END IF
         --Eliminar Encabezado de Transacciones Inventario
         WHENEVER ERROR CONTINUE
         --
         DELETE FROM inv_mtransac
         WHERE lnktra = lnktrainv
         --
         WHENEVER ERROR STOP
         IF SQLCA.SQLCODE < 0 THEN
            LET err_msg = err_get(SQLCA.SQLCODE)
            ROLLBACK WORK
            ERROR err_msg
            CALL errorlog(err_msg CLIPPED)
            CALL box_valdato("No se pudo eliminar transaccion de Inventario.")
            RETURN TRUE 
         END IF
      END IF
      --Eliminar Orden
      IF w_mae_tra.hayord = 1 THEN
         --Buscar Orden
         LET lnktraord = 0
         SELECT nvl(lnkord,0)
         INTO   lnktraord
         FROM   inv_morden
         WHERE  lnktra = w_mae_tra.lnktra
         --Eliminar Detalle de Orden
         WHENEVER ERROR CONTINUE
         --
         DELETE FROM inv_dorden
         WHERE lnkord = lnktraord
         --
         WHENEVER ERROR STOP
         IF SQLCA.SQLCODE < 0 THEN
            LET err_msg = err_get(SQLCA.SQLCODE)
            ROLLBACK WORK
            ERROR err_msg
            CALL errorlog(err_msg CLIPPED)
            CALL box_valdato("No se pudo eliminar detalle de Inventario.")
            RETURN TRUE 
         END IF
         --Eliminar Encabezado de Orden
         WHENEVER ERROR CONTINUE
         --
         DELETE FROM inv_morden
         WHERE lnktra = w_mae_tra.lnktra
         --
         WHENEVER ERROR STOP
         IF SQLCA.SQLCODE < 0 THEN
            LET err_msg = err_get(SQLCA.SQLCODE)
            ROLLBACK WORK
            ERROR err_msg
            CALL errorlog(err_msg CLIPPED)
            CALL box_valdato("No se pudo eliminar Orden.")
            RETURN TRUE 
         END IF
      END IF 
      --Eliminar Detalle de Factura
      WHENEVER ERROR CONTINUE
      --
      DELETE FROM fac_dtransac
      WHERE lnktra = w_mae_tra.lnktra
      --
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         LET err_msg = err_get(SQLCA.SQLCODE)
         ROLLBACK WORK
         ERROR err_msg
         CALL errorlog(err_msg CLIPPED)
         CALL box_valdato("No se pudo eliminar detalle de Factura.")
         RETURN TRUE
      END IF 
      --Eliminar Encabezado de Factura
      WHENEVER ERROR CONTINUE
      --
      DELETE FROM fac_mtransac
      WHERE CURRENT OF c_delete
      --
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         LET err_msg = err_get(SQLCA.SQLCODE)
         ROLLBACK WORK
         ERROR err_msg
         CALL errorlog(err_msg CLIPPED)
         CALL box_valdato("No se pudo eliminar encabezado de Factura.")
         RETURN TRUE
      END IF 
   END FOREACH

   COMMIT WORK
   CALL box_valdato("El documento se anulo exitosamente.")
   
   CLOSE c_delete
   FREE  c_delete

   ERROR ""
   RETURN FALSE  
END FUNCTION 
