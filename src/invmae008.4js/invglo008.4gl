{ 
invglo008.4gl
Mynor Ramirez
Mantenimiento de sucursales
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE glb_sucsxemp.*,
       v_sucsxemp  DYNAMIC ARRAY OF RECORD
        tcodemp    LIKE glb_sucsxemp.codemp,
        tnomemp    LIKE glb_empresas.nomemp, 
        tcodsuc    LIKE glb_sucsxemp.codsuc,
        tnomsuc    LIKE glb_sucsxemp.nomsuc, 
        tnumnit    LIKE glb_sucsxemp.numnit 
       END RECORD
END GLOBALS
