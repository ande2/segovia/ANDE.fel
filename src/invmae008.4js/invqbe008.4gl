{
invqbe008.4gl 
Mynor Ramirez
Mantenimiento de sucursales 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "invglo008.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe008_sucursales(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo  = "Borrar" 
  END IF       

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae008_inival(1)
   CALL librut003_cbxempresas() 
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.codsuc,a.nomsuc,
                                a.nomabr,a.numnit,a.numtel,
                                a.numfax,a.dirsuc,a.userid,
                                a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel  
     -- Salida
     CALL invmae008_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,b.nomemp,a.codsuc,a.nomsuc,a.numnit ",
                 " FROM glb_sucsxemp a,glb_empresas b ",
                 " WHERE b.codemp = a.codemp AND ",qrytext CLIPPED,
                 " ORDER BY 2,3,4 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_sucursales SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_sucsxemp.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_sucursales INTO v_sucsxemp[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_sucursales
   FREE  c_sucursales
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_sucsxemp TO s_sucursales.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL invmae008_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- buscar
      CALL invmae008_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae008_sucursales(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe008_datos(v_sucsxemp[ARR_CURR()].tcodsuc)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
       IF invmae008_sucursales(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe008_datos(v_sucsxemp[ARR_CURR()].tcodsuc)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = invqbe008_integridad() 
      IF (res>0) THEN
         -- Clasificando error
         CASE (res)
          WHEN 1 LET msg = " Esta sucursal ya tiene movimientos registrados, no puede borrarse."
          WHEN 2 LET msg = " Esta sucursal ya tiene bodegas registrados, no puede borrarse." 
         END CASE

         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta sucursal ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae008_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Sucursal"
      LET arrcols[3] = "Nombre Sucursal" 
      LET arrcols[4] = "Nombree Anreviado" 
      LET arrcols[5] = "Numero de NIT" 
      LET arrcols[6] = "Numero Telefono" 
      LET arrcols[7] = "Numero FAX" 
      LET arrcols[8] = "Direccion" 
      LET arrcols[9] = "Usuario Registro"
      LET arrcols[10]= "Fecha Registro"
      LET arrcols[11]= "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT b.nomemp,a.codsuc,a.nomsuc,a.nomabr,a.numnit,a.numtel,a.numfax,a.dirsuc,",
                              "a.userid,a.fecsis,a.horsis ", 
                       " FROM glb_sucsxemp a,glb_empresas b ",
                       " WHERE b.codemp = a.codemp AND ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Sucursales.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 
      
     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe008_datos(v_sucsxemp[1].tcodsuc)
      ELSE
         CALL invqbe008_datos(v_sucsxemp[ARR_CURR()].tcodsuc)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen sucursales con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe008_datos(wcodsuc)
 DEFINE wcodsuc LIKE glb_sucsxemp.codsuc,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_sucsxemp a "||
              "WHERE a.codsuc = "||wcodsuc||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_sucursalest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_sucursalest INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomsuc THRU w_mae_pro.dirsuc 
  DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codsuc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_sucursalest
 FREE  c_sucursalest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomsuc THRU w_mae_pro.dirsuc 
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codsuc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si la sucursal ya tiene algun movimiento registrado

FUNCTION invqbe008_integridad()
 DEFINE conteo INTEGER

 -- Verificando movimientos
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.codsuc = w_mae_pro.codsuc)
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando bodegas 
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_mbodegas a 
      WHERE (a.codsuc = w_mae_pro.codsuc)
      IF (conteo>0) THEN
         RETURN 2
      ELSE
         RETURN 0
      END IF
  END IF
END FUNCTION 
