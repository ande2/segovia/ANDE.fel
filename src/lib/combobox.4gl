{
ADM
Programa : combobox.4gl 
Fecha    : Julio 2010
Programo : Mynor Ramirez
Objetivo : Subruitinas para crear combobox 
}

DATABASE cds 

DEFINE qrytext STRING 


{ Subrutina que carga el combobox de las cuidades }

FUNCTION combobox_cities()
 -- Llenando combobox de cuidades
 LET qrytext = "SELECT a.cityid,a.namecity ",
               " FROM city a ",
               " ORDER BY 2 "

 CALL librut02_combobox("cityid",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los paises }

FUNCTION combobox_countrys()
 -- Llenando combobox de paises 
 LET qrytext = "SELECT a.countryid,a.namec ",
               " FROM country a ",
               " ORDER BY 2 "

 CALL librut02_combobox("countryid",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de tipos de reportes }

FUNCTION combobox_reportparams()
 -- Llenando combobox de paises 
 LET qrytext = "SELECT a.reportparamid,a.desc ",
               " FROM reportparam a ",
               " ORDER BY 1 "

 CALL librut02_combobox("reportparamid",qrytext)
END FUNCTION
