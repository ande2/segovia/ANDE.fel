{
librut002.4gl
Mynor Ramirez
Subrutinas de libreria 
} 

DATABASE segovia 

-- Subrutina excelreport
-- Parametros  :  qry      << string de busqueda
--                cols     << numero de columnas en excel 
--                im       << modo 0: interactivo, 1: no interactivo
--                lbls     << emcabezados de columnas
-- Regresa                 >> Number of lines processed
--
-- Comentarios : Envia un reporte a una hoja de excel           

FUNCTION librut002_excelreport(title1,qry,cols,im,lbls)
 -- Etiquetas
 CONSTANT resWindow = "Reporte Generado"
 CONSTANT resLabel  = "Total de registros: "
 CONSTANT resButton = "Aceptar"
 CONSTANT libro     = "Libro1"
 CONSTANT s         = "\\t"

 -- Variables
 DEFINE qry,ln,cnt    STRING
 DEFINE i,cols,im,x   SMALLINT
 DEFINE tmp,tmp2,tmp3 VARCHAR(10)
 DEFINE excelfile     STRING
 DEFINE title1        STRING
 DEFINE total         INTEGER
 DEFINE pb,err_flag   SMALLINT
 DEFINE pp            DEC(14,6)
 DEFINE w             ui.Window
 DEFINE f             ui.Form
 DEFINE lbls          DYNAMIC ARRAY OF VARCHAR(255)
 DEFINE rec           RECORD
         val1         VARCHAR(255),
         val2         VARCHAR(255),
         val3         VARCHAR(255),
         val4         VARCHAR(255),
         val5         VARCHAR(255),
         val6         VARCHAR(255),
         val7         VARCHAR(255),
         val8         VARCHAR(255),
         val9         VARCHAR(255),
         val10        VARCHAR(255),
         val11        VARCHAR(255),
         val12        VARCHAR(255),
         val13        VARCHAR(255),
         val14        VARCHAR(255),
         val15        VARCHAR(255),
         val16        VARCHAR(255),
         val17        VARCHAR(255),
         val18        VARCHAR(255),
         val19        VARCHAR(255),
         val20        VARCHAR(255)
        END RECORD

 -- Verificando si el modo es interactivo
 IF im THEN
    -- Abriendo ventana para mostrar avance
    OPEN WINDOW progressBar AT 1,1 
     WITH FORM "progrebar" ATTRIBUTES(TEXT=title1)

     -- Obteniendo datos de la ventana
     LET w = ui.Window.getcurrent()
     LET f = w.getForm()

     -- Desplegando mensaje
     CALL f.setElementText("formonly.mensaje","Generando Reporte a EXCEL") 

     -- Inicializando venana de progreso 
     CALL librut002_inicioprogreso()
     LET total = 1

     -- Contando registros del query 
     PREPARE prpPb FROM qry
     DECLARE curPb CURSOR FOR prpPb
     FOREACH curPb INTO rec.*
      LET total = total+1
     END FOREACH
     LET total = total-1
 END IF

 -- Abriendo excel
 CALL ui.Interface.frontCall("standard","shellexec",["excel"],err_flag)
 IF (err_flag=0) THEN
    DISPLAY "Excel no puede ser abierto." 
 ELSE
    -- Excel abierto con el documento
    -- Abriendo coneccion 
    IF NOT DDEConnect("EXCEL",libro) THEN
       DISPLAY "Excel no puede abrir conexion."
    END IF
   
    -- enviando encabezados
    LET i = 1
    IF (LENGTH(lbls[1])>0) THEN 
     FOR x = 1 TO cols
      LET tmp = x

      -- Para Excel >= 2000, Sintaxis ... L1C1:L1C1
      LET ln = "L1C",tmp CLIPPED,":L1C",tmp CLIPPED
      IF NOT DDEPoke("EXCEL",libro,ln,lbls[x]) THEN
         CALL librut002_dsperror("DDEPoke")
      END IF

      -- Para Excel 97, Sintaxis ... F1C1:F1C1
      LET ln = "F1C",tmp CLIPPED,":F1C",tmp CLIPPED
      IF NOT DDEPoke("EXCEL",libro,ln,lbls[x]) THEN
         CALL librut002_dsperror("DDEPoke")
      END IF
     END FOR

     LET i =i+1
    END IF
      
    -- Preparando query
    PREPARE prp FROM qry
    DECLARE cur CURSOR FOR prp
    LET tmp2 = cols
    INITIALIZE rec.* TO NULL
    FOREACH cur INTO rec.*
     -- Enviando datos
     CASE cols
       WHEN  1 LET cnt = rec.val1 CLIPPED
       WHEN  2 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED
       WHEN  3 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED 
       WHEN  4 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED
       WHEN  5 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED, 
                                          s,rec.val5  CLIPPED
       WHEN  6 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED
       WHEN  7 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED
       WHEN  8 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED
       WHEN  9 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED,
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED
       WHEN 10 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED, 
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED
       WHEN 11 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED
       WHEN 12 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED
       WHEN 13 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED
       WHEN 14 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED
       WHEN 15 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED 
       WHEN 16 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED,s,rec.val16 CLIPPED
       WHEN 17 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED,s,rec.val16 CLIPPED,
                                          s,rec.val17 CLIPPED
       WHEN 18 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED,s,rec.val16 CLIPPED,
                                          s,rec.val17 CLIPPED,s,rec.val18 CLIPPED 
       WHEN 19 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED,s,rec.val16 CLIPPED,
                                          s,rec.val17 CLIPPED,s,rec.val18 CLIPPED,s,rec.val19 CLIPPED 
       WHEN 20 LET cnt = rec.val1 CLIPPED,s,rec.val2  CLIPPED,s,rec.val3  CLIPPED,s,rec.val4  CLIPPED,
                                          s,rec.val5  CLIPPED,s,rec.val6  CLIPPED,s,rec.val7  CLIPPED, 
                                          s,rec.val8  CLIPPED,s,rec.val9  CLIPPED,s,rec.val10 CLIPPED,
                                          s,rec.val11 CLIPPED,s,rec.val12 CLIPPED,s,rec.val13 CLIPPED,
                                          s,rec.val14 CLIPPED,s,rec.val15 CLIPPED,s,rec.val16 CLIPPED,
                                          s,rec.val17 CLIPPED,s,rec.val18 CLIPPED,s,rec.val19 CLIPPED,
                                          s,rec.val20 CLIPPED
       OTHERWISE LET cnt = rec.val1 CLIPPED
     END CASE
     LET tmp = i

     -- Para Excel >= 2000, Sintaxis ... L1C1:L1C1
     LET ln = "L",tmp CLIPPED,"C1:L",tmp CLIPPED,"C", tmp2 CLIPPED
     IF NOT DDEPoke("EXCEL",libro,ln,cnt) THEN
        CALL librut002_dsperror("DDEPoke")
     END IF

     -- Para Excel 97, Sintaxis ... F1C1:F1C1
     LET ln = "F",tmp CLIPPED,"C1:F",tmp CLIPPED,"C", tmp2 CLIPPED
     IF NOT DDEPoke("EXCEL",libro,ln,cnt) THEN
        CALL librut002_dsperror("DDEPoke")
     END IF

     -- Si es modo interactivo
     IF im THEN
        -- Calculando progreso de avance          
        LET pp   = ((i-1)*100)/total
        LET pb   = pp
        LET tmp3 = pb
        LET tmp3 = tmp3 CLIPPED,"%"
        DISPLAY BY NAME pb,tmp3
        CALL ui.Interface.refresh()
     END IF
     LET i = i+1
    END FOREACH
    LET i = i-1
      
    -- Llmando a macro de excel si el modo es interactivo
    IF im THEN
       -- CALL librut002_macro("EXCEL", libro)
    END IF

    -- Cerrando conexion a excel
    IF NOT DDEFinish("EXCEL",libro) THEN
       CALL librut002_dsperror("DDEPoke")
    END IF

    -- Finalizando conexion 
    CALL DDEFinishAll()

    -- Verificando si es modo interactivo
    IF im THEN
       -- Desplegando registros encontrados
       CALL f.setElementText("formonly.mensaje",resWindow) 
       CALL f.setElementText("formonly.registros",resLabel||i-1)

       MENU resWindow 
        COMMAND resButton
         EXIT MENU
       END MENU
       CLOSE WINDOW progressBar
    END IF
 END IF
 RETURN i-1
END FUNCTION

-- Subrutina para mostrar un mensaje de error con salida del programa

FUNCTION librut002_dsperror(f) 
 DEFINE f VARCHAR(20)
 DISPLAY "Error: ", f,"():",DDEGetError()
 EXIT PROGRAM
END FUNCTION

-- Subrutina para limpiar la ventana de progreso

FUNCTION librut002_inicioprogreso()
 DEFINE screenToBeCleaned  om.DomNode
 DEFINE doc                om.DomDocument
 DEFINE l                  om.NodeList
 DEFINE att                STRING

 CONSTANT nodeName = "Window"
 CONSTANT nodeAtt  = "screen"

 LET doc               = ui.Interface.getDocument()
 LET screenToBeCleaned = doc.getDocumentElement()
 LET l                 = screenToBeCleaned.selectByTagName(nodeName)
 LET screenToBeCleaned = l.item(1)
 LET att               = screenToBeCleaned.getAttribute("name")

 IF (att=nodeAtt) THEN
    CALL doc.removeElement(screenToBeCleaned)
 END IF
END FUNCTION

-- Subrutina para ejecutar una macro de excel 

FUNCTION librut002_macro(prog, sheet)
 DEFINE prog,sheet,macroTxt STRING
 DEFINE res                 SMALLINT
 DEFINE mess                STRING


 -- Ejecutando archivo de excel
 LET macroTxt = '[Run("PERSONAL.XLS!Sanborns")]'
 CALL ui.Interface.frontCall("WINDDE","DDEExecute",[prog,sheet,macroTxt],[res]);
 IF NOT res THEN
    DISPLAY "Macro:"
    DISPLAY macroTxt
    CALL librut002_dsperror("DDEExecute")
 END IF
END FUNCTION

-- Subrutina para crear combobox dinamicos 

FUNCTION librut002_combobox(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)

 -- Quitando espacioes en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList2 from vl_2_query
    DECLARE c_comboList2 cursor FOR q_comboList2

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    FOREACH c_comboList2 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF

 -- Verificanod is hay datos
 IF (vl_2_combo.getItemCount()=0) THEN 
    ERROR " Atencion: no existe datos registrados. (VERIFICA listas de datos)"
 END IF 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_picklist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum,opci)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        comia        CHAR(2),
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        opci         SMALLINT, 
        scr_cnt      SMALLINT

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET comia = ASCII 34

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "picklist"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL f.setElementText("filtro",title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL fgl_settitle("Buaqueda Dinamica")

  -- Inicializando datos
  CALL la_busqueda.CLEAR()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    BEFORE INPUT
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Detalle")
     CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE estos0 CURSOR FOR ex_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH estos0 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE estos0
       FREE ex_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     IF refe = "Ingrese informacion a buscar ..." THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE estos1 CURSOR FOR ex_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH estos1 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE estos1
     FREE ex_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex_sqlst2 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE estos2 CURSOR FOR ex_sqlst2
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH estos2 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE estos2
      FREE ex_sqlst2

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros",'Registros encontrados '||(i-1)|| "    Presione [ENTER] para seleccionar ")

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    BEFORE DISPLAY
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar\nRegistro")

     -- Si opcion es ingresar datos
     IF opci=1 THEN
        CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva busqueda")
     ELSE
        CALL fgl_dialog_setkeylabel("INTERRUPT","")
     END IF

     -- Si no se ecnontraron datos
     IF i=1 THEN
        --CALL box_valdato("No se encontraron datos.")
        display "NO HAY DATOS" 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    ON KEY(INTERRUPT)
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON KEY(ACCEPT)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_formlist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum,opci,Tpf)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        tipofiltro   SMALLINT, 
        opci,tpf     SMALLINT, 
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET tipofiltro = 0 

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   DISPLAY BY NAME tipofiltro 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    ON ACTION codigo
     -- Seleccionanado filtro por codigo
     LET tipofiltro = 1 
     DISPLAY BY NAME tipofiltro 

    ON ACTION descrip 
     -- Seleccionanado filtro por descripcion 
     LET tipofiltro = 0 
     DISPLAY BY NAME tipofiltro 

    BEFORE INPUT
     -- Desabilitando tipo de filtro cuando seleccion no aplica 
     IF NOT tpf THEN
        CALL Dialog.SetActionHidden("codigo",1) 
        CALL Dialog.SetActionHidden("descrip",1) 
     END IF 

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex2_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE westos0 CURSOR FOR ex2_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH westos0 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE westos0
       FREE ex2_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF
     -- Preparando busqueda
     PREPARE ex0_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE yestos1 CURSOR FOR ex0_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH yestos1 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE yestos1
     FREE ex0_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex1_sqlst2 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE xestos2 CURSOR FOR ex1_sqlst2
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH xestos2 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE xestos2
      FREE ex1_sqlst2

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     IF (opci=0) THEN
        CALL Dialog.SetActionHidden("consulta",1) 
     ELSE 
        CALL Dialog.SetActionHidden("cancel",1) 
     END IF  

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_formlistcli(Title0,Title1,Title2,Title3,Field1,Field2,Field3,Tabname,Condicion,OrderNum,opci,Tpf)
 DEFINE Title0       STRING,
	Title1,  
	Title2,
	Title3       STRING,  
	Field1, 
	Field2,
	Field3,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100),
         numtri      VARCHAR(30) 
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         nombre      VARCHAR(100),
         numtri      VARCHAR(30) 
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        tipofiltro   SMALLINT, 
        opci,tpf     SMALLINT, 
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET tipofiltro = 0 

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist2"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.nombre",title2)
  CALL f.setElementText("formonly.numtri",title3)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   DISPLAY BY NAME tipofiltro 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    ON ACTION nit          
     -- Seleccionanado filtro por numero tributario 
     LET tipofiltro = 1 
     DISPLAY BY NAME tipofiltro 

    ON ACTION nombre  
     -- Seleccionanado filtro por nombre
     LET tipofiltro = 0 
     DISPLAY BY NAME tipofiltro 

    BEFORE INPUT
     -- Desabilitando tipo de filtro cuando seleccion no aplica 
     IF NOT tpf THEN
        CALL Dialog.SetActionHidden("nit",1) 
        CALL Dialog.SetActionHidden("nombre",1) 
     END IF 

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex4_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE westos4 CURSOR FOR ex4_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH westos4 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE westos4
       FREE ex4_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field3 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex5_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE yestos5 CURSOR FOR ex5_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH yestos5 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE yestos5
     FREE ex5_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field3 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex1_sqlst6 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE xestos6 CURSOR FOR ex1_sqlst6
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH xestos6 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE xestos6
      FREE ex1_sqlst6

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     IF (opci=0) THEN
        CALL Dialog.SetActionHidden("consulta",1) 
     ELSE 
        CALL Dialog.SetActionHidden("cancel",1) 
     END IF  

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_selectlist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
        END RECORD,
        i            SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist3"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.nombre",title2)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET int_flag = FALSE  
  LET i = 1

  -- Creando busqueda
  LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, 
                  " FROM ",tabname CLIPPED,
                  " WHERE ",condicion CLIPPED, 
                  " ORDER BY ",OrderNum

  -- Preparando busqueda
  PREPARE esqlst FROM sqlstmnt

  -- Seleccionando datos
  DECLARE cursel CURSOR FOR esqlst
  CALL la_busqueda.CLEAR()
  LET i =1
  FOREACH cursel INTO la_busqueda[i].*
   LET i=i+1
  END FOREACH
  FREE cursel 
  FREE esqlst

  -- Definiendo tecla de aceptacion
  OPTIONS ACCEPT KEY RETURN

  -- Desplegnado datos
  MESSAGE "Presione [ENTER] para seleccionar"

  DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
   ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION seleccion
     EXIT DISPLAY

    ON ACTION cancel
     EXIT DISPLAY 

    ON KEY(RETURN)
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
  END DISPLAY
  MESSAGE "" 
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para crear combobox dinamicos con mensaje de respuesta

FUNCTION librut002_cbxdin(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)

 -- Quitando espacioes en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList3 from vl_2_query
    DECLARE c_comboList3 cursor FOR q_comboList3

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    FOREACH c_comboList3 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     display vl_2_valor 
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF

 -- Retornando numero de datos 
 RETURN vl_2_combo.getItemCount() 
END FUNCTION

