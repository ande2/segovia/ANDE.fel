# Property of Four Js*
# (c) Copyright Four Js 1995, 2019. All Rights Reserved.
# * Trademark of Four Js Development Tools Europe Ltd
#   in the United States and elsewhere

#+ Remote execution of client hosted programs.
#+

DEFINE DDECallStatus SMALLINT
DEFINE DDECallResult SMALLINT

#+ Starts a program on the workstation without waiting.
#+ @return Boolean
#+ @param commandLine Program name.
#+ @return TRUE on success, FALSE on error.
#+
#+ @code 4gl
#+ LET result=winExec("notepad")
#+ -- 1 backslash in the resulting commandline at the client side
#+ -- requires 4 backslash characters in the source. To avoid the
#+ -- backslash hell one should add the executables in the path
#+ -- at the client side.
#+ LET result=winExec("c:\\\\windows\\\\system32\\\\notepad.exe")
#+ LET result=winExec("\\\\\\\\MYCOMPUTER\\\\C\\\\windows\\\\system32\\\\notepad.exe")
FUNCTION winExec(commandLine)
  DEFINE commandLine STRING
  DEFINE result INTEGER
  WHENEVER ERROR CONTINUE
  let commandLine=__fgl_convertTclString(commandLine)
  CALL ui.Interface.frontcall("standard","execute",[commandLine,0],[result])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN result ELSE RETURN FALSE END IF
END FUNCTION

#+ Starts a program on the workstation and waits for program-termination.
#+ @returnType Boolean
#+ @param commandLine Program name with or without absolute path.
#+ @return TRUE on success, FALSE on error.
FUNCTION winExecWait(commandLine)
  DEFINE commandLine STRING
  DEFINE result INTEGER
  WHENEVER ERROR CONTINUE
  let commandLine=__fgl_convertTclString(commandLine)
  CALL ui.Interface.frontcall("standard","execute",[commandLine,1],[result])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN result ELSE RETURN FALSE END IF
END FUNCTION

#+ Opens a document on the workstation with the corresponding program.
#+ @returnType Boolean
#+ @param fileName Document name with or without absolute path.
#+ @return TRUE on success, FALSE on error.
FUNCTION winShellExec(fileName)
  DEFINE fileName STRING
  DEFINE result INTEGER
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("standard","shellexec",[fileName],[result])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN result ELSE RETURN FALSE END IF
END FUNCTION

#+ Opens a dialog to search a file on the workstation.
#+ @returnType Boolean
#+ @param dirName Default directory name.
#+ @param typeName The name to be displayed for the file type.
#+ @param typeExts Blank separated list of file extensions.
#+ @param caption The caption to be displayed.
#+ @return A file name on success, otherwise NULL.
FUNCTION winOpenFile(dirName,typeName,typeExts,caption)
  DEFINE dirName STRING
  DEFINE typeName STRING
  DEFINE typeExts STRING
  DEFINE caption STRING
  DEFINE fn STRING
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("standard","openfile",
          [dirName,typeName,typeExts,caption],[fn])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN fn ELSE RETURN NULL END IF
END FUNCTION

#+ Opens a dialog to save a file on the workstation.
#+ @returnType Boolean
#+ @param dirName Default directory name.
#+ @param typeName The name to be displayed for the file type.
#+ @param typeExts Blank separated list of file extensions.
#+ @param caption The caption to be displayed.
#+ @return A file name on success, otherwise NULL.
FUNCTION winSaveFile(dirName,typeName,typeExts,caption)
  DEFINE dirName STRING
  DEFINE typeName STRING
  DEFINE typeExts STRING
  DEFINE caption STRING
  DEFINE fn STRING
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("standard","savefile",
          [dirName,typeName,typeExts,caption],[fn])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN fn ELSE RETURN NULL END IF
END FUNCTION

#+ Opens a dialog to search a directory on the workstation.
#+ @returnType Boolean
#+ @param dirName Default directory name.
#+ @param caption The caption to be displayed.
#+ @return A directory name on success, otherwise NULL.
FUNCTION winOpenDir(dirName,caption)
  DEFINE dirName STRING
  DEFINE caption STRING
  DEFINE dn STRING
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("standard","opendir",[dirName,caption],[dn])
  WHENEVER ERROR STOP
  IF STATUS=0 THEN RETURN dn ELSE RETURN NULL END IF
END FUNCTION

#-------------------------------------------------------------------------------

FUNCTION __returnDDECallResult(s,r)
  DEFINE s,r INTEGER
  LET DDECallStatus=s
  LET DDECallResult=r
  IF DDECallStatus=0 THEN
    RETURN DDECallResult
  ELSE
    RETURN FALSE
  END IF
END FUNCTION

#+ Starts a DDE connection.
#+ @returnType Boolean
#+ @param progname DDE server program to start ('WINWORD', 'EXCEL').
#+ @param docname Name of the document to open or 'System'.
#+ @return TRUE on success, FALSE on error, get message with DDEGetError().
#+
#+ Warning: Document names depend from the workstation language, for example,
#+ on a German Windows the first document is "Dokument1".
FUNCTION DDEConnect(progname,docname)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE result   INTEGER
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("WINDDE","DDEConnect",[progname,docname],[result])
  WHENEVER ERROR STOP
  RETURN __returnDDECallResult(STATUS,result)
END FUNCTION

FUNCTION __fgl_convertTclString(s)
  DEFINE s String
  DEFINE b base.StringBuffer
  DEFINE i INTEGER
  DEFINE c,cr CHAR
  LET cr=ascii(13)
  LET b=base.StringBuffer.create()
  CALL b.append(s)
  LET i=0
  LET i=b.getIndexOf("\\",i+1)
  WHILE i>0 AND i<b.getLength()
    LET c=b.getCharAt(i+1)
    CASE c
      WHEN "t"
        CALL b.replaceAt(i,2,"\t")
      WHEN "n"
        CALL b.replaceAt(i,2,"\n")
      WHEN "r"
        CALL b.replaceAt(i,2,cr)
      WHEN "\\"
        CALL b.replaceAt(i,2,"\\")
      OTHERWISE
        CALL b.replaceAt(i,1,"")
    END CASE
    LET i=b.getIndexOf("\\",i+1)
  END WHILE
  RETURN b.toString()
END FUNCTION

#+ Sends a DDE command to be executed.
#+ @param progname DDE server program.
#+ @param docname Document name or 'System'.
#+ @param cells Cell specs (Ger = Z#S#:Z#S#, Eng = R#C#:R#C#, Fre = L#C#:L#C#).
#+ @param values Tab separated list of values ( 'XX\tYY\tZZ' ).
#+ @return TRUE on success, FALSE on error, get message with DDEGetError().
FUNCTION DDEPoke(progname,docname,cells,values)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE cells    STRING
  DEFINE values   STRING
  DEFINE result   INTEGER
  WHENEVER ERROR CONTINUE
  let values=__fgl_convertTclString(values)
  CALL ui.Interface.frontcall("WINDDE","DDEPoke",
    [progname CLIPPED,docname CLIPPED,cells CLIPPED,values CLIPPED],[result])
  WHENEVER ERROR STOP
  RETURN __returnDDECallResult(STATUS,result)
END FUNCTION

#+ Sends a DDE command to be executed.
#+ @param progname DDE server program.
#+ @param docname Document name or 'System'.
#+ @param execcmd Command to be executed.
#+ @return TRUE on success, FALSE on error, get message with DDEGetError().
#+
#+ The execcmd will be enclosed in brackets - see DDEExecute.
FUNCTION DDEExecute(progname,docname,execcmd)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE execcmd  STRING
  let execcmd="[",__fgl_convertTclString(execcmd),"]"
  -- let execcmd ="\\["||execcmd||"\\]"
  RETURN DDEExecute2(progname,docname,execcmd)
END FUNCTION

#+ Sends a DDE command to be executed.
#+ @returnType Boolean
#+ @param progname DDE server program.
#+ @param docname Document name or 'System'.
#+ @param execcmd Command to be executed.
#+ @return TRUE on success, FALSE on error, get message with DDEGetError().
#+
#+ The execcmd will be sent untouched - see DDEExecute.
FUNCTION DDEExecute2(progname,docname,execcmd)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE execcmd  STRING
  DEFINE result   INTEGER
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("WINDDE","DDEExecute",
    [progname CLIPPED,docname CLIPPED,execcmd CLIPPED],[result])
  WHENEVER ERROR STOP
  RETURN __returnDDECallResult(STATUS,result)
END FUNCTION

#+ Retrieve a DDE command to be executed.
#+ @param progname DDE server program.
#+ @param docname Document name or 'System'.
#+ @param cells Cell specs (Ger = Z#S#:Z#S#, Eng = R#C#:R#C#, Fre = L#C#:L#C#).
#+ @return Tab separated list of values ( 'XX\tYY\tZZ' )."
FUNCTION DDEPeek(progname,docname,cells)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE cells    STRING
  DEFINE result   INTEGER
  DEFINE value    STRING
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("WINDDE","DDEPeek",
    [progname CLIPPED,docname CLIPPED,cells CLIPPED],[result,value])
  WHENEVER ERROR STOP
  LET DDECallStatus=STATUS
  RETURN value
END FUNCTION


#+ Terminates a DDE connection.
#+ @returnType Boolean
#+ @param progname DDE server program.
#+ @param docname Document name or 'System'.
#+ @return TRUE on success, FALSE on error, get message with DDEGetError().
FUNCTION DDEFinish(progname,docname)
  DEFINE progname STRING
  DEFINE docname  STRING
  DEFINE result   INTEGER
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("WINDDE","DDEFinish",[progname,docname],[result])
  WHENEVER ERROR STOP
  RETURN __returnDDECallResult(STATUS,result)
END FUNCTION

#+ Terminates all DDE connections.
FUNCTION DDEFinishAll()
  DEFINE result INTEGER
  WHENEVER ERROR CONTINUE
  CALL ui.Interface.frontcall("WINDDE","DDEFinishAll",[],[result])
  WHENEVER ERROR STOP
  LET DDECallStatus = STATUS
END FUNCTION

#+ Returns last DDE error message.
#+ @return The last error message.
FUNCTION DDEGetError()
  DEFINE s INTEGER
  DEFINE errmsg STRING
  IF DDECallStatus IS NULL THEN RETURN NULL END IF
  IF DDECallStatus<>0 THEN
    LET errmsg="DDE Function Call Error: " || DDECallStatus
  ELSE
    IF NOT DDECallResult THEN
       LET DDECallResult=TRUE
       WHENEVER ERROR CONTINUE
       CALL ui.Interface.frontcall("WINDDE","DDEError",[],[errmsg])
       WHENEVER ERROR STOP
    END IF
  END IF
  RETURN errmsg
END FUNCTION
