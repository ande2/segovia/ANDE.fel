{
invqbe006.4gl 
Mynor Ramirez
Mantenimiento de clientes 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel

GLOBALS "invglo006.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe006_clientes(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_clientes.estado, 
        wstatus             LIKE fac_clientes.status,
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
        
      --Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN


      
 -- Obteniendo datos de la window actual
 LET w = ui.Window.getCurrent()
 LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL 
  IF (operacion=3) THEN
     LET rotulo = "Borrar" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae006_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codcli,a.nomcli,a.numnit,
                                a.numtel,a.numfax,a.dircli,
                                a.nomcon,a.bemail,a.hayfac,
                                a.moncre,a.diacre,a.haydat, 
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae006_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codcli,a.nomcli,a.numnit,a.estado,a.status ",
                 " FROM fac_clientes a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_clientes SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_clientes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_clientes INTO v_clientes[totlin].tcodcli THRU v_clientes[totlin].tnumnit,westado,wstatus
    -- Verficando estado
    IF (westado=0) THEN
       LET v_clientes[totlin].testado = "mars_cancelar.png"
    ELSE
       LET v_clientes[totlin].testado = "mars_cheque.png"
    END IF

    -- Verficando estatus
    IF (wstatus=0) THEN
       LET v_clientes[totlin].tstatus = "mars_bloqueo.png"
    ELSE
       LET v_clientes[totlin].tstatus = "mars_cheque.png"
    END IF

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_clientes
   FREE  c_clientes
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_clientes TO s_clientes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae006_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae006_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae006_clientes(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe006_datos(v_clientes[ARR_CURR()].tcodcli)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF invmae006_clientes(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe006_datos(v_clientes[ARR_CURR()].tcodcli)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe006_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este cliente ya tiene movimientos. Cliente no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de "||rotulo CLIPPED||" este cliente ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae006_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Cliente"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Numero de NIT"
      LET arrcols[4]  = "Numero Telefono"
      LET arrcols[5]  = "Numero FAX"
      LET arrcols[6]  = "Direccion"
      LET arrcols[7]  = "Contacto"
      LET arrcols[8]  = "Direccion de Correo"
      LET arrcols[9]  = "Estado Cliente"
      LET arrcols[10] = "Estatus Cliente"
      LET arrcols[11] = "Cliente Factura"
      LET arrcols[12] = "Datos Factura"
      LET arrcols[13] = "Monto Credito"
      LET arrcols[14] = "Dias Credito"
      LET arrcols[15] = "Saldo Credito"
      LET arrcols[16] = "Usuario Registro"
      LET arrcols[17] = "Fecha Registro"
      LET arrcols[18] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry         = "SELECT a.codcli,a.nomcli,a.numnit,a.numtel,a.numfax,a.dircli,a.nomcon,a.bemail,",
                               "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END CASE,",
                               "CASE (a.status) WHEN 1 THEN 'Liberado' WHEN 0 THEN 'Bloqueado' END CASE,",
                               "CASE (a.hayfac) WHEN 1 THEN 'Si' WHEN 2 THEN 'No' END CASE,",
                               "CASE (a.haydat) WHEN 1 THEN 'Si' WHEN 2 THEN 'No' END CASE,",
                               "a.moncre,a.diacre,a.salcre,a.userid,a.fecsis,a.horsis ",
                        " FROM fac_clientes a ",
                        " WHERE ",qrytext CLIPPED,
                        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Clientes.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe006_datos(v_clientes[1].tcodcli)
      ELSE
         CALL invqbe006_datos(v_clientes[ARR_CURR()].tcodcli)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen clientes con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe006_datos(wcodcli)
 DEFINE wcodcli LIKE fac_clientes.codcli,
        existe    SMALLINT,
        qryres    STRING 
 DEFINE l         SMALLINT 
 DEFINE lqry      STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_clientes a "||
              "WHERE a.codcli = "||wcodcli||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_clientest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_clientest INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomcli THRU w_mae_pro.estado 
  DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_clientest
 FREE  c_clientest

 --Llenando detalle de frases FEL
 DECLARE curfra CURSOR FOR 
   SELECT cod_frase, cod_escenario FROM fac_clientes_fra
      WHERE codcli = wcodcli ORDER BY 1,2
 LET l = 1
 FOREACH curfra INTO gdet[l].cod_frase, gdet[l].cod_escenario
   LET l = l + 1
 END FOREACH 
 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomcli THRU w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 DISPLAY ARRAY gdet TO safrase.*
   BEFORE DISPLAY
      CALL combo_din2("cod_frase","SELECT cod_frase, cod_frase||' - '||nombre FROM facturafel_fra")

   BEFORE ROW 
      LET l = arr_curr()
      LET lqry = "SELECT cod_escenario, cod_escenario||' - '||nombre FROM facturafel_fra_e " -- WHERE cod_frase = ", gdet[l].cod_frase 
      CALL combo_din2("cod_escenario",lqry)
      EXIT DISPLAY 
 END DISPLAY 
END FUNCTION 

-- Subrutina para verificar si el cliente ya tiene movimientos  

FUNCTION invqbe006_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.coddes = w_mae_pro.codcli) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
