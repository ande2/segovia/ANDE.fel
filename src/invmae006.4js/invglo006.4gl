{ 
invglo006.4gl
Mynor Ramirez
Mantenimiento de clientes
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE fac_clientes.*,
       v_clientes  DYNAMIC ARRAY OF RECORD
        tcodcli    LIKE fac_clientes.codcli,
        tnomcli    LIKE fac_clientes.nomcli, 
        tnumnit    LIKE fac_clientes.numnit,
        testado    CHAR(20),
        tstatus    CHAR(20)
       END RECORD

DEFINE gdet DYNAMIC ARRAY OF RECORD
   cod_frase      LIKE fac_clientes_fra.cod_frase,
   cod_escenario  LIKE fac_clientes_fra.cod_escenario
END RECORD 
END GLOBALS
