{
invmae011.4gl 
Mynor Ramirez
Mantenimiento de medidas
}

-- Definicion de variables globales 

GLOBALS "invglo011.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilo y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae011_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae011_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae011a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invmae011",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de unidades de medida 
  CALL librut003_cbxunidadesmedida("unimed") 
  -- Cargando combobox de unidades de medida de la medida de referencia
  CALL librut003_cbxunidadesmedida("unimto") 

  -- Menu de opciones
  MENU " Medidas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de medidas."
    CALL invqbe011_medidpro(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva medida."
    LET savedata = invmae011_medidpro(1) 
   COMMAND "Modificar"
    " Modificacion de una medida existente."
    CALL invqbe011_medidpro(2) 
   COMMAND "Borrar"
    " Eliminacion de medida existente."
    CALL invqbe011_medidpro(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae011_medidpro(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae011_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codabr,
                w_mae_pro.desmed,
                w_mae_pro.unimed, 
                w_mae_pro.epqfra, 
                w_mae_pro.xlargo, 
                w_mae_pro.yancho, 
                w_mae_pro.medtot,
                w_mae_pro.unimto 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si meddia ya tiene movimientos, no puede modificar el codigo abreviado 
    IF (operacion=2) THEN -- Modificar
     IF invqbe011_integridad() THEN
        CALL Dialog.SetFieldActive("codabr",FALSE) 
        CALL Dialog.SetFieldActive("desmed",FALSE) 
        CALL Dialog.SetFieldActive("unimed",FALSE) 
     ELSE
        -- Habilitando
        CALL Dialog.SetFieldActive("codabr",TRUE) 
        CALL Dialog.SetFieldActive("desmed",TRUE) 
        CALL Dialog.SetFieldActive("unimed",TRUE) 
     END IF
    END IF 

   AFTER FIELD codabr
    -- Verificando codigo de la medida
    IF (LENGTH(w_mae_pro.codabr)<=0) THEN
       ERROR "Error: codigo abreviado invalido, VERIFICA."
       LET w_mae_pro.codabr = NULL
       CLEAR codabr
       NEXT FIELD codabr 
    END IF 

    -- Verificando que no exista otro codigo abreviado
    SELECT UNIQUE (a.codabr)
     FROM  inv_medidpro a
     WHERE (a.codmed != w_mae_pro.codmed)
       AND (a.codabr  = w_mae_pro.codabr)
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra medida con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr 
     END IF

   AFTER FIELD desmed
    -- Verificando nombre de la medida
    IF (LENGTH(w_mae_pro.desmed)<=0) THEN
       ERROR "Error: nombre de la medida invalido, VERIFICA ..."
       LET w_mae_pro.desmed = NULL
       CLEAR desmed
       NEXT FIELD desmed 
    END IF 

   AFTER FIELD unimed 
    -- Verificando undad de medida
    IF w_mae_pro.unimed IS NULL THEN
       ERROR "Error: unidad de medida invalida, VERIFICA ..."
       NEXT FIELD unimed 
    END IF 

   ON CHANGE epqfra
    -- Ajustando ancho y largo
    CASE (w_mae_pro.epqfra)
     WHEN 0 LET w_mae_pro.yancho = 0
            LET w_mae_pro.xlargo = 0
            DISPLAY BY NAME w_mae_pro.yancho,w_mae_pro.xlargo
            CALL Dialog.setFieldActive("xlargo",0)
            CALL Dialog.setFieldActive("yancho",0)
     WHEN 1 LET w_mae_pro.yancho = NULL
            LET w_mae_pro.xlargo = NULL
            DISPLAY BY NAME w_mae_pro.yancho,w_mae_pro.xlargo
            CALL Dialog.setFieldActive("xlargo",1)
            CALL Dialog.setFieldActive("yancho",1)
    END CASE

   AFTER FIELD epqfra 
    -- Verificando si medida tiene empaque fraccionable
    IF w_mae_pro.epqfra IS NULL THEN
       ERROR "Error: empaque fracciomable invalido, VERIFICA ..."
       NEXT FIELD epqfra
    END IF 

    -- Ajustando largo y ancho 
    CASE (w_mae_pro.epqfra)
     WHEN 0 CALL Dialog.setFieldActive("yancho",0)
            CALL Dialog.setFieldActive("xlargo",0)
     WHEN 1 CALL Dialog.setFieldActive("yancho",1)
            CALL Dialog.setFieldActive("xlargo",1)
    END CASE

    -- Verificando operacion
    IF (operacion=1) AND NOT w_mae_pro.epqfra THEN
       LET w_mae_pro.yancho = 0
       LET w_mae_pro.xlargo = 0
       DISPLAY BY NAME w_mae_pro.yancho,w_mae_pro.xlargo
    END IF

   AFTER FIELD xlargo
    -- Verificando largo
    IF w_mae_pro.xlargo IS NULL OR
       (w_mae_pro.xlargo <=0) THEN
       ERROR "Error: largo invalido, VERIFICA ..."
       LET w_mae_pro.xlargo = NULL
       CLEAR xlargo
       NEXT FIELD xlargo
    END IF

   AFTER FIELD yancho
    -- Verificando ancho
    IF w_mae_pro.yancho IS NULL OR
       (w_mae_pro.yancho <=0) THEN
       ERROR "Error: ancho invalido, VERIFICA ..."
       LET w_mae_pro.yancho = NULL
       CLEAR yancho
       NEXT FIELD yancho
    END IF

   AFTER FIELD medtot 
    -- Verificando medida de referencia 
    IF w_mae_pro.medtot IS NULL OR
       (w_mae_pro.medtot <0) THEN
       ERROR "Error: medida de referencia invalida, VERIFICA ..."
       LET w_mae_pro.medtot = NULL
       CLEAR medtot 
       NEXT FIELD medtot 
    END IF

    -- Verificando si medida de referencia es cero
    IF w_mae_pro.medtot=0 THEN 
       LET w_mae_pro.unimto = NULL
       DISPLAY w_mae_pro.unimto  
       CALL Dialog.setFieldActive("unimto",0)
    ELSE
       CALL Dialog.setFieldActive("unimto",1)
    END IF 

   AFTER FIELD unimto 
    -- Verificando undad de medida de la medida total
    IF w_mae_pro.unimto IS NULL THEN
       ERROR "Error: unidad de medida total invalida, VERIFICA ..."
       NEXT FIELD unimto 
    END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codabr IS NULL THEN 
       NEXT FIELD codabr 
    END IF
    IF w_mae_pro.desmed IS NULL THEN 
       NEXT FIELD desmed 
    END IF

    -- Verificando medidas
    IF w_mae_pro.epqfra THEN 
     IF w_mae_pro.xlargo IS NULL THEN
        ERROR "Error: debe ingresarse el largo de la medida, VERIFICA ..." 
        NEXT FIELD xlargo  
     END IF 
     IF w_mae_pro.yancho IS NULL THEN
        ERROR "Error: debe ingresarse el ancho de la medida, VERIFICA ..." 
        NEXT FIELD yancho  
     END IF 
    END IF  

    IF w_mae_pro.medtot IS NULL THEN
       ERROR "Error: debe ingresarse la medida de referencia, VERIFICA ..." 
       NEXT FIELD medtot
    END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae011_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando medida
    CALL invmae011_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae011_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar las medidas

FUNCTION invmae011_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando medida ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codmed),0)
    INTO  w_mae_pro.codmed
    FROM  inv_medidpro a
    IF (w_mae_pro.codmed IS NULL) THEN
       LET w_mae_pro.codmed = 1
    ELSE 
       LET w_mae_pro.codmed = (w_mae_pro.codmed+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_medidpro   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codmed 

   --Asignando el mensaje 
   LET msg = "Medida (",w_mae_pro.codmed USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_medidpro
   SET    inv_medidpro.*      = w_mae_pro.*
   WHERE  inv_medidpro.codmed = w_mae_pro.codmed 

   --Asignando el mensaje 
   LET msg = "Medida (",w_mae_pro.codmed USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM inv_medidpro 
   WHERE (inv_medidpro.codmed = w_mae_pro.codmed)

   --Asignando el mensaje 
   LET msg = "Medida (",w_mae_pro.codmed USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae011_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae011_inival(i)
 DEFINE i SMALLINT

 -- Verificando unidad de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codmed = 0 
   LET w_mae_pro.epqfra = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.desmed THRU w_mae_pro.unimto 
 DISPLAY BY NAME w_mae_pro.codmed,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
