{
facqbe001.4gl 
Mynor Ramirez
Mantenimiento de emisores de tarjetas de credito
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "facglo001.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe001_emisores(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae001_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemi,a.nomemi,a.nomabr,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae001_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemi,a.nomemi ",
                 " FROM fac_emitacre a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_tarcre SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tarjetas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_tarcre INTO v_tarjetas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_tarcre
   FREE  c_tarcre
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tarjetas TO s_tarjetas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF facmae001_emisores(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL facqbe001_datos(v_tarjetas[ARR_CURR()].tcodemi)
      END IF 

     ON KEY (CONTROL-M) 
      IF (operacion=2) THEN 
       -- Modificando 
       IF facmae001_emisores(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL facqbe001_datos(v_tarjetas[ARR_CURR()].tcodemi)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe001_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este emisor ya tiene movimientos, por favor VERIFICAR ...",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este emisor ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
            --  Eliminando
            CALL facmae001_grabar(3)
            EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Emisor" 
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Abreviado"
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.* ",
                       " FROM fac_emitacre a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Emisores_de_Tarjetas.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 
      
     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbe001_datos(v_tarjetas[1].tcodemi)
      ELSE 
         CALL facqbe001_datos(v_tarjetas[ARR_CURR()].tcodemi)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen emisores de tarjetas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe001_datos(wcodemi)
 DEFINE wcodemi LIKE fac_emitacre.codemi,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_emitacre a "||
              "WHERE a.codemi = "||wcodemi||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_tarcret SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_tarcret INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomemi,w_mae_pro.nomabr 
  DISPLAY BY NAME w_mae_pro.codemi,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_tarcret
 FREE  c_tarcret

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomemi,w_mae_pro.nomabr 
 DISPLAY BY NAME w_mae_pro.codemi,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si el emisor ya tiene movimientos 

FUNCTION facqbe001_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_tarjetas a
  WHERE (a.codemi = w_mae_pro.codemi) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
