{
facmae001.4gl 
Mynor Ramirez
Mantenimiento de emisores de tarjetas de credito
}

-- Definicion de variables globales 

GLOBALS "facglo001.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facmae001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL facmae001_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae001_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facmae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facmae001",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Emisores"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de emisores de tarjetas de credito."
    CALL facqbe001_emisores(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo emisor de tarjeta de credito."
    LET savedata = facmae001_emisores(1) 
   COMMAND "Modificar"
    " Modificacion de un emisor de tarjeta de credito existente."
    CALL facqbe001_emisores(2) 
   COMMAND "Borrar"
    " Eliminacion de un emisor de tarjeta de credito existente."
    CALL facqbe001_emisores(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION facmae001_emisores(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae001_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomemi,
                w_mae_pro.nomabr
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomemi  
    --Verificando nombre del emisor
    IF (LENGTH(w_mae_pro.nomemi)=0) THEN
       ERROR "Error: nombre del emisor invalido, VERIFICA"
       LET w_mae_pro.nomemi = NULL
       NEXT FIELD nomemi  
    END IF

    -- Verificando que no exista otro emisor con el mismo nombre
    SELECT UNIQUE (a.codemi)
     FROM  fac_emitacre a
     WHERE (a.codemi != w_mae_pro.codemi) 
       AND (a.nomemi  = w_mae_pro.nomemi) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro emisor con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomemi
     END IF 

   AFTER FIELD nomabr 
    --Verificando nombre abreviado del emisor
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: nombre abreviado invalido, VERIFICA"
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomemi IS NULL THEN 
       NEXT FIELD nomemi
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae001_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando emisor
    CALL facmae001_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facmae001_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un emisor   

FUNCTION facmae001_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando emisor de tarjeta ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codemi),0)
    INTO  w_mae_pro.codemi
    FROM  fac_emitacre a
    IF (w_mae_pro.codemi IS NULL) THEN
       LET w_mae_pro.codemi = 1
    ELSE 
       LET w_mae_pro.codemi = (w_mae_pro.codemi+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_emitacre   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codemi 

   --Asignando el mensaje 
   LET msg = "Emisor (",w_mae_pro.codemi USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_emitacre
   SET    fac_emitacre.*        = w_mae_pro.*
   WHERE  fac_emitacre.codemi = w_mae_pro.codemi 

   --Asignando el mensaje 
   LET msg = "Emisor (",w_mae_pro.codemi USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando emisores
   DELETE FROM fac_emitacre 
   WHERE (fac_emitacre.codemi = w_mae_pro.codemi)

   --Asignando el mensaje 
   LET msg = "Emisor (",w_mae_pro.codemi USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae001_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae001_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codemi = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemi,w_mae_pro.nomabr 
 DISPLAY BY NAME w_mae_pro.codemi,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
