{
invmae002.4gl 
Mynor Ramirez
Mantenimiento de colores de producto 
}

-- Definicion de variables globales 

GLOBALS "invglo002.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae002.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae002_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae002_mainmenu()
 DEFINE titulo   STRING, 
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae002a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,1)
  RETURNING existe,wpais 
  CALL librut001_header("invmae002",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Colors"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de colores."
    CALL invqbe002_colores(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo color."
    LET savedata = invmae002_colores(1) 
   COMMAND "Modificar"
    " Modificacion de un color existente."
    CALL invqbe002_colores(2) 
   COMMAND "Borrar"
    " Eliminacion de un color existente."
    CALL invqbe002_colores(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae002_colores(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae002_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomcol,
                w_mae_pro.codabr
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomcol  
    --Verificando nombre de la color
    IF (LENGTH(w_mae_pro.nomcol)=0) THEN
       ERROR "Error: nombre del  color invalida, VERIFICA"
       LET w_mae_pro.nomcol = NULL
       NEXT FIELD nomcol  
    END IF

    -- Verificando que no exista otro color con el mismo nombre
    SELECT UNIQUE (a.codcol)
     FROM  inv_colorpro a
     WHERE (a.codcol != w_mae_pro.codcol) 
       AND (a.nomcol  = w_mae_pro.nomcol) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro color con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomcol
     END IF 
   AFTER FIELD codabr  
    --Verificando el codigo abreviado
    IF (LENGTH(w_mae_pro.codabr)=0) THEN
       ERROR "Error: codigo abreviado invalida, VERIFICA"
       LET w_mae_pro.codabr = NULL
       NEXT FIELD codabr  
    END IF

    -- Verificando que no exista otro codigo abreviado
    SELECT UNIQUE (a.codabr)
     FROM  inv_colorpro a
     WHERE (a.codcol != w_mae_pro.codcol) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro color con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomcol IS NULL THEN 
       NEXT FIELD nomcol
    END IF
    IF w_mae_pro.codabr IS NULL THEN 
       NEXT FIELD codabr
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae002_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando color
    CALL invmae002_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae002_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un color

FUNCTION invmae002_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando color ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codcol),0)
    INTO  w_mae_pro.codcol
    FROM  inv_colorpro a
    IF (w_mae_pro.codcol IS NULL) THEN
       LET w_mae_pro.codcol = 1
    ELSE 
       LET w_mae_pro.codcol = (w_mae_pro.codcol+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_colorpro   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codcol 

   --Asignando el mensaje 
   LET msg = "Color (",w_mae_pro.codcol USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_colorpro
   SET    inv_colorpro.*        = w_mae_pro.*
   WHERE  inv_colorpro.codcol = w_mae_pro.codcol 

   --Asignando el mensaje 
   LET msg = "Color (",w_mae_pro.codcol USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando colores
   DELETE FROM inv_colorpro 
   WHERE (inv_colorpro.codcol = w_mae_pro.codcol)

   --Asignando el mensaje 
   LET msg = "Color (",w_mae_pro.codcol USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae002_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae002_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codcol = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcol THRU w_mae_pro.codabr 
 DISPLAY BY NAME w_mae_pro.codcol,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
