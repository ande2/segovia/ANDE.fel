DROP VIEW vis_repventa
;
CREATE VIEW vis_repventa (fecemi,docvta,ptovta,codemp,totpag,subtot,totiva,usuario,tipdoc,serial,tipo) as
SELECT c.fecemi , c.nserie[1]||' '||c.numdoc docvta ,a.nompos , b.codemp , c.totpag , c.subtot , c.totiva , d.userid,
CASE WHEN (c.hayord = 0) THEN "PRO" WHEN (c.hayord =1 ) THEN "ORD" 
 END tipdoc , c.lnktra, c.tipdoc
FROM fac_puntovta a , glb_empresas b , fac_mtransac c , glb_usuarios d
where a.codemp = b.codemp
and   c.numpos = a.numpos
and   c.codemp = b.codemp
and   c.usrope = d.userid
and   c.estado ="V"
;
grant select on vis_repventa to public
