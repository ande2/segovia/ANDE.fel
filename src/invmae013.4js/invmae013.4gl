{
invmae013.4gl 
Mynor Ramirez
Mantenimiento de usuarios 
}

-- Definicion de variables globales 

GLOBALS "invglo013.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae013_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae013_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae013a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae013",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Usuarios"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de usuarios."
    CALL invqbe013_usuarios(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo usuario."
    LET savedata = invmae013_usuarios(1) 
   COMMAND "Modificar"
    " Modificacion de un usuario existente."
    CALL invqbe013_usuarios(2) 
   COMMAND "Borrar"
    " Eliminacion de un usuario existente."
    CALL invqbe013_usuarios(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae013_usuarios(operacion)
 DEFINE w_mae_usr         RECORD LIKE glb_usuarios.*,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae013_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.userid,
                w_mae_pro.nomusr,
                w_mae_pro.email1
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE FIELD userid 
    -- Verificando si no es ingreso
    IF (operacion=2) THEN
       NEXT FIELD nomusr
    END IF 

   AFTER FIELD userid
    --Verificando usuario
    IF (LENGTH(w_mae_pro.userid)=0) THEN
       ERROR "Error: codigo del usuario invalido, VERIFICA"
       LET w_mae_pro.userid = NULL
       CLEAR userid
       NEXT FIELD userid
    END IF

    -- Verificando si el usuario existe registrado
    INITIALIZE w_mae_usr.* TO NULL
    CALL librut003_busuario(w_mae_pro.userid)
    RETURNING w_mae_usr.*,existe
    IF existe THEN
       ERROR "Error: usuario ya existe registrado, VERIFICA"
       NEXT FIELD userid
    END IF 

   AFTER FIELD nomusr  
    --Verificando nombre del usuario
    IF (LENGTH(w_mae_pro.nomusr)=0) THEN
       ERROR "Error: nombre del usuario invalido, VERIFICA"
       LET w_mae_pro.nomusr = NULL
       NEXT FIELD nomusr  
    END IF

    -- Verificando que no exista otro usuario con el mismo nombre
    SELECT UNIQUE (a.userid)
     FROM  glb_usuarios a
     WHERE (a.userid != w_mae_pro.userid) 
       AND (a.nomusr  = w_mae_pro.nomusr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro usuario con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomusr
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.userid IS NULL THEN 
       NEXT FIELD userid
    END IF
    IF w_mae_pro.nomusr IS NULL THEN 
       NEXT FIELD nomusr
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae013_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando usuario
    CALL invmae013_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae013_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un usuario

FUNCTION invmae013_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando usuario ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_usuarios   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_usuarios
   SET    glb_usuarios.*      = w_mae_pro.*
   WHERE  glb_usuarios.userid = w_mae_pro.userid 

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando usuarios
   DELETE FROM glb_usuarios 
   WHERE (glb_usuarios.userid = w_mae_pro.userid)

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae013_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae013_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.usuaid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.nomusr THRU w_mae_pro.email1
 DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.usuaid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
