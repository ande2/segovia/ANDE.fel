{
invqbe013.4gl 
Mynor Ramirez
Mantenimiento de usuarios 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "invglo013.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe013_usuarios(operacion)

 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL 
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae013_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.userid,a.nomusr,a.email1,a.usuaid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae013_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.userid,a.nomusr,a.email1 ",
                 " FROM glb_usuarios a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_usuarios SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_usuarios.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_usuarios INTO v_usuarios[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_usuarios
   FREE  c_usuarios
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_usuarios TO s_usuarios.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae013_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae013_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae013_usuarios(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe013_datos(v_usuarios[ARR_CURR()].tuserid)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF invmae013_usuarios(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe013_datos(v_usuarios[ARR_CURR()].tuserid)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res =  invqbe013_integridad()
      IF (res>0) THEN
         -- Evaluando mensaje
         CASE (res)
          WHEN 1 LET msg = "Este usuario ya tiene un perfil asignado. \nUsuario no puede borrarse."
          WHEN 2 LET msg = "Este usuario ya tiene permisos a bodegas. \nUsuario no puede borrarse."
          WHEN 3 LET msg = "Este usuario ya tiene permisos a tipos de movimiento. \nUsuario no puede borrarse."
         END CASE 
      
         CALL fgl_winmessage(" Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de "||rotulo CLIPPED||" este usuario ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae013_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Usuario"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Direccion de Correo"
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM glb_usuarios a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Usuarios.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 
      
     BEFORE ROW 
      -- Asignando contadores del vector
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe013_datos(v_usuarios[1].tuserid)
      ELSE
         CALL invqbe013_datos(v_usuarios[ARR_CURR()].tuserid)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen usuarios con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe013_datos(wuserid)
 DEFINE wuserid   LIKE glb_usuarios.userid,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_usuarios a "||
              "WHERE a.userid = '"||wuserid||"'"||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_usuariost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_usuariost INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomusr THRU w_mae_pro.email1 
  DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.usuaid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_usuariost
 FREE  c_usuariost

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomusr THRU w_mae_pro.email1 
 DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.usuaid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si el usuario ya tiene movimientos  

FUNCTION invqbe013_integridad()
 DEFINE conteo SMALLINT

 -- Verificando roles
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_userrole a
  WHERE (a.userid = w_mae_pro.userid) 
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando permisos a bodegas
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_permxbod a
      WHERE (a.userid = w_mae_pro.userid)
      IF (conteo>0) THEN 
         RETURN 2
      ELSE
         -- Verificando permisos a tipos de movimiento
         SELECT COUNT(*)
          INTO  conteo
          FROM  inv_permxtmv a
          WHERE (a.userid = w_mae_pro.userid)
          IF (conteo>0) THEN 
             RETURN 3
          ELSE
             RETURN 0 
          END IF 
      END IF 
  END IF 
END FUNCTION 
