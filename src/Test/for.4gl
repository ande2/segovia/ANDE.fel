MAIN
DEFINE lg SMALLINT 
DEFINE fr STRING 
DEFINE ar DYNAMIC ARRAY OF STRING
DEFINE i SMALLINT 

   --LET fr = 'Toldo galvanizado 3X6 mts. con paredes, color verde olivo.'
   LET fr = 'Toldo gavanizado 3X6 mts. con paredes, color verde olivo.' --sunap ruebadelaseparacion'
   LET lg = 55 -- 55

   LET ar = convierte(fr,lg)

   DISPLAY "Original ", fr
   FOR i = 1 TO ar.getLength()
      DISPLAY "Linea ", i, "  ", ar[i]
   END FOR 
   
END MAIN 

FUNCTION convierte(txt,lg)
DEFINE lg SMALLINT 
DEFINE txt, res STRING 
DEFINE a DYNAMIC ARRAY OF STRING
DEFINE ini, fin, i  SMALLINT 

   -- 0. Definir posicion inicial y final
   LET i = 0
   LET ini = 1
   LET fin = lg
   IF fin > txt.getlength() THEN LET fin = txt.getlength() END IF
   
   WHILE TRUE 
       
      LET i = i + 1
      CALL separa(txt, ini, fin, lg) RETURNING res, ini
      LET a[i] = res
      LET fin = ini + lg
      IF fin > txt.getlength() THEN LET fin = txt.getlength() END IF 
      IF ini > txt.getlength() THEN EXIT WHILE END IF
      
   END WHILE  
   
   RETURN a

END FUNCTION 

FUNCTION separa(txt, ini, fin, lg)
DEFINE txt, res, res2 STRING
DEFINE ini, fin, lg SMALLINT
DEFINE i, flag, ini2 SMALLINT  

-- 1. Puede ser que no vaya ningun espacio
--    Corta la palabra hasta donde llegue

   LET flag = 0 
   FOR i = ini TO fin
      IF txt.getCharAt(i) = ' ' THEN LET flag = 1 EXIT FOR END IF 
   END FOR
   IF flag = 0 THEN 
      LET res = txt.subString(ini,fin)
   END IF 

-- 2. Si es la ultima linea
   IF fin >= txt.getLength() THEN 
      LET res = txt.subString(ini,fin)
   ELSE 
   -- 3. valida que termina en una palabra
   --    a) La ultima posición sea espacio
   --    b) O que largo de linea + 1 sea espacio
   --    En ese caso al arreglo le asigna a la linea completa

      IF txt.getCharAt(fin) = ' ' OR txt.getCharAt(fin+1) = ' ' THEN
         LET res = txt.subString(ini,fin)
      ELSE --busca el primer espacio de atras para adelante
         FOR i = fin TO ini STEP -1
            IF txt.getCharAt(i) = ' ' THEN
               LET fin = i-1
               LET res = txt.subString(ini,fin)
               EXIT FOR 
            END IF 
         END FOR 
      END IF 
   END IF

   -- 4. Quitando espacios al inicio
   FOR i = 1 TO res.getLength()
      IF res.getCharAt(i) = ' ' THEN 
         LET ini2 = i+1 
      ELSE 
         LET ini2 = i
         EXIT FOR 
      END IF 
   END FOR 
   LET res2 = res.subString(ini2,res.getLength())
   RETURN res2, fin+1 

END FUNCTION 
