MAIN 
  DEFINE  txt, txtj      STRING 
  let txt = "Esta es una  prueba de justificación de texto"
  LET txtj = justifical(txt,55)
  display "txt  ", txt
  display "txtj ", txtj, " tot ", txtj.getLength()
end MAIN


# Recibe un string de caracteres y devuelve un segundo string justificado
# Recibe como parámetro el string a justificar y el largo de linea
# Retorna el string justificado
FUNCTION justifical(str,ll)
DEFINE str      STRING 
DEFINE ll       SMALLINT 
DEFINE ts,i, ini, j, salir     SMALLINT 

  IF LENGTH(str) = ll THEN 
     GOTO _regresa
  END IF 

   LET ts = ll - LENGTH(str)
   LET j = 0
   WHILE ts > 0 
      LET ini = 1
      LET salir = 0
      LET j = j + 1
      WHILE TRUE  
         --buscar espacio
         FOR i = ini TO LENGTH(str)

            IF str.getCharAt(i) = " " THEN 

               LET str = str.subString(1,i), " ", str.subString(i+1,str.getLength())
               LET ts = ts - 1
               IF ts = 0 THEN LET salir = 1 END IF 
               LET ini = i + j + 1
               EXIT FOR 
            ELSE 
               IF i = LENGTH(str) THEN LET salir=1 END IF 
            END IF 
         END FOR 

         IF salir = 1 THEN EXIT WHILE END IF
         
   END WHILE 
 END WHILE   

  LABEL _regresa:

  RETURN str
END FUNCTION 

