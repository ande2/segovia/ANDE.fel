main
define arr dynamic array of string
define var char(1300)
define i smallint

--let var = 'Napole�n I Bonaparte (Ajaccio, 15 de agosto de 1769-Longwood, 5 de mayo de 1821) fue un militar y estadista franc�s, general republicano durante la Revoluci�n y el Directorio, art�fice del golpe de Estado del 18 de brumario que lo convirti� en primer c�nsul (Premier C�nsul) de la Rep�blica el 11 de noviembre de 1799; c�nsul vitalicio desde el 2 de agosto de 1802 hasta su proclamaci�n como emperador de los franceses (Empereur des Fran�ais) el 18 de mayo de 1804, y fue coronado el 2 de diciembre; fue proclamado rey de Italia el 18 de marzo de 1805 y coronado el 26 de mayo. Ostent� ambos t�tulos hasta el 11 de abril de 1814 y, desde el 20 de marzo hasta el 22 de junio de 1815.
--Durante poco m�s de una d�cada, tom� el control de casi toda Europa Occidental y Central mediante una serie de conquistas y alianzas. Solo tras su derrota en la batalla de las Naciones, cerca de Leipzig, en octubre de 1813, se vio obligado a abdicar meses m�s tarde. Regres� a Francia y al poder durante el periodo llamado los Cien D�as y fue derrotado para siempre en la batalla de Waterloo en B�lgica, el 18 de junio de 1815, cuando fue desterrado por los brit�nicos en la isla de Santa Elena, donde falleci�.'

LET var = "Esta es una descripcion especial para una factura que usualmente esta asociada a una orden de compra, ya que la orden de compra tiene una descripcion y el usuario desea esa misma descripion"
let arr = lconvierte(var,55)

display "String ", var
for i = 1 to arr.getLength()
   display "Linea ", i using "<<<", "---> ",arr[i], " largo-> ", arr[i].getLength()
end for
end main

# Recibe como parametro un string de caracteres largo y devuelve un arreglo
# con tantas lineas como se necesite para contener el string recibido
# Recibe como parametro el lago de linea del arreglo que retorna
function lconvierte(lvar,lg)
define lvar     string  --string a dividir
define lg       smallint --largo de linea
define larr     dynamic array of string --array resultado
define ini,fin,i,j,lv   smallint

let lv  = lvar.getlength()

display "Largo del string ", lv

--Si es menor o igual al largo de linea
if lv <= lg then
   let larr[1] = lvar clipped
   goto _salir
end if

--Mas de una linea
let ini = 1
let fin = lg
let lv  = lvar.getlength()
let i   = 1

while true

  if lvar.getCharAt(fin) = ' ' then
     let larr[i] = justifical(lvar.subString(ini,fin),lg)
  else
     for j = fin to ini step - 1
       let fin = fin - 1
       if lvar.getCharAt(fin) = ' ' then
          let larr[i] = justifical(lvar.subString(ini,fin),lg)
          --let larr[i] = lvar.subString(ini,fin)
          exit for
       end if

     end for
  end if

  let i = i + 1
  let ini = fin + 1
  let fin = ini + lg
  if ini > lv then
     exit while
  end if
--DISPLAY "ini ", ini, " lv ", lv
DISPLAY "lin ", i, "-> ", larr[i]
end while

label _salir:

while true
  if length(larr[larr.getLength( )]) = 0 then
     call larr.deleteElement(larr.getLength( ))
  else
     exit while
  end if
end while

return larr

end FUNCTION


FUNCTION justifical(str,ll)
DEFINE str      STRING 
DEFINE ll       SMALLINT 
DEFINE ts,i, ini, j, salir     SMALLINT 

  IF LENGTH(str) = ll THEN 
     GOTO _regresa
  END IF 

   LET ts = ll - LENGTH(str)
   LET j = 0
   WHILE ts > 0 
      LET ini = 1
      LET salir = 0
      LET j = j + 1
      WHILE TRUE  
         --buscar espacio
         FOR i = ini TO LENGTH(str)

            IF str.getCharAt(i) = " " THEN 

               LET str = str.subString(1,i), " ", str.subString(i+1,str.getLength())
               LET ts = ts - 1
               IF ts = 0 THEN LET salir = 1 END IF 
               LET ini = i + j + 1
               EXIT FOR 
            ELSE 
               IF i = LENGTH(str) THEN LET salir=1 END IF 
            END IF 
         END FOR 

         IF salir = 1 THEN EXIT WHILE END IF
         
   END WHILE 
 END WHILE   

  LABEL _regresa:

  RETURN str
END FUNCTION 