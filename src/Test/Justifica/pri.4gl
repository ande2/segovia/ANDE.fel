main
define arr, narr dynamic array of string 
define var char(1300) 
define i SMALLINT

  {LET var = "Napoleon I Bonaparte (Ajaccio, 15 de agosto de 1769-Longwood, 5 de mayo de 1821) fue un militar y estadista frances, general",
      " republicano durante la Revolucion y el Directorio, arti�fice del golpe de Estado del 18 de brumario que lo convirtio en primer consul",
      " (Premier Consul) de la Republica el 11 de noviembre de 1799; consul vitalicio desde el 2 de agosto de 1802 hasta su proclamacion como",
      " emperador de los franceses (Empereur des Francais) el 18 de mayo de 1804, y fue coronado el 2 de diciembre; fue proclamado rey de Italia",
      " el 18 de marzo de 1805 y coronado el 26 de mayo. Ostento ambos ti�tulos hasta el 11 de abril de 1814 y, desde el 20 de marzo hasta el 22 ",
      "de junio de 1815.",
      "Durante poco mas de una decada, tomo el control de casi toda Europa Occidental y Central mediante una serie de conquistas y alianzas.",
      " Solo tras su derrota en la batalla de las Naciones, cerca de Leipzig, en octubre de 1813, se vio obligado a abdicar meses mas tarde. ",
      "Regreso a Francia y al poder durante el periodo llamado los Cien Dias y fue derrotado para siempre en la batalla de Waterloo en Belgica,",
      " el 18 de junio de 1815, cuando fue desterrado por los britanicos en la isla de Santa Elena, donde fallecio."}

   --LET var = "Esta es una prueba de justificacion utilizando la nueva funcion"
   LET var="Esta es una descripcion especial para una factura que usualmente esta asociada a una orden de compra, ya que la orden de compra tiene una descripcion y el usuario desea esa misma descripion"
   LET arr = convierte(var,55)

   --display "String ", var
   FOR i = 1 TO arr.getLength()
   --display "Linea ", i using "<<<", "---> ", arr[i], " --> ", arr[i].getLength() 
   LET narr[i] = justifical(arr[i],55)
   --display "Linea ", i using "<<<", "---> ", narr[i], " --> ", arr[i].getLength() 
   DISPLAY narr[i]
END FOR  
end MAIN




# Recibe como parametro un string de caracteres largo y devuelve un arreglo
# con tantas lineas como se necesite para contener el string recibido
# Recibe como parametro el lago de linea del arreglo que retorna
FUNCTION lconvierte(lvar,lg)
DEFINE lvar             STRING  	--string a dividir
DEFINE lg	            SMALLINT  --largo de linea
DEFINE larr 	         DYNAMIC ARRAY OF STRING  --array resultado
DEFINE ini,fin,i,j,lv 	SMALLINT

LET lv  = lvar.getlength()

--display "Largo del string ", lv

--Si es menor o igual al largo de linea
IF lv <= lg THEN  
   LET larr[1] = lvar CLIPPED  
   GOTO _salir
END IF  

--Mas de una linea
LET ini = 1
LET fin = lg
LET lv  = lvar.getlength()
LET i   = 1

WHILE TRUE 

  IF lvar.getCharAt(fin) = ' ' THEN  
     LET larr[i] = lvar.subString(ini,fin) --justifical(lvar.subString(ini,fin),lg)
  ELSE  
     FOR j = fin TO ini STEP - 1
       LET fin = fin - 1
       IF lvar.getCharAt(fin) = ' ' THEN
          
          LET larr[i] = lvar.subString(ini,fin) --justifical(lvar.subString(ini,fin),lg)
          EXIT FOR  
       END IF  
       
     END FOR   
  END IF  

  LET i = i + 1
  LET ini = fin + 1
  LET fin = ini + lg
  IF ini > lv THEN  
     EXIT WHILE 
  END IF   

END WHILE 

LABEL _salir:

WHILE TRUE 
  IF LENGTH(larr[larr.getLength( )]) = 0 THEN  
     CALL larr.deleteElement(larr.getLength( )) 
  ELSE  
     EXIT WHILE 
  END IF  
END WHILE 

RETURN larr

END FUNCTION  

# Recibe un string de caracteres y devuelve un segundo string justificado
# Recibe como parámetro el string a justificar y el largo de linea 
# Retorna el string justificado
FUNCTION ljustifical(str,ll)
DEFINE str      STRING 
DEFINE ll       SMALLINT 
DEFINE ts,i, ini, j, salir     SMALLINT 

  IF LENGTH(str) = ll THEN 
     GOTO _regresa
  END IF 

   LET ts = ll - LENGTH(str)
   LET j = 0
   WHILE ts > 0 
      LET ini = 1
      LET salir = 0
      LET j = j + 1
      WHILE TRUE  
         --buscar espacio
         FOR i = ini TO LENGTH(str)
            IF str.getCharAt(i) = " " THEN 
               LET str = str.subString(1,i), " ", str.subString(i+1,str.getLength())
               LET ts = ts - 1
               IF ts = 0 THEN LET salir = 1 END IF 
               LET ini = i + j + 1
               EXIT FOR 
            ELSE 
               IF i = LENGTH(str) THEN LET salir=1 END IF 
            END IF 
         END FOR 
         IF salir = 1 THEN EXIT WHILE END IF
   END WHILE 
 END WHILE   

  LABEL _regresa:

  RETURN str
END FUNCTION 
