database segovia

MAIN 
  define idrow, corr, old, i, j, k  int
  declare ccur cursor for 
    select rowid, correlativo from factura_log order by 2

  let i = 0
  let j = 0
  let k = 0
  foreach ccur INTO idrow, corr
    --display "Procesando ", corr
    if i = 0 then 
       let i = 1
       let old = corr
    else 
       if old = corr then 
          let j = j + 1
          display "Repetido ", idrow, "-", corr, " van ", j
          DELETE FROM factura_log WHERE rowid = idrow
          IF sqlca.sqlcode = 0 THEN DISPLAY "Eliminado" ELSE DISPLAY "Error ", sqlca.sqlcode END IF 
       else  
          let old = corr
          let k = k + 1
         -- display "No es repetido ", corr, " van ", k
       end if 
    end if

  end FOREACH
  DISPLAY "Repetidos ", j
  DISPLAY "No repetidos ", k
END MAIN 
