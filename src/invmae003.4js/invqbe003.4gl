{
invqbe003.4gl 
Mynor Ramirez
Mantenimiento de proveedores de producto 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "invglo003.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe003_proveedores(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

 -- Obteniendo datos de la window actual
 LET w = ui.Window.getCurrent()
 LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL 
  CASE (operacion)
   WHEN 3 LET rotulo  = "Borrar" 
  END CASE

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae003_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codprv,a.nomprv,a.codabr,a.numnit,a.numtel,a.numfax,
                                a.dirprv,a.nomcon,a.bemail,a.tipprv,a.codpai,a.observ,
                                a.userid,a.fecsis,a.horsis
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL invmae003_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codprv,a.nomprv,a.codabr,b.nompai ",
                 " FROM inv_provedrs a,glb_mtpaises b ",
                 " WHERE b.codpai = a.codpai AND ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_provedrs SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_provedrs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_provedrs INTO v_provedrs[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_provedrs
   FREE  c_provedrs
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_provedrs TO s_provedrs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae003_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL invmae003_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF invmae003_proveedores(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe003_datos(v_provedrs[ARR_CURR()].tcodprv)
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN
       IF invmae003_proveedores(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe003_datos(v_provedrs[ARR_CURR()].tcodprv)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe003_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este proveedor existe en algun producto. Proveedor no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este proveedor ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae003_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Proveedor"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Codigo Abreviado"
      LET arrcols[4]  = "Numero de NIT"
      LET arrcols[5]  = "Numero Telefono"
      LET arrcols[6]  = "Numero FAX"
      LET arrcols[7]  = "Direccion"
      LET arrcols[8]  = "Nombre Contacto"
      LET arrcols[9]  = "Direccion Email"
      LET arrcols[10] = "Tipo Proveedor"
      LET arrcols[11] = "Pais Origen"
      LET arrcols[12] = "Observaciones"
      LET arrcols[13] = "Usuario Registro"
      LET arrcols[14] = "Fecha Registro"
      LET arrcols[15] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.codprv,TRIM(a.nomprv),a.codabr,a.numnit,a.numtel,a.numfax,TRIM(a.dirprv),",
                              "a.nomcon,a.bemail,CASE (a.tipprv) WHEN 1 THEN 'LOCAL' WHEN 2 THEN 'EXTRANJERO' END,",
                              "b.nompai,TRIM(a.observ),a.userid,a.fecsis,a.horsis",
                       " FROM inv_provedrs a,glb_mtpaises b ",
                       " WHERE b.codpai = a.codpai AND ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Proveedores.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 
      
     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe003_datos(v_provedrs[1].tcodprv)
      ELSE
         CALL invqbe003_datos(v_provedrs[ARR_CURR()].tcodprv)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen proveedores con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe003_datos(wcodprv)
 DEFINE wcodprv LIKE inv_provedrs.codprv,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_provedrs a "||
              "WHERE a.codprv = "||wcodprv||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_provedrst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_provedrst INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomprv THRU w_mae_pro.observ
  DISPLAY BY NAME w_mae_pro.codprv,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
 END FOREACH
 CLOSE c_provedrst
 FREE  c_provedrst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomprv THRU w_mae_pro.observ
 DISPLAY BY NAME w_mae_pro.codprv,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION 

{ Subrutina para verificar si el proveedor ya existe en algun producto }

FUNCTION invqbe003_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a
  WHERE (a.codprv = w_mae_pro.codprv) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
