{
invmae003.4gl 
Mynor Ramirez
Mantenimiento de proveedores de producto 
}

-- Definicion de variables globales 

GLOBALS "invglo003.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae003_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae003_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae003a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae003",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Vargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Menu de opciones
  MENU " Proveedors"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de proveedores."
    CALL invqbe003_proveedores(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo proveedor."
    LET savedata = invmae003_proveedores(1) 
   COMMAND "Modificar"
    " Modificacion de un proveedor existente."
    CALL invqbe003_proveedores(2) 
   COMMAND "Borrar"
    " Eliminacion de un proveedor existente."
    CALL invqbe003_proveedores(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae003_proveedores(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae003_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomprv,
                w_mae_pro.codabr, 
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.dirprv,
                w_mae_pro.nomcon,
                w_mae_pro.bemail,
                w_mae_pro.tipprv,
                w_mae_pro.codpai,
                w_mae_pro.observ 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si proveedor ya existe en algun producto no se puede modificar el campo de codabr
    IF (operacion=2) THEN -- Modificar
     IF invqbe003_integridad() THEN
        -- Desabilitando campo
        CALL Dialog.SetFieldActive("codabr",FALSE) 
     ELSE 
        -- Habilitando campo
        CALL Dialog.SetFieldActive("codabr",TRUE) 
     END IF
    END IF 

   AFTER FIELD nomprv  
    --Verificando nombre de la proveedor
    IF (LENGTH(w_mae_pro.nomprv)=0) THEN
       ERROR "Error: nombre del proveedor invalida, VERIFICA"
       LET w_mae_pro.nomprv = NULL
       NEXT FIELD nomprv  
    END IF

    -- Verificando que no exista otro proveedor con el mismo nombre
    SELECT UNIQUE (a.codprv)
     FROM  inv_provedrs a
     WHERE (a.codprv != w_mae_pro.codprv) 
       AND (a.nomprv  = w_mae_pro.nomprv) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro proveedor con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomprv
     END IF 

   AFTER FIELD codabr  
    --Verificando el codigo abreviado
    IF (LENGTH(w_mae_pro.codabr)=0) THEN
       ERROR "Error: codigo abreviado invalida, VERIFICA"
       LET w_mae_pro.codabr = NULL
       NEXT FIELD codabr  
    END IF

    -- Verificando que no exista otro codigo abreviado
    SELECT UNIQUE (a.codabr)
     FROM  inv_provedrs a
     WHERE (a.codprv != w_mae_pro.codprv) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro proveedor con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr
     END IF 

   AFTER FIELD numnit  
    --Verificando numero de nit 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       ERROR "Error: numero de NIT invalida, VERIFICA"
       LET w_mae_pro.numnit = NULL
       NEXT FIELD numnit  
    END IF

    -- Verificando que no exista otro NIT                      
    SELECT UNIQUE (a.numnit)
     FROM  inv_provedrs a
     WHERE (a.codprv != w_mae_pro.codprv) 
       AND (a.numnit  = w_mae_pro.numnit) 
       AND (a.numnit  != "N/A") 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro proveedor con el mismo numero de NIT, VERIFICA ...",
        "information")
        NEXT FIELD numnit
     END IF 

   AFTER FIELD dirprv
    --Verificando direccion
    IF (LENGTH(w_mae_pro.dirprv)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA"
       LET w_mae_pro.dirprv = NULL
       NEXT FIELD dirprv  
    END IF

   AFTER FIELD tipprv
    --Verificando tipo de proveedor
    IF w_mae_pro.tipprv IS NULL THEN
       NEXT FIELD tipprv 
    END IF

   AFTER FIELD codpai
    --Verificando pais
    IF w_mae_pro.codpai IS NULL THEN
       ERROR "Error: pais de origen invalido, VERIFICA"
       NEXT FIELD codpai  
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomprv IS NULL THEN 
       NEXT FIELD nomprv
    END IF
    IF w_mae_pro.codabr IS NULL THEN 
       NEXT FIELD codabr
    END IF
    IF w_mae_pro.numnit IS NULL THEN 
       NEXT FIELD numnit
    END IF
    IF w_mae_pro.dirprv IS NULL THEN 
       NEXT FIELD dirprv 
    END IF
    IF w_mae_pro.tipprv IS NULL THEN 
       NEXT FIELD tipprv 
    END IF
    IF w_mae_pro.codpai IS NULL THEN 
       NEXT FIELD codpai 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae003_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando proveedor
    CALL invmae003_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae003_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un proveedor

FUNCTION invmae003_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando proveedor ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codprv),0)
    INTO  w_mae_pro.codprv
    FROM  inv_provedrs a
    IF (w_mae_pro.codprv IS NULL) THEN
       LET w_mae_pro.codprv = 1
    ELSE 
       LET w_mae_pro.codprv = (w_mae_pro.codprv+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_provedrs   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codprv 

   --Asignando el mensaje 
   LET msg = "Proveedor (",w_mae_pro.codprv USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_provedrs
   SET    inv_provedrs.*        = w_mae_pro.*
   WHERE  inv_provedrs.codprv = w_mae_pro.codprv 

   --Asignando el mensaje 
   LET msg = "Proveedor (",w_mae_pro.codprv USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando proveedores
   DELETE FROM inv_provedrs 
   WHERE (inv_provedrs.codprv = w_mae_pro.codprv)

   --Asignando el mensaje 
   LET msg = "Proveedor (",w_mae_pro.codprv USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae003_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae003_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codprv = 0 
   LET w_mae_pro.tipprv = 1  
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codprv,w_mae_pro.nomprv THRU w_mae_pro.observ
 DISPLAY BY NAME w_mae_pro.codprv,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
