{
Fecha    : Diciembre 2010  
Programo : Mynor Ramirez
Objetivo : Reporte comparativo de inventario 
}

DATABASE segovia

-- Definicion de variables globales }
GLOBALS "invrep008_globals.4gl"


-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 --Fecha de Inventario Fisico
 LET lfechafisico = "10/05/2012"  

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar4")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep008.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep008_comparativo()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep008_comparativo()
DEFINE 
	qrytxt    		CHAR(80),
	wpais     		VARCHAR(255),
	loop      		SMALLINT

	-- Abriendo la ventana para el reporte
	OPEN WINDOW wrep008 AT 5,2  
	WITH FORM "invrep008" ATTRIBUTE(BORDER)

	-- Definiendo nivel de aislamiento
	SET ISOLATION TO DIRTY READ

	-- Desplegando datos del encabezado
	CALL librut003_parametros(1,0) RETURNING existe,wpais
	CALL librut001_header("invrep008",wpais,1)

	-- Llenando combobox
	-- Anios
	--CALL librut003_cbxanio(0)

	LET loop = TRUE
	WHILE loop
		-- Inicializando datos
		CALL invrep008_inival()

		-- Construyendo busqueda
		INPUT 
			BY NAME w_datos.codbod, w_datos.fecinv
			WITHOUT DEFAULTS 
			ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

			ON ACTION salir
				-- Salida
				CALL invrep008_inival()  
				LET loop = FALSE
				EXIT INPUT

			ON ACTION visualizar
				-- Limpiando datos  
				CALL pInventario.clear()

				-- Desplegnado datos
				CALL invrep008_visualizar()
				NEXT FIELD codbod

			ON CHANGE codbod 
				-- Obteniendo datos de la bodega 
				CALL librut003_bbodega(w_datos.codbod) 
				RETURNING w_mae_bod.*,existe 

				-- Limpiando datos  
				CALL pInventario.clear()
				CALL invrep008_limpiadetalle(50)

			ON CHANGE fecinv
				-- Limpiando datos  
				CALL pInventario.clear()
				CALL invrep008_limpiadetalle(50)


			AFTER FIELD codbod 
				IF w_datos.codbod IS NULL THEN
					ERROR "Error: debe de seleccionarse la bodega a listar." 
					NEXT FIELD codbod
				END IF

			AFTER FIELD fecinv 
				IF w_datos.fecinv IS NULL THEN
					ERROR "Error: debe de seleccionarse la fecha a listar." 
					NEXT FIELD fecinv
				END IF

		END INPUT
		
		IF NOT loop THEN
			EXIT WHILE
		END IF
	END WHILE

	CLOSE WINDOW wrep008 
END FUNCTION

-- Subrutina para visualizar el reporte comparativo 

FUNCTION invrep008_visualizar()
 DEFINE w_mae_prg  RECORD LIKE glb_programs.*

 -- Generando el reporte
 CALL invrep008_datos()

 -- Verificando si hay datos
 IF (cntprod=0) THEN
    CALL fgl_winmessage(
    "Atencion",
    "No existen datos con los filtros seleccionados.",
    "information")
    RETURN
 END IF

 -- Desplegando datos
 DISPLAY ARRAY pInventario TO sInventario.*
  ATTRIBUTES (COUNT=cntprod,ACCEPT=FALSE,CANCEL=FALSE)

  ON ACTION filtros
   EXIT DISPLAY

 END DISPLAY
END FUNCTION

-- Subrutina para generar los datos del reporte comparativo

FUNCTION invrep008_datos()
DEFINE 
	wtipsin          SMALLINT, 
	wcodcat          SMALLINT,
	wsubcat          SMALLINT,
	mesant           SMALLINT,
	aniant           SMALLINT,
	wnomcat          CHAR(50),
	wnomsub          CHAR(50),
	strcodcat        STRING,
	strsubcat        STRING,
	strqry           STRING,
	wfecini,wfecfin  DATE,
	lcodemp,
	lcodsuc				INTEGER

 -- Verificando nulos
 IF (w_datos.codbod IS NULL) OR   
    (w_datos.fecinv IS NULL) THEN 
    RETURN
 END IF

 -- Inicio de contador
 LET cntprod = 1


 -- Obteniendo el tipo de saldo INICIAL
 CALL librut003_parametros(3,2)
 RETURNING existe,wtipsin 

 -- Creando selecccion 
 LET strqry = "SELECT  	a.cditem, d.codabr, d.dsitem, a.exican, nvl(b.exican,0), a.codemp, a.codsuc ",
				  "	FROM 	fis_proenbod a, outer(dia_proenbod b), inv_products d ",
				  "	WHERE a.codbod = ", w_datos.codbod,
				  "   AND   b.fecha  = '", w_datos.fecinv, "'",
				  "	AND 	b.codemp = a.codemp",
				  "	AND 	b.codsuc = a.codsuc",
				  "	AND 	b.codbod = a.codbod",
				  "	AND 	b.cditem = a.cditem",
				  "	AND 	d.cditem = a.cditem",
				  "   ORDER BY d.dsitem"

 -- Seleccionando datos
 PREPARE c_prep FROM strqry 
 DECLARE c_prd  CURSOR FOR c_prep

 FOREACH c_prd 
	INTO 	pInventario[cntprod].cditem,
			pInventario[cntprod].codabr,
			pInventario[cntprod].dsitem,
			pInventario[cntprod].invfis,
			pInventario[cntprod].salsis,
			lcodemp,
			lcodsuc

  -- Obteniendo entradas  
  CALL movtosxrango (lcodemp,
                     lcodsuc,
							w_datos.codbod,
						   pInventario[cntprod].cditem,
						   lfechafisico+1,
							w_datos.fecinv,
							1 )
  RETURNING pInventario	[cntprod].entuni

  -- Obteniendo salidas 
  CALL movtosxrango (lcodemp,
                     lcodsuc,
							w_datos.codbod,
						   pInventario[cntprod].cditem,
						   lfechafisico+1,
							w_datos.fecinv,
							0 )
  RETURNING pInventario[cntprod].saluni

  IF pInventario[cntprod].cditem = 152 THEN
	ERROR "Aqui"
  END IF
  
  LET pInventario[cntprod].slduni = pInventario[cntprod].invfis + 
												pInventario[cntprod].entuni +
												pInventario[cntprod].saluni 
  -- Calculando diferencias
  LET pInventario[cntprod].saldif = (	pInventario[cntprod].slduni - 
													pInventario[cntprod].salsis)

  -- Incrementando contador
  LET cntprod = (cntprod+1)
 END FOREACH
 CLOSE c_prd
 FREE  c_prd
 LET cntprod = (cntprod-1)

END FUNCTION

-- Subrutina para inicializar las variables de trabajo }

FUNCTION movtosxrango (lcodemp,
                       lcodsuc,
							  lcodbod,
						     lcditem,
						     lfecini,
							  lfecfin,
							  ltipope )
DEFINE
	lcodemp		LIKE inv_proenbod.codemp,
	lcodsuc		LIKE inv_proenbod.codemp,
	lcodbod		LIKE inv_proenbod.codemp,
	lcditem		LIKE inv_proenbod.codemp,
	lfecini		DATE,
	lfecfin		DATE,
	ltipope		LIKE inv_dtransac.tipope,
	ltotalmovto	LIKE inv_dtransac.opeuni
	LET ltotalmovto = 0
	SELECT 	SUM(opeuni)
		INTO	ltotalmovto
		FROM 	inv_dtransac
		WHERE codemp = lcodemp
		AND 	codsuc = lcodsuc
		AND 	codbod = lcodbod
		AND 	cditem = lcditem
		AND 	tipope = ltipope
		AND 	fecemi between lfecini AND lfecfin
	IF ltotalmovto IS NULL THEN
		LET ltotalmovto = 0
	END IF
	RETURN ltotalmovto
END FUNCTION


FUNCTION invrep008_inival()
 -- Inicializando datos
 INITIALIZE w_datos.* TO NULL

 -- Limpiando detalle de productos
 CALL pInventario.clear()
 CALL invrep008_limpiadetalle(50)

 -- Lllenando  combobox de bodegas x usuario
 CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
 -- Llenando combox de categorias
 CALL librut003_cbxcategorias()   

 -- Desplegando datos 
 CLEAR FORM
END FUNCTION 


-- Subrutina para limpiar el detalle de productos
FUNCTION invrep008_limpiadetalle(cont)
 DEFINE i,cont INT  

 -- Limpiando 
 FOR i = 1 TO cont
  DISPLAY pInventario[i].* TO sInventario[i].*
 END FOR
END FUNCTION
