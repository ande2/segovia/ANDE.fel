{
invqbe009.4gl 
Mynor Ramirez
Mantenimiento de bodegas 
}

{ Definicion de variables globales }

IMPORT FGL fgl_excel

GLOBALS "invglo009.4gl" 
DEFINE totlin,totusr INT, 
       w             ui.Window,
       f             ui.Form


-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe009_bodegas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12)

        --Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN
      
  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN
     LET rotulo  = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae009_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.codbod,a.nombod,a.nomabr,
                                a.chkexi,a.hayfac,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae009_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,b.nomemp,a.codbod,a.nombod ",
                 " FROM inv_mbodegas a,glb_empresas b ",
                 " WHERE b.codemp = a.codemp AND ",qrytext CLIPPED,
                 " ORDER BY 2,3,4 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_bodegas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_mbodegas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_bodegas INTO v_mbodegas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_bodegas
   FREE  c_bodegas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_mbodegas TO s_bodegas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL invmae009_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae009_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae009_bodegas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
         CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
       IF invmae009_bodegas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
          CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe009_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta bodega ya tiene movimientos registrados, no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta bodega ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae009_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      IF (totusr=0) THEN
         ERROR "Atencion: no existen usuarios con accesos autorizados."
      END IF

      -- Desplegando accesos x usuario
      CALL invqbe009_detpermxusr(v_mbodegas[ARR_CURR()].tcodbod)

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Sucursal"
      LET arrcols[3] = "Bodega"
      LET arrcols[4] = "Nombre Bodega" 
      LET arrcols[5] = "Nombree Abreviado" 
      LET arrcols[6] = "Chequea Existencia"
      LET arrcols[7] = "Bodega Factura"
      LET arrcols[8] = "Usuario Registro"
      LET arrcols[9] = "Fecha Registro"
      LET arrcols[10]= "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT b.nomemp,c.nomsuc,a.codbod,a.nombod,a.nomabr,",
                              "CASE (a.chkexi) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE, ",
                              "CASE (a.hayfac) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE, ",
                              " a.userid,a.fecsis,a.horsis ", 
                       " FROM inv_mbodegas a,glb_empresas b,glb_sucsxemp c ",
                       " WHERE b.codemp = a.codemp AND ",
                             " c.codsuc = a.codsuc AND ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      --LET res          = librut002_excelreport("Bodegas",qry,10,1,arrcols)

       -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Bodegas.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe009_datos(v_mbodegas[1].tcodbod)
         CALL invqbe009_permxusr(v_mbodegas[1].tcodbod,1)
      ELSE
         CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
         CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen bodegas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe009_datos(wcodbod)
 DEFINE wcodbod LIKE inv_mbodegas.codbod,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_mbodegas a "||
              "WHERE a.codbod = "||wcodbod||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_bodegast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Lllenando combobox
 CALL librut003_cbxempresas() 

 -- Llenando vector de seleccion
 FOREACH c_bodegast INTO w_mae_pro.*
  -- Lllenando combobox
  CALL librut003_cbxsucursales(w_mae_pro.codemp) 

  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.codsuc,w_mae_pro.nombod THRU w_mae_pro.hayfac 
  DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codbod,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_bodegast
 FREE  c_bodegast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codsuc,w_mae_pro.nombod THRU w_mae_pro.hayfac  
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codbod,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para buscar los accesos a usuarios

FUNCTION invqbe009_permxusr(wcodbod,opcion)
 DEFINE wcodbod   LIKE inv_mbodegas.codbod,
        i,opcion  SMALLINT,
        qrytext   STRING

 -- Inicializando vector de datos
 CALL v_permxusr.clear()
 FOR i = 1 TO 4
  DISPLAY v_permxusr[i].* TO s_permxusr[i].*
 END FOR

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de usuarios con acceso
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis "||
                  "FROM  inv_permxbod a, glb_usuarios b " ||
                  "WHERE a.codbod = "||wcodbod||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC "

  WHEN 2 -- Cargando usuarios para seleccionar acceso
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM inv_permxbod b "||
                                     "WHERE b.codbod = "||wcodbod||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2"
 END CASE

 -- Seleccionando datos
 PREPARE cp1 FROM qrytext
 DECLARE cperm CURSOR FOR cp1
 LET totusr = 1
 FOREACH cperm INTO v_permxusr[totusr].*
  -- Desplegando datos
  IF (totusr<=4) THEN
      DISPLAY v_permxusr[totusr].* TO s_permxusr[totusr].*
  END IF
  LET totusr = (totusr+1)
 END FOREACH
 CLOSE cperm
 FREE  cperm
 LET totusr = (totusr-1)
END FUNCTION

-- Subrutina para visualizar el detalle de accesos a usuarios

FUNCTION invqbe009_detpermxusr(wcodbod)
 DEFINE wcodbod    LIKE inv_mbodegas.codbod,
        loop,opc,i SMALLINT

 -- Desplegando usuarios
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_permxusr TO s_permxusr.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY

   ON ACTION agregarusers
    -- Agregando usuarios
    CALL invqbe009_permxusr(wcodbod,2)
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas usuarios para agregar.\n Todos los usuarios ya tienen acceso.",
        "stop")

       -- Cargando usuarios con accesos
       CALL invqbe009_permxusr(wcodbod,1)
       EXIT DISPLAY
    ELSE
      -- Mostrando campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando usuarios
      INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

       ON ACTION cancel
        -- Salida
        EXIT INPUT

       ON ACTION accept
        -- Verificando si existen usuarios que agregar
        IF NOT invqbe009_hayusuarios(1) THEN 
           ERROR "Atencion: no existen usuarios marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando usuarios seleccionados
        LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK

          -- Guardando accesos
          FOR i = 1 TO totusr
           IF (v_permxusr[i].tcheckb=0) OR
              (v_permxusr[i].tcheckb IS NULL) THEN
              CONTINUE FOR
           END IF

           -- Grabando
           SET LOCK MODE TO WAIT
           INSERT INTO inv_permxbod
           VALUES (wcodbod,
                   v_permxusr[i].tuserid,
                   v_permxusr[i].tusuaid,
                   v_permxusr[i].tfecsis,
                   v_permxusr[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK

          EXIT INPUT
         WHEN 2 -- Modificando
          CONTINUE INPUT
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF
      END INPUT

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando usuarios con accesos
      CALL invqbe009_permxusr(wcodbod,1)
      EXIT DISPLAY
    END IF

   ON ACTION eliminarusers
    -- Eliminando usuarios
    -- Mostrando campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando usuarios
    INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT

     ON ACTION accept
      -- Verificando si existen usuarios que borrar  
      IF NOT invqbe009_hayusuarios(0) THEN 
         ERROR "Atencion: no existen usuarios desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando usuarios seleccionados
      LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK

        -- Guardando accesos
        FOR i = 1 TO totusr
         IF (v_permxusr[i].tcheckb=1) OR
            (v_permxusr[i].tcheckb IS NULL) THEN
            CONTINUE FOR
         END IF

         -- Eliminando
         SET LOCK MODE TO WAIT
         DELETE FROM inv_permxbod
         WHERE inv_permxbod.codbod = wcodbod
           AND inv_permxbod.userid = v_permxusr[i].tuserid
        END FOR

        -- Terminando la transaccion
        COMMIT WORK

        EXIT INPUT
       WHEN 2 -- Modificando
        CONTINUE INPUT
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF
    END INPUT

    -- Escondiendo campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando usuarios con accesos
    CALL invqbe009_permxusr(wcodbod,1)
    EXIT DISPLAY

   BEFORE ROW
    -- Verificando control del total de lineas
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF
  END DISPLAY
 END WHILE
END FUNCTION

-- Subrutina para verificar si existen usuarios seleccionados que agregar o que borrar

FUNCTION invqbe009_hayusuarios(estado)
 DEFINE i,estado,hayusr SMALLINT

 -- Chequeando estado
 LET hayusr = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_permxusr[i].tcheckb=estado) THEN
     LET hayusr = 1
     EXIT FOR
  END IF
 END FOR

 RETURN hayusr
END FUNCTION

-- Subrutina para verificar si la bodega ya tiene algun movimiento registrado

FUNCTION invqbe009_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.codbod = w_mae_pro.codbod) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
