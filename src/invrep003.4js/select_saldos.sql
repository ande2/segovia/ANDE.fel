SELECT a.dsitem , c.nombod , SUM(b.opeuni) saldo_t
FROM inv_products a , inv_dtransac b , inv_mbodegas c
WHERE a.cditem = b.cditem
AND   b.codbod = c.codbod
GROUP BY 1 , 2
ORDER BY 2,1
INTO TEMP saldos
