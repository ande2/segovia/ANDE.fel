{ 
invqbe018.4gl 
Mynor Ramirez
Mantenimiento de paises 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "invglo018.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe018_paises(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN
  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN
     LET rotulo  = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae018_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codpai,a.nompai,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL invmae018_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codpai,a.nompai ",
                 " FROM glb_mtpaises a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_paispro SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_paises.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_paispro INTO v_paises[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_paispro
   FREE  c_paispro
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_paises TO s_paispro.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL invmae018_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL invmae018_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF invmae018_paises(2) THEN
         EXIT DISPLAY 
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
       IF invmae018_paises(2) THEN
          EXIT DISPLAY 
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe018_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este pais existe en algun proveedor. \n Pais no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta pais ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae018_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Pais"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Usuario Registro"
      LET arrcols[4] = "Fecha Registro"
      LET arrcols[5] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM glb_mtpaises a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Paises.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 

     BEFORE ROW 
      -- Asignando contadores del vector
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe018_datos(v_paises[1].tcodpai)
      ELSE
         CALL invqbe018_datos(v_paises[ARR_CURR()].tcodpai)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen paises con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION invqbe018_datos(wcodpai)
 DEFINE wcodpai LIKE glb_mtpaises.codpai,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.codpai,a.nompai,a.userid,a.fecsis,a.horsis "||
              "FROM  glb_mtpaises a "||
              "WHERE a.codpai = "||wcodpai||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_paisprot SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_paisprot INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nompai
  DISPLAY BY NAME w_mae_pro.codpai,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
 END FOREACH
 CLOSE c_paisprot
 FREE  c_paisprot

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nompai
 DISPLAY BY NAME w_mae_pro.codpai,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION 

-- Subrutina para verificar si el pais ya existe en algun proveedoro 

FUNCTION invqbe018_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_provedrs a 
  WHERE (a.codpai = w_mae_pro.codpai) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
