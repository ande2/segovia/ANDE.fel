{
facqbe003.4gl 
Mynor Ramirez
Mantenimiento de tipos de documento x punto de venta
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "facglo003.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe003_tipos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_tdocxpos.estado, 
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae003_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numpos,a.codemp,a.tipdoc,a.nserie,a.numcor,a.estado,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae003_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.lnktdc,b.nompos,c.nomemp,d.nomdoc,a.nserie,a.estado ",
                  "FROM  fac_tdocxpos a,fac_puntovta b,glb_empresas c,fac_tipodocs d ",
                  "WHERE b.numpos = a.numpos ",
                    "AND c.codemp = a.codemp ",
                    "AND d.tipdoc = a.tipdoc ",
                    "AND ",qrytext CLIPPED,
                  " ORDER BY 1,2,3,4"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_datos1 SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tipodocs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos1 INTO v_tipodocs[totlin].tlnktdc THRU v_tipodocs[totlin].tnserie,westado
    -- Verficando estado
    IF (westado="B") THEN
       LET v_tipodocs[totlin].testado = "mars_bloqueo.png"
    ELSE
       LET v_tipodocs[totlin].testado = "mars_cheque.png"
    END IF 

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos1
   FREE  c_datos1
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tipodocs TO s_tipodocs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae003_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae003_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF facmae003_tipos(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
      END IF 

     ON KEY (CONTROL-M) 
      IF (operacion=2) THEN 
       -- Modificando 
       IF facmae003_tipos(2) THEN
          EXIT DISPLAY 
       ELSE 
         -- Desplegando datos
          CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe003_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de documento ya tiene movimientos. \n VERIFICA resoluciones o ventas realizadas.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este tipo de documento ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
            --  Eliminando
            CALL facmae003_grabar(3)
            EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Punto de Venta" 
      LET arrcols[2] = "Empresa"
      LET arrcols[3] = "Tipo de Documento"
      LET arrcols[4] = "Numero Serie"
      LET arrcols[5] = "Correlativo Inicial"
      LET arrcols[6] = "Estado"
      LET arrcols[7] = "Usuario Registro"
      LET arrcols[8] = "Fecha Registro"
      LET arrcols[9] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT b.nompos,c.nomemp,d.nomdoc,a.nserie,a.numcor,",
                              "CASE (a.estado) WHEN 'A' THEN 'Activo' WHEN 'B' THEN 'Bloqueado' END,",
                              "a.userid,a.fecsis,a.horsis ",
                       " FROM fac_tdocxpos a,fac_puntovta b,glb_empresas c,fac_tipodocs d ",
                       " WHERE b.numpos = a.numpos ",
                          "AND c.codemp = a.codemp ",
                          "AND d.tipdoc = a.tipdoc ",
                          "AND ",qrytext CLIPPED,
                       " ORDER BY 1,2,3,4 "

      -- Ejecutando el reporte
      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Tipos_Documento_x_Punto_de_Venta.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF 
      
     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbe003_datos(v_tipodocs[1].tlnktdc)
      ELSE 
         CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de documento con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe003_datos(wlnktdc)
 DEFINE wlnktdc LIKE fac_tdocxpos.lnktdc,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_tdocxpos a "||
              "WHERE a.lnktdc = "||wlnktdc||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_datos1t SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_datos1t INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.numpos THRU w_mae_pro.estado
  DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_datos1t
 FREE  c_datos1t

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.numpos THRU w_mae_pro.estado
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si el tipo de documento ya tiene movimientos 

FUNCTION facqbe003_integridad()
 DEFINE conteo INTEGER 

 -- Verificando resoluciones
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_impresol a 
  WHERE (a.codemp = w_mae_pro.codemp) 
    AND (a.tipdoc = w_mae_pro.tipdoc) 
    AND (a.nserie = w_mae_pro.nserie) 
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando ventas
     SELECT COUNT(*)
      INTO  conteo
      FROM  fac_mtransac a 
      WHERE (a.numpos = w_mae_pro.numpos) 
        AND (a.codemp = w_mae_pro.codemp) 
        AND (a.tipdoc = w_mae_pro.tipdoc) 
        AND (a.nserie = w_mae_pro.nserie) 
     IF (conteo>0) THEN
        RETURN 1
     ELSE
        RETURN 0 
     END IF
  END IF
END FUNCTION 
