{ 
facglo003.4gl
Mynor Ramirez
Mantenimiento de tipos de documento x punto de venta
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE fac_tdocxpos.*,
       v_tipodocs  DYNAMIC ARRAY OF RECORD
        tlnktdc    LIKE fac_tdocxpos.lnktdc,
        tnompos    CHAR(30),                    
        tnomemp    CHAR(30),
        tnomdoc    CHAR(30),
        tnserie    LIKE fac_tdocxpos.nserie,
        testado    CHAR(20)
       END RECORD
END GLOBALS
