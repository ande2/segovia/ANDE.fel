################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "glbm0003_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO glb_paramtrs ( ",
        " numpar, tippar, nompar, valchr, userid, fecsis, horsis ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE vAndeSesId char(25)

   -- Para tippar
   LET g_reg.tippar = 0
   TRY 
      SELECT MAX(tippar) INTO g_reg.tippar FROM glb_paramtrs WHERE numpar = g_reg.numpar
      IF sqlca.sqlcode = 0 THEN
         LET g_reg.tippar = g_reg.tippar + 1
      ELSE 
         LET g_reg.tippar = 1 
      END IF 
      IF g_reg.tippar IS NULL THEN LET g_reg.tippar = 1 END IF 
    CATCH 
      CALL msgError(sqlca.sqlcode,"Grabarr Registro")
      RETURN FALSE
    END TRY 
    
   --LET g_reg.estado = 1
   LET g_reg.fecsis = TODAY 
   LET g_reg.horsis = CURRENT 

   LET g_reg.userid = get_AndeVarLog("LOGNAME")
   IF g_reg.userid IS NULL THEN
      LET g_reg.userid = "prueba"
   END IF 

   TRY 
      EXECUTE st_insertar USING 
         g_reg.numpar, g_reg.tippar, g_reg.nompar, g_reg.valchr, g_reg.userid, 
         g_reg.fecsis, g_reg.horsis

      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.id_commpue = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 