########################################################################
## Function  : catUsuario()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Usuarios de Catalogos
#########################################################################
SCHEMA face
GLOBALS "menuGlobals.4gl"
FUNCTION catUsuario()
   DEFINE rec RECORD LIKE adm_usu.*
   DEFINE recbac RECORD LIKE adm_usu.*
   DEFINE qry  STRING
   DEFINE cmb  STRING
   DEFINE cmb1  STRING
   DEFINE cmb2  STRING
   DEFINE cmb3  STRING
   DEFINE cmb4  STRING
   DEFINE cmb5  STRING
   DEFINE cmb6  STRING
   DEFINE message  STRING
   DEFINE resp SMALLINT
	DEFINE tit   STRING
   DEFINE vestado_gru LIKE adm_gru.est_id
   DEFINE nombre_jefedep LIKE adm_usu.usu_nom
   DEFINE jefe_ant LIKE adm_usu.usu_nom
	DEFINE where_clause STRING
	DEFINE empresa CHAR(20)

   SELECT a.empr_id
   INTO empresa
   FROM face:mempr a
   WHERE a.empr_db = dbname

	LET cmb = "SELECT a.gru_id, a.gru_nom FROM face:adm_gru a ORDER BY 2 asc"

   LET cmb1 = "SELECT adm_usu.usu_id, adm_usu.usu_nom  FROM face:adm_usu WHERE adm_usu.usu_tip = 1 AND adm_usu.est_id = 13 ORDER BY usu_nom asc"

   LET cmb2 = "SELECT a.dpe_id, a.dpe_desc FROM face:mdepemp a  WHERE est_id<> 14 ORDER BY dpe_desc asc "

   LET cmb3 = "SELECT a.empr_id, a.empr_nom FROM face:mempr a ",
               "WHERE a.empr_id = ", empresa 

   LET cmb4 = "SELECT a.est_id, a.est_desc FROM face:mest a WHERE a.est_id IN(13,14,20) ORDER BY 2 asc"

	LET cmb5 = "SELECT a.usu_id, a.usu_nom FROM face:adm_usu a WHERE a.usu_tip = 1 ORDER BY 2 asc "

   OPEN WINDOW menCatUsuario AT 1,1 WITH FORM "menCatUsuario" 

		LET tit = vempr_nom CLIPPED, " - [C�talogos de Usuarios]"
   	CALL fgl_settitle(tit)

      MENU ""
			BEFORE MENU
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"

				INITIALIZE rec.* TO NULL
				LET recbac.* = rec.*

         ON ACTION buscar

            CLEAR FORM
				SHOW OPTION ALL
				LET recbac.* = rec.*
				LET int_flag = FALSE

				LET vusuario = FGL_GETENV("LOGNAME")
				INITIALIZE rec.* TO NULL 

     			DISPLAY BY NAME rec.usu_id, rec.empr_id,rec.gru_id, 
                          rec.usu_nomunix, rec.usu_nom, rec.usu_tip, 
                          rec.est_id, rec.usu_passw, 
                          rec.dpe_id, rec.usu_tip, rec.usu_jef

            CALL combo_din2("gru_id", cmb)
            CALL combo_din2("est_id", cmb4)
            CALL combo_din2("dpe_id", cmb2)
            CALL combo_din2("empr_id", cmb3)
      		CALL combo_din2("usu_id", cmb)
      		CALL combo_din2("usu_jef", cmb5)

            CONSTRUCT BY NAME where_clause ON usu_id, usu_nomunix, usu_nom, est_id, dpe_id, usu_tip, gru_id, empr_id, usu_jef

					AFTER CONSTRUCT

               IF int_flag = FALSE THEN
						CALL get_fldbuf(usu_id, usu_nomunix,usu_nom, est_id, dpe_id)
						RETURNING rec.usu_id, rec.usu_nomunix, rec.usu_nom, rec.est_id, rec.dpe_id, rec.empr_id, rec.usu_jef
					ELSE
						EXIT CONSTRUCT
					END IF

				END CONSTRUCT

				IF int_flag = FALSE THEN

					LET qry = "SELECT a.* FROM face:adm_usu a WHERE ", where_clause CLIPPED,
                         "ORDER BY 1"

      			PREPARE prpCU1 FROM qry
      			DECLARE curCU1 SCROLL CURSOR WITH HOLD FOR prpCU1

      			OPEN curCU1
      			FETCH FIRST curCU1 INTO rec.*

      			IF STATUS = NOTFOUND THEN
      	   		CALL msg("No existen datos en la tabla")
						HIDE OPTION ALL
						SHOW OPTION "buscar","agregar"
					END IF

      			DISPLAY BY NAME rec.usu_id, rec.empr_id,rec.gru_id, 
                            rec.usu_nomunix, rec.usu_nom, rec.usu_tip, 
                            rec.est_id, rec.usu_passw, 
                            rec.dpe_id, rec.usu_tip, rec.usu_jef
				ELSE

					LET message = "Busqueda Cancelada"
					CALL msg(message)
					LET int_flag = FALSE
					LET rec.* = recbac.*

      			DISPLAY BY NAME rec.usu_id, rec.empr_id,rec.gru_id, 
                            rec.usu_nomunix, rec.usu_nom, rec.usu_tip, 
                            rec.est_id, rec.usu_passw, 
                            rec.dpe_id, rec.usu_tip, rec.usu_jef

				END IF

         ON ACTION agregar
            LET recbac.* = rec.*
            INITIALIZE rec.* TO NULL 

            CALL combo_din2("gru_id", cmb)
            CALL combo_din2("est_id", cmb4)
            CALL combo_din2("dpe_id", cmb2)
            CALL combo_din2("empr_id", cmb3)
				CALL combo_din2("usu_jef", cmb5)

            LET int_flag = FALSE
            LET rec.est_id = 13

            DISPLAY BY NAME rec.usu_id, rec.est_id, rec.usu_nomunix,
                            rec.usu_passw, rec.usu_nom, rec.usu_tip,
                            rec.gru_id, rec.dpe_id, 
									 rec.empr_id, rec.usu_jef

            INPUT BY NAME rec.usu_nomunix, rec.usu_passw, rec.usu_nom, 
                          rec.usu_tip, rec.gru_id, rec.dpe_id, rec.empr_id, 
                          rec.usu_jef  WITHOUT DEFAULTS

               AFTER INPUT

                  IF (NOT int_flag) THEN

                     LET rec.ptb_id = 1
                     LET rec.usu_ext = 0
                     LET rec.usu_nomwin = NULL
                     LET rec.usu_id = 0
							LET rec.usu_jefdep = NULL
							LET rec.usu_gruliq = NULL

                     BEGIN WORK
                     WHENEVER ERROR STOP
                     INSERT INTO face:adm_usu VALUES (rec.*)
                     WHENEVER ERROR CONTINUE
                     IF STATUS = 0 THEN
                        COMMIT WORK
                        CALL msg(cAddOK)
                        CLOSE curCU1
                        OPEN curCU1
                        FETCH LAST curCU1 INTO rec.*

                        DISPLAY BY NAME rec.usu_id,rec.est_id, rec.usu_nomunix, 
                                        rec.usu_passw, rec.usu_nom, 
                                        rec.gru_id, rec.usu_jef, 
                          					 rec.dpe_id, rec.empr_id, rec.usu_tip
                     ELSE
                        ROLLBACK WORK
                        CALL msg(cErr)
                     END IF
                  ELSE
                     CALL msg("Ingreso Cancelado")
                     LET int_flag = TRUE
                     LET rec.* = recbac.*

            			DISPLAY BY NAME rec.usu_id, rec.est_id, rec.usu_nomunix,
                            rec.usu_passw, rec.usu_nom, rec.usu_tip,
                            rec.gru_id, rec.dpe_id, rec.empr_id,
                            rec.usu_jef
                  END IF
            END INPUT
				CLEAR FORM

         ON ACTION actualizar

			  LET recbac.* = rec.*
			  LET int_flag = FALSE

           SELECT a.est_id
           INTO vestado_gru
           FROM face:adm_gru a
           WHERE adm_gru.gru_id = rec.gru_id
			  IF vestado_gru = 14 or vestado_gru = 20 THEN
					LET int_flag = TRUE
			  END IF

         IF rec.est_id <> 20 THEN 
				 IF (NOT int_flag) THEN
            	IF rec.usu_id  <> cRaiz THEN
   					LET cmb = " SELECT a.gru_id, a.gru_nom  ",
									 " FROM face:adm_gru a  ",
									 " WHERE a.est_id = 13 ",
									 " ORDER BY gru_nom asc "

   					LET cmb1 = "SELECT a.usu_id, a.usu_nom ",  
									  " FROM face:adm_usu WHERE a.usu_tip = 1 ",
                             " AND a.est_id = 13  ORDER BY a.usu_nom desc"
   					LET cmb2 = "SELECT a.dpe_id, a.dpe_desc ", 
									  " FROM face:mdepemp a WHERE a.est_id = 13 ORDER BY a.dpe_desc asc"

   					LET cmb3 = "SELECT a.empr_id, a.empr_nom ", 
                             " FROM face:mempr a WHERE a.est_id = 1 ", 
									  " AND  a.empr_id = ", empresa,
                             " ORDER BY empr_id"
						LET cmb4 = 
						          "SELECT a.est_id, a.est_desc ", 
                            "FROM face:mest a ", 
                            "WHERE a.est_id IN(13,14,20) ORDER BY 2 asc"

            		CALL combo_din2("gru_id", cmb)
            		CALL combo_din2("est_id", cmb4)
            		CALL combo_din2("dpe_id", cmb2)
            		CALL combo_din2("empr_id", cmb3)
            		CALL combo_din2("usu_jef", cmb5)

            		INPUT BY NAME rec.est_id, rec.usu_nomunix,rec.usu_passw,
                             rec.usu_nom, rec.usu_tip, 
                             rec.gru_id, rec.dpe_id, rec.usu_jef, rec.empr_id 
									  WITHOUT DEFAULTS

                  	AFTER INPUT
                     	IF (NOT int_flag) THEN

                        	BEGIN WORK
                           LET qry = "UPDATE face:adm_usu a SET a.usu_nomunix = ?, a.usu_passw = ?, a.usu_nom = ?, a.gru_id = ?, a.usu_jef = ?, a.dpe_id = ?, a.empr_id = ?, a.est_id = ?, a.usu_tip = ? WHERE a.usu_id = ?"

                        	PREPARE prpCUU FROM qry
                        	IF SQLCA.SQLCODE < 0 THEN 
                           	display sqlca.sqlcode
                        	END IF

                        	EXECUTE prpCUU USING rec.usu_nomunix, rec.usu_passw, rec.usu_nom, rec.gru_id, rec.usu_jef, rec.dpe_id, rec.empr_id, rec.est_id, rec.usu_tip, rec.usu_id
                        	IF STATUS = 0 THEN
                           	COMMIT WORK
                           	CALL msg(cUpdOK)

                    				DISPLAY BY NAME rec.est_id, rec.usu_nomunix, 
                          			          rec.usu_passw, rec.usu_nom, 
                                 		    rec.gru_id, rec.usu_jef, rec.dpe_id, 
                                 		    rec.empr_id, rec.usu_tip

                        	ELSE
                           	ROLLBACK WORK
                           	CALL msg(cErr)
                        	END IF
                     	ELSE
                        	LET int_flag = TRUE
                     	END IF
               	END INPUT
            	END IF
				ELSE
					IF vestado_gru = 14 THEN
						LET message ="No puede modificar los datos del usuario, grupo deshabilitado"	
					ELSE
						LET message ="No puede modificar los datos del usuario, grupo dado de baja"	
					END IF
					CALL msg(message)
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
				END IF

			ELSE
           	CALL msg("NO PUEDE MODIFICAR EL REGISTRO")
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"
			END IF

         ON ACTION eliminar
			IF rec.est_id <> 20 THEN
            IF rec.usu_id <> cRaiz THEN
               LET resp = FALSE
               LET resp = confirma("Esta seguro que desea dar de baja al usuario?")
               IF resp THEN
                  BEGIN WORK
                  LET rec.est_id = 20
                  LET qry ="UPDATE face:adm_usu a SET a.est_id=? WHERE a.usu_id=?"
                  PREPARE prpCUD FROM qry
                  EXECUTE prpCUD USING rec.est_id, rec.usu_id 
                  IF STATUS = 0 THEN
                     COMMIT WORK
                     CALL msg(cDelOk)
                     CLOSE curCU1
                     OPEN curCU1
                     FETCH FIRST curCU1 INTO rec.*
                    	DISPLAY BY NAME rec.est_id, rec.usu_nomunix, rec.usu_passw,
                                     rec.usu_nom, rec.gru_id, rec.usu_jef, 
                         				 rec.dpe_id, rec.empr_id, rec.usu_tip
                  ELSE
                     CALL msg(cErr)
                     ROLLBACK WORK
                  END IF
               END IF
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF
			ELSE
            CALL msg("NO PUEDE MODIFICAR EL REGISTRO")
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"
			END IF

--va al primer registro
         ON ACTION primero
            FETCH FIRST curCU1 INTO rec.*

           	DISPLAY BY NAME rec.usu_id, rec.est_id, rec.usu_nomunix, 
                            rec.usu_passw, rec.usu_nom, rec.gru_id, 
                            rec.dpe_id, rec.empr_id, rec.usu_tip, 
                            rec.usu_jef

--va al siguiente registro
         ON ACTION siguiente

            FETCH NEXT curCU1 INTO rec.*

				IF SQLCA.SQLCODE = 100 THEN
				   LET message = "Esta posicionado en el ultimo registro"
					CALL msg(message)
				END IF

				SELECT a.*
				INTO rec.*
				FROM face:adm_usu a
				WHERE a.usu_id = rec.usu_id

           	DISPLAY BY NAME rec.usu_id,rec.est_id, rec.usu_nomunix, 
                            rec.usu_passw, rec.usu_nom, rec.gru_id, 
                            rec.dpe_id, rec.empr_id, rec.usu_tip,
                            rec.usu_jef

         ON ACTION anterior
            FETCH PREVIOUS curCU1 INTO rec.*
				IF SQLCA.SQLCODE = 100 THEN
				   LET message = "Esta posicionado en el primer registro"
					CALL msg(message)
				END IF

				SELECT a.*
				INTO rec.*
				FROM face:adm_usu  a
				WHERE a.usu_id = rec.usu_id

           	DISPLAY BY NAME rec.usu_id,rec.est_id, rec.usu_nomunix, 
                            rec.usu_passw, rec.usu_nom, rec.gru_id, 
                            rec.dpe_id, rec.empr_id, rec.usu_tip, 
                            rec.usu_jef

         ON ACTION ultimo
            FETCH LAST curCU1 INTO rec.*

           	DISPLAY BY NAME rec.usu_id, rec.est_id, rec.usu_nomunix, 
                            rec.usu_passw, rec.usu_nom, rec.gru_id, 
                            rec.dpe_id, rec.empr_id, rec.usu_tip, 
                            rec.usu_jef

         COMMAND KEY(INTERRUPT)
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatUsuario
END FUNCTION