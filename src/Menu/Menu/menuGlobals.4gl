SCHEMA segovia
#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
globals
DEFINE gUsuario   RECORD LIKE usuario.*
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operación no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
END globals
