GLOBALS "menm0160_glob.4gl"


FUNCTION PrintReport ( )
DEFINE 
   handler om.SaxDocumentHandler,
   cmd      STRING,  
   nombre   STRING,
   salida   STRING,
   preview  INTEGER,
   accion SMALLINT 

   LET nombre = "menm0160_report1"

   CALL parametrosreporte() RETURNING salida, preview, accion 
   IF accion THEN 
   WHENEVER ERROR CONTINUE
   IF fgl_report_loadCurrentSettings(nombre||".4rp") THEN
      CALL fgl_report_selectDevice(salida)
      CALL fgl_report_selectPreview(preview)
      IF salida <> "Image" THEN
         CALL fgl_report_setOutputFileName("/tmp/"||nombre CLIPPED||"."||salida)
      ELSE
         CALL fgl_report_configureImageDevice(0,0,0,1,NULL,"jpg","/tmp",nombre,300)
      END IF
      LET handler = fgl_report_commitCurrentSettings()
   ELSE
      ERROR "No se encuentra el formato "||nombre CLIPPED ||".4rp"
   END IF
   WHENEVER ERROR STOP
   IF handler IS NOT NULL THEN
      CALL run_report1_to_handler(handler)
   END IF
            
   IF SALIDA = "Image" THEN
      LET cmd="C:\\temp\\"||nombre||".jpg"
      CALL fgl_putfile("/tmp/"||nombre ||".jpg",cmd)
      IF WINSHELLEXEC(cmd) THEN
      END IF
   END IF
  END IF  
END FUNCTION