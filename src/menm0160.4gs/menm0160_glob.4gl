################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################

DATABASE segovia

GLOBALS
TYPE recDet RECORD
   mov_fecha         LIKE vbit.mov_fecha,
   tab_id            LIKE vbit.tab_id,
   tab_nom           LIKE vbit.tab_nom,
   cam_exp           LIKE vbit.cam_exp,
   cam_id            LIKE vbit.cam_id,
   cam_des           LIKE vbit.cam_des,
   cam_vanterior     LIKE vbit.cam_vanterior,
   cam_vactual       LIKE vbit.cam_vactual,
   usu_nom           LIKE vbit.usu_nom
END RECORD
DEFINE
   gr_reg  RECORD
      proc_id           LIKE bproc.proc_id,
      proc_desc         LIKE bproc.proc_desc,
      cam_llavedes      LIKE bcam.cam_des,
      cam_llaveval      LIKE bbit.cam_llaveval,
      llaveval_des      VARCHAR (50)
   END RECORD,
   gr_det DYNAMIC ARRAY OF recDet,
   gr_filtro, ur_filtro RECORD
   mov_fecha         LIKE vbit.mov_fecha,
   fecha             DATE,
   tab_id            LIKE vbit.tab_id,
   cam_exp           LIKE vbit.cam_exp,
   cam_id            LIKE vbit.cam_id,
   cam_vanterior     LIKE vbit.cam_vanterior,
   cam_vactual       LIKE vbit.cam_vactual,
   usu_nom           LIKE vbit.usu_nom
END RECORD,
   lusu_id            	LIKE adm_usu.usu_id,
   dbname 					CHAR(20),
	vempr_nomlog 			CHAR(15),
	usuario 					CHAR(10),
	hr   						DATE, 
	gtit_enc, 
	gtit_pie,
	nombre_depproamento 	VARCHAR(80),
	where_clause 			STRING,
	aui 						om.DOMnode,
	nombre_empresa   		CHAR(50),
	queryReport				STRING,
	lorden					INTEGER
	CONSTANT kiloalibra = 2.2046
	CONSTANT libraakilo = 0.4536
END GLOBALS