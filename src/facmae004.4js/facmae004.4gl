{
facmae004.4gl 
Mynor Ramirez
Mantenimiento de resoluciones
}

-- Definicion de variables globales 

GLOBALS "facglo004.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facmae004.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL facmae004_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae004_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facmae004a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facmae004",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Resoluciones"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de resoluciones existentes."
    CALL facqbe004_resoluciones(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva resolucion."
    LET savedata = facmae004_resoluciones(1) 
   COMMAND "Modificar"
    " Modificacion de una resolucion existente."
    CALL facqbe004_resoluciones(2) 
   COMMAND "Borrar"
    " Eliminacion de una resolucion existente."
    CALL facqbe004_resoluciones(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION facmae004_resoluciones(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae004_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codemp,
                w_mae_pro.tipdoc,
                w_mae_pro.nserie,
                w_mae_pro.nresol, 
                w_mae_pro.fecres, 
                w_mae_pro.numini, 
                w_mae_pro.numfin, 
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si resolucion de docunmento ya tiene movimientos no se puede modificar nombre 
    IF (operacion=2) THEN -- Si es modificacion
     IF facqbe004_integridad() THEN
       CALL Dialog.SetFieldActive("codemp",FALSE)
       CALL Dialog.SetFieldActive("tipdoc",FALSE)
       CALL Dialog.SetFieldActive("nserie",FALSE)
       CALL Dialog.SetFieldActive("nresol",FALSE)
       CALL Dialog.SetFieldActive("fecres",FALSE)
       CALL Dialog.SetFieldActive("numini",FALSE)
       CALL Dialog.SetFieldActive("numfin",FALSE)
     ELSE
       CALL Dialog.SetFieldActive("codemp",TRUE)
       CALL Dialog.SetFieldActive("tipdoc",TRUE)
       CALL Dialog.SetFieldActive("nserie",TRUE)
       CALL Dialog.SetFieldActive("nresol",TRUE)
       CALL Dialog.SetFieldActive("fecres",TRUE) 
       CALL Dialog.SetFieldActive("numini",TRUE)
       CALL Dialog.SetFieldActive("numfin",TRUE)
     END IF
    END IF 

   AFTER FIELD codemp
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       ERROR "Error: empresa invalida, VERIFICA ..."
       NEXT FIELD codemp
    END IF

   AFTER FIELD tipdoc 
    --Verificando tipo de documento
    IF w_mae_pro.tipdoc IS NULL THEN
       ERROR "Error: tipo de documento invalido, VERIFICA ..."
       NEXT FIELD tipdoc
    END IF

   AFTER FIELD nserie
    -- Verificando numero de serie
    IF (LENGTH(w_mae_pro.nserie)<=0) THEN 
       ERROR "Error: numero de serie invalido, VERIFICA ..."
       NEXT FIELD nserie
    END IF 

   AFTER FIELD nresol
    -- Verificando numero de resolucion
    IF (LENGTH(w_mae_pro.nresol)<=0) THEN 
       ERROR "Error: numero de resolucion invalida, VERIFICA ..."
       NEXT FIELD nresol 
    END IF 

   AFTER FIELD fecres
    --Verificando fecha de resolucion
    IF w_mae_pro.fecres IS NULL OR
       (w_mae_pro.fecres >TODAY) THEN 
       ERROR "Error: fecha de resolucion invalida, VERIFICA ..."
       LET w_mae_pro.fecres = NULL
       CLEAR fecres 
       NEXT FIELD fecres
    END IF

   AFTER FIELD numini
    --Verificando numero inicial
    IF w_mae_pro.numini IS NULL OR
       (w_mae_pro.numini <=0) THEN 
       ERROR "Error: correlativo inicial invalido, VERIFICA ..."
       LET w_mae_pro.numini = NULL
       CLEAR numini 
       NEXT FIELD numini 
    END IF

   AFTER FIELD numfin
    --Verificando numero final
    IF w_mae_pro.numfin IS NULL OR
       (w_mae_pro.numfin <=0) THEN 
       ERROR "Error: correlativo final invalido, VERIFICA ..."
       LET w_mae_pro.numfin = NULL
       CLEAR numfin 
       NEXT FIELD numfin 
    END IF

   AFTER FIELD estado  
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado invalido, VERIFICA ..."
       NEXT FIELD estado
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codemp IS NULL THEN 
       NEXT FIELD codemp
    END IF
    IF w_mae_pro.tipdoc IS NULL THEN 
       NEXT FIELD tipdoc
    END IF
    IF w_mae_pro.nserie IS NULL THEN 
       NEXT FIELD nserie
    END IF
    IF w_mae_pro.nresol IS NULL THEN 
       NEXT FIELD nresol
    END IF
    IF w_mae_pro.fecres IS NULL THEN 
       NEXT FIELD fecres
    END IF
    IF w_mae_pro.numini IS NULL THEN 
       NEXT FIELD numini
    END IF
    IF w_mae_pro.numfin IS NULL THEN 
       NEXT FIELD numfin
    END IF
    IF (w_mae_pro.numfin<w_mae_pro.numini) THEN
       ERROR "Error: correlativos invalidos, VERIFICA ..."
       NEXT FIELD numini 
    END IF 
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF

    -- Verificando si ya existeuna resolucion activa para el tipo de documento
    SELECT UNIQUE a.lnkres
     FROM  fac_impresol a
     WHERE a.lnkres != w_mae_pro.lnkres
       AND a.codemp  = w_mae_pro.codemp
       AND a.tipdoc  = w_mae_pro.tipdoc
       AND a.nserie  = w_mae_pro.nserie
       AND a.estado  = 1
     IF (status!=NOTFOUND) THEN
       -- Desplegando mensaje
       CALL fgl_winmessage(
       " Atencion",
       " Ya existe una resolucion de ALTA para este tipo de documento. \n"||
       " Solo puede existir una resolucion de ALTA.",
       "information")
       CONTINUE INPUT
     END IF

    -- Verificando si resolucion ya existe
    SELECT UNIQUE a.lnkres
     FROM  fac_impresol a
     WHERE a.lnkres != w_mae_pro.lnkres 
       AND a.codemp  = w_mae_pro.codemp  
       AND a.tipdoc  = w_mae_pro.tipdoc 
       AND a.nserie  = w_mae_pro.nserie
       AND a.nresol  = w_mae_pro.nresol 
     IF (status!=NOTFOUND) THEN
       -- Desplegando mensaje
       CALL fgl_winmessage(
       " Atencion", 
       " Resolucion ya existe registrada. \n"||
       " VERIFICAR  empresa, tipo documento y serie.",
       "information")
       CONTINUE INPUT
     END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae004_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facmae004_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facmae004_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un resolucion 

FUNCTION facmae004_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando resolucion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   LET w_mae_pro.lnkres = 0

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_impresol   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.lnkres = SQLCA.SQLERRD[2]

   --Asignando el mensaje 
   LET msg = "Resolucion (",w_mae_pro.lnkres USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_impresol
   SET    fac_impresol.*      = w_mae_pro.*
   WHERE  fac_impresol.lnkres = w_mae_pro.lnkres 

   --Asignando el mensaje 
   LET msg = "Resolucion (",w_mae_pro.lnkres USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando 
   DELETE FROM fac_impresol 
   WHERE (fac_impresol.lnkres = w_mae_pro.lnkres)

   --Asignando el mensaje 
   LET msg = "Resolucion (",w_mae_pro.lnkres USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae004_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae004_inival(i)
 DEFINE i SMALLINT

 -- Verificando resolucion de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.lnkres = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando combobox
 CALL librut003_cbxempresas() 
 CALL librut003_cbxtiposdocumento(1) 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.estado
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
