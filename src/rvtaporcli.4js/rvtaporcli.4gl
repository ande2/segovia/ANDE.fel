{ 
Fecha    : Febrero 2020  
Programo : rvtaporpro.4gl 
Objetivo : Reporte de ventas por cliente
}

IMPORT FGL fgl_zoom
 
GLOBALS "rvtaporcli_glob.4gl" 
{ Definicion de variables globales }

GLOBALS
DEFINE 
		 arr , s_line INTEGER,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
       fwd      CHAR(12),
       tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       
      w_empr            RECORD LIKE glb_empresas.*,
       wpais     VARCHAR(100), 
       nomusu    VARCHAR(100), 
       
       filename  STRING,
       fnumpos   STRING,
       
-- Subrutina principal 

      dbname      CHAR(20),
      n_param     SMALLINT,
      prog_name   STRING 
END GLOBALS
MAIN       

 -- Atrapando interrupts
 DEFER INTERRUPT

 CALL ui.Interface.loadStyles("style_rep")
 
 LET prog_name = "rvtaporcli" || ".log"
    CALL STARTLOG(prog_name)

 LET n_param = num_args()

   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = ARG_VAL(1)
      DATABASE dbname
   END IF

 -- Cargando estilos y acciones default
 --CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "rvtaporcli.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep010_input()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep010_input()

DEFINE 
        pipeline   STRING,
        userid     VARCHAR(20),
        loop       SMALLINT
        
DEFINE handler om.SaxDocumentHandler
 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep010a AT 5,2
  WITH FORM "rvtaporcli" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  --GIOVANNICALL librut003_parametros(1,0)
  --GIOVANNI RETURNING existe,wpais
  --GIOVANNI CALL librut001_header("facrep004",wpais,1)

  -- Definiendo archivo de impresion
  -- GIOVANNILET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrep004.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  
  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
	LET w_datos.fecini = TODAY
	LET w_datos.fecfin = TODAY
   LET w_datos.tipsal = "PDF"
   CLEAR FORM

	LET userid = FGL_GETENV("LOGNAME")

	SELECT glb_usuarios.nomusr
	  INTO nomusu
	  FROM glb_usuarios
	 WHERE glb_usuarios.userid = userid 

OPTIONS INPUT WRAP
   -- Construyendo busqueda
   INPUT BY NAME w_datos.* ATTRIBUTES(WITHOUT DEFAULTS )

		BEFORE INPUT
			CALL DIALOG.setActionHidden("accept",1)
			CALL DIALOG.setActionHidden("cancel",1)

	 AFTER FIELD fecini
		IF w_datos.fecini IS NOT NULL AND w_datos.fecini > TODAY THEN
			CALL box_valdato("Debe ingresar una fecha correcta")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF

	 AFTER FIELD fecfin
		IF w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NULL AND (w_datos.fecini <= TODAY) THEN
			LET w_datos.fecfin = TODAY
			DISPLAY BY NAME w_datos.fecfin
			NEXT FIELD NEXT
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF

    ON CHANGE numnit
    
        SELECT FIRST 1 UNIQUE  fac_mtransac.nomcli
        INTO w_datos.nomcli
        FROM fac_mtransac
        WHERE fac_mtransac.numnit = w_datos.numnit

        DISPLAY BY NAME w_datos.nomcli
			
	 ON KEY (CONTROL-E)
         LET loop = FALSE
         EXIT INPUT  

    ON ACTION zoom INFIELD numnit
      LET w_datos.numnit = zoom_numnit(FGL_DIALOG_GETBUFFER())
      DISPLAY BY NAME w_datos.numnit 
    
    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT  

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 
	  GOTO :label1 

	AFTER INPUT
     -- Obteniendo filtros
	  LABEL label1:
	  IF loop = TRUE THEN
     		LET w_datos.fecini= GET_FLDBUF(w_datos.fecini)
     		LET w_datos.fecfin= GET_FLDBUF(w_datos.fecfin)
 		
		
     		IF w_datos.fecini IS NULL THEN
		  		CALL fgl_winmessage("atention","Debe ingresar la fecha inicial","Atencion")
		  		NEXT FIELD fecini
	  		END IF
		
     		IF w_datos.fecfin IS NULL THEN
					IF w_datos.fecini <= TODAY THEN
						LET w_datos.fecfin = TODAY
						DISPLAY BY NAME w_datos.fecfin
					ELSE
						CALL fgl_winmessage("atention","Debe ingresar un punto de venta","Atencion")
						NEXT FIELD fecfin
					END IF
	  		ELSE
		  		IF w_datos.fecfin < w_datos.fecini THEN
						CALL fgl_winmessage("atention","Fecha final no puede ser mayor a fecha inicial","Atencion")
		      		NEXT FIELD fecfin
		  		END IF
     		END IF
		ELSE
			EXIT INPUT
		END IF
	END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 
 
   CALL run_report10_to_handler(HANDLER)
 
  END WHILE
 CLOSE WINDOW wrep010a   
END FUNCTION 

 --funcion zoom_numnit()
   #+ A simple zoom window to select the store code
PRIVATE FUNCTION zoom_numnit(l_current_value STRING)
    DEFINE numnit_zoom fgl_zoom.zoomType

    CALL numnit_zoom.init()
    LET numnit_zoom.cancelvalue = l_current_value
    LET numnit_zoom.title = "Selección de NIT"
    LET numnit_zoom.sql = "SELECT %2 FROM fac_mtransac WHERE %1 ORDER BY numnit"

    LET numnit_zoom.multiplerow = TRUE
    # values are (c)har, (i)nteger, (f)loat, (d)ate
    CALL numnit_zoom.column[1].quick_set("numnit", TRUE, "c", 4, "NIT")
    CALL numnit_zoom.column[2].quick_set("nomcli", FALSE, "c", 20, "Nombre")
    CALL numnit_zoom.column[3].quick_set("dircli", FALSE, "c", 20, "Dirección")
    CALL numnit_zoom.column[4].quick_set("telcli", FALSE, "c", 20, "Teléfono")
    CALL numnit_zoom.column[5].quick_set("maicli", FALSE, "c", 15, "Email")
    CALL numnit_zoom.column[6].quick_set("nomcom", FALSE, "c", 2, "Nombre Comercial")
    CALL numnit_zoom.column[7].quick_set("dircom", FALSE, "c", 5, "Dirección Comercial")

    RETURN numnit_zoom.call()
END FUNCTION