{ 
Fecha    : 2012-12-26
Programo : invrep009.4gl 
Objetivo : Reporte de existencias en bodega valorizado. 
}

DATABASE segovia 

{ Definicion de variables globales }
TYPE t_data RECORD
   codemp   LIKE inv_proenbod.codemp, 
   codsuc   LIKE inv_proenbod.codsuc, 
   codbod   LIKE inv_proenbod.codbod,
   cditem   LIKE inv_proenbod.cditem, 
   codabr   CHAR(20),
   dsitem   CHAR(40),
   nommed   CHAR(20),
   exican   LIKE inv_proenbod.exican,
   pulcom   LIKE inv_products.pulcom,
   ptotal   LIKE inv_products.pulcom
END RECORD
        
DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE inv_categpro.codcat,
        subcat   LIKE inv_subcateg.subcat,
        exiact   SMALLINT,
        eximin   SMALLINT, 
        eximax   SMALLINT
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  STRING,
       fcodcat   STRING,
       fsubcat   STRING,
       fexiact   STRING,
       feximin   STRING,
       feximax   STRING,
       ptotal    LIKE inv_products.pulcom

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep009.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep009_existencias()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep009_existencias()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              t_data,
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        streximin         STRING,
        streximax         STRING,
        strexiact         STRING,
        loop              SMALLINT
   DEFINE myHandler om.SaxDocumentHandler 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "invrep009a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep009",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/invrep009.txt"
  DISPLAY filename
  --LET filename = "c:\\temp\\invrep009.txt"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.exiact = 3 
   LET w_datos.eximin = 3 
   LET w_datos.eximax = 3 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.exiact,
                 w_datos.eximax, 
                 w_datos.eximin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion."
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER FIELD codbod
      -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de existencia actual
   LET strexiact = NULL
   CASE (w_datos.exiact)
    WHEN 1    LET strexiact = "AND a.exican >0"
              LET fexiact   = ">0" 
    WHEN 2    LET strexiact = "AND a.exican =0"
              LET fexiact   = "=0" 
    OTHERWISE LET strexiact = NULL
              LET fexiact   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia minima 
   LET streximin = NULL
   CASE (w_datos.eximin)
    WHEN 1    LET streximin = "AND a.eximin >0"
              LET feximin   = ">0" 
    WHEN 2    LET streximin = "AND a.eximin =0"
              LET feximin   = "=0" 
    OTHERWISE LET streximin = NULL
              LET feximin   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia maxima
   LET streximax = NULL
   CASE (w_datos.eximax)
    WHEN 1    LET streximax = "AND a.eximax >0"
              LET feximax   = ">0" 
    WHEN 2    LET streximax = "AND a.eximax =0"
              LET feximax   = "=0" 
    OTHERWISE LET streximax = NULL
              LET feximax   = "Todas" 
   END CASE 

   -- Construyendo seleccion 
	IF w_datos.codbod IS NOT NULL THEN
   	LET qrytxt = 
         "SELECT a.codemp,a.codsuc,a.codbod,a.cditem,b.codabr,b.dsitem,c.nommed,", 
            "a.exican, b.pulcom, (a.exican * b.pulcom) ptotal ",
         "FROM  inv_proenbod a,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e ",
         "WHERE a.codemp = ",w_datos.codemp,
         " AND a.codsuc = ",w_datos.codsuc,
         " AND a.codbod = ",w_datos.codbod,
         " AND b.cditem = a.cditem ",
         " AND c.unimed = b.unimed ",
         " AND d.codcat = b.codcat ",
         " AND e.codcat = b.codcat ",
         " AND e.subcat = b.subcat ",
         strcodcat CLIPPED," ",
         strsubcat CLIPPED," ",
         strexiact CLIPPED," ",
         streximin CLIPPED," ",
         streximax CLIPPED," ",
         " ORDER BY 4 "
	END IF

   LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep009 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep009
   LET existe = FALSE
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*

       LET existe = TRUE
       LET ptotal = 0
       START REPORT invrep009_impinvbod --TO filename
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT invrep009_impinvbod(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep009_impinvbod 
      LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
		--CALL librut001_rep_pdf("Existencias de Inventario",filename,8,"L",2)
      -- Enviando reporte al destino seleccionado
      --CALL librut001_enviareporte(filename,pipeline,"Existencias en Bodega")
      --ERROR "" 
      --CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep009_impinvbod(imp1)
 DEFINE imp1      t_data,
        linea     CHAR(158),
        exis      SMALLINT
        

  OUTPUT LEFT MARGIN 2
         PAGE LENGTH 66
         TOP MARGIN 3 
         BOTTOM MARGIN 3 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "______________________________________"

    -- Configurando tipos de letra
    PRINT ASCII 27 -- color negro
    PRINT 1 SPACES

    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 137,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"invrep009",
          COLUMN  69,"EXISTENCIAS VALORIZADO",
          COLUMN 137,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN  73,"** AL DIA **",
          COLUMN 137,"Hora  : ",TIME 
    PRINT linea 
    PRINT "Codigo                Descripcion del Producto                  Unidad de ",
          "Medida         Existencia     Precio Ult.       Precio" 
    PRINT "Producto                                                                  ",
          "               Actual         Compra            Total" 
    PRINT linea

   BEFORE GROUP OF imp1.codbod 
    -- Imprimiendo datos de la bodega
    PRINT "EMPRESA  (",imp1.codemp USING "<<<",") ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL (",imp1.codsuc USING "<<<",") ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA   (",imp1.codbod USING "<<<",") ",w_mae_bod.nombod CLIPPED
    SKIP 1 LINES
 
   ON EVERY ROW
    -- Imprimiendo productos
    PRINT COLUMN   1,imp1.codabr                                 ,2 SPACES,
                     imp1.dsitem                                 ,
          COLUMN  65,           
                     imp1.nommed                                 ,2 SPACES,
                     imp1.exican          USING  "--,---,--&.&&" ,2 SPACES,
                     imp1.pulcom          USING  "--,---,--&.&&" ,2 SPACES,
                     imp1.ptotal          USING  "--,---,--&.&&"
      LET ptotal = ptotal + imp1.ptotal
 
   AFTER GROUP OF imp1.codbod
    -- Totalizando por bodega
    SKIP 1 LINES
    PRINT COLUMN   1,"Total ",GROUP COUNT(*)    USING "<<<,<<&",
                     " Producto(s) en Bodega",
          COLUMN 117,ptotal               USING  "--,---,--&.&&"
    SKIP 1 LINES 

   ON LAST ROW
    -- Imprimiendo filtros
    PRINT fnt.twd CLIPPED,"FILTROS",fnt.fwd CLIPPED
    IF (LENGTH(fcodcat)>0) THEN
       PRINT "Categoria            : ",fcodcat
    END IF
    IF (LENGTH(fsubcat)>0) THEN
       PRINT "Subcategoria      : ",fsubcat
    END IF
    PRINT "Existencia Actual : ",fexiact 
    PRINT "Existencia Minima : ",feximin 
    PRINT "Existencia Maxima : ",feximax 
END REPORT 
