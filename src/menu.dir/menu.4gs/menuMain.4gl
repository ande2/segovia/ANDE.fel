SCHEMA "segovia"
GLOBALS "menuglob.4gl"

#########################################################################
## Function  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
#########################################################################
MAIN
   DEFINE aui  om.DomNode
   DEFINE sm   om.DomNode
   DEFINE ok   SMALLINT
	DEFINE vexe SMALLINT
	DEFINE n_param SMALLINT
   DEFINE tit  STRING
    
   OPTIONS
      INPUT NO WRAP,
      ON CLOSE APPLICATION STOP
   	WHENEVER ERROR CONTINUE

--	CLOSE WINDOW screen
-- CALL ui.Interface.loadActionDefaults("actiondefaultsLogin")
   CALL ui.Interface.loadActionDefaults("default.4ad")
   CALL ui.Interface.loadStyles("styles")

	LET n_param = num_args()
	{IF n_param = 0 THEN
   	   LET ok = login()
	ELSE
		LET ok = cambiobd(arg_val(1))
	END IF}
    LET gDbname = ARG_VAL(1) 
	 --Para el notificador
    LET ok = login()
	LET ok = TRUE
    LET cnt1 = 1
	LET nodo = 1
	LET accion = 0

   IF (ok) THEN
		--Corre el Cliente WTK
		--CALL correr_wtk()
      LET tit = "SISTEMA INTEGRADO DE ", dbempr_nom CLIPPED," - [MENU DE OPCIONES] "
   	CALL ui.Interface.loadStyles("fuente")
     	OPEN WINDOW menuBlank AT 1,1 WITH FORM "menuBlank" 
		
		DISPLAY dbempr_nom TO tit ATTRIBUTE (BLUE)
		LET tit = "SISTEMA INTEGRADO DE ", dbempr_nom CLIPPED," - [MENU DE OPCIONES] "
      CALL fgl_settitle(tit)
      CALL ui.Interface.setText(tit)
		MESSAGE  "USUARIO: ", gUsuario.usu_nom CLIPPED, "        ", today ATTRIBUTE(BLUE)
         LET aui = ui.Interface.getRootNode()
         LET sm = aui.createChild("StartMenu")
			CALL menu_pulldown()
         CALL sm.setAttribute("text","Menu Principal")
         CALL men(cRaiz, sm)

         MENU ""

			--COMMAND "NOTIFICADOR"
			--		IF gDbname = "ventas" THEN
			--			RUN "fglrun venp0068.42r ventas"
			--		END IF
					
			--ON IDLE 300
			--		IF gDbname = "ventas" THEN
			--			RUN "fglrun venp0068.42r ventas"
            --          display "pase ", CURRENT
            --          CALL pedidoBrowse(fgl_getenv("LOGNAME")) 
			--		END IF

			--COMMAND "VENTAS"
					--CALL desconecta(dbname)
					--CLOSE FORM menuBlank
					--RUN "fglrun *.42r 802" without waiting
					--CALL ui.interface.refresh()
					--EXIT PROGRAM


				COMMAND "reportes"
					#CALL ui.interface.frontcall("standard","shellexec",["iexplore http://saeecweb/eec/index.html"],[vexe])
					CALL ui.interface.frontcall("standard","shellexec",["//Saeecnt01/reportes/exe/mnuReportes.exe"],[vexe])

            COMMAND "Salir"
					IF NOT (veri_session(1)) THEN
               	EXIT MENU
					ELSE
						CALL msg("Cierre Todas las Ventanas del Sistema Integrado antes de Salir")
					END IF

            COMMAND KEY(INTERRUPT)
					IF NOT (veri_session(1)) THEN
               	EXIT MENU
					ELSE
						CALL msg("Cierre Todas las Ventanas del Sistema Integrado antes de Salir")
					END IF
         END MENU
      CLOSE FORM menuBlank
   END IF
END MAIN


#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION menu_pulldown()
DEFINE fn om.DomNode
DEFINE tm om.DomNode
DEFINE tmg om.DomNode
DEFINE tmi om.DomNode

LET fn = ui.Interface.getRootNode()
LET tm = fn.CreateChild("TopMenu")
LET tmg = tm.CreateChild("TopMenuGroup")
CALL tmg.setAttribute("text","Empresas")

LET tmi = tmg.createChild("TopMenuCommand")
CALL tmi.setAttribute("name","VENTS")
CALL tmi.setAttribute("text","VENTAS")
CALL tmi.setAttribute("image","logo")

END FUNCTION

#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION elimen(sm)
DEFINE sm om.DomNode
DEFINE append SMALLINT
DEFINE cnt SMALLINT
DEFINE p om.Domnode


LET cnt = arre.getlength() 
LET append = cnt
WHILE cnt > 0
	CALL sm.removeChild(arre[append].node)
	CALL arre.deleteElement(append)
	LET cnt = arre.getlength() 
	LET append = cnt
END  WHILE
LET cnt1 = 1

END FUNCTION



#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION men(p,sm)
   DEFINE p      INTEGER 
   DEFINE  i,n, cnt  INTEGER
   DEFINE qry     STRING
   DEFINE nom     STRING
	DEFINE condicion STRING
   DEFINE sm      om.DomNode
   DEFINE smg     om.DomNode
   DEFINE smc     om.DomNode
	DEFINE arr    DYNAMIC ARRAY OF
      RECORD
         menId    INTEGER,
         menNombre VARCHAR(500,1),
         menTipo  CHAR(1),
         menVerhe SMALLINT,
         menCmd   VARCHAR(2000,1),
         node     om.DomNode,
         prog_idx INTEGER
      END RECORD


  LET qry = "SELECT a.prog_id, a.prog_des, a.prog_tip,a.prog_verhe, ",
      " CASE WHEN a.prog_tip = 'R'  THEN ",
      "'", g_host CLIPPED, "'||(SELECT mempr.empr_hostrep FROM mempr mempr WHERE mempr.empr_db = '", dbname,"')", # "    END ",
             " ||TRIM(a.prog_dirpro) || '/' || TRIM(a.prog_nom) || '.exe' ",
             " ELSE ",
             "   CASE   WHEN  a.prog_verhe   = 0 THEN  ",
             "          a.prog_dirpro ",
             "          WHEN  a.prog_verhe   = 2 THEN  ",
             "          a.prog_dirpro ",
             "   WHEN a.prog_verhe = 1 THEN ",
             "     a.prog_dirpro ",
             "   WHEN a.prog_verhe = 3 THEN ",
             "    'cd ' || a.prog_dirpro || ';fglrunws *.42r ",dbname," '",
             "   END ",
             "  END, NVL(a.prog_idx,0) ",
				 " FROM mprog a, dmenprog b, adm_gru c ", 
				 " WHERE a.prog_id <> ", cRaiz, 
				 " AND a.prog_padre =",p, 
				 " AND a.est_id = 13",
				 " AND c.est_id = 13",
				 " AND b.gru_id = c.gru_id  ",
				 " AND b.prog_id = a.prog_id ",
				 " AND b.gru_id = ",gUsuario.gru_id, 
				 " ORDER BY 1 "
   PREPARE prpM1 FROM qry
   DECLARE curM1 CURSOR FOR prpM1
	LET i = 1
   FOREACH curM1 INTO arr[i].menId, 
							 arr[i].menNombre, 
							 arr[i].menTipo, 
							 arr[i].menVerhe, 
							 arr[i].menCmd,
							 arr[i].prog_idx
 
		IF arr[i].menTipo = "R" THEN
			LET arr[i].menCmd = "fglrun reporte/wait.42r ",
									  arr[i].menCmd CLIPPED ," ",dbname," ",
                             arr[i].prog_idx
		ELSE
			IF arr[i].menVerhe = 0 THEN
				LET arr[i].menCmd = ". ./",dbmon_pais CLIPPED,"; cp *.bmp ",
										  arr[i].menCmd CLIPPED, 
										  ";cd ",arr[i].menCmd CLIPPED,
										  ";fglrun *.42r ", 
										  dbname
			END IF
			IF arr[i].menVerhe = 1 THEN
#				LET arr[i].menCmd = ". ./ejecutargen.sh",
--				LET arr[i].menCmd = "cd ",FGL_GETENV("BASEDIR") CLIPPED,arr[i].menCmd CLIPPED," ",dbname
				LET arr[i].menCmd = arr[i].menCmd CLIPPED," ",dbname
--						    ";fglrun *.42r ", 
--						    dbname
			END IF
			IF arr[i].menVerhe = 2 THEN
				LET arr[i].menCmd = ". ./ejecutarws.sh; cp *.bmp ",
										  arr[i].menCmd CLIPPED, 
										  ";cd ",arr[i].menCmd CLIPPED,
										  ";fglrunws *.42r ", 
										  dbname
			END IF
		END IF
      LET i = i + 1
   END FOREACH

   LET i = i - 1

   IF i > 0 THEN
      FOR n = 1 TO i
         IF arr[n].menTipo = "M" THEN    -- Crea un submenu
            --CALL men(arr[n].menId,arr[n].node)
            LET arr[n].node = createStartMenuGroup(sm,arr[n].menNombre)
         ELSE                          -- Crea un comando
            LET arr[n].node = createStartMenuCommand(sm,arr[n].menNombre CLIPPED,arr[n].menCmd CLIPPED, "circle.png")
         END IF
		 	LET arre[cnt1].node = arr[n].node
		 	LET arre[cnt1].menTipo = arr[n].menTipo
			LET cnt1 = cnt1 + 1

            CALL men(arr[n].menId,arr[n].node)
      END FOR

		#DISCONNECT "generales"
   END IF

END FUNCTION

#########################################################################
## Function  : login()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Firma del sistema
#########################################################################
FUNCTION login()
   DEFINE ok,c,s  SMALLINT
	DEFINE cmb STRING
   DEFINE rec
      RECORD
         usuLogin CHAR(15),
         usuPwd   CHAR(20)
      END RECORD
   DEFINE qry     STRING
   LET ok = FALSE
   LET s = TRUE

	--Captura el Usuario que ingreso al sistema
	LET rec.usuLogin = fgl_getenv("LOGNAME")

   --OPEN WINDOW menLogin AT 1,1 WITH FORM "menLogin" ATTRIBUTES (TEXT = "Inicio de sesión...")
   --CALL lib_cleanTinyScreen()
   --CALL ui.Interface.setText("Inicio de sesión...")
   --WHILE s
      --LET int_flag = FALSE

		--Selecciona la Base de Datos para iniciar carga del menu
			SELECT	empr_db
				INTO	gDbname
				FROM	mempr
				WHERE empr_id = 0

		--CONNECT TO "generales"
        CONNECT TO gDbname 

		--LET cmb = "SELECT empr_id,empr_nomct FROM mempr ORDER BY 1"
        --CALL combo_din2("dbname", cmb)

      --INPUT BY NAME dbname,rec.*
         --AFTER INPUT
            --IF (NOT int_flag) THEN

					--Obtiene los datos de la empresa a la que ingreso el usuario
					LET dbname= 0
					SELECT pais_id,empr_db,empr_nom,empr_archejec
					INTO gpais_id,dbname, dbempr_nom, dbmon_pais
					FROM mempr
					WHERE empr_id = dbname

					--se genera la conexion hacia la BD a la que se ingresa
					CONNECT TO dbname 

               --LET qry = "SELECT COUNT(*) FROM adm_usu WHERE usu_nomunix = ? and usu_passw= ?  "
               LET qry = "SELECT COUNT(*) FROM adm_usu WHERE usu_nomunix = ?"
               PREPARE prpL1 FROM qry
               EXECUTE prpL1 USING rec.usuLogin INTO c
               --EXECUTE prpL1 USING rec.usuLogin,rec.usuPwd INTO c
               IF STATUS = 0 THEN
                  IF c = 1 THEN
                     LET qry = "SELECT adm_usu.usu_nomunix,adm_usu.gru_id,adm_usu.usu_nom FROM adm_usu WHERE usu_nomunix = ? AND est_id = 13"
                     PREPARE prpL2 FROM qry
                     EXECUTE prpL2 USING rec.usuLogin INTO gUsuario.usu_nomunix,
																			  gUsuario.gru_id,
																			  gUsuario.usu_nom
                     IF STATUS = 0 THEN
                        LET s = FALSE
                        LET ok = TRUE
								LET g_host = busca_host()
							ELSE
								CALL msg("Usuario No tiene Acceso, Consulte a Informática")
								--NEXT FIELD usuLogin
                     END IF
                  ELSE
                     CALL msg("Usuario No tiene Permiso en Esta Empresa, Consulte a Informática")
                     --NEXT FIELD usuLogin
                  END IF
               END IF
            --ELSE
               --LET int_flag = TRUE
               --LET s = FALSE
            --END IF
      --END INPUT
   --END WHILE

	--se realiza desconexion de la BD generales, para no tener conexiones colgadas
	#DISCONNECT "generales"

   --CLOSE WINDOW menLogin
   RETURN ok
END FUNCTION

#########################################################################
## Function  : cambiobd()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Obtiene el nombre del menu
#########################################################################
FUNCTION cambiobd(base)
DEFINE c,s  SMALLINT
DEFINE qry     STRING
DEFINE ventero SMALLINT
DEFINE base    SMALLINT
DEFINE vbase   CHAR(20)
DEFINE rec
	RECORD
  	 usuLogin CHAR(15),
    usuPwd   CHAR(20)
END RECORD

	CALL progbar()

	--Creacion de Usuario
	LET rec.usuLogin = fgl_getenv("LOGNAME")

	LET vbase = dbname

	--CONNECT TO "generales"
    CONNECT TO gDbname

	--Obtiene la Base de Datos
	SELECT pais_id, empr_db,empr_nom,empr_archejec
	INTO gpais_id,dbname, dbempr_nom, dbmon_pais
	FROM mempr
	WHERE empr_id = base

	DISCONNECT CURRENT

	CONNECT TO dbname 

   LET qry = "SELECT COUNT(*) FROM adm_usu WHERE usu_nomunix = ? "
   PREPARE existe FROM qry
   EXECUTE existe USING rec.usuLogin INTO c

   IF STATUS = 0 THEN
   	IF c = 1 THEN
         SELECT adm_usu.usu_nomunix,
					 adm_usu.gru_id,
					 adm_usu.usu_nom 
			INTO 
				gUsuario.usu_nomunix,
			   gUsuario.gru_id,
			   gUsuario.usu_nom
			FROM adm_usu 
			WHERE adm_usu.usu_nomunix = rec.usuLogin 
			AND est_id = 13

   		IF STATUS = 0 THEN
				LET g_host = busca_host()
			ELSE
				CALL msg("Usuario No tiene Acceso, Consulte a Informática")
				RETURN FALSE
   		END IF
    	ELSE
    		CALL msg("Usuario No tiene Permiso en Esta Empresa, Consulte a Informática")
			RETURN FALSE
    	END IF
	ELSE
		CALL msg("Usuario No existe")
		RETURN FALSE
	END IF
	RETURN TRUE
END FUNCTION

#########################################################################
## Function  : veri_session()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Obtiene el total de sesiones en la base de datos
#########################################################################
FUNCTION veri_session(tipo_operacion)
DEFINE vcuantos INTEGER
DEFINE tipo_operacion SMALLINT
DEFINE qry      STRING
DEFINE fgltty   CHAR(100)

LET fgltty = FGL_GETENV("FGLTTY")

LET vcuantos = 0

	SELECT COUNT(*)
	INTO vcuantos
	FROM sysmaster:syssessions syssessions, sysmaster:sysopendb sysopendb
	WHERE syssessions.username = gUsuario.usu_nomunix
	AND   sysopendb.odb_sessionid = syssessions.sid
	AND   sysopendb.odb_dbname <> 'generales'
	AND   sysopendb.odb_dbname <> 'sysmaster'
	AND   syssessions.tty = fgltty

	DISPLAY "cuantos", vcuantos

IF tipo_operacion = 1 THEN
	IF vcuantos = 1 THEN
		RETURN FALSE
	ELSE
		RETURN FALSE
		#RETURN TRUE
	END IF
ELSE
	IF vcuantos = 1 THEN
		RETURN FALSE
	ELSE
		RETURN FALSE
		#RETURN TRUE
	END IF
END IF

END FUNCTION 


#########################################################################
## Function  : correr_wtk()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Corre el Cliente WTK
#########################################################################
FUNCTION correr_wtk()
DEFINE vos  STRING
DEFINE cmd  STRING
DEFINE vexe SMALLINT

	CALL ui.interface.frontcall("standard","getenv",["PROGRAMFILES"],[vos])
	LET vos = '"',vos, '\\\Fourjs\\\cliwtk\\\bin\\\startwtk.exe"'
	CALL ui.interface.frontcall("standard","shellexec",[vos],[vexe])

END FUNCTION

#########################################################################
## Function  : Traer Hosts.
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Buscar Hosts
#########################################################################
FUNCTION busca_host()
DEFINE ip1 CHAR(9)
DEFINE ip2 CHAR(9)
DEFINE ip3 CHAR(7)
DEFINE host CHAR(50)
DEFINE cnt SMALLINT

	DECLARE maquina CURSOR FOR

	SELECT dempr.demp_host, dempr.demp_path
	FROM dempr dempr

	FOREACH maquina INTO host,ip1
		LET ip3 = FGL_GETENV("REMOTEHOST")

		IF ip3[1,3] = 175 THEN
			LET ip3 = FGL_GETENV("REMOTEHOST")
			IF ip1 = ip3 THEN
				EXIT FOREACH	
			END IF
		END IF	

		IF ip3[1,3] <> 175 THEN
			LET ip2 = FGL_GETENV("REMOTEHOST")
			IF ip1 = ip2 THEN
				EXIT FOREACH	
			END IF
		END IF	
	
	END FOREACH

	#DISCONNECT "generales"

	RETURN host

END FUNCTION

#########################################################################
## Function  : Desconecta
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Buscar Hosts
#########################################################################

FUNCTION desconecta(vbase_des)
	DEFINE vbase_des CHAR(20)

	DISCONNECT CURRENT
	DISCONNECT vbase_des

END FUNCTION

#########################################################################
## Function  : Desconecta
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Buscar Hosts
#########################################################################

FUNCTION progbar()
	DEFINE i SMALLINT

 CLOSE WINDOW SCREEN 
 OPEN WINDOW w3 WITH FORM "menprogbar"

 DISPLAY "GENERANDO OPCIONES" TO etiqueta
 FOR i = 1 TO 100 STEP 50
   DISPLAY i TO rptbar
	CALL ui.Interface.refresh()
   SLEEP 1
 END FOR

 CLOSE WINDOW w3

END FUNCTION
