#########################################################################
## Function  : catGrupo()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Grupo de Catalogos
#########################################################################
DATABASE conta
GLOBALS "menuGlobals.4gl"

FUNCTION catGrupo()
   DEFINE rec  RECORD LIKE adm_gru.*
   DEFINE recbac  RECORD LIKE adm_gru.*
--   DEFINE insvit RECORD LIKE mvitdmen.*
   DEFINE qry  STRING
   DEFINE qry1  STRING
	DEFINE message STRING
	DEFINE cmb STRING
	DEFINE cmb1 STRING
   DEFINE resp SMALLINT
   --DEFINE vestado_gru LIKE mest.est_id
	DEFINE tit STRING
	DEFINE where_clause STRING


   OPEN WINDOW menCatGrupo AT 1,1 WITH FORM "menCatGrupo" 
		LET tit = vempr_nom CLIPPED, " - [C�talogos de Grupos]"
		CALL fgl_settitle(tit)
      MENU ""
			BEFORE MENU
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"

--            INITIALIZE insvit.* TO NULL
				INITIALIZE rec. * TO NULL
				LET recbac.* = rec.*

				LET cmb = " SELECT a.est_id,a.est_desc FROM mest a",
							 " WHERE a.est_id IN (13,14,20) "

				LET cmb1 = " SELECT a.empr_id,a.empr_nom FROM mempr a ",
                       " WHERE a.est_id = 1 ORDER BY 1"

			LET vusuario = FGL_GETENV("LOGNAME")

         ON ACTION buscar
				SHOW OPTION ALL
				CALL combo_din2("est_id",cmb)
				CALL combo_din2("empr_id",cmb1)

				LET recbac.* = rec.*	
				
				CONSTRUCT BY NAME where_clause ON gru_id, gru_nom, est_id, empr_id

					AFTER CONSTRUCT

					IF int_flag = FALSE THEN
						CALL get_fldbuf(gru_id, gru_nom, est_id, empr_id) RETURNING rec.gru_id, rec.gru_nom, rec.est_id, rec.empr_id
					ELSE
						EXIT CONSTRUCT
					END IF
				END CONSTRUCT


				IF int_flag = FALSE THEN

  					LET qry = "SELECT a.* FROM adm_gru a WHERE ", where_clause CLIPPED,
                 	 "ORDER BY 1 "

      			PREPARE prpCG1 FROM qry
					DECLARE curCG1 SCROLL CURSOR WITH HOLD FOR prpCG1

      			OPEN curCG1
      			FETCH FIRST curCG1 INTO rec.*

      			IF STATUS = NOTFOUND THEN
         			CALL msg("No existen datos en la tabla")
						HIDE OPTION ALL
						SHOW OPTION "buscar","agregar"
      			END IF

					CALL combo_din2("empr_id",cmb1)
      			DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id
				ELSE
					LET message = "Busqueda Cancelada"
					CALL msg(message)
					LET int_flag = FALSE
					LET rec.* = recbac.*
      			DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id
				END IF


         ON ACTION agregar
				LET recbac.* = rec.*
				INITIALIZE rec. * TO NULL

            LET int_flag = FALSE
				LET cmb = " SELECT est_id,est_desc FROM mest",
							 " WHERE est_id IN (13,14,20) "

            LET rec.est_id = 13

				CALL combo_din2("est_id",cmb)
				CALL combo_din2("empr_id",cmb1)

            DISPLAY BY NAME rec.gru_id, rec.gru_nom, rec.est_id, rec.empr_id

            INPUT BY NAME rec.gru_nom, rec.empr_id WITHOUT DEFAULTS

               AFTER INPUT
                  IF (NOT int_flag) THEN
                     LET rec.gru_id = 0

                     BEGIN WORK
                     LET rec.fectran = CURRENT
                     INSERT INTO adm_gru VALUES (rec.*)
                     IF STATUS = 0 THEN
                        COMMIT WORK
                        LET message = "Grupo dado de alta en el sistema satisfactoriamente"
                        CALL msg(message)
                        --OPEN curCG1
                        --FETCH LAST curCG1 INTO rec.*
                        DISPLAY BY NAME rec.gru_id,rec.est_id,rec.gru_nom, rec.empr_id
                     ELSE
                        ROLLBACK WORK
                        CALL msg(cErr)
                     END IF
                  ELSE
                     LET int_flag = TRUE
                     CALL msg("Ingreso Cancelado")
							LET rec.* = recbac.*
                     DISPLAY BY NAME rec.gru_id,rec.est_id,rec.gru_nom, rec.empr_id
                  END IF
            END INPUT
				CLEAR FORM


         ON ACTION actualizar
				LET recbac.* = rec.*
            IF rec.gru_id <> cRaiz THEN
               LET int_flag = FALSE

              	IF rec.est_id <> 20 THEN
						CALL combo_din2("est_id",cmb)
						CALL combo_din2("empr_id",cmb1)

               	INPUT BY NAME rec.est_id, rec.gru_nom, rec.empr_id WITHOUT DEFAULTS

                  AFTER INPUT
                     IF (NOT int_flag) THEN
	               		LET resp = FALSE

								IF recbac.est_id <> rec.est_id THEN
  		             			LET resp = confirma("Si modifica el estado del grupo, modificara el estado de los usuarios del grupo?")
  		             			IF NOT resp THEN
                        		CALL msg("Modificacion Cancelada") 
										EXIT INPUT
     		             		END IF
								END IF

                        BEGIN WORK
                        LET qry1= "UPDATE adm_usu SET est_id = ? WHERE adm_usu.gru_id = ? AND adm_usu.usu_id NOT IN(20)"
  		                  PREPARE prpCGU1 FROM qry1
  		                  EXECUTE prpCGU1 USING rec.est_id, rec.gru_id
      	               IF STATUS <> 0 THEN
                         	ROLLBACK WORK
                          	CALL msg(cErr)
								END IF
		                  LET qry = "UPDATE adm_gru SET est_id = ?, gru_nom = ?, empr_id = ? WHERE adm_gru.gru_id = ?"
  		                  PREPARE prpCGU FROM qry
  		                  EXECUTE prpCGU USING rec.est_id,rec.gru_nom, rec.empr_id, rec.gru_id
      	               IF STATUS = 0 THEN

    	                     COMMIT WORK
     		                  LET message = "Grupo Actualizado Satisfactoriamente" 
        		               CALL msg(message)

                 		      DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id
                    		ELSE
                    	 		ROLLBACK WORK
                       		CALL msg(cErr)
                      	END IF
                    	ELSE
                    		LET int_flag = TRUE
                   	END IF
              		END INPUT
           		ELSE
              		CALL msg("No puede modificar el registro")
						HIDE OPTION ALL
						SHOW OPTION "buscar","agregar"
					END IF
           	END IF


         ON ACTION eliminar
         IF rec.est_id <> 20 THEN
            IF rec.gru_id <> cRaiz THEN
               LET resp = FALSE
               LET resp = confirma("Esta seguro que desea dar de baja el registro?")
               IF resp THEN
                  BEGIN WORK
                  LET qry = "UPDATE adm_gru SET est_id.est_id = ? WHERE adm_gru.gru_id = ?"
                  PREPARE prpCGD FROM qry
                  EXECUTE prpCGD USING rec.est_id, rec.gru_id
                  IF STATUS = 0 THEN
                     COMMIT WORK
                     LET message = "Grupo Eliminado del sistema"
                     CALL msg(message)
                     CLOSE curCG1
                     OPEN curCG1
                     FETCH FIRST curCG1 INTO rec.*
                     DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id    
                  ELSE
                     CALL msg(cErr)
                     ROLLBACK WORK
                  END IF
               END IF
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF
          ELSE
            CALL msg("NO PUEDE MODIFICAR EL REGISTRO")
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"
          END IF

         ON ACTION primero
            FETCH FIRST curCG1 INTO rec.*
           	DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id    

         ON ACTION siguiente
            FETCH NEXT curCG1 INTO rec.*
          IF SQLCA.SQLCODE = 100 THEN
               LET message = "Esta posicionado en el ultimo registro"
               CALL msg(message)
            ELSE
            	DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id    
            END IF

         ON ACTION anterior
            FETCH PREVIOUS curCG1 INTO rec.*
				IF SQLCA.SQLCODE = 100 THEN
               LET message = "Esta posicionado en el primer registro"
               CALL msg(message)
            ELSE
           		DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id  
				END IF

         ON ACTION ultimo
            FETCH LAST curCG1 INTO rec.*
            DISPLAY BY NAME rec.gru_id, rec.est_id, rec.gru_nom, rec.empr_id    

         COMMAND KEY(INTERRUPT)
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatGrupo
END FUNCTION