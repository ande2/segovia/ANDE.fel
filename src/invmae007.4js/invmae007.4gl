{
invmae007.4gl 
Mynor Ramirez
Mantenimiento de empresas 
}

-- Definicion de variables globales 

GLOBALS "invglo007.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae007.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae007_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae007_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae007a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0) 
  RETURNING existe,wpais
  CALL librut001_header("invmae007",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Empresas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de empresas."
    CALL invqbe007_empresas(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva empresa."
    LET savedata = invmae007_empresas(1) 
   COMMAND "Modificar"
    " Modificacion de una empresa existente."
    CALL invqbe007_empresas(2) 
   COMMAND "Borrar"
    " Eliminacion de una empresa existente."
    CALL invqbe007_empresas(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae007_empresas(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae007_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomemp,
                w_mae_pro.nomabr,
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.diremp
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT 
    -- Verificando integridad
    -- Si empresa ya tiene movimientos no se puede modificar el nombre
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe007_integridad() THEN
        CALL Dialog.SetFieldActive("nomemp",FALSE)
     ELSE
        CALL Dialog.SetFieldActive("nomemp",TRUE)
     END IF
    END IF

   AFTER FIELD nomemp  
    --Verificando nombre del empresa
    IF (LENGTH(w_mae_pro.nomemp)=0) THEN
       ERROR "Error: nombre de la empresa invalida, VERIFICA"
       LET w_mae_pro.nomemp = NULL
       NEXT FIELD nomemp  
    END IF

    -- Verificando que no exista otra empresa con el mismo nombre
    SELECT UNIQUE (a.codemp)
     FROM  glb_empresas a
     WHERE (a.codemp != w_mae_pro.codemp) 
       AND (a.nomemp  = w_mae_pro.nomemp) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra empresa con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomemp
     END IF 
   AFTER FIELD numnit  
    --Verificando numero de nit 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       ERROR "Error: numero de NIT invalida, VERIFICA"
       LET w_mae_pro.numnit = NULL
       NEXT FIELD numnit  
    END IF

    -- Verificando que no exista otro NIT                      
    SELECT UNIQUE (a.numnit)
     FROM  glb_empresas a
     WHERE (a.codemp != w_mae_pro.codemp) 
       AND (a.numnit  = w_mae_pro.numnit) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra empresa con el mismo numero de NIT, VERIFICA ...",
        "information")
        NEXT FIELD numnit
     END IF 
   AFTER FIELD diremp
    --Verificando direccion
    IF (LENGTH(w_mae_pro.diremp)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA"
       LET w_mae_pro.diremp = NULL
       NEXT FIELD diremp  
    END IF
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomemp IS NULL THEN 
       NEXT FIELD nomemp
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.numnit IS NULL THEN 
       NEXT FIELD numnit
    END IF
    IF w_mae_pro.diremp IS NULL THEN 
       NEXT FIELD diremp
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae007_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando empresa
    CALL invmae007_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae007_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una empresa

FUNCTION invmae007_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando empresa ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codemp),0)
    INTO  w_mae_pro.codemp 
    FROM  glb_empresas a
    IF (w_mae_pro.codemp IS NULL) THEN
       LET w_mae_pro.codemp = 1
    ELSE
       LET w_mae_pro.codemp = w_mae_pro.codemp+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_empresas   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codemp 

   --Asignando el mensaje 
   LET msg = "Empresa (",w_mae_pro.codemp USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_empresas
   SET    glb_empresas.*      = w_mae_pro.*
   WHERE  glb_empresas.codemp = w_mae_pro.codemp 

   --Asignando el mensaje 
   LET msg = "Empresa (",w_mae_pro.codemp USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando empresas
   DELETE FROM glb_empresas 
   WHERE (glb_empresas.codemp = w_mae_pro.codemp)

   --Asignando el mensaje 
   LET msg = "Empresa (",w_mae_pro.codemp USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae007_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae007_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codemp = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.nomemp THRU w_mae_pro.diremp 
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
