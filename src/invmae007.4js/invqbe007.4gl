{
invqbe007.4gl 
Mynor Ramirez
Mantenimiento de empresas 
}

{ Definicion de variables globales }
IMPORT FGL fgl_excel
GLOBALS "invglo007.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe007_empresas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form
--Definiendo variables para enviar a excel
      DEFINE filename         STRING 
      DEFINE rfilename        STRING 
      DEFINE HEADER           BOOLEAN 
      DEFINE preview          BOOLEAN 
      DEFINE result           BOOLEAN

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae007_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.nomemp,a.nomabr,
                                a.numnit,a.numtel,a.numfax,
                                a.diremp,a.userid,a.fecsis,
                                a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL invmae007_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,a.nomemp,a.numnit ",
                 " FROM glb_empresas a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_empresas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_empresas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_empresas INTO v_empresas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_empresas
   FREE  c_empresas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_empresas TO s_empresas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL invmae007_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar 
      CALL invmae007_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae007_empresas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe007_datos(v_empresas[ARR_CURR()].tcodemp)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
       IF invmae007_empresas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe007_datos(v_empresas[ARR_CURR()].tcodemp)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = invqbe007_integridad()
      IF (res>0) THEN  
         -- Claisifcando error
         CASE (res)
          WHEN 1 LET msg = " Esta empresa ya tiene movimientos registrados, no puede borrarse. "
          WHEN 2 LET msg = " Esta empresa ya tiene sucursales registradas, no puede borrarse."
          WHEN 3 LET msg = " Esta empresa ya tiene ventas registradas, no puede borrarse."
         END CASE

         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta empresa ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae007_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Nombree Anreviado" 
      LET arrcols[4] = "Numero de NIT" 
      LET arrcols[5] = "Numero Telefono" 
      LET arrcols[6] = "Numero FAX" 
      LET arrcols[7] = "Direccion" 
      LET arrcols[8] = "Usuario Registro"
      LET arrcols[9] = "Fecha Registro"
      LET arrcols[10]= "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM glb_empresas a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      LET HEADER = TRUE 
      LET preview = TRUE 
      LET filename = "Empresas.xlsx"
      LET rfilename = "C:\\\\tmp\\", filename CLIPPED 
      IF sql_to_excel(qry, filename, HEADER) THEN 
          IF preview THEN 
              CALL fgl_putfile(filename, rfilename)
              CALL ui.Interface.frontCall("standard","shellExec", rfilename, result)
          ELSE 
              MESSAGE "Spreadsheet created"
          END IF 
      ELSE 
          ERROR "Something went wrong"
      END IF

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe007_datos(v_empresas[1].tcodemp)
      ELSE
         CALL invqbe007_datos(v_empresas[ARR_CURR()].tcodemp)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen empresas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe007_datos(wcodemp)
 DEFINE wcodemp LIKE glb_empresas.codemp,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.codemp,a.nomemp,a.nomabr,a.numnit,a.numtel,a.numfax,a.diremp,a.userid,a.fecsis,a.horsis "||
              "FROM  glb_empresas a "||
              "WHERE a.codemp = "||wcodemp||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_empresast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_empresast INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomemp THRU w_mae_pro.diremp 
  DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_empresast
 FREE  c_empresast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomemp THRU w_mae_pro.diremp 
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si la empresa ya tiene algun movimiento registrado

FUNCTION invqbe007_integridad()
 DEFINE conteo SMALLINT

 -- Verificando movimientos
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.codemp = w_mae_pro.codemp) 
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando sucursales
     SELECT COUNT(*)
      INTO  conteo
      FROM  glb_sucsxemp a
      WHERE (a.codemp = w_mae_pro.codemp) 
      IF (conteo>0) THEN
         RETURN 2 
      ELSE 
         -- Verificando sucursales
         SELECT COUNT(*)
          INTO  conteo
          FROM  fac_mtransac a 
          WHERE (a.codemp = w_mae_pro.codemp) 
          IF (conteo>0) THEN
             RETURN 3 
          ELSE
             RETURN 0
          END IF 
      END IF 
  END IF
END FUNCTION 
