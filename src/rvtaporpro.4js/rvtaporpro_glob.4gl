DATABASE segovia 

{ Definicion de variables globales }
GLOBALS
TYPE report9_data RECORD
   parametros RECORD
      fecini DATE,
      fecfin DATE,
      numnit  VARCHAR(20,1),
      nomcli  VARCHAR(100,1),
      codsuc  LIKE glb_sucsxemp.codsuc,
      numpos  LIKE fac_usuaxpos.numpos,
      codcat  LIKE inv_products.codcat,
      subcat  LIKE inv_products.subcat,
      cditem   INTEGER,
      tipsal  VARCHAR(20,1),
      nomsuc   STRING,
      nompos   STRING,
      nomcat   STRING,
      nomsub   STRING,
      nomitem  STRING,
      fecha_rep DATE,
      hora_rep DATETIME HOUR TO SECOND
   END RECORD,
   imp1  RECORD
        aniofac SMALLINT,
        mesfac SMALLINT,
        nommes VARCHAR(20,1),
        diafac SMALLINT,
        fecemi  DATE,
        factura VARCHAR(30,1),
        codcli LIKE fac_mtransac.codcli, 
        numnit LIKE fac_mtransac.numnit, 
        nomcli LIKE fac_mtransac.nomcli,
        cditem LIKE fac_dtransac.cditem,
        cantid LIKE fac_dtransac.cantid,
        codcat LIKE inv_products.codcat,
        subcat LIKE inv_products.subcat,
        dsitem LIKE inv_products.dsitem,
        unimed LIKE inv_unimedid.unimed,
        nommed LIKE inv_unimedid.nommed,
        nomanio STRING
    END RECORD
       
END RECORD

DEFINE w_datos RECORD
			fecini DATE,
			fecfin DATE,
			numnit  VARCHAR(20,1),
         nomcli  VARCHAR(100,1),
         codsuc  LIKE glb_sucsxemp.codsuc,
         numpos  LIKE fac_usuaxpos.numpos,
         codcat  LIKE inv_products.codcat,
         subcat  LIKE inv_products.subcat,
         cditem   INTEGER,
         tipsal  VARCHAR(20,1)
		 END RECORD

DEFINE nombre   STRING

END GLOBALS