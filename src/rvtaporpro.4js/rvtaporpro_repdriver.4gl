GLOBALS "rvtaporpro_glob.4gl"

FUNCTION run_report9_to_handler(handler)
    DEFINE
        data report9_data,
        handler om.SaxDocumentHandler,
        qrytxt STRING,
        qrytxt_header STRING,
        existe INTEGER

   LET qrytxt_header = "SELECT YEAR(e1.fecemi) aniofac,MONTH(e1.fecemi) mesfac, ' ' nommes,",
                 "DAY(e1.fecemi) diafac ,e1.fecemi,TRIM(e1.nserie)||'-'||e1.numdoc factura, ",
                 "e1.codcli, e1.numnit, e1.nomcli, ",
                 "d1.cditem,d1.cantid, ",
                 "i1.codcat,i1.subcat,i1.dsitem, ",
                 "u1.unimed,u1.nommed"
                 
   LET qrytxt =  "FROM fac_mtransac e1, fac_dtransac d1, inv_products i1,inv_unimedid u1 ",
                 "WHERE e1.fecemi between '",w_datos.fecini,"' and '",w_datos.fecfin,"' ",
                 "and e1.estado = 'V' "
                 
    IF w_datos.numnit IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " AND e1.numnit matches '",w_datos.numnit CLIPPED,"' "
    END IF
    --Para Nombre
    IF w_datos.nomcli IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " AND e1.nomcli matches '*", w_datos.nomcli CLIPPED,"*' "
    END IF
    IF w_datos.numpos IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " AND e1.usrope matches '",w_datos.numpos CLIPPED,"' "
    END IF

    LET qrytxt = qrytxt CLIPPED,
                 " and d1.lnktra = e1.lnktra "

    IF w_datos.codsuc IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " and d1.codsuc = ",w_datos.codsuc
    END IF

    LET qrytxt = qrytxt CLIPPED,
                " and i1.cditem = d1.cditem ",
                "and u1.unimed = d1.unimed"

    IF w_datos.codcat IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " and i1.codcat = ",w_datos.codcat
    END IF
    IF w_datos.subcat IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " and i1.subcat = ",w_datos.subcat
    END IF
    IF w_datos.cditem IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " and i1.cditem = ",w_datos.cditem
    END IF

   LET data.parametros.fecini = w_datos.fecini
   LET data.parametros.fecfin = w_datos.fecfin
   LET data.parametros.numnit = w_datos.numnit
   LET data.parametros.nomcli = w_datos.nomcli
   LET data.parametros.codsuc = w_datos.codsuc
   LET data.parametros.numpos = w_datos.numpos
   LET data.parametros.codcat = w_datos.codcat
   LET data.parametros.subcat = w_datos.subcat
   LET data.parametros.cditem = w_datos.cditem
   LET data.parametros.tipsal = w_datos.tipsal
    
   IF data.parametros.codsuc IS NOT NULL THEN 
      SELECT a.nomsuc INTO data.parametros.nomsub FROM glb_sucsxemp a WHERE a.codsuc = data.parametros.codsuc
   ELSE
      LET data.parametros.nomsub = "TODOS"
   END IF

   IF data.parametros.numpos IS NOT NULL THEN 
      SELECT a.userid INTO data.parametros.nompos FROM fac_usuaxpos a WHERE a.numpos = data.parametros.numpos
   ELSE
      LET data.parametros.nompos = "TODOS"
   END IF
   
   IF data.parametros.codcat IS NOT NULL THEN 
      SELECT a.nomcat INTO data.parametros.nomcat FROM inv_categpro a WHERE a.codcat = data.parametros.codcat
   ELSE
      LET data.parametros.nomcat = "TODOS"
   END IF      

   IF data.parametros.subcat IS NOT NULL THEN 
      SELECT a.nomsub INTO data.parametros.nomsub FROM inv_subcateg a WHERE a.subcat = data.parametros.subcat
   ELSE
      LET data.parametros.nomsub = "TODOS"
   END IF      

   IF data.parametros.cditem IS NOT NULL THEN 
      SELECT a.dsitem INTO data.parametros.nomitem FROM inv_products a WHERE a.cditem = data.parametros.cditem
   ELSE
      LET data.parametros.nomitem = "TODOS"
   END IF

   LET data.parametros.fecha_rep = TODAY;
   LET data.parametros.hora_rep = TIME;
   LET existe = 0
   PREPARE c_rep009_count FROM "SELECT COUNT(*) "||qrytxt CLIPPED
   EXECUTE c_rep009_count INTO existe

   IF existe > 0 THEN
      LET qrytxt = qrytxt, " ORDER BY 1,2,10,5"

      WHENEVER ERROR CONTINUE
      IF fgl_report_loadCurrentSettings(nombre||".4rp") THEN
         CALL fgl_report_selectDevice(w_datos.tipsal)
         CALL fgl_report_selectPreview(1)
         IF w_datos.tipsal <> "Image" THEN
            CALL fgl_report_setOutputFileName("/tmp/"||nombre CLIPPED||"."||w_datos.tipsal)
         ELSE
            CALL fgl_report_configureImageDevice(0,0,0,1,NULL,"jpg","/tmp",nombre,300)
         END IF
         LET handler = fgl_report_commitCurrentSettings()
      ELSE
         ERROR "No se encuentra el formato "||nombre CLIPPED ||".4rp"
      END IF
      WHENEVER ERROR STOP
      IF handler IS NOT NULL THEN
   
         PREPARE c_rep009 FROM qrytxt_header CLIPPED||" "||qrytxt CLIPPED 
         DECLARE c_crep009 CURSOR FOR c_rep009
   
         START REPORT report9_report TO XML HANDLER HANDLER
         FOREACH c_crep009 INTO data.imp1.*
            LET existe = TRUE
            LET data.imp1.nomanio = "AÑO: ",DATA.imp1.aniofac
            LET data.imp1.nommes = "MES: "|| nomMes(data.imp1.mesfac)
            OUTPUT TO REPORT report9_report(data.*)
         END FOREACH
         FINISH REPORT report9_report
         CLOSE c_crep009 
         FREE  c_crep009
      END IF
   ELSE
      
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF
   
END FUNCTION 

REPORT report9_report(data)
    DEFINE
        data report9_data
    ORDER EXTERNAL BY 
        data.imp1.aniofac, data.imp1.mesfac

    FORMAT
        ON EVERY ROW
            PRINTX data.*
END REPORT