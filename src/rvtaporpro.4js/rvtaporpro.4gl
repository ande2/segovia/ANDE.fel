{ 
Fecha    : Febrero 2020  
Programo : rvtaporpro.4gl 
Objetivo : Reporte de ventas por producto
}

IMPORT FGL fgl_zoom
 
GLOBALS "rvtaporpro_glob.4gl" 
{ Definicion de variables globales }

GLOBALS
DEFINE 
		             
		 arr , s_line INTEGER,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
       fwd      CHAR(12),
       tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       
        w_empr            RECORD LIKE glb_empresas.*,
       wpais     VARCHAR(100), 
       nomusu    VARCHAR(100), 
       
       filename  STRING,
       fnumpos   STRING,
       
      -- DEFINE handler om.SaxDocumentHandler-- report handler

-- Subrutina principal 



      dbname      CHAR(20),
      n_param     SMALLINT,
      prog_name   STRING 
END GLOBALS
MAIN       

 -- Atrapando interrupts
 DEFER INTERRUPT

 CALL ui.Interface.loadStyles("style_rep")

 LET prog_name = "rvtaporpro" || ".log"
    CALL STARTLOG(prog_name)

 LET n_param = num_args()

   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = ARG_VAL(1)
      DATABASE dbname
   END IF

 -- Cargando estilos y acciones default
 --CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "rvtaporpro.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep009_input()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep009_input()

DEFINE 
        pipeline,qrytxt,qrycombo   STRING,
        strnumpos         STRING,
        strfecini         STRING,
        strfecfin         STRING,
        strtipo           STRING,
        userid            VARCHAR(20),
        loop , i , o      SMALLINT,
        lastkey           SMALLINT,
		  labono				  DEC(10,2)

    DEFINE myHandler om.SaxDocumentHandler
    
DEFINE handler om.SaxDocumentHandler
 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep006a AT 5,2
  WITH FORM "rvtaporpro" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
	LET w_datos.fecini = TODAY
	LET w_datos.fecfin = TODAY
   LET w_datos.tipsal = "PDF"
   CLEAR FORM
   
	CALL librut002_combobox("codsuc","SELECT codsuc, nomsuc FROM glb_sucsxemp")
    CALL librut002_combobox("numpos","SELECT numpos, userid FROM fac_usuaxpos")
    CALL librut002_combobox("codcat","SELECT codcat, nomcat FROM inv_categpro")

	LET userid = FGL_GETENV("LOGNAME")

	SELECT glb_usuarios.nomusr
	  INTO nomusu
	  FROM glb_usuarios
	 WHERE glb_usuarios.userid = userid 

OPTIONS INPUT WRAP
   -- Construyendo busqueda
   INPUT BY NAME w_datos.* ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED ) 

		BEFORE INPUT
			CALL DIALOG.setActionHidden("accept",1)
			CALL DIALOG.setActionHidden("cancel",1)

	 AFTER FIELD fecini
		IF w_datos.fecini IS NOT NULL AND w_datos.fecini > TODAY THEN
			CALL box_valdato("Debe ingresar una fecha correcta")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF

	 AFTER FIELD fecfin
		IF w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NULL AND (w_datos.fecini <= TODAY) THEN
			LET w_datos.fecfin = TODAY
			DISPLAY BY NAME w_datos.fecfin
			NEXT FIELD NEXT
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF

   BEFORE FIELD subcat
      CALL librut002_combobox("subcat","SELECT subcat, nomsub FROM inv_subcateg WHERE inv_subcateg.codcat = "||w_datos.codcat)

   BEFORE FIELD cditem
      LET qrycombo = "SELECT cditem, dsitem FROM inv_products WHERE 1 = 1";
      
      IF w_datos.codcat IS NOT NULL THEN
         LET qrycombo = qrycombo CLIPPED,
                        " AND inv_products.codcat = ",w_datos.codcat
      END IF

      IF w_datos.subcat IS NOT NULL THEN
         LET qrycombo = qrycombo CLIPPED,
                        " AND inv_products.subcat = ",w_datos.subcat
      END IF
      CALL librut002_combobox("cditem",qrycombo)
      

    ON CHANGE numnit
    
        SELECT UNIQUE fac_mtransac.nomcli
        INTO w_datos.nomcli
        FROM fac_mtransac
        WHERE fac_mtransac.numnit = w_datos.numnit

        DISPLAY BY NAME w_datos.nomcli
			
	 ON KEY (CONTROL-E)
     LET loop = FALSE
     EXIT INPUT  

   ON ACTION zoom INFIELD numnit
      LET w_datos.numnit = zoom_numnit(FGL_DIALOG_GETBUFFER())
      DISPLAY BY NAME w_datos.numnit

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT  

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 
	  GOTO :label1 

	AFTER INPUT
     -- Obteniendo filtros
	  LABEL label1:
	  IF loop = TRUE THEN
     		LET w_datos.numpos= GET_FLDBUF(w_datos.numpos)
     		LET w_datos.fecini= GET_FLDBUF(w_datos.fecini)
     		LET w_datos.fecfin= GET_FLDBUF(w_datos.fecfin)
 		
		
     		IF w_datos.fecini IS NULL THEN
		  		CALL fgl_winmessage("atention","Debe ingresar la fecha inicial","Atencion")
		  		NEXT FIELD fecini
	  		END IF
		
     		IF w_datos.fecfin IS NULL THEN
					IF w_datos.fecini <= TODAY THEN
						LET w_datos.fecfin = TODAY
						DISPLAY BY NAME w_datos.fecfin
					ELSE
						CALL fgl_winmessage("atention","Debe ingresar un punto de venta","Atencion")
						NEXT FIELD fecfin
					END IF
	  		ELSE
		  		IF w_datos.fecfin < w_datos.fecini THEN
						CALL fgl_winmessage("atention","Fecha final no puede ser mayor a fecha inicial","Atencion")
		      		NEXT FIELD fecfin
		  		END IF
     		END IF
		ELSE
			EXIT INPUT
		END IF
	END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 
 
   LET nombre = "rvtaporpro"
    
   CALL run_report9_to_handler(HANDLER)
 
  END WHILE
 CLOSE WINDOW wrep006a   
END FUNCTION 

 --funcion zoom_numnit()
   #+ A simple zoom window to select the store code
PRIVATE FUNCTION zoom_numnit(l_current_value STRING)
    DEFINE numnit_zoom fgl_zoom.zoomType

    CALL numnit_zoom.init()
    LET numnit_zoom.cancelvalue = l_current_value
    LET numnit_zoom.title = "Selección de NIT"
    LET numnit_zoom.sql = "SELECT %2 FROM fac_mtransac WHERE %1 ORDER BY numnit"

    LET numnit_zoom.multiplerow = TRUE
    # values are (c)har, (i)nteger, (f)loat, (d)ate
    CALL numnit_zoom.column[1].quick_set("numnit", TRUE, "c", 4, "NIT")
    CALL numnit_zoom.column[2].quick_set("nomcli", FALSE, "c", 20, "Nombre")
    CALL numnit_zoom.column[3].quick_set("dircli", FALSE, "c", 20, "Dirección")
    CALL numnit_zoom.column[4].quick_set("telcli", FALSE, "c", 20, "Teléfono")
    CALL numnit_zoom.column[5].quick_set("maicli", FALSE, "c", 15, "Email")
    CALL numnit_zoom.column[6].quick_set("nomcom", FALSE, "c", 2, "Nombre Comercial")
    CALL numnit_zoom.column[7].quick_set("dircom", FALSE, "c", 5, "Dirección Comercial")

    RETURN numnit_zoom.call()
END FUNCTION