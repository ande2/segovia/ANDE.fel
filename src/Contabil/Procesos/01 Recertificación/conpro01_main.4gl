DATABASE segovia

TYPE t_reg RECORD
   serie       LIKE facturafel_e.serie, 
   num_doc     LIKE facturafel_e.num_doc, 
   tipod       LIKE facturafel_e.tipod,
   estado_doc  LIKE facturafel_e.estado_doc, 
   fecha       LIKE facturafel_e.fecha, 
   fac_id      LIKE facturafel_e.fac_id, 
   estatus     LIKE facturafel_e.estatus,
   id_factura  LIKE facturafel_e.id_factura,
   
   rserie       LIKE facturafel_e.serie, 
   rnum_doc     LIKE facturafel_e.num_doc, 
   restado_doc  LIKE facturafel_e.estado_doc, 
   rfecha       CHAR(10), --LIKE facturafel_e.fecha, 
   restatus     LIKE facturafel_e.estatus,
   
   fel_msg     LIKE facturafel_e.fel_msg,
   msg_error   LIKE factura_log.msg_error
END RECORD 

DEFINE g_reg t_reg
DEFINE g_arr DYNAMIC ARRAY OF t_reg 

DEFINE cmbmes INTEGER 
DEFINE cmbserie   CHAR(10)
DEFINE cmbanio    INTEGER 
DEFINE cmbdocs    CHAR(1)
DEFINE i SMALLINT 
DEFINE mensajelg STRING
DEFINE sql_stmt CHAR(250)  
DEFINE sql_stmt2 STRING 

MAIN

CLOSE WINDOW SCREEN 
OPEN WINDOW w1 WITH FORM "conpro01_form1"
 
   CALL consulta()

END MAIN

FUNCTION consulta()
   DEFINE continua SMALLINT 
   DEFINE runcmd  STRING 
   DEFINE f ui.Form
   DEFINE mensaje STRING 
   DEFINE nfecem LIKE facturafel_e.fecha_em

   LET continua = 1
   WHILE continua 
      INPUT BY NAME cmbanio, cmbmes, cmbserie, cmbdocs
         BEFORE INPUT
            --Para anio
            LET sql_stmt="SELECT UNIQUE YEAR(fecha) FROM facturafel_e ORDER BY 1"
            CALL librut002_combobox("cmbanio",sql_stmt)
            LET cmbanio = YEAR(TODAY)
            DISPLAY BY NAME cmbanio
            
            --Para mes
            LET cmbmes = MONTH(TODAY) - 1
            DISPLAY BY NAME cmbmes

            --Para series de documentos   
            LET sql_stmt="select 'TODAS' from systables where tabid = 1 union select unique serie from facturafel_e where month(fecha) = ", cmbmes
            CALL librut002_combobox("cmbserie",sql_stmt)
            LET cmbserie = "TODAS"
            DISPLAY BY NAME cmbserie 

            --Para filtrar documentos pendientes, certificados o ambos
            LET sql_stmt="SELECT 'C', 'Certificados' FROM systables WHERE tabid = 1 UNION ",
                         "SELECT 'P', 'Pendientes'   FROM systables WHERE tabid = 1 UNION ",
                         "SELECT 'T', 'Todos'        FROM systables WHERE tabid = 1  "
            CALL librut002_combobox("cmbdocs",sql_stmt)
            LET cmbdocs = "P"
            DISPLAY BY NAME cmbdocs
            
         ON CHANGE cmbmes
            --Si cambia el mes, actualiza series disponibles
            LET sql_stmt="select 'TODAS' from systables where tabid = 1 union select unique serie from facturafel_e where month(fecha) = ", cmbmes
            CALL librut002_combobox("cmbserie",sql_stmt)
            
      END INPUT 
      IF int_flag THEN
         EXIT WHILE 
      END IF 

      LET sql_stmt2 = "SELECT e.serie, e.num_doc, e.tipod, e.estado_doc, e.fecha, e.fac_id, e.estatus, e.id_factura, e.fel_msg ",
         " FROM facturafel_e e ",
         " WHERE MONTH(e.fecha) = ", cmbmes,
         IIF(cmbserie != "TODAS", SFMT(" AND serie = '%1'", cmbserie),NULL), 
         " AND YEAR(e.fecha) = ", cmbanio,
         IIF(cmbdocs != "T", SFMT(" AND e.estatus = '%1'", cmbdocs),NULL),
         " UNION ",
         " SELECT e.serie, e.num_doc, e.tipod, e.estado_doc, e.fecha, e.fac_id, e.estatus, e.id_factura, e.fel_msg ", 
         " FROM facturafel_e e ",
         " WHERE e.id_factura IN ",
         "   (SELECT fac_id ",
         "    FROM facturafel_e ",
         "    WHERE MONTH(fecha) = ", cmbmes,
         "    AND YEAR(e.fecha) = ", cmbanio, 
              IIF(cmbserie != "TODAS", SFMT(" AND serie = '%1'", cmbserie),NULL),
              IIF(cmbdocs != "T", SFMT(" AND e.estatus = '%1'", cmbdocs),NULL),
         "    ) ",
         IIF(cmbdocs != "T", SFMT(" AND e.estatus = '%1'", cmbdocs),NULL), 
         " ORDER BY 1,2 "
      PREPARE ex_stmt FROM sql_stmt2

      DECLARE ccur CURSOR FOR ex_stmt
         
      CALL g_arr.clear()
      LET i = 0
   
      FOREACH ccur INTO g_reg.serie, g_reg.num_doc, g_reg.tipod, g_reg.estado_doc,
                        g_reg.fecha, g_reg.fac_id, g_reg.estatus, g_reg.id_factura,
                        g_reg.fel_msg, g_reg.msg_error
         LET g_reg.msg_error = NULL 
         --SELECT FIRST 1 msg_error INTO g_reg.msg_error FROM factura_log WHERE correlativo = g_reg.fel_msg

         IF g_reg.id_factura <> 0 THEN 
            SELECT serie, num_doc, estado_doc, fecha, estatus
               INTO g_reg.rserie, g_reg.rnum_doc, g_reg.restado_doc, g_reg.rfecha, g_reg.restatus
               FROM facturafel_e
               WHERE fac_id = g_reg.id_factura
         ELSE
            LET g_reg.id_factura=NULL LET g_reg.rserie=NULL LET g_reg.rnum_doc=NULL LET  g_reg.restado_doc=NULL INITIALIZE g_reg.rfecha TO NULL  LET g_reg.restatus=NULL 
         END IF 
         
         LET i = i + 1
         LET g_arr[i].* = g_reg.*
         DISPLAY "i lleva ", i
      END FOREACH 

      LET mensaje =  "Total de registros encontrados: ", i
      DISPLAY BY NAME mensaje
      WHILE TRUE 
         DISPLAY ARRAY g_arr TO sDet.* ATTRIBUTES(CANCEL=FALSE)
            BEFORE ROW
               LET f = DIALOG.getForm()
               IF g_arr[arr_curr()].estado_doc = "ANULADO" THEN
                  --CALL f.setElementHidden("certificar",1)
                  CALL f.setElementHidden("actua_cert",0)
               ELSE    
                  --CALL f.setElementHidden("certificar",0)
                  CALL f.setElementHidden("actua_cert",1)
               END IF 
               LET mensajelg =  g_arr[arr_curr()].msg_error
               DISPLAY BY NAME mensajelg
               
            ON ACTION certificar
               LET runcmd = "sh runfel.sh ", g_arr[arr_curr()].fac_id 
               RUN runcmd

               --Para mostrar informaci�n actualizada en el arreglo 
               SELECT e.estatus, l.msg_error INTO g_arr[arr_curr()].estatus, g_arr[arr_curr()].msg_error
                  FROM facturafel_e e, factura_log l
                  WHERE e.fel_msg = l.correlativo 
                  AND e.fac_id = g_arr[arr_curr()].fac_id

               IF g_arr[arr_curr()].estatus = 'C' THEN 
                  CALL box_valdato("Documento certificado satisfactoriamente")
               ELSE 
                  CALL box_valdato(g_arr[arr_curr()].msg_error)
               END IF 
               
               EXIT DISPLAY  

            ON ACTION actua_cert
               LET nfecem = SFMT("%1-%2-%3T17:56:05.190-06:00", YEAR(TODAY), MONTH(TODAY) USING "&&", DAY(TODAY))
      
               UPDATE facturafel_e 
                  SET fecha = TODAY, fecha_em = nfecem, fecha_anul = nfecem
                  WHERE fac_id = g_arr[arr_curr()].fac_id
               
               LET runcmd = "sh runfel.sh ", g_arr[arr_curr()].fac_id 
               RUN runcmd

               --Para mostrar informaci�n actualizada en el arreglo despu�s de la certificaci�n 
               SELECT e.estatus, l.msg_error INTO g_arr[arr_curr()].estatus, g_arr[arr_curr()].msg_error
                  FROM facturafel_e e, factura_log l
                  WHERE e.fel_msg = l.correlativo 
                  AND e.fac_id = g_arr[arr_curr()].fac_id
                  
               IF g_arr[arr_curr()].estatus = 'C' THEN 
                  CALL box_valdato("Documento certificado satisfactoriamente")
               ELSE 
                  CALL box_valdato(g_arr[arr_curr()].msg_error)
               END IF 
               
               EXIT DISPLAY     

            ON ACTION ACCEPT 
               EXIT WHILE  
               
            ON ACTION salir
               LET continua = 0 
               EXIT DISPLAY 
         END DISPLAY 
         
         IF continua = 0 THEN
            EXIT WHILE 
         END IF 
      END WHILE 
   END WHILE 
END FUNCTION 
