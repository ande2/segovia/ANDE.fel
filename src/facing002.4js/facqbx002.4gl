{
facqbx002.4gl 
Mynor Ramirez
Corte de Caja 
}

{ Definicion de variables globales }

GLOBALS "facglb002.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbx002_cortescaja(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=2) THEN 
     LET rotulo = "Anular" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facing002_inival(1)
   CALL librut003_cbxcajeros()
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numpos,a.codemp,a.cajero,a.feccor,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facing002_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.lnkcor,b.nompos,c.nomemp,d.nomusr,a.feccor ",
                  "FROM  fac_vicortes a,fac_puntovta b,glb_empresas c,glb_usuarios d ",
                  "WHERE b.numpos = a.numpos ",
                    "AND c.codemp = a.codemp ",
                    "AND d.userid = a.cajero ",
                    "AND ",qrytext CLIPPED,
                  " ORDER BY a.feccor DESC,b.nompos" 

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_datos1 SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_cortes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos1 INTO v_cortes[totlin].*
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos1
   FREE  c_datos1
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_cortes TO s_cortes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facing002_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facing002_inival(1)
      EXIT DISPLAY 

     ON ACTION anular
      -- Anulando  
      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" el corte de caja ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         -- Anulando 
         CALL facing002_grabar(2)
         EXIT DISPLAY
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbx002_datos(v_cortes[1].tlnkcor)
      ELSE 
         CALL facqbx002_datos(v_cortes[ARR_CURR()].tlnkcor)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("anular",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen cortes de caja con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del corte

FUNCTION facqbx002_datos(wlnkcor)
 DEFINE wlnkcor LIKE fac_vicortes.lnkcor,
        existe  SMALLINT,
        qryres  STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_vicortes a "||
              "WHERE a.lnkcor = "||wlnkcor

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_datos1t SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_datos1t INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.numpos,w_mae_pro.codemp,w_mae_pro.cajero,w_mae_pro.feccor 
  DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_datos1t
 FREE  c_datos1t

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.numpos 
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 
