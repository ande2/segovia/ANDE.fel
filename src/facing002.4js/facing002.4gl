{
facing002.4gl 
Mynor Ramirez
Cortes de Caja
}

-- Definicion de variables globales 

GLOBALS "facglb002.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar10")   

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facing002.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL facing002_mainmenu()
END MAIN

-- Subrutina para el menu principal 

FUNCTION facing002_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facing002a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facing002",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Cortes de Caja"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Anular   
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Anular"
    END IF
    
   COMMAND "Buscar"
    " Busqueda de cortes de caja existentes."
    CALL facqbx002_cortescaja(1) 
    
   COMMAND "Nuevo"
    " Emision de un nuevo corte de caja."
    LET savedata = facing002_cortescaja() 
    
   COMMAND "Anular"
    " Anulacion de un corte de caja existente."
    CALL facqbx002_cortescaja(2)
    
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
    
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
    
  END MENU
  CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para la emision de un corte de caja                        

FUNCTION facing002_cortescaja()
 DEFINE loop,existe,opc   SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING   

 -- Inicio del loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     CALL facing002_inival(1)
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.feccor,
                w_mae_pro.numpos,
                w_mae_pro.codemp,
                w_mae_pro.cajero
                
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 
            
   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

	ON CHANGE feccor
		IF w_mae_pro.feccor IS NOT NULL THEN
			IF w_mae_pro.feccor < today - 7 THEN 
				ERROR "Error: Fecha de corte puede ser hasta una semana."
				NEXT FIELD feccor
			ELSE
				IF w_mae_pro.feccor > TODAY THEN
					ERROR "Error: Corte no puede ser mayor a hoy."
					NEXT FIELD feccor
				END IF
			END IF
		ELSE
			ERROR "Error: Debe ingresar una fecha"
			NEXT FIELD feccor
		END IF

   AFTER FIELD numpos 
    --Verificando punto de venta
    IF w_mae_pro.numpos IS NULL THEN
       ERROR "Error: punto de venta invalido, VERIFICA ..."
       NEXT FIELD numpos
    END IF

   AFTER FIELD codemp
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       ERROR "Error: empresa invalida, VERIFICA ..."
       NEXT FIELD codemp
    END IF

   BEFORE FIELD cajero    
    -- Llenando combobox de cajeros del punto de venta y la empresa
    IF librut003_cbxcajeroscorte(w_mae_pro.numpos,w_mae_pro.codemp,w_mae_pro.feccor,1) =0 THEN 
       CALL fgl_winmessage(
       " Atencion: ",
       " No existe ningun cajero disponible para el corte de caja. \n Cortes ya realizados o no hay facturacion emitida.",
       "stop") 
       NEXT FIELD numpos 
    END IF 
   AFTER FIELD cajero 
    --Verificando cajero
    IF w_mae_pro.cajero IS NULL THEN
       ERROR "Error: cajero invalido, VERIFICA ..."
       NEXT FIELD cajero 
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.numpos IS NULL THEN 
       NEXT FIELD numpos
    END IF
    IF w_mae_pro.codemp IS NULL THEN 
       NEXT FIELD codemp
    END IF
    IF w_mae_pro.cajero IS NULL THEN 
       NEXT FIELD cajero
    END IF
	 IF w_mae_pro.feccor IS NULL OR
       w_mae_pro.feccor > TODAY THEN
		 NEXT FIELD feccor
	 END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  LET opc = librut001_menuopcs("Confirmacion",
                               "Desea Realizar el Corte de Caja",
                               "Si",
                               "No",
                               NULL,
                               NULL)

  CASE (opc)
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facing002_grabar(1)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Cancelando
    CALL facing002_inival(1)
    LET loop = FALSE
  END CASE 
 END WHILE

 -- Inicializando datos
 CALL facing002_inival(1)

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar el corte de caja 

FUNCTION facing002_grabar(operacion)
 DEFINE msg        CHAR(80),
        operacion  SMALLINT,
        sqltxt     STRING  

 -- Grabando transaccion
 ERROR " Registrando corte de caja ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

  CASE (operacion)
   WHEN 1 -- Grabando 

    -- Asignando datos
    LET w_mae_pro.lnkcor = 0

    IF w_mae_pro.cajero = "TODOS" THEN
       LET sqltxt = "SELECT UNIQUE a.userid ",
                    " FROM  fac_mtransac a ",
                    " WHERE a.numpos = ", w_mae_pro.numpos,
                    " AND a.codemp = ", w_mae_pro.codemp,
                    " AND a.fecemi = '", w_mae_pro.feccor, "'"
       PREPARE ex_stmt FROM sqltxt               
       DECLARE curall CURSOR FOR ex_stmt
       FOREACH curall INTO w_mae_pro.cajero
         -- Grabando 
         SET LOCK MODE TO WAIT
         INSERT INTO fac_vicortes   
            VALUES (w_mae_pro.*)
         --LET w_mae_pro.lnkcor = SQLCA.SQLERRD[2]
       END FOREACH   
       --Asignando el mensaje 
       LET msg = "Corte de Caja del POS ", w_mae_pro.numpos, " registrado."
         
    ELSE 
       -- Grabando 
       SET LOCK MODE TO WAIT
       INSERT INTO fac_vicortes   
       VALUES (w_mae_pro.*)
       LET w_mae_pro.lnkcor = SQLCA.SQLERRD[2]

       --Asignando el mensaje 
       LET msg = "Corte de Caja # ",w_mae_pro.lnkcor USING "<<<,<<<"," registrado."
    END IF 
    
   WHEN 2 -- Anulando 
    -- Anulando   
    SET LOCK MODE TO WAIT
    DELETE FROM fac_vicortes
    WHERE fac_vicortes.lnkcor = w_mae_pro.lnkcor 

    --Asignando el mensaje 
    LET msg = "Corte de Caja # ",w_mae_pro.lnkcor USING "<<<,<<<"," anulado."
  END CASE 

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 CALL facing002_inival(1)
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing002_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.tipcor = 1   
   LET w_mae_pro.feccor = CURRENT 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando combobox
 CALL librut003_cbxempresas() 
 CALL librut003_cbxpuntosventa()

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.feccor 
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
