{ 
Fecha    : Mayo 2020 
Programo : GYanes 
Objetivo : Programa de variables globales Nota de credito 
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_mnot  RECORD LIKE fac_mnotcre.*,
       w_mae_dnot  RECORD LIKE fac_dnotcre.*,
       
       w_mae_fac   RECORD LIKE fac_mtransac.*,
--       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_tip_doc   RECORD LIKE fac_tipodocs.*,
       w_mae_tdc   RECORD LIKE fac_tdocxpos.*,
       w_mae_res   RECORD LIKE fac_impresol.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       w_mae_pos   RECORD LIKE fac_puntovta.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
       w_med_pro   RECORD LIKE inv_medidpro.*,
       w_mae_fel   RECORD LIKE facturafel_e.*,
       w_enc RECORD 
         serdoc LIKE fac_mnotcre.serdoc,
         numdoc LIKE fac_mnotcre.numdoc,
         fecdoc LIKE fac_mnotcre.fecdoc,
         serie_e LIKE facturafel_e.serie_e,
         numdoc_e LIKE facturafel_e.num_doc,
         autorizacion LIKE facturafel_e.autorizacion,
         exportacion LIKE fac_mnotcre.exportacion,
         codcli LIKE fac_mnotcre.codcli,
         numnit LIKE fac_mnotcre.numnit,
         motanl      LIKE fac_mnotcre.motanl,
         serfac   LIKE fac_mtransac.nserie,
         numfac   LIKE fac_mtransac.numdoc,
         lnktra   INTEGER,
         serie_efac LIKE facturafel_e.serie_e,
         numdoc_efac LIKE facturafel_e.num_doc,
         nomcli   LIKE fac_mnotcre.nomcli,
         dircli   LIKE fac_mnotcre.dircli,
         telcli   LIKE fac_mnotcre.telcli,
         maicli   LIKE fac_mnotcre.maicli,
         credit   LIKE fac_mtransac.credit,
         tipfac   LIKE fac_mtransac.tipfac,
         tascam   LIKE fac_mtransac.tascam
       END RECORD,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem,
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(30), 
        canori     LIKE fac_dtransac.canori, 
        unimto     LIKE fac_dtransac.unimto,
        canuni     LIKE fac_dtransac.cantid,
        preuni     LIKE fac_dtransac.preuni,
        totpro     LIKE fac_dtransac.totpro,
        predol     LIKE fac_dtransac.preuni,
        prtdol     LIKE fac_dtransac.totpro,
        factor     LIKE fac_dtransac.factor,
		  nuitemf	 LIKE inv_dtransac.nuitem,
        deitemh    STRING,
        canfac     LIKE fac_dtransac.cantid,
        preunifac  LIKE fac_dtransac.preuni,
        pretotfac  LIKE fac_dtransac.totpro,
        preunidolfac LIKE fac_dtransac.preuni,
        pretotdolfac LIKE fac_dtransac.totpro
       END RECORD, 
       xnumpos     LIKE fac_mtransac.numpos,
       totuni      DEC(14,2),
       totval      DEC(14,2),
       totvaldol      DEC(14,2),
       reimpresion SMALLINT,
       totlin      SMALLINT,
       nomori      STRING,   
       nomest      STRING,   
       nomdes      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form,
       flag_prec   SMALLINT,
       pre_med     DECIMAL(14,2),
		 flag_especial SMALLINT,
		 flag_tmp    SMALLINT,
		 sin_orden   SMALLINT,
		 tot     LIKE inv_dorden.precio,
     username    LIKE glb_permxusr.userid
       
END GLOBALS