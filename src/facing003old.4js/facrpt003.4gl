{
Fecha    : Junio 2020   
Programo : GYanes 
Objetivo : Impresion de NC.
}

-- Definicion de variables globales  

GLOBALS "facglb003.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECorD,
       existe       SMALLINT, 
       filename     CHAR(90),
       pipeline     CHAR(90),
       nfile   VARCHAR(20),
       npc     VARCHAR (50),
       nprin   VARCHAR(50),
       npath   VARCHAR(100),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt003_facturacion(pct)
 DEFINE pct DEC(9,6),
	opc SMALLINT 
   DEFINE diractual STRING 

   INITIALIZE w_mae_fel.* TO NULL
    SELECT *
    INTO w_mae_fel.*
    FROM facturafel_e
    WHERE facturafel_e.num_int = w_mae_mnot.linknc
    AND facturafel_e.tipod = "NC"
    ;

    IF w_mae_fel.estatus = "P" OR w_mae_fel.estatus IS NULL THEN
      LET opc = 2
    ELSE
      -- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
      LET opc = box_pregunta("Imprimir?") 
    END IF
    
  CASE (opc)
   WHEN 1 -- SI
    -- Definiendo archivo de impresion
    LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt003.txt"

    DISPLAY "FILENAME: ",filename

    -- Asignando valores
    --LET pipeline = "screen"
    LET pipeline = "local"   

    -- Seleccionando fonts para impresora epson
    CALL librut001_fontsprn(pipeline,"epson") 
    RETURNING fnt.* 

    LET diractual = FGL_GETENV("PWD")

    -- Iniciando reporte
    START REPORT facrpt003_printfacfel TO filename 
     OUTPUT TO REPORT facrpt003_printfacfel(1,pct)
    FINISH REPORT facrpt003_printfacfel  
      
    --DISPLAY "num pos ", w_mae_tra.numpos
    -- Imprimiendo el reporte
        CALL librut001_enviareporte(filename, pipeline, "")
      
   WHEN 2 -- NO 
     CALL fgl_winmessage(" Atencion","No Se Imprimira . . .","information")
	
  END CASE

END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 
REPORT facrpt003_printfacfel(numero,pct) 
 DEFINE wcanletras  STRING,
        pct         DEC(9,6),
        wnommes     CHAR(10), 
        exis,i,j    SMALLINT, 
        totdet      SMALLINT, 
        numero,nl   SMALLINT,
        tarjeta     SMALLINT,
        wnomcol     CHAR(30), 
        wnomcla     CHAR(30), 
        lh,lv,si    CHAR(1),
        sd,ii,id    CHAR(1),
        x           CHAR(1),
        medida      VARCHAR(30),
        aniodos     CHAR(10),
        anio        CHAR(2),
		  atras       VARCHAR(20)

        OUTPUT LEFT   MARGIN 1
         PAGE   LENGTH 51
         TOP    MARGIN 0
         BOTTOM MARGIN 3

 FORMAT 
   BEFORE GROUP OF numero 
      -- Imprimiendo encabezado
      LET wcanletras = librut001_numtolet(w_mae_mnot.totdoc) 
      LET wnommes    = librut001_nombremeses(MONTH(w_mae_mnot.fecdoc),1)
      LET aniodos    =w_mae_mnot.fecdoc
      LET anio       = aniodos[9,10]
      LET atras  = ASCII 27,ASCII 25,ASCII 66 --"B"

      -- Inicializando impresora
      PRINT fnt.ini CLIPPED, fnt.t66 CLIPPED, fnt.nrm CLIPPED, fnt.cmp CLIPPED 
      IF w_mae_mnot.numpos = 3 THEN
         SKIP 2 LINES
      ELSE    
 --        SKIP 1 LINES
      END IF    
           
      PRINT COLUMN  78,w_mae_fel.serie_e
      PRINT COLUMN  78,w_mae_fel.numdoc_e
      PRINT COLUMN 78, DAY(w_mae_mnot.fecdoc) USING "&&","-",wnommes CLIPPED,"-", YEAR(w_mae_mnot.fecdoc) USING "&&&&" 
      SKIP 1 LINE         
      PRINT COLUMN 78, w_mae_fel.c_moneda CLIPPED
      PRINT COLUMN 78, w_mae_mnot.lnktra USING "<<<<<<<<<"
      PRINT COLUMN 78, w_mae_fel.num_interno CLIPPED
      SKIP 2 LINE
      PRINT COLUMN  15,w_mae_mnot.nomcli CLIPPED
      SKIP 1 LINE
      PRINT COLUMN  75,w_mae_mnot.numnit CLIPPED
      IF LENGTH(w_mae_mnot.dircli) > 50 THEN
         PRINT COLUMN 22, w_mae_mnot.dircli CLIPPED WORDWRAP RIGHT MARGIN 65
      ELSE
         PRINT COLUMN 22, w_mae_mnot.dircli CLIPPED
         SKIP 1 LINES
      END IF
           
      SKIP 2 LINES  

  ON EVERY ROW
   -- Imprimiendo detalle 
   
      LET totdet = 15 
      FOR i = 1 TO 7
         IF v_products[i].cditem IS NULL THEN
            CONTINUE FOR
         END IF 
         IF v_products[i].canuni <=1 THEN
            IF v_products[i].nomuni MATCHES "*S" THEN
               LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
            END IF
         END IF
         LET totdet = (totdet-1) 
            
         IF v_products[i].nomuni != "UNIDAD" THEN
            --Factura yardas
            PRINT COLUMN  5, fnt.cmp CLIPPED, " ",
                           v_products[i].canuni USING "##,##&.&&" ,
                  COLUMN 24, v_products[i].nomuni CLIPPED, " ",
                           v_products[i].dsitem[1,40] CLIPPED,
                  COLUMN 85, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
         ELSE
            --Factura por tapacarga
            PRINT COLUMN  5, fnt.cmp CLIPPED,  " ",  
                           v_products[i].canuni USING "##,##&.&&" ,
                  COLUMN 24, v_products[i].dsitem[1,40] CLIPPED ,
                  COLUMN 85, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
         END IF
         IF v_products[i].nomuni MATCHES "*S" THEN
            LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
         END IF
         LET totdet = (totdet-1)
         PRINT COLUMN  18, fnt.cmp CLIPPED,
                           "PRECIO ",
                           (v_products[i].preuni*(1+(pct/100))) USING "Q<<,<<&.&&"," C/",v_products[i].nomuni
      
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 17,"-------------- ULTIMA LINEA --------------" 

      -- Saltando lineas extra
      IF (totdet>0) THEN 
         SKIP totdet LINES 
         DISPLAY "linea 209ccc"
      END IF 
    

  ON LAST ROW
   -- Verificando formato 
      -- Imprimiendo final
      SKIP 5 LINES
      PRINT COLUMN  81,w_mae_mnot.totdoc  USING "###,###,##&.&&"
      SKIP 1 LINES 
      PRINT COLUMN  18,wcanletras CLIPPED
      SKIP 1 LINES
      PRINT COLUMN 14, w_mae_fel.autorizacion CLIPPED
      SKIP 2 LINES
      PRINT COLUMN 53, w_mae_fel.fecha_em CLIPPED
      SKIP 1 LINES 
      PRINT COLUMN 10, w_mae_mnot.usrope CLIPPED, TIME  

END REPORT 