{ 
Fecha    : Abril 2011  
Programo : facrep004.4gl 
Objetivo : Reporte de ordenes de trabajo internas  para confeccion 
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_pto RECORD LIKE fac_puntovta.*,
		 w_datos RECORD
			nompos LIKE fac_puntovta.nompos,
			fecini DATE,
			fecfin DATE,
            nomusuario LIKE glb_usuarios.userid,
			orden  VARCHAR(10,1),
		   tipo   INTEGER	
		 END RECORD,
		 arr , s_line INTEGER,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
        w_empr            RECORD LIKE glb_empresas.*,
       wpais     VARCHAR(100), 
       nomusu    VARCHAR(100), 
       existe    SMALLINT,
       filename  STRING,
       fnumpos   STRING
       
       

-- Subrutina principal 

MAIN
DEFINE

      dbname      CHAR(20),
      n_param     SMALLINT,
      prog_name   STRING 
      

 -- Atrapando interrupts
 DEFER INTERRUPT

 LET prog_name = "facrep004" || ".log"
    CALL STARTLOG(prog_name)

 LET n_param = num_args()

   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = ARG_VAL(1)
      DATABASE dbname
   END IF

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep004_ordentra()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep004_ordentra()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        imp1              RECORD LIKE vis_repventa.*, 
        pipeline,qrytxt   STRING,
        strnompos         STRING,
        strfecini         STRING,
        strfecfin         STRING,
        strtipo           STRING,
        userid            VARCHAR(20),
        loop , i , o      SMALLINT,
        lastkey           SMALLINT,
		labono			  DEC(10,2)
        
DEFINE myHandler om.SaxDocumentHandler

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep006a AT 5,2
  WITH FORM "facrep100" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep100",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrep004.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de puntos de venta
  --CALL librut003_cbxpuntosventa() 

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
--	LET w_datos.fec_ini = TODAY
--	LET w_datos.fec_fin = TODAY
   CLEAR FORM
	CALL librut002_combobox("nompos","SELECT nompos, nompos FROM fac_puntovta")
	CALL librut002_combobox("nomusuario","SELECT userid, nomusr FROM glb_usuarios order by 2")

	LET userid = FGL_GETENV("LOGNAME")

	SELECT glb_usuarios.nomusr
	  INTO nomusu
	  FROM glb_usuarios
	 WHERE glb_usuarios.userid = userid 
	 LET w_datos.orden =",1,2"
	 LET w_datos.tipo  = 0 
OPTIONS INPUT WRAP
   -- Construyendo busqueda
   INPUT BY NAME w_datos.* ATTRIBUTES(WITHOUT DEFAULTS )

		BEFORE INPUT
			CALL DIALOG.setActionHidden("accept",1)
			CALL DIALOG.setActionHidden("cancel",1)



	 AFTER FIELD fecini
		IF w_datos.fecini IS NOT NULL AND w_datos.fecini > TODAY THEN
			CALL box_valdato("Debe ingresar una fecha correcta")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF

	 AFTER FIELD fecfin
		IF w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NULL AND (w_datos.fecini <= TODAY) THEN
			LET w_datos.fecfin = TODAY
			DISPLAY BY NAME w_datos.fecfin
			NEXT FIELD NEXT
		END IF
		IF (w_datos.fecini IS NOT NULL AND w_datos.fecfin IS NOT NULL) AND (w_datos.fecini > w_datos.fecfin) THEN
			CALL box_valdato("Fecha inicial no puede ser mayor a fecha final.")
			LET w_datos.fecini=NULL
			LET w_datos.fecfin=NULL
			CLEAR fecfin,fecini
			NEXT FIELD fecini
		END IF
			
	 ON KEY (CONTROL-E)
     LET loop = FALSE
     EXIT INPUT  

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT  

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 
	  GOTO :label1 

	AFTER INPUT
     -- Obteniendo filtros
	  LABEL label1:
	  IF loop = TRUE THEN
     		LET w_datos.nompos= GET_FLDBUF(w_datos.nompos)
     		LET w_datos.fecini= GET_FLDBUF(w_datos.fecini)
     		LET w_datos.fecfin= GET_FLDBUF(w_datos.fecfin)
     		LET w_datos.orden = GET_FLDBUF(w_datos.orden )
     		LET w_datos.tipo  = GET_FLDBUF(w_datos.tipo  )
     		LET w_datos.nomusuario= GET_FLDBUF(w_datos.nomusuario)
		
	  		--IF pipeline IS NULL THEN
        		--NEXT FIELD proveedor
	  		--END IF
		
     		IF w_datos.nompos IS NULL THEN
		  		CALL fgl_winmessage("atention","Debe ingresar un punto de venta","Atencion")
		  		LET strnompos = NULL 
		  		NEXT FIELD nompos
	  		ELSE
					LET strnompos = " a.ptovta = '",w_datos.nompos CLIPPED,"' "
     		END IF


			IF w_datos.tipo IS NOT NULL AND w_datos.tipo <> 0 THEN
				LET strtipo = " AND a.tipo = ",w_datos.tipo USING "<<"
			ELSE
				LET strtipo =NULL
			END IF
		
		
     		IF w_datos.fecini IS NULL THEN
		  		CALL fgl_winmessage("atention","Debe ingresar la fecha inicial","Atencion")
		  		LET strfecini = NULL 
		  		NEXT FIELD fecini
	  		ELSE
					LET strfecini = " AND a.fecemi >= '",w_datos.fecini CLIPPED,"' "
     		END IF
		
     		IF w_datos.fecfin IS NULL THEN
					IF w_datos.fecini <= TODAY THEN
						LET w_datos.fecfin = TODAY
			   		LET strfecfin = " AND a.fecemi <= '",w_datos.fecfin CLIPPED,"' "
						DISPLAY BY NAME w_datos.fecfin
					ELSE
						CALL fgl_winmessage("atention","Debe ingresar un punto de venta","Atencion")
		      		LET strfecfin = NULL 
						NEXT FIELD fecfin
					END IF
	  		ELSE
		  		IF w_datos.fecfin < w_datos.fecini THEN
						CALL fgl_winmessage("atention","Fecha final no puede ser mayor a fecha inicial","Atencion")
						LET strfecfin = NULL
            		NEXT FIELD fecfin
		  		ELSE
			   		LET strfecfin = " AND a.fecemi <= '",w_datos.fecfin CLIPPED,"' "
		  		END IF
     		END IF
		ELSE
			EXIT INPUT
		END IF
	END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 


   -- Construyendo seleccion 
   LET qrytxt = " SELECT a.* ",
                " FROM vis_repventa a ",
                " WHERE ",strnompos CLIPPED, 
                " ",strfecini CLIPPED," ",strfecfin CLIPPED ,strtipo CLIPPED
    IF w_datos.nomusuario IS NOT NULL THEN 
        LET qrytxt = qrytxt CLIPPED, " AND usuario = '", w_datos.nomusuario CLIPPED, "'"
    END IF 
    {IF w_datos.nomusuario IS NULL THEN 
        LET qrytxt = qrytxt CLIPPED, " ORDER BY 3",w_datos.orden
    ELSE}
        LET qrytxt = qrytxt CLIPPED, " ORDER BY usuario, 3",w_datos.orden
    --END IF 
    DISPLAY qrytxt
   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep006 FROM qrytxt 
   DECLARE c_crep006 CURSOR FOR c_rep006
   LET existe = FALSE
   LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
   
   FOREACH c_crep006 INTO imp1.* 
    		IF NOT existe THEN
       		-- Seleccionando fonts para impresora epson
       		CALL librut001_fontsprn("local","epson")
       		RETURNING fnt.*
	 			INITIALIZE w_empr.* TO NULL
	 			SELECT *
	   			INTO w_empr.*
      			FROM glb_empresas
     			WHERE codemp = imp1.codemp

       		LET existe = TRUE
       		LET existe = TRUE
       		START REPORT facrep004_impordenint --CS TO filename
            --START REPORT facrep004_impordenint TO SCREEN --filename
    		END IF 

    		-- Llenando el reporte
    		OUTPUT TO REPORT facrep004_impordenint(imp1.*)
   END FOREACH
   CLOSE c_crep006 
   FREE  c_crep006 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT facrep004_impordenint 

      -- Enviando reporte al destino seleccionado
			--CS CALL librut001_rep_pdf("Facturas",filename,8,"P",3)
      LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
   


--      CALL librut001_enviareporte(filename,pipeline,"Ordenes de Trabajo Internas Para Confeccion") 
 --     ERROR "" 
  --    CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 

  END WHILE
 CLOSE WINDOW wrep006a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT facrep004_impordenint(imp1)
 DEFINE 
	imp1              RECORD LIKE vis_repventa.*,
		  lineas      SMALLINT    ,
		  pagina      SMALLINT    ,
		  total       INTEGER     ,
		  i           SMALLINT    ,
        linea       CHAR(158),
		  labono		  DEC(10,2)

  OUTPUT LEFT   MARGIN 9
         PAGE   LENGTH 80
         --PAGE   LENGTH 53
         TOP    MARGIN 0 
         RIGHT  MARGIN 2 
         BOTTOM MARGIN 0 
  FORMAT 
   FIRST PAGE HEADER
	 LET pagina =1
    LET total  =  0
    LET linea = "__________________________________________________",
                "__________________________________________________________________"
    -- Configurando tipos de letra -- Imprimiendo Encabezado
    PRINT            ASCII 27 ,
          COLUMN  55,"FECHA: ", TODAY USING "dd/mm/yyyy"
    PRINT COLUMN  55,"FACTURAS/TICKETS POR VENDEDOR"
    PRINT COLUMN  00, w_empr.nomemp
    PRINT COLUMN  00, "NIT: ",w_empr.numnit CLIPPED
    PRINT COLUMN  00, "DIRECCION: ",w_empr.diremp[1,20] ,
          COLUMN  55, "PUNTO DE VENTA: ",imp1.ptovta[1,10] CLIPPED
    PRINT COLUMN  00, "TELEFONO: ",w_empr.numtel CLIPPED ,
          COLUMN  55, "DE: ",w_datos.fecini USING "dd/mm/yy"," A ",w_datos.fecfin USING "dd/mm/yy"


    PRINT linea CLIPPED
    PRINT COLUMN   3,"FECHA", 
			 COLUMN  12,"|",
          COLUMN  15,"DOCUMENTO",
			 COLUMN  26,"|",
          COLUMN  31,"SUBTOTAL",
			 COLUMN  42,"|",
			 COLUMN  48, "ANTICIPO",
			 COLUMN  59, "|",
          COLUMN  67,"IVA"	,
			 COLUMN  76,"|",
          COLUMN  83,"VENTA",
			 COLUMN  93,"|",
          COLUMN  99,"OPERO",
			 COLUMN 109,"|",
          COLUMN 112,"TIPO"
    PRINT linea CLIPPED

    --PRINT linea
   PAGE HEADER
    PRINT linea CLIPPED
    PRINT COLUMN   3,"FECHA", 
			 COLUMN  12,"|",
          COLUMN  15,"DOCUMENTO",
			 COLUMN  26,"|",
          COLUMN  31,"SUBTOTAL",
			 COLUMN  42,"|",
			 COLUMN  48, "ANTICIPO",
			 COLUMN  59, "|",
          COLUMN  67,"IVA"	,
			 COLUMN  76,"|",
          COLUMN  83,"VENTA",
			 COLUMN  93,"|",
          COLUMN  99,"OPERO",
			 COLUMN 109,"|",
          COLUMN 112,"TIPO"
    PRINT linea CLIPPED



   ON EVERY ROW
	 -- Buscando si la factura tiene abono
	 LET labono = 0
	 IF imp1.tipo = 2 THEN
		IF imp1.totpag < imp1.subtot THEN
			 SELECT	totabo
				INTO	labono
				FROM	inv_morden
				WHERE lnktra = imp1.serial	
			IF STATUS = NOTFOUND OR labono IS NULL THEN
				LET labono = 0
			END IF
			IF labono > 0 THEN
				LET imp1.totiva = 0
				LET imp1.totpag = 0
			END IF
		END IF
	END IF
		
    -- Imprimiendo ordenes 
    PRINT COLUMN   1, imp1.fecemi   USING "dd/mm/yyyy"    ,
			 COLUMN  12, "|",
			 COLUMN  15, imp1.docvta   CLIPPED                ,
			 COLUMN  26, "|",
          COLUMN  23, imp1.subtot   USING "#,###,##&.&&",
			 COLUMN  42, "|",
			 COLUMN  45, labono    		USING "#,###,##&.&&",
			 COLUMN  59, "|",
          COLUMN  62, imp1.totiva   USING "#,###,##&.&&",
			 COLUMN  76, "|",
			 COLUMN  79, imp1.totpag   USING "#,###,##&.&&",
			 COLUMN  93, "|",
			 COLUMN  96, imp1.usuario[1,10]                   ,  
			 COLUMN 109, "|"                                  ,  
			 COLUMN 112, imp1.tipdoc[1,3]

	ON LAST ROW
		PRINT linea CLIPPED
		PRINT COLUMN   1, "TOTALES:",
				COLUMN  14, "DOCS: ", COUNT(*) USING "<<<<<<<<<",
		      COLUMN  28, SUM(imp1.subtot) USING "#,###,##&.&&",
				COLUMN  45, SUM(labono)      USING "#,###,##&.&&",
			   COLUMN  62, SUM(imp1.totiva) USING "#,###,##&.&&",
				COLUMN  79, SUM(imp1.totpag) USING "#,###,##&.&&"
		
END REPORT 
