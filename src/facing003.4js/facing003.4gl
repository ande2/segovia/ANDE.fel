GLOBALS "facglb003.4gl"
CONSTANT programa = "facing003" 

DEFINE tasimp      DEC(9,6), 
       tascom      DEC(9,6), 
       tasvta      DEC(9,6), 
       vuelto      DEC(10,2), 
       portar      DEC(9,6),
       facord      DEC(9,6),
       haytar      SMALLINT, 
       wpais       VARCHAR(255), 
       regreso     SMALLINT,
       ordenes     SMALLINT, 
       existe      SMALLINT,
       msg         STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar9")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facing003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Logeando usuario 
 CALL facing003_login(0)
 RETURNING regreso,username 
 LET flag_tmp=FALSE

 -- Verificanod regreso
 IF NOT regreso THEN 
    -- Menu de opciones
    CALL facing003_menu()
 END IF
 
CALL facing003_menu()
END MAIN

-- Subutina para el menu de facturacion 

FUNCTION facing003_menu()
 DEFINE regreso    SMALLINT, 
        titulo     STRING,
       vfac_id INTEGER,
      cmd STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "facing003a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("facing003",wpais,1)

  -- Desplegando campo de motivo de anulacion
  CALL f.setFieldHidden("motanl",1)
  CALL f.setElementHidden("labeld",1)
--  CALL f.setElementHidden("group4",1)
  CALL f.setElementHidden("labelg",1)
  CALL f.setFieldHidden("obspre",1)
  --CALL f.setElementHidden("group7",1)

  -- Cargando unidades de medida
  CALL librut003_cbxunidadesmedida("unimto") 

  -- Inicializando datos 
  CALL facing003_inival(1)

  -- Menu de opciones
  MENU " Opciones" 
  {
   ON ACTION Fac_electronica

      --IF jjjjjj
      LET vfac_id = NULL;
      
      SELECT a.fac_id
      INTO vfac_id
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      
      LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
      RUN cmd
      }
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
	 SHOW OPTION ALL
    IF NOT seclib001_accesos(username,4) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(username,1) THEN
       HIDE OPTION "Ingresar"
    END IF
    -- Anular     
    IF NOT seclib001_accesos(username,2) THEN
       HIDE OPTION "Anular"
    END IF
	 
   COMMAND "Consultar" 
    " Consulta de documentos de facturacion existentes."
    --CALL facqbx001_facturacion(1)
   COMMAND "Ingresar" 
    " Emision de facturacion."
    LET ordenes = FALSE 
    CALL facing003_facturacion(1) 
   COMMAND "Anular"
    " Anulacion de documentos de facturacion existentes."
   -- CALL facqbx001_facturacion(2)
	 
	 
   COMMAND "Salir"
    " Abandona el menu de facturacion." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para ingresar el cajero y el punto de venta
FUNCTION facing003_login(opc)
 DEFINE x         RECORD 
         userid   CHAR(15),
         passwd   CHAR(10),
         numpos   SMALLINT 
        END RECORD,
        wfecha    VARCHAR(80), 
        xnompos   VARCHAR(40), 
        msg       STRING, 
        regreso   SMALLINT,
        loop      SMALLINT,
        seleccion SMALLINT,
        conteo    SMALLINT,
        opc       SMALLINT,
        w2        ui.Window,
        f2        ui.Form,
        sql_stmt STRING 

 -- Abriendo la ventana del login
 OPEN WINDOW wing001b AT 5,2  
  WITH FORM "facing001b" ATTRIBUTE(BORDER)

  -- Desplegando fecha 
  LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
  CALL librut001_dpelement("labelz",wfecha)

  -- Desactivando salida automatica del input 
  OPTIONS INPUT WRAP 

  -- Escondiendo punto de venta
  LET w2 = ui.Window.getCurrent()
  LET f2 = w2.getForm()
  --CALL f2.setFieldHidden("formonly.numpos",1)
  
  --CALL f2.setElementHidden("labelx",1)
  CALL f2.setElementHidden("labelb",1)
  CALL f2.setElementHidden("labelc",1)

  -- Verificando si cajero es ingresado o tomado del sistema 
  INITIALIZE x.* TO NULL 
  IF (opc=1) THEN 
     -- Obteniendo cajero logeado
     LET x.userid = FGL_GETENV("LOGNAME")
     DISPLAY BY NAME x.userid 
  END IF 

  -- Ingresando datos
  LET loop = TRUE
  WHILE loop
   INPUT BY NAME x.userid,
                 x.passwd, x.numpos WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)
   
    ON ACTION cancelar
     -- Salida 
     LET regreso = TRUE
     LET loop    = FALSE 
     EXIT INPUT 

    ON KEY(F4,CONTROL-E) 
     -- Salida 
     LET regreso = TRUE 
     LET loop    = FALSE 
     EXIT INPUT 

    ON ACTION aceptar  
     -- Verificando datos
     IF x.userid IS NULL THEN
        ERROR "Error: usuario invalido, VERIFICA ..."
        NEXT FIELD userid
     END IF 
     IF x.passwd IS NULL THEN
        ERROR "Error: password invalido, VERIFICA ..."
        NEXT FIELD passwd
     END IF 

     LET regreso = FALSE
     LET loop    = TRUE 
     EXIT INPUT 

    BEFORE INPUT
     -- Verificando si cajero es ingresado o tomado del sistema 
     IF (opc=1) THEN
        CALL Dialog.SetFieldActive("userid",0) 
     END IF 

    AFTER FIELD userid 
     -- Verificando usuario 
     IF (LENGTH(x.userid)=0) THEN 
        ERROR "Error: usuario invalido, VERIFICA ..."
        NEXT FIELD userid
     ELSE 
        LET sql_stmt = "SELECT u.numpos, p.nompos FROM fac_usuaxpos u,fac_puntovta p WHERE u.numpos = p.numpos AND u.userid = '", x.userid CLIPPED, "'" 
        --CALL combo_din2("numpos",sql_stmt)
        CALL librut002_combobox("numpos",sql_stmt)
     END IF 

    AFTER FIELD passwd
     -- Verificando password
     IF (LENGTH(x.passwd)=0) THEN 
        ERROR "Error: password invalido, VERIFICA ..."
        NEXT FIELD passwd
     END IF 
    AFTER FIELD numpos
      IF (LENGTH(x.numpos)=0) THEN
         ERROR "Seleccione Punto de Venta"
         NEXT FIELD numpos
      END IF
     EXIT INPUT  
   END INPUT

   -- Verificando puntos de venta por cajero
   IF loop THEN 
    -- Buscando puntos de venta por cajero 
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_usuaxpos a
     WHERE a.userid = x.userid
     IF (conteo=0) THEN
        LET msg = "Cajero (",x.userid CLIPPED,") no tiene puntos de venta asignados. \n VERIFICA ..."

        CALL fgl_winmessage(
        " Atencion",
        msg, 
        "stop")
        CONTINUE WHILE 
     ELSE
        LET xnumpos = NULL
        SELECT a.numpos 
         INTO  xnumpos   
         FROM  fac_usuaxpos a,fac_puntovta y
         WHERE a.numpos = y.numpos 
           AND a.userid = x.userid
           AND a.passwd = x.passwd
         IF (status=NOTFOUND) THEN
            ERROR "Atencion: password incorrecto, VERIFICA ..."
            CONTINUE WHILE  
         END IF 
         LET xnumpos = x.numpos
         LET loop = FALSE 
     END IF 
   END IF 
  END WHILE

  -- Activando salida automatica del input 
  OPTIONS INPUT NO WRAP 
  
 CLOSE WINDOW wing001b

 RETURN regreso,x.userid 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing003_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_mnot.*,w_mae_dnot.* TO NULL 
	
   CLEAR FORM 
 END CASE 
 -- Inicializando datos
 LET w_mae_mnot.numpos = xnumpos
 LET w_mae_mnot.estado = "V" 
 LET w_mae_mnot.exportacion = "0"
 LET w_mae_mnot.fecdoc = TODAY 
 LET w_mae_mnot.poriva = tasimp 
 LET w_mae_mnot.usrsis = username 
 LET w_mae_mnot.fecsis = CURRENT 
 LET w_mae_mnot.horsis = CURRENT HOUR TO SECOND
 LET w_mae_mnot.subtotal = 0
 LET w_mae_mnot.totiva = 0
 LET w_mae_mnot.totdoc = 0
 LET totlin           = 0 
 LET totuni           = 0
 LET totval           = 0
 LET vuelto           = 0 

 -- Obteniendo datos del punto de venta
 INITIALIZE w_mae_pos.* TO NULL
 CALL librut003_bpuntovta(w_mae_mnot.numpos)
 RETURNING w_mae_pos.*,existe 
 CALL f.setElementText("labela","PUNTO DE VENTA [ "||w_mae_pos.nompos CLIPPED||" ]")

 -- Llenando combo de tipos de documento x punot de venta
 --CALL librut003_cbxtiposdocxpos(w_mae_tra.numpos)

 -- Inicializando vectores de datos
 CALL facing003_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_mnot.usrsis,w_mae_mnot.fecdoc,w_mae_mnot.horsis
 DISPLAY BY NAME totuni,totval,totlin,vuelto
 --DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.cheque,w_mae_tra.tarcre,w_mae_tra.ordcom,w_mae_tra.totiva,w_mae_tra.totdoc
 DISPLAY BY NAME w_mae_mnot.estado
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 
FUNCTION facing003_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_product.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_product[i].*
 END FOR 
END FUNCTION

-- Subrutina para el ingreso de los datos del encabezado de la facturacion 

FUNCTION facing003_facturacion(operacion)
 DEFINE userauth          LIKE glb_permxusr.userid,
        xnomcli           LIKE fac_mtransac.nomcli, 
        xnumnit           LIKE fac_mtransac.dircli,
        xdircli           LIKE fac_mtransac.dircli,
        xtelcli           LIKE fac_mtransac.telcli,
        xdocto            CHAR(20), 
        retroceso         SMALLINT,
        operacion,acceso  SMALLINT,
        loop,existe,i     SMALLINT


IF operacion = 1 THEN
   LET w_enc.fecdoc = TODAY
   LET w_enc.serdoc = "A"
   
END IF
 -- Obteniendo tasa de impuesto
 CALL librut003_parametros(8,1)
 RETURNING existe,tasimp
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de impuesto.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de dolares compra
 CALL librut003_parametros(9,1)
 RETURNING existe,tascom
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de compra de dolares.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de venta de dolares 
 CALL librut003_parametros(9,2)
 RETURNING existe,tasvta
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrada la tasa de compra de dolares.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo tasa de incremento por uso de tc
 CALL librut003_parametros(11,1)
 RETURNING existe,portar
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " No existe registrado el % de incremento por uso de TARJETA DE CREDITO.\n Definir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

-- Inicio del loop
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF 

 LET loop  = TRUE
 WHILE loop   

 	LET flag_especial = FALSE
	LET sin_orden = 0

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL facing003_inival(1) 
     CALL f.setFieldHidden("preuni",0)
     CALL f.setFieldHidden("totpro",0)
     CALL f.setFieldHidden("totval",0)
     CALL f.setFieldHidden("predol",1)
     CALL f.setFieldHidden("prtdol",1)
     CALL f.setFieldHidden("totvaldol",1)
     --CALL f.setElementHidden("group7",1)
  END IF
 
  -- Ingresando datos
  {
  INPUT BY NAME --w_mae_mnot.lnktdc,
                w_mae_mnot.serdoc,
                w_mae_mnot.numdoc,
		          w_mae_mnot.fecdoc,
         
                w_mae_mnot.codcli,
                w_mae_mnot.numnit,
                w_mae_mnot.nomcli,
                w_mae_mnot.dircli,
                w_mae_mnot.telcli,
                w_mae_mnot.maicli,
                --w_mae_mnot.credit,
                w_mae_mnot.exportacion,
                w_mae_mnot.tascam
                }
   INPUT BY NAME 
         w_enc.serdoc,
         w_enc.fecdoc,
         w_enc.codcli,
         w_enc.numnit,
         w_enc.serfac,
         w_enc.numfac,
         w_enc.lnktra,
         w_enc.serie_efac,
         w_enc.numdoc_efac
         WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Escondiendo botom de modificar el numero de documento y boton de anular numero de documento  
  --  CALL DIALOG.setActionActive("modnumdoc",FALSE)
--    CALL DIALOG.setActionActive("anunumdoc",FALSE)

    -- Deshabilitando campo de numero de documento 
   -- CALL DIALOG.setFieldActive("numdoc",FALSE)

    -- Salida 
     LET loop = FALSE 
     EXIT INPUT 
    

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

    
   
   {

   ON ACTION modnumdoc 
    -- Verificando acceso para modificar el numero de documento
    -- Ingresando confirmacion de cajero
    CALL facing003_confirmacion() 
    RETURNING acceso,userauth 
    IF acceso THEN 
       -- Habilitando campo de numero de documento 
       CALL DIALOG.setFieldActive("numdoc",TRUE)
       CALL DIALOG.setFieldActive("fecemi",TRUE)
       NEXT FIELD numdoc 
    END IF 

   ON ACTION anunumdoc
    -- Obteniendo numero de documento 
    LET xdocto = GET_FLDBUF(w_mae_tra.numdoc)
    LET w_mae_mnot.numdoc = xdocto
    IF LENGTH(w_mae_mnot.numdoc)=0 THEN
       ERROR "Error: debe ingresarse el numero de documento, VERIFICA ..."
       NEXT FIELD numdoc 
    END IF
   } 
{
    -- Verificando si el documento existe
    IF facing003_bdocumento() THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Numero de documento ya existe registrado, \n No puede ANULARSE con esta opcion.",
       "stop")
       NEXT FIELD numdoc
    END IF 

    -- Anulando documentos no grabados 
    -- Verificando anulacion
    IF librut001_yesornot("Confirmacion",
                          "Esta Seguro de Anular el Documento ?",
                          "Si",
                          "No",
                          "question") THEN

       -- Grabando documento como anulado 
       CALL facing003_anular(userauth)

       -- Inicializando datos
       CALL Dialog.setActionActive("anunumdoc",FALSE)
       CALL Dialog.SetFieldActive("lnktdc",TRUE) 
       CALL facing003_inival(1)
       NEXT FIELD lnktdc 
    END IF
}
   ON ACTION listcliente 
    -- Seleccionado lista de clientes
    CALL librut002_formlistcli("Consulta de Clientes","Cliente","Nombre del Cliente","Numero de Nit",
                               "codcli","nomcli","numnit","fac_clientes","1=1",2,1,1)
    RETURNING w_enc.codcli,w_enc.nomcli,w_enc.numnit,regreso
    IF regreso THEN
       LET w_enc.codcli = NULL 
       LET w_enc.nomcli = NULL 
       LET w_enc.numnit = NULL 
       LET w_enc.dircli = NULL 
       CLEAR codcli,nomcli,numnit,dircli 
       NEXT FIELD codcli
    ELSE 
       DISPLAY BY NAME w_enc.codcli,w_enc.nomcli,w_enc.numnit 
       NEXT FIELD codcli
    END IF 

   ON ACTION listfac
      CALL librut002_selectlist("CONSULTA DE FACTURAS","SERIE","NUMERO","nserie","numdoc","fac_mtransac","numnit = '"||w_enc.numnit||"'","1,2 desc")
      RETURNING w_enc.serfac, w_enc.numfac, int_flag
      IF int_flag THEN
         LET int_flag = FALSE
         LET w_enc.serfac = NULL 
         LET w_enc.numfac = NULL
         CLEAR serfac, numfac
      ELSE
         SELECT fac_mtransac.lnktra, fac_mtransac.nomcli, fac_mtransac.dircli, fac_mtransac.telcli, fac_mtransac.maicli,
         fac_mtransac.credit, fac_mtransac.tipfac, fac_mtransac.tascam, 
         facturafel_e.serie_e, facturafel_e.numdoc_e
         INTO w_enc.lnktra, w_enc.nomcli, w_enc.dircli, w_enc.telcli, w_enc.maicli,
         w_enc.credit, w_enc.tipfac, w_enc.tascam, 
         w_enc.serie_efac, w_enc.numdoc_efac
         FROM fac_mtransac, OUTER(facturafel_e)
         WHERE fac_mtransac.nserie = w_enc.serfac
         AND fac_mtransac.numdoc = w_enc.numfac
         AND facturafel_e.num_int = fac_mtransac.lnktra
         AND facturafel_e.tipod = "FC"
         
         DISPLAY BY NAME w_enc.serfac, w_enc.numfac, w_enc.lnktra, 
         w_enc.nomcli, w_enc.dircli, w_enc.telcli, w_enc.maicli,
         w_enc.credit, w_enc.tipfac, w_enc.tascam, 
         w_enc.serie_efac, w_enc.numdoc_efac, w_enc.autorizacion
      END IF

ON ACTION listface
      CALL librut002_selectlist("CONSULTA DE FACTURAS","SERIE","NUMERO","nserie","numdoc","fac_mtransac","numnit = '"||w_enc.numnit||"'","1,2 desc")
      RETURNING w_enc.serfac, w_enc.numfac, int_flag
      IF int_flag THEN
         LET int_flag = FALSE
         LET w_enc.serfac = NULL 
         LET w_enc.numfac = NULL
         CLEAR serfac, numfac
      ELSE
         SELECT fac_mtransac.lnktra, fac_mtransac.nomcli, fac_mtransac.dircli, fac_mtransac.telcli, fac_mtransac.maicli,
         fac_mtransac.credit, fac_mtransac.tipfac, fac_mtransac.tascam, 
         facturafel_e.serie_e, facturafel_e.numdoc_e
         INTO w_enc.lnktra, w_enc.nomcli, w_enc.dircli, w_enc.telcli, w_enc.maicli,
         w_enc.credit, w_enc.tipfac, w_enc.tascam, 
         w_enc.serie_efac, w_enc.numdoc_efac
         FROM fac_mtransac, OUTER(facturafel_e)
         WHERE fac_mtransac.nserie = w_enc.serfac
         AND fac_mtransac.numdoc = w_enc.numfac
         AND facturafel_e.num_int = fac_mtransac.lnktra
         AND facturafel_e.tipod = "FC"
         
         DISPLAY BY NAME w_enc.serfac, w_enc.numfac, w_enc.lnktra, 
         w_enc.nomcli, w_enc.dircli, w_enc.telcli, w_enc.maicli,
         w_enc.credit, w_enc.tipfac, w_enc.tascam, 
         w_enc.serie_efac, w_enc.numdoc_efac, w_enc.autorizacion
      END IF

   
   BEFORE INPUT
   {
    -- Deshabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)
    -- Deshabilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",FALSE)
    -- Deshabilitando campo de numero de documento 
    CALL DIALOG.setFieldActive("numdoc",FALSE)
    CALL DIALOG.setFieldActive("fecdoc",FALSE)
    IF w_mae_mnot.exportacion = "L" THEN
         LET w_mae_mnot.tascam = NULL
         CALL f.setFieldHidden("preuni",0)
         CALL f.setFieldHidden("totpro",0)
         CALL f.setFieldHidden("totval",0)
         CALL f.setFieldHidden("predol",1)
         CALL f.setFieldHidden("prtdol",1)
         CALL f.setFieldHidden("totvaldol",1)
         CALL f.setElementHidden("group7",1)
      ELSE
         LET w_mae_mnot.tascam = tascom
         CALL f.setFieldHidden("preuni",1)
         CALL f.setFieldHidden("totpro",1)
         CALL f.setFieldHidden("totval",1)
         CALL f.setFieldHidden("predol",0)
         CALL f.setFieldHidden("prtdol",0)
         CALL f.setFieldHidden("totvaldol",0)
         CALL f.setElementHidden("group7",0)
      END IF
      DISPLAY BY NAME w_mae_mnot.tascam
      }
{
   BEFORE FIELD lnktdc 
    -- Verificando si es regreso se mueve al campo de cliente omitiendo el campo de tipo de documento
    IF retroceso THEN
       CALL Dialog.SetFieldActive("lnktdc",FALSE) 
       LET retroceso = FALSE 
    END IF 

   AFTER FIELD lnktdc
    -- Verificando tipo de documento
    IF w_mae_tra.lnktdc IS NULL THEN
       ERROR "Error: tipo de documento invalido, VERIFICA ..."
       NEXT FIELD lnktdc 
    END IF 
    CALL DIALOG.setFieldActive("codcli",TRUE)
 
    -- Obteniendo datos del tipo de documento 
    INITIALIZE w_tip_doc.* TO NULL
    CALL librut003_btipodoc(w_mae_tra.tipdoc)
    RETURNING w_tip_doc.*,regreso
    -- Verificando si el documento existe 
    SELECT UNIQUE a.codemp
     FROM  fac_mtransac a
     WHERE (a.codemp = w_mae_tra.codemp)
       AND (a.tipdoc = w_mae_tra.tipdoc)
       AND (a.nserie = w_mae_tra.nserie)
    IF (status=NOTFOUND) THEN 
       -- Verificando si existe resolucion 
       INITIALIZE w_mae_res.* TO NULL 
       CALL librut003_bresolucion(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie)
       RETURNING w_mae_res.*,existe
       IF existe THEN 
          -- Obteniendo correlativo inicial de la resolucion 
          LET w_mae_tra.numdoc = w_mae_res.numini 
       ELSE
          -- Obteniendo correaltivo inicial del tipo de documento x punto de venta
          LET w_mae_tra.numdoc = w_mae_tdc.numcor
       END IF 
    ELSE
       -- Buscando correlativo del tipo de documento 
       LET w_mae_tra.numdoc = librut003_correltipdoc(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie,0,0)

       -- Verificando si existe resolucion 
       INITIALIZE w_mae_res.* TO NULL 
       CALL librut003_bresolucion(w_mae_tra.codemp,w_mae_tra.tipdoc,w_mae_tra.nserie)
       RETURNING w_mae_res.*,existe
       IF existe THEN
          -- Verificando si el correlativo esta fuera de la resolucion 
          IF w_mae_tra.numdoc >= w_mae_res.numini AND
             w_mae_tra.numdoc <= w_mae_res.numfin THEN
             -- Correlativo correcto
          ELSE
             CALL fgl_winmessage(
             " Atencion",
             " Numero de documento fuera de resolucion. \n VERIFICA correlativos de la resolucion.", 
             "stop")
          END IF 
       END IF 
    END IF 

    -- Desplegando numero de documento
	 IF w_tip_doc.tipdoc = 2 AND w_mae_tra.numpos=1 AND w_mae_tra.nserie = "G" THEN
       CALL Dialog.SetFieldActive("numnit",0)
       CALL Dialog.SetFieldActive("nomcli",0)
       CALL Dialog.SetFieldActive("dircli",0)
       CALL Dialog.SetFieldActive("telcli",0)
		 LET w_mae_tra.codcli = 54
	 ELSE
	 	 LET w_mae_tra.codcli=1
		 --CALL DIALOG.setFieldActive("numnit",0)
		 --CALL DIALOG.setFieldActive("nomcli",0)
		 --CALL DIALOG.setFieldActive("dircli",0)
		 --CALL DIALOG.setFieldActive("telcli",0)
	 END IF
}
{
   BEFORE FIELD numdoc 
    -- Habilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",TRUE)
    -- DesHabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)

   AFTER FIELD numdoc
    -- Verificando campo
    IF w_mae_mnot.numdoc IS NULL OR
       (w_mae_mnot.numdoc <=0) THEN 
       ERROR "Error: numero de documento invalido, VERIFICA ..." 
       LET w_mae_mnot.numdoc = NULL
       CLEAR numdoc
       NEXT FIELD numdoc  
    END IF 

    -- Verificanod si numero de documento existe
    IF facing003_bdocumento() THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Numero de documento ya existe registrado. \n VERIFICA ...",
       "stop")
       NEXT FIELD numdoc 
    END IF 

    -- Deshabilitando campo de numero de documento 
    CALL DIALOG.setFieldActive("numdoc",FALSE)

    -- Deshabilitando botom de anular el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",TRUE)

    -- Habilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("anunumdoc",FALSE)

--   BEFORE FIELD codcli
    -- Habilitando botom de modificar el numero de documento
--	 LET w_mae_tra.codcli=1
--	 NEXT FIELD NEXT

	AFTER FIELD fecdoc
		CALL DIALOG.setFieldActive("fecdoc",FALSE)
   --ON CHANGE codcli 
   BEFORE FIELD codcli 
    -- Verificando cliente
    IF w_mae_mnot.codcli IS NULL THEN
       ERROR "Error: debe ingresarse el cliente a facturar, VERIFICA ..." 
       LET w_mae_mnot.codcli = NULL 
       LET w_mae_mnot.nomcli = NULL 
       LET w_mae_mnot.numnit = NULL
       LET w_mae_mnot.dircli = NULL 
       LET w_mae_mnot.telcli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli
    END IF 

    -- Buscando datos del cliente
	IF w_mae_mnot.numnit IS NULL AND w_mae_mnot.nomcli IS NULL THEN
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_mnot.codcli)
    RETURNING w_mae_cli.*,existe 
    IF NOT existe THEN
       INITIALIZE w_mae_mnot.codcli TO NULL 
       CALL fgl_winmessage(" Atencion","Cliente no existe registrado. \nVERIFICA ...","stop")
       LET w_mae_mnot.codcli = NULL 
       LET w_mae_mnot.nomcli = NULL 
       LET w_mae_mnot.numnit = NULL
       LET w_mae_mnot.dircli = NULL 
       LET w_mae_mnot.telcli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli 
    END IF 

    -- Obteniendo datos del maestro de clientes 
    IF w_mae_mnot.numnit <> "CF" OR w_mae_mnot.nomcli <> "CONSUMIDOR FINAL" OR w_mae_mnot.nomcli <> "CLIENTES VARIOS" OR
       w_mae_mnot.numnit IS NULL OR w_mae_mnot.nomcli IS NULL THEN
       LET w_mae_mnot.nomcli = w_mae_cli.nomcli
       LET w_mae_mnot.numnit = w_mae_cli.numnit 
       LET w_mae_mnot.dircli = w_mae_cli.dircli
       LET w_mae_mnot.telcli = w_mae_cli.numtel
    END IF
    DISPLAY BY NAME w_mae_mnot.nomcli,w_mae_mnot.numnit,w_mae_mnot.dircli,w_mae_mnot.telcli 
	END IF
    -- Verificando si cliente esta autorizado para facturar
    IF NOT w_mae_cli.hayfac THEN
       CALL fgl_winmessage(" Atencion"," Cliente no autorizado para facturar. \n VERIFICA ...","stop")
       LET w_mae_mnot.codcli = NULL 
       LET w_mae_mnot.nomcli = NULL 
       LET w_mae_mnot.numnit = NULL
       LET w_mae_mnot.dircli = NULL 
       CLEAR codcli,nomcli,numnit,dircli,telcli 
       NEXT FIELD codcli 
    END IF 

    -- Verificando si cliente no tiene ingreso de datos
    IF NOT w_mae_cli.haydat THEN
       IF LENGTH(w_mae_mnot.numnit)=0 THEN LET w_mae_mnot.numnit = "CF" END IF 
       IF LENGTH(w_mae_mnot.nomcli)=0 THEN LET w_mae_mnot.nomcli = "CONSUMIDOR FINAL" END IF 
       IF LENGTH(w_mae_mnot.dircli)=0 THEN LET w_mae_mnot.dircli = "CIUDAD" END IF 
       CALL Dialog.SetFieldActive("numnit",0) 
       CALL Dialog.SetFieldActive("nomcli",0) 
       CALL Dialog.SetFieldActive("dircli",0) 
       CALL Dialog.SetFieldActive("telcli",0) 
    ELSE 
       DISPLAY BY NAME w_mae_mnot.numnit,w_mae_mnot.nomcli,w_mae_mnot.dircli,w_mae_mnot.telcli
       CALL Dialog.SetFieldActive("numnit",1) 
       CALL Dialog.SetFieldActive("nomcli",1) 
       CALL Dialog.SetFieldActive("dircli",1) 
       CALL Dialog.SetFieldActive("telcli",1) 
    END IF 
	 NEXT FIELD NEXT

    -- Deshabilitando botom de modificar el numero de documento
    CALL DIALOG.setActionActive("modnumdoc",FALSE)
    }

    AFTER FIELD codcli
     IF w_enc.codcli IS NULL THEN
       LET w_enc.codcli = NULL
       LET w_enc.nomcli = NULL
       LET w_enc.numnit = NULL
       LET w_enc.dircli = NULL
       LET w_enc.telcli = NULL
       --CS CALL fgl_winmessage(" Atencion","Debe ingresarse el cliente. \nVERIFICA ...","stop")
       --CS CLEAR codcli,nomcli,numnit,dircli,telcli
       --CS NEXT FIELD codcli
     END IF


	BEFORE FIELD numnit
    CALL DIALOG.setFieldActive("codcli",FALSE)
    --CALL DIALOG.setActionActive("modnumdoc",TRUE)
   -- LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 

   AFTER FIELD numnit
    -- Verificanod nit
    IF LENGTH(w_enc.numnit)=0 THEN 
       LET w_enc.numnit = "CF" 
       DISPLAY BY NAME w_enc.numnit 
    ELSE
		IF w_enc.codcli = 0 OR w_enc.codcli = 1 THEN
       -- Verificando si cliente ya existe registrado por el numero de NIT
       INITIALIZE xnomcli,xdircli TO NULL
       IF w_enc.numnit!="CF" THEN 
		  INITIALIZE xnumnit,xdircli,xtelcli TO NULL
        SELECT a.nomcli,a.dircli,a.telcli 
         INTO  xnomcli,xdircli,xtelcli
         FROM  fac_mnotcre a
         WHERE a.linknc = (SELECT MAX(x.linknc)
                            FROM  fac_mnotcre x
                            WHERE x.numnit = w_mae_mnot.numnit)
         IF (status!=NOTFOUND) THEN
           -- Asignando datos
            LET w_enc.nomcli = xnomcli  
            LET w_enc.dircli = xdircli
            LET w_enc.telcli = xtelcli
            DISPLAY BY NAME w_enc.nomcli,w_enc.dircli,w_enc.telcli 
         END IF 
       END IF 
		 END IF
    END IF 
     
  END INPUT

 
  
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del movimiento 
  LET retroceso = facing003_detingreso()
 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL facing003_inival(1) 
 END IF 

END FUNCTION

-- Subrutina para buscar si un numero de documento existe
FUNCTION facing003_bdocumento()
 -- Verificando si el documento existe
 SELECT UNIQUE a.numdoc
  FROM  fac_mnotcre a
  WHERE a.codemp = w_mae_mnot.codemp
    AND a.serdoc = w_mae_mnot.serdoc 
    AND a.numdoc = w_mae_mnot.numdoc 
 IF (status=NOTFOUND) THEN
    RETURN FALSE 
 ELSE
    RETURN TRUE 
 END IF 
END FUNCTION 

-- Subutina para el ingreso del detalle de productos de la factur
FUNCTION facing003_detingreso()
 DEFINE w_mae_art  RECORD LIKE inv_products.*,
        w_uni_vta  RECORD LIKE inv_unimedid.*,
        wpremin    DEC(12,2),
        wpresug    DEC(14,2),
        xtippre    SMALLINT, 
        loop,scr   SMALLINT,
        confirm    SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER,  
        repetido   SMALLINT,
        strcon     STRING,
        line_esp   SMALLINT,
		  flag_exis  SMALLINT,
        flag_despecial SMALLINT,
        des_especial STRING,
        vpremindol DECIMAL(12,2),
        vpresugdol DECIMAL(14,2)
        
 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

 LET vpremindol = NULL;
 LET vpresugdol = NULL;
  
  -- Ingresando productos
  INPUT ARRAY v_product WITHOUT DEFAULTS FROM s_product.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT


      
   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "1;Atencion: calculadora no disponible."
    END IF

   ON ACTION listproducto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos","Producto","Descirpcion del Producto",
                        --    "codabr","dsitem","inv_products","1=1",2,1,1)
                            "inv_products.codabr","inv_products.dsitem","fac_dtransac, inv_products","fac_dtransac.lnktra = "||w_enc.lnktra||" AND inv_products.codabr = fac_dtransac.codabr" ,2,1,1)
    RETURNING v_product[arr].cditem,v_product[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD cditem
    ELSE 
       -- Asignando descripcion
       DISPLAY v_product[arr].cditem TO s_product[scr].cditem 
       DISPLAY v_product[arr].dsitem TO s_product[scr].dsitem 
    END IF 

   ON CHANGE canori 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing003_totdet()

   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)
		

    -- Totalizando unidades
    CALL facing003_totdet()

   BEFORE ROW
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      LET totlin = ARR_COUNT()
      
      LET vpremindol = NULL;
      LET vpresugdol = NULL;
    
   BEFORE FIELD cditem
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cditem
    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cditem 
    END IF

   ON CHANGE cditem
   
   
   --BEFORE FIELD canuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Verificando producto
    IF v_product[arr].cditem IS NULL OR
       (v_product[arr].cditem <=0) THEN 
       ERROR "Error: codigo del producto invalido, VERIFICA ..."
       INITIALIZE v_product[arr].* TO NULL
       CLEAR s_product[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_product[arr].cditem)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       ERROR "Error: producto no registrado, VERIFICA ..."
       INITIALIZE v_product[arr].* TO NULL
       CLEAR s_product[scr].* 
       NEXT FIELD cditem 
    END IF 

    SELECT fac_dtransac.canori, fac_dtransac.preuni, fac_dtransac.predol, 
    fac_dtransac.totpro, fac_dtransac.prtdol
    INTO v_product[arr].canfac, v_product[arr].preunifac, v_product[arr].preunidolfac, 
    v_product[arr].pretotfac, v_product[arr].pretotdolfac
    FROM fac_dtransac
    WHERE fac_dtransac.lnktra = w_enc.lnktra
    AND fac_dtransac.codabr = v_product[arr].cditem

    IF SQLCA.sqlcode = NOTFOUND THEN 
      ERROR "Error: producto no existe en la factura, VERIFICA ..."
      INITIALIZE v_product[arr].* TO NULL
      CLEAR s_product[scr].* 
      NEXT FIELD cditem
    END IF

    LET v_product[arr].canori = v_product[arr].canfac
    LET v_product[arr].canuni = v_product[arr].canfac
    LET v_product[arr].preuni = v_product[arr].preunifac
    LET v_product[arr].predol = v_product[arr].preunidolfac
    DISPLAY v_product[arr].preunifac TO s_product[scr].canfac
    DISPLAY v_product[arr].preunidolfac TO s_product[scr].preunidolfac
    DISPLAY v_product[arr].canfac TO s_product[scr].canfac
    DISPLAY v_product[arr].canori TO s_product[scr].canori
    DISPLAY v_product[arr].canuni TO s_product[scr].canuni
    DISPLAY v_product[arr].preuni TO s_product[scr].preuni
    DISPLAY v_product[arr].predol TO s_product[scr].predol
    
    -- Asignando descripcion
    LET v_product[arr].codpro = w_mae_art.cditem
	 IF flag_especial = FALSE THEN
	    LET v_product[arr].dsitem = w_mae_art.dsitem 
	 END IF
    LET v_product[arr].unimed = w_mae_art.unimed 
    DISPLAY v_product[arr].dsitem TO s_product[scr].dsitem 

    -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed) 
    RETURNING w_uni_med.*,existe

    -- ASignando unidad de medida
    LET v_product[arr].nomuni = w_uni_med.nommed 
    DISPLAY v_product[arr].nomuni TO s_product[scr].nomuni 

    -- Obteniendo datos de la medida de producto
    INITIALIZE w_med_pro.* TO NULL
    CALL librut003_bmedidapro(w_mae_art.codmed) 
    RETURNING w_med_pro.*,existe

    -- Verificando productos duplicados 
    LET repetido = FALSE
    FOR i = 1 TO totlin
     IF v_product[i].cditem IS NULL THEN
        CONTINUE FOR
     END IF

     -- Verificando producto
     IF w_mae_art.dsitem NOT MATCHES "*MEDIDA*"   THEN
	     IF (i!=ARR_CURR()) THEN 
   	   IF (v_product[i].cditem = v_product[arr].cditem) THEN
      	   LET repetido = TRUE
	         EXIT FOR
   	   END IF 
	     END IF
	  END IF
    END FOR

    -- Si hay repetido
    IF repetido THEN
       LET msg = "Producto ya registrado en el detalle. Linea (",i USING "<<<",") \n VERIFICA ..." 
       CALL fgl_winmessage(" Atencion",msg,"stop")
       INITIALIZE v_product[arr].* TO NULL
       CLEAR s_product[scr].* 
       NEXT FIELD cditem
    END IF

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_product[arr].* TO NULL
       CLEAR s_product[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede facturarse, VERIFICA ...",
       "stop")
       INITIALIZE v_product[arr].* TO NULL
       CLEAR s_product[scr].* 
       NEXT FIELD cditem 
    END IF 

    -- Determinando tipo de precio
    LET xtippre = 1 -- Normal
    
    -- Verificando si mdida del producto es fraccionable
    IF w_med_pro.epqfra AND
       w_med_pro.medtot>0 THEN
       LET xtippre = 2  -- Rollos
    END IF 

    IF NOT w_med_pro.epqfra AND 
       w_med_pro.medtot>0 THEN
       LET xtippre = 3 -- Precios especiales
    END IF 

   
   -- Asignando unidad de medida
   LET v_product[arr].unimto = w_mae_art.unimed 
   DISPLAY v_product[arr].unimto TO s_product[scr].unimto 

   -- Deshabiiltando campos 
   CALL Dialog.SetFieldActive("unimto",0) 
   --CALL Dialog.SetFieldActive("canuni",0) 

   
    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing003_totdet()

   AFTER FIELD canuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD canuni 
    END IF

    -- Verificando cantidad
    IF v_product[arr].canuni IS NULL OR
       (v_product[arr].canuni <=0) THEN 
       ERROR "Error: cantidad invalida, VERIFICA ..."
       NEXT FIELD canuni 
    END IF 

    IF v_product[arr].canuni > v_product[arr].canfac THEN
      ERROR "Error: La cantidad no puede ser mayor a la cantidad facturada"
      NEXT FIELD canuni
    END IF

    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)
	

    -- Totalizando unidades
    CALL facing003_totdet()

   AFTER FIELD unimto
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD unimto 
    END IF

    -- Verificando cantidad
    IF v_product[arr].unimto IS NULL THEN
       ERROR "Error: unidad de medida invalida, VERIFICA ..."
       NEXT FIELD unimto 
    END IF 

    -- Verificando medidas 
    IF (v_product[arr].unimed!=v_product[arr].unimto) THEN
       -- Obteniendo datos de la unidad de medida 
       INITIALIZE w_uni_vta.* TO NULL
       CALL librut003_bumedida(v_product[arr].unimto) 
       RETURNING w_uni_vta.*,existe
       
      -- Verificanod conversion 
      IF w_uni_vta.factor IS NULL OR
         w_uni_vta.factor=0 THEN 
         ERROR "Error: unidad de medida sin factor de conversion, VERIFICA ..."
         NEXT FIELD unimto 
      END IF 

      -- Convirtiendo cantidad de acuerdo al factor 
      LET v_product[arr].canuni = (v_product[arr].canori*w_uni_vta.factor) 
      LET v_product[arr].factor = w_uni_vta.factor 
      DISPLAY v_product[arr].canuni TO s_product[scr].canuni 
    ELSE 
      -- Asignando cantidad origen a cantidad total 
      LET v_product[arr].canuni = v_product[arr].canori
      LET v_product[arr].factor = 0
      DISPLAY v_product[arr].canuni TO s_product[scr].canuni 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing003_totdet()

   AFTER FIELD preuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD preuni 
    END IF

    -- Verificando precio
    IF v_product[arr].preuni IS NULL OR
       (v_product[arr].preuni <=0) THEN 
       ERROR "Error: precio invalido, VERIFICA ..."
       NEXT FIELD preuni 
    END IF 

    IF v_product[arr].preuni > v_product[arr].preunifac THEN
         ERROR "Error: El precio no puede ser mayor al precio facturado, VERIFICA ..."
         NEXT FIELD preuni
    END IF
    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing003_totdet()

   

  AFTER FIELD predol
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD predol 
    END IF

    -- Verificando precio
    IF v_product[arr].predol IS NULL OR
       (v_product[arr].predol <=0) THEN 
       ERROR "Error: precio invalido, VERIFICA ..."
       NEXT FIELD predol 
    END IF

   IF v_product[arr].predol > v_product[arr].preunidolfac THEN
         ERROR "Error: El precio no puede ser mayor al precio facturado, VERIFICA ..."
         NEXT FIELD preuni
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing003_cantidadtotal(arr,scr)

    -- Totalizando unidades
    CALL facing003_totdet()


   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing003_totdet()

   AFTER DELETE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Limpiando producto borrado
    INITIALIZE v_product[arr].* TO NULL
    CLEAR s_product[scr].* 
	 IF arr = line_esp THEN
		LET flag_especial = FALSE
		CALL DIALOG.SetActionActive("agregar_especial",TRUE)
	   LET line_esp = NULL
	 END IF

    -- Totalizando unidades
    CALL facing003_totdet()

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing003_totdet()
  END INPUT

  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando lineas incompletas 
  LET linea = facing003_incompletos()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") de producto incompleta. \n VERIFICA ..."
     CALL fgl_winmessage(
     " Atencion",
     msg, 
     "stop")
     CONTINUE WHILE
  END IF 

  -- Verificando que se ingrese al menos un producto
  IF (totval<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     " Debe facturarse al menos un producto. \n VERIFICA ...",
     "stop")
     CONTINUE WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
   {
    -- Ingresando confirmacion de cajero
    CALL facing003_confirmacion() 
    RETURNING confirm,w_mae_mnot.usrsis
    IF NOT confirm THEN 
       LET loop = TRUE 
    ELSE
       LET loop      = FALSE
       LET retroceso = FALSE

       -- Grabando inventario 
       CALL facing003_grabar()
    END IF 
}
      LET loop      = FALSE
       LET retroceso = FALSE

       -- Grabando inventario 
       CALL facing003_grabar()

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para ingresar la clave de confirmacion antes de grabar
FUNCTION facing003_confirmacion()
 DEFINE passwd    LIKE fac_usuaxpos.passwd,
        username  LIKE fac_usuaxpos.userid,
        confirm   SMALLINT

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wsecurity AT 9,27
  WITH FORM "facing003c"

  -- Ingresando clave de confirmacion 
  LET confirm = TRUE  
  INPUT BY NAME passwd
   --ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=TRUE)
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE)

   ON ACTION cancel
    LET confirm = FALSE 
    EXIT INPUT 

   AFTER FIELD passwd
    -- Verificanod password
    IF LENGTH(passwd)<=0 THEN
       ERROR "Error: password invalido ..."
       NEXT FIELD passwd
    END IF 

    -- Verificando cajero por medio de su password corto
    LET username = NULL 
    SELECT UNIQUE a.userid
     INTO  username 
     FROM  fac_usuaxpos a
     WHERE a.numpos = w_mae_tra.numpos
       AND a.passwd = passwd
     IF (status=NOTFOUND) THEN
        ERROR "Error: password incorrecto ..."
        NEXT FIELD passwd 
     END IF
  END INPUT 
 CLOSE WINDOW wsecurity 

 RETURN confirm,username 
END FUNCTION 

-- Subrutina para calcular las undiades totales (unidades ingresdas * cantidad empaque)

FUNCTION facing003_cantidadtotal(arr,scr) 
 DEFINE arr,scr  SMALLINT

 -- Calculando precio total
 LET v_product[arr].totpro = (v_product[arr].canuni*v_product[arr].preuni)
 LET v_product[arr].prtdol = (v_product[arr].canuni*v_product[arr].predol)

 -- Desplegando datos
 DISPLAY v_product[arr].canuni TO s_product[scr].canuni 
 DISPLAY v_product[arr].totpro TO s_product[scr].totpro
DISPLAY v_product[arr].prtdol TO s_product[scr].prtdol 
END FUNCTION 

-- Subrutina para totalizar unidades contadas

FUNCTION facing003_totdet()
 DEFINE i SMALLINT

 -- Totalizando
 LET totuni = 0
 LET totval = 0
 LET totvaldol = 0
 FOR i = 1 TO totlin
  IF v_product[i].codpro IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Totalizando
  LET totuni = (totuni+v_product[i].canuni)
  LET totval = (totval+v_product[i].totpro)
  LET totvaldol = (totvaldol+v_product[i].prtdol)
  
 END FOR
{
 -- Desplegando total de unidades del inventario
 IF w_mae_mnot.exportacion = 1 THEN 
   LET w_mae_mnot.totpag = totvaldol
 ELSE
   LET w_mae_mnot.totpag = totval
 END IF
 }
 --DISPLAY BY NAME totuni,totval,totlin,w_mae_tra.totpag,totvaldol
 DISPLAY BY NAME totuni,totval,totlin,totvaldol
END FUNCTION 

-- Subrutina para verificar si hay productos incompletos
FUNCTION facing003_incompletos()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO 100 
  IF v_product[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_product[i].canuni IS NULL OR
     v_product[i].preuni IS NULL OR
     v_product[i].totpro IS NULL THEN
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para grabar la facturacion 
FUNCTION facing003_grabar()
 DEFINE i,correl SMALLINT,
        pct      DEC(9,6) ,
		  flag     SMALLINT,
        vfac_id INTEGER,
        cmd STRING
 DEFINE reg_e RECORD
   estatus LIKE facturafel_e.estatus,
   serie_e LIKE facturafel_e.serie_e,
   numdoc_e LIKE facturafel_e.numdoc_e,
   autorizacion LIKE facturafel_e.autorizacion
 END RECORD


	LET flag=FALSE

 -- Grabando transaccion
 ERROR " Registrando Nota de cr�dito ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grababdo encabezado de la facturacion 
   -- Asignando datos
   IF w_mae_mnot.tascam IS NULL THEN LET w_mae_mnot.tascam=0 END IF
   IF w_mae_mnot.totdoc IS NULL THEN LET w_mae_mnot.totdoc=0 END IF
   IF w_mae_mnot.totiva IS NULL THEN LET w_mae_mnot.totiva=0 END IF
   IF w_mae_mnot.poriva IS NULL THEN LET w_mae_mnot.poriva=0 END IF
   
   LET w_mae_mnot.linknc = 0 
   IF w_mae_mnot.exportacion = "E" THEN
      LET w_mae_mnot.moneda = 2
   ELSE
      LET w_mae_mnot.moneda = 1
   END IF
   
   LET w_mae_mnot.usranl = NULL
   LET w_mae_mnot.fecanl = NULL
   LET w_mae_mnot.horanl = NULL

   -- Verificando tipo de moneda para obtener la tasa de cambio
   IF (w_mae_mnot.moneda=2) THEN
      -- Moneda en Dolares
      LET w_mae_mnot.tascam = tascom                  
   ELSE
      -- Moneda en Quetzales
      LET w_mae_mnot.tascam = tasvta 
   END IF

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO fac_mnotcre
   VALUES (w_mae_mnot.*)
   LET w_mae_mnot.linknc = SQLCA.SQLERRD[2] 
	IF STATUS <> 0 THEN
		LET flag = TRUE
	END IF

   -- 2. Grabando detalle de la facturacion 
   -- Si no es orden de trabajo
	DISPLAY "flag ",flag
	IF flag = FALSE THEN
   	 
      LET correl = 0
      FOR i = 1 TO totlin 
     		IF v_product[i].cditem IS NULL OR 
        		v_product[i].canuni IS NULL THEN 
        		CONTINUE FOR 
     		END IF 
     		LET correl = (correl+1) 
		
     		-- Grabando
     		SET LOCK MODE TO WAIT
     		INSERT INTO fac_dnotcre 
     		VALUES (w_mae_mnot.linknc    , -- link del encabezado
             		correl              , -- correlativo de ingreso
             		w_mae_pos.codemp    , -- empresa del pos
             		w_mae_pos.codsuc    , -- sucursal del pos
             		w_mae_pos.codbod    , -- bodega del pos 
             		v_product[i].codpro, -- codigo del producto 
             		v_product[i].cditem, -- codigo del producto abreviado
             		v_product[i].unimed, -- unidad de medida del producto 
             		v_product[i].canori, -- cantidad original
             		v_product[i].unimto, -- unidad de medida de la medida de venta
             		v_product[i].canuni, -- cantidad total 
             		v_product[i].preuni, -- precio unitario 
             		v_product[i].totpro, -- valor total 
             		v_product[i].factor,
                  v_product[i].deitemh,
                  v_product[i].predol,
                  v_product[i].prtdol ) --Descripcion especial del producto
     		
      END FOR 
      
      ###############################################################
      #Registro en tablas de FEL
      CALL registra_tablas_FEL(1) RETURNING flag  --Inserta factura electronica
      IF flag = TRUE THEN
         ROLLBACK WORK 
      END IF
      ###############################################################
   	 
	END IF


 IF flag = FALSE THEN

   -- Finalizando la transaccion
 	COMMIT WORK

   -- Desplegando mensaje
   CALL fgl_winmessage(" Atencion","Transaccion registrada.","information")
 	

   --Envia factura a firma
   LET vfac_id = NULL;
      
   SELECT a.fac_id
   INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "NC"
      
   LET cmd = "fglrun ande_fel.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
   DISPLAY "cmd: ",cmd
   RUN cmd

   SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
   INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra
   AND a.tipod = "NC"
   
   DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
   IF reg_e.estatus = "P" THEN
      -- Desplegando mensaje
      CALL fgl_winmessage(" Atencion","El documento no pudo ser firmado electronicamente.","stop")
   ELSE
   -- Desplegando mensaje
      
      CALL fgl_winmessage(" Atencion","No. de autorizaci�n electronica:"||reg_e.autorizacion,"information")
      -- Imprimiendo movimiento
      ERROR "Imprimiendo ... por favor espere ...."
	
      CALL facrpt003_facturacion(pct)
      ERROR ""
   END IF 
   
 	 
 ELSE
	CALL fgl_winmessage(" Atencion","Error en la transaccion, no se ha grabado","stop")
 END IF
END FUNCTION 

FUNCTION registra_tablas_FEL(tipooperacion)
DEFINE tipooperacion SMALLINT; --1=Emision de factura, 2=Anulaci�n de factura 
DEFINE flag SMALLINT
DEFINE fel_e RECORD LIKE facturafel_e.*
DEFINE fel_ed RECORD LIKE facturafel_ed.*
DEFINE emp_fel RECORD LIKE empresas1.*
DEFINE vtime DATETIME HOUR TO FRACTION(3)
DEFINE i SMALLINT
DEFINE vcodcat SMALLINT;

INITIALIZE fel_e.*, fel_ed.*, emp_fel TO NULL
LET vtime = CURRENT HOUR TO FRACTION;
LET flag = FALSE;

SELECT empresas1.*
INTO emp_fel.*
FROM empresas1
WHERE empresas1.bodega = 1


LET fel_e.serie   = w_mae_mnot.serdoc;
LET fel_e.num_doc = w_mae_mnot.numdoc;
LET fel_e.tipod   = "NC"

SELECT fac_mtransac.*
INTO w_mae_fac.*
FROM fac_mtransac
WHERE fac_mtransac.lnktra = w_mae_mnot.lnktra;

IF tipooperacion = 1 THEN
   LET fel_e.fecha   = w_mae_mnot.fecdoc;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(w_mae_mnot.fecdoc) USING "&&&&",MONTH(w_mae_mnot.fecdoc) USING "&&",DAY(w_mae_mnot.fecdoc) USING "&&",vtime);
   LET fel_e.fecha_anul = NULL; --SE LLENA SOLO SI ES ANULACION
   LET fel_e.estado_doc = "ORIGINAL"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"
   IF w_mae_mnot.exportacion = 1 THEN
      LET fel_e.total_b          = w_mae_mnot.tdocdol; --Ej.  117.00
      LET fel_e.total_d          = w_mae_mnot.tdocdol; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es ero
      LET fel_e.ing_netosg       = w_mae_mnot.tdocdol; --Ej.  100.00
      LET fel_e.total_i          = 0;
      LET fel_e.total_iva1       = 0;
      LET fel_e.base1            = w_mae_mnot.tdocdol; --Ej.  100.00
      LET fel_e.tasa1            = 0; --Ej.   12.00
      LET fel_e.monto1           = 0; --Ej.   12.00
      LET fel_e.total_neto       = w_mae_mnot.tdocdol; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_mnot.tdocdol)
      LET fel_e.es_exportacion = 1;
      LET fel_e.incoterm = w_mae_fac.incote;
      LET fel_e.nomconcomprador = w_mae_fac.nomcom;
      LET fel_e.dirconcomprador = w_mae_fac.dircom;
      LET fel_e.otrreferencia = w_mae_fac.otrref;
   ELSE
      LET fel_e.total_b          = w_mae_mnot.totdoc; --Ej.  117.00
      LET fel_e.total_d          = w_mae_mnot.totdoc; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es ero
      LET fel_e.ing_netosg       = w_mae_mnot.subtotal; --Ej.  100.00
      LET fel_e.total_i          = w_mae_mnot.totiva; --Ej.   12.00 
      LET fel_e.total_iva1       = w_mae_mnot.totiva; --Ej.   12.00
      LET fel_e.base1            = w_mae_mnot.subtotal; --Ej.  100.00
      LET fel_e.tasa1            = w_mae_mnot.poriva; --Ej.   12.00
      LET fel_e.monto1           = w_mae_mnot.totiva; --Ej.   12.00
      LET fel_e.total_neto       = w_mae_mnot.totdoc; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_mnot.totdoc)
      LET fel_e.es_exportacion = 0;
   END IF
   LET fel_e.monto_d          = 0                 --Ej.    5.00, poner cero, porque no manejan descuentos
   LET fel_e.tipo1            = "IVA";
   LET fel_e.id_factura       = 0 --Apunta a un apuntador recursivo cuando es anulacion o NC, si es factura va nulo--De donde se obtiene
   
   IF w_mae_fac.totpag = 0 THEN
      LET fel_e.tipo_pago = "CR";
   ELSE
      LET fel_e.tipo_pago = "CO";
   END IF
ELSE
   LET fel_e.fecha   = TODAY;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(fel_e.fecha) USING "&&&&",MONTH(fel_e.fecha) USING "&&",DAY(fel_e.fecha) USING "&&",vtime);
   LET fel_e.fecha_anul = fel_e.fecha_em;
   LET fel_e.estado_doc = "ANULADO"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"

   SELECT facturafel_e.fac_id
   INTO fel_e.id_factura
   FROM facturafel_e
   WHERE facturafel_e.num_int  = w_mae_mnot.linknc
   AND facturafel_e.estado_doc = "ORIGINAL"
   AND facturafel_e.tipod = "NC"

   LET fel_e.tipo_pago        = "CO";
   LET fel_e.nota1 = w_mae_mnot.motanl;
END IF


LET fel_e.from    = emp_fel.e_mail; --  DEJAR QUEMADO EL CORREO QUE SE COLOCARA AQUI   
LET fel_e.to      = w_mae_mnot.maicli;
LET fel_e.cc = NULL;
LET fel_e.formats  = "PDF"; 
LET fel_e.tipo_doc = "NOTAC"; --DEJAR QUEMADO

LET fel_e.ant_serie = NULL;  
LET fel_e.ant_numdoc = NULL; 
LET fel_e.ant_fecemi = NULL;
LET fel_e.ant_resoluc = NULL;
IF w_mae_mnot.moneda = 1 THEN 
   LET fel_e.c_moneda   = "GTQ"; 
   LET fel_e.tipo_cambio      = 1;
ELSE
   LET fel_e.c_moneda   = "USD";
   LET fel_e.tipo_cambio      = w_mae_mnot.tascam;
END IF

LET fel_e.num_int          = w_mae_mnot.linknc;
LET fel_e.nit_e            = emp_fel.nit; --tomarlo de la tabla empresas1.nit donde la bodega 1; 
LET fel_e.nombre_c         = emp_fel.nom_sucursal;-- tomarlo de la tabla empresas1.nom_sucursal
LET fel_e.idioma_e         = "ES";
LET fel_e.nombre_e         = emp_fel.razon_social; --tomarlo de la tabla empresas1.razon_social
LET fel_e.codigo_e         = emp_fel.cod_sucursal; --Tomarlo de la tabla empresas1.cod_sucursal
LET fel_e.dispositivo_e    = NULL;
LET fel_e.direccion_e      = emp_fel.direccion1; --tomarlo de la tabla empresas1.direccion1
LET fel_e.departamento_e   = emp_fel.depto; --tomarlo de la tabla empresas1.depto
LET fel_e.municipio_e      = emp_fel.municipio; --tomarlo de la tabla empresas1.municipio
LET fel_e.pais_e           = "GT" --dejarlo quemado
LET fel_e.codpos_e         = emp_fel.codigo_postal --tomarlo de la tabla empresas1.codigo_postal
LET fel_e.nit_r            = w_mae_mnot.numnit;
LET fel_e.es_cui           = NULL;
LET fel_e.nombre_r         = w_mae_mnot.nomcli;
LET fel_e.idioma_r         = "ES";
LET fel_e.direccion_r      = w_mae_mnot.dircli;
LET fel_e.departamento_r   = NULL;
LET fel_e.municipio_r      = NULL;
LET fel_e.pais_r           = "GT";
LET fel_e.codpos_r         = NULL;
LET fel_e.serie_e          = NULL; --este campo lo llena la interfaz fel
LET fel_e.numdoc_e         = NULL; --este campo lo llena la interfaz fel
LET fel_e.autorizacion     = NULL; --este campo lo llena la interfaz fel
LET fel_e.estatus          = "P"; --P=pendiente, la interface lo vuelve C=certificado si todo bien
LET fel_e.fac_id           = 0; --Llave primaria 
LET fel_e.p_descuento      = 0; --PONER CERO no manejan descuento detallado en la factura
LET fel_e.mto_descuento    = 0; -- PONER CERO no manejan descuento detallado en la factura
LET fel_e.fel_msg          = NULL; --Lo llena la interfaz de fel
LET fel_e.info_reg         = "PAGO_TRIMESTRAL";
LET fel_e.num_oc           = NULL;
LET fel_e.un_pago_de       = NULL;
LET fel_e.npagos           = NULL;
LET fel_e.mto_pago         = NULL;
LET fel_e.dia_pago         = NULL;
LET fel_e.fecha_pago       = NULL;
LET fel_e.nom_firma        = NULL;
LET fel_e.dpi_firma        = NULL;
LET fel_e.vencod           = w_mae_mnot.usrope --AMPLIAR CAMPO EN TBLA A 15
LET fel_e.num_interno      = w_mae_mnot.serdoc CLIPPED||"-"||w_mae_mnot.numdoc; 

SET LOCK MODE TO WAIT

   WHENEVER ERROR CONTINUE
   INSERT INTO facturafel_e(
   serie,num_doc,tipod,fecha,fecha_em,fecha_anul,
   from,to,cc,formats,tipo_doc,estado_doc,ant_serie,ant_numdoc,
   ant_fecemi,ant_resoluc,c_moneda,tipo_cambio,num_int,nit_e,nombre_c,
   idioma_e,nombre_e,codigo_e,dispositivo_e,direccion_e,departamento_e,municipio_e,
   pais_e,codpos_e,nit_r,es_cui,nombre_r,idioma_r,direccion_r,departamento_r,
   municipio_r,pais_r,codpos_r,total_b,total_d,monto_d,total_i,ing_netosg,total_iva1,
   tipo1,base1,tasa1,monto1,total_neto,serie_e,numdoc_e,autorizacion,estatus,
   fac_id,p_descuento,mto_descuento,fel_msg,id_factura,total_en_letras,tipo_pago,
   num_oc,un_pago_de,npagos,mto_pago,dia_pago,fecha_pago,nom_firma,dpi_firma,
   vencod,num_interno,info_reg,es_exportacion,incoterm,nomconcomprador,dirconcomprador,otrreferencia)
   VALUES(
   fel_e.serie, fel_e.num_doc, fel_e.tipod, fel_e.fecha, fel_e.fecha_em, fel_e.fecha_anul, 
   fel_e.from, fel_e.to, 
   fel_e.cc, fel_e.formats, fel_e.tipo_doc, fel_e.estado_doc, fel_e.ant_serie,
   fel_e.ant_numdoc, fel_e.ant_fecemi, fel_e.ant_resoluc, fel_e.c_moneda,
   fel_e.tipo_cambio, fel_e.num_int, fel_e.nit_e, fel_e.nombre_c,
   fel_e.idioma_e, fel_e.nombre_e, fel_e.codigo_e, fel_e.dispositivo_e, fel_e.direccion_e,
   fel_e.departamento_e, fel_e.municipio_e, fel_e.pais_e, fel_e.codpos_e, fel_e.nit_r,
   fel_e.es_cui, fel_e.nombre_r, fel_e.idioma_r, fel_e.direccion_r, fel_e.departamento_r,
   fel_e.municipio_r, fel_e.pais_r, fel_e.codpos_r, fel_e.total_b, fel_e.total_d,
   fel_e.monto_d, fel_e.total_i, fel_e.ing_netosg, fel_e.total_iva1, fel_e.tipo1,
   fel_e.base1, fel_e.tasa1, fel_e.monto1, fel_e.total_neto, fel_e.serie_e, fel_e.numdoc_e,
   fel_e.autorizacion, fel_e.estatus, fel_e.fac_id, fel_e.p_descuento, fel_e.mto_descuento,
   fel_e.fel_msg, fel_e.id_factura, fel_e.total_en_letras, fel_e.tipo_pago, fel_e.num_oc,
   fel_e.un_pago_de, fel_e.npagos, fel_e.mto_pago, fel_e.dia_pago, fel_e.fecha_pago,
   fel_e.nom_firma, fel_e.dpi_firma, fel_e.vencod, fel_e.num_interno, fel_e.info_reg,
   fel_e.es_exportacion, fel_e.incoterm, fel_e.nomconcomprador, fel_e.dirconcomprador, fel_e.otrreferencia)

   WHENEVER ERROR STOP

   IF sqlca.sqlcode < 0 THEN
      ERROR "Existi� un error al insertar el encabezado de facura FEL"
      LET flag = TRUE
      RETURN flag
   ELSE 
      LET fel_e.fac_id = SQLCA.sqlerrd[2] --Registrando valor serial asignado en la insercion del registro
   END IF 
   
   FOR i = 1 TO v_product.getLength() 
      IF v_product[i].cditem IS NULL OR 
         v_product[i].canuni IS NULL THEN 
        		CONTINUE FOR 
      END IF
      INITIALIZE fel_ed.* TO NULL;

      LET fel_ed.fac_id = fel_e.fac_id;
      LET fel_ed.serie  = fel_e.serie;
      LET fel_ed.num_doc = fel_e.num_doc;
      LET fel_ed.tipod  = fel_e.tipod;
      LET fel_ed.fecha  = fel_e.fecha;
      LET fel_ed.descrip_p = v_product[i].dsitem CLIPPED;
      LET fel_ed.codigo_p  =  v_product[i].codpro;
      LET fel_ed.unidad_m  = v_product[i].nomuni;
      LET fel_ed.cantidad  = v_product[i].canuni;    --Ej.    10
      IF w_mae_mnot.exportacion = 1 THEN
         LET fel_ed.precio_u  = v_product[i].predol;    --Ej.  6513.00
         LET fel_ed.precio_t  = v_product[i].prtdol;    --Ej. 65130.00
         LET fel_ed.precio_p  = v_product[i].prtdol;    --Ej.    42350.00
         LET fel_ed.precio_m  = v_product[i].prtdol;    --Ej.   42350.00
         LET fel_ed.total_iva = 0;
         LET fel_ed.total_imp = 0;
         LET fel_ed.ing_netos = v_product[i].prtdol; --Ej.    42350.00
         LET fel_ed.base      = v_product[i].prtdol - fel_ed.total_iva; --Ej.   37812.50
         LET fel_ed.tasa      = 0;
         LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50

      ELSE
         LET fel_ed.precio_u  = v_product[i].preuni;    --Ej.  6513.00
         LET fel_ed.precio_t  = v_product[i].totpro;    --Ej. 65130.00
         LET fel_ed.precio_p  = v_product[i].totpro;    --Ej.    42350.00
         LET fel_ed.precio_m  = v_product[i].totpro;    --Ej.   42350.00
         LET fel_ed.total_iva = v_product[i].totpro - (v_product[i].totpro/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
         LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
         LET fel_ed.ing_netos = v_product[i].totpro; --Ej.    42350.00
         LET fel_ed.base      = v_product[i].totpro - fel_ed.total_iva; --Ej.   37812.50
         LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
         LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
      
      END IF
      
      LET fel_ed.descto_t  = 0;  --No se manejan descuentos Ej.    22780.00
      LET fel_ed.descto_m  = 0; --NO se manejan descuentos  Ej.    22780.00
      
      
      LET fel_ed.tipo      = "IVA";
      
      LET vcodcat = NULL;
      SELECT inv_products.codcat
      INTO vcodcat
      FROM inv_products
      WHERE inv_products.cditem = v_product[i].codpro
      
      IF vcodcat = 6 THEN --OTROS  (SERVICIOS)
         LET fel_ed.categoria  =   "SERVICIO" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
      ELSE
         LET fel_ed.categoria  =   "BIEN" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
      END IF
      
      IF v_product[i].deitemh IS NULL THEN
         LET fel_ed.txt_especial = "N";
      ELSE
         LET fel_ed.txt_especial = "S";
         LET fel_ed.txt_largo = v_product[i].deitemh;
      END IF
      
      SET LOCK MODE TO WAIT

      WHENEVER ERROR CONTINUE
      INSERT INTO facturafel_ed(
      fac_id,serie,num_doc,tipod,fecha,descrip_p,codigo_p,unidad_m,cantidad,precio_u,
      precio_t,descto_t,descto_m,precio_p,precio_m,total_imp,ing_netos,total_iva,
      tipo,base,tasa,monto,categoria,txt_especial,txt_largo)
      VALUES(      
      fel_ed.fac_id,fel_ed.serie,fel_ed.num_doc,fel_ed.tipod,fel_ed.fecha,fel_ed.descrip_p,
      fel_ed.codigo_p,fel_ed.unidad_m,fel_ed.cantidad,fel_ed.precio_u,fel_ed.precio_t,
      fel_ed.descto_t,fel_ed.descto_m,fel_ed.precio_p,fel_ed.precio_m,fel_ed.total_imp,
      fel_ed.ing_netos,fel_ed.total_iva,fel_ed.tipo,fel_ed.base,fel_ed.tasa,fel_ed.monto,
      fel_ed.categoria,fel_ed.txt_especial,fel_ed.txt_largo)

      WHENEVER ERROR STOP
      IF sqlca.sqlcode < 0 THEN
         ERROR "Existi� un problema al insertar el detalle de la factura FEL"
         LET flag = TRUE
         EXIT FOR
      END IF
   END FOR
   
RETURN flag 
END FUNCTION
