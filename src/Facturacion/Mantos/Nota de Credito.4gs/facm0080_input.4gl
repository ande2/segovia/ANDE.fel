
GLOBALS "glbm0009_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE sql_text STRING 
DEFINE detalle SMALLINT 

DEFINE idx2 SMALLINT 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      CALL gr_itemsExp.clear()
      CALL gr_itemsLoc.clear()
   END IF

   LET detalle = 0
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT --BY NAME  
      g_reg.lidItem, g_reg.lidFamilia, g_reg.lidTipo, --g_reg.desItemCt, 
      --g_reg.desItemMd, 
      g_reg.ldesItemLg
      --g_reg.variedad, g_reg.calidad, g_reg.mercado
      FROM 
      lidItem, lidFamilia, lidTipo, --g_reg.desItemCt, 
      --g_reg.desItemMd, 
      ldesItemLg --,
      --g_reg.variedad, g_reg.calidad, g_reg.mercado
      
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)

      ON CHANGE lidFamilia
         LET sql_text = "SELECT * FROM glbmtip where idfamilia = ", g_reg.lidFamilia
         CALL combo_din2("lidtipo", sql_text)

      ON CHANGE ldesItemLg 
        IF g_reg.ldesItemLg IS NULL THEN
           CALL msg("Debe ingresar Variedad ")
           NEXT FIELD CURRENT
        END IF  
      
   END INPUT 

   INPUT ARRAY gr_itemsExp FROM sDet2.*
   
     BEFORE INPUT 
       LET detalle = 1
       
     {ON CHANGE idItemSAP
       LET idx2 = arr_curr()
       IF validaLinea(idx2) THEN 
          CALL msg("Error, ID de Item SAP ya existe, ingrese de nuevo")
          NEXT FIELD idItemSAP 
       END IF }

    AFTER FIELD nombre
       LET idx2 = arr_curr()
       IF gr_itemsExp[idx2].nombre IS NULL THEN 
          CALL msg("Debe ingresar nombre")
          NEXT FIELD nombre 
       END IF

    {ON CHANGE peso
       LET idx2 = arr_curr()
       IF validaLinea(idx2) THEN 
          CALL msg("Error, ID de Item SAP con ese peso ya existe, ingrese de nuevo")
          NEXT FIELD peso 
       END IF} 

    {ON CHANGE idItemSAPfac
       LET idx2 = arr_curr()
       IF validaLinea3(idx2) THEN 
          CALL msg("Error, ID de Item SAP de facturacion ya existe, ingrese de nuevo")
          NEXT FIELD idItemSAP 
       END IF }

    {AFTER ROW  
      LET idx2 = arr_curr()
       IF validaLinea(idx2) THEN 
          CALL msg("Error, ID de Item SAP con ese peso ya existe, ingrese de nuevo")
          NEXT FIELD peso  
       END IF} 
      
   END INPUT 
   INPUT ARRAY gr_itemsLoc FROM sDet3.*
     BEFORE INPUT 
       LET detalle = 1
       
     {ON CHANGE idItemSAP
       LET idx2 = arr_curr()
       IF validaLinea2(idx2) THEN 
          CALL msg("Error, ID de Item SAP ya existe, ingrese de nuevo")
          NEXT FIELD idItemSAP 
       END IF }

    AFTER FIELD nombre
       LET idx2 = arr_curr()
       IF gr_itemsLoc[idx2].nombre IS NULL THEN 
          CALL msg("Debe ingresar nombre")
          NEXT FIELD nombre 
       END IF

    {ON CHANGE peso
       LET idx2 = arr_curr()
       IF validaLinea2(idx2) THEN 
          CALL msg("Error, ID de Item SAP con ese peso ya existe, ingrese de nuevo")
          NEXT FIELD peso 
       END IF 

     AFTER ROW  
      LET idx2 = arr_curr()
       IF validaLinea2(idx2) THEN 
          CALL msg("Error, ID de Item SAP con ese peso ya existe, ingrese de nuevo")
          NEXT FIELD peso  
       END IF }
       
   END INPUT 
   ON ACTION ACCEPT

        IF g_reg.ldesItemLg IS NULL THEN
           CALL msg("Debe ingresar variedad")
           NEXT FIELD CURRENT
        END IF  

      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         IF detalle = 0 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG
         END IF 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

FUNCTION validaLinea2(idx)
DEFINE i, idx SMALLINT 

  SELECT FIRST 1 * FROM itemsxgrupo 
    WHERE idItemSAP = gr_itemsLoc[idx].idItemSAP
      AND idItem    = g_reg.lidItem  --gr_itemsLoc[idx].idItem 
      AND peso = gr_itemsLoc[idx].peso
  IF sqlca.sqlcode = 0 THEN 
     RETURN TRUE 
  END IF 

  FOR i = 1 TO gr_itemsLoc.getLength()
    IF i <> idx THEN
       IF gr_itemsLoc[i].idItemSAP = gr_itemsLoc[idx].idItemSAP AND
          --gr_itemsLoc[i].idItem    = g_reg.idItem AND --gr_itemsLoc[idx].idItem AND 
          gr_itemsLoc[i].peso = gr_itemsLoc[idx].peso THEN
          RETURN TRUE  
       END IF 
    END IF
  END FOR 

  FOR i = 1 TO gr_itemsExp.getLength()
    --IF i <> idx THEN
    IF gr_itemsExp[i].idItemSAP = gr_itemsLoc[idx].idItemSAP AND
       --gr_itemsLoc[i].idItem    = g_reg.idItem AND --gr_itemsLoc[idx].idItem AND 
       gr_itemsExp[i].peso = gr_itemsLoc[idx].peso THEN
       RETURN TRUE  
    END IF 
    --END IF 
  END FOR 
  
  RETURN FALSE
       
END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

  SELECT FIRST 1 * FROM itemsxgrupo 
    WHERE idItemSAP = gr_itemsExp[idx].idItemSAP 
      --AND idItem = g_reg.idItem   --gr_itemsExp[idx].idItem
      AND peso = gr_itemsExp[idx].peso
  IF sqlca.sqlcode = 0 THEN 
     RETURN TRUE 
  END IF 

  FOR i = 1 TO gr_itemsExp.getLength()
    IF i <> idx THEN
       IF gr_itemsExp[i].idItemSAP = gr_itemsExp[idx].idItemSAP AND
          --gr_itemsExp[i].idItem    = g_reg.idItem AND  --gr_itemsExp[idx].idItem AND 
          gr_itemsExp[i].peso = gr_itemsExp[idx].peso THEN
          RETURN TRUE  
       END IF 
    END IF 
  END FOR 

  FOR i = 1 TO gr_itemsLoc.getLength()
    --IF i <> idx THEN
    IF gr_itemsLoc[i].idItemSAP = gr_itemsExp[idx].idItemSAP AND
       --gr_itemsExp[i].idItem    = g_reg.idItem AND  --gr_itemsExp[idx].idItem AND 
       gr_itemsLoc[i].peso = gr_itemsExp[idx].peso THEN
       RETURN TRUE  
    END IF 
    --END IF 
  END FOR 
  
  RETURN FALSE  
END FUNCTION 

FUNCTION validaLinea3(idx)
DEFINE i, idx SMALLINT 

  SELECT FIRST 1 * FROM itemsxgrupo 
    WHERE idItemSAPfac = gr_itemsExp[idx].idItemSAPfac 
      --AND idItem = g_reg.idItem   --gr_itemsExp[idx].idItem
      AND pesofac = gr_itemsExp[idx].pesofac
  IF sqlca.sqlcode = 0 THEN 
     RETURN TRUE 
  END IF 

  FOR i = 1 TO gr_itemsExp.getLength()
    IF i <> idx THEN
       IF gr_itemsExp[i].idItemSAPfac = gr_itemsExp[idx].idItemSAPfac AND
          --gr_itemsExp[i].idItem    = g_reg.idItem AND  --gr_itemsExp[idx].idItem AND 
          gr_itemsExp[i].pesofac = gr_itemsExp[idx].pesofac THEN
          RETURN TRUE  
       END IF 
    END IF 
  END FOR 
  
  RETURN FALSE  
END FUNCTION 

--FUNCTION existe_cat_cod(l_cod)
--DEFINE l_cod LIKE bautizo.idBautizo
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE idBautizo = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener codigo")
   --END TRY  
   --RETURN resultado 
--END FUNCTION

--FUNCTION existe_cat_nom(l_cod)
--DEFINE l_cod LIKE bautizo.librob
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE librob = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener id_commdep")
   --END TRY  
   --RETURN resultado 
--END FUNCTION