{ 
invglo017.4gl
Mynor Ramirez
Mantenimiento de puntos de venta
}

DATABASE segovia 

{ Definicion de variables globales }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD
         numpos      LIKE fac_puntovta.numpos, 
         nompos      LIKE fac_puntovta.nompos, 
         chkinv      LIKE fac_puntovta.chkinv, 
         codemp      LIKE fac_puntovta.codemp, 
         codsuc      LIKE fac_puntovta.codsuc, 
         codbod      LIKE fac_puntovta.codbod, 
         chkexi      LIKE fac_puntovta.chkexi, 
         sat_establecimiento  LIKE fac_puntovta.sat_establecimiento, 
         userid      LIKE fac_puntovta.userid, 
         fecsis      LIKE fac_puntovta.fecsis, 
         horsis      LIKE fac_puntovta.horsis
      END RECORD,
       v_puntos    DYNAMIC ARRAY OF RECORD
        tnumpos    LIKE fac_puntovta.numpos,
        tnompos    LIKE fac_puntovta.nompos 
       END RECORD,
       v_cajeros  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxbod.userid,
        tnomusr    VARCHAR(50),
        tusuaid    LIKE inv_permxbod.usuaid,
        tfecsis    LIKE inv_permxbod.fecsis,
        thorsis    LIKE inv_permxbod.horsis
       END RECORD,
       w_mae_suc  RECORD LIKE glb_sucsxemp.*,
       w_mae_emp  RECORD LIKE glb_empresas.*, 
       w_mae_bod  RECORD LIKE inv_mbodegas.*
END GLOBALS
