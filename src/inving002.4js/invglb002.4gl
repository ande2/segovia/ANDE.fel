{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales movimientos de inventario
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_tra   RECORD LIKE inv_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_mae_tip   RECORD LIKE inv_tipomovs.*,
       w_mae_bod   RECORD LIKE inv_mbodegas.*,
       w_mae_prv   RECORD LIKE inv_provedrs.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       w_mae_fis   RECORD 
        codemp     LIKE inv_tofisico.codemp, 
        codsuc     LIKE inv_tofisico.codsuc, 
        codbod     LIKE inv_tofisico.codbod, 
        aniotr     LIKE inv_tofisico.aniotr, 
        mestra     LIKE inv_tofisico.mestra, 
        fecinv     LIKE inv_tofisico.fecinv, 
        codcat     LIKE inv_products.codcat, 
        subcat     LIKE inv_products.subcat 
       END RECORD,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem, 
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        codepq     SMALLINT,
        canepq     LIKE inv_tofisico.canuni,
        canuni     LIKE inv_dtransac.canuni,
        canexi     LIKE inv_dtransac.canuni,
        candif     LIKE inv_dtransac.canuni 
       END RECORD, 
       totuni      DEC(14,2),
       totlin      SMALLINT,
       nomori      STRING,   
       nomest      STRING,   
       nomdes      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form
END GLOBALS
