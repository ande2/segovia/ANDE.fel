################################################################################
# Funcion     : %M%
# Descripcion : Funcion principal de las emplilias de productos
# Funciones   : empl_init(dbname)
#               empl_menu()
#               encabezado()
#               usu_destino()
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador       Fecha                    Descripcion de la modificacion
#
################################################################################


DATABASE segovia 

MAIN

   DEFER INTERRUPT

   MENU 
      ON ACTION reporte
        CALL printreport ()
      ON ACTION salir
         RETURN 
   END MENU 

END MAIN  