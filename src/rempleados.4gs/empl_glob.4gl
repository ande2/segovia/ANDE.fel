################################################################################
# Funcion     : %M%
# Descripcion : Funcion para definicion de globales
# Funciones   : 
#              
#              
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Estuardo Villatoro
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

DATABASE segovia

GLOBALS
DEFINE
# Definicion de variables para mantenimiento de Familia
	g_sccs_var CHAR(70),          # Variable para SCCS
	gr_empl	RECORD LIKE mempl.*,
	nr_empl	RECORD LIKE mempl.*,
	ur_empl	RECORD LIKE mempl.*,
	vempr_nomlog CHAR(15),
	usuario char(10), 
	hr      date,
	nombre_departamento,gtit_enc varchar(80), report_clause STRING 


END GLOBALS
