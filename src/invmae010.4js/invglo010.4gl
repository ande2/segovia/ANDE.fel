{ 
invglo010.4gl
Mynor Ramirez
Mantenimiento de productos 
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       v_products  DYNAMIC ARRAY OF RECORD
        tcditem    LIKE inv_products.cditem,
        tdsitem    LIKE inv_products.dsitem, 
        tcodabr    LIKE inv_products.codabr,
        testado    CHAR(20),
        tstatus    CHAR(20) 
       END RECORD, 
       v_epqsxpro  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tcodepq    LIKE inv_epqsxpro.codepq,
        tnomepq    LIKE inv_epqsxpro.nomepq,
        tcantid    LIKE inv_epqsxpro.cantid
       END RECORD,
       v_existpro  DYNAMIC ARRAY OF RECORD
        tcodemp    VARCHAR(40),
        tcodsuc    VARCHAR(40),
        tcodbod    VARCHAR(40),
        tcantid    LIKE inv_proenbod.exican,
        tfulent    LIKE inv_proenbod.fulent, 
        tfulsal    LIKE inv_proenbod.fulsal  
       END RECORD, 
       v_preciosm  DYNAMIC ARRAY OF RECORD
        pcditem    LIKE inv_products.cditem,
        palerta    CHAR(20),
        pcodabr    LIKE inv_products.codabr,
        pdsitem    LIKE inv_products.dsitem, 
        pnommed    CHAR(40),
        pcompra    LIKE inv_products.pulcom,
        ppreant    LIKE inv_products.premin,
        pprecio    LIKE inv_products.premin, 
        ppresan    LIKE inv_products.presug,
        ppresug    LIKE inv_products.presug
       END RECORD, 
       w           ui.Window,
       f           ui.Form
END GLOBALS
