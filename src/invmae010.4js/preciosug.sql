


drop table "sistemas".his_precioss;
create table "sistemas".his_precioss
  (
    lnkpre serial not null ,
    cditem integer not null ,
    preant decimal(12,2) not null ,
    presug decimal(12,2) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkpre) constraint pkhisprecioss1
  );

alter table "sistemas".his_precioss add constraint (foreign key
    (cditem) references "sistemas".inv_products  on delete cascade
    constraint "sistemas".fkinvproducts4);
