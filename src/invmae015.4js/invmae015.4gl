{
invmae015.4gl 
Mynor Ramirez
Mantenimiento de tipos de saldo
}

-- Definicion de variables globales 

GLOBALS "invglo015.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar3")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae015_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae015_mainmenu()
 DEFINE titulo    STRING,
        savedata  SMALLINT,
        wpais     VARCHAR(255)

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae015a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae015",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Saldo"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de saldo."
    CALL invqbe015_tiposald(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de saldo."
    LET savedata = invmae015_tiposald(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de saldo existente."
    CALL invqbe015_tiposald(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de saldo existente."
    CALL invqbe015_tiposald(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae015_tiposald(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae015_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.destip
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD destip  
    --Verificando nombre del tipo de saldo 
    IF (LENGTH(w_mae_pro.destip)=0) THEN
       ERROR "Error: nombre del tipo de saldo invalida, VERIFICA"
       LET w_mae_pro.destip = NULL
       NEXT FIELD destip  
    END IF

    -- Verificando que no exista otro tipo de saldo con el mismo nombre
    SELECT UNIQUE (a.tipsld)
     FROM  inv_mtiposal a
     WHERE (a.tipsld != w_mae_pro.tipsld) 
       AND (a.destip  = w_mae_pro.destip) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro tipo de saldo con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD destip
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.destip IS NULL THEN 
       NEXT FIELD destip
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae015_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando tipo de saldo
    CALL invmae015_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae015_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de saldo

FUNCTION invmae015_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de saldo ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tipsld),0)
    INTO  w_mae_pro.tipsld
    FROM  inv_mtiposal a
    IF (w_mae_pro.tipsld IS NULL) THEN
       LET w_mae_pro.tipsld = 1
    ELSE 
       LET w_mae_pro.tipsld = (w_mae_pro.tipsld+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_mtiposal   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tipsld 

   --Asignando el mensaje 
   LET msg = "Tipo de Saldo (",w_mae_pro.tipsld USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_mtiposal
   SET    inv_mtiposal.*      = w_mae_pro.*
   WHERE  inv_mtiposal.tipsld = w_mae_pro.tipsld 

   --Asignando el mensaje 
   LET msg = "Tipo de Saldo (",w_mae_pro.tipsld USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM inv_mtiposal 
   WHERE (inv_mtiposal.tipsld = w_mae_pro.tipsld)

   --Asignando el mensaje 
   LET msg = "Tipo de Saldo (",w_mae_pro.tipsld USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae015_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae015_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.tipsld = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tipsld THRU w_mae_pro.destip
 DISPLAY BY NAME w_mae_pro.tipsld,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
