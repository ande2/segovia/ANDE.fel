################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Frases FEL
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA segovia

GLOBALS 
TYPE 
   tDet RECORD 
      cod_frase      LIKE facturafel_fra.cod_frase,
      nombre         LIKE facturafel_fra.nombre,
      descripcion    LIKE facturafel_fra.descripcion
      
   END RECORD

DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      cod_frase      STRING,
      nombre         STRING,
      descripcion    STRING 
   END RECORD
   
DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "segm0703"
END GLOBALS