DATABASE segovia
FUNCTION lseclib001_accesos(wcodpro,wprogid,wuserid)
 DEFINE wuserid LIKE glb_permxusr.userid,
        wcodpro LIKE glb_permxusr.codpro,
        wprogid LIKE glb_permxusr.progid,
        cnt     SMALLINT

 -- Verificando accesos
 SELECT COUNT(*)
  INTO  cnt
  FROM  glb_permxusr a
  WHERE (a.codpro = wcodpro OR a.codpro = 'manager')
    AND (a.progid = wprogid OR a.progid = 0)
    AND a.userid = wuserid
    AND a.activo = 1
  IF (cnt>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION