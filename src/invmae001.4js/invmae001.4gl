{
invmae001.4gl 
Mynor Ramirez
Mantenimiento de categorias de producto 
}

-- Definicion de variables globales 
IMPORT FGL fgl_excel
GLOBALS "invglo001.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae001_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae001_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("invmae001",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Categorias"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de categorias."
    CALL invqbe001_categorias(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva categoria."
    LET savedata = invmae001_categorias(1) 
   COMMAND "Modificar"
    " Modificacion de una categoria existente."
    CALL invqbe001_categorias(2) 
   COMMAND "Borrar"
    " Eliminacion de una categoria existente."
    CALL invqbe001_categorias(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae001_categorias(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae001_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomcat,
                w_mae_pro.codabr
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomcat  
    --Verificando nombre de la categoria
    IF (LENGTH(w_mae_pro.nomcat)=0) THEN
       ERROR "Error: nombre de la categoria invalida, VERIFICA"
       LET w_mae_pro.nomcat = NULL
       NEXT FIELD nomcat  
    END IF

    -- Verificando que no exista otra categoria con el mismo nombre
    SELECT UNIQUE (a.codcat)
     FROM  inv_categpro a
     WHERE (a.codcat != w_mae_pro.codcat) 
       AND (a.nomcat  = w_mae_pro.nomcat) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra categoria con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomcat
     END IF 
   AFTER FIELD codabr  
    --Verificando el codigo abreviado
    IF (LENGTH(w_mae_pro.codabr)=0) THEN
       ERROR "Error: codigo abreviado invalida, VERIFICA"
       LET w_mae_pro.codabr = NULL
       NEXT FIELD codabr  
    END IF

    -- Verificando que no exista otro codigo abreviado
    SELECT UNIQUE (a.codabr)
     FROM  inv_categpro a
     WHERE (a.codcat != w_mae_pro.codcat) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro categoria con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomcat IS NULL THEN 
       NEXT FIELD nomcat
    END IF
    IF w_mae_pro.codabr IS NULL THEN 
       NEXT FIELD codabr
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae001_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando categoria
    CALL invmae001_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae001_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una categoria

FUNCTION invmae001_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando categoria ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codcat),0)
    INTO  w_mae_pro.codcat
    FROM  inv_categpro a
    IF (w_mae_pro.codcat IS NULL) THEN
       LET w_mae_pro.codcat = 1
    ELSE 
       LET w_mae_pro.codcat = (w_mae_pro.codcat+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_categpro   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codcat 

   --Asignando el mensaje 
   LET msg = "Categoria (",w_mae_pro.codcat USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_categpro
   SET    inv_categpro.*        = w_mae_pro.*
   WHERE  inv_categpro.codcat = w_mae_pro.codcat 

   --Asignando el mensaje 
   LET msg = "Categoria (",w_mae_pro.codcat USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando categorias
   DELETE FROM inv_categpro 
   WHERE (inv_categpro.codcat = w_mae_pro.codcat)

   --Asignando el mensaje 
   LET msg = "Categoria (",w_mae_pro.codcat USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae001_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae001_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codcat = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.nomcat
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
