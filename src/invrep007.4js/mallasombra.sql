SELECT a.codemp , a.cditem , b.codabr , b.dsitem , c.nommed ,
       b.codcat , b.subcat , b.codcol , d.codabr , sum(a.opeuni)
FROM inv_dtransac a , inv_products b , inv_unimedid c , inv_medidpro d
WHERE a.cditem = b.cditem
 AND   d.codmed = b.codmed
 AND   c.unimed = b.unimed
 AND   a.estado ='V'
 AND   a.codemp <> 21
 AND a.fecemi <= '08/12/2011'
 AND a.horsis <= '15:00:00'
 AND a.codbod =      8
 AND b.codcat =      2
 AND b.subcat =     38
 AND b.cditem =     562
GROUP BY 1,2,3,4,5,6,7,8,9
ORDER BY 1,6,7,8,9
