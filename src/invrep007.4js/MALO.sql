eELECT a.* --a.codemp , a.cditem, b.codabr , b.dsitem , c.nommed ,
       --b.codcat,b.subcat,b.codcol,d.codabr,sum(a.opeuni)
FROM inv_dtransac a , inv_products b , inv_unimedid c ,
     inv_medidpro d
WHERE a.cditem = b.cditem
AND   d.codmed = b.codmed
AND   c.unimed = b.unimed
AND   a.estado ='V'
AND   a.codbod <> 21
and   a.codabr="TCTEBEKA14X16"
AND   YEAR(a.fecemi) ||MONTH(a.fecemi) || DAY (a.fecemi) || a.horsis  <= '2011123012:00:00'
--AND a.lnktra <= ( SELECT MAX(x.lnktra)
--       			  FROM inv_dtransac x , inv_products z
					   --WHERE YEAR(x.fecemi) ||MONTH(x.fecemi) || DAY (x.fecemi) || x.horsis  <= '2011123013:00:00'
					   --AND x.cditem = z.cditem
					   --AND a.cditem = x.cditem
					   --AND x.codbod =      8
					   --AND z.codcat =      1  )
AND a.codbod =      8
AND b.codcat =      1
--GROUP BY 1,2,3,4,5,6,7,8,9
--ORDER BY 1,6,7,8,9
