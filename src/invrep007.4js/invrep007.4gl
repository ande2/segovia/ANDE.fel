{ 
Fecha    : Enero 2011
Programa : invrep007.4gl 
Objetivo : Reporte de existencias en bodega. 
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE inv_categpro.codcat,
        subcat   LIKE inv_subcateg.subcat,
		  codabr   LIKE inv_products.cditem,
		  fecemi   LIKE inv_dtransac.fecemi,
		  horsis   LIKE inv_dtransac.horsis,
		  detrep   SMALLINT,
        exiact   SMALLINT,
        eximin   SMALLINT, 
        eximax   SMALLINT
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
		 w_fact    RECORD
		   ser_fact  LIKE fac_mtransac.nserie,
		   num_fact  LIKE fac_mtransac.numdoc,
		   fec_fact  LIKE fac_mtransac.fecsis,
		   hor_fact  LIKE fac_mtransac.horsis,
		   lnk_fact  LIKE fac_mtransac.lnktra 
		 END RECORD        ,
		 w_tickt   RECORD
		   ser_tickt LIKE fac_mtransac.nserie,
		   num_tickt LIKE fac_mtransac.numdoc,
		   fec_tickt LIKE fac_mtransac.fecsis,
		   hor_tickt LIKE fac_mtransac.horsis,
		   lnk_tickt LIKE fac_mtransac.lnktra 
		 END RECORD        ,
		 w_mov    RECORD
		   lnk_mov   LIKE inv_mtransac.lnktra,
			nom_mov   LIKE inv_tipomovs.nomabr,
			fec_mov   LIKE inv_mtransac.fecsis,
			hor_mov   LIKE inv_mtransac.horsis
		 END RECORD        ,
		 hora      DATETIME HOUR TO SECOND,
		 hora2     VARCHAR(15),
       existe    SMALLINT,
       filename  STRING,
       fcodcat   STRING,
       fsubcat   STRING,
       fexiact   STRING,
       feximin   STRING,
       feximax   STRING

DEFINE rdestino, rtamanio STRING 

       
-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
  
CALL ui.Interface.loadActionDefaults("actiondefaults")
CALL ui.Interface.loadStyles("styles")
CALL ui.Interface.loadToolbar("toolbar7a")

 -- Definiendo teclas de control
 OPTIONS --HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep007.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep007_existencias()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep007_existencias()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp   LIKE inv_proenbod.codemp,
         cditem   LIKE inv_proenbod.cditem,
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
         totbod1  decimal(14,2),
         totbod8  LIKE inv_proenbod.exican,
         totbod12 LIKE inv_proenbod.exican,
         totbod16 LIKE inv_proenbod.exican,
         totbod20 LIKE inv_proenbod.exican,
         totbod21 LIKE inv_proenbod.exican,
         totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican
        END RECORD,
		  categ,subcat,codcol INTEGER,
		  codabr_med       LIKE inv_medidpro.codabr,
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        cmbtext           STRING,
        strcodbod         STRING,
        strcodabr         STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strfecemi         STRING,
        strhorsis         STRING,
        streximin         STRING,
        streximax         STRING,
        strexiact         STRING,
        loop              SMALLINT,
        strcodbod2        STRING,
        strcodabr2        STRING,
        strcodcat2        STRING,
        strsubcat2        STRING,
        strfecemi2        STRING,
        strhorsis2        STRING,
        streximin2        STRING,
        streximax2        STRING,
        strexiact2        STRING
DEFINE myHandler om.SaxDocumentHandler 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep007a AT 5,2
  WITH FORM "invrep007a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep007",wpais,1)
  CALL fgl_settitle("Inventario General")

  -- Definiendo archivo de impresion

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  --CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  LET cmbtext = "SELECT a.codbod,a.nombod ",
                " FROM  inv_mbodegas a ",
                " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                " WHERE b.codbod = a.codbod ",
                " AND b.userid = '",FGL_GETENV("LOGNAME") CLIPPED,"')",
 					 " AND bodcon = 0 ",
                " ORDER BY 2 "
  CALL librut002_combobox("codbod",cmbtext)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.exiact = 3 
   LET w_datos.eximin = 3 
   LET w_datos.eximax = 3 
   LET w_datos.detrep = 2 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.codabr,
                 w_datos.fecemi,
                 w_datos.horsis,
                 w_datos.detrep
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
      LET rdestino = "PDF"
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     ---IF w_datos.codbod IS NULL THEN
        ---ERROR "Error: deben completarse los filtos de seleccion."
        ---NEXT FIELD codbod
     ---END IF 
     EXIT INPUT 
     
   ON ACTION excel
      LET rdestino = "XLSX"
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     ---IF w_datos.codbod IS NULL THEN
        ---ERROR "Error: deben completarse los filtos de seleccion."
        ---NEXT FIELD codbod
     ---END IF 
     EXIT INPUT 
     
    ON CHANGE codbod
      --Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     --LET w_datos.codemp = w_mae_bod.codemp
     --LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     --CALL librut003_bempresa(w_mae_bod.codemp)
     --RETURNING w_mae_emp.*,existe
     ---- Obteniendo datos de la sucursal
     --CALL librut003_bsucursal(w_mae_bod.codsuc)
     --RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     --DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc
--
     ---- Limpiando combos de categorias y subcategorias
     --LET w_datos.codcat = NULL
     --LET w_datos.subcat = NULL
     --CLEAR codcat,subcat

	 --BEFORE FIELD subcat --NAP lo pase a ON CHANGE
		--IF w_datos.codcat IS NULL THEN
			--ERROR "Error: Debe ingresar Categoria"
			--NEXT FIELD codcat
		--END IF

	 --BEFORE FIELD codabr
		--IF w_datos.codcat IS NULL OR w_datos.subcat IS NULL THEN
			--ERROR "Error: Debe ingresar Categoria o Subcategoria Correcta..."
			--NEXT FIELD NEXT  
		--END IF

    ON CHANGE codcat
     -- Limpiando combos
	  INITIALIZE w_datos.codabr , w_datos.subcat TO NULL 
     CLEAR subcat, codabr

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    --AFTER FIELD codbod
     -- Verificando bodega 
    -- IF w_datos.codbod IS NULL THEN
   --     ERROR "Error: debe de seleccionarse la bodega a listar."
    --    NEXT FIELD codbod
     --END IF


   ON CHANGE subcat
      IF w_datos.codcat IS NULL THEN --NAP
         INITIALIZE w_datos.subcat TO NULL 
         ERROR "Error: Debe ingresar Categoria"
         NEXT FIELD codcat
      END IF
      INITIALIZE w_datos.codabr TO NULL
      CLEAR codabr
      IF w_datos.codcat IS NOT NULL AND w_datos.subcat IS NOT NULL THEN
         CALL librut003_cbxproductosds(w_datos.codcat,w_datos.subcat) 
      END IF

	 BEFORE FIELD detrep
		 IF w_datos.codbod IS NOT NULL THEN
          ERROR "Al especificar bodega no puede emitir el reporte detallado"
			 LET w_datos.detrep=2
			 NEXT FIELD subcat
		 END IF

    AFTER INPUT 
     -- Verificando datos
     IF --w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de bodega   
   LET strcodbod = NULL
   LET strcodbod2= NULL
   IF w_datos.codbod IS NOT NULL THEN
      LET strcodbod  = " AND a.codbod = ",w_datos.codbod
      LET strcodbod2 = " AND x.codbod = ",w_datos.codbod
   END IF

   -- Verificando seleccion de producto 
   LET strcodabr = NULL
   LET strcodabr2= NULL
   IF w_datos.codabr IS NOT NULL THEN
      LET strcodabr = " AND b.cditem = ",w_datos.codabr
      LET strcodabr2= " AND z.cditem = ",w_datos.codabr
   END IF

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   LET strcodcat2= NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = " AND b.codcat = ",w_datos.codcat
      LET strcodcat2= " AND z.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   LET strsubcat2= NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = " AND b.subcat = ",w_datos.subcat
      LET strsubcat2= " AND z.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de fecha
	IF w_datos.fecemi IS NOT NULL OR w_datos.horsis IS NOT NULL THEN
	LET strfecemi = NULL
		IF w_datos.fecemi IS NOT NULL AND (w_datos.horsis IS NULL OR w_datos.horsis = "00:00:00" ) THEN
      	LET strfecemi = " AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
		ELSE	
			LET strfecemi = " AND LPAD(YEAR(a.fecemi),4,0)||LPAD(MONTH(a.fecemi),2,0)||LPAD(DAY(a.fecemi),2,0)||a.horsis <= ",
								 "'",
								 YEAR(w_datos.fecemi) 	USING "&&&&", 
								 MONTH(w_datos.fecemi) 	USING "&&", 
								 DAY(w_datos.fecemi) 	USING "&&",
								 w_datos.horsis CLIPPED,"'"
{
      	LET strfecemi = " AND a.lnktra <= ( SELECT MAX(x.lnktra) FROM inv_dtransac x , inv_products z "||
								 " WHERE LPAD(YEAR(x.fecemi),4,0) ||LPAD(MONTH(x.fecemi),2,0) || LPAD(DAY(x.fecemi),2,0) || x.horsis  <= '",
                           YEAR(w_datos.fecemi) USING "&&&&", MONTH(w_datos.fecemi) USING "&&", DAY(w_datos.fecemi) USING "&&",
                           w_datos.horsis CLIPPED,"'",
								 " AND x.cditem = z.cditem ",
								 " AND a.cditem = x.cditem ",
								 " ",strcodbod2," ",strcodabr2," ",strcodcat2," ",strsubcat2," )"
}
   	END IF
	END IF
	{
   LET strfecemi = NULL
   IF w_datos.fecemi IS NOT NULL THEN
      LET strfecemi = "AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
   END IF

   -- Verificando condicion de hora
	LET strhorsis = NULL
   IF w_datos.horsis IS NOT NULL AND w_datos.horsis <> "00:00:00" THEN
      LET strhorsis = "AND a.horsis <= '",w_datos.horsis CLIPPED,"'"
   END IF
   }

   -- Verificando condicion de existencia actual
   LET strexiact = NULL
   CASE (w_datos.exiact)
    WHEN 1    LET strexiact = "AND a.exican >0"
              LET fexiact   = ">0" 
    WHEN 2    LET strexiact = "AND a.exican =0"
              LET fexiact   = "=0" 
    OTHERWISE LET strexiact = NULL
              LET fexiact   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia minima 
   LET streximin = NULL
   CASE (w_datos.eximin)
    WHEN 1    LET streximin = "AND a.eximin >0"
              LET feximin   = ">0" 
    WHEN 2    LET streximin = "AND a.eximin =0"
              LET feximin   = "=0" 
    OTHERWISE LET streximin = NULL
              LET feximin   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia maxima
   LET streximax = NULL
   CASE (w_datos.eximax)
    WHEN 1    LET streximax = "AND a.eximax >0"
              LET feximax   = ">0" 
    WHEN 2    LET streximax = "AND a.eximax =0"
              LET feximax   = "=0" 
    OTHERWISE LET streximax = NULL
              LET feximax   = "Todas" 
   END CASE 

   -- Construyendo seleccion 
		LET qrytxt = " SELECT a.codemp , a.cditem, b.codabr , b.dsitem , c.nommed , ",
						 " b.codcat,b.subcat,b.codcol,d.codabr,sum(a.opeuni) ",
						 " FROM inv_dtransac a , inv_products b , inv_unimedid c , inv_medidpro d ",
						 " WHERE a.cditem = b.cditem ",
						 --" AND   a.codepq = 0", --Nap comentado como esta en kardex
						 " AND   d.codmed = b.codmed ",
						 " AND   c.unimed = b.unimed ",
						 " AND   a.estado ='V' ",
						 --" AND   a.codbod <> 21 ", --Nap no esta en kardex
						 strfecemi CLIPPED," ",
						 strcodbod CLIPPED," ",
						 strcodcat CLIPPED," ",
						 strsubcat CLIPPED," ",
						 strcodabr CLIPPED," ",
						 " GROUP BY 1,2,3,4,5,6,7,8,9 ",
						 " ORDER BY 1,6,7,8,9 "

	IF FGL_GETENV("LOGNAME") = "sistemas" THEN
		DISPLAY qrytxt
	END IF

{
   	LET qrytxt = " SELECT a.codemp,a.cditem,b.codabr,b.dsitem,c.nommed, ", 
                   " SUM(a.exican) ",
                   " FROM  inv_proenbod a,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e , inv_dtransac f",
                   " WHERE b.cditem = a.cditem ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
						 " AND f.cditem = b.cditem ",
                   " GROUP BY 1,2,3,4,5 ",
                   " ORDER BY 3 "
}
   --LET myHandler = invrep007_reporte("carta","horizaontal","PDF",80,"ANDE_Rep_238")
   IF rdestino = "XLSX" THEN 
      LET rtamanio = "unapagina"
   ELSE 
      LET rtamanio = "carta"
   END IF 
   LET myHandler = invrep007_reporte(rtamanio,"horizaontal",rdestino,80,"ANDE_Rep_238")
	-- CREANDO TABLA TEMPORAL
	CALL invrep007_temptab(1)
	PREPARE c_rep007 FROM qrytxt
   DECLARE c_crep007 CURSOR FOR c_rep007
	FOREACH c_crep007 INTO imp1.codemp,imp1.cditem,imp1.codabr,imp1.dsitem,imp1.nommed,categ,subcat,codcol,codabr_med,imp1.exican
		IF imp1.dsitem MATCHES "*A LA MEDIDA*" THEN
			CONTINUE FOREACH
		END IF
		INITIALIZE imp1.totbod1  , imp1.totbod8  , imp1.totbod12 , imp1.totbod16 , 
                 imp1.totbod20 , imp1.totbod21 , imp1.totbod22 TO NULL
		IF  w_datos.detrep = 1 AND
            w_datos.codbod IS NULL THEN
            
            IF w_datos.fecemi IS NULL THEN 
                LET qrytxt = 
                " SELECT ",
                " SUM ( CASE WHEN codbod =  1 THEN exican ELSE 0 END ) Bod1,",
                " SUM ( CASE WHEN codbod =  8 THEN exican ELSE 0 END ) Bod8,",
                " SUM ( CASE WHEN codbod = 12 THEN exican ELSE 0 END ) Bod12,",
                " SUM ( CASE WHEN codbod = 16 THEN exican ELSE 0 END ) Bod16,",
                " SUM ( CASE WHEN codbod = 20 THEN exican ELSE 0 END ) Bod20,",
                " SUM ( CASE WHEN codbod = 21 THEN exican ELSE 0 END ) Bod21,",
                " SUM ( CASE WHEN codbod = 22 THEN exican ELSE 0 END ) Bod22,",
                " SUM ( exican ) Tot",
                " FROM inv_proenbod d ",
                " WHERE cditem = ?"
                --
                PREPARE st_totbod FROM qrytxt
                EXECUTE st_totbod USING imp1.cditem 
                INTO imp1.totbod1, imp1.totbod8, imp1.totbod12, imp1.totbod16,
                    imp1.totbod20, imp1.totbod21, imp1.totbod22, imp1.exican
            ELSE 
                LET qrytxt = 
                " SELECT ",
                " SUM ( CASE WHEN codbod =  1 THEN opeuni ELSE 0 END ) Bod1,",
                " SUM ( CASE WHEN codbod =  8 THEN opeuni ELSE 0 END ) Bod8,",
                " SUM ( CASE WHEN codbod = 12 THEN opeuni ELSE 0 END ) Bod12,",
                " SUM ( CASE WHEN codbod = 16 THEN opeuni ELSE 0 END ) Bod16,",
                " SUM ( CASE WHEN codbod = 20 THEN opeuni ELSE 0 END ) Bod20,",
                " SUM ( CASE WHEN codbod = 21 THEN opeuni ELSE 0 END ) Bod21,",
                " SUM ( CASE WHEN codbod = 22 THEN opeuni ELSE 0 END ) Bod22,",
                " SUM ( opeuni ) Tot",
                " FROM inv_dtransac a", --strfecemi usa alias a
                " WHERE estado = 'V' ",
                " AND cditem = ?", strfecemi CLIPPED
                --
                PREPARE st_totbodfec FROM qrytxt
                EXECUTE st_totbodfec USING imp1.cditem 
                INTO imp1.totbod1, imp1.totbod8, imp1.totbod12, imp1.totbod16,
                    imp1.totbod20, imp1.totbod21, imp1.totbod22, imp1.exican 
                
            END IF 
				
			IF imp1.totbod1  IS NULL THEN LET imp1.totbod1 =0 END IF
			IF imp1.totbod8  IS NULL THEN LET imp1.totbod8 =0 END IF
			IF imp1.totbod12 IS NULL THEN LET imp1.totbod12=0 END IF
			IF imp1.totbod16 IS NULL THEN LET imp1.totbod16=0 END IF
			IF imp1.totbod20 IS NULL THEN LET imp1.totbod20=0 END IF
			IF imp1.totbod21 IS NULL THEN LET imp1.totbod21=0 END IF
			IF imp1.totbod22 IS NULL THEN LET imp1.totbod22=0 END IF
		END IF

		IF imp1.codemp IS NOT NULL AND
			imp1.cditem IS NOT NULL AND
			imp1.codabr IS NOT NULL AND
			imp1.dsitem IS NOT NULL AND
			imp1.nommed IS NOT NULL AND
			imp1.exican IS NOT NULL THEN
			INSERT INTO totbod VALUES (imp1.*)
		END IF


	END FOREACH
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
	LET hora = TIME
	LET hora2= hora
   LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/invrep007-",
                  hora2[1,2],"_",hora2[4,5],"_",hora2[7,8],".txt"
	DECLARE c_crep002 CURSOR FOR
		SELECT *
		  FROM totbod
		 --ORDER BY 3
   LET existe = FALSE
	INITIALIZE imp1.* TO NULL
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*
       LET existe = TRUE
       START REPORT invrep007_impinvbod --TO filename
    END IF 

			IF w_datos.codbod IS NOT NULL AND w_datos.codbod > 0 THEN
				INITIALIZE w_fact.*,w_tickt.*,w_mov.* TO NULL
				-- Ultima factura hecha
				WHENEVER ERROR CONTINUE
				SELECT nserie , numdoc , fecemi , horsis , lnktra
				  INTO w_fact.* 
              FROM fac_mtransac a , fac_dtransac c
             WHERE a.lnktra = c.lnktra
               AND a.fecemi=(SELECT MAX(o.fecemi)
                             FROM fac_mtransac o , fac_puntovta p , fac_dtransac q
                            WHERE o.numpos =  p.numpos
                              AND o.estado = "V"
                              AND o.lnktra = q.lnktra
                              AND o.tipdoc = 1  
                              AND p.codbod = w_datos.codbod
                              AND q.codabr = w_datos.codabr)
               AND a.horsis=(SELECT MAX(j.horsis)
                             FROM fac_mtransac j , fac_puntovta k, fac_dtransac h
                            WHERE k.numpos =  j.numpos
                              AND j.estado = "V"
                              AND h.lnktra = j.lnktra
                              AND j.tipdoc = 1  
                              AND k.codbod = w_datos.codbod
                              AND h.codabr = w_datos.codabr)
                              AND j.fecemi =(SELECT MAX(o.fecemi)
                                               FROM fac_mtransac o , fac_puntovta p
                                              WHERE o.numpos =  p.numpos
                              						AND o.tipdoc = 1  
                                                AND o.estado = "V"
                                                AND p.codbod = w_datos.codbod  )
				-- Ultimo ticket hecho
				SELECT nserie,numdoc,fecemi,horsis,lnktra
			  	  INTO w_tickt.*
				  FROM fac_mtransac
             WHERE fecemi=(SELECT MAX(o.fecemi)
                             FROM fac_mtransac o , fac_puntovta p
                            WHERE o.numpos =  p.numpos
                              AND o.estado = "V"
                              AND o.tipdoc = 2
                              AND p.codbod = w_datos.codbod)
               AND horsis=(SELECT MAX(j.horsis)
                             FROM fac_mtransac j , fac_puntovta k
                            WHERE k.numpos =  j.numpos
                              AND j.estado = "V"
                              AND j.tipdoc = 2
                              AND k.codbod = w_datos.codbod
                              AND j.fecemi =(SELECT MAX(o.fecemi)
                                               FROM fac_mtransac o , fac_puntovta p
                                              WHERE o.numpos =  p.numpos
                                                AND o.tipdoc = 2
                                                AND o.estado = "V"
                                                AND p.codbod = w_datos.codbod  ))

				-- Ultimo movimiento hecho
				   SELECT a.lnktra , b.nomabr , a.fecemi , a.horsis
			  	     INTO w_mov.lnk_mov , w_mov.nom_mov ,
				     	    w_mov.fec_mov , w_mov.hor_mov
			  	     FROM inv_mtransac a , inv_tipomovs b , inv_dtransac c
			 	    WHERE a.tipmov = b.tipmov
						AND a.lnktra = b.lnktra
						AND b.codabr = imp1.codabr
                  AND a.fecemi = (SELECT MAX(o.fecemi)
                                    FROM inv_mtransac o
                                   WHERE o.codbod = w_datos.codbod
                                     AND o.estado = "V")
                  AND a.horsis = (SELECT MAX(o.horsis)
                                    FROM inv_mtransac o
                                   WHERE o.codbod = w_datos.codbod
                                     AND o.estado = "V"
                                     AND o.fecemi =(SELECT MAX(q.fecemi)
																      FROM inv_mtransac q
																     WHERE q.codbod = w_datos.codbod
                                                       AND q.estado = "V"))
						AND c.codepq = 0
				WHENEVER ERROR STOP
				IF SQLCA.SQLCODE <> 0 THEN
					--DISPLAY SQLERRMESSAGE
					--DISPLAY w_datos.codbod, " - ",imp1.codabr
				END IF
			END IF
    -- Llenando el reporte
    OUTPUT TO REPORT invrep007_impinvbod(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep007_impinvbod 
      --LET myHandler = gral_reporte("carta","horizontal","PDF",80,"ANDE_Rep_238")
      
      -- Enviando reporte al destino seleccionado
		--IF w_datos.codbod IS NOT NULL THEN
		--	CALL librut001_rep_pdf("Inventario Por Bodega",filename,10,"L",4)
		--ELSE
		--	IF w_datos.detrep = 1 THEN
		--		CALL librut001_rep_pdf("Inventario General",filename,8,"L",4)
		--	ELSE
		--		CALL librut001_rep_pdf("Inventario General",filename,10,"L",4)
		--	END IF
		--END IF

      --CALL librut001_enviareporte(filename,pipeline,"Existencias en Bodega")
      --ERROR "" 
      --CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
	-- ELIMINANDO TABLA TEMPORAL
	CALL invrep007_temptab(2)
  END WHILE
 CLOSE WINDOW wrep007a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep007_impinvbod(imp1)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
			totbod1  LIKE inv_proenbod.exican,
			totbod8  LIKE inv_proenbod.exican,
			totbod12 LIKE inv_proenbod.exican,
			totbod16 LIKE inv_proenbod.exican,
			totbod20 LIKE inv_proenbod.exican,
			totbod21 LIKE inv_proenbod.exican,
			totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican
        END RECORD,
        linea     VARCHAR(180),
        exis               ,
        lLines    SMALLINT ,
		  flag_lLocal SMALLINT

  OUTPUT LEFT   MARGIN  0
			RIGHT  MARGIN  0
         TOP    MARGIN  5 
         BOTTOM MARGIN  0 
			-- PAGE LENGTH 41
         PAGE LENGTH 70

  FORMAT 
   FIRST PAGE HEADER
	 LET lLines = 70 --49
	 LET flag_lLocal = FALSE
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "__________________________________________________",
                 "__________________"--"________________________________"
                 --"____________________________________________"
	IF w_datos.detrep  = 2 THEN
	 LET lLines = 62 --41
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "________________________________" 
	END IF

    -- Configurando tipos de letra
    --PRINT ASCII 27
    --PRINT fnt.ini CLIPPED

    -- Imprimiendo Encabezado
	 IF w_mae_bod.codbod IS NOT NULL AND w_mae_bod.codbod > 0 THEN
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
            -- COLUMN 137,PAGENO USING "Pagina: <<<<"
       PRINT COLUMN   1,"Invrep007",
             COLUMN  55,"EXISTENCIAS EN ",w_mae_bod.nombod,
             COLUMN 112,"Fecha : ",TODAY USING "dd/mmm/yyyy"
		 IF w_datos.fecemi IS NULL THEN
          PRINT COLUMN  83,"** AL DIA **",
                COLUMN 112,"Hora  : ",TIME
		 ELSE
          PRINT COLUMN   1,"Al: ",w_datos.fecemi USING "dd/mmm/yyyy",' - ',w_datos.horsis,
                COLUMN 112,"Hora  : ",TIME
		 END IF
	 ELSE
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
	     --COLUMN 137,PAGENO USING "Pagina: <<<<"
		IF w_datos.detrep = 1 THEN
       PRINT COLUMN   1,"Invrep007",
             COLUMN  80,"EXISTENCIAS - GENERALES",
             COLUMN 147,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  83,"** AL DIA **",
             COLUMN 147,"Hora  : ",TIME 
		ELSE
       PRINT COLUMN   1,"Invrep007",
             COLUMN  60,"EXISTENCIAS GENERALES",
             COLUMN 117,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  63,"** AL DIA **",
             COLUMN 117,"Hora  : ",TIME 
		END IF
	 END IF
    PRINT linea 
	 IF w_datos.detrep = 2 THEN
    	PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				"             Total            Doc        Doc        Doc"
    	PRINT "                                                                   Medida   ",
				"            General         Factura     Ticket      Mov."
    	--PRINT "Codigo Prod.       Descripcion del Producto                                 Unidad de ",
				--"                         Total"
    	--PRINT "                                                                             Medida  ",
				--"                        General"
	 ELSE
    	PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				"      Total       Total       Total        Total       Total      Total       Total        Total "
    	PRINT "                                                              Medida  ",
				"     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	 END IF
    PRINT linea
	 LET lLines = lLines - 7

	PAGE HEADER
	 LET lLines = 70
	   IF w_datos.detrep = 2 THEN
		   LET lLines = 69
    	   PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				   "             Total            Doc        Doc        Doc"
    	   PRINT "                                                                   Medida   ",
				   "            General         Factura     Ticket      Mov."
	   ELSE
    	  PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				  "      Total       Total       Total        Total       Total      Total       Total        Total "
    	  PRINT "                                                              Medida  ",
				  "     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	   END IF
      PRINT linea
		LET lLines = lLines - 3


   ON EVERY ROW
    -- Imprimiendo productos
	 IF w_datos.detrep = 2 THEN
		 --IF flag_lLocal = FALSE THEN
       	PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             	COLUMN  20,imp1.dsitem[1,50]                       CLIPPED,
             	COLUMN  75,imp1.nommed[1,10]                       CLIPPED,
             	COLUMN  85,imp1.exican                 USING  "---,--&.&&" 
				 	--COLUMN 106,w_fact.ser_fact                         CLIPPED,1 SPACES,
                          --w_fact.num_fact             USING        "<<<<&",
             	--COLUMN 117,w_tickt.ser_tickt                       CLIPPED,1 SPACES,
                          --w_tickt.num_tickt           USING       "<<<<&",
				 	--COLUMN 129,w_mov.lnk_mov               USING       "<<<<&"
			--LET flag_lLocal=TRUE
		--ELSE
       --PRINT COLUMN   1,imp1.codabr[1,18]                         CLIPPED,
             --COLUMN  20,imp1.dsitem[1,50]                         CLIPPED,
             --COLUMN  78,imp1.nommed[1,10]                         CLIPPED,
             --COLUMN 105,imp1.exican                   USING  "---,--&.&&"
--
		--END IF
 	 ELSE
       PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             COLUMN  20,imp1.dsitem[1,40]                       CLIPPED,
             COLUMN  62,imp1.nommed[1,8]                        CLIPPED,
			    COLUMN  73,imp1.totbod1                USING  "---,--&.&&",
			    COLUMN  85,imp1.totbod8                USING  "---,--&.&&",
			    COLUMN  97,imp1.totbod12               USING  "---,--&.&&",
			    COLUMN 109,imp1.totbod16               USING  "---,--&.&&",
			    COLUMN 121,imp1.totbod20               USING  "---,--&.&&",
			    COLUMN 133,imp1.totbod21               USING  "---,--&.&&",
			    COLUMN 145,imp1.totbod22               USING  "---,--&.&&",
             COLUMN 158,imp1.exican                 USING  "---,--&.&&" 
	 END IF
	  LET lLines = lLines -1
	  IF lLines <= 1 THEN
		  SKIP TO TOP OF PAGE
	  END IF
	  --IF w_datos.detrep = 1 OR 

   ON LAST ROW
    -- Imprimiendo filtros
	 IF lLines >= 3 THEN
       PRINT "FILTROS"
       IF (LENGTH(fcodcat)>0) THEN
          PRINT "Categoria         : ",fcodcat
       END IF
       IF (LENGTH(fsubcat)>0) THEN
          PRINT "Subcategoria      : ",fsubcat
       END IF
	 END IF
	 IF lLines >= 1 THEN
       PRINT    "Existencia Actual : ",fexiact 
	 END IF
END REPORT 
FUNCTION invrep007_temptab(opc)
DEFINE opc INTEGER

	IF opc = 1 THEN
		CREATE TEMP TABLE totbod
		(
		codemp   INTEGER ,
		cditem   INTEGER ,
		codabr   CHAR(20),
		dsitem   VARCHAR(100),
		nommed   CHAR(20),
		totbod1  decimal(14,2),
		totbod8  decimal(14,2),
		totbod12 decimal(14,2),
		totbod16 decimal(14,2),
		totbod20 decimal(14,2),
		totbod21 decimal(14,2),
		totbod22 decimal(14,2),
		exican   DECIMAL(14,2)
		)
	ELSE
		DROP TABLE totbod
	END IF
END FUNCTION

FUNCTION invrep007_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF
        END IF

        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         --DISPLAY "ancho y alto ", ancho, alto
         --CALL fgl_report_configurePageSize(ancho,alto)
         --CALL fgl_report_configurePageSize("11in","8.5in")
         --CALL fgl_report_configurePageSize("8.5in","11in")
         --CALL fgl_report_configureCompatibilityOutput(124,"Monospaced",false,nom_reporte,"","")
         DISPLAY "Tipo de letra Monospaced 124"
         CALL fgl_report_configureCompatibilityOutput(160,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureCompatibilityOutput(124,"DialogInput",false,nom_reporte,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         --CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           --CALL fgl_report_configureXLSXDevice(null,null,null,null,null,null,1)
           CALL fgl_report_configureXLSXDevice(null,null,null,null,1,null,1)
        ELSE 
           CALL fgl_report_configurePageSize("11in","8.5in") 
        END IF
        CALL fgl_report_selectDevice(tipo)
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION