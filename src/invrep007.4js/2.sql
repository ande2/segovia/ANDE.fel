
            SELECT a.nserie, a.numdoc,a.fecemi,a.horsis,a.lnktra
              FROM fac_mtransac a , fac_puntovta b
             WHERE b.numpos = a.numpos
               AND b.codbod = 20
               AND a.tipdoc = 1
               AND a.fecemi = (SELECT MAX(o.fecemi) FROM fac_mtransac o , fac_puntovta p
                               WHERE o.numpos =  p.numpos AND p.codbod = b.codbod)
               AND a.horsis = (SELECT MAX(j.horsis) FROM fac_mtransac j , fac_puntovta k
                               WHERE k.numpos =  j.numpos AND k.codbod = b.codbod AND j.fecemi = (SELECT MAX(o.fecemi) FROM fac_mtransac o , fac_puntovta p
                               WHERE o.numpos =  p.numpos AND p.numpos = b.numpos))
               AND a.estado = "V"
;
            SELECT a.nserie,a.numdoc,a.fecemi,a.horsis,a.lnktra
              FROM fac_mtransac a , fac_puntovta b
             WHERE b.numpos = a.numpos
               AND b.codbod = 20
               AND a.fecemi = (SELECT MAX(o.fecemi) FROM fac_mtransac o , fac_puntovta p
                               WHERE o.numpos =  p.numpos AND p.codbod=b.codbod)
               AND a.horsis = (SELECT MAX(o.horsis) FROM fac_mtransac o , fac_puntovta p
                               WHERE o.numpos =  p.numpos AND p.codbod=b.codbod)
               AND a.tipdoc = 2
               AND a.estado = "V"
;
            SELECT a.lnktra , b.nomabr , a.fecemi , a.horsis
              FROM inv_mtransac a , inv_tipomovs b
             WHERE a.tipmov = b.tipmov
               AND a.estado = "V"
               AND a.codbod = 20
               AND a.fecemi = (SELECT MAX(o.fecemi) FROM inv_mtransac o
                               WHERE o.codbod = a.codbod )
               AND a.horsis = (SELECT MAX(o.horsis) FROM inv_mtransac o
                               WHERE o.codbod = a.codbod )
