{ 
Fecha    : Abril 2011  
Programo : facrep002.4gl 
Objetivo : Reporte de ordenes de trabajo internas  para confeccion 
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_pto RECORD LIKE fac_puntovta.*,
       proveedor VARCHAR(125,0),
		 v_orden DYNAMIC ARRAY OF RECORD
		   seleccion   SMALLINT   ,
			lnkord      INTEGER    ,
			subcat      VARCHAR(50),
			medida      VARCHAR(100),
			cantidad    INTEGER     ,
			cliente     VARCHAR(100),
			observ      VARCHAR(100),
			color       VARCHAR(100)
		 END RECORD,
		 arr , s_line INTEGER,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       wpais     VARCHAR(100), 
       existe    SMALLINT,
       filename  STRING,
       fnumpos   STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facrep003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep002_ordentra()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep002_ordentra()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        imp1              RECORD LIKE vis_ordentraint.*, 
        pipeline,qrytxt   STRING,
        shell             STRING,
        dir_win           VARCHAR(512),
        dir_fed           STRING,
        strnumpos         STRING,
        horario_string    STRING,
        loop , i , o      SMALLINT,
        lastkey           SMALLINT,
        abrio             INTEGER

    DEFINE myHandler om.SaxDocumentHandler 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep006a AT 5,2
  WITH FORM "facrep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep002",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrep003-",TODAY USING "dd-mmm-yyyy",".txt"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de puntos de venta
  CALL librut003_cbxpuntosventa() 

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE proveedor,pipeline TO NULL
--	LET w_datos.fec_ini = TODAY
--	LET w_datos.fec_fin = TODAY
   CLEAR FORM
	CALL facrep003_llena()
	CALL librut002_combobox("proveedor","SELECT nomprv, nomprv FROM inv_provedrs")

	LET o = v_orden.getLength()

   -- Construyendo busqueda
	DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME proveedor ATTRIBUTES(WITHOUT DEFAULTS )



    AFTER INPUT 
     -- Verificando datos
     IF proveedor IS NULL THEN
		  CALL fgl_winmessage("atention","Debe ingresar un proveedor","Atencion")
			NEXT FIELD proveedor
	  ELSE
     END IF
   END INPUT
	INPUT ARRAY v_orden FROM sd_orden.*

		BEFORE INPUT
			CALL DIALOG.setActionHidden("insert",1)
			CALL DIALOG.setActionHidden("delete",1)
			CALL DIALOG.setActionHidden("append",1)

		AFTER ROW  
			LET lastkey=FGL_LASTKEY()
			IF (ARR_CURR() = o) AND ((lastkey = FGL_KEYVAL("down")) OR (lastkey = FGL_KEYVAL("tab")) OR (lastkey = FGL_KEYVAL("return"))) THEN
				ERROR "Error: No hay mas lineas por debajo"
				NEXT FIELD seleccion
			END IF
	END INPUT	

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

--    ON ACTION visualizar
     -- Asignando dispositivo 
  --   LET pipeline = "screen" 

     -- Obteniendo filtros
     --LET fnumpos = GET_FLDBUF(w_datos.numpos)
	   IF facrpt003_valida() = TRUE THEN
		  EXIT DIALOG
		ELSE
			CALL fgl_winmessage("atention","Debe seleccionar alguna orden","stop")
			NEXT FIELD seleccion
		END IF
     -- Verificando datos 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fnumpos = GET_FLDBUF(w_datos.numpos)
	  EXIT DIALOG	

     -- Verificando datos 

	AFTER DIALOG 
		IF pipeline IS NULL THEN
        NEXT FIELD proveedor
		END IF

	END DIALOG
   IF NOT loop THEN
      EXIT WHILE
   END IF 


   -- Construyendo seleccion 
   --LET qrytxt = " SELECT a.* ",
    --            " FROM inv_ordentra a ",
                --" WHERE a.fechaorden >= '",w_datos.fec_ini,"' ",
                --" AND   a.fechaorden <= '",w_datos.fec_fin,"' ", horario_string CLIPPED,"  ",
   --              --strnumpos CLIPPED, 
--                " ORDER BY 1,2,4"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   --PREPARE c_rep006 FROM qrytxt 
   --DECLARE c_crep006 CURSOR FOR c_rep006
   
   LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
   
   LET existe = FALSE
	FOR i = 1 to v_orden.getLength()
   --FOREACH c_crep006 INTO imp1.* 
		IF v_orden[i].seleccion = 1 THEN
    		IF NOT existe THEN
       		-- Seleccionando fonts para impresora epson
       		CALL librut001_fontsprn(pipeline,"epson")
       		RETURNING fnt.*

       		LET existe = TRUE
       		LET existe = TRUE
       		START REPORT facrep002_impordenint --TO filename
    		END IF 
	 		CALL facrep002_actualiza(v_orden[i].lnkord)

    		-- Llenando el reporte
    		OUTPUT TO REPORT facrep002_impordenint(v_orden[i].*)
		END IF
	END FOR
   --END FOREACH
   --CLOSE c_crep006 
   --FREE  c_crep006 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT facrep002_impordenint 
		IF pipeline <> "local" THEN
      	-- Enviando reporte al destino seleccionado
      	CALL librut001_enviareporte(filename,pipeline,"Ordenes de Trabajo Internas Para Confeccion") 
		ELSE
      LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
      {
			LET shell="a2pdf --icon=/app/scapp.dir/ifc/img/logo_seg.jpg --icon-scale=0.15 --noline-numbers --title='Orden de Compra' ",
				  filename CLIPPED ," -o ",FGL_GETENV("SPOOLDIR") CLIPPED,"/","ordenes-",today using "dd-mmm-yyyy",".pdf "
		   RUN shell	
			LET dir_fed=FGL_GETENV("SPOOLDIR") CLIPPED||"/"||"ordenes-",today using "dd-mmm-yyyy",".pdf"
			LET dir_win="C:\\temp\\ordenes-",today using "dd-mmm-yyyy",".pdf"
			DISPLAY dir_fed
			DISPLAY dir_win
			CALL fgl_putfile(dir_fed,dir_win)
			LET abrio = WINSHELLEXEC(dir_win) 
			DISPLAY abrio
         }
		END IF
      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 

  END WHILE
 CLOSE WINDOW wrep006a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT facrep002_impordenint(imp1)
 DEFINE imp1              RECORD 
         seleccion   SMALLINT   ,
         lnkord      INTEGER    ,
         subcat      VARCHAR(50),
         medida      VARCHAR(100),
			cantidad    INTEGER     ,
         cliente     VARCHAR(100),
			observ      VARCHAR(100),
			color       VARCHAR(100)
        END RECORD, 
		  lineas      SMALLINT    ,
		  total       INTEGER     ,
		  i           SMALLINT    ,
        linea             CHAR(158)

  OUTPUT LEFT MARGIN 2
         PAGE LENGTH 55
         TOP MARGIN 3 
         BOTTOM MARGIN 2 
  FORMAT 
   PAGE HEADER
    LET total  =  0
    LET lineas = 30
    LET linea = "--------------------------------------------------",
                "--------------------------------------"
    -- Configurando tipos de letra
--    PRINT fnt.ini CLIPPED,fnt.t66 CLIPPED,fnt.cmp CLIPPED,fnt.p12 CLIPPED

    -- Imprimiendo Encabezado
    PRINT COLUMN  66, "Guatemala, ",TODAY USING "dd/mmm/yyyy" 
	 PRINT COLUMN 1, "LONAS SEGOVIA, S.A."
	 PRINT COLUMN 1, "2a. Avenida 3-91, Zona 9"
	 PRINT COLUMN 1, "2334 - 2612"
    PRINT COLUMN 1, "Nit: 2942515-8"
	 SKIP 1 LINES
	 PRINT COLUMN 1, "Sr(s). ",proveedor CLIPPED,":"
	 SKIP 1 LINES
	 PRINT COLUMN 4, "Por este medio le hacemos el pedido de lo siguiente:"

    PRINT linea 
    PRINT "|      Clase         |      Color      |       Medida                |       Cantidad        | "
    PRINT linea

   ON EVERY ROW
		LET total = total + imp1.cantidad
    -- Imprimiendo ordenes 
	 IF imp1.observ IS NULL THEN
    	PRINT COLUMN   1,"|",
			 	COLUMN   4,imp1.subcat                              ,
          	COLUMN  24,"|",
          	COLUMN  31,imp1.medida[1,30]                        ,
          	COLUMN  60,"|",
          	COLUMN  69,imp1.cantidad              USING "####"  ,
          	COLUMN  84,"|"
	 ELSE
    	PRINT COLUMN   1,"|",
			 	COLUMN   4,imp1.subcat  ," ",imp1.observ[1,10]      ,
          	COLUMN  24,"|",
          	COLUMN  31,imp1.medida[1,30]                        ,
          	COLUMN  60,"|",
          	COLUMN  69,imp1.cantidad              USING "####"  ,
          	COLUMN  84,"|"
	 END IF
	 LET lineas = lineas -1

 
   AFTER GROUP OF imp1.subcat 
    -- Totalizando por bodega
    --SKIP 1 LINES
	 IF lineas > 0 THEN
		FOR i = 1 TO lineas
    		PRINT COLUMN   1,"|",
          		COLUMN  28,"|",
          		COLUMN  64,"|",
          		COLUMN  88,"|"
		END FOR
	 END IF
    PRINT linea
    PRINT "Total: ", total USING "<<<,<<&"," Orden(es) de Trabajo"
    SKIP 2 LINES 

   ON LAST ROW
    -- Imprimiendo filtros
    --PRINT fnt.twd CLIPPED,"FILTROS",fnt.fwd CLIPPED
		PRINT COLUMN 5,"Me subscribo, LONAS SEGOVIA, S.A."
		
END REPORT 

FUNCTION facrep002_actualiza(orden)
DEFINE orden INTEGER

	SELECT nofase
	  FROM inv_ordentra
	 WHERE lnkord = orden
	   AND nofase = 1
	IF STATUS = 0 THEN
		--UPDATE inv_ordentra set nofase=2
		 --WHERE lnkord = orden
	END IF 

END FUNCTION

FUNCTION facrep003_llena()
DEFINE i , o  SMALLINT,
		 medida INTEGER,
		 xlargo DECIMAL(5,2),
		 yancho DECIMAL(5,2),
		 query_txt VARCHAR(512,0)
		 
	LET query_txt = " SELECT '0', a.lnkord , a.subcat , a.xlargo ,",
                   " a.yancho  , a.cantid , a.medida , a.observ ,",
						 " b.nomcol ",
						 " FROM inv_ordentra a , inv_colorpro b ",
						 " WHERE a.ordext = 1 ",
						 " AND   a.nofase = 1 ",
						 " AND   a.codcol = b.codcol "
		--SELECT 0        ,  lnkord  , subcat  ,
	DECLARE llenar CURSOR FROM query_txt
{
	  	SELECT             lnkord  , subcat  ,
             xlargo   ,  yancho  , medida
		  FROM inv_ordentra
		 WHERE ordext = 1
		   AND nofase = 1
}
		SELECT COUNT(lnkord)
		  INTO o
		  FROM inv_ordentra
		 WHERE ordext = 1
		   AND nofase = 1

	LET i = 1
	FOREACH llenar INTO v_orden[i].seleccion , v_orden[i].lnkord , v_orden[i].subcat , xlargo           , yancho,
                       v_orden[i].cantidad  , medida            , v_orden[i].observ , v_orden[i].color

		SELECT a.nomcli
		  INTO v_orden[i].cliente
		  FROM fac_mtransac a , inv_ordentra b
		 WHERE a.lnktra  = b.lnktra
		   AND b.lnkord  = v_orden[i].lnkord 

		SELECT nomsub
		  INTO v_orden[i].subcat
		  FROM inv_subcateg
		 WHERE subcat = v_orden[i].subcat 

		IF medida = 1 THEN
			LET v_orden[i].medida= xlargo USING "<<<<.<<",' X ', yancho USING "<<<<.<<"," PIES"
		ELSE
			LET v_orden[i].medida= xlargo USING "<<<<.<<",' X ', yancho USING "<<<<.<<"," METROS"
		END IF

		IF i < o THEN
			LET i = i + 1
		END IF
	END FOREACH
		
END FUNCTION

FUNCTION facrpt003_valida()
DEFINE i SMALLINT

	FOR i = 1 to v_orden.getLength()
		IF v_orden[i].seleccion = 1 THEN
			RETURN TRUE
		END IF
	END FOR

RETURN FALSE
END FUNCTION
