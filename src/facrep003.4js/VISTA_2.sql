SELECT c.fecemi , c.nserie[1]||' '||c.numdoc ,a.nompos , b.nomemp , c.totpag , c.subtot , c.totiva , d.nomusr,
CASE WHEN (c.hayord = 0) THEN "PRODUCTOS" WHEN (c.hayord =1 ) THEN "ORDEN #"||(select j.lnkord FROM inv_morden j where j.lnktra = c.lnktra)
 END CASE, c.lnktra
FROM fac_puntovta a , glb_empresas b , fac_mtransac c , glb_usuarios d
where a.codemp = b.codemp
and   c.numpos = a.numpos
and   c.codemp = b.codemp
and   c.usrope = d.userid
and   c.estado ="V"
