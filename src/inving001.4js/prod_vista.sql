DROP VIEW productos
;
CREATE VIEW productos(cditem, codcat, subcat, codcol, codprv, codmed, unimed,
                      dsitem, codabr, premin, presug, pulcom, estado, status,
                      fulcam, userid, fecsis, horsis, tipo) as
SELECT *,0 tipo FROM inv_products
UNION
SELECT a.lnkord , d.codcat , c.subcat ,
       b.codcol , 0 codprv , f.codmed ,
       g.unimed ,
       c.nomsub||' '||h.nomcol||' A LA MEDIDA, ORDEN # '||b.lnkord||' LONA '||b.nuitem dsitem,
       e.codabr , b.precio , b.precio ,
       e.pulcom , e.estado , e.status ,
       e.fulcam , a.userid , a.fecsis ,
       a.horsis , b.nuitem
  FROM inv_morden a   , inv_dorden b   ,
       inv_subcateg c , inv_categpro d ,
       inv_products e , inv_medidpro f ,
       inv_unimedid g , inv_colorpro h
 WHERE a.lnkord = b.lnkord
   AND b.subcat = c.subcat
   AND c.codcat = d.codcat
   AND b.codcol = h.codcol
   AND g.nommed = "UNIDAD"
   AND f.codabr = "NOAPL"
   AND e.codcat = d.codcat
   AND e.subcat = c.subcat
   AND e.dsitem LIKE "%"||c.nomsub||"%MEDIDA%"
