{
Fecha    : Diciembre 2010 
Programo : Mynor Ramirez 
Objetivo : Impresion de movimientos.
}

-- Definicion de variables globales  

GLOBALS "invglb001.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECORD,
       existe,x,y   SMALLINT, 
       nlinea       SMALLINT, 
       cambio       SMALLINT, 
       largo        SMALLINT, 
       w_flag       SMALLINT,
       filename     CHAR(90),
       pipeline     CHAR(90),
       titulo       CHAR(50),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION invrpt001_reportemov()
DEFINE opc SMALLINT
-- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el movimiento.","information")

  LET opc = librut001_menugraba("Confirmacion",
                                "Desea imprimir el movimiento?",
                                "Si",
                                "No",
                                "Cancelar",
                                "")
 CASE opc
	WHEN 0
 		CALL fgl_winmessage(" Atencion","No se imprimira el movimiento.","information")
	WHEN 1
	 -- Definiendo archivo de impresion
	 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/inving001.txt"

	 -- Obteniendo datos del tipo de movimiento
	 INITIALIZE w_mae_tip.* TO NULL
	 CALL librut003_btipomovimiento(w_mae_tra.tipmov)
	 RETURNING w_mae_tip.*,existe

	 -- Asignando valores
	 LET pipeline = "local"   
	 LET nlinea   = 42
	 LET largo    = 20 

	 -- Seleccionando fonts para impresora epson
	 CALL librut001_fontsprn(pipeline,"epson") 
	 RETURNING fnt.* 

	 -- Iniciando reporte
	 START REPORT invrpt001_printmov TO filename 
	  OUTPUT TO REPORT invrpt001_printmov()
	 FINISH REPORT invrpt001_printmov 

	 -- Imprimiendo el reporte

	 CALL librut001_rep_pdf("Movimientos de Inventario",filename,9,"L",1)
--	 CALL librut001_enviareporte(filename,
--	                             pipeline,
--	                             "")
	WHEN 2
 		CALL fgl_winmessage(" Atencion","No se imprimira el movimiento.","information")
 END CASE
END FUNCTION 

-- Subrutinar para generar la impresion del movimiento 

REPORT invrpt001_printmov() 
 DEFINE linea     CHAR(130),
        exis      SMALLINT, 
        lh,lv,si  CHAR(1),
        sd,ii,id  CHAR(1),
        x         CHAR(1),
        w_tipcob  CHAR(7),
        w_tipcar  CHAR(7)

  OUTPUT LEFT   MARGIN 5
         PAGE   LENGTH 66
         TOP    MARGIN 3
         BOTTOM MARGIN 3

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "_____________________________________________________________",
               "_____________________________________________________________",
               "_____________________________________________________________",
               "______________________"


   -- Inicializando impresora
	DISPLAY fnt.ini CLIPPED," FUENTE"
	PRINT fnt.ini CLIPPED 
   PRINT 1 SPACES 
	--SKIP 1 LINES

   -- Imprimiendo Encabezado
   PRINT COLUMN   1,"Inventario",
         COLUMN 110,PAGENO USING  "Pagina: <<"
   PRINT COLUMN   1,"invrpt001",
         COLUMN 110,"Fecha : ",TODAY USING "dd/mmm/yyyy"
   PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
         COLUMN 110,"Hora  : ",TIME

   PRINT linea
   PRINT COLUMN   1,w_mae_tip.nommov CLIPPED
   PRINT COLUMN   1,"Movimiento Numero (",w_mae_tra.lnktra USING "<<<<<<<<<<<",")"
   PRINT COLUMN   1,"Empresa           : ",
                    w_mae_tra.codemp USING "<<<<<<"," ",
                    w_mae_emp.nomemp CLIPPED,
         COLUMN  95,"Fecha de Registro : ",
                    w_mae_tra.fecsis USING "dd/mm/yyyy"

   PRINT COLUMN   1,"Sucursal          : ",
                    w_mae_tra.codsuc USING "<<<<<<"," ",
                    w_mae_suc.nomsuc CLIPPED,
         COLUMN  95,"Hora de Registro  : ",
                    w_mae_tra.horsis 

   PRINT COLUMN   1,"Bodega            : ",
                    w_mae_tra.codbod USING "<<<<<<"," ",
                    w_mae_bod.nombod CLIPPED,
         COLUMN  95,"Operador          : ",
                    w_mae_tra.userid 

   PRINT COLUMN   1,"Fecha Movimiento  : ",
                    w_mae_tra.fecsis USING "dd/mm/yyyy",
         COLUMN  95,"Estado            : ",
                    w_mae_tra.estado  

   -- Verificando tipo de operacion del movimiento
   IF (w_mae_tra.tipope=1) THEN 
      PRINT COLUMN   1,"Origen            : ",
                        w_mae_tra.codori USING "<<<<<<", " ",nomori CLIPPED 
   ELSE
      PRINT COLUMN   1,"Destino           : ",
                       w_mae_tra.coddes USING "<<<<<<", " ",nomdes CLIPPED 
   END IF 

   PRINT COLUMN   1,"Docto Referencia 1: ",
                    w_mae_tra.numrf1
   PRINT COLUMN   1,"Docto Referencia 2: ",
                    w_mae_tra.numrf2

   PRINT COLUMN   1,"Observaciones     : ",
                    w_mae_tra.observ[1,100]
   PRINT COLUMN  25,w_mae_tra.observ[101,200]

   PRINT 1 SPACES

   -- Imprimiendo rotulo del detalle de productos
   PRINT COLUMN   1,"DETALLE DEL MOVIMIENTO"
   PRINT COLUMN   1,"Codigo               D E S C R I P C I O N                                 Cantidad    Cantidad      ",
                    "  Precio          Total"   
   PRINT COLUMN   1,"Producto                                                                   Empaque     TOTAL         ",
                    "  Unitario        Producto"  
   PRINT linea
   PRINT 1 SPACES

  ON EVERY ROW
   -- Imprimiendo detalle de productos
   FOR i = 1 TO totlin
    PRINT COLUMN   1,v_products[i].cditem                              ,
          COLUMN  22,v_products[i].dsitem                              , 2 SPACES,
                     v_products[i].canepq       USING "###,##&.&&"     , 2 SPACES,
                     v_products[i].canuni       USING "###,##&.&&"     , 2 SPACES,
                     v_products[i].preuni       USING "###,##&.&&&&&&" , 2 SPACES,
                     v_products[i].totpro       USING "###,###,##&.&&" 
   END FOR

  ON LAST ROW
   -- Imprimiendo totales y pie de pagina 
   PRINT linea
   PRINT COLUMN   1,"TOTALES --->"                                     , 73 SPACES,
                    totuni                      USING "###,##&.&&"     , 18 SPACES,
                    totval                      USING "###,###,##&.&&" 
   SKIP 1 LINES
   PRINT "Productos (",totlin USING "<<<",")"

   -- Verificando si es reimpresion
   SKIP 1 LINES
   IF reimpresion THEN
      PRINT "**REIMPRESION**" 
   ELSE
      PRINT "**ORIGINAL**" 
   END IF 
END REPORT 
