<?xml version='1.0' encoding='ANSI_X3.4-1968'?>
<Form name="inving003a" build="2.30.06-1815.7" windowStyle="completa" spacing="compact" style="8pt" width="145" height="22">
  <Group style="fondo5">
    <VBox>
      <Grid width="123" height="2">
        <Image name="imag" width="4" image="mars_screen2.bmp" autoScale="1" posY="0" posX="1" gridWidth="4"/>
        <Label name="labela" width="2" fontPitch="fixed" style="dmagenta negrita 12pt" posY="0" posX="7" gridWidth="2"/>
        <Label name="labelz" width="111" color="red" posY="0" posX="11" gridWidth="111"/>
        <Label text="Usuario:" posY="1" posX="2" gridWidth="8"/>
        <FormField name="formonly.userid" colName="userid" fieldId="20" sqlTabName="formonly" tabIndex="1">
          <Edit width="11" shift="down" posY="1" posX="11" gridWidth="11"/>
        </FormField>
        <Label text="Estado:" posY="1" posX="24" gridWidth="7"/>
        <FormField name="formonly.estado" colName="estado" fieldId="23" sqlTabName="formonly" tabIndex="2">
          <ComboBox width="13" posY="1" posX="32" gridWidth="15">
            <Item name="V" text="VIGENTE"/>
            <Item name="A" text="ANULADO"/>
          </ComboBox>
        </FormField>
        <Label text="Fecha:" posY="1" posX="49" gridWidth="6"/>
        <FormField name="formonly.fecsis" colName="fecsis" fieldId="21" sqlTabName="formonly" tabIndex="3">
          <DateEdit width="11" posY="1" posX="56" gridWidth="13"/>
        </FormField>
        <Label text="Hora:" posY="1" posX="71" gridWidth="5"/>
        <FormField name="formonly.horsis" colName="horsis" fieldId="22" sqlTabName="formonly" tabIndex="4">
          <TimeEdit width="20" posY="1" posX="77" gridWidth="20"/>
        </FormField>
      </Grid>
      <Grid width="145" height="22">
        <Group name="group1" sizePolicy="fixed" posY="0" posX="0" gridWidth="119" gridHeight="8">
          <Label name="labelb" width="107" text="Informacion de la orden" style="dmagenta negrita" posY="1" posX="1" gridWidth="107"/>
          <Label text="Asignacion #:" posY="2" posX="1" gridWidth="13"/>
          <FormField name="formonly.lnkasi" colName="lnkasi" fieldId="19" sqlTabName="formonly" tabIndex="5">
            <Edit width="5" posY="2" posX="15" gridWidth="5"/>
          </FormField>
          <Label text="Orden #:" posY="3" posX="6" gridWidth="8"/>
          <FormField name="formonly.lnkord" colName="lnkord" fieldId="11" sqlTabName="formonly" required="1" tabIndex="6">
            <ButtonEdit width="4" color="red" action="listordenes" image="mars_listar" posY="3" posX="15" gridWidth="6"/>
          </FormField>
          <Label text="Fecha Ord:" posY="3" posX="41" gridWidth="10"/>
          <FormField name="formonly.fecord" colName="fecord" fieldId="12" sqlTabName="formonly" tabIndex="7">
            <DateEdit width="12" comment="Fecha en la cual se genero la orden" posY="3" posX="52" gridWidth="14"/>
          </FormField>
          <Label text="Material:" posY="4" posX="5" gridWidth="9"/>
          <FormField name="formonly.subcat" colName="subcat" fieldId="13" sqlTabName="formonly" tabIndex="8">
            <ComboBox width="23" comment="Material de la orden de trabajo" posY="4" posX="15" gridWidth="25"/>
          </FormField>
          <Label text="Color:" posY="4" posX="45" gridWidth="6"/>
          <FormField name="formonly.codcol" colName="codcol" fieldId="14" sqlTabName="formonly" tabIndex="9">
            <ComboBox width="12" comment="Color de la orden de trabajo" posY="4" posX="52" gridWidth="14"/>
          </FormField>
          <Label text="Largo:" posY="4" posX="68" gridWidth="6"/>
          <FormField name="formonly.xlargo" colName="xlargo" fieldId="15" sqlTabName="formonly" tabIndex="10">
            <Edit width="7" comment="Largo de la lona" posY="4" posX="75" gridWidth="7"/>
          </FormField>
          <Label text="Ancho:" posY="4" posX="83" gridWidth="6"/>
          <FormField name="formonly.yancho" colName="yancho" fieldId="16" sqlTabName="formonly" tabIndex="11">
            <Edit width="6" comment="Ancho de la lona" posY="4" posX="90" gridWidth="6"/>
          </FormField>
          <FormField name="formonly.medida" colName="medida" fieldId="18" sqlTabName="formonly" tabIndex="12">
            <ComboBox width="8" comment="Medida en la que fue tomada la orden" posY="4" posX="98" gridWidth="10">
              <Item name="1" text="PIES"/>
              <Item name="0" text="METROS"/>
            </ComboBox>
          </FormField>
          <Label text="Cliente:" posY="5" posX="6" gridWidth="8"/>
          <FormField name="formonly.nomcli" colName="nomcli" fieldId="17" sqlTabName="formonly" tabIndex="13">
            <Edit width="93" comment="Nombre del cliente que hizo la orden" posY="5" posX="15" gridWidth="93"/>
          </FormField>
          <Label name="labelc" width="87" text="Detalle de Productos" style="dmagenta negrita" posY="6" posX="1" gridWidth="87"/>
        </Group>
        <Table pageSize="12" name="tabla1" style="arial detail3 small" fontPitch="fixed" sizePolicy="dynamic" width="116" posY="8" posX="0" gridWidth="116" gridHeight="13" tabName="s_products">
          <TableColumn name="formonly.codpro" colName="codpro" fieldId="0" sqlTabName="formonly" hidden="1" tabIndex="14">
            <Edit width="2"/>
          </TableColumn>
          <TableColumn name="formonly.cditem" colName="cditem" fieldId="1" sqlTabName="formonly" text="Producto" tabIndex="15">
            <ButtonEdit width="16" shift="up" image="mars_listar" action="listproducto" comment="Codigo abreviado del producto."/>
          </TableColumn>
          <TableColumn name="formonly.dsitem" colName="dsitem" fieldId="2" sqlTabName="formonly" text="Descripcion del Producto" noEntry="1" tabIndex="16">
            <Edit width="33" style="negrita"/>
          </TableColumn>
          <TableColumn name="formonly.unimed" colName="unimed" fieldId="3" sqlTabName="formonly" hidden="1" tabIndex="17">
            <Edit width="2"/>
          </TableColumn>
          <TableColumn name="formonly.nomuni" colName="nomuni" fieldId="4" sqlTabName="formonly" text="Unidad Medida&#10;Inventario" noEntry="1" tabIndex="18">
            <Edit width="15" comment="Unidad de medida de inventario del producto."/>
          </TableColumn>
          <TableColumn name="formonly.canori" colName="canori" sqlType="INTEGER" fieldId="5" sqlTabName="formonly" text="  Cantidad" tabIndex="19" numAlign="1">
            <Edit width="10" format="###,##&amp;.&amp;&amp;" comment="Cantidad del producto."/>
          </TableColumn>
          <TableColumn name="formonly.unimto" colName="unimto" sqlType="INTEGER" fieldId="6" sqlTabName="formonly" text="Unidad Medida&#10;Venta" tabIndex="20" numAlign="1">
            <ComboBox width="13" sizePolicy="fixed" comment="Unidad de medida de venta del producto."/>
          </TableColumn>
          <TableColumn name="formonly.canuni" colName="canuni" sqlType="INTEGER" fieldId="7" sqlTabName="formonly" text="Cantidad&#10;De Venta" tabIndex="21" numAlign="1">
            <Edit width="10" format="###,##&amp;.&amp;&amp;" comment="Cantidad de venta del producto."/>
          </TableColumn>
          <TableColumn name="formonly.preuni" colName="preuni" sqlType="DECIMAL" fieldId="8" sqlTabName="formonly" text="Precio&#10;Unitario" tabIndex="22" numAlign="1">
            <Edit width="13" format="##,##&amp;.&amp;&amp;&amp;&amp;&amp;&amp;" comment="Precio unitario del producto."/>
          </TableColumn>
          <TableColumn name="formonly.totpro" colName="totpro" sqlType="DECIMAL" fieldId="9" sqlTabName="formonly" text="   T O T A L" noEntry="1" tabIndex="23" numAlign="1">
            <Edit width="13" format="##,###,##&amp;.&amp;&amp;"/>
          </TableColumn>
          <TableColumn name="formonly.factor" colName="factor" fieldId="10" sqlTabName="formonly" hidden="1" tabIndex="24">
            <Edit width="2"/>
          </TableColumn>
        </Table>
      </Grid>
    </VBox>
  </Group>
  <RecordView tabName="formonly">
    <Link colName="codpro" fieldIdRef="0"/>
    <Link colName="cditem" fieldIdRef="1"/>
    <Link colName="dsitem" fieldIdRef="2"/>
    <Link colName="unimed" fieldIdRef="3"/>
    <Link colName="nomuni" fieldIdRef="4"/>
    <Link colName="canori" fieldIdRef="5"/>
    <Link colName="unimto" fieldIdRef="6"/>
    <Link colName="canuni" fieldIdRef="7"/>
    <Link colName="preuni" fieldIdRef="8"/>
    <Link colName="totpro" fieldIdRef="9"/>
    <Link colName="factor" fieldIdRef="10"/>
    <Link colName="lnkord" fieldIdRef="11"/>
    <Link colName="fecord" fieldIdRef="12"/>
    <Link colName="subcat" fieldIdRef="13"/>
    <Link colName="codcol" fieldIdRef="14"/>
    <Link colName="xlargo" fieldIdRef="15"/>
    <Link colName="yancho" fieldIdRef="16"/>
    <Link colName="nomcli" fieldIdRef="17"/>
    <Link colName="medida" fieldIdRef="18"/>
    <Link colName="lnkasi" fieldIdRef="19"/>
    <Link colName="userid" fieldIdRef="20"/>
    <Link colName="fecsis" fieldIdRef="21"/>
    <Link colName="horsis" fieldIdRef="22"/>
    <Link colName="estado" fieldIdRef="23"/>
  </RecordView>
</Form>
