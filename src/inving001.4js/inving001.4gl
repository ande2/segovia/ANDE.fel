{
Fecha    : Diciembre 2010 
Programo : Mynor Ramirez
Objetivo : Programa de ingreso de movimientos inventario.
}

-- Definicion de variables globales

GLOBALS "invglb001.4gl"
DEFINE regreso     SMALLINT,
       existe      SMALLINT,
       msg         STRING 

-- Subrutina principal

MAIN
  DEFINE
      dbname      CHAR(20),
      n_param     SMALLINT
      
 -- Atrapando interrupts
 DEFER INTERRUPT

  LET n_param = num_args()

   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = ARG_VAL(1)
      DATABASE dbname
   END IF

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 IF ui.Interface.getFrontEndName() = "GDC" THEN
   CALL ui.Interface.loadStyles("styles")
 END IF
 CALL ui.Interface.loadToolbar("toolbar2")

 --CALL ui.Interface.loadActionDefaults(FGL_GETENV("BASEDIR")||"/std/actiondefaults")
 --CALL ui.Interface.loadStyles(FGL_GETENV("BASEDIR")||"/std/styles")
 --CALL ui.Interface.loadToolbar(FGL_GETENV("BASEDIR")||"/std/toolbar2")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("inving001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de opciones
 CALL inving001_menu()
END MAIN

-- Subutina para el menu de movimientos de inventario 

FUNCTION inving001_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "inving001a" ATTRIBUTE(BORDER)
  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("inving001",wpais,1)
  CALL f.setElementText("labela","Observaciones")
  CALL f.setElementText("labelb","Motivo de la Anulacion")
  CALL f.setElementText("labeld","Numero de Movimiento")

  -- Escondiendo motivo de anulacion 
  CALL f.setFieldHidden("formonly.motanl",1)
  CALL f.setElementHidden("labelb",1)

  CREATE TEMP TABLE tmp_products
   ( cditem     VARCHAR(20),
     dsitem     VARCHAR(50),
     codepq     SMALLINT,
     nomepq     VARCHAR(40),
     canepq     DEC(14,2),
     canuni     DEC(14,2),
     canmtc     DEC(14,2),
     preuni     DEC(14,6),
     totpro     DEC(14,2),
     lnkord     INT      ,
     nuitem     INT      ,
     idprod     SERIAL 
   ) WITH NO LOG 
    
  -- Inicializando datos 
  CALL inving001_inival(1)

  -- Menu de opciones
  MENU " Opciones" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN
       HIDE OPTION "Ingresar"
    END IF
    -- Anular     
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN
       HIDE OPTION "Anular"
    END IF
   COMMAND "Consultar" 
    " Consulta de movimientos existentes."
    CALL invqbx001_movimientos(1)
   COMMAND "Ingresar" 
    " Ingreso de nuevos movimientos."
    CALL inving001_movimientos(1) 
   COMMAND "Anular"
    " Anulacion de movimientos existentes."
    CALL invqbx001_movimientos(2)
   COMMAND "Salir"
    " Abandona el menu de movimientos." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU

  -- Borrando tabla temporal
  DROP TABLE tmp_products 

 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del movimiento (encabezado) 
-- Si es modificar operacion=2
 
FUNCTION inving001_movimientos(operacion)
 DEFINE w_mae_art         RECORD LIKE productos.*,
        w_mae_ord         RECORD LIKE inv_ordentra.*,
        retroceso         SMALLINT,
        operacion         SMALLINT,
        opc               SMALLINT,
        loop,existe,i     SMALLINT,
        fechasmesactual   SMALLINT

 -- Escondiendo motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("labelb",1)

 -- Obteniendo parametro si el ingreso valida fechas solo del mes
 CALL librut003_parametros(7,1)
 RETURNING existe,fechasmesactual

 -- Llenando combobox de tipo de movimiento 
 CALL librut003_cbxtipomovxusuario(FGL_GETENV("LOGNAME")) 

 -- Inicio del loop
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF 
 LET confirma=1
 LET loop  = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving001_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.codbod ,
                w_mae_tra.tipmov ,
                w_mae_tra.fecemi , 
                w_mae_tra.codori , 
                w_mae_tra.coddes , 
                w_mae_tra.numrf1 , 
                w_mae_tra.numrf2 ,
                --w_mae_tra.numord ,
                w_mae_tra.observ WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(codbod) THEN
        LET loop = FALSE
        CALL inving001_inival(1)
        EXIT INPUT
     ELSE
        CALL Dialog.SetFieldActive("codbod",TRUE) 
        CALL Dialog.SetFieldActive("tipmov",TRUE) 
        CALL inving001_inival(1)
        NEXT FIELD codbod
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codbod 
    -- Obteniendo datos de la bodega 
    CALL librut003_bbodega(w_mae_tra.codbod) 
    RETURNING w_mae_bod.*,existe 

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_tra.codemp = w_mae_bod.codemp 
    LET w_mae_tra.codsuc = w_mae_bod.codsuc 

    -- Obteniendo datos de la empresa 
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe 
    -- Obteniendo datos de la sucursal 
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe 
   
    -- Desplegando datos de la empresa y sucursal 
    DISPLAY BY NAME w_mae_emp.nomemp,w_mae_suc.nomsuc

   ON CHANGE tipmov
    -- Obteniendo datos del tipo de movimiento
    CALL librut003_btipomovimiento(w_mae_tra.tipmov)
    RETURNING w_mae_tip.*,existe 
	 IF w_mae_tip.tipope = 2 OR w_mae_tip.tipope =3 THEN
		LET flag_tras = TRUE
	 ELSE
		LET flag_tras = FALSE
	 END IF
    IF w_mae_tip.tipope = 3 AND w_mae_bod.bodcon <> 1 THEN
		 CALL box_valdato("Este movimiento solo es para bodegas de confeccion")
		 INITIALIZE w_mae_tra.tipmov TO NULL
		 NEXT FIELD tipmov
	ELSE
    -- Habilitando y deshabilitando campos dependiendo de la operacion del tipo de
    -- movimiento
    CASE (w_mae_tip.tipope)
     WHEN 1 CALL Dialog.SetFieldActive("codori",TRUE)    -- Cargos
            CALL Dialog.SetFieldActive("coddes",FALSE) 
            LET w_mae_tra.coddes = NULL
            LET nomdes           = NULL
            CLEAR coddes,nomdes
     WHEN 0 
            CALL Dialog.SetFieldActive("coddes",TRUE)    -- Abonos 
            CALL Dialog.SetFieldActive("codori",FALSE) 
            LET w_mae_tra.codori = NULL
            LET nomori           = NULL
            CLEAR codori,nomori 
     WHEN 2
            CALL Dialog.SetFieldActive("coddes",TRUE)    -- Abonos 
            CALL Dialog.SetFieldActive("codori",FALSE) 
            LET w_mae_tra.codori = NULL
            LET nomori           = NULL
            CLEAR codori,nomori 
     WHEN 3
            CALL Dialog.SetFieldActive("coddes",TRUE)    -- Abonos 
            CALL Dialog.SetFieldActive("codori",FALSE) 
            LET w_mae_tra.codori = NULL
            LET nomori           = NULL
            CLEAR codori,nomori 
    END CASE 
	END IF

   ON CHANGE fecemi
    -- Asignando datos al anio y al mes
    LET w_mae_tra.aniotr = YEAR(w_mae_tra.fecemi) 
    LET w_mae_tra.mestra = MONTH(w_mae_tra.fecemi) 
    DISPLAY BY NAME w_mae_tra.aniotr,w_mae_tra.mestra 

   ON ACTION listorigen
    -- Seleccionado lista de origenes
    CALL librut002_formlist("Consulta de Origenes","Origen","Nombre del Origen",
                            "codprv","nomprv","inv_provedrs","1=1",2,1,0)
    RETURNING w_mae_tra.codori,nomori,regreso
    IF regreso THEN
       INITIALIZE w_mae_tra.codori,nomori TO NULL
       CLEAR codori,nomori 
       NEXT FIELD codori 
    ELSE 
       -- Verificando regreso sin seleccion
       DISPLAY BY NAME w_mae_tra.codori,nomori 
       NEXT FIELD numrf1 
    END IF 

   ON ACTION listdestino 
    -- Seleccionado lista de destinos 
	 IF w_mae_tip.tipope = 2 OR w_mae_tip.tipope = 3 THEN
    	CALL librut002_formlist("Consulta de Destinos","Destino","Nombre del Destino",
                            	"codcli","nomcli","fac_clientes","1=1 AND nomcli MATCHES '*BODEGA* '",2,1,0)
    	RETURNING w_mae_tra.coddes,nomdes,regreso
	 ELSE
    	CALL librut002_formlist("Consulta de Destinos","Destino","Nombre del Destino",
                            	"codcli","nomcli","fac_clientes","1=1",2,1,0)
    	RETURNING w_mae_tra.coddes,nomdes,regreso
	 END IF
    IF regreso THEN
       INITIALIZE w_mae_tra.coddes,nomdes TO NULL
       CLEAR codori,nomori 
       NEXT FIELD coddes 
    ELSE 
       -- Verificando regreso sin seleccion
       DISPLAY BY NAME w_mae_tra.coddes,nomdes 
   --    NEXT FIELD numrf1 
    END IF 

   BEFORE FIELD codbod 
    -- Verificando si es regreso se mueve al campo de fecha ya que los campos de bodega y tipo
    -- de movimiento no se pueden modificar si ya se ingresaron otros datos
    IF retroceso THEN
       CALL Dialog.SetFieldActive("codbod",FALSE) 
       CALL Dialog.SetFieldActive("tipmov",FALSE) 
       LET retroceso = FALSE 
    END IF 

    -- Deshabilitando campos 
    CALL Dialog.SetFieldActive("codori",FALSE) 
    CALL Dialog.SetFieldActive("coddes",FALSE) 

   AFTER FIELD codbod
    -- Verificando bodega
    IF w_mae_tra.codbod IS NULL THEN
       ERROR "Error: bodega invalida, VERIFICA ..."
       NEXT FIELD codbod 
    END IF 

   AFTER FIELD tipmov
    -- Verificando tipo de movimiento
    IF w_mae_tra.tipmov IS NULL THEN
       ERROR "Error: tipo de movimiento invalido, VERIFICA ..." 
       NEXT FIELD tipmov
    END IF 

    -- Obteniendo datos del tipo de movimiento
    INITIALIZE w_mae_tip.* TO NULL 
    CALL librut003_btipomovimiento(w_mae_tra.tipmov)
    RETURNING w_mae_tip.*,existe 


   AFTER FIELD codori
    -- Verificando si tipo de movimiento es un cargo
    IF (w_mae_tip.tipope=1) THEN
       IF w_mae_tra.codori IS NULL THEN
          ERROR "Error: debe de ingresarse el origen del movimiento."
          NEXT FIELD codori 
       END IF 
    END IF 

    -- Buscando datos del origen
    INITIALIZE w_mae_prv.* TO NULL
    CALL librut003_bproveedor(w_mae_tra.codori)
    RETURNING w_mae_prv.*,existe 
    IF NOT existe THEN
       INITIALIZE w_mae_tra.codori,nomori TO NULL 
       CALL fgl_winmessage(" Atencion","Origen no existe registrado. \nVERIFICA ...","stop")
       CLEAR codori,nomori
       NEXT FIELD codori 
    END IF 

    -- Desplegando origen
    LET w_mae_tra.codori = w_mae_prv.codprv
    LET nomori           = w_mae_prv.nomprv
    DISPLAY BY NAME w_mae_tra.codori,nomori 


   AFTER FIELD coddes
    -- Verificando si tipo de movimiento es un cargo
    IF (w_mae_tip.tipope=0 OR w_mae_tip.tipope=2 OR w_mae_tip.tipope=3) THEN
       IF w_mae_tra.coddes IS NULL THEN
          ERROR "Error: debe de ingresarse el destino del movimiento."
          NEXT FIELD coddes 
       END IF 
    END IF 

    -- Buscando datos del origen
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.coddes)
    RETURNING w_mae_cli.*,existe 
    IF NOT existe THEN
       INITIALIZE w_mae_tra.coddes,nomdes TO NULL 
       CALL fgl_winmessage(" Atencion","Destino no existe registrado. \nVERIFICA ...","stop")
       CLEAR coddes,nomdes
       NEXT FIELD coddes 
    END IF 
	 IF w_mae_cli.nomcli NOT MATCHES "*BODEGA*" THEN
		CALL fgl_winmessage(" Atencion","Destino debe ser una bodega. \nVERIFICA ...","stop")
		NEXT FIELD coddes
	 END IF
    -- Desplegando destino 
    LET w_mae_tra.coddes = w_mae_cli.codcli 
    LET nomdes           = w_mae_cli.nomcli 
    DISPLAY BY NAME w_mae_tra.coddes,nomdes 
    
   AFTER FIELD fecemi
    -- Verificando fecha de emision 
    IF w_mae_tra.fecemi IS NULL OR 
       (w_mae_tra.fecemi >TODAY) THEN
       ERROR "Error: fecha del movimiento invalida, VERIFICA ..."
       NEXT FIELD fecemi
    END IF

    -- Verificando parametro para aceptacion de fechas solo del mes actual 
    IF fechasmesactual THEN
     -- Verificando que fecha de emision sea solo del mes
     IF EXTEND(w_mae_tra.fecemi,YEAR TO MONTH) != EXTEND(CURRENT,YEAR TO MONTH) THEN
        ERROR "Error: solamente pueden trabajarse movimiento del mes actual, VERIFICA ..."
        NEXT FIELD fecemi
     END IF 
    END IF 

   AFTER FIELD numrf1
    -- Verificando documento de referencia
    IF (LENGTH(w_mae_tra.numrf1)<=0) THEN 
       ERROR "Error: numero de documento de referencia invalido, VERIFICA ..."
       NEXT FIELD numrf1 
    END IF 

{
   AFTER FIELD numord
    -- Verificando o/t
    IF LENGTH(w_mae_tra.numord)>0 THEN
       IF w_mae_tra.numord != "  -            " THEN 
        -- Verificando si orden de trabajo existe 
        INITIALIZE w_mae_ord.* TO NULL 
        SELECT a.*
         INTO  w_mae_ord.*
         FROM  inv_ordentra a
         WHERE (a.lnkord= w_mae_tra.numord)
         IF status=NOTFOUND THEN 
            CALL fgl_winmessage(
            " Atencion: ",
            " Orden de trabajo no existe registrada. \n VERIFICA.",
            "stop")
            NEXT FIELD numord
         END IF 

         -- Verificando fases
         CASE (w_mae_ord.nofase)
          WHEN 1 -- Emitida
           CALL fgl_winmessage(
           " Atencion: ",
           " Orden de trabajo no ha sido enviada. \n No puede ingresarse al inventario.",
           "stop")
           NEXT FIELD numord 
          WHEN 2 -- Inventario
           -- Verificando tipo de operacion del movimiento
           IF (w_mae_tip.tipope=0 OR w_mae_tip.tipope=2) AND w_mae_bod.bodcon = 0 THEN
              CALL fgl_winmessage(
              " Atencion: ",
              " Orden de trabajo no ha sido registrado en el inventario. \n Verifica.",
              "stop")
              NEXT FIELD numord 
			  ELSE
				CALL inving001_llena_ord(w_mae_ord.*)
--				CALL inving001_material(w_mae_ord.*)
           END IF 
          WHEN 3 -- Terminada
           -- Verificando tipo de operacion del movimiento
           IF (w_mae_tip.tipope = 0 OR w_mae_tip.tipope =2) AND w_mae_bod.bodcon = 1 THEN
              CALL fgl_winmessage(
              " Atencion: ",
              " Orden de trabajo ya registrada como terminada. \n Verifica.",
              "stop")
              NEXT FIELD numord 
			  ELSE
					IF w_mae_tip.tipope = 1 THEN
						CALL inving001_llena_ord(w_mae_ord.*)
					END IF
           END IF 
          WHEN 4 -- Inventario
           -- Verificando tipo de operacion del movimiento
           IF w_mae_tip.tipope=1 THEN
              CALL fgl_winmessage(
              " Atencion: ",
              " Orden de trabajo ya registrada en el inventario. \n Verifica.",
              "stop")
              NEXT FIELD numord 
           END IF 
          WHEN 5 -- Despachada
           CALL fgl_winmessage(
           " Atencion: ",
           " Orden de trabajo ya fue despachada. \n Verifica.",
           "stop")
           NEXT FIELD numord 
         END CASE
      END IF 
    END IF 

}

   AFTER INPUT
    -- Verificando nulos
    IF w_mae_tra.codbod IS NULL THEN
      NEXT FIELD codbod
    END IF
    IF w_mae_tra.tipmov IS NULL THEN
      NEXT FIELD tipmov
    END IF
    IF w_mae_tra.fecemi IS NULL THEN
      NEXT FIELD fecemi
    END IF
    
    CASE (w_mae_tip.tipope) 
     WHEN 1 -- Cargo    
      IF w_mae_tra.codori IS NULL THEN
         CALL Dialog.SetFieldActive("codori",TRUE) 
         NEXT FIELD codori
      END IF 
     WHEN 0 OR w_mae_tip.tipope = 2 -- Abono    
      IF w_mae_tra.coddes IS NULL THEN
         CALL Dialog.SetFieldActive("coddes",TRUE) 
         NEXT FIELD coddes
      END IF 
    END CASE  

    IF (LENGTH(w_mae_tra.numrf1)<0) THEN 
       NEXT FIELD numrf1 
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del movimiento 
   LET retroceso = inving001_detingreso(0,operacion)

  -- Verificando operacion
	 IF (w_mae_tip.tipope = 2 OR w_mae_tip.tipope = 3) AND retroceso <> 1 THEN
		 CALL invqbx001_in_bodtra()
	 END IF
  IF (operacion=2) THEN
     EXIT WHILE 
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) AND w_mae_tip.tipope <> 3  THEN 
    CALL inving001_inival(1) 
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos del movimiento (detalle) 

FUNCTION inving001_detingreso(modifica,operacion)
 DEFINE w_mae_art    RECORD LIKE productos.*,
        canexi       LIKE inv_proenbod.exican,
        canval       LIKE inv_proenbod.exival, 
        wcanepq      LIKE inv_epqsxpro.cantid, 
        loop,arr,scr SMALLINT,
        opc,repetido SMALLINT, 
        retroceso    SMALLINT, 
        operacion    SMALLINT, 
        modifica,i   SMALLINT,
        xcditem      STRING

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT BY NAME r_products.cditem,
                r_products.codepq, 
                r_products.canepq,
                r_products.preunt, 
                r_products.canmtc,
                r_products.avanza WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION ACCEPT
    -- Si es modificacion envia el arreglo a su linea
    IF modifica THEN
       LET loop = FALSE
       EXIT INPUT
    ELSE
       INITIALIZE r_products.* TO NULL
       CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
       EXIT INPUT
    END IF

   ON ACTION cancel 
    -- Saliendo del insert de producto
    IF INFIELD(cditem) THEN
       INITIALIZE r_products.* TO NULL
       CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
       LET retroceso = TRUE
       LET loop = FALSE
       EXIT INPUT
    ELSE
       IF NOT modifica THEN 
          INITIALIZE r_products.* TO NULL
          CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
          NEXT FIELD cditem
       ELSE 
          INITIALIZE r_products.* TO NULL
          CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
          LET retroceso = TRUE
          LET loop = FALSE
          EXIT INPUT
       END IF
    END IF 

   ON ACTION listaproducto
    -- Deplegando listado de productos
	 IF w_mae_tip.tipope = 3 THEN
       CALL librut002_formlist("Consulta de Productos de Inventario","Producto","Descricpion",
                               "codabr","dsitem","productos","1=1 AND tipo > 0 ",2,1,1)
       RETURNING r_products.cditem,r_products.dsitem,regreso
       IF regreso THEN
          INITIALIZE r_products.* TO NULL
          CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro 
          NEXT FIELD cditem
       END IF
	 ELSE
       CALL librut002_formlist("Consulta de Productos de Inventario","Producto","Descricpion",
                               "codabr","dsitem","productos","1=1 AND tipo = 0 ",2,1,1)
       RETURNING r_products.cditem,r_products.dsitem,regreso
       IF regreso THEN
          INITIALIZE r_products.* TO NULL
          CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro 
          NEXT FIELD cditem
       END IF
	 END IF

    -- Desplegando prodcuto
    DISPLAY BY NAME r_products.cditem,r_products.dsitem

   ON ACTION productos
    -- Desplegando productos registrados
    CALL inving001_verproductos(0,operacion)

   ON ACTION agregarproducto
    -- Agregando producto
    -- Opcion Ingreso / Modificacion
    IF INFIELD(avanza) THEN 
     -- Calculando unidades totales
     LET wcanepq = librut003_canempaque(w_mae_art.cditem,r_products.codepq)
     LET r_products.canuni = (r_products.canepq*wcanepq)
     DISPLAY BY NAME r_products.canuni 

     CASE (modifica)
      WHEN 0  -- si es ingreso
       -- Enviando linea al arreglo de productos
       CALL inving001_arrprod()
       NEXT FIELD cditem
      WHEN 1 -- si es modificacion
       -- Enviando record a su linea en el arreglo
       LET loop = FALSE
       EXIT INPUT
     END CASE
    ELSE
     CONTINUE INPUT 
    END IF 

   ON CHANGE codepq
    -- Calculando unidades totales
    LET wcanepq = librut003_canempaque(w_mae_art.cditem,r_products.codepq)
    LET r_products.canuni = (r_products.canepq*wcanepq)
    DISPLAY BY NAME r_products.canuni 
    NEXT FIELD canepq 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT
    -- Verificando si movimiento no es valorizado
    IF NOT w_mae_tip.hayval THEN 
       CALL DIALOG.setFieldActive("preunt",FALSE) 
    END IF 
	 -- Verificando que las bodegas sean correctas para los metros cuadrados
    IF w_mae_bod.bodcon = 1 OR w_mae_cli.nomcli MATCHES "*BODEGA*CONFECCION*" THEN
       CALL DIALOG.setFieldActive("canmtc",TRUE) 
	 ELSE
       CALL DIALOG.setFieldActive("canmtc",FALSE) 
    END IF 

    -- Si operacion es modificacion
    IF modifica THEN
       CALL DIALOG.setFieldActive("cditem",FALSE) 
       CALL DIALOG.SetActionActive("productos",FALSE)
    END IF 

	--BEFORE FIELD canmtc
    --IF w_mae_bod.bodcon = 1 OR w_mae_cli.nomcli MATCHES "*BODEGA*CONFECCION*" THEN 
		--CALL inving001_mtc(w_mae_art.codmed,r_products.canuni) RETURNING r_products.canmtc
		--DISPLAY BY NAME r_products.canmtc
		--NEXT FIELD NEXT
	 --ELSE
		--LET r_products.canmtc=0
		--NEXT FIELD NEXT
	 --END IF

   BEFORE FIELD cditem 
    -- Desactivando tecla de accept
    CALL DIALOG.SetActionActive("accept",TRUE)
    CALL DIALOG.SetActionActive("productos",TRUE) 

    -- Inicializando campos 
    IF NOT modifica THEN 
      -- Inicializando datos
      INITIALIZE r_products.* TO NULL
      CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
    END IF 

   AFTER FIELD cditem
    -- Verificando codigo del producto
    IF (LENGTH(r_products.cditem)<=0) THEN 
       ERROR "Error: codigo del producto invalido, VERIFICA ..."
       INITIALIZE r_products.* TO NULL
       CLEAR dsitem
       NEXT FIELD cditem
    END IF

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
	 IF w_mae_tip.tipope = 3 THEN
    	CALL librut003_bordenabr(r_products.dsitem)
    	RETURNING w_mae_art.*,existe
    	IF NOT existe THEN
       	CALL fgl_winmessage(" Atencion","Producto no existe registrado. \nVERIFICA ...","stop")
       	INITIALIZE r_products.* TO NULL
       	CLEAR cditem,dsitem,canepq,canuni,preunt,totpro 
       	NEXT FIELD cditem
    	END IF
	 ELSE
    	CALL librut003_bproductosabr(r_products.cditem)
    	RETURNING w_mae_art.*,existe
    	IF NOT existe THEN
       	CALL fgl_winmessage(" Atencion","Producto no existe registrado. \nVERIFICA ...","stop")
       	INITIALIZE r_products.* TO NULL
       	CLEAR cditem,dsitem,canepq,canuni,preunt,totpro 
       	NEXT FIELD cditem
    	END IF
	 END IF

    -- Desplegando datos del producto
    --LET r_products.dsitem = w_mae_art.dsitem 
    DISPLAY BY NAME r_products.dsitem 

    -- Verificando estado del producto
    IF (w_mae_art.estado=0) THEN
       CALL fgl_winmessage(" Atencion","Producto de BAJA, no puede utilizarse. \nVERIFICA ...","stop")
       INITIALIZE r_products.* TO NULL
       CLEAR cditem,dsitem,canepq,canuni,preunt,totpro 
       NEXT FIELD cditem
    END IF 

    -- Verificando duplicados
    -- Si operacion no es modificacion
    IF NOT modifica THEN
     LET repetido = FALSE
     FOR i = 1 TO totlin  
      IF v_products[i].cditem IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto  
      IF (v_products[i].cditem = r_products.cditem) THEN
         LET repetido = TRUE
         EXIT FOR 
      END IF 
     END FOR
    
     -- Si hay repetido
     IF repetido AND w_mae_tip.tipope <> 3 THEN
        CALL fgl_winmessage(" Atencion","Producto ya fue registrado en el detalle. \nVERIFICA ...","stop")
        NEXT FIELD cditem
     END IF 
    END IF 

   BEFORE FIELD codepq
    -- Cargando combobox de empaques x producto
    CALL librut003_cbxempaques(w_mae_art.cditem,1)

   AFTER FIELD codepq 
    -- Verificando empaque
    IF r_products.codepq IS NULL THEN 
       NEXT FIELD codepq	
    END IF

   BEFORE FIELD canepq 
    -- Desactivando teclas 
    CALL DIALOG.SetActionActive("accept",FALSE) 
    CALL DIALOG.SetActionActive("productos",FALSE) 
	 IF w_mae_tip.tipope=3 THEN
		LET r_products.canepq=inving001_can_le(w_mae_art.cditem,w_mae_art.tipo)
		IF r_products.canepq IS NOT NULL AND r_products.canepq > 0  THEN
			NEXT FIELD avanza
		END IF
	 END IF

   AFTER FIELD canepq
    -- Verificando cantidad
    IF (r_products.canepq IS NULL) OR
       (r_products.canepq<=0) THEN
       ERROR "Error: unidades invalidas, VERIFICA ..."
       LET r_products.canepq = NULL
       CLEAR canepq
       NEXT FIELD canepq
    END IF

    -- Calculando unidades totales
    LET wcanepq = librut003_canempaque(w_mae_art.cditem,r_products.codepq)
    LET r_products.canuni = (r_products.canepq*wcanepq)
    DISPLAY BY NAME r_products.canuni 

    -- Chequeando existencia 
    -- Verificando si tipo de operacion del movimiento es de salida
    IF (w_mae_tip.tipope=0 OR w_mae_tip.tipope=2) THEN 
     -- Verificando si bodega tiene chequeo de existencia
     IF w_mae_bod.chkexi THEN
      -- Chequeando existencia
      IF (operacion=1) THEN -- Ingreso
       LET canexi = librut003_chkexistencia(w_mae_tra.codemp,w_mae_tra.codsuc,w_mae_tra.codbod,w_mae_art.cditem)
       IF (r_products.canuni>canexi) THEN 
          LET msg = " Existencia del producto insuficiente, VERIFICA \n", 
                    " Existencia Actual ( ",canexi USING "--,---,--&.&&"," )" 

          CALL fgl_winmessage(" Atencion",msg,"stop")
          NEXT FIELD canepq
       END IF 
      END IF 
     END IF 
    END IF 

    -- Verificando si tipo de movimiento no es valorizado
    IF NOT w_mae_tip.hayval THEN 
       LET r_products.preunt = 0 
       DISPLAY BY NAME r_products.preunt 
    END IF 

    -- Totalizando productos 
    CALL inving001_totdet()

   AFTER FIELD preunt 
    -- Verificando precio 
    IF (r_products.preunt IS NULL) OR
       (r_products.preunt<0) THEN
       ERROR "Error: precio invalido, VERIFICA ..."
       LET r_products.preunt = NULL
       CLEAR preunt
       NEXT FIELD preunt
    END IF

    -- Totalizando productos 
    CALL inving001_totdet()

   AFTER INPUT 
    -- Totalizando productos 
    CALL inving001_totdet()

    -- Verificando campos 
    IF r_products.canepq IS NULL THEN
       NEXT FIELD canepq
    END IF 
    IF r_products.codepq IS NULL THEN
       NEXT FIELD codepq
    END IF 

    -- Verificando si tipo de movimiento es valorizado
    IF w_mae_tip.hayval THEN 
     IF r_products.preunt IS NULL THEN
        NEXT FIELD preunt
     END IF 
    END IF 

    IF r_products.avanza IS NULL THEN
       NEXT FIELD avanza
    END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando que se ingrese al menos un producto
  IF (totuni<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     "Debe ingresarse al menos un producto en el detalle del movimiento.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Verificando existencia de productos por cualquier cambio antes de grabar
  -- Verificando si tipo de operacion del movimiento es de salida
  IF (w_mae_tip.tipope=0 OR w_mae_tip.tipope=2) THEN
     -- Verificando si bodega tiene chequeo de existencia
     IF w_mae_bod.chkexi THEN
        -- Chequeando existencia
        IF (operacion=1) THEN -- Ingreso
         CALL inving001_existencia() 
         RETURNING xcditem,canexi 
         IF (LENGTH(xcditem)>0) THEN 
           LET msg = " Producto "||xcditem CLIPPED||" sin existencia suficiente, VERIFICA. \n",
                     " Existencia Actual ( ",canexi USING "<<,<<<,<<&"," )"

           CALL fgl_winmessage(" Atencion",msg,"stop")
           CONTINUE WHILE
         END IF 
        END IF 
     END IF 
  END IF 

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando movimiento
    CALL inving001_grabar(operacion)

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina que despliega los productos que se van ingresando 

FUNCTION inving001_arrprod()
 DEFINE v_dspprod  DYNAMIC ARRAY OF RECORD
        cditem     LIKE inv_products.codabr,
        dsitem     CHAR(50),
        codepq     SMALLINT,
        nomepq     VARCHAR(40,1),
        canepq     LIKE inv_dtransac.canepq,
        canuni     LIKE inv_dtransac.canuni,
        canmtc     LIKE inv_dtransac.canmtc,
        preuni     LIKE inv_dtransac.preuni,
        totpro     LIKE inv_dtransac.totpro,
        lnkord     LIKE inv_dtransac.lnkord,
        nuitem     LIKE inv_dtransac.nuitem
       END RECORD   ,
       i            ,
       totprd    INT,
		 w_mae_art RECORD LIKE productos.*

		CALL librut003_bordenabr(r_products.dsitem)
		RETURNING w_mae_art.*,existe
 -- Inicializando datos 
 IF r_products.preunt IS NULL THEN LET r_products.preunt = 0 END IF
 IF r_products.totpro IS NULL THEN LET r_products.totpro = 0 END IF
 LET totlin = (totlin+1)
IF r_products.canmtc IS NULL THEN LET r_products.canmtc=0 END IF
 -- Asignando datos
 LET v_products[totlin].cditem = r_products.cditem
 LET v_products[totlin].dsitem = r_products.dsitem
 LET v_products[totlin].codepq = r_products.codepq
 LET v_products[totlin].nomepq = inving001_nomempaque(r_products.cditem,r_products.codepq)
 LET v_products[totlin].canepq = r_products.canepq
 LET v_products[totlin].canuni = r_products.canuni 
 LET v_products[totlin].canmtc = r_products.canmtc 
 LET v_products[totlin].preuni = r_products.preunt 
 LET v_products[totlin].totpro = r_products.totpro
 IF w_mae_tip.tipope = 3 THEN 
	LET v_products[totlin].lnkord = w_mae_art.cditem
   LET v_products[totlin].nuitem = w_mae_art.tipo
 END IF


 -- Insertando productos en tabla temporal
 INSERT INTO tmp_products
 VALUES (v_products[totlin].*,0)

 -- Seleccionando productos antes de desplegar
 CALL v_dspprod.clear()

 DECLARE c1 CURSOR FOR
 SELECT a.*
  FROM  tmp_products a
  ORDER BY a.idprod DESC 
  LET totprd = 1 
  FOREACH c1 INTO v_dspprod[totprd].* 
   LET totprd = (totprd+1)
  END FOREACH 
  CLOSE c1
  FREE  c1
  LET totprd = (totprd-1)

 -- Desplegando productos
 DISPLAY ARRAY v_dspprod TO l_products.* ATTRIBUTE(BLUE)
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY

 -- Totalizando productos 
 CALL inving001_totdet()
END FUNCTION

		  -- Subrutina para totalizar el detalle de productos 

FUNCTION inving001_totdet()
 DEFINE i INT

 -- Totalizando ingreso
 LET r_products.totpro = (r_products.canuni*r_products.preunt)
 DISPLAY BY NAME r_products.totpro 

 -- Totalilzando detalle
 LET totuni = 0
 LET totval = 0
 FOR i=1 TO totlin
  LET v_products[i].totpro = (v_products[i].canuni*v_products[i].preuni) 
  LET totuni               = (totuni+v_products[i].canuni)
  LET totval               = (totval+v_products[i].totpro) 
 END FOR

 -- Desplegando totales 
 DISPLAY BY NAME totuni,totval,totlin ATTRIBUTE(YELLOW,REVERSE)
END FUNCTION 

-- Subrutina desplegar los productos ya ingresados 

FUNCTION inving001_verproductos(consulta,operacion)
 DEFINE w_row,w_scr,consulta,loop  SMALLINT,
        operacion                  SMALLINT 

 -- Inicializando datos
 IF NOT consulta THEN 
    INITIALIZE r_products.* TO NULL
    CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
 END IF 

 -- Verificando si hay productos
 IF (totlin>0) THEN
   LET loop = TRUE 
   WHILE loop
    -- Desplegando productos
    DISPLAY ARRAY v_products TO l_products.* 
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE,BLUE)

     ON ACTION accept 
      LET loop = FALSE 
      EXIT DISPLAY 

     ON ACTION cancel 
      LET loop = FALSE 
      EXIT DISPLAY 

     ON ACTION calculator
      -- Cargando calculadora
      IF NOT winshellexec("calc") THEN
         ERROR "Atencion: calculadora no disponible."
      END IF

     ON ACTION eliminar 
      -- Verificando si hay lineas que borrar
      IF (totlin>0) THEN
       -- Obteniendo la linea del vector
       LET w_row = ARR_CURR()

       -- Quitando la linea
       LET totlin = inving001_delrows(w_row)

       -- Totalizando productos
       CALL inving001_totdet()
      ELSE
       ERROR "Atencion: no hay productos que eliminar, VERIFICA ..." 
      END IF

      EXIT DISPLAY 

     ON ACTION modificar
      -- Verificando si hay lineas que borrar
      IF (totlin>0) THEN
       -- Modificando
       -- Obteniendo la linea del vector
       LET w_row = ARR_CURR()
       LET w_scr = SCR_LINE()

       -- Llenando record de ingresos
       LET r_products.cditem = v_products[w_row].cditem
       LET r_products.dsitem = v_products[w_row].dsitem
       LET r_products.codepq = v_products[w_row].codepq 
       LET r_products.canepq = v_products[w_row].canepq 
       LET r_products.canuni = v_products[w_row].canuni 
       LET r_products.preunt = v_products[w_row].preuni
       LET r_products.totpro = v_products[w_row].totpro
       DISPLAY BY NAME r_products.* 

       -- Modificando el producto
       IF NOT inving001_detingreso(1,operacion) THEN
          -- Recibiendo nuevos valores
          LET v_products[w_row].cditem = r_products.cditem
          LET v_products[w_row].dsitem = r_products.dsitem
          LET v_products[w_row].codepq = r_products.codepq 
          LET v_products[w_row].canepq = r_products.canepq 
          LET v_products[w_row].nomepq = inving001_nomempaque(v_products[w_row].cditem,v_products[w_row].codepq)
          LET v_products[w_row].canuni = r_products.canuni
          LET v_products[w_row].canmtc = r_products.canmtc
          LET v_products[w_row].preuni = r_products.preunt
          LET v_products[w_row].totpro = r_products.totpro

          -- Desplegando datos
          DISPLAY v_products[w_row].* TO l_products[w_scr].*
 
          -- Inicializando datos
          INITIALIZE r_products.* TO NULL
          CLEAR cditem,dsitem,codepq,canepq,canuni,preunt,totpro
       END IF

       -- Totalizando productos
       CALL inving001_totdet()
      ELSE 
       ERROR "Atencion: no hay productos que modificar, VERIFICA ..." 
      END IF 

     BEFORE DISPLAY 
      -- Verificando si es consulta
      IF consulta THEN
         CALL DIALOG.SetActionActive("modificar",FALSE)
         CALL DIALOG.SetActionActive("eliminar",FALSE)
         CALL DIALOG.SetActionActive("accept",FALSE)
      ELSE
         CALL DIALOG.SetActionActive("modificar",TRUE)
         CALL DIALOG.SetActionActive("eliminar",TRUE)
         CALL DIALOG.SetActionActive("cancel",TRUE)
         CALL DIALOG.SetActionActive("accept",FALSE)
      END IF

     BEFORE ROW
      -- Verificando control del total de lineas
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 
    END DISPLAY 
   END WHILE 
 ELSE
    CALL fgl_winmessage(
    " Atencion",
    " Movimiento sin detalle de productos.",
    "stop")
 END IF
END FUNCTION

-- Subrutina para eliminar linea 

FUNCTION inving001_delrows(xrow)
 DEFINE v_nuevo   DYNAMIC ARRAY OF RECORD
         cditem   LIKE inv_products.codabr,
         dsitem   CHAR(80),
         codepq   SMALLINT,
         nomepq   VARCHAR(40), 
         canepq   LIKE inv_dtransac.canepq,
         canuni   LIKE inv_dtransac.canuni,
         canmtc   LIKE inv_dtransac.canmtc,
         preuni   LIKE inv_dtransac.preuni,
         totpro   LIKE inv_dtransac.totpro,
         lnkord   LIKE inv_dtransac.lnkord,
         nuitem   LIKE inv_dtransac.nuitem
        END RECORD,
        xrow,n,i  SMALLINT

 -- Inicializando vector
 CALL v_nuevo.clear()

 -- Eliminando linea seleccionada y cargando lineas restantes
 LET n = 0
 FOR i = 1 TO totlin 
  -- Quitando filas a eliminar
  IF (v_products[i].cditem = v_products[xrow].cditem) AND
     (i=xrow) THEN
     CONTINUE FOR
  END IF 

  -- Guardando filas
  LET n = (n+1)
  LET v_nuevo[n].* = v_products[i].*
 END FOR 

 -- Inicializando vector
 CALL inving001_inivec() 

 -- Cargando lineas restantes
 FOR i = 1 TO n
  IF v_nuevo[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Cargando filas 
  LET v_products[i].* = v_nuevo[i].*
 END FOR 

 RETURN n 
END FUNCTION 

-- Subrutina para verificar la existencia de los productos antes de grabar

FUNCTION inving001_existencia()
 DEFINE w_mae_art  RECORD LIKE productos.*,
        xcanexi    LIKE inv_proenbod.exican,
        xcditem    STRING, 
        i          SMALLINT

-- Iniciando la transaccion
-- Chequeando existencia
 LET xcditem = NULL
 LET xcanexi = 0 
 FOR i = 1 TO totlin
  IF v_products[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Obteniendo datos del producto
  INITIALIZE w_mae_art.* TO NULL
  display "paso 1 ",v_products[i].cditem
  CALL librut003_bproductosabr(v_products[i].cditem)
  RETURNING w_mae_art.*,existe

  -- Chequeando existencia
  LET xcanexi = librut003_chkexistencia(w_mae_tra.codemp,w_mae_tra.codsuc,w_mae_tra.codbod,w_mae_art.cditem)
  IF (v_products[i].canuni>xcanexi) THEN
     LET xcditem = v_products[i].cditem 
     EXIT FOR   
  END IF
 END FOR 

 RETURN xcditem,xcanexi 
END FUNCTION 

-- Subrutina para grabar el movimiento 

FUNCTION inving001_grabar(operacion)
 DEFINE w_mae_art  RECORD LIKE productos.*, 
        w_mae_his  RECORD LIKE inv_mtransac.*, 
        w_det_his  RECORD LIKE inv_dtransac.*, 
        wtipmov    LIKE inv_mtransac.tipmov,
        wopeuni    LIKE inv_dtransac.opeuni, 
        wopeval    LIKE inv_dtransac.opeval, 
        wcanmtc    LIKE inv_dtransac.canmtc, 
        wnofase    LIKE inv_ordentra.nofase, 
        operacion  SMALLINT,
        i,correl   SMALLINT,
        wlnkhis    INTEGER ,
		  lSubcat    VARCHAR(30),
		  lCditem    INTEGER,
        flag_ope    SMALLINT

 LET flag_ope = FALSE
 WHENEVER ERROR CONTINUE 
 -- Grabando transaccion
 ERROR " Registrando Movimiento ..." ATTRIBUTE(CYAN)
 -- Iniciando la transaccion
IF (confirma = 1 AND flag_tras = 1) OR operacion <> 1 THEN
   BEGIN WORK
END IF
   -- 1. Grabando datos generales del movimiento (encabezado) 
   -- Asignando datos
   IF (operacion=1) THEN -- ingreso
      LET w_mae_tra.lnktra = 0
      LET w_mae_tra.tipope = w_mae_tip.tipope 
      LET w_mae_tra.haytra = 0
   ELSE
      -- Guardando movimiento sin cambios
      -- Obteniendo datos del movimiento
      CALL librut003_bmovimiento(w_mae_tra.lnktra)
      RETURNING w_mae_his.*,existe
		LET w_mae_tra.haytra=0
      
      -- Grabando datos generales
      SET LOCK MODE TO WAIT 
      INSERT INTO his_mtransac
      VALUES ("0",w_mae_his.lnktra, w_mae_his.codsuc, w_mae_his.codbod, w_mae_his.codbod, w_mae_his.tipmov, w_mae_his.codori, w_mae_his.coddes,
                  w_mae_his.numrf1, w_mae_his.numrf2, w_mae_his.aniotr, w_mae_his.mestra, w_mae_his.fecemi, w_mae_his.tipope, w_mae_his.observ,
                  w_mae_his.estado, w_mae_his.motanl, w_mae_his.fecanl, w_mae_his.horanl, w_mae_his.usranl, w_mae_his.impres, w_mae_his.userid,
                  w_mae_his.fecsis, w_mae_his.horsis, USER            , CURRENT, CURRENT HOUR TO SECOND)
      LET wlnkhis = SQLCA.SQLERRD[2]
      IF SQLCA.SQLCODE <> 0 THEN
         LET flag_ope = TRUE
      END IF
    
      -- Guardando detalle 
      IF flag_ope = FALSE THEN 
      DECLARE c_dethis CURSOR FOR
      SELECT d.*
       FROM  inv_dtransac d
       WHERE (d.lnktra = w_mae_his.lnktra)
       ORDER BY d.correl 
       FOREACH c_dethis INTO w_det_his.*
        -- Grabando detalle 
        SET LOCK MODE TO WAIT 
        INSERT INTO his_dtransac
        VALUES (wlnkhis,w_det_his.*)
        IF SQLCA.SQLCODE <> 0 THEN
           LET flag_ope = TRUE
           EXIT FOREACH
        END IF
       END FOREACH
       CLOSE c_dethis
       FREE  c_dethis 
      END IF

      --Eliminando movimiento anterior "A BODEGA TRASLADO" si es modificación
      IF g_accion = 'Modificar' THEN 
         IF g_flagdel = 0 THEN 
            DELETE FROM inv_mtransac WHERE numrf1 = "TEMPORAL: "||w_mae_tra.lnktra
            LET g_flagdel = 1 
         END IF 
      END IF 
      -- Eliminando documento antes dde grabar
      IF flag_ope = FALSE THEN
         SET LOCK MODE TO WAIT
         DELETE FROM inv_mtransac
         WHERE inv_mtransac.lnktra = w_mae_tra.lnktra
         IF SQLCA.SQLCODE<> 0 THEN
				DELETE FROM inv_mtransac
				WHERE inv_mtransac.numrf1 IN ("TRASLADO: "||lnktra,"TEMPORAL: "||lnktra)
				AND   inv_mtransac.numrf2 = inv_mtransac.numrf1
				IF SQLCA.SQLCODE <> 0 THEN
            	LET flag_ope = TRUE
				END IF
         END IF
      END IF
   END IF 
   
   -- Verificando origen
   IF w_mae_tra.codori IS NULL THEN
      LET w_mae_tra.codori = 0
   END IF 

   -- Verificando destino
   IF w_mae_tra.coddes IS NULL THEN
      LET w_mae_tra.coddes = 0
   END IF 

   IF flag_ope = FALSE THEN
      SET LOCK MODE TO WAIT
      INSERT INTO inv_mtransac
      VALUES (w_mae_tra.*)
      IF SQLCA.SQLCODE <> 0 THEN
         LET flag_ope = TRUE
      ELSE
         LET w_mae_tra.lnktra = SQLCA.SQLERRD[2]
      END IF
   END IF
   -- 2. Grabando detalle de productos del movimiento (detalle)
   LET correl = 0 
  IF flag_ope = FALSE THEN
   FOR i = 1 TO totlin 
    IF v_products[i].cditem IS NULL OR 
       v_products[i].canuni IS NULL THEN 
       CONTINUE FOR 
    END IF 
    LET correl = (correl+1) 
	 IF v_products[i].canmtc IS NULL THEN LET v_products[i].canmtc= 0 END IF

    -- Verificando el tipo de operacion del movimiento 
    INITIALIZE wopeuni,wopeval TO NULL
    CASE (w_mae_tra.tipope)
      WHEN 3
        LET wopeuni = (v_products[i].canuni*(0))
        LET wopeval = (v_products[i].totpro*(0))
        LET wcanmtc = (v_products[i].canmtc*(0))
      WHEN 2
        LET wopeuni = (v_products[i].canuni*(-1))
        LET wopeval = (v_products[i].totpro*(-1))
        LET wcanmtc = (v_products[i].canmtc*(-1))
      WHEN 0
        LET wopeuni = (v_products[i].canuni*(-1))
        LET wopeval = (v_products[i].totpro*(-1))
        LET wcanmtc = (v_products[i].canmtc*(-1))
      WHEN 1
        LET wopeuni = v_products[i].canuni
        LET wopeval = v_products[i].totpro
        LET wcanmtc = v_products[i].canmtc
    END CASE

	 -- Obteniendo detalles de la bodega
		SELECT *
		  INTO w_mae_bod.*
		  FROM inv_mbodegas
		 WHERE codbod = w_mae_tra.codbod
      IF SQLCA.SQLCODE <> 0 THEN
         LET flag_ope = TRUE
      END IF
    -- Obteniendo datos del productos
    INITIALIZE w_mae_art.* TO NULL
    IF w_mae_tip.tipope <> 3 THEN
    	CALL librut003_bproductosabr(v_products[i].cditem)
    	RETURNING w_mae_art.*,existe
	 ELSE
		CALL librut003_bordenabr(r_products.dsitem)
      RETURNING w_mae_art.*,existe
      
		SELECT "*"||inv_subcateg.nomsub||"*MEDIDA*"
		  INTO lSubcat
		  FROM inv_dorden a , inv_subcateg
		 WHERE a.lnkord = v_products[i].lnkord
         AND a.nuitem = v_products[i].nuitem
			AND a.subcat = inv_subcateg.subcat
      IF SQLCA.SQLCODE <> 0 THEN
         LET flag_ope = TRUE
         EXIT FOR
      END IF
      
      SELECT cditem
		  INTO w_mae_art.cditem
		  FROM inv_products
		 WHERE dsitem MATCHES lSubcat
      IF SQLCA.SQLCODE <> 0 THEN
         LET flag_ope = TRUE
         EXIT FOR
      END IF
      
	 END IF

    -- Grabando
    SET LOCK MODE TO WAIT
    INSERT INTO inv_dtransac 
    VALUES (w_mae_tra.lnktra     , -- id del movimiento 
            w_mae_tra.codemp     , -- empresa 
            w_mae_tra.codsuc     , -- sucursal 
            w_mae_tra.codbod     , -- bodega   
            w_mae_tra.tipmov     , -- tipo de movimiento
            w_mae_tra.codori     , -- origen del movimiento 
            w_mae_tra.coddes     , -- destino del movimiento 
            w_mae_tra.fecemi     , -- fecha del movimiento 
            w_mae_tra.aniotr     , -- anio del movimiento 
            w_mae_tra.mestra     , -- mes del movimiento
            w_mae_art.cditem     , -- codigo del producto 
            v_products[i].cditem , -- codigo del producto abreviado
            v_products[i].lnkord , -- codigo del producto abreviado
            v_products[i].nuitem , -- codigo del producto abreviado
            v_products[i].codepq , -- codigo empaque
            v_products[i].canepq , -- cantidad ingresada
            v_products[i].canuni , -- cantidad empaque * cantidad de unidades contenidad en el empaque 
            wcanmtc              , -- cantidad empaque por metro cuadrado
            v_products[i].preuni , -- precio unitario            
            v_products[i].totpro , -- precio total               
            0                    , -- precio promedio 
            correl               , -- numero correlativo de producto 
            "XXXXXXXXXX"         , -- barcod, numero de codigo de barras
            w_mae_tra.tipope     , -- tipo de operacion del movimiento 
            w_mae_tra.estado     , -- estado del movimiento 
            wopeval              , -- valor dependiendo del tipo de la operacion es + o -
            wopeuni              , -- cantidad en unidades dependiendo del tipo de la operacion es + o -
            w_mae_tra.userid     , -- usuario que registro el movimiento 
            w_mae_tra.fecsis     , -- fecha en que se registro el movimiento 
            w_mae_tra.horsis)      -- hora en la que registro el movimiento 
			IF SQLCA.SQLCODE <> 0 THEN
				LET flag_ope = TRUE
				EXIT FOR
			ELSE
				IF w_mae_tip.tipope= 3 AND v_products[i].lnkord IS NOT NULL AND v_products[i].lnkord > 0 THEN
					UPDATE inv_dorden SET nofase = 4
					 WHERE lnkord = v_products[i].lnkord
						AND nuitem = v_products[i].nuitem
					IF SQLCA.SQLCODE  <> 0 THEN
						LET flag_ope = TRUE 
						EXIT FOR
					END IF
				ELSE
					IF w_mae_tip.tipope = 1                      AND v_products[i].lnkord IS NOT NULL AND
						w_mae_bod.nombod NOT MATCHES "*TRASLADO*" AND v_products[i].lnkord > 0 THEN
						UPDATE inv_dorden SET nofase = 5
					 	 WHERE lnkord = v_products[i].lnkord
						   AND nuitem = v_products[i].nuitem
						IF SQLCA.SQLCODE <> 0 THEN
							LET flag_ope = TRUE
							EXIT FOR
						END IF
					END IF
				END IF
			END IF
   END FOR 
  END IF
   ---- Actualizando orden de trabajo
   --IF w_mae_tra.numord IS NOT NULL THEN
      ---- Verificadno tipo de operacion del movimiento
      --CASE (w_mae_tra.tipope)  
       --WHEN (0 OR w_mae_tra.tipope = 2)AND w_mae_bod.bodcon = 1 -- TERMINADA
			   --LET wnofase = 3
				--EXIT CASE
		 --WHEN 1 AND w_mae_bod.bodcon = 0 -- INVENTARIO
				--LET wnofase =4
				--EXIT CASE
       --WHEN 0 AND w_mae_bod.bodcon = 0 -- DESPACHADA
				--LET wnofase = 5 
				--EXIT CASE
      --END CASE

      -- Actualizando   
      --UPDATE inv_ordentra 
      --SET    inv_ordentra.nofase = wnofase
      --WHERE  inv_ordentra.lnkord = w_mae_tra.numord 
   --END IF 

   -- Desplegando numero de movimiento 
   CASE (operacion)
    WHEN 1 LET msg = " Movimiento registrado presione [ENTER] para continuar" 
    WHEN 2 LET msg = " Movimiento actualizado presione [ENTER] para continuar" 
   END CASE

	IF flag_ope = TRUE THEN
		ROLLBACK WORK
		CALL fgl_winmessage(" Atencion","No se ha Realizado la Operacion","information")
	ELSE
 -- Finalizando la transaccion
      IF ((confirma = 1 AND flag_tras = 0) OR
         (confirma = 2 AND flag_tras = 1)) OR operacion = 2  THEN
            COMMIT WORK
            DISPLAY BY NAME w_mae_tra.lnktra 
            CALL fgl_winmessage(" Atencion",msg,"information")
            --ERROR "Imprimiendo movimiento ... por favor espere ...."
            LET reimpresion = FALSE
            CALL invrpt001_reportemov()
            ERROR "" 
       END IF
	END IF
  WHENEVER ERROR STOP
 -- Imprimiendo movimiento
END FUNCTION 

-- Subrutina para obtener datos del empaque          

FUNCTION inving001_nomempaque(wcodabr,wcodepq)
 DEFINE wcodepq  LIKE inv_epqsxpro.codepq,
        wcodabr  LIKE inv_products.codabr,
        wnomepq  LIKE inv_epqsxpro.nomepq

 -- Obteniendo cantidad de empaque
 INITIALIZE wnomepq TO NULL
 SELECT inv_epqsxpro.nomepq
  INTO  wnomepq
  FROM  inv_products,inv_epqsxpro 
  WHERE inv_products.cditem = inv_epqsxpro.cditem 
    AND inv_products.codabr = wcodabr 
    AND inv_epqsxpro.codepq = wcodepq

 RETURN wnomepq
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving001_inival(i)
 DEFINE i      SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*  TO NULL 
   INITIALIZE r_products.* TO NULL
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.impres = "N"
 LET w_mae_tra.estado = "V" 
 LET w_mae_tra.userid = FGL_GETENV("LOGNAME")
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET totlin           = 0 
 LET totuni           = 0
 LET totval           = 0

 -- Lllenando combo de bodegas
 CALL librut003_cbxbodegasxusuario(w_mae_tra.userid) 

 -- Inicializando vectores de datos
 CALL inving001_inivec() 

 -- Limpiando tabla temporal
 DELETE FROM tmp_products

 -- Obtiendo nombre del estado
 LET nomest = librut001_valores(w_mae_tra.estado) 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,w_mae_tra.estado,nomest 
 DISPLAY BY NAME totuni,totval,totlin ATTRIBUTE(REVERSE,YELLOW) 
 DISPLAY BY NAME w_mae_tra.lnktra 
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION inving001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR l_products[i].*
 END FOR 
END FUNCTION 

FUNCTION inving001_mtc(codmed,cant)
DEFINE codmed    INTEGER,
		 xlargo    DECIMAL(4,2),
		 xancho    DECIMAL(4,2),
		 cant      LIKE inv_dtransac.canuni,
		 total     LIKE inv_dtransac.canuni,
		 w_med_mae RECORD LIKE inv_medidpro.*,
		 factor    DECIMAL(4,2)

	SELECT inv_medidpro.*
	INTO w_med_mae.*
	FROM inv_medidpro
	WHERE inv_medidpro.codmed = codmed

	SELECT NVL(inv_unimedid.factor,1)
	INTO   factor
	FROM   inv_unimedid
	WHERE  unimed =   6

	LET total= cant * w_med_mae.yancho
	LET total = total*factor

RETURN total
END FUNCTION

FUNCTION inving001_llena_ord(w_ord)
DEFINE w_ord RECORD like inv_ordentra.*,
		 w_pro RECORD LIKE inv_products.*,
		 w_epq RECORD LIKE inv_epqsxpro.*,
		 w_asi RECORD LIKE inv_dasignac.*,
		 i     SMALLINT,
		 fac   DECIMAL(4,2),
		 nom   VARCHAR(50,1),
		 subca LIKE inv_subcateg.nomsub,
		 codco LIKE inv_colorpro.codcol,
		 nomco LIKE inv_colorpro.nomcol

	LET codco = w_ord.codcol
	SELECT nomsub
	INTO   subca
	FROM   inv_subcateg
	WHERE  subcat = w_ord.subcat

	SELECT nomcol
	  INTO nomco
	  FROM inv_colorpro
	 WHERE codcol = codco

	IF w_ord.nofase = 2 THEN
		LET nom = "*",subca CLIPPED,"*",nomco clipped,"*"
	ELSE
		LET nom = "*",subca CLIPPED,"*MEDIDA*"
	END IF

	SELECT *
	  INTO w_pro.*
	  FROM inv_products
	 WHERE dsitem MATCHES nom

	SELECT *
	  INTO w_epq.*
	  FROM inv_epqsxpro
	 WHERE cditem = w_pro.cditem

	IF  w_ord.nofase = 3 AND w_mae_tip.tipope = 1 THEN
		LET v_products[1].cditem=w_pro.codabr
		LET v_products[1].dsitem=w_pro.dsitem
		LET v_products[1].canepq=w_ord.cantid
		LET v_products[1].codepq=w_epq.codepq
		LET v_products[1].nomepq=w_epq.nomepq --"UNIDAD"
		LET v_products[1].canuni=w_ord.cantid
		LET v_products[1].preuni=w_ord.precio
		LET v_products[1].totpro=w_ord.totord
		LET v_products[1].canmtc=0
	ELSE
		LET v_products[1].cditem=w_pro.codabr
		LET v_products[1].dsitem=w_pro.dsitem
		LET v_products[1].canepq=0#w_ord.cantid
		LET v_products[1].codepq=w_epq.codepq
		LET v_products[1].nomepq=w_epq.nomepq  --"MTS2"
		LET v_products[1].canuni=0#w_ord.cantid
		IF  w_ord.medida = 1 THEN
		-- Factor de pies a yardas
				SELECT NVL(inv_unimedid.factor,1)
  	 			INTO   fac
		  		FROM   inv_unimedid
	  	 		WHERE  unimed =   1

				LET w_ord.xlargo=w_ord.xlargo*fac
				LET w_ord.yancho=w_ord.yancho*fac
	
				-- Factor de yardas a metros2
  				SELECT NVL(inv_unimedid.factor,1)
				INTO   fac
  				FROM   inv_unimedid
				WHERE  unimed =   6

				LET v_products[1].canmtc=((w_ord.xlargo*w_ord.yancho)*fac)*w_ord.cantid
		ELSE
				LET v_products[1].canmtc=(w_ord.xlargo*w_ord.yancho)*w_ord.cantid
		END IF
		LET v_products[1].preuni=0
		LET v_products[1].totpro=0
	END IF

	IF w_ord.nofase = 2 THEN
		SELECT *
		  FROM inv_masignac
		 WHERE lnkord = w_mae_tra.numord
		IF STATUS = 0 THEN
			DECLARE asignados CURSOR FOR
				SELECT inv_dasignac.*
				  FROM inv_masignac , inv_dasignac
				 WHERE inv_masignac.lnkasi = inv_dasignac.lnkasi
					AND inv_masignac.lnkord = w_mae_tra.numord

			LET i =  1
			FOREACH asignados INTO w_asi.* 
					SELECT *
	  				  INTO w_pro.*
	  				  FROM inv_products
	 				 WHERE cditem = w_asi.cditem
					SELECT *
	  				  INTO w_epq.*
	  				  FROM inv_epqsxpro
	 				 WHERE cditem = w_asi.cditem

				LET v_products[i].cditem=w_asi.codabr
				LET v_products[i].dsitem=w_pro.dsitem
				LET v_products[i].canepq=w_asi.cantid
				LET v_products[i].codepq=w_epq.codepq
				LET v_products[i].nomepq=w_epq.nomepq
				LET v_products[i].canuni=w_ord.cantid
				LET v_products[i].preuni=0
				LET v_products[i].totpro=w_ord.cantid
				LET v_products[i].canmtc=0

				LET i = i + 1
			END FOREACH

			FOR i = 1 TO v_products.getLength()
				IF v_products[i].cditem IS NULL THEN
					CALL v_products.deleteElement(i)
				END IF
			END FOR
				
		END IF	
	END IF
	DISPLAY ARRAY v_products TO l_products.*

		BEFORE ROW 
			EXIT DISPLAY

	END DISPLAY

END FUNCTION

FUNCTION inving001_material(orden)
DEFINE   nombre VARCHAR(30),
			orden   RECORD LIKE inv_ordentra.*,
			subca  LIKE inv_subcateg.nomsub,
			nomco  LIKE inv_colorpro.nomcol,
			codco  LIKE inv_colorpro.codcol

END FUNCTION

FUNCTION inving001_can_le(lord,litm)
DEFINE cant   DECIMAL(12,4),
		 pos1                ,
		 pos2                ,
		 pos3                ,
		 lord                ,
       litm                ,
		 o      INT          ,
       producto VARCHAR(100)

			SELECT cantid
			  INTO cant
			  FROM inv_dorden
			 WHERE lnkord = lord
				AND nuitem = litm
			IF STATUS <> NOTFOUND THEN
				RETURN cant
			ELSE
				RETURN 0
			END IF


END FUNCTION

FUNCTION inving001_ord_item(desc)
DEFINE desc   VARCHAR(100) ,
       pos1                ,
       pos2                ,
       pos3                ,
       orden               ,
       item                 ,
       o      INT

   FOR o = LENGTH(desc) TO 1 STEP -1

      IF desc[o,o]="A" THEN
         LET pos1 = o +1
         EXIT FOR
      END IF

   END FOR

   LET item = desc[pos1,LENGTH(desc)]

   FOR o = pos1 TO 1 STEP -1
      IF desc[o,o]="L" THEN
         LET pos2 = o-1
      END IF

      IF desc[o,o]="#" THEN
         LET pos3 = o+1
         EXIT FOR
      END IF

   END FOR
   LET orden = desc[pos3,pos2]

RETURN orden,item
END FUNCTION
