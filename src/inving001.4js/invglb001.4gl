{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales movimientos de inventario
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_des   RECORD LIKE inv_mbodegas.*,
		 w_tra       RECORD LIKE inv_mtransac.*,
       w_mae_tra   RECORD LIKE inv_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_mae_tip   RECORD LIKE inv_tipomovs.*,
       w_mae_bod   RECORD LIKE inv_mbodegas.*,
       w_mae_prv   RECORD LIKE inv_provedrs.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       v_products  DYNAMIC ARRAY OF RECORD
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(80), 
        codepq     SMALLINT,
        nomepq     VARCHAR(40,1), 
        canepq     LIKE inv_dtransac.canepq,
        canuni     LIKE inv_dtransac.canuni,
        canmtc     LIKE inv_dtransac.canmtc,
        preuni     LIKE inv_dtransac.preuni,
        totpro     LIKE inv_dtransac.totpro,
        lnkord     LIKE inv_dtransac.lnkord,
        nuitem     LIKE inv_dtransac.nuitem 
       END RECORD, 
       r_products  RECORD
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(100), 
        codepq     SMALLINT,
        canepq     LIKE inv_dtransac.canepq,
        canuni     LIKE inv_dtransac.canuni,
        canmtc     LIKE inv_dtransac.canmtc,
        preunt     LIKE inv_dtransac.preuni,
        totpro     LIKE inv_dtransac.totpro,
        avanza     CHAR(1) 
       END RECORD, 
       totuni      DEC(14,2),
       totval      DEC(14,2),
       reimpresion SMALLINT,
       totlin      SMALLINT,
       flag_sa     SMALLINT,
       nomori      STRING,   
       nomest      STRING,   
       nomdes      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form,
       confirma    SMALLINT,
       flag_tras   SMALLINT,
       g_accion    STRING, 
       g_flagdel   SMALLINT 
END GLOBALS
