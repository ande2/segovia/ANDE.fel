{ 
invglo012.4gl
Mynor Ramirez
Mantenimiento de subcategorias
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE inv_subcateg.*,
       v_sucsxemp  DYNAMIC ARRAY OF RECORD
        tcodcat    LIKE inv_subcateg.codcat,
        tnomcat    LIKE inv_categpro.nomcat, 
        tsubcat    LIKE inv_subcateg.subcat,
        tnomsub    LIKE inv_subcateg.nomsub, 
        tcodabr    LIKE inv_subcateg.codabr 
       END RECORD
END GLOBALS
