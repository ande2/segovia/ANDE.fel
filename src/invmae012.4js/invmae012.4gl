{
invmae012.4gl 
Mynor Ramirez
Mantenimiento de subcategorias 
}

-- Definicion de variables globales 

GLOBALS "invglo012.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae012_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae012_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae012a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invmae012",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Subcategorias"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de subcategorias."
    CALL invqbe012_subcategorias(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva subcategoria."
    LET savedata = invmae012_subcategorias(1) 
   COMMAND "Modificar"
    " Modificacion de una subcategoria existente."
    CALL invqbe012_subcategorias(2) 
   COMMAND "Borrar"
    " Eliminacion de una subcategoria existente."
    CALL invqbe012_subcategorias(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae012_subcategorias(operacion)
 DEFINE wabrcat           LIKE inv_categpro.codabr,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae012_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codcat,
                w_mae_pro.nomsub,
                w_mae_pro.codabr,
                w_mae_pro.tipsub,
                w_mae_pro.lonmed, 
                w_mae_pro.premin,
                w_mae_pro.presug 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON CHANGE codcat
    -- Seleccionando codigo abreviado de la categoria
    LET wabrcat = NULL
    SELECT NVL(x.codabr,"NA")
     INTO  wabrcat 
     FROM  inv_categpro x
     WHERE x.codcat = w_mae_pro.codcat

    -- Concatenando codigo abreviado de la ctagoria y subcategoria
    LET w_mae_pro.codabr = wabrcat CLIPPED
    DISPLAY BY NAME w_mae_pro.codabr 

   BEFORE INPUT 
    -- Verificanod integridad
    IF invqbe012_integridad() THEN
       CALL Dialog.SetFieldActive("codcat",0) 
       CALL Dialog.SetFieldActive("nomsub",0) 
       CALL Dialog.SetFieldActive("codabr",0) 
       CALL Dialog.SetFieldActive("tipsub",0) 
    ELSE
       CALL Dialog.SetFieldActive("codcat",1) 
       CALL Dialog.SetFieldActive("nomsub",1) 
       CALL Dialog.SetFieldActive("codabr",1) 
       CALL Dialog.SetFieldActive("tipsub",1) 
    END IF 

   BEFORE FIELD codcat
    -- Cargando combobox de categorias
    CALL librut003_cbxcategorias() 

   AFTER FIELD codcat 
    --Verificando catagoria
    IF w_mae_pro.codcat IS NULL THEN
       NEXT FIELD codcat
    END IF

   AFTER FIELD nomsub  
    --Verificando nombre de lia subcategoria
    IF (LENGTH(w_mae_pro.nomsub)=0) THEN
       ERROR "Error: nombre de la subcategoria invalida, VERIFICA"
       LET w_mae_pro.nomsub = NULL
       NEXT FIELD nomsub  
    END IF

    -- Verificando que no exista otra subcategoria con el mismo nombre
    SELECT UNIQUE (a.subcat)
     FROM  inv_subcateg a
     WHERE (a.codcat  = w_mae_pro.codcat)
       AND (a.subcat != w_mae_pro.subcat) 
       AND (a.nomsub  = w_mae_pro.nomsub) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra subcategoria con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomsub
     END IF 

   AFTER FIELD codabr  
    --Verificando codigo abreviado 
    IF (LENGTH(w_mae_pro.codabr)=0) THEN
       ERROR "Error: codigo abreviado invalido, VERIFICA"
       LET w_mae_pro.codabr = NULL
       NEXT FIELD codabr  
    END IF

    -- Verificando que no exista otra subcategoria con el mismo codigo
    SELECT UNIQUE (a.subcat)
     FROM  inv_subcateg a
     WHERE (a.subcat != w_mae_pro.subcat) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra subcategoria con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr
     END IF 

   AFTER FIELD tipsub 
    --Verificando tipo de subcategoria 
    IF w_mae_pro.tipsub IS NULL THEN
       ERROR "Error: tipo de subcategoria invalida, VERIFICA"
       NEXT FIELD tipsub  
    END IF

   AFTER FIELD lonmed 
    --Verificando si es lona a la medida 
    IF w_mae_pro.lonmed IS NULL THEN
       ERROR "Error: lona a la medida invalida, VERIFICA"
       NEXT FIELD lonmed  
    END IF

   AFTER FIELD premin
    -- Verificando precio minimo
    IF w_mae_pro.premin IS NULL OR
       w_mae_pro.premin <0 THEN
       LET w_mae_pro.premin = 0
       DISPLAY BY NAME w_mae_pro.premin
    END IF 

   AFTER FIELD presug
    -- Verificando precio minimo
    IF w_mae_pro.presug IS NULL OR
       w_mae_pro.presug <0 THEN
       LET w_mae_pro.presug = 0
       DISPLAY BY NAME w_mae_pro.presug
    END IF 

   AFTER INPUT   
    --Verificando ingreso de datos
    --Verificando catagoria
    IF w_mae_pro.codcat IS NULL THEN
       NEXT FIELD codcat 
    END IF
    IF w_mae_pro.nomsub IS NULL THEN 
       NEXT FIELD nomsub
    END IF
    IF w_mae_pro.codabr IS NULL THEN 
       NEXT FIELD codabr
    END IF
    IF w_mae_pro.tipsub IS NULL THEN 
       NEXT FIELD tipsub
    END IF
    IF w_mae_pro.lonmed IS NULL THEN 
       NEXT FIELD lonmed
    END IF
    IF w_mae_pro.premin IS NULL THEN 
       NEXT FIELD premin
    END IF
    IF w_mae_pro.presug IS NULL THEN 
       NEXT FIELD presug
    END IF

    -- Seleccionando codigo abreviado de la categoria
    LET wabrcat = NULL
    SELECT NVL(x.codabr,"NA")
     INTO  wabrcat 
     FROM  inv_categpro x
     WHERE x.codcat = w_mae_pro.codcat

    -- Concatenando codigo abreviado de la ctagoria y subcategoria
    LET w_mae_pro.codabr[1,2] = wabrcat[1,2] 
    DISPLAY BY NAME w_mae_pro.codabr 

    -- Verificando largo del codigo abreviado
    IF (LENGTH(w_mae_pro.codabr)<4) THEN
       ERROR "Error: codigo abreviado de la subcategoria incompleto, VERIFICA ..." 
       NEXT FIELD codabr 
    END IF 

    -- Verificando que no exista otra subcategoria con el mismo codigo
    SELECT UNIQUE (a.subcat)
     FROM  inv_subcateg a
     WHERE (a.subcat != w_mae_pro.subcat) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra subcategoria con el mismo codigo abreviado, VERIFICA ...",
        "information")
        NEXT FIELD codabr
     END IF 

    -- Verificando precios
    IF w_mae_pro.premin>w_mae_pro.presug THEN
       ERROR "Error: precio minimo mayor a precio sugerido, VERIFICA ..."
       NEXT FIELD premin
    END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae012_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando subcategoria
    CALL invmae012_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso 
 IF (operacion=1) THEN
    CALL invmae012_inival(1) 
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una subcategoria

FUNCTION invmae012_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando subcategoria ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.subcat),0)
    INTO  w_mae_pro.subcat 
    FROM  inv_subcateg a
    IF (w_mae_pro.subcat IS NULL) THEN
       LET w_mae_pro.subcat = 1
    ELSE
       LET w_mae_pro.subcat = w_mae_pro.subcat+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_subcateg   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_subcateg
   SET    inv_subcateg.*      = w_mae_pro.*
   WHERE  inv_subcateg.subcat = w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando subcategorias
   DELETE FROM inv_subcateg 
   WHERE  inv_subcateg.subcat = w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae012_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae012_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.subcat = 0 
   LET w_mae_pro.tipsub = 1 
   LET w_mae_pro.lonmed = 0 
   LET w_mae_pro.premin = 0
   LET w_mae_pro.presug = 0
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.nomsub,w_mae_pro.codabr,
                 w_mae_pro.tipsub,w_mae_pro.premin,w_mae_pro.presug
 DISPLAY BY NAME w_mae_pro.subcat,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
