DATABASE segovia

MAIN
DEFINE
	r RECORD
		codemp		LIKE inv_proenbod.codemp, 
		codsuc		LIKE inv_proenbod.codsuc, 
		codbod		LIKE inv_proenbod.codbod, 
		cditem		LIKE inv_proenbod.cditem, 
		exican		LIKE inv_proenbod.exican
	END RECORD,
	lFecha			DATE

	LET lFecha = TODAY
	
	DECLARE listaprod CURSOR FOR
		SELECT 	a.codemp, a.codsuc, a.codbod, a.cditem, b.exican
			FROM 	fis_proenbod a, inv_proenbod b
			WHERE b.codemp = a.codemp
			AND 	b.codsuc = a.codsuc
			AND 	b.codbod = a.codbod
			AND 	b.cditem = a.cditem

	FOREACH listaprod INTO r.*
		UPDATE	dia_proenbod
			SET	exican = r.exican
			WHERE	codemp = r.codemp
			AND	codsuc = r.codsuc
			AND	codbod = r.codbod
			AND  	cditem = r.cditem
		IF SQLCA.sqlerrd[3] = 0 THEN
			INSERT INTO dia_proenbod VALUES ( lFecha, r.* )
		END IF
	END FOREACH
END MAIN