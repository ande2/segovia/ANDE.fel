################################################################################
# Funcion     : %M%
# ccliente : Modulo para ingreso de catalogo 
# Funciones   : cat_inse()
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        ccliente de la modificacion
################################################################################

GLOBALS "menm0159_glob.4gl"

FUNCTION cat_inse( operacion )
DEFINE
   operacion 	SMALLINT,
   lcondicion  STRING ,
   respuesta   CHAR(06),
   res,
   scr,
   idx			SMALLINT,
   scrd,
   idxd			SMALLINT,
   lselTodos   BOOLEAN 

   LET int_flag = FALSE

   
	--CLEAR FORM
   --LET gtit_enc="MODIFICAR PERMISOS"
   --DISPLAY BY NAME gr_reg.*
   IF operacion = 1 THEN
      CALL limpiar()
      CALL gr_tdet.clear()
      LET gr_reg.est_id=14
   ELSE
      LET nr_reg.* = gr_reg.*
   END IF 

   LET idxd = 0
   DIALOG ATTRIBUTES(UNBUFFERED)

      INPUT BY NAME gr_reg.proc_id, gr_reg.proc_desc
         ATTRIBUTES (WITHOUT DEFAULTS)
         ON KEY (CONTROL-B)
            CASE
               WHEN INFIELD(proc_id)
                  LET int_flag = FALSE
                  LET lcondicion =  " prog_id NOT IN ( SELECT proc_id FROM bproc ) ",
                                    " AND prog_tip = 'P'"
                  --LET ldesc = "perpag_fechai"," ||' - '|| ","perpag_fechaf"
                  CALL librut002_picklist('Busqueda de Procesos','C�digo','Descripci�n',
                                  'prog_id','prog_des',
                                  'mprog',lcondicion,1,0)
                   RETURNING gr_reg.proc_id, gr_reg.proc_desc, INT_FLAG
                   DISPLAY BY NAME gr_reg.*
            END CASE
            
         BEFORE INPUT
            CALL DIALOG.setFieldActive( "proc_id", (operacion = 1) )

         AFTER INPUT
            IF operacion = 1 THEN 
               IF gr_reg.proc_id IS NULL THEN
                  CALL box_valdato("Debe ingresar Id del Proceso.")
                  NEXT FIELD proc_id
               END IF 
               --Verificar si ya existe en bitacora
               SELECT proc_id
               INTO gr_reg.proc_id
               FROM bproc
               WHERE proc_id = gr_reg.proc_id
               IF sqlca.sqlcode = 0 THEN
                  CALL box_valdato("Proceso ya existe en Bitacora.")
                  INITIALIZE gr_reg.* TO NULL 
                  NEXT FIELD proc_id
               END IF
               --Verificar si existe en sistema
               SELECT prog_des
               INTO gr_reg.proc_desc
               FROM mprog
               WHERE prog_id = gr_reg.proc_id
                 AND prog_tip = 'P'
               IF sqlca.sqlcode != 0 THEN
                  CALL box_valdato("No existe proceso con ese Id.")
                  INITIALIZE gr_reg.* TO NULL 
                  NEXT FIELD proc_id
               END IF
            END IF
            IF gr_reg.proc_desc IS NULL THEN
               CALL box_valdato("Debe ingresar descripcion del Proceso.")
               NEXT FIELD proc_desc
            END IF
            
      END INPUT 
   
      DISPLAY ARRAY tree_arr TO sTree.* 

      BEFORE ROW
         LET idxd = 0
         LET idx = ARR_CURR()
         LET scr = SCR_LINE()
         CALL CargarDetalle ( tree_arr[idx].id ) RETURNING lselTodos 

      ON ACTION deleteNode
         LET idx = arr_curr()
         IF idx > 0 THEN 
            CALL EliminarMovimientoTabla(tree_arr[idx].id)
            CALL DIALOG.deleteNode("stree", idx);
            IF tree_arr.getLength() > 0 THEN
               IF idx > tree_arr.getLength() THEN 
                  LET idx = idx - 1
               END IF 
               CALL CargarDetalle ( tree_arr[idx].id ) RETURNING lselTodos
            END IF
         END IF 

      ON ACTION insertNode
         LET idx = arr_curr()
         IF idx > 1 THEN
            IF ObtenerDatosTabla(1) THEN 
               CALL DIALOG.insertNode("stree", idx);
               -- tree[r].parentId has been initialized in insertNode()
               LET tree_arr[idx].id = gr_tab.tab_id
               LET tree_arr[idx].nombre = gr_tab.tab_nom
               LET tree_arr[idx].descripcion = gr_tab.tab_des
               CALL Cargarmovimientotabla(tree_arr[idx].id)
            END IF 
         END IF

      ON ACTION appendNode
        LET idx = arr_curr()
        IF ObtenerDatosTabla(1) THEN 
            LET idx = DIALOG.appendNode("stree", idx)
            -- tree[r].parentId has been initialized in appendNode()
            LET tree_arr[idx].id = gr_tab.tab_id
            LET tree_arr[idx].nombre = gr_tab.tab_nom
            LET tree_arr[idx].descripcion = gr_tab.tab_des
            CALL Cargarmovimientotabla(tree_arr[idx].id)
         END IF 
         
      ON ACTION editNode
         LET idx = arr_curr()
         IF idx > 0 THEN
            LET gr_tab.tab_id = tree_arr[idx].id 
            LET gr_tab.tab_nom = tree_arr[idx].nombre 
            LET gr_tab.tab_des = tree_arr[idx].descripcion
            IF ObtenerDatosTabla(2) THEN 
               LET tree_arr[idx].descripcion = gr_tab.tab_des
            END IF
         END IF 
--
      ON ACTION Todos
            LET lselTodos = NOT lselTodos
            CALL SelecTodos(lselTodos)

      END DISPLAY
      
      INPUT ARRAY gr_det FROM sDet.* 
         ATTRIBUTES(WITHOUT DEFAULTS, AUTO APPEND=FALSE, 
                     DELETE ROW =FALSE, INSERT ROW =FALSE, APPEND ROW =FALSE   )
      
         BEFORE INPUT
      		--CALL DIALOG.setActionHidden("append",1)
				--CALL DIALOG.setActionHidden("insert",1)
				--CALL DIALOG.setActionHidden("delete",1)
            
         ON CHANGE tiene_bitacora
            LET idxd = ARR_CURR()
            CALL MoverDetalle ( gr_det[idxd].* )
         ON CHANGE cam_des
            LET idxd = ARR_CURR()
            CALL MoverDetalle ( gr_det[idxd].* )
         ON CHANGE es_llave
            LET idxd = ARR_CURR()
            IF NOT gr_det[idxd].cam_llave THEN
               LET gr_det[idxd].cam_llave = TRUE
               NEXT FIELD CURRENT 
            END IF 
            CALL SetLlave ( idxd )
         ON CHANGE cam_identifica
            LET idxd = ARR_CURR()
            CALL MoverDetalle ( gr_det[idxd].* )
         
         AFTER ROW
            LET idxd = ARR_CURR()
            LET scrd = SCR_LINE()
            CALL MoverDetalle ( gr_det[idxd].* )

         ON ACTION Todos
            LET lselTodos = NOT lselTodos
            CALL SelecTodos(lselTodos)

      END INPUT

      BEFORE DIALOG 
         CALL DIALOG.setActionHidden("close", 1)
         
      ON ACTION ACCEPT

         --Mover ultimo detalle
         IF idxd > 0 AND gr_det.getLength() > 0 THEN 
            LET idxd = ARR_CURR()
            CALL MoverDetalle ( gr_det[idxd].* )
         END IF 
         IF operacion = 1 THEN 
            IF gr_reg.proc_id IS NULL THEN
               CALL box_valdato("Debe ingresar Id del Proceso.")
               NEXT FIELD proc_id
            END IF 
            --Verificar si ya existe en bitacora
            SELECT proc_id
            INTO gr_reg.proc_id
            FROM bproc
            WHERE proc_id = gr_reg.proc_id
            IF sqlca.sqlcode = 0 THEN
               CALL box_valdato("Proceso ya existe en Bitacora.")
               INITIALIZE gr_reg.* TO NULL 
               NEXT FIELD proc_id
            END IF
            --Verificar si existe en sistema
            SELECT prog_des
            INTO gr_reg.proc_desc
            FROM mprog
            WHERE prog_id = gr_reg.proc_id
              AND prog_tip = 'P'
            IF sqlca.sqlcode != 0 THEN
               CALL box_valdato("No existe proceso con ese Id.")
               INITIALIZE gr_reg.* TO NULL 
               NEXT FIELD proc_id
            END IF
         END IF 
         --
         IF gr_reg.proc_desc IS NULL THEN
            CALL box_valdato("Debe ingresar descripcion del Proceso.")
            NEXT FIELD proc_desc
         END IF 
         
         --CALL box_gradato("Desea guardar la informaci�n.")
         CALL box_pregunta("Desea guardar la informaci�n .")
         RETURNING res
         CASE
            WHEN res = 1
               LET int_flag = FALSE
            WHEN res = 2
               LET int_flag = TRUE
         END CASE
         EXIT DIALOG

      ON ACTION cancel
         LET INT_FLAG = TRUE
         EXIT DIALOG

   END DIALOG
      
	IF int_flag = TRUE THEN
		LET int_flag = FALSE
		LET gr_reg.* = nr_reg.*
      CALL cat_desp15()
      --CALL Limpiar()
		RETURN FALSE
	END IF

   IF GrabarInfo(operacion) THEN
      --CALL CreaTrigger()
      CALL box_valdato("Informacion grabada exitosamente.")
      RETURN TRUE 
   END IF 
   
	RETURN FALSE 
END FUNCTION

FUNCTION ObtenerDatosTabla(loperacion)
DEFINE loperacion SMALLINT 
DEFINE int_flag INTEGER
DEFINE lcondicion STRING 

   OPEN WINDOW formTabla WITH FORM "menm0159_form_tabla"
      --ATTRIBUTE (STYLE = "dialog")

   --Para ingreso de datos
   IF loperacion = 1 THEN 
      INITIALIZE gr_tab.* TO NULL
   END IF  
   
   DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME  gr_tab.tab_id, gr_tab.tab_nom, gr_tab.tab_des
      ATTRIBUTES(WITHOUT DEFAULTS)
         
      BEFORE INPUT
         --CALL fgl_dialog_setkeylabel( "ACCEPT","Grabar" )
         --CALL fgl_dialog_setkeylabel( "INTERRUPT","Cancelar")
         CALL DIALOG.setFieldActive( "tab_id", (loperacion = 1) )

      ON KEY (CONTROL-B)
         CASE
            WHEN INFIELD(tab_id)
				LET int_flag = FALSE
            LET lcondicion =  "tabid NOT IN (SELECT tab_id FROM btab WHERE tab_habilitado=1)",
                              " AND tabtype ='T'",
                              " AND tabname NOT LIKE 'sys%'"
            CALL librut002_picklist('Busqueda De Tablas','C�digo','Nombre',
                            'tabid','tabname','systables',lcondicion, 1, 0 )
               RETURNING gr_tab.tab_id, gr_tab.tab_nom, INT_FLAG
         END CASE

      AFTER FIELD tab_id
         IF loperacion = 1 THEN 
            IF gr_tab.tab_id IS NOT NULL THEN 
               --Verificar si ya fue agregada
               IF ContieneTabla(gr_tab.tab_id) THEN
                  CALL box_valdato("Id de tabla ya fue agregado anteriormente.")
                  INITIALIZE gr_tab.* TO NULL 
                  NEXT FIELD tab_id
               END IF 
               --Verificar si ya existe en bitacora
               SELECT tab_id
               INTO gr_tab.tab_id 
               FROM btab
               WHERE tab_id = gr_tab.tab_id
                 AND tab_habilitado = 1
               IF sqlca.sqlcode = 0 THEN
                  CALL box_valdato("Tabla ya existe en Bitacora.")
                  INITIALIZE gr_tab.* TO NULL 
                  NEXT FIELD tab_id
               END IF
               --Verificar si existe en sistema
               SELECT tabname 
               INTO gr_tab.tab_nom
               FROM systables
               WHERE  tabid = gr_tab.tab_id
               AND tabtype ='T'
               AND tabname NOT LIKE 'sys%'
               IF sqlca.sqlcode != 0 THEN
                  CALL box_valdato("No existe tabla con ese Id.")
                  INITIALIZE gr_tab.* TO NULL 
                  NEXT FIELD tab_id
               END IF
               --
            END IF
         END IF 
      END INPUT

      BEFORE DIALOG 
         CALL DIALOG.setActionHidden("close", 1)
         
      ON ACTION ACCEPT
         IF loperacion = 1 THEN 
            IF gr_tab.tab_id IS NULL THEN
               CALL box_valdato("Debe ingresar Id.")
               NEXT FIELD tab_id
            END IF
            --Verificar si ya fue agregada
            IF ContieneTabla(gr_tab.tab_id) THEN
               CALL box_valdato("Id de tabla ya fue agregado anteriormente.")
               INITIALIZE gr_tab.* TO NULL 
               NEXT FIELD tab_id
            END IF 
            --Verificar si ya existe en bitacora
            SELECT tab_id
            INTO gr_tab.tab_id 
            FROM btab
            WHERE tab_id = gr_tab.tab_id
              AND tab_habilitado = 1
            IF sqlca.sqlcode = 0 THEN
               CALL box_valdato("Tabla ya existe en Bitacora.")
               INITIALIZE gr_tab.* TO NULL 
               NEXT FIELD tab_id
            END IF
            --Verificar si existe en sistema
            SELECT tabname 
            INTO gr_tab.tab_nom
            FROM systables
            WHERE  tabid = gr_tab.tab_id
              AND tabtype ='T'
              AND tabname NOT LIKE 'sys%'
            IF sqlca.sqlcode != 0 THEN
               CALL box_valdato("No existe tabla con ese Id.")
               INITIALIZE gr_tab.* TO NULL 
               NEXT FIELD tab_id
            END IF
         END IF
         --
         IF gr_tab.tab_des IS NULL THEN
            CALL box_valdato("Debe ingresar descripcion.")
            NEXT FIELD tab_des
         END IF
         
         LET INT_FLAG = FALSE 
         EXIT DIALOG 
      ON ACTION CANCEL
         LET INT_FLAG = TRUE 
         EXIT DIALOG
   END DIALOG
   
   IF INT_FLAG THEN --Ingreso cancelado
      LET INT_FLAG = FALSE
      CLOSE WINDOW formTabla
      RETURN FALSE 
   END IF
   CLOSE WINDOW formTabla
   RETURN TRUE 
END FUNCTION

FUNCTION GrabarInfo( lope ) 
DEFINE lope SMALLINT --1 Ingreso 2 Modificacion
DEFINE idx  INTEGER
DEFINE rtab recTab
DEFINE err_msg STRING
DEFINE lsentencia CHAR (5000)  

   BEGIN WORK
   --Grabar Encabezado
   WHENEVER ERROR CONTINUE
   IF lope = 1 THEN
      --
      INSERT INTO bproc (proc_id, proc_desc)
      VALUES (gr_reg.proc_id, gr_reg.proc_desc)
      --
   ELSE 
      --
      IF gr_reg.proc_desc != nr_reg.proc_desc THEN 
         UPDATE bproc
         SET proc_desc = gr_reg.proc_desc
         WHERE proc_id = gr_reg.proc_id
      END IF 
      --
   END IF 
   WHENEVER ERROR STOP 
   IF SQLCA.SQLCODE < 0 THEN
      LET err_msg = err_get(SQLCA.SQLCODE)
      ROLLBACK WORK
      ERROR err_msg
      CALL errorlog(err_msg CLIPPED)
      CALL box_valdato("Error al grabar encabezado.")
      RETURN FALSE 
   END IF
   -- Grabar Detalle de Tablas
   FOR idx = 1 TO tree_arr.getLength ()
      LET rTab.tab_fid = tree_arr[idx].parentid
      LET rTab.tab_id  = tree_arr[idx].id
      LET rTab.tab_nom = tree_arr[idx].nombre
      LET rTab.tab_des = tree_arr[idx].descripcion 
      WHENEVER ERROR CONTINUE
      UPDATE btab
      SET   tab_fid = rTab.tab_fid,
            tab_nom = rTab.tab_nom,
            tab_des = rTab.tab_des,
            proc_id = gr_reg.proc_id,
            tab_habilitado = 1
      WHERE tab_id = rTab.tab_id
      IF sqlca.sqlerrd[3] = 0 THEN --No afecto ningun registro
         INSERT INTO btab (tab_id, tab_fid, tab_nom, tab_des, proc_id)
         VALUES ( rTab.tab_id, rTab.tab_fid, rTab.tab_nom,
                  rTab.tab_des, gr_reg.proc_id)
      END IF 
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         LET err_msg = err_get(SQLCA.SQLCODE)
         ROLLBACK WORK
         ERROR err_msg
         CALL errorlog(err_msg CLIPPED)
         CALL box_valdato("Error al grabar el detalle de tablas.")
         RETURN FALSE 
      END IF
   END FOR
   -- Grabar Detalle
   FOR idx = 1 TO gr_tdet.getLength ()
      WHENEVER ERROR CONTINUE
      IF gr_tdet[idx].cam_id IS NULL THEN
         INSERT INTO bcam (tab_id, cam_nom, cam_des, cam_llave, cam_identifica, cam_bitacora)
         VALUES (gr_tdet[idx].tab_id, gr_tdet[idx].cam_nom, gr_tdet[idx].cam_des,
                  gr_tdet[idx].cam_llave, gr_tdet[idx].cam_identifica, gr_tdet[idx].cam_bitacora)
      ELSE 
         UPDATE bcam
         SET   tab_id = gr_tdet[idx].tab_id,
               cam_nom = gr_tdet[idx].cam_nom,
               cam_des = gr_tdet[idx].cam_des,
               cam_llave = gr_tdet[idx].cam_llave,
               cam_identifica = gr_tdet[idx].cam_identifica,
               cam_bitacora = gr_tdet[idx].cam_bitacora
         WHERE cam_id = gr_tdet[idx].cam_id
      END IF 
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         LET err_msg = err_get(SQLCA.SQLCODE)
         ROLLBACK WORK
         ERROR err_msg
         CALL errorlog(err_msg CLIPPED)
         CALL box_valdato("Error al grabar el detalle.")
         RETURN FALSE 
      END IF
   END FOR
   COMMIT WORK
   --CALL box_valdato("Informacion grabada exitosamente.")
   --CALL cat_desp15()
   RETURN TRUE
END FUNCTION

FUNCTION CreaTrigger()
DEFINE idx  INTEGER
DEFINE rtab recTab
DEFINE err_msg STRING
DEFINE lsentencia CHAR (10000)
   
   -- Grabar Detalle de Tablas
   FOR idx = 1 TO tree_arr.getLength ()
      LET rTab.tab_id  = tree_arr[idx].id
      LET rTab.tab_nom = tree_arr[idx].nombre  
      --Trigger de Insert
      SELECT create_trigger_bit(btab.tab_nom,'I')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         EXIT FOR 
      END IF 
      --Trigger de Update
      SELECT create_trigger_bit(btab.tab_nom,'U')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         EXIT FOR 
      END IF 
      --Trigger de Delete
      SELECT create_trigger_bit(btab.tab_nom,'D')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         EXIT FOR 
      END IF 
   END FOR
   IF SQLCA.SQLCODE < 0 THEN
      LET err_msg = err_get(SQLCA.SQLCODE)
      ROLLBACK WORK
      ERROR err_msg
      CALL errorlog(err_msg CLIPPED)
      CALL box_valdato("Error al grabar trigger.")
      RETURN FALSE 
   END IF
   RETURN TRUE  
END FUNCTION

FUNCTION EliminaTrigger()
DEFINE idx  INTEGER
DEFINE rtab recTab
DEFINE err_msg STRING
DEFINE lsentencia CHAR (5000)
   
   FOR idx = 1 TO tree_arr.getLength ()
      LET rTab.tab_id  = tree_arr[idx].id
      LET rTab.tab_nom = tree_arr[idx].nombre

      --Eliminar Triggers
      SELECT delete_trigger_bit(btab.tab_nom,'I')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      --IF SQLCA.SQLCODE < 0 THEN
         --EXIT FOR 
      --END IF 
      --
      SELECT delete_trigger_bit(btab.tab_nom,'U')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      --IF SQLCA.SQLCODE < 0 THEN
         --EXIT FOR 
      --END IF 
      --
      SELECT delete_trigger_bit(btab.tab_nom,'D')
      INTO lsentencia
      FROM btab WHERE btab.tab_id = rTab.tab_id
      WHENEVER ERROR CONTINUE
      EXECUTE IMMEDIATE lsentencia 
      WHENEVER ERROR STOP
      --IF SQLCA.SQLCODE < 0 THEN
         --EXIT FOR 
      --END IF 
   END FOR
   --IF SQLCA.SQLCODE < 0 THEN
      --LET err_msg = err_get(SQLCA.SQLCODE)
      --ROLLBACK WORK
      --ERROR err_msg
      --CALL errorlog(err_msg CLIPPED)
      --CALL box_valdato("Error al eliminar trigger.")
      --RETURN FALSE 
   --END IF
   RETURN TRUE 
END FUNCTION    