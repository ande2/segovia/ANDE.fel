GLOBALS "menm0159_glob.4gl"

FUNCTION CargarTablas (pid)
DEFINE pid INTEGER 
DEFINE 
	x		  INTEGER,
   rtab    recTab
   
   LET x = 0

   CALL tree_arr.clear()
   
	DECLARE listatab CURSOR FOR 
      SELECT tab_fid, tab_id, tab_nom, tab_des
      FROM  btab
      WHERE proc_id = pid
        AND tab_habilitado = 1
         
   FOREACH listatab INTO rtab.*
      LET x = x + 1
      LET tree_arr[x].nombre = rtab.tab_nom
      LET tree_arr[x].descripcion = rtab.tab_des
      LET tree_arr[x].id = rtab.tab_id
      LET tree_arr[x].parentid = rtab.tab_fid
      LET tree_arr[x].expanded = TRUE
   END FOREACH   
END FUNCTION

FUNCTION CargarMovimiento ( lid )
DEFINE
   idx,
   lid         INTEGER, --proc_id
   r           recDet,
   lfecha      DATE
   
   CALL gr_tdet.clear()
   
   DECLARE ListaDet CURSOR FOR      
   SELECT t2.tab_id, c2.cam_id, c.colno, c.colname, 
   nvl(c2.cam_des,"SIN DESCRIPCION"), 
   nvl(c2.cam_llave,0), nvl(c2.cam_identifica,0), nvl(c2.cam_bitacora,0) 
   FROM   syscolumns c, systables t, btab t2, outer(bcam c2)
   WHERE  t.tabid = c.tabid 
     AND t.tabname = t2.tab_nom
     AND t2.tab_id = c2.tab_id
     AND c.colname = c2.cam_nom
     AND t2.proc_id = lid
     AND t2.tab_habilitado = 1
   ORDER BY 1, c.colno

   FOREACH ListaDet INTO r.*
      CALL gr_tdet.appendElement()
      LET idx = gr_tdet.getLength()
      LET gr_tdet[idx].* = r.*
   END FOREACH
END FUNCTION

FUNCTION CargarMovimientoTabla ( lid )
DEFINE
   idx,
   lid         INTEGER, --tab_id
   r           recDet,
   lfecha      DATE
   
   DECLARE ListaDetT CURSOR FOR      
   SELECT t.tabid, c2.cam_id, c.colno, c.colname, 
          nvl(c2.cam_des,"SIN DESCRIPCION"), 
          nvl(c2.cam_llave,0), nvl(c2.cam_identifica,0), nvl(c2.cam_bitacora,0) 
   FROM   syscolumns c, systables t, outer(bcam c2, btab t2)
   WHERE  t.tabid = c.tabid 
     AND t.tabname = t2.tab_nom
     AND t2.tab_id = c2.tab_id
     AND c.colname = c2.cam_nom
     AND t.tabid = lid
   ORDER BY 1, c.colno

   FOREACH ListaDetT INTO r.*
      CALL gr_tdet.appendElement()
      LET idx = gr_tdet.getLength()
      LET gr_tdet[idx].* = r.*
   END FOREACH
END FUNCTION


FUNCTION CargarDetalle ( pid )
DEFINE 
   pid INTEGER, --tab_id
   idx INTEGER,
   idx2 INTEGER
DEFINE lTieneLlave, lselTodos BOOLEAN
   LET lselTodos = TRUE 
   CALL gr_det.clear()
   FOR idx = 1 TO gr_tdet.getLength()
      IF gr_tdet[idx].tab_id = pid THEN
         CALL gr_det.appendElement()
         LET idx2 = gr_det.getLength()
         LET gr_det[idx2].* = gr_tdet[idx].*
         IF gr_det[idx2].cam_bitacora = 0 THEN 
            LET lselTodos = FALSE
         END IF 
      END IF
   END FOR
   --
   LET lTieneLlave = TRUE 
   IF NOT TieneLlave() THEN
      LET lTieneLlave = FALSE 
      IF gr_det.getLength() > 0 THEN 
         LET gr_det[1].cam_llave = TRUE
         CALL SetLlave(1)
      END IF
   END IF
   IF NOT TieneIdentifica() THEN
      LET lTieneLlave = FALSE 
      IF gr_det.getLength() > 0 THEN 
         LET gr_det[1].cam_identifica = TRUE
         CALL MoverDetalle ( gr_det[1].* )
      END IF 
   END IF
   DISPLAY ARRAY gr_det TO sdet.*
   BEFORE DISPLAY 
   EXIT DISPLAY 
   END DISPLAY 
   RETURN lselTodos
END FUNCTION

FUNCTION EliminarMovimientoTabla ( pid )
DEFINE 
   pid INTEGER, --tab_id
   idx INTEGER,
   idx2 INTEGER
   FOR idx2 = 1 TO tree_arr.getLength()
      IF tree_arr[idx2].parentid = pid THEN
         CALL EliminarMovimientoTabla(tree_arr[idx2].id)
      END IF 
   END FOR 
   LET idx = 1
   WHILE idx <= gr_tdet.getLength()
      IF gr_tdet[idx].tab_id = pid THEN
         CALL gr_tdet.deleteElement(idx)
         CONTINUE WHILE 
      END IF
      LET idx = idx + 1 
   END WHILE 
   --Deshabilitar de BD si fuera el caso
   WHENEVER ERROR CONTINUE
   UPDATE btab
   SET tab_habilitado = 0
   WHERE tab_id = pid
   --
   UPDATE bcam
   SET cam_bitacora = 0
   WHERE tab_id = pid
   WHENEVER ERROR STOP 
END FUNCTION


FUNCTION MoverDetalle (r_det)
DEFINE r_det recDet
DEFINE idx2 INTEGER 
   FOR idx2 = 1 TO gr_tdet.getLength()
      IF gr_tdet[idx2].tab_id = r_det.tab_id AND
         --gr_tdet[idx2].cam_id = r_det.cam_id THEN
         gr_tdet[idx2].colno = r_det.colno THEN 
         LET gr_tdet[idx2].* = r_det.*
         EXIT FOR
      END IF
   END FOR
END FUNCTION

FUNCTION SetLlave (idx2)
DEFINE idx, idx2 INTEGER 
   FOR idx = 1 TO gr_det.getLength()
      IF idx != idx2 THEN 
         LET gr_det[idx].cam_llave = FALSE 
      END IF
      CALL MoverDetalle ( gr_det[idx].* )
   END FOR
END FUNCTION

FUNCTION TieneLlave ()
DEFINE idx INTEGER 
   FOR idx = 1 TO gr_det.getLength()
      IF gr_det[idx].cam_llave THEN 
         RETURN TRUE 
      END IF
   END FOR
   RETURN FALSE 
END FUNCTION

FUNCTION TieneIdentifica ()
DEFINE idx INTEGER 
   FOR idx = 1 TO gr_det.getLength()
      IF gr_det[idx].cam_identifica THEN 
         RETURN TRUE 
      END IF
   END FOR
   RETURN FALSE 
END FUNCTION

FUNCTION SelecTodos (lTiene)
DEFINE lTiene BOOLEAN 
DEFINE idx INTEGER 
   FOR idx = 1 TO gr_det.getLength()
      LET gr_det[idx].cam_bitacora = lTiene  
      CALL MoverDetalle ( gr_det[idx].* )
   END FOR
END FUNCTION

FUNCTION ContieneTabla( tid )
DEFINE tid LIKE btab.tab_id
DEFINE idx SMALLINT 
DEFINE res BOOLEAN
   LET res = FALSE
   FOR idx = 1 TO tree_arr.getLength()
      IF tree_arr[idx].id = tid THEN
         LET res = TRUE
         EXIT FOR 
      END IF 
   END FOR 
   RETURN res
END FUNCTION 
   
FUNCTION ClearDet ( )
   CALL gr_det.clear()
END FUNCTION