################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para cliente
# Funciones   : main
#					 cat_init()
#               cat_menu()
#               encabezado()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################

GLOBALS "menm0159_glob.4gl"

MAIN
DEFINE
   n_param SMALLINT

DEFER INTERRUPT

OPTIONS
INPUT WRAP,
HELP FILE "menm0159_help.ex",
HELP KEY CONTROL-W,
COMMENT 	LINE OFF,
PROMPT 	LINE LAST - 2,
MESSAGE 	LINE LAST - 1,
ERROR 	LINE LAST
                                                                                
LET n_param = num_args()
IF n_param = 0 THEN
   RETURN
ELSE
   LET dbname = arg_val(1)
   DATABASE dbname
END IF
CALL ui.interface.loadStyles("styles.4st")
CALL startlog("menm0159.log")
CALL cat_init(dbname)
CALL modi_init()
CALL cat_busca_init()
CALL cat_menu()

CLOSE WINDOW SCREEN 
END MAIN

FUNCTION cat_init(dbname)
DEFINE
   nombre_depproamento CHAR(50),
   cmd CHAR(40),
   ip_usu, dbname CHAR(20),
   --usuario CHAR(8),
   --vempr_log LIKE db0000:mempr.empr_log,
   fecha_actual DATE,
   dir_destino CHAR(100)
                                                                                
LET dir_destino = fgl_getenv("PWD")
LET dir_destino = dir_destino CLIPPED, "/empr_log.bmp"
                                                                                
--LOCATE vempr_log IN FILE dir_destino
                                                       
INITIALIZE nr_reg.* TO NULL
LET gr_reg.* = nr_reg.*
LET usuario = fgl_getenv("LOGNAME")
LET fecha_actual = today
let hr =fecha_actual
                                                                                
--SELECT mempr.empr_nom, mempr.empr_log,mempr.empr_nomlog
--INTO nombre_empresa, vempr_log,vempr_nomlog 
--FROM mempr
--WHERE mempr.empr_db = dbname

OPEN FORM cat_form FROM "menm0159_form"

DISPLAY FORM cat_form
CLEAR FORM
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

DISPLAY "DEPARTAMENTO" AT 04,41 ATTRIBUTE (BLUE,BOLD)
DISPLAY nombre_depproamento AT 05,41 ATTRIBUTE (BLUE,BOLD)
DISPLAY "Usuario : ", usuario AT 03,60 ATTRIBUTE (BLUE,BOLD)
DISPLAY "  Fecha : ", fecha_actual AT 04,60 ATTRIBUTE (BLUE,BOLD)
--CALL combo_din2("est_id","select est_id,est_desc FROM mest")
CALL librut002_combobox("est_id","select est_id,est_desc FROM mest")

--CALL encabezado()
CALL fgl_settitle("MANTENIMIENTO DE BITACORAS")

END FUNCTION

FUNCTION cat_menu()
DEFINE 
	cuantos		SMALLINT,
   usuario CHAR(8),
   vopciones CHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt     SMALLINT,    --contdor
  tol ARRAY[13] OF RECORD
   opcion   CHAR(15),
   acc      CHAR(15),
   desc_opc CHAR(60),
   des_cod  SMALLINT,
   imagen    CHAR(20),
   linkprop INTEGER
   END RECORD,
   cpo ARRAY[13] OF RECORD
      opcion   CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,
   aui om.DOMnode,
   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   tm  om.DomNode,
   tmg om.DomNode,
   tmi om.DomNode,
   tms om.DomNode,
   f  om.DomNode

   LET cnt = 1
   LET ord_opc = NULL

   -- captura nombre de usuario
   LET usuario = fgl_getenv("LOGNAME")

   -- captura permisos del usuario sobre las opciones del menu
   --CALL men_opc(usuario,"menm0159") RETURNING vopciones
   LET vopciones ="111111111"
   -- carga opciones de menu
   --DECLARE opciones_menu CURSOR FOR
      --SELECT   b.prog_ord,c.des_desc_md,b.prop_desc,
               --c.des_cod,c.des_desc_ct
         --FROM  ande:mprog a,ande:dprogopc b,ande:sd_des c
         --WHERE a.prog_nom = "menm0159"
         --AND   b.prog_id = a.prog_id
         --AND   c.des_tipo = 22
         --AND   c.des_cod = b.des_cod
         --ORDER BY b.prog_ord
         
	--LET cnt = 1
   --
   --FOREACH opciones_menu INTO ord_opc,cpo[cnt].*
          --LET cnt = cnt + 1
   --END FOREACH
   --FREE opciones_menu
--
   --FOR cnt = 1 TO LENGTH(vopciones)
          --LET tol[cnt].acc     =DOWNSHIFT(cpo[cnt].opcion CLIPPED)
          --LET tol[cnt].opcion  =cpo[cnt].opcion CLIPPED
          --LET tol[cnt].desc_opc = cpo[cnt].desc_opc CLIPPED
          --LET tol[cnt].des_cod  = cpo[cnt].des_cod
          --LET tol[cnt].imagen   = DOWNSHIFT(cpo[cnt].imagen CLIPPED)
          --LET tol[cnt].linkprop = cpo[cnt].linkprop
--
   --END FOR
   --
   --LET aui=ui.Interface.getRootNode()
   --LET tb =aui.createChild("ToolBar")
   --LET tbi=createToolBarItem(tb,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --Buscar
   --LET tbi=createToolBarItem(tb,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) --Anterior
   --LET tbi=createToolBarItem(tb,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) --proximo
   --LET tbs=createToolBarSeparator(tb)
   --LET tbi=createToolBarItem(tb,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --Ingreso
   --LET tbi=createToolBarItem(tb,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) --modificar
   --LET tbi=createToolBarItem(tb,tol[6].acc,tol[6].opcion,tol[6].desc_opc,tol[6].imagen) --deshabilitar
   --LET tbi=createToolBarItem(tb,tol[7].acc,tol[7].opcion,tol[7].desc_opc,tol[7].imagen) --habilitar
   --LET tbs = createToolBarSeparator(tb) -- separador
   --LET tbi=createToolBarItem(tb,tol[8].acc,tol[8].opcion,tol[8].desc_opc,tol[8].imagen) -- imprimir
   --LET tbs=createToolBarSeparator(tb)
   --LET tbi=createToolBarItem(tb,tol[9].acc,tol[9].opcion,tol[9].desc_opc,tol[9].imagen) --salir 
--
-- menu top
   --LET f  = ui.Interface.getRootNode()
   --LET tm = f.createChild("TopMenu")
   --LET tmg = tm.createChild("TopMenuGroup")
   --CALL tmg.setAttribute("text","Archivo")
--
   --LET tbi =creaopcion(tmg,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --ingreso
   --LET tbi =creaopcion(tmg,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) --modificar
   --LET tbi =creaopcion(tmg,tol[6].acc,tol[6].opcion,tol[6].desc_opc,tol[6].imagen) --deshabilitar
   --LET tmi =creaopcion(tmg,tol[7].acc,tol[7].opcion,tol[7].desc_opc,tol[7].imagen) --habilitar
   --LET tms =tmg.CreateChild("TopMenuSeparator")
   --LET tmi =creaopcion(tmg,tol[9].acc,tol[9].opcion,tol[9].desc_opc,tol[9].imagen) --salir
   --LET tmg = tm.createChild("TopMenuGroup")
   --CALL tmg.setAttribute("text","Edici�n")
   --LET tmi =creaopcion(tmg,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --buscar
   --LET tms =tmg.CreateChild("TopMenuSeparator")
   --LET tmi =creaopcion(tmg,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) -- anterior
   --LET tmi =creaopcion(tmg,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) -- proximo
   --LET tmg =tm.createChild("TopMenuGroup")
   --CALL tmg.setAttribute("text","Herramientas")

MENU "OPCIONES"
	BEFORE MENU

   LET cuantos = 0

-- despliega opciones a las que tiene acceso el usuario
   --HIDE OPTION ALL
   --FOR cnt = 1 TO LENGTH(vopciones)
		--IF vopciones[cnt] = 1 AND
     		--(tol[cnt].des_cod = 1 OR
     		--tol[cnt].des_cod = 6 OR
     		--tol[cnt].des_cod = 9) THEN
         --DISPLAY tol[cnt].opcion
      	--SHOW OPTION tol[cnt].opcion
      	--LET cuantos = cuantos + 1
   	--END IF
	--END FOR

   --IF cuantos = 0 THEN
      --CALL box_error("Lo siento, no tiene acceso a este programa")
      --EXIT MENU
   --END IF

-- opcion busqueda
	--COMMAND KEY("B") tol[1].opcion tol[1].desc_opc
   COMMAND KEY ("B") "Buscar" "Buscar"
		HELP 2
		CALL busca_cat() RETURNING int_flag, cuantos
		IF int_flag = TRUE THEN
			LET int_flag = FALSE
			IF cuantos > 1 THEN
-- despliega opciones a las que tiene acceso el usuario
        		--HIDE OPTION ALL
        		--FOR cnt = 1 TO LENGTH(vopciones)
	        		--IF vopciones[cnt] = 1 THEN
                  --SHOW OPTION tol[cnt].opcion
                 	--IF tol[cnt].des_cod = 2 THEN
                 		--NEXT OPTION tol[cnt].opcion
                  	--END IF
              	--END IF
           	--END FOR

			ELSE
-- despliega opciones a las que tiene acceso el usuario
			--HIDE OPTION ALL
				--FOR cnt = 1 TO LENGTH(vopciones)
					--IF vopciones[cnt] = 1 AND
						--(tol[cnt].des_cod <> 2 AND
						--tol[cnt].des_cod <> 3) THEN
                  --DISPLAY tol[cnt].opcion
						--SHOW OPTION tol[cnt].opcion
						--IF tol[cnt].des_cod = 2 THEN
							--NEXT OPTION tol[cnt].opcion
						--END IF
					--END IF
				--END FOR
			END IF
		ELSE
-- despliega opciones a las que tiene acceso el usuario
			--HIDE OPTION ALL
        		--FOR cnt = 1 TO LENGTH(vopciones)
               --IF vopciones[cnt] = 1 AND
                  --(tol[cnt].des_cod = 1 OR
                  --tol[cnt].des_cod = 6 OR
                  --tol[cnt].des_cod = 9) THEN
                  --SHOW OPTION tol[cnt].opcion
               --END IF
            --END FOR

		END IF
      
-- opcion anterior
   --COMMAND KEY("A") tol[2].opcion tol[2].desc_opc
   COMMAND KEY("A") "Anterior" "Anterior"
		CALL fetch_cat(-1)


-- opcion Siguiente
	--COMMAND KEY("S") tol[3].opcion tol[3].desc_opc
   COMMAND KEY("S") "Siguiente" "Siguiente"
		CALL fetch_cat(1)

-- opcion modificar
   --COMMAND KEY("O") tol[5].opcion tol[5].desc_opc
   COMMAND KEY("O") "Modificar" "Modificar"
		IF gr_reg.proc_id IS NOT NULL THEN 
         --gr_reg.est_id = 13 THEN 
         DISPLAY "pase"
			CALL cat_inse( 2 ) RETURNING int_flag
			IF int_flag = TRUE THEN
				LET int_flag = FALSE
				CALL limpiar()
-- despliega opciones a las que tiene acceso el usuario
         	--HIDE OPTION ALL
         	--FOR cnt = 1 TO LENGTH(vopciones)
         		--IF vopciones[cnt] = 1 AND
            		--(tol[cnt].des_cod = 1 OR
            		--tol[cnt].des_cod = 6 OR
            		--tol[cnt].des_cod = 9) THEN
            		--SHOW OPTION tol[cnt].opcion
         		--END IF
         	--END FOR
			END IF
		ELSE
			CALL box_valdato("Registro se encuentra Anulado y/o Registro Nulo")

		END IF
      
-- opcion ingreso
   --COMMAND KEY("I") tol[4].opcion tol[4].desc_opc
   COMMAND KEY("I") "Ingreso" "Ingreso"
		CALL cat_inse( 1 ) RETURNING int_flag
		IF int_flag = TRUE THEN
			LET int_flag = FALSE
			CALL limpiar()
-- despliega opciones a las que tiene acceso el usuario
         --HIDE OPTION ALL
         --FOR cnt = 1 TO LENGTH(vopciones)
             --IF vopciones[cnt] = 1 AND
                --(tol[cnt].des_cod = 1 OR
                 --tol[cnt].des_cod = 6 OR
                 --tol[cnt].des_cod = 9) THEN
                 --SHOW OPTION tol[cnt].opcion
              --END IF
         --END FOR

		END IF
      
-- Deshabilitar
   --COMMAND KEY("D") tol[6].opcion tol[6].desc_opc
   COMMAND KEY("D") "Deshabilitar" "Deshabilitar"
		IF gr_reg.proc_id IS NOT NULL THEN 
         --gr_reg.est_id = 13 THEN
				IF HabilitaBitProc(FALSE) THEN END IF 
		END IF
-- Habilitar
   --COMMAND KEY("H") tol[7].opcion tol[7].desc_opc
   COMMAND KEY("H") "Habilitar" "Habilitar"
		IF gr_reg.proc_id IS NOT NULL THEN
         --gr_reg.est_id = 14 THEN 
				IF HabilitaBitProc(TRUE) THEN END IF 
		END IF

 -- opcion imprimir
   --COMMAND KEY ("R") tol[8].opcion tol[8].desc_opc
   COMMAND KEY ("R") "Reporte" "Reporte"
		HELP 8
        --CALL PrintReport ( )

-- opcion salir
   --COMMAND key("Q") tol[9].opcion tol[9].desc_opc
   COMMAND key("Q") "Salir" "Salir"
		HELP 8
		EXIT MENU
END MENU
END FUNCTION

FUNCTION encabezado()
--CALL combo_din2("ctipopedido","SELECT * FROM vntipopedido")
--CALL combo_din2("ccondicionventa","SELECT * FROM excondicionventa")
--CALL combo_din2("ccondicionpago","SELECT * FROM excondicionpago")
--CALL combo_din2("ctransporte","SELECT * FROM vnvisitador")
--CALL combo_din2("ctipoprecio","SELECT * FROM vntipoprecio")
--CALL combo_din2("estado","SELECT * FROM glestado ")
END FUNCTION

FUNCTION usu_destino()
DEFINE
   ip_usu CHAR(20),
   i SMALLINT
                                                                                
LET ip_usu = fgl_getenv("FGLSERVER")
                                                                                
FOR i = 1 TO 20
   IF ip_usu[i] = ":" THEN
      EXIT FOR
   END IF
END FOR
                                                                                
LET ip_usu = ip_usu[1,i-1] CLIPPED
                                                                                
RETURN ip_usu
                                                                                
END FUNCTION