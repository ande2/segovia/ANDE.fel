






{ TABLE "informix".empresas1 row size = 1139 number of columns = 18 index size = 22 }

create table "informix".empresas1 
  (
    bodega smallint,
    nit char(15),
    razon_social char(70),
    nom_sucursal char(70),
    cod_sucursal char(3),
    direccion1 char(70),
    direccion2 char(70),
    depto char(20),
    municipio char(20),
    codigo_postal char(5),
    e_mail char(50),
    telefonos char(80),
    requestor char(40),
    userface char(20),
    url_services varchar(250),
    llave_archivo varchar(70),
    llave_pass varchar(30),
    url_anulacion varchar(250),
    primary key (nit,bodega) 
  );

revoke all on "informix".empresas1 from "public" as "informix";




