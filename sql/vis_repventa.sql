  
DROP VIEW vis_repventa;  

CREATE VIEW sistemas.vis_repventa (fecemi, docvta, ptovta, codemp, totpag, subtot, totiva, usuario, tipdoc, serial, tipo) as 
   select fac.fecemi, --fecemi
         TRIM (fac.nserie ) || '-'  || fac.numdoc, --docvta
         pos.nompos, --ptovta
         emp.codemp, --codemp
         CASE WHEN (tipfac='E') THEN fac.totpag*fac.tascam ELSE fac.totpag END, --totpag
         CASE WHEN (tipfac='E') THEN fac.totpag*fac.tascam ELSE fac.subtot END, --subtot
         fac.totiva, --totiva
         usu.userid, --usuario 
         CASE WHEN (fac.hayord = 0 )  THEN 'PRO'  WHEN (fac.hayord = 1 )  THEN 'ORD'  END, --tipdoc
         fac.lnktra, --serial
         fac.tipdoc  --tipo
    from "sistemas".fac_puntovta pos, "sistemas".glb_empresas emp, "sistemas".fac_mtransac fac, "sistemas".glb_usuarios usu 
   where pos.codemp = emp.codemp
     AND fac.numpos = pos.numpos 
     AND fac.codemp = emp.codemp
     AND fac.usrope = usu.userid
     AND fac.estado = 'V' 
   ;                                                                                                                                        
GO

                                              

