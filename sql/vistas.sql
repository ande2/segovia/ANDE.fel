create view "sistemas".vis_saldosxmovxmes (codemp,codsuc,codbod,aniotr,mestra,cditem,tipope,saluni,salval) as 
  select x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ,NVL (sum(x0.canuni ) ,0 ),NVL (sum(x0.totpro 
    ) ,0 )from "sistemas".inv_dtransac x0 where (x0.estado = 'V'
     ) group by x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ;                                  
                                                             
                                     
create view "sistemas".vis_tipdocxpos (lnktdc,numpos,nomdoc) as 
  select x0.lnktdc ,x0.numpos ,((((('EMPRESA ' || x2.nomabr ) || 
    ' ' ) || TRIM ( BOTH ' ' FROM x1.nomdoc ) ) || ' SERIE ' ) || 
    TRIM ( BOTH ' ' FROM x0.nserie ) ) from "sistemas".fac_tdocxpos 
    x0 ,"sistemas".fac_tipodocs x1 ,"sistemas".glb_empresas x2 
    where (((x0.tipdoc = x1.tipdoc ) AND (x0.estado = 'A' ) ) 
    AND (x2.codemp = x0.codemp ) ) ;                         
                                                             
                                
create view "sistemas".productos (cditem,codcat,subcat,codcol,codprv,codmed,unimed,dsitem,codabr,premin,presug,pulcom,estado,status,fulcam,userid,fecsis,horsis,tipo) as 
  select x0.cditem ,x0.codcat ,x0.subcat ,x0.codcol ,x0.codprv 
    ,x0.codmed ,x0.unimed ,x0.dsitem ,x0.codabr ,x0.premin ,x0.presug 
    ,x0.pulcom ,x0.estado ,x0.status ,x0.fulcam ,x0.userid ,x0.fecsis 
    ,x0.horsis ,0 from "sistemas".inv_products x0  union select 
    x1.lnkord ,x4.codcat ,x3.subcat ,x2.codcol ,0 ,x6.codmed 
    ,x7.unimed ,((((((x3.nomsub || ' ' ) || x8.nomcol ) || ' A LA MEDIDA, ORDEN # '
     ) || x2.lnkord ) || ' LONA ' ) || x2.nuitem ) ,x5.codabr 
    ,x2.precio ,x2.precio ,x5.pulcom ,x5.estado ,x5.status ,x5.fulcam 
    ,x1.userid ,x1.fecsis ,x1.horsis ,x2.nuitem from "sistemas"
    .inv_morden x1 ,"sistemas".inv_dorden x2 ,"sistemas".inv_subcateg 
    x3 ,"sistemas".inv_categpro x4 ,"sistemas".inv_products x5 
    ,"sistemas".inv_medidpro x6 ,"sistemas".inv_unimedid x7 ,"sistemas"
    .inv_colorpro x8 where (((((((((x1.lnkord = x2.lnkord ) AND 
    (x2.subcat = x3.subcat ) ) AND (x3.codcat = x4.codcat ) ) 
    AND (x2.codcol = x8.codcol ) ) AND (x7.nommed = 'UNIDAD' ) 
    ) AND (x6.codabr = 'NOAPL' ) ) AND (x5.codcat = x4.codcat 
    ) ) AND (x5.subcat = x3.subcat ) ) AND (x5.dsitem LIKE ((
    '%' || x3.nomsub ) || '%MEDIDA%' ) ) ) ;               
create view "sistemas".vis_ordentraint (numpos,nombrepos,ordentrabajo,numlona,fechaorden,cliente,cantidad,clase,color,medidas,nombremedida,usuario,serie,documento,ordencompra,fechaofrecida,descripcion,hora) as 
  select x0.numpos ,x5.nompos ,x0.lnkord ,x6.nuitem ,x0.fecord 
    ,x4.nomcli ,x6.cantid ,x2.nomsub ,x3.nomcol ,((x6.xlargo 
    || ' X ' ) || x6.yancho ) ,CASE WHEN (x6.medida = 1 )  THEN 
    'PIES'  WHEN (x6.medida = 0 )  THEN 'METROS'  END ,x4.usrope 
    ,x4.nserie ,x4.numdoc ,x4.ordcmp ,x0.fecofe ,x6.observ ,x0.horsis 
    from "sistemas".inv_morden x0 ,"sistemas".fac_clientes x1 ,
    "sistemas".inv_subcateg x2 ,"sistemas".inv_colorpro x3 ,"sistemas"
    .fac_mtransac x4 ,"sistemas".fac_puntovta x5 ,"sistemas".inv_dorden 
    x6 where (((((((((x1.codcli = x0.codcli ) AND (x2.subcat 
    = x6.subcat ) ) AND (x3.codcol = x6.codcol ) ) AND (x4.lnktra 
    = x0.lnktra ) ) AND (x5.numpos = x0.numpos ) ) AND (x6.tipord 
    = 1 ) ) AND (x6.nofase IN (1 ,2 )) ) AND (x4.estado != 'A'
     ) ) AND (x6.lnkord = x0.lnkord ) ) ;                    
                        
create view "sistemas".vis_repventa (fecemi,docvta,ptovta,codemp,totpag,subtot,totiva,usuario,tipdoc,serial,tipo) as 
  select x2.fecemi ,((x2.nserie [1,1] || ' ' ) || x2.numdoc ) ,
    x0.nompos ,x1.codemp ,x2.totpag ,x2.subtot ,x2.totiva ,x3.userid 
    ,CASE WHEN (x2.hayord = 0 )  THEN 'PRO'  WHEN (x2.hayord = 
    1 )  THEN 'ORD'  END ,x2.lnktra ,x2.tipdoc from "sistemas".fac_puntovta 
    x0 ,"sistemas".glb_empresas x1 ,"sistemas".fac_mtransac x2 
    ,"sistemas".glb_usuarios x3 where (((((x0.codemp = x1.codemp 
    ) AND (x2.numpos = x0.numpos ) ) AND (x2.codemp = x1.codemp 
    ) ) AND (x2.usrope = x3.userid ) ) AND (x2.estado = 'V' ) 
    ) ;                                                      
                                                             
                                              

